using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;

namespace VmaVendingJob.Framework.Shared
{
    public class GlobalConst
    {
        public const string ApplicationName = "VmaVendingJob";
        public static AppVersion ApplicationVersion = new AppVersion(1, 0, 0);
    }
}

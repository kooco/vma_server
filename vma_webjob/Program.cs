﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using SendGrid;
using SendGrid.Helpers.Mail;

using Kooco.Framework.Shared;

using VmaBase.Models;
using VmaBase.Models.DataStorage;
using VmaBase.Providers;

namespace VmaVendingJob
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                InternalJobScheduler.RunScheduled("BuyBuyVendingImport", IntervalType.Minutes, 1, RunVendingImport);
                InternalJobScheduler.RunScheduled("CheckExpiredMachineMsg", IntervalType.Seconds, 59, RunCheckExpiredMachineMsg);
                InternalJobScheduler.RunScheduled("CheckMachinesRecommend", IntervalType.Minutes, 10, RunCheckRecommendedMachines);
                InternalJobScheduler.RunScheduled("UpdateSalesLists", IntervalType.Minutes, 15, RunUpdateSalesLists);
                InternalJobScheduler.RunScheduled("AddDailyCoins", new DateTime(1900, 1, 1, 23, 59, 0), RunAddDailyCoins);
                InternalJobScheduler.RunScheduled("UpdateStatistics", new DateTime(1900, 1, 1, 2, 30, 0), RunUpdateStatistics);
                InternalJobScheduler.RunScheduled("ClearExpiredAPISessions", new DateTime(1900, 1, 1, 3, 50, 0), RunClearExpiredAPISessions);
                InternalJobScheduler.RunScheduled("AutoCouponCreator", new DateTime(1900, 1, 1, 4, 10, 0), RunAutoCouponCreator);
                InternalJobScheduler.SaveTaskSettings();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in VmaWebJob: " + exc.Message);
                Console.WriteLine(exc.StackTrace);
                System.Threading.Thread.Sleep(20000);

                Utils.LogError(exc, "Error in VmaWebJob");
            }
        }

        public static void RunAutoCouponCreator()
        {
            try
            {
                Console.WriteLine("Starting create auto coupon creator..");

                int CouponsCreated = AutoCouponsProvider.CreateAllUsersMissingAutoCoupons();
                Console.WriteLine(CouponsCreated.ToString() + " coupons created");

                Console.WriteLine("Auto coupon creator Finished.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in AutoCouponCreator: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in AutoCouponCreator");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunClearExpiredAPISessions()
        {
            try
            {
                Console.WriteLine("Starting Clear Expired API Sessions (plus login logs)..");

                ApplicationDbContext DbContext = new ApplicationDbContext();
                DbContext.SP_ClearExpiredAPISessions(DateTime.UtcNow);

                Console.WriteLine("Clear Expired API Sessions Finished.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in ClearExpiredAPISessions: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in ClearExpiredAPISessions");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunVendingImport()
        {
            try
            {
                Console.WriteLine("Starting Vending Import..");

                MOCCPImportProvider Import = new MOCCPImportProvider();
                Import.StartVendingImport();

                Console.WriteLine("Vending Import Finished.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in VendingImport: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in VendingImport");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunUpdateSalesLists()
        {
            try
            {
                Console.WriteLine("Starting Sales Lists Update..");

                APIVendingProvider VP = new APIVendingProvider();
                VP.UpdateAllSalesLists();

                Console.WriteLine("Sales Lists Update Finished.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in UpdateSalesLists: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in UpdateSalesLists");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunCheckRecommendedMachines()
        {
            try
            {
                Console.WriteLine("Starting CheckRecommendedMachines..");

                APIVendingProvider AP = new APIVendingProvider();
                AP.CheckMachinesRecommended();

                Console.WriteLine("Finished CheckRecommendedMachines.");
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in CheckRecommendedMachines");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunCheckExpiredMachineMsg()
        {
            try
            {
                Console.WriteLine("Starting CheckExpiredMachineMsg..");

                APIVendingProvider AP = new APIVendingProvider();
                AP.CheckMachineMessagesExpiry();

                Console.WriteLine("Finished CheckExpiredMachineMsg.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in RunCheckExpiredMachineMsg: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in CheckExpiredMachineMsg");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunAddDailyCoins()
        {
            try
            {
                Console.WriteLine("Starting AddDailyCoins..");

                LotteryGameProvider LP = new LotteryGameProvider();
                LP.AddDailyLotteryCoins();

                Console.WriteLine("Finished AddDailyCoins.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in RunAddDailyCoins: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in AddDailyCoins");
                EmailNotifyError(exc).Wait();
            }
        }

        public static void RunUpdateStatistics()
        {
            try
            {
                Console.WriteLine("Starting UpdateStatistics..");

                StatisticsProvider SP = new StatisticsProvider();
                SP.UpdateUserStatistics(2);
                SP.UpdateLotteryStatistics(3);
                SP.UpdateWatersportsStatistics(3);
                SP.UpdateSalesStatistics(3);
                SP.UpdateActivityStatistics(3);

                Console.WriteLine("Finished UpdateStatistics.");
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error in UpdateStatistics: " + exc.Message);
                System.Threading.Thread.Sleep(4000);

                Utils.LogError(exc, "Error in UpdateStatistics");
                EmailNotifyError(exc).Wait();
            }
        }

        #region EmailNotifyError
        public static async Task EmailNotifyError(Exception exc)
        {
            try
            {
                string sPath = System.IO.Path.GetDirectoryName(ConfigManager.InternalSchedulerControlFile);
                string sLastNotifyFile = Path.Combine(sPath, "VmaWebJob_EmailNotification.ctl");
                DateTime LastNotificationSent = DateTime.MinValue;

                if (File.Exists(sLastNotifyFile))
                {
                    try
                    {
                        string sContents = File.ReadAllText(sLastNotifyFile);
                        if (!string.IsNullOrWhiteSpace(sContents))
                        {
                            long Ticks = Convert.ToInt64(sContents.Trim());
                            LastNotificationSent = new DateTime(Ticks);
                        }
                    }
                    catch (Exception) { }
                }

                if (LastNotificationSent < DateTime.UtcNow.AddMinutes(-30))
                {
                    string SendGridApiKey = "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";
                    SendGridClient Client = new SendGridClient(SendGridApiKey);

                    SendGridMessage message = new SendGridMessage();
                    message.From = new EmailAddress("system@kooco.com.tw", "kooco.com System");
                    message.AddTo("armin.cheng@kooco.com.tw", "Armin Cheng");
                    message.Subject = "VMA - An error occured in the VmaWebJob! (" + VmaBase.Shared.ConfigManager.Environment.ToString() + ")";
                    message.PlainTextContent = "An exception occured while trying to execute a job in the VmaWebJob:\n\n" + exc.Message + "\n\nStackTrace: " + exc.StackTrace;

                    await Client.SendEmailAsync(message);

                    File.WriteAllText(sLastNotifyFile, DateTime.UtcNow.Ticks.ToString());
                }
            }
            catch (Exception ex)
            {
                Utils.LogError(ex, "Error in EmailNotifyError");
            }
        }
        #endregion
    }
}

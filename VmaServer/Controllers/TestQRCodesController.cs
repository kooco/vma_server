﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

using AutoMapper;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Providers;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.MOCCP;
using Kooco.Framework.Authentication;

namespace VmaServer.Controllers
{
    public class TestQRCodesController : Controller
    {
        // GET: TestQRCodes
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AjaxGetVendingMachineData(long? VendingMachineId)
        {
            try
            {
                APIVendingProvider VP = new APIVendingProvider();
                return Json(VP.GetTestMachine(VendingMachineId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc);
                throw exc;
            }            
        }

        public class DecryptorParamsDTO
        {
            public string Data { get; set; }
        }

        [HttpPost]
        public ActionResult AjaxTestDecrypt(DecryptorParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    return Json(new BaseResponseDTO(), JsonRequestBehavior.AllowGet);

                string sDecrypted = Utils.BuyBuyAESDecrypt(HttpUtility.UrlDecode(Params.Data));
                return Json(new BaseResponseDTO(sDecrypted));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc);
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError));
            }
        }

        public ActionResult AjaxGetTestMachineQRImage(string QRToken)
        {
            return base.File(Utils.GetTestQRImage(QRToken), "image/png");
        }

        public ActionResult AjaxGetCurrentMachineQR(string Organization, string AssetNo)
        {
            BuyBuyMachineParamsDTO Params = new BuyBuyMachineParamsDTO();
            Params.ApiKey = GlobalConst.BuyBuyApiKey;
            Params.Organization = Organization;
            Params.AssetNo = AssetNo;
            Params.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");

            MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
            return Json(MP.GetMachineQR(Params), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxGetBuyBuyMachineParams(string Organization, string AssetNo)
        {
            string CurrentDate = (string)System.Web.HttpContext.Current.Cache["buybuymachineparamscache"];
            if (string.IsNullOrWhiteSpace(CurrentDate))
            {
                CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");
                System.Web.HttpContext.Current.Cache.Insert("buybuymachineparamscache", CurrentDate, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 1, 0), System.Web.Caching.CacheItemPriority.High, null);
            }

            BuyBuyMachineParamsDTO Params = new BuyBuyMachineParamsDTO();
            Params.ApiKey = GlobalConst.BuyBuyApiKey;
            Params.Organization = Organization;
            Params.AssetNo = AssetNo;
            Params.CurrentDate = CurrentDate;

            string sSerialized = Newtonsoft.Json.JsonConvert.SerializeObject(Params);
            sSerialized = Utils.BuyBuyAESEncrypt(sSerialized);
            return Json(new BaseResponseDTO(sSerialized), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxPollProduct(string Organization, string AssetNo)
        {
            BuyBuyMachineParamsDTO Params = new BuyBuyMachineParamsDTO();
            Params.ApiKey = GlobalConst.BuyBuyApiKey;
            Params.Organization = Organization;
            Params.AssetNo = AssetNo;
            Params.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");

            MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
            BaseResponseDTO Response = MP.QueryMerchandiseSelection(Params);
            if (Response.ReturnCode == ReturnCodes.NoData)
                Response.ReturnCode = ReturnCodes.Success;
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxPollCoupon(string Organization, string AssetNo)
        {
            BuyBuyMachineParamsDTO Params = new BuyBuyMachineParamsDTO();
            Params.ApiKey = GlobalConst.BuyBuyApiKey;
            Params.Organization = Organization;
            Params.AssetNo = AssetNo;
            Params.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");

            MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
            BaseResponseDTO Response = MP.QueryRedeemCoupon(Params);
            if (Response.ReturnCode == ReturnCodes.NoData)
                Response.ReturnCode = ReturnCodes.Success;
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxGetProduct(long SalesTransactionId)
        {
            ApplicationDbContext DbContext = new ApplicationDbContext();
            SalesTransaction ListEntry = DbContext.SalesTransactions.Include(itm => itm.Product).FirstOrDefault(itm => itm.Id == SalesTransactionId);
            if (ListEntry != null)
                return Json(new BaseResponseDTO(Mapper.Map<VendingProduct, VendingProductDTO>(ListEntry.Product)), JsonRequestBehavior.AllowGet);
            else
                return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound));
        }

        public ActionResult AjaxGetProductByMerchandiseID(string MerchandiseID)
        {
            ApplicationDbContext DbContext = new ApplicationDbContext();
            VendingProduct ProductDb = DbContext.VendingProducts.FirstOrDefault(itm => itm.MOCCPGoodsId == MerchandiseID && itm.Status == VmaBase.Models.Enum.VendingProductStatus.Available);
            if (ProductDb!= null)
                return Json(new BaseResponseDTO(Mapper.Map<VendingProduct, VendingProductDTO>(ProductDb)), JsonRequestBehavior.AllowGet);
            else
                return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound));
        }

        public ActionResult AjaxGetProductByInternalID(int internalID)
        {
            ApplicationDbContext DbContext = new ApplicationDbContext();
            VendingProduct ProductDb = DbContext.VendingProducts.FirstOrDefault(itm => itm.MOCCPInternalId == internalID && itm.Status == VmaBase.Models.Enum.VendingProductStatus.Available);
            if (ProductDb != null)
                return Json(new BaseResponseDTO(Mapper.Map<VendingProduct, VendingProductDTO>(ProductDb)), JsonRequestBehavior.AllowGet);
            else
                return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound));
        }

        [HttpPost]
        public ActionResult AjaxCompleteOrder(AjaxCompleteOrderParams Params)
        {
            BuyBuyCompleteOrderParamsDTO ParamsDTO = new BuyBuyCompleteOrderParamsDTO();
            ParamsDTO.ApiKey = GlobalConst.BuyBuyApiKey;
            ParamsDTO.Organization = Params.Organization;
            ParamsDTO.AssetNo = Params.AssetNo;
            ParamsDTO.VmaTransactionId = Params.SalesTransactionId;
            ParamsDTO.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");
            ParamsDTO.Cancel = 0;

            MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
            return Json(MP.CompleteOrder(ParamsDTO), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AjaxCancelOrder(AjaxCompleteOrderParams Params)
        {
            BuyBuyCompleteOrderParamsDTO ParamsDTO = new BuyBuyCompleteOrderParamsDTO();
            ParamsDTO.ApiKey = GlobalConst.BuyBuyApiKey;
            ParamsDTO.Organization = Params.Organization;
            ParamsDTO.AssetNo = Params.AssetNo;
            ParamsDTO.VmaTransactionId = Params.SalesTransactionId;
            ParamsDTO.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");
            ParamsDTO.Cancel = 1;

            MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
            return Json(MP.CompleteOrder(ParamsDTO), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AjaxGetManualProductList(long VendingMachineId)
        {
            APIVendingProvider VP = new APIVendingProvider();
            GetSalesListParamsDTO Params = new GetSalesListParamsDTO();
            Params.AppToken = GlobalConst.ApiKey;
            Params.SessionToken = "exb3b6bb-1875-4a1f-8c5c-ee3ac150a7db";
            Params.VendingMachineId = VendingMachineId;
            Params.OnlyAvailableProducts = true;
            Params.IncludeImages = true;
            return Json(VP.GetSalesList(Params), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AjaxValidatePriceCoupon(ValidateCouponParams Params)
        {
            try
            {
                BuyBuyValidateCouponParamsDTO ParamsDTO = new BuyBuyValidateCouponParamsDTO();
                ParamsDTO.ApiKey = GlobalConst.BuyBuyApiKey;
                ParamsDTO.AssetNo = Params.AssetNo;
                ParamsDTO.Organization = Params.Organization;
                ParamsDTO.CouponToken = Params.CouponToken;
                ParamsDTO.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO ResponseDTO = MP.ValidateCoupon(ParamsDTO);
                return Json(ResponseDTO, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "in AjaxValidatePriceCoupon for CouponToken " + (Params != null ? Utils.SafeString(Params.CouponToken) : "Params null!!"));
                throw new Exception(exc.Message, exc);
            }
        }

        [HttpGet]
        public ActionResult AjaxGetStampsQRActivities(string StampsQR)
        {
            try
            {
                ApplicationDbContext DbContext = new ApplicationDbContext();
                SalesTransaction TransactionDb = DbContext.SalesTransactions.FirstOrDefault(itm => itm.QRToken == StampsQR);
                if (TransactionDb != null)
                {
                    List<string> ActivityList = new List<string>();

                    List<NewsArticle> Articles = DbContext.NewsArticles.Include(itm => itm.Activity).Where(itm => itm.Status == VmaBase.Models.Enum.NewsStatus.Online && itm.ActivityId != null && DateTime.UtcNow >= itm.Activity.LimitStartTime && DateTime.UtcNow <= itm.Activity.LimitEndTime).ToList();
                    foreach (NewsArticle article in Articles)
                    {
                        if (DbContext.ActivityParticipatingProducts.FirstOrDefault(itm => itm.ActivityId == article.ActivityId && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active && itm.ProductId == TransactionDb.ProductId) != null)
                        {
                            if ((article.Activity.AllVendingMachines) ||
                                (DbContext.ActivityParticipatingMachines.FirstOrDefault(itm => itm.ActivityId == article.ActivityId && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active && itm.VendingMachineId == TransactionDb.VendingMachineId) != null))
                            {
                                ActivityList.Add(article.ActivityId.ToString());
                            }
                        }
                    }

                    return Json(new BaseResponseDTO(string.Join(", ", ActivityList.ToArray())), JsonRequestBehavior.AllowGet);
                }

                return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound, "This product/machine combination does not participate in any activity"), JsonRequestBehavior.AllowGet);
            }
            catch(Exception exc)
            {
                Utils.LogError(exc);
                throw exc;
            }
        }

        [HttpPost]
        public ActionResult ManualOrder(ManualOrderParams Params)
        {
            try
            {
                BuyBuyCompleteOrderParamsDTO ParamsDTO = new BuyBuyCompleteOrderParamsDTO();
                ParamsDTO.ApiKey = GlobalConst.BuyBuyApiKey;
                ParamsDTO.Organization = Params.Organization;
                ParamsDTO.AssetNo = Params.AssetNo;
                ParamsDTO.InternalId = Params.InternalId;
                ParamsDTO.BuyBuyTransactionNr = "TEST" + DateTime.UtcNow.Ticks.ToString();
                ParamsDTO.CurrentDate = DateTime.UtcNow.Year.ToString("0000") + DateTime.UtcNow.Month.ToString("00") + DateTime.UtcNow.Day.ToString("00") + DateTime.UtcNow.Hour.ToString("00") + DateTime.UtcNow.Minute.ToString("00") + DateTime.UtcNow.Second.ToString("00");

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                return Json(MP.CompleteOrder(ParamsDTO));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc);
                throw exc;
            }
        }
    }

    public class AjaxCompleteOrderParams
    {
        public string Organization { get; set; }
        public string AssetNo { get; set; }
        public long SalesTransactionId { get; set; }
        public string CurrentDate { get; set; }
    }

    public class ManualOrderParams
    {
        public string Organization { get; set; }
        public string AssetNo { get; set; }
        public int InternalId { get; set; }
    }

    public class ValidateCouponParams
    {
        public string Organization { get; set; }
        public string AssetNo { get; set; }
        public string CouponToken { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects.MOCCP;
using VmaBase.Models.DataTransferObjects;


namespace VmaServer.Controllers
{
    public class VendingMachineQueryAPIController : KoocoBaseApiController
    {
        private const string GeneralNote = "\n[u] Note:[/u]\nThe swagger test form here uses the NOT encrypted version of the API method!\nThe parameters used here are passed directly unencrypted in the url\nand the 'data' property of this response will contain the unencrypted version of the returned data\n" +
            "which also can help for a more clear understand of the process.\n" +
            "The unencrypted version can [b]only[/b] be used with this swagger form!\n" +
            "For using the webservice externally the parameters need to be passed as encrypted JSON string, passed as the Url parameter 'Params'\n" +
            "And the 'data' property of the response will contain an encrypted JSON string\n" +
            "See [url:/files/Vending%20Server%20API.docx]here[/url] for more information about the encryption";


        [Route("api/vma/buybuy/queryqr")]
        [HideInDocs]
        [HttpGet]
        public HttpResponseMessage GetMachineQRCode([FromUri] string Params)
        {
            try
            {
                BuyBuyMachineParamsDTO ParamsDTO = null;
                try
                {
                    ParamsDTO = Utils.DecryptParams<BuyBuyMachineParamsDTO>(Params);
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying to decrypt the parameters: " + Params);
                    return Utils.CreateApiHttpResponse(string.Empty, new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError));
                }

                string RequestGuid = string.Empty;
                bool bDoLogging = ConfigManager.EnableVendingMachineQueryAPISuccessLog;
                if (bDoLogging)
                    Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.GetMachineQR(ParamsDTO);
                if (Result.ReturnCode != ReturnCodes.Success)
                {
                    Utils.LogApiRequest(Request, this);
                    Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption");
                    bDoLogging = true;
                }
                Result.AESEncrypt();

                return Utils.CreateApiHttpResponse(string.Empty, Result, Request, bDoLogging);
            }
            catch (Exception exc)
            {
                if (!ConfigManager.EnableVendingMachineQueryAPISuccessLog)
                    try { Utils.LogApiRequest(Request, this); } catch (Exception) { }

                return Utils.HandleApiException(exc, "API method api/vma/buybuy/queryqr");
            }
        }

        [Route("api/vma/buybuy/queryqr_swagger")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<BuyBuyGetMachineQRResultDTO>))]
        [SwaggerDescription(Description = "Returns the QR code for the given machine assetNo.\n" + GeneralNote, Title = "Get machine QR token",
            ReturnCodes = "1,-1001,-10000")]
        [HttpGet]
        public HttpResponseMessage GetMachineQRCodeSwagger([FromUri] BuyBuyMachineParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    throw new SecurityException("Calling the swagger-version of the VendingMachineQueryAPI's is not allowed on production!");

                string RequestGuid = string.Empty;
                bool bDoLogging = ConfigManager.EnableVendingMachineQueryAPISuccessLog;
                if (bDoLogging)
                    Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.GetMachineQR(Params);
                if (Result.ReturnCode != ReturnCodes.Success)
                {
                    Utils.LogApiRequest(Request, this);
                    bDoLogging = true;
                }

                return Utils.CreateApiHttpResponse(string.Empty, Result, Request, bDoLogging);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/queryqr_swagger");
            }
        }

        [Route("api/vma/buybuy/querymerchselection")]
        [HideInDocs]
        [HttpGet]
        public HttpResponseMessage QueryMerchandiseSelection([FromUri] string Params)
        {
            try
            {
                BuyBuyMachineParamsDTO ParamsDTO = null;
                try
                {
                    ParamsDTO = Utils.DecryptParams<BuyBuyMachineParamsDTO>(Params);
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying to decrypt the parameters: " + Params);
                    return Utils.CreateApiHttpResponse(string.Empty, new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError));
                }

                string RequestGuid = string.Empty;
                bool bDoLogging = ConfigManager.EnableVendingMachineQueryAPISuccessLog;
                if (bDoLogging)
                    Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.QueryMerchandiseSelection(ParamsDTO);
                if (((Result.ReturnCode != ReturnCodes.Success) && (Result.ReturnCode != ReturnCodes.NoData)) || 
                    ((Result.Data is BuyBuyChooseProductResultDTO) && (((BuyBuyChooseProductResultDTO)Result.Data).InternalId > 0)))
                {
                    Utils.LogApiRequest(Request, this);

                    try
                    {
                        // try to log the parameters
                        string sJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(ParamsDTO);
                        Request.Properties.Add(GlobalConst.RequestProperty_EscapedJsonFormResult, "JSON string:\n" + sJsonData);
                    }
                    catch (Exception) { }

                    Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption");
                    bDoLogging = true;
                }
                Result.AESEncrypt();

                return Utils.CreateApiHttpResponse(string.Empty, Result, Request, bDoLogging);
            }
            catch (Exception exc)
            {
                if (!ConfigManager.EnableVendingMachineQueryAPISuccessLog)
                    try { Utils.LogApiRequest(Request, this); } catch (Exception) { }

                return Utils.HandleApiException(exc, "API method api/vma/buybuy/querymerchselection");
            }
        }

        [Route("api/vma/buybuy/querymerchselection_swagger")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<BuyBuyChooseProductResultDTO>))]
        [SwaggerDescription(Description = "This API is made for the vending machines to query if there is a product that needs to be shown\n\n" +
            "If the client app sends chooses a product the vending machine should show, then a message is created for the vending machine\n" +
            "which will be picked up with this API (querymerchselection).\n" +
            "Note that the product choose messages are having a timeout of 1 minute.\n" +
            "Usually the vending machines should poll for the products every few seconds. If after one minute still the 'product selected' message is not retrieved with this API,\n" +
            "then it's assumed that the vending machine is having some problem and the message is being cancelled.\n\n" +
            "Also note that after the product choice is retrieved by this API there is another timeout of 10 minutes!\n" +
            "The vending machine have maximum 10 minutes for an answer with the 'completeorder' API, otherwise the product choice is automatically cancelled!\n" + GeneralNote, Title = "Machine - query product selection",
            ReturnCodes = "1,0,-1001,-10000")]
        [HttpGet]
        public HttpResponseMessage QueryMerchandiseSelectionSwagger([FromUri] BuyBuyMachineParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    throw new SecurityException("Calling the swagger-version of the VendingMachineQueryAPI's is not allowed on production!");

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.QueryMerchandiseSelection(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);

                return Utils.CreateApiHttpResponse(string.Empty, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/querymerchselection_swagger");
            }
        }

        [Route("api/vma/buybuy/queryredeemcoupon")]
        [HideInDocs]
        [HttpGet]
        public HttpResponseMessage QueryRedeemCoupon([FromUri] string Params)
        {
            try
            {
                BuyBuyMachineParamsDTO ParamsDTO = null;
                try
                {
                    ParamsDTO = Utils.DecryptParams<BuyBuyMachineParamsDTO>(Params);
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying to decrypt the parameters: " + Params);
                    return Utils.CreateApiHttpResponse(string.Empty, new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError));
                }

                string RequestGuid = string.Empty;
                bool bDoLogging = ConfigManager.EnableVendingMachineQueryAPISuccessLog;
                if (bDoLogging)
                    Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.QueryRedeemCoupon(ParamsDTO);
                if (((Result.ReturnCode != ReturnCodes.Success) && (Result.ReturnCode != ReturnCodes.NoData)) ||
                    ((Result.Data is BuyBuyRedeemCouponResultDTO) && (((BuyBuyRedeemCouponResultDTO)Result.Data).CouponId != 0)))
                {
                    Utils.LogApiRequest(Request, this);

                    try
                    {
                        // try to log the parameters
                        string sJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(ParamsDTO);
                        Request.Properties.Add(GlobalConst.RequestProperty_EscapedJsonFormResult, "JSON string:\n" + sJsonData);
                    }
                    catch (Exception) { }

                    Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption - queryredeemcoupon");
                    bDoLogging = true;
                }
                Result.AESEncrypt();

                return Utils.CreateApiHttpResponse(string.Empty, Result, Request, bDoLogging);
            }
            catch (Exception exc)
            {
                if (!ConfigManager.EnableVendingMachineQueryAPISuccessLog)
                    try { Utils.LogApiRequest(Request, this); } catch (Exception) { }

                return Utils.HandleApiException(exc, "API method api/vma/buybuy/queryredeemcoupon");
            }
        }

        [Route("api/vma/buybuy/queryredeemcoupon_swagger")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<BuyBuyRedeemCouponResultDTO>))]
        [SwaggerDescription(Description = "This API is made for the vending machines to query if there is a product that needs to be dropped for free\n" +
            "Note: Because there are two types of coupons now (activity coupons and lottery coupons), the 'couponId' also can be a negative value!\n" +
            "Activity coupons will have a positive 'couponId' and lottery coupons will have a negative 'couponId'\n" + GeneralNote, Title = "Machine - query redeem coupon",
            ReturnCodes = "1,-1001,-10000")]
        [HttpGet]
        public HttpResponseMessage QueryRedeemCouponSwagger([FromUri] BuyBuyMachineParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    throw new SecurityException("Calling the swagger-version of the VendingMachineQueryAPI's is not allowed on production!");

                string RequestGuid = Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.QueryRedeemCoupon(Params);                

                return Utils.CreateApiHttpResponse(string.Empty, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/queryredeemcoupon_swagger");
            }
        }

        [Route("api/vma/buybuy/completeorder")]
        [HideInDocs]
        [HttpGet]
        public HttpResponseMessage CompleteOrder([FromUri] string Params)
        {
            try
            {
                BuyBuyCompleteOrderParamsDTO ParamsDTO = null;
                try
                {
                    ParamsDTO = Utils.DecryptParams<BuyBuyCompleteOrderParamsDTO>(Params);

                    try
                    {
                        // try to log the parameters
                        string sJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(ParamsDTO);
                        Request.Properties.Add(GlobalConst.RequestProperty_EscapedJsonFormResult, "JSON string:\n" + sJsonData);
                    }
                    catch (Exception) { }
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying to decrypt the parameters: " + Params);
                    return Utils.CreateApiHttpResponse(string.Empty, new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError));
                }

                string RequestGuid = Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.CompleteOrder(ParamsDTO);
                Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption");
                Result.AESEncrypt();

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/completeorder");
            }
        }

        [Route("api/vma/buybuy/completeorder_swagger")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<BuyBuyCompleteOrderResultDTO>))]
        [SwaggerDescription(Description = "This API is made for the vending machines to send that a product order is completed (product is payed and the machine dropped it)\n" +
            "This API is for both: transactions created by the VMA app and transactions manually done by the user without the app\n" +
            "If its's a VMA transaction, then 'VmaTransactionId' need to be passed. 'BuyBuyTransactionNr' can be null or omitted then\n" +
            "If it's a 'without app' transaction, then 'BuyBuyTransactionNr' and the 'MerchandiseID' need to be passed, 'VmaTransactionId' can be null or omitted then\n\n" +
            "'cancel' should always be passed and defines wether the order should be cancelled ('cancel' = 1) or completed ('cancel' = 0). The default value is '0'.\n" +
            "[blue]Please note that when cancelling the order the success code returned is 5! (Not 1)[/blue]\n\n" +
            "If this 'completeorder' API is called after the order processing timeout of 10 minutes, then the return code -303 will be returned,\n" +
            "which means that the order already has been cancelled automatically.\n" + GeneralNote,
            Title = "Machine - complete product order",
            ReturnCodes = "1,5[Order successfully cancelled],-303,-304,-1001,-10000,-10007,-10008")]
        [HttpGet]
        public HttpResponseMessage CompleteOrderSwagger([FromUri] BuyBuyCompleteOrderParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    throw new SecurityException("Calling the swagger-version of the VendingMachineQueryAPI's is not allowed on production!");

                string RequestGuid = Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.CompleteOrder(Params);

                Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption");

                return Utils.CreateApiHttpResponse(string.Empty, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/completeorder_swagger");
            }
        }

        [Route("api/vma/buybuy/validatecoupon")]
        [HideInDocs]
        [HttpGet]
        public HttpResponseMessage ValidateCoupon([FromUri] string Params)
        {
            try
            {
                BuyBuyValidateCouponParamsDTO ParamsDTO = null;
                try
                {
                    ParamsDTO = Utils.DecryptParams<BuyBuyValidateCouponParamsDTO>(Params);

                    try
                    {
                        // try to log the decrypted parameters
                        string sJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(ParamsDTO);
                        Request.Properties.Add(GlobalConst.RequestProperty_EscapedJsonFormResult, "JSON string:\n" + sJsonData);
                    }
                    catch (Exception) { }
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying to decrypt the parameters: " + Params);
                    return Utils.CreateApiHttpResponse(string.Empty, new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError));
                }

                string RequestGuid = Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.ValidateCoupon(ParamsDTO);
                Utils.APILogJsonResult(RequestGuid, Result, "Result before encryption");
                Result.AESEncrypt();

                return Utils.CreateApiHttpResponse(string.Empty, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/validatecoupon");
            }
        }

        [Route("api/vma/buybuy/validatecoupon_swagger")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<BuyBuyValidateCouponResultDTO>))]
        [SwaggerDescription(Description = "This API is made for the vending machines to validate a lottery price coupon token\n" +
            "for lottery prices, that are won to pick up at the vending machine\n" + GeneralNote,
            Title = "Machine - validate price coupon token",
            ReturnCodes = "1,-405,-406,-408,-409,-1001,-10007,-10200")]
        [HttpGet]
        public HttpResponseMessage ValidateCouponSwagger([FromUri] BuyBuyValidateCouponParamsDTO Params)
        {
            try
            {
                if (ConfigManager.Environment == VmaBase.Models.Enum.HostEnvironment.Production)
                    throw new SecurityException("Calling the swagger-version of the VendingMachineQueryAPI's is not allowed on production!");

                string RequestGuid = Utils.LogApiRequest(Request, this);

                MOCCPMachineQueryProvider MP = new MOCCPMachineQueryProvider();
                BaseResponseDTO Result = MP.ValidateCoupon(Params);

                return Utils.CreateApiHttpResponse(string.Empty, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/buybuy/validatecoupon_swagger");
            }
        }
    }
}

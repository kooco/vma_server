﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;

namespace VmaServer.Controllers
{
    public class APIActivitiesController : KoocoBaseApiController
    {
        [Route("api/vma/activities/get_stamptypes")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<StampTypeListDTO>))]
        [SwaggerDescription(Description = "Retrieves a list of all available stamp types with number and name.\n" +
            "baseImageUrl contains the base url, which is the same for all stamp type images.\n" +
            "Note that the baseImageUrl will already contain the ending slash (/) character\n" +
            "The url for each stamp type can be built as follows then:\n" +
            "    [blue]baseImageUrl[/blue][green]stamptype_##.jpg[/green]\n" +
            "    ## is the number of the stamp type.\n\n" +
            "Example:\n" +
            "baseImageUrl = http://vmadev.oss-cn-shanghai.aliyuncs.com/\n" +
            "number = 02\n" +
            "stamp type image url = http://vmadev.oss-cn-shanghai.aliyuncs.com/stamptype_02.jpg"
            , Title = "Get stamp types list",
            ReturnCodes = "1,-10200")]
        [HttpPost]
        public HttpResponseMessage GetStampTypesList(VmaBase.Models.DataTransferObjects.BaseAPIParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<VmaBase.Models.DataTransferObjects.BaseAPIParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetStampTypesList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_stamptypes");
            }
        }

        [Route("api/vma/activities/get_coupon")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<CouponActivityDTO>))]
        [SwaggerDescription(Description = "Get informations about the coupon/activity of a news article or by the activity/coupon id.\nAlso will return the stamps of this activity, except you set 'noStamps' to 'true'\n" +
            "Every stamp also contains a 'status' field which will return if this stamp already have been collected by the user or not.\nThe user is always the currently logged in user (sessionToken)\n\n" +
            "Note: news/get_list will return the activityId for every activity news. Best (best performance) is to use this activity id to query the coupon.\n\n" +
            "[u]Stamps:[/u]\n" +
            "The stamps returned will contain a 'status' and the 'number' and 'name' of the stamp to be collected or the stamp type already collected.\n" +
            "status 0 = not collected yet. 'number' and 'name' will contain the stamp type to be collected or number '00' and name '', if any stamp type can be collected here.\n" +
            "status 1 = already collected. 'number' and 'name' will contain the stamp type collected.\n\n" +
            "For 'NoStamps activities', if the parameter 'noStamps' is not set to true, always an empty stamps list will be returned (0 stamps)",
            Title = "Get coupon with stamps", ReturnCodes = "1,-10000,-10001,-10007,-10008,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetCoupon(GetActivityCouponParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetActivityCouponParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetActivityCoupon(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_coupon");
            }
        }

        [Route("api/vma/activities/get_coupon_stamps")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerCountResponseDTO<List<CouponStampDTO>>))]
        [SwaggerDescription(Description = "Returns a list of stamps for the given activity (coupon)\n" +
            "Every stamp also contains a 'status' field which will return if this stamp already have been collected by the user or not.\nThe user is always the currently logged in user (sessionToken)\n\n" +
            "The stamps returned will contain a 'status' and the 'number' and 'name' of the stamp to be collected or the stamp type already collected.\n" +
            "status 0 = not collected yet. 'number' and 'name' will contain the stamp type to be collected or number '00' and name '', if any stamp type can be collected here.\n" +
            "status 1 = already collected. 'number' and 'name' will contain the stamp type collected.\n\n" +
            "For 'NoStamps activities' this API will always return an empty list",
            Title = "Get stamps list for coupon", ReturnCodes = "1,-10000,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetCouponStamps(GetCouponStampsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetCouponStampsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetCouponStamps(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_coupon_stamps");
            }
        }

        [Route("api/vma/activities/send_stamps")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<StampsSentDTO>))]
        [SwaggerDescription(Description = "Sends the given stamps from a coupon (activity) to another user as a gift.\n\n" +
            "The list of stamp types to send can be given either with the 'amount' or one stamp type for every single stamp\n\n" +
            "Following examples both are valid:\n" +
            "{\n    'activityId': 1,\n    'userIdReceiver': 7,\n    stamps: [ { 'number': '01, 'amount': 2 }, { 'number': '04', 'amount': 1 }]\n}\n\n" +
            "{\n    'activityId': 1,\n    'userIdReceiver': 7,\n    stamps: [ { 'number': '01 }, { 'number': '01' }, { 'number': '04' }]\n}\n\n" +
            "[u]If the stamps has been sent successfully then a [brown]push notification[/brown] will be sent to the friend:[/u]\n" +
            "[blue]Title:[/blue] 你的朋友送你印花了\n[blue]Message:[/blue] 你的朋友 Nickname 剛剛送你#個印花。來看看你的飲料卷！\n[blue]Content:[/blue] CODE:2;VALUE:ActivityId\n\n" +
            "Code 2 = Coupon stamps received by friend\n" +
            "'ActivityId' will be the id of the coupon (activity) for which the stamps has been sent. This id could be used for the get_coupon API.",
            Title = "Send stamps to friend", ReturnCodes = "1,-201,-202,-204,-205,-206,-207,-214,-10000,-10001,-10007,-10008,-10200,-10203,-10209")]
        [HttpPost]
        public HttpResponseMessage SendStampsToFriend(SendStampsToFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SendStampsToFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.SendStampsToFriend(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/send_stamps");
            }
        }

        [Route("api/vma/activities/get_all_completed_coupons")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerCountResponseDTO<List<CompletedCouponDTO>>))]
        [SwaggerDescription(Description = "[b]Returns a list of ALL completed, but not yet redeemed coupons[/b], this user currently have, as a detailed list.\n" +
            "It's a list of every single coupon with expiry date, received by user, ... which all can be used for exchange to a drink.\n\n" +
            "This list can return more than one coupon per activity!\n" +
            "For a summarized list of collected coupons see [url:api/vma/activities/get_coupon_activities]get_coupon_activities[/url]",
            Title = "Get detailed completed coupons list", ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetAllCompletedCoupons(GetCompletedCouponsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetCompletedCouponsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetAllCompletedCouponsList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_coupons_completed");
            }
        }

        [Route("api/vma/activities/get_coupon_activities")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerCountResponseDTO<List<VmaBase.Models.DataStorage.Resultsets.GetCouponActivityListResultDTO>>))]
        [SwaggerDescription(Description = "Returns a list of all activities with the amount of completed, but not yet redeemed coupons, this user currently have.\n" +
            "It's a summarized list of all coupons per activity.\n" +
            "\n" +
            "[u]What this API can do[/u]\n" +
            "[b]return a list of activities that currently exists, no matter if the user has coupons or not[/b] (onlyCompleted=false)\n" +
            "[b]return a list of activities the user has coupons for (with amount of coupons)[/b] (onlyCompleted=true)\n" +
            "[b]return a list of activities for a vending machine[/b] (vendingMachineId=xxx)\n" +
            "\n" +
            "[b]Note:[/b]\n" +
            "When using this API for the recommended machines list (販賣機地圖的推廣頁面) [brown]please set the 'forPromotionMachinesList' flag to true[/brown]!\n" +
            "\n" +
            "If the user don't have any completed coupons for an activity 'amountTotal' will be 0\n" +
            "'amountTotal' will contain the amount of coupons this user has completed and not used so far for the activity\n" +
            "'amountSendable' is the amount of coupons that can be sent as a gift to other users\n\n" +
            "This list will return only one row per activity!\n" +
            "For a detailed list of collected coupons see [url:api/vma/activities/get_all_completed_coupons]get_all_completed_coupons[/url]",
            Title = "Get activity list with completed coupons amount", ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetCouponActivities(GetCouponActivityListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetCouponActivityListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetCouponActivityList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_coupon_activities");
            }
        }

        [Route("api/vma/activities/get_coupons_uncompleted")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerCountResponseDTO<List<CouponActivityDTO>>))]
        [SwaggerDescription(Description = "Returns a list of not yet completed coupons (coupons where there are still stamps to be collected) with the stamps for this user.\n\n" +
            "This list ONLY returns [brown]uncompleted[/brown] coupons for [brown]every[/brown] currently active activity.\n" +
            "If for an activity so far no uncompleted coupons exists, then this API function will create an uncompleted coupon automatically.\n" +
            "Means if the user just collected all stamps and completed one coupon, another empty coupon will be created.",
            Title = "Get list of (uncompleted) coupons", ReturnCodes = "1,-10000,-10001,-10007,-10008,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetUncompletedCoupons(GetUncompletedCouponsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetUncompletedCouponsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.GetUncompletedCoupons(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/get_coupons_uncompleted");
            }
        }

        [Route("api/vma/activities/send_coupon")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Sends a completed coupon to a friend user.\nOnly completed, but not yet redeemed coupons can be sent!\n\n" +
            "[u]If the stamps has been sent successfully then a [brown]push notification[/brown] will be sent to the friend:[/u]\n" +
            "[blue]Title:[/blue] 你的朋友送你一張免費飲料卷\n[blue]Message:[/blue] 你的朋友 Nickname 送給你一張免費飲料卷。剛快去兌換吧！\n[blue]Content:[/blue] CODE:3;VALUE:ActivityId\n\n" +
            "Code 3 = Completed coupon received by friend\n" +
            "'ActivityId' will be the id of the coupon (activity) for which the stamps has been sent. This id could be used for the get_coupon API.",
            Title = "Send coupon to friend", ReturnCodes = "1,-202,-203,-208[Can not send a coupon to the user him/herself!],-209,-210,-213,-10000,-10001,-10007,-10200,-10203,-10209")]
        [HttpPost]
        public HttpResponseMessage SendCouponToFriend(SendCouponToFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SendCouponToFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.SendCouponToFriend(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/send_coupon");
            }
        }

        [Route("api/vma/activities/user_has_completed_coupons")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerCountResponseDTO<List<CouponActivityDTO>>))]
        [SwaggerDescription(Description = "Returns a boolean (true or false) if the user have completed coupons for this activity or not.\ntrue = this user has at least one completed coupon for this activity\nfalse = this user does not have any completed coupon so far for this activity",
            Title = "Check if user has coupons completed", ReturnCodes = "1,-10000,-10001,-10007,-10008,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage UserHasCompletedCoupons(QueryActivityCouponParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<QueryActivityCouponParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.UserHaveCompletedCoupons(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/user_has_completed_coupons");
            }
        }

        [Route("api/vma/activities/redeem_coupon")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Redeems a coupon for the given activity and lets the vending machine dropping a free drink",
            Title = "Redeem completed coupon", ReturnCodes = "1,-212,-301,-310,-311,-1000,-10000,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage RedeemCoupon(RedeemCouponParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<RedeemCouponParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.RedeemCoupon(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/activities/redeem_coupon");
            }
        }
    }
}
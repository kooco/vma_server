﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Input;

namespace VmaServer.Controllers
{
    public class APISystemController : KoocoBaseApiController
    {
        [Route("api/system/isreviewenv")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<bool>))]
        [SwaggerDescription(Description = "Returns wether this environment is the review environment or not\n'data' will return wether this environment is the review environment or not\n'data'=true: this environment is the review environment.\n'data'=false: this environment is a 'normal' environment", Title = "Check for review environment",
            ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage IsReviewEnvironment()
        {
            try
            {                
                string RequestGuid = Utils.LogApiRequest(Request, this);                

                return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ConfigManager.IsReviewEnvironment));
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/system/isreviewenv");
            }
        }

        [Route("api/system/reportproblem")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Sends an error or problem report to the API server and logs this problem on the server", Title = "Send error/problem report",
            ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage ReportProblem(ReportErrorParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<ReportErrorParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ErrorReportProvider RP = new ErrorReportProvider();
                BaseResponseDTO Result = RP.ReportError(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/system/reportproblem");
            }
        }

        [Route("api/system/checkversion")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        [SwaggerDescription(Description = "Checks if the given app version meets the required minimum version.\n\nReturns true or false.\n", Title = "Send error/problem report",
            ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage CheckAppVersion(CheckAppVersionParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<CheckAppVersionParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APISystemProvider SP = new APISystemProvider();
                BaseResponseDTO Result = SP.CheckAppVersion(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/system/checkversion");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaServer.Controllers
{
    public class APILotteryController : KoocoBaseApiController
    {

        [Route("api/vma/lottery/get_game_info")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<LotteryGameInfoDTO>))]
        [SwaggerDescription(Description = "Returns infos about the current lottery game and the coins the user has available for it", Title = "Retrieving lottery game info",
            ReturnCodes = "1,-400,-403,-10000,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetGameInfo(GetLotteryGameParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetLotteryGameParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.GetLotteryGame(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/get_game_info");
            }
        }

        [Route("api/vma/lottery/get_price_list")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<LotteryPriceItemDTO>>))]
        [SwaggerDescription(Description = "Returns a list of available prices for the current lottery game", Title = "Get price list",
            ReturnCodes = "1,-400,-403,-10000,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetPrices(BaseLotteryParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseLotteryParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.GetPriceList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/get_price_list");
            }
        }

        [Route("api/vma/lottery/draw_game")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<LotteryDrawGameResultDTO>))]
        [SwaggerDescription(Description = "Draws the lottery game with a chance to win a price", Title = "Draw game",
            ReturnCodes = "1,-400,-403,-404,-10000,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage DrawGame(BaseLotteryParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseLotteryParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.DrawGame(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/draw_game");
            }
        }

        [Route("api/vma/lottery/get_coupon_list_won")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<LotteryPriceCouponDTO>>))]
        [SwaggerDescription(Description = "Returns a list of coupons the user has won", Title = "Returns list of won coupons",
            ReturnCodes = "1,-400,-403,-10000,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetCouponListWon(GetLotteryCouponListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetLotteryCouponListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.GetCouponsWon(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/get_coupon_list_won");
            }
        }

        [Route("api/vma/lottery/redeem_coupon")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Redeems a price that can be redeemed at a vending machine.\n" +
            "This is only possible for prices with the flag 'isVendingMachinePrice' set to 'true' ([url:api/vma/lottery/get_coupon_list_won]get_coupon_list_won[/url] will return this field.\n\n" +
            "'vendingMachineId', return by [url:api/qr/qr_action]qr_action[/url] and the 'priceId', returned by [url:api/vma/lottery/get_coupon_list_won]get_coupon_list_won[/url] are needed for redeeming the coupon\n" +
            "If the coupon is valid for the given vending machine, then the machine will drop the free drink.\n\n" +
            "Note, that only vending machines which are part of the lottery activity, can be used to redeem these coupons!", Title = "Redeems a won drink coupon",
            ReturnCodes = "1,-301,-310,-316,-317,-402,-406,-407,-410,-411,-412,-10000,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage RedeemCoupon(RedeemLotteryCouponParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<RedeemLotteryCouponParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.RedeemCoupon(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/redeem_coupon");
            }
        }

        [Route("api/vma/lottery/test_add_coins")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API is ONLY for testing!!\nThis will add the given amount of lottery coins (type 'earned') to the user account for testing purposes.", Title = "TEST - add lottery coins to user account",
            ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage TestAddCoins(TestAddLotteryCoinsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<TestAddLotteryCoinsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.TestAddLotteryCoins(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/test_add_coins");
            }
        }

        [Route("api/vma/lottery/test_delete_coupons")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API is ONLY for testing!!\nThis will [red]DELETE[/red] all won lottery coupons for the given user account!", Title = "TEST - delete lottery coupons",
            ReturnCodes = "1,-10001,-10200")]
        [HttpPost]
        public HttpResponseMessage TestDeleteCoupons(TestResetLotteryCouponsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<TestResetLotteryCouponsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.TestDeleteLotteryCoupons(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/test_delete_coupons");
            }
        }

        [Route("api/vma/lottery/test_reset_coupons")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API is ONLY for testing!!\nThis will reset the state of all lottery coupons of the given user account.\nAll lottery coupons this user already won and have been redeemed will be reset to 'not redeemed', so they can be used again for testing.", Title = "TEST - reset lottery coupons state",
            ReturnCodes = "1,-10001,-10200")]
        [HttpPost]
        public HttpResponseMessage TestResetCoupons(TestResetLotteryCouponsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<TestResetLotteryCouponsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.TestResetLotteryCoupons(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/test_reset_coupons");
            }
        }

        [Route("api/vma/lottery/test_respread_prices")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API is ONLY for testing!!\nThis will reset the prices availability list generated for winning prices!\n\n" +
            "This is due to the winning system. The lottery game draws will use a 'price availability list' internally,\n" +
            "which means every single item that can be won will have a 'release date + time', where basically after that date/time the first lucky player will win this item.\n" +
            "The lottery spreading code will make sure that the prices are spread randomly, but equally on the date range the lottery game is played at.\n" +
            "This makes sure the prices are not already all won in the beginning, and that all prices are given away in the end\n" +
            "This test API will regenerate this price availability list with new random dates/times.\n" +
            "There is still a chance to loose the draw, even if a price already has been released.\n" +
            "Please note that on the test system the beginning date of the lottery is always the current date, when respreading the prices.\n\n" +
            "If you want all prices become available again you also have to set the 'alsoResetPriceAvailability' to true.", Title = "TEST - respread prices on lottery date range",
            ReturnCodes = "1,-10001,-10200")]
        [HttpPost]
        public HttpResponseMessage TestRespreadPrices(TestRespreadLotteryPricesParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<TestRespreadLotteryPricesParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                LotteryGameProvider LP = new LotteryGameProvider();
                BaseResponseDTO Result = LP.TestRespreadLotteryPrices(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/lottery/test_respread_prices");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;

namespace VmaServer.Controllers
{
    public class QRApiController : KoocoBaseApiController
    {
        [Route("api/qr/get_user_qr")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Returns the QR code for an user account.\nYou can also use 'users/user_get' to retrieve the QR code with the user profile.\nThis API will only return the QR code, so it will be more quick.", Title = "Get user QR token",
            ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetUserQRCode(BaseAPIAuthParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseAPIAuthParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                QRProvider QP = new QRProvider();
                BaseResponseDTO Result = QP.GetUserQRCode(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/qr/get_user_qr");
            }
        }

        [Route("api/qr/qr_action")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<QRActionInfoDTO>))]
        [SwaggerDescription(Description = "This API method will parse the given QR-Token and do the accossiated action.\nCurrently supported QR actions:\nIf user QR-Token is sent: Add this user to the friend list\nIf a vending machine QR code is sent: return the vending machine id\n" + 
            "Note that for adding a friend the return code can be 3! (Friend request pending - need to be confirmed by the friend user)\n\n\n" +
            "Note for Stamps QR Codes: there is a Return code 3 implemented, but it never should be returned!\n" +
            "If for stamp QR codes (QR action 3) a return code 3 is returned, it should be considered as ERROR!\n" +
            "Return code 3 would mean - the product/machine combination has more than one activity where it can be collected currently,\n" +
            "THIS HAS NOT BEEN IMPLEMENTED ON THE CLIENT AND ALSO NOT FULLY IMPLEMENTED AT THE SERVER!",
            Title = "Execute QR action",
            ReturnCodes = "1,3[(Operation Pending) Friend request sent. Waiting for confirmation.],-100,-101,-102,-105,-308,-10000,-10001,-10008[User can not add him/herself as a friend],-10200,-10203,-10209,-10210")]
        [HttpPost]
        public HttpResponseMessage ExecuteQRAction(QRActionParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<QRActionParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this, AuditType: (Params != null ? Params.QRToken : string.Empty));
                QRProvider QP = new QRProvider();
                BaseResponseDTO Result = QP.ExecuteQRAction(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request, AuditType: (Params != null ? Params.QRToken : string.Empty), AuditItem: ((int)Result.ReturnCode).ToString());
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/qr/get_user_qr");
            }
        }

        [Route("api/qr/add_stamps")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<AddStampsApiResultDTO>))]
        [SwaggerDescription(Description = "This API method parse the given stamps QR token and will add the stamps for this QR token to the user account\n\n" +
            "Note: if 'activityId' is set, this function will return an error if the activity does not match the QR token scanned\n" +
            "Also note, that this function ONLY accepts stamp QR tokens! If another QR type has been scanned (for example a machine QR), this function will return an error!",
            Title = "Execute QR action",
            ReturnCodes = "1,-102[This QR token is invalid or not a stamps QR (Invalid QR type)],-105,-211,-309,-10001,-10007,-10008,-10200,-10209")]
        // 3[(Operation Pending) More than one activity is matching the QR code. An activity selection list has been returned]
        [HttpPost]
        public HttpResponseMessage AddStamps(AddStampsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<AddStampsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                ActivitiesProvider AP = new ActivitiesProvider();
                BaseResponseDTO Result = AP.AddStamps(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/qr/get_user_qr");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web.Http.Description;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;


namespace VmaServer.Controllers
{
    public class NewsApiController : KoocoBaseApiController
    {
        [HttpGet]
        [Route("api/vma/news/get_list")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetNewsList([FromUri] SearchNewsParamsDTO SearchParams)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (SearchParams == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                if (SearchParams.IncludeNewsInTickerList == null)
                    SearchParams.IncludeNewsInTickerList = true;

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetNews(SearchParams);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getlist");
            }
        }

        [HttpPost]
        [Route("api/vma/news/get_list")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<GetNewsListEntryDTO>))]
        [SwaggerDescription(Description = "Returns a list of available news", Title = "Get news list", ReturnCodes = "1,-10001,-10007,-10200,-10203")]
        public HttpResponseMessage GetNewsListPOST(SearchNewsParamsDTO SearchParams)
        {
            try
            {
                SearchParams = CheckJsonPostParameter<SearchNewsParamsDTO>(SearchParams);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (SearchParams == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                if (SearchParams.IncludeNewsInTickerList == null)
                    SearchParams.IncludeNewsInTickerList = true;

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetNews(SearchParams);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getlist(POST)");
            }
        }

        [HttpGet]
        [Route("api/vma/news/get_ticker")]        
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetNewsTickerList(bool AlsoOffline = false)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetNewsTicker(AlsoOffline);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getlist");
            }
        }

        [HttpPost]
        [Route("api/vma/news/get_ticker")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<NewsTickerDTO>))]
        [SwaggerDescription(Description = "Returns a list of news tickers (banners)", Title = "Get news banner list")]
        public HttpResponseMessage GetNewsTickerListPOST(bool AlsoOffline = false)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetNewsTicker(AlsoOffline);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getlist(POST)");
            }
        }

        [HttpPost]
        [Route("api/vma/news/get_details")]
        [SwaggerDescription(Description = "Returns details for a news article", Title = "Get details for news article")]
        public HttpResponseMessage GetNewsDetails(GetNewsDetailsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetNewsDetailsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetNewsDetails(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getdetails");
            }
        }

        [HttpPost]
        [Route("api/vma/news/get_messages")]
        [SwaggerDescription(Description = "Returns a list of messages (comments) for this news article", Title = "Get news messages (comments)")]
        public HttpResponseMessage GetMessages(GetNewsMessagesParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetNewsMessagesParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetMessages(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/getmessages");
            }
        }

        [HttpPost]
        [Route("api/vma/news/get_categories")]
        [SwaggerDescription(Description = "Returns a list of available news categories", Title = "Get news category list")]
        public HttpResponseMessage GetCategories()
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.GetCategories();

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/get_categories");
            }
        }

        [HttpPost]
        [Route("api/vma/news/save_message")]
        [SwaggerDescription(Description = "Saves a message to a news article", Title = "Add new message")]
        public HttpResponseMessage SaveNewsMessage(SaveNewsMessageParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SaveNewsMessageParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                Params = CheckJsonPostParameter<SaveNewsMessageParamsDTO>(Params);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.SaveMessage(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/savemessage");
            }
        }

        [HttpPost]
        [Route("api/vma/news/add_tags")]
        [HideInDocs]
        public HttpResponseMessage AddTagsToNews(UpdateNewsTagsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<UpdateNewsTagsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                Params = CheckJsonPostParameter<UpdateNewsTagsParamsDTO>(Params);

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.AddTagsToNews(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/add_tags");
            }
        }

        [HttpPost]
        [Route("api/vma/news/remove_tags")]
        [HideInDocs]
        public HttpResponseMessage RemoveTagsFromNews(UpdateNewsTagsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<UpdateNewsTagsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                Params = CheckJsonPostParameter<UpdateNewsTagsParamsDTO>(Params);

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.RemoveTagsFromNews(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/remove_tags");
            }
        }

        [HttpPost]
        [Route("api/vma/news/add_like")]
        [HideInDocs]
        public HttpResponseMessage AddLike(NewsLikeOrShareParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<NewsLikeOrShareParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                Params = CheckJsonPostParameter<NewsLikeOrShareParamsDTO>(Params);

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.AddLikeOrShare(AddLike: true, bAdd: true, Params: Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/add_like");
            }
        }

        [HttpPost]
        [Route("api/vma/news/add_share")]
        [SwaggerDescription(Description = "Increases the share count for the given news article", Title = "Share news (set count)")]
        public HttpResponseMessage AddShare(NewsLikeOrShareParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<NewsLikeOrShareParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params == null)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.MissingArgument, "No arguments given"));

                Params = CheckJsonPostParameter<NewsLikeOrShareParamsDTO>(Params);

                NewsProvider NP = new NewsProvider();
                BaseResponseDTO Result = NP.AddLikeOrShare(AddLike: false, bAdd: true, Params: Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/news/add_share");
            }
        }
    }
}

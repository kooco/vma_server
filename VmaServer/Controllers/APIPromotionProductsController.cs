﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;


namespace VmaServer.Controllers
{
    public class APIPromotionProductsController : KoocoBaseApiController
    {
        [Route("api/vma/promoprod/get_products")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<PromotionProductListDTO>))]
        [SwaggerDescription(Description = "Returns a list of generally available promotion products (禮品).\n\n" +
            "baseImageUrl contains the base url, which is the same for all product images.\n" +
            "Note that the baseImageUrl will already contain the ending slash (/) character\n" +
            "The url for each product can be built as follows then:\n" +
            "    [blue]baseImageUrl[/blue][green]promoprod_##.jpg[/green]\n" +
            "    ## is the unique product number (NOT the id!).\n\n" +
            "Example:\n" +
            "baseImageUrl = http://vmadev.oss-cn-shanghai.aliyuncs.com/\n" +
            "number = 02\n" +
            "product image url = http://vmadev.oss-cn-shanghai.aliyuncs.com/promoprod_02.jpg",
            Title = "Get promo-products list (禮品清單)", ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetProducts(GetPromoProductListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetPromoProductListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIPromotionProductsProvider PP = new APIPromotionProductsProvider();
                BaseResponseDTO Result = PP.GetProductList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/promoprod/get_products");
            }
        }

        [Route("api/vma/promoprod/exchange_product")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
        [SwaggerDescription(Description = "Exchanges points for the given promotion product.\n" +
            "'data' will contain the order number which can be used for pick-up",
            Title = "Exchange points to product (禮品)", ReturnCodes = "1,-300,-301,-302,-10000,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage OrderProduct(PromoOrderParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<PromoOrderParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIPromotionProductsProvider PP = new APIPromotionProductsProvider();
                BaseResponseDTO Result = PP.OrderProduct(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/promoprod/exchange_product");
            }
        }

        [Route("api/vma/promoprod/get_exchanged_products")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<PromotionOrderListDTO>))]
        [SwaggerDescription(Description = "Returns a list of exchanged promotion products (禮品).\n\n" +
            "baseImageUrl contains the base url, which is the same for all product images.\n" +
            "Note that the baseImageUrl will already contain the ending slash (/) character\n" +
            "The url for each product can be built as follows then:\n" +
            "    [blue]baseImageUrl[/blue][green]promoprod_##.jpg[/green]\n" +
            "    ## is the unique product number (NOT the id!).\n\n" +
            "Example:\n" +
            "baseImageUrl = http://vmadev.oss-cn-shanghai.aliyuncs.com/\n" +
            "number = 02\n" +
            "product image url = http://vmadev.oss-cn-shanghai.aliyuncs.com/promoprod_02.jpg",
            Title = "Get exchanged product list (禮品兌換清單)", ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetExchangedProducts(GetExchangedProductsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetExchangedProductsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIPromotionProductsProvider PP = new APIPromotionProductsProvider();
                BaseResponseDTO Result = PP.GetExchangedProducts(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/promoprod/get_exchanged_products");
            }
        }
    }
}

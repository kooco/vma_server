﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;


namespace VmaServer.Controllers
{
    public class APIVendingController : KoocoBaseApiController
    {
        [Route("api/vma/vending/get_vendingmachines")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<VendingMachineDTO>>))]
        [SwaggerDescription(Description = "Returns a list of vending machines around the given location or around the users position.\nIf no location is given then a list of all vending machines is returned.\n\n" +
            "[u]What this API can do[/u]\n" +
            "[b]Return a list of ALL vending machines[/b] (radiusMeters not set or null)\n" +
            "[b]Return a list of vending machines around a position[/b] (radiusMeters set and latitude and longitude (or use user's position))\n" +
            "[b]Return a list of recommended vending machines[/b] (onlyRecommended=true) (recommended = machines, which are part of at least one activity)\n" +
            "[b]Return a list of vending machines for an activity[/b] (activityId=xxx)\n" +
            "\n" +
            "With this API you can get a list of all vending machines or a list of all vending machines around a location (latitude+longitude)\n" + 
            "If a location is given (latitude+longitude) then this API also will return the distance from this location for all vending machines returned\n" + 
            "If 'radiusMeters' is set, then only vending machines within this radius around the given location will be returned\n" +
            "The coordinate system 'WGS 84' is used.\n\n" +
            "'count' will return the amount of total records available with this search", Title = "Get vending machines list",
            ReturnCodes = "1,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetVendingMachines(GetVendingMachinesParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetVendingMachinesParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIVendingProvider VP = new APIVendingProvider();
                BaseResponseDTO Result = VP.GetVendingMachineList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/vending/get_vendingmachines");
            }
        }

        [Route("api/vma/vending/get_test_randommachineqr")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<string>))]
        [SwaggerDescription(Description = "Returns a random vending machine QR token for test purposes.\nThis code can be used for the [url:api/qr/qr_action]qr_action API[/url] to retrieve the sales list id",
            Title = "Test - get random machine QR", ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage GetTestRandomMachineQR()
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIVendingProvider VP = new APIVendingProvider();
                BaseResponseDTO Result = VP.GetTestMachineQR();

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/vending/get_test_randommachineqr");
            }
        }

        [Route("api/vma/vending/get_products")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<VendingProductListDTO>))]
        [SwaggerDescription(Description = "Returns a list of generally available products.\nThis API is independant from any vending machine!\nIt just returns all products which could be available in any of the vending machines.",
            Title = "Get overall products list", ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetProducts(GetProductListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetProductListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIVendingProvider VP = new APIVendingProvider();
                BaseResponseDTO Result = VP.GetProductList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/vending/get_products");
            }
        }

        [Route("api/vma/vending/get_saleslist")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<SalesListEntryDTO>>))]
        [SwaggerDescription(Description = "Returns a list of currently available products for the given vending machine\n\n" +
            "This API will contact the BuyBuy API for getting the availability status for every product before returning the sales list.\n" +
            "The product list will contain only products which are currently available, except you set the parameter 'onlyAvailableProducts' to 'false'.\n" +
            "In this case the field 'Status' will contain if the product is available (1) or already sold out (0)\n\n" +
            "[u]Note:[/u]\nThere is [blue]special behavior[/blue] for the ReturnCodes [blue]-306 and -307[/blue]!\nIf the machine is currently processing another order (-307) or still have an open order message not yet processed (-306)\nthen -307 or -306 is returned as 'ReturnCode',\n" +
            "but the sales list anyway will be returned!\nThis makes it possible for the client app to show the sales list anyway\nwhile waiting for the previous order to be completed.\n" +
            "The [url:api/vma/vending/start_order]start_order API[/url] also will return -306 or -307 if the machine still is not ready\n\n" +
            "Please note that -307 usually will happen if another user still has not finished his order (did not insert the coin so far, for example),\n" +
            "but -306 often could mean that the machine has some problem or there are network related problems, because it did not even read the last order message so far!\n" + 
            "-306 can happen in a 'regular' way if two users trying to order a product at the same machine within the same time (maximum 5 seconds difference)\n" +
            "because the machine should read the order message within maximum 5 seconds.\n\n" +
            "On the test environment -306 can happen more often if the Virtual test vending machine is not used, so noone is processing the order message", 
            Title = "Product (sales) list for vending machine", ReturnCodes = "1,-306,-307,-1000,-10000[There is no vending machine with this id],-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetSalesList(GetSalesListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetSalesListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIVendingProvider VP = new APIVendingProvider();
                BaseResponseDTO Result = VP.GetSalesList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/vending/get_saleslist");
            }
        }

        [Route("api/vma/vending/start_order")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<bool>))]
        [SwaggerDescription(Description = "Starts the order process for a sales product\n" +
            "This will create an initial product order and sends a message to the vending machine tho show the product\n" +
            "The user now can pay the product and the vending machine will drop the product then.\n" +
            "After the transaction is completed and the machine confirms the transaction with the server\n" +
            "the server will send a [brown]push notification[/brown] to the app.\n\n" +
            "The [u]push notification[/u] for a complete order will look like this:\n" +
            "[blue]Title:[/blue] 訂單完成\n[blue]Message:\nContent:[/blue] CODE:5;VALUE:ProductId\n\n'Content' is the push content not shown to the user.\n'CODE' is the push action - in this case 5, means: a (vending machine) product order has been completed\n'VALUE' contains the id of the product ordered (can be used for a further verification)\n\n" +
            "[u]Returned 'data'[/u]\n" +
            "'data' will be a boolean value, which contains wether this product has stamps assigned that can be collected or not.\n" +
            "true = this product has stamps assigned. The machine will show a QR code for collecting the stamps after the transaction is completed.\n" +
            "false = this product does not have any stamps assigned. There will be no QR code after the finished transaction.",
            Title = "Start ordering product (send to machine)", ReturnCodes = "1,-301,-306,-307,-310,-10000[There is no sales list or vending machine with this id],-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage StartOrderProduct(StartOrderProductParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<StartOrderProductParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIVendingProvider VP = new APIVendingProvider();
                BaseResponseDTO Result = VP.StartOrderProduct(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result, Request);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/vending/start_order");
            }
        }
    }
}

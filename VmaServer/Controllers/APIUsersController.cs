﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;

namespace VmaServer.Controllers
{
    public class APIUsersController : KoocoBaseApiController
    {
        [Route("api/vma/users/user_login")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "'data' will contain the new sessionToken after a successful login!\nThis sessionToken is valid for 30 minutes until the next API call with a 'sessionToken' parameter.", Type = typeof(BaseResponseDTO))]
        [SwaggerDescription(Description = "Login for all user types.\nWith loginType you can choose the type of login (User/Pwd, Facebook, Wechat, ..)\nThe user must be already registered. Use user_register to register a new user.\n" +
            "\nFor SMS login the login process is as following:\n*) send loginType = 10 and user credentials (Success = returnCode 3)\n*) wait for the SMS verification code to receive\n*) User enters verfication code or app retrieves it automatically.\n*) send loginType = 11 with the user credentials and verification code (Success = returnCode 1)\n*) User is logged in\n\n" +
            "The new sessionToken for this user is returned in 'data'. This sessionToken can be used for all other API calls requiring the sessionToken parameter.\nThis sessionToken is valid for 30 minutes until the next API call with a 'sessionToken' parameter.\n\n" +
            "[color:brown]Note that for loginType 10 the returnCode for a successful operation is 3, not 1!\nFor loginType 11 it will be 1 then.[/color]\n", 
            Title = "User Login", ReturnCodes = "1,3[(Operation Pending) SMS has been sent. Waiting for verification code (loginType 11)],-10001,-10006,-10007,-10020,-10200,-10201,-10204,-10206,-10207,-10209,-10210")]
        [HttpPost]
        public HttpResponseMessage LoginUser(GetLoginAPIParamsDTO LoginParams)
        {
            try
            {
                LoginParams = CheckJsonPostParameter<GetLoginAPIParamsDTO>(LoginParams);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.LoginUser(LoginParams);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/login_user");
            }
        }

        [Route("api/vma/users/create__test__records")]  
        [HideInDocs]      
        [HttpGet]
        public HttpResponseMessage CreateTestRecords()
        {
            try
            {
                TestProvider TP = new TestProvider();
                TP.CreateTestUsers(10000);

                return Utils.CreateApiHttpResponse("0000", "Okay");
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/create__test__records");
            }
        }

        [Route("api/vma/users/user_register")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "'data' will contain the new account name. This is the way how you can get the generated account name.", Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Register a new user for use with the application\nIn VMA SMS registration is activated.\nThat means after successful registration an SMS will be sent to the phone number given with a verification code\n" +
            "Use the [blue]user_activate[/blue] API to confirm the registration with this verification code!\n\nNote: the 'account' will be generated automatically and returned in the 'data' parameter!\n\n" +
            "[color:brown]Note: Because SMS registration is activated the return code for a successful registration will be 3, not 1![/color]\n\n" +
            "[color:brown]Note also: 'sessionToken' is only for user_update and is NOT needed here! Also 'account' can be omitted, as it will be generated automatically[/color]\n\n" +
            "[u]If this user registered with a recommended account id then this friend will receive a [brown]push notification[/brown]:[/u]\n[blue]Title:[/blue] 你的好友完成註冊了\n[blue]Message:[/blue] 你的好友 Nickname 完成註冊了。你收到了一個硬幣。快去試試你的好手氣！\n[blue]Content:[/blue] CODE: 7;VALUE: UID\n\n'Content' is the push content not shown to the user.\n'CODE' is the push action - in this case 7, means: a friend recommended by you has been registered successfully. 1 Lottery coin received.\n'VALUE' contains the UID of the friend who has been recommended\n\n", 
            Title = "Register new user (Registration Part 1)",
            ReturnCodes = "1,3[(Operation Pending) Registration accepted but user not active. Verification SMS sent. Waiting for verification code],-10001[The given recommended account id does not exist],-103,-104,-10005,-10006,-10007,-10008,-10011,-10012,-10020,-10100,-10101,-10102,-10104,-10105,-10200,-10201,-10204,-10208")]
        [HttpPost]
        public HttpResponseMessage RegisterUser(UserProfileDTO UserData)
        {
            try
            {
                UserData = CheckJsonPostParameter<UserProfileDTO>(UserData);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.RegisterUser(UserData);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/user_register");
            }
        }

        [Route("api/vma/users/user_activate")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This will activate a pending registration.\nIf SMS registration is activated (in VMA it is), then after the registration a SMS will be sent to the given phone number with a verification code\n" +
            "Use this API to confirm the registration with the verification code in the SMS.\n\nYou can use the account name OR the phone number to do the verification. (Pass either 'account' or 'phone')\n", 
            Title = "Confirm SMS registration (Registration Part 2)",
            ReturnCodes = "1,-10001,-10200,-10206,-10209")]
        [HttpPost]
        public HttpResponseMessage Activateuser(ActivateUserParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<ActivateUserParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.ActivatePendingUser(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/user_activate");
            }
        }

        [Route("api/vma/users/user_update")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This will update the user profile information for the currently logged in user.\nWith that API you can change every user information you want, except the user id, userToken and loginType.\n" +
            "Set 'null' to the fields or just omit the fields you don't want to change.\n[color:brown]'Account' only should be set if you want to change the account name![/color]\n\n" +
            "[color:brown]Note: the social login userToken can NOT be changed with the user_update API! Use 'user_add_sociallogin' instead![/color]", Title = "Update profile information",
            ReturnCodes = "1,-103,-104,-10001,-10005,-10006,-10007,-10011,-10012,-10101,-10102,-10104,-10200,-10201,-10203,-10204,-10208")]
        [HttpPost]
        public HttpResponseMessage UpdateUser(UserProfileDTO UserData)
        {
            try
            {
                UserData = CheckJsonPostParameter<UserProfileDTO>(UserData);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.UpdateUser(UserData);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/user_update");
            }
        }

        [Route("api/vma/users/location_update")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Updates the current location of the user (Latitude + Longitude)\nThis position will be used then for querying vending machines around a radius\nNote: the location can only be set for the currently logged in user!\n", 
            Title = "Update current user's position",
            ReturnCodes = "1,-10001,-10003,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage UpdateLocation(UpdateLocationParamsDTO UserData)
        {
            try
            {
                UserData = CheckJsonPostParameter<UpdateLocationParamsDTO>(UserData);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.UpdateLocation(UserData);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/location_update");
            }
        }

        [Route("api/vma/users/user_get")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<UserProfileDTO>))]
        [SwaggerDescription(Description = "Return user profile information for a user\nThe user profile can be retrieved via the account name or the user id.\nYou can search by UID, account, userToken or phone", Title = "Get user profile",
            ReturnCodes = "1,-10001,-10004,-10007,-10200")]
        [HttpPost]
        public HttpResponseMessage GetUser(GetUserParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetUserParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.GetUser(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/user_get");
            }
        }

        [Route("api/vma/users/user_add_sociallogin")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Adds a new social login for an existing user (or changes the userToken for an existing one).\nThis API only should be used if one user account can have more than one social platforms connected.\nThen with this API you can add an additional social platform login.\n\n" +
            "Changing the bound userToken for a social platform is also possible with this API\nThe API will overwrite the userToken if another one already existed already for the same social platform\nFor each social platform only one userToken can exist.", Title = "Add additional social login",
            ReturnCodes = "1,-10001,-10004,-10106,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage AddSocialLogin(AddRemoveSocialLoginAPIParamsDTO<VmaBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.AddSocialLogin(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/user_add_sociallogin");
            }
        }

        [Route("api/vma/users/user_verify_phone")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Confirms a changed phone number which before has been changed with user_register or user_update and now should be verified with the SMS token", Title = "Verify phone number (SMS)",
            ReturnCodes = "1,-10000,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage VerifyUserPhoneNumber(VerifyPhoneNumberParamsDTO<VmaBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
                Params = CheckJsonPostParameter<VerifyPhoneNumberParamsDTO<VmaBase.Models.DataStorage.ApplicationDbContext>>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.VerifyUserPhoneNumber(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/##overallprojectname_lower##/users/user_verify_phone");
            }
        }

        [Route("api/vma/users/user_reg_resendsms")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Resends the SMS code for an already registered, but not yet activated user, who did the SMS registration", Title = "Resend Registration SMS",
            ReturnCodes = "1,-10000,-10001,-10007,-10016,-10017,-10200,-10203,-10209")]
        [HttpPost]
        public HttpResponseMessage RegistrationResendSMS(RegistrationResendSMSParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<RegistrationResendSMSParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.RegistrationResendSMS(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/##overallprojectname_lower##/users/user_reg_resendsms");
            }
        }

        [Route("api/vma/users/search_byphone")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<UserSearchResultDTO>>))]
        [SwaggerDescription(Description = "Searches for users which have the given phone numbers assigned.\nThe parameter is a list of phone numbers to be searched for.\nThe result will be all users matching to one of the phone numbers.\n" +
            "\nThis API makes it possible to search for users which are in the contact list of the current logged in user.\nThen a list of possible friends can be showed which can be added via the add_friend API then\n\n" +
            "The phone numbers given don't need to match completely the phone number given on registration\nSeveral formats are supported (+886..., 00886..., 0908..., 908...) because just the last 7 digits of the phone number will be used for searching for matching users", 
            Title = "Get user list by matching phone numbers",
            ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage SearchUsersByPhone(SearchUsersByPhoneParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SearchUsersByPhoneParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.SearchUsersByPhoneNr(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/search_byphone");
            }
        }

        [Route("api/vma/users/get_friendlist")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<UserSearchResultDTO>>))]
        [SwaggerDescription(Description = "Retrieves the list of friends for the currently logged in user.\n" +
            "No recently added friends (added within the last 7 days) will be returned.\nWith the parameter 'recentAlso' set to 'true' also recently added friends will be returned.\n\n" +
            "[u]Friend list and requests list working together[/u]\nThe [i]Default[/i] behavior of get_friendrequests and get_friendlist is to return the result the other API is missing:\n\n" +
            "[brown]get_friendrequests:[/brown] return all pending requests with no time-limit and return confirmed(+declined) friends which have been added [u]within the last week[/u]\n" +
            "[blue]get_friendlist:[/blue] return all confirmed friends which have been added [u]more than one week before[/u]\n" +
            "\n" +
            "The default behavior can be changed with the parameter 'alsoRecent'. If that is set to true then also recently added (less than one week ago) friends will be returned"
            , Title = "Get friend list", 
            ReturnCodes = "1,-10001,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetFriendList(GetFriendListParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetFriendListParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.GetFriendList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/get_friendlist");
            }
        }

        [Route("api/vma/users/add_friend")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Add a new friend to this users friend list.\nNote: A friend request will be sent to the user\nThe friend will be in 'Pending' state in the beginning until the friend user accepted the friend request!\n\n" +
            "[u]If the friend request was created successfully then a [brown]push notification[/brown] will be sent to the friend:[/u]\n[blue]Title:[/blue] 你有一個交友邀請\n[blue]Message:[/blue] Nickname (ID Account) 想加你好友\n[blue]Content:[/blue] CODE:1;VALUE:Account\n\n'Content' is the push content not shown to the user.\n'CODE' is the push action - in this case 1, means: a user wants to add you as a friend\n'VALUE' contains the account name of the user who wants to add this user as a friend\n\n" +
            "Note: if at the same time the friend request is sent already this other user has sent a friend request to that user, then the friend request automatically is accepted and 1 is returned as returnCode.",
            Title = "Add new friend to friend list", ReturnCodes = "1[Friend successfully added and confirmed],3[(Operation Pending) Friend request sent. Waiting for confirmation.],-100,-101,-215,-10000,-10001,-10007,-10008[User can not add him/herself as a friend],-10009,-10200,-10203,-10209,-10210")]
        [HttpPost]
        public HttpResponseMessage AddFriend(AddRemoveFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<AddRemoveFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.AddFriend(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/add_friend");
            }
        }

        [Route("api/vma/users/remove_friend")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Removes a friend from the friend list.", Title = "Remove friend from friend list", ReturnCodes = "1,-10000,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage RemoveFriend(AddRemoveFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<AddRemoveFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.RemoveFriend(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/remove_friend");
            }
        }

        [Route("api/vma/users/accept_friend")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Accepts a received friend request", Title = "Accept friend request", ReturnCodes = "1,-10000[There is no pending friend request for this friend]")]
        [HttpPost]
        public HttpResponseMessage AcceptFriend(AddRemoveFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<AddRemoveFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.SetFriendRequestStatus(Params, bConfirm: true);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/accept_friend");
            }
        }

        [Route("api/vma/users/decline_friend")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Declines a received friend request", Title = "Decline friend request", ReturnCodes = "1,-10000[There is no pending friend request for this friend]")]
        [HttpPost]
        public HttpResponseMessage DeclineFriend(AddRemoveFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<AddRemoveFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.SetFriendRequestStatus(Params, bConfirm: false);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/decline_friend");
            }
        }

        [Route("api/vma/users/get_friendrequests")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<List<UserFriendRequestDTO>>))]
        [SwaggerDescription(Description = "Returns a list of friend requests sent and/or received (and recently added friends)\nAlso recently (within the last 7 days) added friends, which already have been confirmed, will be returned.\n\n" +
            "[u]Friend list and requests list working together[/u]\nThe [i]Default[/i] behavior of get_friendrequests and get_friendlist is to return the result the other API is missing:\n\n" +
            "[brown]get_friendrequests:[/brown] return all pending requests with no time-limit and return confirmed(+declined) friends which have been added [u]within the last week[/u]\n" +
            "[blue]get_friendlist:[/blue] return all confirmed friends which have been added [u]more than one week before[/u]\n" +
            "\n" +
            "The default behavior can be changed with the parameters 'noAccepted', 'onlyReceived' and 'onlySent'", 
            Title = "Get friend requests list", ReturnCodes = "1")]
        [HttpPost]
        public HttpResponseMessage GetPendingFriendRequests(GetFriendRequestsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetFriendRequestsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIFriendListProvider FP = new APIFriendListProvider();
                BaseResponseDTO Result = FP.GetPendingFriendRequests(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/get_friendrequests_pending");
            }
        }

        [Route("api/vma/users/get_points_list")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<PointListDTO>))]
        [SwaggerDescription(Description = "Retrieves a monthly list of points collected by the user\n\n" +
            "[color:brown]Note: the expiry date/time is UTC![/color]", Title = "Get monthly point list",
            ReturnCodes = "1,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage GetPointsList(BaseAPIAuthParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseAPIAuthParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.GetPointsList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/get_points_list");
            }
        }

        [Route("api/vma/users/send_points")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<SentToFriendInfoDTO>))]
        [SwaggerDescription(Description = "Sends points as a gift to another user (send to friend)\n\n" +
            "[u]If the points have been sent successfully then a [brown]push notification[/brown] will be sent to the friend:[/u]\n" +
            "[blue]Title:[/blue] 你的朋友送你積分了\n[blue]Message:[/blue] 你的朋友 Nickname 剛剛送你#點積分。\n[blue]Content:[/blue] CODE:4;VALUE:FriendUserId\n\n" +
            "Code 4 = Points received by friend\n" +
            "'FriendUserId' will be the id of this user who have sent the points.", 
            Title = "Send points to friend",
            ReturnCodes = "1,-300,-10001,-10007,-10008,-10203,-10209[This friend user has been banned]")]
        [HttpPost]
        public HttpResponseMessage SendPoints(SendPointsToFriendParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SendPointsToFriendParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.SendPointsToFriend(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/send_points");
            }
        }

        [Route("api/vma/users/get_session_info")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<Kooco.Framework.Models.DataTransferObjects.APISessionInfoDTO>))]
        [SwaggerDescription(Description = "Retrieves information about the current user's session.\nThe login data (UID, Account), last login time and expiry date will be returned.\n\n" +
            "[color:brown]Note: the login and expiry date/time are UTC![/color]", Title = "Get user session info",
            ReturnCodes = "1,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage GetSessionInfo(BaseAPIAuthParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseAPIAuthParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.GetSessionInfo(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/get_session_info");
            }
        }

        [Route("api/vma/users/test_add_points")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API is ONLY for testing!!\nThis will add the given amount points to the user account for testing purposes.", Title = "TEST - add points to user account",
            ReturnCodes = "1,-10001,-10200")]
        [HttpPost]
        public HttpResponseMessage TestAddPoints(TestAddPointsParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<TestAddPointsParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.TestAddPoints(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/test_add_points");
            }
        }

        [Route("api/vma/users/update_pushtoken")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Sends the current device's push token to the server to register it for sending push notifications.\n" +
            "The push token will expire after one week.\n" +
            "Best would be to send the current push token after every login", Title = "Update push token",
            ReturnCodes = "1,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage UpdatePushToken(UpdatePushTokenParamsDTO<VmaBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
                Params = CheckJsonPostParameter<UpdatePushTokenParamsDTO<VmaBase.Models.DataStorage.ApplicationDbContext>>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.UpdatePushToken(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/update_pushtoken");
            }
        }

        [HttpPost]
        [Route("api/vma/users/send_test_notification")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This API - only for test purposes! - will send a push notification to the given user id",
            Title = "Send test push notification to user",
            ReturnCodes = "1,-1,-7")]
        public HttpResponseMessage SendTestPushNotification(SendTestPushNotificationParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<SendTestPushNotificationParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                if (Params.AppToken != GlobalConst.ApiKey)
                    return Utils.CreateApiHttpResponse(RequestGuid, new BaseResponseDTO(ReturnCodes.InvalidAPIToken));

                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.SendTestPushNotification(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/send_test_notification(POST)");
            }
        }

        [Route("api/vma/users/refresh_token")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Refreshes the expiry date of the current user session.\nThis is meant to be used if the user is not active, but the app is still loaded and active,\nto keep the session alive.\n" + 
            "With this call the expiry date will be set to now + 30 minutes.\n\n[blue]Please note that this is not necessary if you regularly use other API calls with the 'sessionToken' parameter\n" +
            "as every API call with a 'sessionToken' will also automatically refresh the user session to now + 30 minutes.[/blue]", Title = "Refresh user session",
            ReturnCodes = "1,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage RefreshSessionToken(BaseAPIAuthParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseAPIAuthParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.RefreshSessionToken(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/refresh_token");
            }
        }

        public class TestUpdateStatisticsParamsDTO
        {
            public int Days { get; set; }
        }

        [Route("api/vma/users/update_statistics")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Updates all statistics for the given amount of days\nONLY for test purposes or to initialize/repair the statistics data.\nNOT FOR USE WITH THE PHONE APP!\nNOT FOR REGURARELY USE!", Title = "(Test) Update Statistics",
            ReturnCodes = "1,-10007")]
        [HttpPost]
        public HttpResponseMessage TestUpdateStatistics(TestUpdateStatisticsParamsDTO Params)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                BaseResponseDTO Response = null;

                if ((Params == null) || (Params.Days <= 0))
                    Response = new BaseResponseDTO(ReturnCodes.MissingArgument, "The parameter 'Days' is missing or invalid!");

                if (Response == null)
                {
                    StatisticsProvider SP = new StatisticsProvider();
                    SP.UpdateUserStatistics(Params.Days);
                    SP.UpdateLotteryStatistics(Params.Days);
                    SP.UpdateSalesStatistics(Params.Days);
                    SP.UpdateActivityStatistics(Params.Days);

                    Response = new BaseResponseDTO();
                }

                return Utils.CreateApiHttpResponse(RequestGuid, Response);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/update_statistics");
            }
        }

    }
}

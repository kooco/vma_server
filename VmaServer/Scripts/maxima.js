﻿/*
 * Utils - Number handling
 * =======================
 */

function padNum(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

/*
 * Utils - Date/Time Conversion
 * ============================
 */
function jsonDateToDate(jsonDate) {
    var x = [{ "id": 1, "start": jsonDate }, { "id": 2, "start": "\/Date(1238626800000)\/" }];
    var myDate = new Date(x[0].start.match(/\d+/)[0] * 1);

    return myDate;
}

function jsonDateToDateString(jsonDate) {
    var myDate = jsonDateToDate(jsonDate);
    return myDate.getFullYear() + '/' + padNum(myDate.getMonth() + 1, 2) + '/' + padNum(myDate.getDate(), 2);
}

function jsonDateToDateTimeString(jsonDate) {
    var myDate = jsonDateToDate(jsonDate);
    return myDate.getFullYear() + '/' + padNum(myDate.getMonth() + 1, 2) + '/' + padNum(myDate.getDate(), 2) + ' ' + padNum(myDate.getHours(), 2) + ':' + padNum(myDate.getMinutes(), 2);
}

function dateToString(dateValue) {
    if (dateValue instanceof Date)
        return padNum(dateValue.getFullYear(), 4) + '/' + padNum(dateValue.getMonth() + 1, 2) + '/' + padNum(dateValue.getDate(), 2);
    else
        return dateValue;
}

function dateTimeToString(dateValue) {
    if (dateValue instanceof Date)
        return padNum(dateValue.getFullYear(), 4) + '/' + padNum(dateValue.getMonth() + 1, 2) + '/' + padNum(dateValue.getDate(), 2) + ' ' + padNum(dateValue.getHours(), 2) + ':' + padNum(dateValue.getMinutes(), 2);
    else
        return dateValue;
}

/*
 * Utils - String Handling
 * =======================
 */

function replaceString(value, replace_from, replace_to)
{
    return value.replace(replace_from, replace_to);
}

function replaceAll(str, find, replace) {
    if ((typeof str == 'undefined') || (str == null))
        return null;
    else
        return str.toString().replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function maxLenStr(stringValue, maxLength, appendToOverflow) {
    if ((typeof stringValue == 'undefined') || (stringValue == null))
        return '';
    if (stringValue.length > maxLength)
        return stringValue.substring(0, maxLength) + ((typeof appendToOverflow != 'undefined') && (appendToOverflow != null) ? appendToOverflow : '');
    else
        return stringValue;
}



function escapeHtml(html) {
    if ((typeof html == 'undefined') || (html == null))
        return null;
    else
        return html.toString()
             .replace(/&/g, "&amp;")
             .replace(/</g, "&lt;")
             .replace(/>/g, "&gt;")
             .replace(/"/g, "&quot;")
             .replace(/'/g, "&#039;");
}

/*
 * Utils - Cookies
 * ===============
 */

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function savePageSetting(cname, cvalue)
{
    var pageName = replaceAll(window.location.pathname, '/', '_');
    if (pageName.startsWith('_'))
        pageName = pageName.substring(1);

    setCookie('AppSetting' + pageName + '_' + cname, encodeURI(cvalue), 3600);
}

function getPageSetting(cname)
{
    var pageName = replaceAll(window.location.pathname, '/', '_');
    if (pageName.startsWith('_'))
        pageName = pageName.substring(1);

    return decodeURI(getCookie('AppSetting' + pageName + '_' + cname));
}

/*
 * Utils - Urls
 * ============
 */

function addServerUrl(url) {
    if (url.startsWith('/'))
        return ApplicationApiServerUrl + url.substring(1);
    else
        return ApplicationApiServerUrl + url;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/*
 * Utils - misc
 * ============
 */

function TaiwanChineseUsed()
{
    switch(ApplicationLanguage.toLowerCase().trim())
    {
        case 'zh-tw':
        case 'tw':
        case 'chinesetw':
            return true;
    }

    return false;
}

/*
 * Utils - DataTableWrapper
 * ========================
 */

function DataTableWrapper(TableElementSelector, data, TableProperties)
{
    var Properties = {
        data: data,
        language: {
            "processing": "處理中...",
            "loadingRecords": "載入中...",
            "lengthMenu": "顯示 _MENU_ 項結果",
            "zeroRecords": "沒有符合的結果",
            "info": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
            "infoEmpty": "顯示第 0 至 0 項結果，共 0 項",
            "infoFiltered": "(從 _MAX_ 項結果中過濾)",
            "infoPostFix": "",
            "search": "搜尋:",
            "paginate": {
                "first": "第一頁",
                "previous": "«",
                "next": "»",
                "last": "最後一頁"
            },
            "aria": {
                "sortAscending": ": 升冪排列",
                "sortDescending": ": 降冪排列"
            }
        },
        deferRender: false,
        scroller: false,
        scrollCollapse: true,
        filter: true,
        ordering: true,
        colReorder: true,
        paginate: false
    };
    var DataTable = null;
    var OnDrawCallback = null;
    var OnSortChangeCallback = null;
    var DoDraw = true;
    var HasColumns = false;
    var DestroyBefore = true;

    if ((typeof TableProperties == 'undefined') || (TableProperties == null))
        TableProperties = {};

    // copy properties to data table
    for (var property in TableProperties)
    {
        if (TableProperties.hasOwnProperty(property))
        {
            if (property == 'ondraw')
                OnDrawCallback = TableProperties[property];
            else if (property == 'onsortchange')
                OnSortChangeCallback = TableProperties[property];
            else if (property == 'drawnow')
                DoDraw = TableProperties[property];
            else if (property == 'destroy')
                DestroyBefore = TableProperties[property];
            else if (property == 'columns') {
                HasColumns = true;
                Properties[property] = TableProperties[property];
            }
            else
                Properties[property] = TableProperties[property];
        }
    }

    var predefinedOrder = $(TableElementSelector).data('maxima-dt-sortorder');
    if ((typeof predefinedOrder != 'undefined') && (predefinedOrder != null) && (predefinedOrder != ''))
    {
        Properties['order'] = predefinedOrder;
        //$(TableElementSelector).data('maxima-dt-sortorder', null);   // marked out, otherwise the sortorder get's lost when the page is changed
    }

    // if no columns given -> determine columns automatically
    if (!HasColumns)
    {
        Columns = [];

        if (data.length > 0)
        {
            for(var property in data[0])
            {
                if (data[0].hasOwnProperty(property))
                    Columns.push({ data: property });
            }
        }

        Properties.columns = Columns;
    }

    if (DestroyBefore)
    {
        DataTable = $(TableElementSelector).DataTable();
        if ((typeof DataTable != 'undefined') && (DataTable != null))
            DataTable.destroy();
        DataTable = null;
    }

    $(TableElementSelector).data('just-init', true);    
    DataTable = $(TableElementSelector).DataTable(Properties);

    if (OnDrawCallback != null)
        DataTable.on('draw.dt', OnDrawCallback);

    DataTable.on('order.dt', function (e, settings) {
        var newOrder = DataTable.order();

        if (newOrder.length > 0) {
            if ($(TableElementSelector).data('just-init') != true) {

                var newOrderString = "";
                for (var iNr = 0; iNr < newOrder.length; iNr++) {
                    if (newOrderString != "")
                        newOrderString += ",";
                    newOrderString += DataTable.column(newOrder[0][0]).dataSrc();
                    newOrderString += " " + newOrder[0][1];
                }

                $(TableElementSelector).data('maxima-dt-sortorder', newOrder);

                if (OnSortChangeCallback != null)
                    OnSortChangeCallback(DataTable, newOrderString);
            }
            else
                $(TableElementSelector).data('just-init', false);
        }
    });

    if (DoDraw)
        DataTable.draw();

    return DataTable;
}

/*
 * Error Handling
 * ==============
 */

function showError(errorMessage)
{
    GlobalMessagePopup.title('<span class="errordialog_title">An error occured!</span>');
    GlobalMessagePopup.content('<span class="errordialog_text">' + errorMessage + '</span>');
    GlobalMessagePopup.open();
}

function showMessage(message)
{
    GlobalMessagePopup.title('<span class="infodialog_title">Information</span>');
    GlobalMessagePopup.content('<span class="infodialog_text">' + message + '</span>');
    GlobalMessagePopup.open();
}

/*
 * inputKeyDownDefaultButton
 * -------------------------
 * for "onKeyDown" events of text input controls: 
 * sets the 'buttonId' as default button for this input control.
 */
function inputKeyDownDefaultButton(ev, buttonId)
{
    if (ev.keyCode == 13)
    {
        $('#' + buttonId).click();
    }
}

/*
 * loginUser
 * ---------
 * 
 * sends a login request to the server with the given username and password
 * and handles the result in proper way.
 */
function loginUser(username, password)
{
    var url = ApplicationVirtualDirectory + "Users/AjaxUserLogin";

    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        data: {
            Username: username,
            Password: password
        },
        success: function (data) {
            if (data.ReturnCode == 1) {
                return;
            }
            else if (data.ReturnCode == 2)
            {
                if (data == 'current')
                    location.href = location.href;
                else
                    location.href = location.protocol + '//' + location.host + ApplicationVirtualDirectory + (data.Data.startsWith("/") ? data.Data.substring(1) : data.Data);
            }
            else
            {
                if (data.ReturnCode == 0)
                    showError('An unexpected error occured while logging in!');
                else
                    showError(data.ErrorMessage);
            }
        },
        error: function (data) {
            showError('An unexpected error occurred! - ' + data.status + ' ' + data.statusText);
        }
    });
}

function refreshLoginTicket()
{
    var url = ApplicationVirtualDirectory + "Users/AjaxRefreshAuthTicket";

    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            if (data.ReturnCode == 1) {
                return;
            }
            else if (data.ReturnCode == 2) {
                if (data == 'current')
                    location.href = location.href;
                else
                    location.href = location.protocol + '//' + location.host + ApplicationVirtualDirectory + (data.Data.startsWith("/") ? data.Data.substring(1) : data.Data);
            }
            else {
                if (data.ReturnCode == 0)
                    showError('An unexpected error occured while refreshing the session!');
                else
                    showError(data.ErrorMessage);
            }
        },
        error: function (data) {
            showError('An unexpected error occurred! - ' + data.status + ' ' + data.statusText);
        }
    });
}

/*
 * Logs the user out, so that he is not authenticated anymore.
 * This function will also redirect to the login or home page (or current page) (depending to what is set up in the web.config)
 */
function logout()
{
    var url = ApplicationVirtualDirectory + "Users/AjaxUserLogout";

    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        success: function (data) {
            if (data.ReturnCode == 1) {
                return;
            }
            else if (data.ReturnCode == 2) {
                if (data == 'current')
                    location.href = location.href;
                else
                    location.href = location.protocol + '//' + location.host + ApplicationVirtualDirectory + (data.Data.startsWith("/") ? data.Data.substring(1) : data.Data);
            }
            else {
                if (data.ReturnCode == 0)
                    showError('An unexpected error occured while logging out!');
                else
                    showError(data.ErrorMessage);
            }
        },
        error: function (data) {
            showError('An unexpected error occurred! - ' + data.status + ' ' + data.statusText);
        }
    });
}



/*
 * Titlebar Clock
 * --------------
 */

function clockSetTime(controlId)
{
    var control = $('#' + controlId);
    var currentTime = new Date();
    var timeString = '';

    timeString = currentTime.getFullYear() + '年 ' +
                 currentTime.getMonth() + '月 ' +
                 currentTime.getDate() + '日 ';

    switch(currentTime.getDay())
    {
        case 0:
            timeString = timeString + '週一';
            break;
        case 1:
            timeString = timeString + '週二';
            break;
        case 2:
            timeString = timeString + '週三';
            break;
        case 3:
            timeString = timeString + '週四';
            break;
        case 4:
            timeString = timeString + '週五';
            break;
        case 5:
            timeString = timeString + '週六';
            break;
        case 6:
            timeString = timeString + '週天';
            break;
    }

    timeString = timeString + currentTime.getHours() + ':' + currentTime.getMinutes() + ':' + currentTime.getSeconds();

    control.text(timeString);
}

/*
 * Sidebar (Old Design)
 * ====================
 */

function toggleSidebar()
{
    var sidebarCol = $('.maxima-sidebar-col');
    var contentsCol = $('.maxima-contents-col');

    if ((typeof sidebarCol.data('collapsed') == 'undefined') || (sidebarCol.data('collapsed') == null) || (sidebarCol.data('collapsed') == false)) {        
        sidebarCol.removeClass('col-md-2');
        sidebarCol.css('display', 'none');
        contentsCol.removeClass('col-md-10');
        contentsCol.addClass('col-md-12');

        sidebarCol.data('collapsed', true);
    }
    else
    {
        contentsCol.removeClass('col-md-12');
        contentsCol.addClass('col-md-10');
        sidebarCol.addClass('col-md-2');
        sidebarCol.css('display', 'block');

        sidebarCol.data('collapsed', false);
    }
}

/*
 * Masonry Grid Handling
 * =====================
 */

function emptyMasonryGrid() {
    $('.o-grid__cell').remove();
}

function getMasonryGrid() {
    return $('.c-Masonry');
}

function adjustLayout() {
    if (typeof $('.c-Masonry').data('masonry') != 'undefined') {
        $('.c-Masonry').masonry('reloadItems');

        var result = $('.c-Masonry').masonry({
            itemSelector: '.o-grid__cell',
            columnWidth: '.c-Masonry__sizer',
            percentPosition: true
        });

        $('.c-Masonry').masonry('layout');
    }
}

var LoadingImagesLoadedWaiterActive = false;

function adjustLayoutAfterImagesLoaded(imageSelector, maxSeconds, finishedCallback) {
    var imgs = $(imageSelector).not(function () { return this.complete; });
    var count = imgs.length;
    var maxWaitMs = ((typeof maxSeconds == 'undefined') || (maxSeconds == null) || (maxSeconds < 3)) ? 15000 : (maxSeconds * 1000);

    LoadingImagesLoadedWaiterActive = true;
    // set a timeout of 10 seconds - for the case there is a problem with loading the images - after 12 seconds we will activate the layout anyway
    setTimeout(function (e) {
        adjustLayout();
        hideLoader();

        // wait further 15 seconds until we deactivate the re-arrangement completely
        setTimeout(function (e) {
            LoadingImagesLoadedWaiterActive = false;
        }, maxWaitMs);
    }, maxWaitMs);

    // wait for all images loaded
    if (count) {
        imgs.load(function () {
            count--;
            if (!count) {
                if (LoadingImagesLoadedWaiterActive) {
                    // rearrange widgets when all images are loaded                        
                    adjustLayout();
                    hideLoader();

                    if (finishedCallback != null)
                        finishedCallback(imageSelector);
                }
            }
        });
    }
    else {
        adjustLayout();
        hideLoader();
    }
}

function AddMasonryInlet(templateElementId, insertBefore, id, title) {
    var templateHtml = $('#' + templateElementId).html();
    var pageContentsDiv = getMasonryGrid();

    var iPos = 0;
    var phdKey = '';
    var phdValue = '';

    templateHtml = replaceAll(templateHtml, '##gridcellclass##', 'widget_dyngridcell');
    templateHtml = replaceAll(templateHtml, '##id##', id);
    templateHtml = replaceAll(templateHtml, '##title##', title);

    for (var iArg = 4; iArg < arguments.length; iArg++) {
        iPos = arguments[iArg].indexOf(':');

        if (iPos > 0) {
            phdKey = arguments[iArg].substring(0, iPos);
            phdValue = arguments[iArg].substring(iPos + 1).trim();

            templateHtml = replaceAll(templateHtml, '##' + phdKey + '##', phdValue);
        }
    }

    if ((typeof insertBefore != 'undefined') && (insertBefore != null) && (insertBefore != '')) {
        //$('#' + insertBefore).css('border', '1px solid red;');
        $('#' + insertBefore).before(templateHtml);
    }
    else
        pageContentsDiv.append(templateHtml);
}

function AddMasonryInletWithHtml(templateHtml, insertBefore, id, title) {
    var pageContentsDiv = getMasonryGrid();

    var iPos = 0;
    var phdKey = '';
    var phdValue = '';

    templateHtml = replaceAll(templateHtml, '##gridcellclass##', 'widget_dyngridcell');
    templateHtml = replaceAll(templateHtml, '##id##', id);
    templateHtml = replaceAll(templateHtml, '##title##', title);

    for (var iArg = 4; iArg < arguments.length; iArg++) {
        iPos = arguments[iArg].indexOf(':');

        if (iPos > 0) {
            phdKey = arguments[iArg].substring(0, iPos);
            phdValue = arguments[iArg].substring(iPos + 1).trim();

            templateHtml = replaceAll(templateHtml, '##' + phdKey + '##', phdValue);
        }
    }

    if ((typeof insertBefore != 'undefined') && (insertBefore != null) && (insertBefore != '')) {
        //$('#' + insertBefore).css('border', '1px solid red;');
        $('#' + insertBefore).before(templateHtml);
    }
    else
        pageContentsDiv.append(templateHtml);
}

/*
 * jQuery Loading Animation
 * ========================
 */

var MaximaShowLoaderTimeout = null;

function showLoader(loadingText, elementSelector) {
    var selector = ((typeof elementSelector == 'undefined') || (elementSelector == null) || (elementSelector == '')) ? '#pagebody' : elementSelector;
    $(selector).loadingOverlay();

    if ((typeof loadingText != 'undefined') && (loadingText != null) && (loadingText != ''))
        $('.loading-text').text(loadingText);
    else
        $('.loading-text').text('loading');

    if (MaximaShowLoaderTimeout != null)
        MaximaShowLoaderTimeout.stop();

    window.setTimeout(function (e) {
        hideLoader(elementSelector);
        MaximaShowLoaderTimeout = null;
    }, 30000);
}

function hideLoader(elementSelector) {
    var selector = ((typeof elementSelector == 'undefined') || (elementSelector == null) || (elementSelector == '')) ? '#pagebody' : elementSelector;
    $('#pagebody').loadingOverlay('remove');

    if (MaximaShowLoaderTimeout != null)
        MaximaShowLoaderTimeout.stop();
}

/*
 * Helper functions for BaseResponseDTO
 * ------------------------------------
 */

function getResult_ReturnValue(resultObject)
{
    if (typeof resultObject.returnValue != 'undefined')
        return resultObject.returnValue;
    else if (typeof resultObject.ReturnValue != 'undefined')
        return resultObject.ReturnValue;
    else if (typeof resultObject.returnCode != 'undefined')
        return resultObject.returnCode;
    else if (typeof resultObject.ReturnCode != 'undefined')
        return resultObject.ReturnCode;
    else if (typeof resultObject.retValue != 'undefined')
        return resultObject.retValue;
    else if (typeof resultObject.RetValue != 'undefined')
        return resultObject.RetValue;
    else if (typeof resultObject.retCode != 'undefined')
        return resultObject.retCode;
    else if (typeof resultObject.RetCode != 'undefined')
        return resultObject.RetCode;
    else
        return -9999;
}

function getResult_ErrorMessage(resultObject)
{
    if (typeof resultObject.errorMessage != 'undefined')
        return resultObject.errorMessage;
    else if (typeof resultObject.ErrorMessage != 'undefined')
        return resultObject.ErrorMessage;
    else if (typeof resultObject.errorMsg != 'undefined')
        return resultObject.errorMsg;
    else if (typeof resultObject.ErrorMsg != 'undefined')
        return resultObject.ErrorMsg;
    else if (typeof resultObject.error != 'undefined')
        return resultObject.error;
    else if (typeof resultObject.Error != 'undefined')
        return resultObject.Error;
    else
        return 'internal result processing error! (MaximaWeb) - could not find a error message property!';
}

function getResult_data(resultObject)
{
    if (typeof resultObject.data != 'undefined')
        return resultObject.data;
    else if (typeof resultObject.Data != 'undefined')
        return resultObject.Data;
}

function getResult_totalPages(resultObject)
{
    if (typeof resultObject.totalPages != 'undefined')
        return resultObject.totalPages;
    else if (typeof resultObject.TotalPages != 'undefined')
        return resultObject.TotalPages;
    else
        return 'internal result processing error! (MaximaWeb) - could not find a totalPages property!';
}

/*
 * AJAX GET - getRemoteData
 * ========================
 */

function getRemoteData(url, name, successCallback, cancelledCallback) {
    refreshLoginTicket();
    kendo.ui.progress($(document.body), true);

    var authcookiekey = "jh348dkfhue_dZow837AEbx";
    var authcookievalue = getCookie(authcookiekey);
    var authcookiepair = authcookiekey + ":" + authcookievalue;

    var reqHeaders = {};
    reqHeaders[AuthHeaderKey] = authcookiepair;

    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        headers: reqHeaders,
        success: function (data) {
            var returnValue = getResult_ReturnValue(data);
            var message = getResult_ErrorMessage(data);

            if (returnValue == 1) {
                successCallback(getResult_data(data), data);
            }
            else {
                console.log('error message returned by "' + url + '": code ' + returnValue + ', message: ' + message);
                hideLoader();

                if (returnValue == 0) {
                    alert('unable to read remote data for "' + name + '"! An unknown error occured!');
                }
                else
                    alert('unable to read remote data for "' + name + '"! Error: ' + returnValue + ' - ' + message);

                if (cancelledCallback != null)
                    cancelledCallback();
            }
        },
        error: function (data) {
            hideLoader();

            console.log('remote service "' + url + '" returned an error: ' + data.statusText);
            alert('unable to read remote data for "' + name + '"! The remote service returned an error: ' + data.statusText);

            if (cancelledCallback != null)
                cancelledCallback();
        }
    });
}

/*
 * AJAX Upload File - uploadFile
 * =============================
 */

function uploadFile(FileElementId, url, progressCallback, finishedCallback, cancelledCallback, suppressAlerts) {
    var myFile = new FormData();
    myFile.append("file", $('#' + FileElementId)[0].files[0]);

    showLoader('uploading');

    var authcookiekey = "jh348dkfhue_dZow837AEbx";
    var authcookievalue = getCookie(authcookiekey);
    var authcookiepair = authcookiekey + ":" + authcookievalue;

    var uplHeaders = {};
    uplHeaders[AuthHeaderKey] = authcookiepair;

    $.ajax({
        async: true,

        url: url,
        type: 'POST',

        headers: uplHeaders,
        data: myFile,

        cache: false,
        contentType: false,
        processData: false,

        xhr: function () {
            var xhrreq = $.ajaxSettings.xhr();
            if (xhrreq.upload) {
                xhrreq.upload.addEventListener('progress', function (e) {
                    if ((typeof progressCallback != 'undefined') && (progressCallback != null))
                        progressCallback(e);
                }, false);
                xhrreq.upload.addEventListener('error', function (e) {
                    hideLoader();
                    if ((typeof suppressAlerts == 'undefined') || (suppressAlerts == null) || (suppressAlerts != true))
                        alert('Sorry! The upload failed due to an error!');
                    if (cancelledCallback != null)
                        cancelledCallback('Sorry! Upload failed due to an error!', false);
                }, false);
                xhrreq.upload.addEventListener('abort', function (e) {
                    hideLoader();
                    console.log('upload cancelled by the user!');
                    if (cancelledCallback != null)
                        cancelledCallback('Upload cancelled', true);
                }, false);
            }

            return xhrreq;
        },

        success: function (data) {
            var ReturnValue = getResult_ReturnValue(data);
            var message = getResult_ErrorMessage(data);
            var dataObject = getResult_data(data);

            hideLoader();

            if (ReturnValue == 1) {
                finishedCallback(dataObject, data);
            }
            else {
                console.log('error message returned by the uploader "' + url + '": code ' + ReturnValue + ', message: ' + message);

                if ((typeof suppressAlerts == 'undefined') || (suppressAlerts == null) || (suppressAlerts != true)) {
                    if (ReturnValue == 0) {
                        alert('unable to upload file for "' + name + '"! An unknown error occured!');
                    }
                    else
                        alert('unable to upload file for "' + name + '"! Error: ' + ReturnValue + ' - ' + message);
                }

                if (cancelledCallback != null)
                    cancelledCallback(message);
            }            
        },

        error: function (e) {
            hideLoader();
            if ((typeof suppressAlerts == 'undefined') || (suppressAlerts == null) || (suppressAlerts != true))
                console.log('an error occured when trying to upload a file: ' + e.statusText);
            if (cancelledCallback != null)
                cancelledCallback('An error occured when trying to upload the file: ' + e.statusText);
        }
    });
}

/*
 * AJAX POST - postData
 * ====================
 */

function postData(url, name, data, successCallback, cancelledCallback, suppressAlerts) {
    refreshLoginTicket();
    kendo.ui.progress($(document.body), true);

    var authcookiekey = "jh348dkfhue_dZow837AEbx";
    var authcookievalue = getCookie(authcookiekey);
    var authcookiepair = authcookiekey + ":" + authcookievalue;

    var postHeaders = {};
    postHeaders[AuthHeaderKey] = authcookiepair;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        headers: postHeaders,
        success: function (data) {
            var ReturnValue = getResult_ReturnValue(data);
            var message = getResult_ErrorMessage(data);
            var dataObject = getResult_data(data);

            if (ReturnValue == 1) {
                successCallback(dataObject, data);
            }
            else {
                hideLoader();
                console.log('error message returned by "' + url + '": code ' + ReturnValue + ', message: ' + message);

                if ((typeof suppressAlerts == 'undefined') || (suppressAlerts == null) || (suppressAlerts != true)) {
                    if (ReturnValue == 0) {
                        alert('unable to post data for "' + name + '"! An unknown error occured!');
                    }
                    else
                        alert('unable to post data for "' + name + '"! Error: ' + ReturnValue + ' - ' + message);
                }

                if (cancelledCallback != null)
                    cancelledCallback(message, ReturnValue);
            }
        },
        error: function (data) {
            hideLoader();

            console.log('remote service "' + url + '" returned an error: ' + data.statusText);
            if ((typeof suppressAlerts == 'undefined') || (suppressAlerts == null) || (suppressAlerts != true))
                alert('unable to post data for "' + name + '"! The remote service returned an error: ' + data.statusText);

            if (cancelledCallback != null)
                cancelledCallback('Unable to post data! The remote service returned an error: ' + data.statusText, 0);
        }
    });
}

function equalWidth(eleSelector) {
    var iMaxWidth = 0;

    $(eleSelector).each(function (index) {
        if ($(this).width() > iMaxWidth)
            iMaxWidth = $(this).width();
    });

    $(eleSelector).width(iMaxWidth);
}

/*   
 * Breadcrumb - Context Buttons
 * ============================
  */

function clearContextButtons() {
    $('.contextbuttons-container').empty();
}

function addContextButton(id, icon, link, tooltip) {
    var container = $('.contextbuttons-container');
    container.append("<div class='contextbutton'>\n  <a id='" + id + "' class='c-icon c-icon--main u-color-major u-opacity-50p' " + (typeof tooltip != 'undefined' && tooltip != null && tooltip != "" ? "title='" + tooltip + "'" : "") + " href='" + link + "'>" + icon + "</a>\n</div>\n");
}

function addContextSeparator(id) {
    var container = $('.contextbuttons-container');
    container.append("<div class='contextbutton' id='" + id + "'></div>");
}

function disableContextButton(id)
{
    var container = $('.contextbuttons-container');
    var buttonLink = container.find('#' + id);
    var buttonDiv = buttonLink.parent();

    if ((typeof buttonDiv.data('disabled') == 'undefined') || (buttonDiv.data('disabled') == null) || (buttonDiv.data('disabled') == false)) {

        buttonDiv.removeClass('disabled');
        buttonDiv.addClass('disabled');
        buttonDiv.data('disabled', true);

        buttonLink.attr('data-oldhref', buttonLink.attr('href'));
        buttonLink.attr('href', 'javascript:void(0);');
        buttonLink.removeClass('u-color-major');
        buttonLink.addClass('u-color-muted');
        buttonLink.removeClass('u-opacity-50p');
        buttonLink.addClass('u-opacity-100p');
    }
}

function enableContextButton(id)
{
    var container = $('.contextbuttons-container');
    var buttonLink = container.find('#' + id);
    var buttonDiv = buttonLink.parent();

    if ((typeof buttonDiv.data('disabled') != 'undefined') && (buttonDiv.data('disabled') != null) && (buttonDiv.data('disabled') == true)) {
        buttonDiv.removeClass('disabled');
        buttonDiv.data('disabled', false);

        buttonLink.attr('href', buttonLink.attr('data-oldhref'));
        buttonLink.removeClass('u-color-muted');
        buttonLink.addClass('u-color-major');
        buttonLink.removeClass('u-opacity-100p');
        buttonLink.addClass('u-opacity-50p');
    }
}

/*
 * Paging for the Masonry design
 * =============================
 */

var MaximaPagingCallback = null;
var MaximaCurrentPage = 0;
var MaximaTotalPages = 0;
var MaximaLastPageInput = -1;

function _max_toFirstPage() {
    if (MaximaPagingCallback != null)
        MaximaPagingCallback(1);
    else
        toFirstPage();
}

function _max_toPreviousPage() {
    if (MaximaCurrentPage <= 1)
        alert('你已經在第一頁！');
    else {
        if (MaximaPagingCallback != null)
            MaximaPagingCallback(MaximaCurrentPage - 1);
        else
            toPreviousPage();
    }
}

function _max_toNextPage() {
    if (MaximaCurrentPage >= MaximaTotalPages)
        alert('你已經在最後一頁！');
    else {
        if (MaximaPagingCallback != null)
            MaximaPagingCallback(MaximaCurrentPage + 1);
        else
            toNextPage();
    }
}

function _max_toLastPage() {
    if (MaximaPagingCallback != null)
        MaximaPagingCallback(MaximaTotalPages);
    else
        toLastPage();
}

function _max_pagenrEnter(event) {
    if (event.keyCode == 13)
        _max_pagenrTakeInput();
    if (event.keyCode == 27)
        _max_pagenrCancelInput(MaximaCurrentPage);
}

function _max_pagenrCancelInput(newPageNr) {
    $('#_max_pagenr_inp').remove();
    $('#_max_pagenr').append(newPageNr);
    $('#_max_pagenr').attr('onclick', '_max_pagenrEdit()');
}

function _max_pagenrTakeInput() {
    var inputValue = $('#_max_pagenr_inp').val();

    if (MaximaLastPageInput != inputValue) {
        if (isNaN(inputValue))
            alert('請輸入頁碼!');
        else if ((inputValue < 1) || (inputValue > MaximaTotalPages))
            alert('請輸入頁碼範圍1到' + MaximaTotalPages + '!');
        else {
            _max_pagenrCancelInput(Number(inputValue));

            if (MaximaPagingCallback)
                MaximaPagingCallback(Number(inputValue));
            else
                toPage(Number(inputValue));
        }

        MaximaLastPageInput = Number(inputValue);
        window.setTimeout(function () { MaximaLastPageInput = -1; }, 1000);
    }
}

function _max_pagenrEdit() {
    MaximaLastPageInput = -1;

    $('#_max_pagenr').empty();
    $('#_max_pagenr').append("<input id='_max_pagenr_inp' class='pagenredit' type='text' value='" + MaximaCurrentPage + "' onkeydown='_max_pagenrEnter(event)' onchange='_max_pagenrTakeInput()' />");
    $('#_max_pagenr').attr('onclick', '');
    $('#_max_pagenr_inp').select();
}

function ContextPagingPanel(pageNr, pagesTotal, pagingCallback) {
    var container = $('.contextbuttons-container');

    MaximaCurrentPage = Number(pageNr);
    MaximaTotalPages = pagesTotal;
    MaximaPagingCallback = pagingCallback;

    if ($('#_max_ctxPagingSep').length <= 0) {
        addContextSeparator('_max_ctxPagingSep');
        addContextButton('_max_ctxLastPage', 'last_page', 'javascript:_max_toLastPage()', '最後一頁');
        addContextButton('_max_ctxNextPage', 'chevron_right', 'javascript:_max_toNextPage()', '下一頁');
        addContextButton('_max_ctxPreviousPage', 'chevron_left', 'javascript:_max_toPreviousPage()', '上一頁');
        addContextButton('_max_ctxFirstPage', 'first_page', 'javascript:_max_toFirstPage()', '第一頁');
    }

    if (MaximaCurrentPage == 1) {
        disableContextButton('_max_ctxFirstPage');
        disableContextButton('_max_ctxPreviousPage');
    }
    else
    {
        enableContextButton('_max_ctxFirstPage');
        enableContextButton('_max_ctxPreviousPage');
    }

    if (MaximaCurrentPage == MaximaTotalPages) {
        disableContextButton('_max_ctxLastPage');
        disableContextButton('_max_ctxNextPage');
    }
    else
    {
        enableContextButton('_max_ctxLastPage');
        enableContextButton('_max_ctxNextPage');
    }

    $('#_max_pageinfo').remove();
    container.append("<div id='_max_pageinfo' class='context_pageinfo'><span><div class='pagecount'> / " + pagesTotal + "</div><div id='_max_pagenr' class='pagenr' onclick='_max_pagenrEdit()'>" + pageNr + "</div></span></div>");

    MaximaLastPageInput = -1;
}

/*
 * Popup Input Validation
 * ======================
 */

/* Popup Input Validation - Required Field (General) */
function checkRequiredPopupField(ValidationOk, ElementId, FieldName) {
    if (ValidationOk) {
        var element = $('#' + ElementId);
        var inputValue;

        if (element.data('kendoDropDownList') != null)
            inputValue = element.data('kendoDropDownList').value();
        else
            inputValue = element.val();

        if ((inputValue == null) || (inputValue.trim() == "")) {
            element.focus();
            alert('The field "' + FieldName + '" can not be empty!');
            return false;
        }
        else
            return ValidationOk;
    }
}
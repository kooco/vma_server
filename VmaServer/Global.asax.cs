﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

using AutoMapper;

using Kooco.Framework.Models.DataStorage;

using VmaBase.Shared;


// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace VmaServer
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SqlProviderServices.SqlServerTypesAssemblyName = "Microsoft.SqlServer.Types, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91";

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<VmaBase.Models.DataStorage.ApplicationDbContext, VmaBase.DataMigrations.Configuration>());
            VmaBase.Models.DataStorage.ApplicationDbContext DbContext = new VmaBase.Models.DataStorage.ApplicationDbContext();            
            DbContext.Database.Initialize(false);

            Kooco.Framework.Shared.FrameworkManager.Initialize(GlobalConst.ApplicationName, GlobalConst.ApplicationVersion, GetMappingProfile(), Kooco.Framework.Shared.FrameworkLanguages.ChineseTW, "/", GlobalConst.ApiKey);
        }

        public static Profile GetMappingProfile()
        {
            return new MappingProfile();
        }
    }
}

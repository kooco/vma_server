﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models
{
    public class LotteryGameInfo
    {
        public long Id { get; set; }
        public LotteryTestMode TestMode { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime? TestStartTime { get; set; }
        public LotteryGameStatus Status { get; set; }
    }
}

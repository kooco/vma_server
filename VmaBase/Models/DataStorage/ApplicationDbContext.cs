﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Models.Interfaces;

using VmaBase.Shared;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects.MOCCP;
// ##section[NEWSMODULE_NAMESPACES] begin - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
using VmaBase.Models.DataStorage.Resultsets;
using VmaBase.Models.DataTransferObjects;
// ##section[NEWSMODULE_NAMESPACES] end - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)


namespace VmaBase.Models.DataStorage
{
    public class ApplicationDbContext : Kooco.Framework.Models.DataStorage.ApplicationDbContext, IMaximaBasedDbContext
    {
        // Table Classes here:
        public DbSet<UserData> UserDatas { get; set; }
        public DbSet<UserFriend> UserFriends { get; set; }
        public DbSet<VendingMachine> VendingMachines { get; set; }
        public DbSet<VendingProduct> VendingProducts { get; set; }
        public DbSet<VendingProductType> VendingProductTypes { get; set; }
        public DbSet<VendingProductBrand> VendingProductBrands { get; set; }
        public DbSet<VendingProductActivitySetting> VendingProductActivitySettings { get; set; }
        public DbSet<StampType> StampTypes { get; set; }
        public DbSet<SalesList> SalesLists { get; set; }
        public DbSet<SalesTransaction> SalesTransactions { get; set; }
        public DbSet<VendingMachineMessage> VendingMachineMessages { get; set; }
        public DbSet<PointsTransaction> PointsTransactions { get; set; }
        public DbSet<UserPoint> UserPoints { get; set; }
        public DbSet<PromotionProduct> PromotionProducts { get; set; }
        public DbSet<PromotionOrder> PromotionOrders { get; set; }
        public DbSet<JPushMessageLog> JPushMessageLogs { get; set; }
        public DbSet<UserLotteryCoin> UserLotteryCoins { get; set; }
        public DbSet<SingleUseQRToken> SingleUseQRTokens { get; set; }

        public DbSet<CouponActivity> CouponActivities { get; set; }
        public DbSet<CouponStampDefinition> CouponStampDefinitions { get; set; }
        public DbSet<ActivityParticipatingProduct> ActivityParticipatingProducts { get; set; }
        public DbSet<ActivityParticipatingMachine> ActivityParticipatingMachines { get; set; }
        public DbSet<UserCoupon> UserCoupons { get; set; }
        public DbSet<UserCouponStamp> UserCouponStamps { get; set; }
        public DbSet<UserStampLog> UserStampLogs { get; set; }

        public DbSet<LotteryGame> LotteryGames { get; set; }
        public DbSet<LotteryPrice> LotteryPrices { get; set; }
        public DbSet<LotteryPriceRedeemProduct> LotteryPriceRedeemProducts { get; set; }
        public DbSet<LotteryPriceCoupon> LotteryPriceCoupons { get; set; }
        public DbSet<LotteryPriceSpreadingParameter> LotteryPriceSpreadingParameters { get; set; }
        public DbSet<LotteryDrawLog> LotteryDrawLogs { get; set; }
        public DbSet<LotteryGameUseStatistic> LotteryGameUseStatistics { get; set; }
        public DbSet<LotteryCoinLog> LotteryCoinLogs { get; set; }
        public DbSet<LotteryPriceAvailabilityItem> LotteryPriceAvailabilityItems { get; set; }        

        public DbSet<ImportLog> ImportLogs { get; set; }
        public DbSet<ImportLogMessage> ImportLogMessages { get; set; }

        public DbSet<AutoCouponModule> AutoCouponModules { get; set; }
        public DbSet<AutoCouponCriteria> AutoCouponCriterias { get; set; }
        public DbSet<AutoCouponCriteriaProduct> AutoCouponCriteriaProducts { get; set; }
        public DbSet<AutoCouponCriteriaMachine> AutoCouponCriteriaMachines { get; set; }
        public DbSet<AutoCouponUserState> AutoCouponUserStates { get; set; }
        public DbSet<AutoCouponAssignment> AutoCouponAssignments { get; set; }

		// ##section[NEWSMODULE_TABLES] begin - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
		public DbSet<NewsCategory> NewsCategories { get; set; }
		public DbSet<NewsTag> NewsTags { get; set; }
		public DbSet<NewsArticle> NewsArticles { get; set; }
		public DbSet<NewsMessage> NewsMessages { get; set; }
		public DbSet<NewsTicker> NewsTickers { get; set; }
		public DbSet<UserLike> UserLikes { get; set; }
		public DbSet<NewsSubscription> NewsSubscriptions { get; set; }
		// ##section[NEWSMODULE_TABLES] end - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
        private _LambdaViews LambdaViewsHelper;

        // Lambda Views here:
        #region UserView
        /// <summary>
        ///     Example Lambda View for Users
        /// </summary>
        public IQueryable<LambdaViewModels.UserView> UserView
        {
            get
            {
                return LambdaViewsHelper.UserView();
            }
        }
        #endregion

        // Stored Procedures here:
		// ##section[NEWSMODULE_STOREDPROCEDURES] begin - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
		#region SP_GetNewsList
		public List<GetNewsListResult> SP_GetNewsList(string CategoryCode, int[] TagIds, bool IncludeNewsInNewsTickerList, bool IncludeContent, bool OnlySubscribed, bool OnlyAvailable, long? UID, int PageNr, int PageSize, string Categories = null, DateTime? DateFrom = null, DateTime? DateTo = null)
		{
			string sSQL = "[dbo].[sp_GetNewsList] @CategoryCode, @Categories, @TagIds, @TagNames, @IncludeTickerNews, @IncludeContent, @DateFrom, @DateTo, @PageNr, @PageSize, @OnlySubscribed, @OnlyAvailable, @UID";
			string sTags = string.Empty;
				
			if ((TagIds != null) && (TagIds.Length > 0))
				sTags = String.Join(",", TagIds);
				
			return Database.SqlQuery<GetNewsListResult>(sSQL, new SqlParameter("@CategoryCode", Utils.NullToDbNull(CategoryCode)),
																new SqlParameter("@Categories", Utils.NullToDbNull(Categories)),
				                                                new SqlParameter("@TagIds", sTags),
				                                                new SqlParameter("@TagNames", System.DBNull.Value),
				                                                new SqlParameter("@IncludeTickerNews", IncludeNewsInNewsTickerList),
				                                                new SqlParameter("@IncludeContent", IncludeContent),
																new SqlParameter("@DateFrom", Utils.NullToDbNull(DateFrom)),
																new SqlParameter("@DateTo", Utils.NullToDbNull(DateTo)),
				                                                new SqlParameter("@PageNr", PageNr),
				                                                new SqlParameter("@PageSize", PageSize),
				                                                new SqlParameter("@OnlySubscribed", OnlySubscribed),
		                                                        new SqlParameter("@OnlyAvailable", OnlyAvailable),
				                                                new SqlParameter("@UID", Utils.NullToDbNull(UID))
				                                        ).ToList();
		}
				
		public List<GetNewsListResult> SP_GetNewsList(string CategoryCode, string[] TagNames, bool IncludeNewsInNewsTickerList, bool IncludeContent, bool OnlySubscribed, bool OnlyAvailable, long? UID, int PageNr, int PageSize, string Categories = null, DateTime? DateFrom = null, DateTime? DateTo = null)
		{
			string sSQL = "[dbo].[sp_GetNewsList] @CategoryCode, @Categories, @TagIds, @TagNames, @IncludeTickerNews, @IncludeContent, @DateFrom, @DateTo, @PageNr, @PageSize, @OnlySubscribed, @OnlyAvailable, @UID";
			string sTags = string.Empty;
				
			if ((TagNames != null) && (TagNames.Length > 0))
				sTags = String.Join(",", TagNames);
				
			return Database.SqlQuery<GetNewsListResult>(sSQL, new SqlParameter("@CategoryCode", Utils.NullToDbNull(CategoryCode)),
																new SqlParameter("@Categories", Utils.NullToDbNull(Categories)),
				                                                new SqlParameter("@TagIds", System.DBNull.Value),
				                                                new SqlParameter("@TagNames", sTags),
				                                                new SqlParameter("@IncludeTickerNews", IncludeNewsInNewsTickerList),
				                                                new SqlParameter("@IncludeContent", IncludeContent),
																new SqlParameter("@DateFrom", Utils.NullToDbNull(DateFrom)),
																new SqlParameter("@DateTo", Utils.NullToDbNull(DateTo)),
				                                                new SqlParameter("@PageNr", PageNr),
				                                                new SqlParameter("@PageSize", PageSize),
				                                                new SqlParameter("@OnlySubscribed", OnlySubscribed),
		                                                        new SqlParameter("@OnlyAvailable", OnlyAvailable),
		                                                        new SqlParameter("@UID", Utils.NullToDbNull(UID))
				                                        ).ToList();
		}
		#endregion
		#region SP_GetNewsListCount
		public long SP_GetNewsListCount(string CategoryCode, int[] TagIds, bool IncludeNewsInNewsTickerList, bool OnlySubscribed, bool OnlyAvailable, long? UID, string Categories = null, DateTime? DateFrom = null, DateTime? DateTo = null)
		{
			string sSQL = "[dbo].[sp_GetNewsListCount] @CategoryCode, @Categories, @TagIds, @TagNames, @IncludeTickerNews, @DateFrom, @DateTo, @OnlySubscribed, @OnlyAvailable, @UID";
			string sTags = string.Empty;
				
			if ((TagIds != null) && (TagIds.Length > 0))
				sTags = String.Join(",", TagIds);
				
			long? Count = Database.SqlQuery<long>(sSQL, new SqlParameter("@CategoryCode", Utils.NullToDbNull(CategoryCode)),
																new SqlParameter("@Categories", Utils.NullToDbNull(Categories)),
				                                                new SqlParameter("@TagIds", sTags),
				                                                new SqlParameter("@TagNames", System.DBNull.Value),
				                                                new SqlParameter("@IncludeTickerNews", IncludeNewsInNewsTickerList),
																new SqlParameter("@DateFrom", Utils.NullToDbNull(DateFrom)),
																new SqlParameter("@DateTo", Utils.NullToDbNull(DateTo)),
				                                                new SqlParameter("@OnlySubscribed", OnlySubscribed),
		                                                        new SqlParameter("@OnlyAvailable", OnlyAvailable),
		                                                        new SqlParameter("@UID", Utils.NullToDbNull(UID))).FirstOrDefault();
			return (Count == null ? 0 : Count.Value);
		}
				
		public long SP_GetNewsListCount(string CategoryCode, string[] TagNames, bool IncludeNewsInNewsTickerList, bool OnlySubscribed, bool OnlyAvailable, long? UID, string Categories = null, DateTime? DateFrom = null, DateTime? DateTo = null)
		{
			string sSQL = "[dbo].[sp_GetNewsListCount] @CategoryCode, @Categories, @TagIds, @TagNames, @IncludeTickerNews, @DateFrom, @DateTo, @OnlySubscribed, @OnlyAvailable, @UID";
			string sTags = string.Empty;
				
			if ((TagNames != null) && (TagNames.Length > 0))
				sTags = String.Join(",", TagNames);
				
			long? Count = Database.SqlQuery<long>(sSQL, new SqlParameter("@CategoryCode", Utils.NullToDbNull(CategoryCode)),
																new SqlParameter("@Categories", Utils.NullToDbNull(Categories)),
				                                                new SqlParameter("@TagIds", System.DBNull.Value),
				                                                new SqlParameter("@TagNames", sTags),
				                                                new SqlParameter("@IncludeTickerNews", IncludeNewsInNewsTickerList),
																new SqlParameter("@DateFrom", Utils.NullToDbNull(DateFrom)),
																new SqlParameter("@DateTo", Utils.NullToDbNull(DateTo)),
				                                                new SqlParameter("@OnlySubscribed", OnlySubscribed),
		                                                        new SqlParameter("@OnlyAvailable", OnlyAvailable),
		                                                        new SqlParameter("@UID", Utils.NullToDbNull(UID))).FirstOrDefault();
			return (Count == null ? 0 : Count.Value);
		}
		#endregion
		#region SP_GetNewsMessages
		public List<NewsMessageDTO> SP_GetNewsMessages(long NewsId, long? UID, long? ParentId, bool? Recursive)
		{
			string sSQL = "[dbo].[sp_GetNewsMessages] @NewsId, @UID, @ParentId, @Recursive";
				
			return Database.SqlQuery<NewsMessageDTO>(sSQL, new SqlParameter("@NewsId", NewsId),
				                                            new SqlParameter("@UID", Utils.NullToDbNull(UID)),
				                                            new SqlParameter("@ParentId", Utils.NullToDbNull(ParentId)),
				                                            new SqlParameter("@Recursive", Utils.NullToDbNull(Recursive))
				                                        ).ToList();
		}
		#endregion
		#region SP_UpdateNewsStatus
		public void SP_UpdateNewsStatus()
		{
		    string sSQL = "[dbo].[sp_UpdateNewsStatus]";
		
		    Database.ExecuteSqlCommand(sSQL);
		}
		#endregion
		// ##section[NEWSMODULE_STOREDPROCEDURES] end - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
        #region SP_GetUsersByPhoneNumbers
        public List<UserBase> SP_GetUsersByPhoneNumbersSearch7(long AuthenticatedUID, List<string> PhoneNumbersSearch7)
        {
            string sSQL = "[dbo].[sp_GetUsersByPhoneNumbers] @UIDAuthUser, @PhoneListSearch7";
            return Database.SqlQuery<UserBase>(sSQL, 
                new SqlParameter("@UIDAuthUser", AuthenticatedUID),
                new SqlParameter("@PhoneListSearch7", string.Join(",", PhoneNumbersSearch7.ToArray()))).ToList();
        }
        #endregion
        #region SP_GetVendingMachineList
        public List<VendingMachineDTO> SP_GetVendingMachineList(bool OnlyActive = true, bool OnlyRecommended = false, bool OnlyAppActivated = true, long? ActivityId = null, long? CriteriaId = null, decimal? Latitude = null, decimal? Longitude = null, long RadiusMeters = 100000, bool IsTestUser = false, int PageNr = 1, int PageSize = 99999)
        {
            string sSQL = "[dbo].[sp_GetVendingMachineList] @Latitude, @Longitude, @RadiusMeters, @OnlyActive, @ActivityId, @CriteriaId, @OnlyRecommended, @OnlyAppActivated, @IsTestUser, @PageNr, @PageSize";
            return Database.SqlQuery<VendingMachineDTO>(sSQL, new SqlParameter("@Latitude", Utils.NullToDbNull(Latitude)),
                                                              new SqlParameter("@Longitude", Utils.NullToDbNull(Longitude)),
                                                              new SqlParameter("@RadiusMeters", RadiusMeters),
                                                              new SqlParameter("@OnlyActive", OnlyActive),
                                                              new SqlParameter("@ActivityId", Utils.NullToDbNull(ActivityId)),
                                                              new SqlParameter("@CriteriaId", Utils.NullToDbNull(CriteriaId)),
                                                              new SqlParameter("@OnlyRecommended", OnlyRecommended),
                                                              new SqlParameter("@OnlyAppActivated", OnlyAppActivated),
                                                              new SqlParameter("@IsTestUser", IsTestUser),
                                                              new SqlParameter("@PageNr", PageNr),
                                                              new SqlParameter("@PageSize", PageSize)).ToList();
        }
        #endregion
        #region SP_GetVendingMachineListCount
        public long SP_GetVendingMachineListCount(bool OnlyActive = true, bool OnlyRecommended = false, bool OnlyAppActivated = true, long? ActivityId = null, long? CriteriaId = null, decimal? Latitude = null, decimal? Longitude = null, long RadiusMeters = 100000, bool IsTestUser = false)
        {
            string sSQL = "[dbo].[sp_GetVendingMachineListCount] @Latitude, @Longitude, @RadiusMeters, @OnlyActive, @ActivityId, @OnlyRecommended, @OnlyAppActivated, @IsTestUser";

            long? Count = Database.SqlQuery<long>(sSQL, new SqlParameter("@Latitude", Utils.NullToDbNull(Latitude)),
                                                        new SqlParameter("@Longitude", Utils.NullToDbNull(Longitude)),
                                                        new SqlParameter("@RadiusMeters", RadiusMeters),
                                                        new SqlParameter("@OnlyActive", OnlyActive),
                                                        new SqlParameter("@ActivityId", Utils.NullToDbNull(ActivityId)),
                                                        new SqlParameter("@CriteriaId", Utils.NullToDbNull(CriteriaId)),
                                                        new SqlParameter("@OnlyRecommended", OnlyRecommended),
                                                        new SqlParameter("@OnlyAppActivated", OnlyAppActivated),
                                                        new SqlParameter("@IsTestUser", IsTestUser)).FirstOrDefault();
            return (Count == null ? 0 : Count.Value);
        }
        #endregion
        #region SP_CheckMachinesRecommended
        public void SP_CheckMachinesRecommended()
        {
            string sSQL = "[dbo].[sp_CheckMachinesRecommended]";

            Database.ExecuteSqlCommand(sSQL);
        }
        #endregion
        #region SP_CheckExpiredPoints
        /// <summary>
        ///     Checks the points for the users for expired points,
        ///     refreshes the current points of the user
        ///     and returns the total amount of points for this user.
        /// </summary>
        public long SP_CheckExpiredPoints(long UID)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_CheckExpiredPoints] @UID";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            Database.ExecuteSqlCommand(sSQL, new SqlParameter("@UID", UID), parReturnCode);

            return Convert.ToInt32(parReturnCode.Value);
        }
        #endregion
        #region SP_AddUserPoints
        public int SP_AddUserPoints(long UID, int Amount, PointsTransactionType TransactionType, long? ReferenceUID, long? SalesTransactionId, long? ActivityId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_AddUserPoints] @UID, @Amount, @ExpiryDate, @TransactionType, @ReferenceUID, @SalesTransactionId, @ActivityId, @PointsList";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sSQL,  
                                        new SqlParameter("@UID", UID),
                                        new SqlParameter("@Amount", Amount),
                                        new SqlParameter("@ExpiryDate", Utils.GetPointsExpiryDate(DateTime.UtcNow)),
                                        new SqlParameter("@TransactionType", TransactionType),
                                        new SqlParameter("@ReferenceUID", Utils.NullToDbNull(ReferenceUID)),
                                        new SqlParameter("@SalesTransactionId", Utils.NullToDbNull(SalesTransactionId)),
                                        new SqlParameter("@ActivityId", Utils.NullToDbNull(ActivityId)),
                                        new SqlParameter("@PointsList", System.DBNull.Value),
                                        parReturnCode);

            return Convert.ToInt32(parReturnCode.Value);
        }
        #endregion
        #region SP_UseUserPoints
        public int SP_UseUserPoints(long UID, int Amount, PointsTransactionType TransactionType, long? ReferenceUID, long? SalesTransactionId, long? ActivityId, long? PromotionOrderId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_UseUserPoints] @UID, @Amount, @TransactionType, @ReferenceUID, @SalesTransactionId, @ActivityId, @PromoOrderId, @PointsList";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            int iRetValue = Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sSQL,  // procedure have it's own transaction
                                                             new SqlParameter("@UID", UID),
                                                             new SqlParameter("@Amount", Amount),
                                                             new SqlParameter("@TransactionType", TransactionType),
                                                             new SqlParameter("@ReferenceUID", Utils.NullToDbNull(ReferenceUID)),
                                                             new SqlParameter("@SalesTransactionId", Utils.NullToDbNull(SalesTransactionId)),
                                                             new SqlParameter("@ActivityId", Utils.NullToDbNull(ActivityId)),
                                                             new SqlParameter("@PromoOrderId", Utils.NullToDbNull(PromotionOrderId)),
                                                             new SqlParameter("@PointsList", System.DBNull.Value),
                                                             parReturnCode);

            return Convert.ToInt32(parReturnCode.Value);
        }
        #endregion
        #region SP_SendPoints
        public int SP_SendPoints(long UID, long FriendUID, int Amount)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_SendPoints] @UID, @FriendUID, @Amount";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            int iRetValue = Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sSQL,  // procedure have it's own transaction
                                                             new SqlParameter("@UID", UID),
                                                             new SqlParameter("@FriendUID", FriendUID),
                                                             new SqlParameter("@Amount", Amount),
                                                             parReturnCode);

            return Convert.ToInt32(parReturnCode.Value);
        }
        #endregion
        #region SP_UpdateSalesList
        /// <summary>
        ///     Updates the product availability for the SalesList of the given VendingMachineId
        ///     with the given 'ProductsAvailability' availability list (returned by the MOCCPImportProvider).
        ///     After updating the availability this function also returns the current sales list for this machine.
        /// </summary>
        public List<SalesListEntryDTO> SP_UpdateSalesList(long VendingMachineId, string ProductsAvailability, long UID, out bool MachineProcessingOtherOrder, out bool MachineNotYetProcessedLastMessage, bool ReturnOnlyAvailableProducts = true, long? ActivityIdFilter = null, long? CriteriaIdFilter = null, bool UpdateAvailabilityList = true, bool ForRedeem = false, long? LotteryPriceId = null)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_UpdateSalesList] @VendingMachineId, @ActivityIdFilter, @CriteriaIdFilter, @AvailabilityList, @UpdateAvailList, @ForRedeem, @LotteryPriceId, @ReturnOnlyAvailable, @SalesTransExpiryMinutes, @VendingMachineMsgExpirySeconds, @UID";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<SalesListEntryDTO> SalesList = Database.SqlQuery<SalesListEntryDTO>(sSQL, new SqlParameter("@VendingMachineId", VendingMachineId),
                                                                                           new SqlParameter("@ActivityIdFilter", Utils.NullToDbNull(ActivityIdFilter)),
                                                                                           new SqlParameter("@CriteriaIdFilter", Utils.NullToDbNull(CriteriaIdFilter)),
                                                                                           new SqlParameter("@AvailabilityList", ProductsAvailability),
                                                                                           new SqlParameter("@ForRedeem", ForRedeem),
                                                                                           new SqlParameter("@LotteryPriceId", Utils.NullToDbNull(LotteryPriceId)),
                                                                                           new SqlParameter("@UpdateAvailList", UpdateAvailabilityList),
                                                                                           new SqlParameter("@ReturnOnlyAvailable", ReturnOnlyAvailableProducts),
                                                                                           new SqlParameter("@SalesTransExpiryMinutes", GlobalConst.SalesTransactionsExpiryTime),
                                                                                           new SqlParameter("@VendingMachineMsgExpirySeconds", GlobalConst.SalesTransactionMessageExpiryTimeSeconds),
                                                                                           new SqlParameter("@UID", UID),
                                                                                           parReturnCode).ToList();

            int iReturnCode = Convert.ToInt32(parReturnCode.Value);
            MachineProcessingOtherOrder = false;
            MachineNotYetProcessedLastMessage = false;

            if (iReturnCode != 1)
            {
                if (((ReturnCodes)iReturnCode) == ReturnCodes.VendingMachineAlreadyHasOrder)
                    MachineProcessingOtherOrder = true;
                else if (((ReturnCodes)iReturnCode) == ReturnCodes.VendingMachineHasOpenOrderMessage)
                    MachineNotYetProcessedLastMessage = true;
                else
                    throw new Exception("An error occured while updating/retrieving the sales list for the vending machine " + VendingMachineId.ToString() + "! The ReturnCode of the sp_UpdateSalesList procedure was: " + iReturnCode.ToString() + ". ProductsAvailability List: " + ProductsAvailability);
            }

            return SalesList;
        }
        #endregion
        #region SP_StartOrderProduct
        public BaseResponseDTO SP_StartOrderProduct(long UID, long VendingMachineId, long ProductId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_StartOrderProduct] @UID, @VendingMachineId, @ProductId, @SalesTransExpiryMinutes, @VendingMachineMsgExpirySeconds";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<StartOrderProductResultDTO> Msg = Database.SqlQuery<StartOrderProductResultDTO>(sSQL,
                                                        new SqlParameter("@UID", UID),
                                                        new SqlParameter("@VendingMachineId", VendingMachineId),
                                                        new SqlParameter("@ProductId", ProductId),
                                                        new SqlParameter("@SalesTransExpiryMinutes", GlobalConst.SalesTransactionsExpiryTime),
                                                        new SqlParameter("@VendingMachineMsgExpirySeconds", GlobalConst.SalesTransactionMessageExpiryTimeSeconds),
                                                        parReturnCode).ToList();

            BaseResponseDTO Response = new BaseResponseDTO((ReturnCodes)Convert.ToInt32(parReturnCode.Value), (Msg.Count > 0 ? Msg[0].Message : string.Empty));
            Response.Data = (Msg.Count > 0 ? Msg[0].HasStamps : false);
            return Response;
        }
        #endregion
        #region SP_GetMachineMsgProductSelection
        public BaseResponseDTO SP_GetMachineMsgProductSelection(string Organization, string AssetNo)
        {
            string sSQL = "[dbo].[sp_GetMachineMsgProductSelection] @Organization, @AssetNo";

            List<BuyBuyChooseProductResultDTO> ChosenList = Database.SqlQuery<BuyBuyChooseProductResultDTO>(sSQL, 
                                                                            new SqlParameter("@Organization", Organization),
                                                                            new SqlParameter("@AssetNo", AssetNo)).ToList();

            BaseResponseDTO Response = null;
            if (ChosenList.Count > 0)
            {
                Response = new BaseResponseDTO(ChosenList[0]);
                if (ChosenList[0].TransactionId <= 0)
                    Response.ReturnCode = ReturnCodes.NoData;
                return Response;
            }
            else
            {
                BuyBuyChooseProductResultDTO Result = new BuyBuyChooseProductResultDTO();
                Result.MerchandiseID = string.Empty;
                Result.InternalId = -1;
                Result.TransactionId = -1;

                Response = new BaseResponseDTO(ReturnCodes.NoData);
                Response.Data = Result;
                return Response;
            }
        }
        #endregion
        #region SP_GetMachineMsgRedeemCoupon
        public BaseResponseDTO SP_GetMachineMsgRedeemCoupon(string Organization, string AssetNo)
        {
            string sSQL = "[dbo].[sp_GetMachineMsgRedeemCoupon] @Organization, @AssetNo";

            List<BuyBuyRedeemCouponResultDTO> CouponList = Database.SqlQuery<BuyBuyRedeemCouponResultDTO>(sSQL,
                                                                         new SqlParameter("@Organization", Organization),
                                                                         new SqlParameter("@AssetNo", AssetNo)).ToList();

            BaseResponseDTO Response = null;
            if (CouponList.Count > 0)
            {
                Response = new BaseResponseDTO(CouponList[0]);
                if (CouponList[0].CouponId == 0)
                    Response.ReturnCode = ReturnCodes.NoData;
                return Response;
            }
            else
            {
                BuyBuyRedeemCouponResultDTO Result = new BuyBuyRedeemCouponResultDTO();
                Result.MerchandiseID = string.Empty;
                Result.CouponId = -1;

                Response = new BaseResponseDTO(ReturnCodes.NoData);
                Response.Data = Result;
                return Response;
            }
        }
        #endregion
        #region SP_CompleteMachineOrder
        public BaseResponseDTO SP_CompleteMachineOrder(string Organization, string AssetNo, int? InternalId, long? VmaTransactionId, string BuyBuyTransactionNr, bool Cancel, out long? UID, out long? ProductId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_CompleteMachineOrder] @Organization, @AssetNo, @InternalId, @VmaTransactionId, @BuyBuyTransactionNr, @Cancel, @PointsExpiryDate, @SalesTransExpiryMinutes";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<CompleteOrderResultDTO> Msg = Database.SqlQuery<CompleteOrderResultDTO>(sSQL, 
                                                        new SqlParameter("@Organization", Organization),
                                                        new SqlParameter("@AssetNo", AssetNo),
                                                        new SqlParameter("@InternalId", Utils.NullToDbNull(InternalId)),
                                                        new SqlParameter("@VmaTransactionId", Utils.NullToDbNull(VmaTransactionId)),
                                                        new SqlParameter("@BuyBuyTransactionNr", Utils.NullToDbNull(BuyBuyTransactionNr)),
                                                        new SqlParameter("@Cancel", Cancel),
                                                        new SqlParameter("@PointsExpiryDate", Utils.GetPointsExpiryDate(DateTime.UtcNow)),
                                                        new SqlParameter("@SalesTransExpiryMinutes", GlobalConst.SalesTransactionsExpiryTime),
                                                        parReturnCode).ToList();

            BaseResponseDTO Response = new BaseResponseDTO((ReturnCodes)Convert.ToInt32(parReturnCode.Value), (Msg.Count > 0 ? Msg[0].Message : string.Empty));
            Response.Data = (Msg.Count > 0 ? Msg[0].QRToken : string.Empty);
            UID = (Msg.Count > 0 ? Msg[0].UID : null);
            ProductId = (Msg.Count > 0 ? Msg[0].ProductId : null);

            if (Response.ReturnCode == ReturnCodes.PointsCouldNotBeAdded)
            {
                Utils.LogError("WARNING! Could not add points for buying a product to the user account. Error message returned by sp_CompleteMachineOrder: " + Response.ErrorMessage + ". This is a warning. The order has been completed anyway!");
                Response.ErrorMessage = string.Empty;
                Response.ReturnCode = ReturnCodes.Success;
            }

            return Response;
        }
        #endregion
        #region SP_AcquireOrderStamps
        public BaseResponseDTO SP_AcquireOrderStamps(UserBase AuthenticatedUser, string QRToken, long? ChosenActivityId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_AcquireOrderStamps] @UID, @QRToken, @PointsExpiryDate, @ChosenActivityId";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<AcquireOrderStampsResultDTO> Result = Database.SqlQuery<AcquireOrderStampsResultDTO>(sSQL,
                                                        new SqlParameter("@UID", AuthenticatedUser.UID),
                                                        new SqlParameter("@QRToken", QRToken),
                                                        new SqlParameter("@PointsExpiryDate", Utils.GetPointsExpiryDate(DateTime.UtcNow)),
                                                        new SqlParameter("@ChosenActivityId", Utils.NullToDbNull(ChosenActivityId)),
                                                        parReturnCode).ToList();

            // process the result
            BaseResponseDTO Response = new BaseResponseDTO((ReturnCodes)Convert.ToInt32(parReturnCode.Value), (Result.Count > 0 && (! string.IsNullOrWhiteSpace(Result[0].Message)) ? Result[0].Message : string.Empty));
            AddStampsApiResultDTO ResultDTO = new AddStampsApiResultDTO();
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                // for return code 1 - return the coupon data
                if ((Result[0].UserCouponId != null) && (Result[0].UserCouponId > 0))
                {
                    Providers.ActivitiesProvider AP = new Providers.ActivitiesProvider();
                    GetActivityCouponParamsDTO GetCouponParams = new GetActivityCouponParamsDTO();
                    GetCouponParams.SetAuthenticatedUser(AuthenticatedUser);
                    GetCouponParams.ActivityId = null;
                    GetCouponParams.UserCouponId = Result[0].UserCouponId.Value;
                    GetCouponParams.NoStamps = false;

                    BaseResponseDTO GetCouponResponse = AP.GetActivityCoupon(GetCouponParams);
                    if (GetCouponResponse.ReturnCode == ReturnCodes.Success)
                    {
                        ResultDTO.Activity = (CouponActivityDTO)GetCouponResponse.Data;
                        ResultDTO.PointsAdded = (Result[0].PointsAdded != null ? Result[0].PointsAdded.Value : 0);
                        ResultDTO.StampsAdded = (Result[0].StampsAdded != null ? Result[0].StampsAdded.Value : 0);
                        ResultDTO.WatersportsCoinsAdded = (Result[0].WatersportsCoinsAdded != null ? Result[0].WatersportsCoinsAdded.Value : 0);
                    }
                    else
                        return GetCouponResponse;
                }
                else
                {
                    ResultDTO.Activity = null;
                    ResultDTO.PointsAdded = (Result[0].PointsAdded != null ? Result[0].PointsAdded.Value : 0);
                    ResultDTO.StampsAdded = (Result[0].StampsAdded != null ? Result[0].StampsAdded.Value : 0);
                    ResultDTO.WatersportsCoinsAdded = (Result[0].WatersportsCoinsAdded != null ? Result[0].WatersportsCoinsAdded.Value : 0);
                }
            }
            else if (Response.ReturnCode == ReturnCodes.OperationPending)
            {
                // for return code 3 - return the activity selection list
                ResultDTO.CouponSelectionList = Result.Select(Mapper.Map<AcquireOrderStampsResultDTO, CouponSelectionEntryDTO>).ToList();
            }

            Response.Data = ResultDTO;
            return Response;
        }
        #endregion
        #region SP_GetCouponActivityList
        public List<GetCouponActivityListResultDTO> SP_GetCouponActivityList(long UID, bool OnlyCompleted = false, bool ForPromotionMachinesList = false, long? VendingMachineId = null)
        {
            string sSQL = "[dbo].[sp_GetCouponActivityList] @UID, @OnlyCompleted, @ForPromotionMachinesList, @VendingMachineId";
            return Database.SqlQuery<GetCouponActivityListResultDTO>(sSQL, new SqlParameter("@UID", UID),
                                                                           new SqlParameter("@OnlyCompleted", OnlyCompleted),
                                                                           new SqlParameter("@ForPromotionMachinesList", ForPromotionMachinesList),
                                                                           new SqlParameter("@VendingMachineId", Utils.NullToDbNull(VendingMachineId))).ToList();
        }
        #endregion
        #region SP_LotteryDrawGame
        public BaseResponseDTO SP_LotteryDrawGame(long UID, long LotteryGameId)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_LotteryDrawGame] @UID, @LotteryGameId";
            LotteryDrawGameResultDTO ResultDTO = new LotteryDrawGameResultDTO();

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<LotteryDrawGameResult> ResultListDb = Database.SqlQuery<LotteryDrawGameResult>(sSQL, new SqlParameter("@UID", UID),
                                                                                                      new SqlParameter("@LotteryGameId", LotteryGameId),
                                                                                                      parReturnCode).ToList();

            ReturnCodes ReturnCode = (ReturnCodes)Convert.ToInt32(parReturnCode.Value);
            if (ReturnCode == ReturnCodes.Success)
            {
                LotteryDrawGameResult ResultDb = null;
                if (ResultListDb.Count > 0)
                    ResultDb = ResultListDb[0];
                else
                    throw new Exception("SP_LotteryDrawGame did not return any result!");

                ResultDTO.Won = ResultDb.Won;
                ResultDTO.CoinsLeft = ResultDb.CoinsLeft;
                if (ResultDb.Won)
                {
                    ResultDTO.PriceWon = new LotteryPriceCouponDTO();
                    ResultDTO.PriceWon.PriceId = ResultDb.PriceId;
                    ResultDTO.PriceWon.IsVendingMachinePrice = ResultDb.IsVendingMachinePrice;
                    ResultDTO.PriceWon.PickupNumber = ResultDb.PricePickupNumber;
                    ResultDTO.PriceWon.Title = ResultDb.PriceTitle;
                    ResultDTO.PriceWon.Unit = ResultDb.PriceUnit;
                    ResultDTO.PriceWon.ImageUrl = ResultDb.PriceImageUrl;
                    ResultDTO.PriceWon.IsUsable = ResultDb.IsUsable;
                    ResultDTO.PriceWon.PickupAddress = ResultDb.PricePickupAddress;
                    ResultDTO.PriceWon.PickupPhone = ResultDb.PricePickupPhone;
                }
                else
                    ResultDTO.PriceWon = null;

                return new BaseResponseDTO(ResultDTO);
            }
            else
                return new BaseResponseDTO(ReturnCode);
        }
        #endregion
        #region SP_GetLotteryCouponList
        public List<LotteryPriceCouponDTO> SP_GetLotteryCouponList(long LotteryGameId, long UID, bool OnlyUsable = false)
        {
            string sSQL = "[dbo].[sp_GetLotteryCouponList] @LotteryGameId, @UID, @OnlyUsable";
            return Database.SqlQuery<LotteryPriceCouponDTO>(sSQL, new SqlParameter("@LotteryGameId", LotteryGameId),
                                                                  new SqlParameter("@UID", UID),
                                                                  new SqlParameter("@OnlyUsable", OnlyUsable)).ToList();
        }
        #endregion
        #region SP_AddDailyLotteryCoins
        public void SP_AddDailyLotteryCoins()
        {
            string sSQL = "[dbo].[sp_AddDailyLotteryCoins]";
            Database.CommandTimeout = 0;
            Database.ExecuteSqlCommand(sSQL);
        }
        #endregion
        #region SP_LotteryRedeemMachinePrice
        public BaseResponseDTO SP_LotteryRedeemMachinePrice(long UID, long LotteryGamePriceId, long VendingMachineId, long ProductId, long? ActivityId, string PickupToken)
        {
            string sSQL = "exec @ReturnCode = [dbo].[sp_LotteryRedeemMachinePrice] @UID, @PriceId, @VendingMachineId, @SelectedProductId, @ActivityIdFilter, @PickupToken";

            SqlParameter parReturnCode = new SqlParameter("@ReturnCode", System.Data.SqlDbType.Int);
            parReturnCode.Direction = System.Data.ParameterDirection.Output;

            List<LotteryDrawGameResult> ResultListDb = Database.SqlQuery<LotteryDrawGameResult>(sSQL, new SqlParameter("@UID", UID),
                                                                                                      new SqlParameter("@PriceId", LotteryGamePriceId),
                                                                                                      new SqlParameter("@VendingMachineId", VendingMachineId),
                                                                                                      new SqlParameter("@SelectedProductId", Utils.NullToDbNull(ProductId)),
                                                                                                      new SqlParameter("@ActivityIdFilter", Utils.NullToDbNull(ActivityId)),
                                                                                                      new SqlParameter("@PickupToken", Utils.NullToDbNull(PickupToken)),
                                                                                                      parReturnCode).ToList();

            ReturnCodes ReturnCode = (ReturnCodes)Convert.ToInt32(parReturnCode.Value);
            return new BaseResponseDTO(ReturnCode);
        }
        #endregion
        #region SPCheckMachineMessagesExpiry
        public void SP_CheckMachineMessagesExpiry()
        {
            string sSQL = "[dbo].[sp_CheckMachineMessagesExpiry] @VendingMachineMsgExpirySeconds";

            Database.ExecuteSqlCommand(sSQL, new SqlParameter("@VendingMachineMsgExpirySeconds", GlobalConst.SalesTransactionMessageExpiryTimeSeconds));
        }
        #endregion
        #region SP_CleanImportLog
        public void SP_CleanImportLog()
        {
            string sSQL = "[dbo].[sp_CleanImportLog] @MaxAgeDays";

            Database.CommandTimeout = 300;
            Database.ExecuteSqlCommand(sSQL, new SqlParameter("@MaxAgeDays", ConfigManager.ImportLogMaxAgeInDays));
        }
        #endregion

        #region Constructor
        public ApplicationDbContext()
            : base(ConfigManager.ConnectionString)
        {
            LambdaViewsHelper = new _LambdaViews(this);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<VmaBase.Models.DataStorage.ApplicationDbContext, VmaBase.DataMigrations.Configuration>());
            //System.Data.Entity.Infrastructure.Interception.DbInterception.Add(new EFDebugTraceCommandInterceptor()); // prints SQL statements generated by EF to the debug window
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.MaximaNewsModule
{
    public class NewsTickerBaseDTO
    {
        [JsonProperty("order")]
        public int BannerOrder { get; set; }

        [JsonProperty("bannerImageUrl")]
        public string BannerImageUrl { get; set; }

        [JsonProperty("categoryCode")]
        public string CategoryCode { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("newsId")]
        public long NewsId { get; set; }

        [JsonProperty("dateUpdate")]
        public DateTime UpdateTime { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("newsImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("status")]
        public NewsTickerStatus Status { get; set; }
    }
}

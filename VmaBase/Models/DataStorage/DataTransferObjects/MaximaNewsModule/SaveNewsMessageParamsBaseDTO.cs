﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.MaximaNewsModule
{
    public class SaveNewsMessageParamsBaseDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "newsId")]
		[SwaggerDescription(Description = "Id of the news article to add or modify the message for", Example = "#int:1")]
        public long NewsId { get; set; }

        [JsonProperty(PropertyName = "message")]
		[SwaggerDescription(Description = "The message to save.\nFor new messages (updateType 1) ONLY the 'Message' property is needed!\nFor modifying a message please also specify the 'Id' property (which is the message id to modify)")]
        public NewsMessageDTO Message { get; set; }

        [JsonProperty(PropertyName = "updateType")]
		[SwaggerDescription(Description = "Type of update to be done for this news message.\n1 = Add a new news message\n2 = Modify an already existing news message (please specify the message id in Message.Id!)\n3 = Delete a message (not supported yet!)", Example = "#int:1")]
        public NewsUpdateTypes UpdateType { get; set; }
    }
}

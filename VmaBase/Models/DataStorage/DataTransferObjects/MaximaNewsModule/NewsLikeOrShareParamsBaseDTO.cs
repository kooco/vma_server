﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using VmaBase.Models.Enum;
using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MaximaNewsModule
{
    public class NewsLikeOrShareParamsBaseDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "type")]
        [SwaggerDescription(Description = "Type of the article to add/remove a like/share to/from: 1 = News Article, 2 = News comment (user comment)", Example = "#int:1")]
        public NewsArticleTypes Type { get; set; }

        [JsonProperty(PropertyName = "newsId")]
        [SwaggerDescription(Description = "Id of the news article, if type = 1. If type = 2: null should be set!", Example = "#int:1044")]
        public long NewsId { get; set; }

        [JsonProperty(PropertyName = "messageId")]
        [SwaggerDescription(Description = "Id of the news comment (user message), if type = 2. If type = 1: null should be set!", Example = "#null")]
        public long MessageId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class AcquireOrderStampsResultDTO
    {
        public long? ActivityId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? LimitStartTime { get; set; }
        public DateTime? LimitEndTime { get; set; }
        public long? UserCouponId { get; set; }
        public int? PointsAdded { get; set; }
        public int? StampsAdded { get; set; }
        public int? WatersportsCoinsAdded { get; set; }
        public string Message { get; set; }
    }
}

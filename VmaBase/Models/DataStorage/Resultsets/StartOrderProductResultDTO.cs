﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class StartOrderProductResultDTO
    {
        public string Message { get; set; }
        public bool HasStamps { get; set; }
    }
}

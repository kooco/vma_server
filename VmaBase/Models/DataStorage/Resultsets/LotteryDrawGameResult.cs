﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class LotteryDrawGameResult
    {
        public string Message { get; set; }

        public bool Won { get; set; }

        public int CoinsLeft { get; set; }

        public long PriceId { get; set; }

        public string PriceTitle { get; set; }

        public string PriceImageUrl { get; set; }

        public string PriceUnit { get; set; }

        public string PricePickupNumber { get; set; }

        public string PricePickupAddress { get; set; }

        public string PricePickupPhone { get; set; }

        public bool IsVendingMachinePrice { get; set; }

        public bool IsUsable { get; set; }
    }
}

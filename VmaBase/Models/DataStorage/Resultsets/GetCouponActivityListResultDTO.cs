﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class GetCouponActivityListResultDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "criteriaId")]
        [SwaggerDescription(Description = "Id of the rule/criteria which created this auto-coupon", Example = "#int:1")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "couponsType")]
        [SwaggerDescription(Description = "Type of the coupons.\n1 = Activity coupons. (ActivityId is used)\n2 = automatically created coupons. (CriteriaId is used)")]
        public UserCouponType CouponsType { get; set; }

        [JsonProperty(PropertyName = "title")]
        [SwaggerDescription(Description = "Activity/News title", Example = "Newest news - now you can get one free coke for 6 stickers!!")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "activityText")]
        [SwaggerDescription(Description = "Text line for this activity", Example = "Just collect 6 stickers and you get one coke for free")]
        public string ActivityText { get; set; }

        [JsonProperty(PropertyName = "limitStartTime")]
        [SwaggerDescription(Description = "Start time (date+time) of this activity")]
        public DateTime LimitStartTime { get; set; }

        [JsonProperty(PropertyName = "expiryTime")]
        [SwaggerDescription(Description = "Expiry time of the coupons for this activity")]
        public DateTime ExpiryTime { get; set; }

        [JsonProperty(PropertyName = "limitEndTime")]
        [SwaggerDescription(Description = "End time (date+time) of this activity")]
        public DateTime LimitEndTime { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Url for the list image of this activity", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/max_news_list_image_0000003.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "activityListImageUrl")]
        [SwaggerDescription(Description = "Url for the special activity-list image of this activity (not implemented yet!)", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/max_act_list_image_0000003.jpg")]
        public string ActivityListImageUrl { get; set; }

        [JsonProperty(PropertyName = "amountTotal")]
        [SwaggerDescription(Description = "Amount of completed (and not yet redeemed) coupons this user have for this activity, which could be used for redeeming.", Example = "#int:5")]
        public int AmountTotal { get; set; }

        [JsonProperty(PropertyName = "amountSendable")]
        [SwaggerDescription(Description = "Amount of completed (and not yet redeemed) coupons that can be sent to other users.", Example = "#int:4")]
        public int AmountSendable { get; set; }

        [JsonProperty(PropertyName = "isNoStampsActivity")]
        [SwaggerDescription(Description = "true = this activity is a 'NoStamps activity', which means no stamps can be collected for this coupon! Also no stamps should be shown.\nThis is a special activity type for using the activity coupon system for prices won (for example), to be able to use the same redeem system.\nfalse = stamps can be collected for this activity. This is the case for most types of activities.", Example = "#bool:false")]
        public bool IsNoStampsActivity { get; set; }

        [JsonProperty(PropertyName = "sendAllowed")]
        [SwaggerDescription(Description = "true = sending coupons is allowed for this activity.\nfalse = sending coupons to friends is NOT allowed for this activity", Example = "#bool:true")]
        public bool SendAllowed { get; set; }
    }
}

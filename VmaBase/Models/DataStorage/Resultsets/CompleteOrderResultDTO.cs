﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class CompleteOrderResultDTO
    {
        public string Message { get; set; }
        public string QRToken { get; set; }
        public long? UID { get; set; }
        public long? ProductId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Resultsets
{
    public class GetNewsListResult
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string ImageUrl { get; set; }

        public string DetailImageUrl { get; set; }

        public long CategoryId { get; set; }

        public string CategoryCode { get; set; }

        public string CategoryName { get; set; }

        // will be overwritten by the AutoMapper when converting to GetNewsListEntryDTO! (NewsContents will be used then)
        public string Content { get; set; }

        public long? UIDAuthor { get; set; }

        public string AuthorAccount { get; set; }

        public long? UIDEditor { get; set; }

        public string EditorAccount { get; set; }

        public long LikeCount { get; set; }

        public long ShareCount { get; set; }

        public int IsTicker { get; set; }

        public int OnSubscription { get; set; }

        public DateTime? LimitStartTime { get; set; }

        public DateTime? LimitEndTime { get; set; }

        public NewsStatus Status { get; set; }

        public ActivityStatus? ActivityStatus { get; set; }

        public DateTime CreateTime { get; set; }

        public bool UserLike { get; set; }

        // will be mapped by the AutoMapper to GetNewsListEntryDTO.Content!
        public string NewsContents { get; set; }

        public long? ActivityId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     VendingProduct Data Class
    ///     =========================
    ///     
    ///     Data Class: 商品
    ///     
    ///     Purpose: 
    ///     Stores a list of all available products can be used for the vending machines
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class VendingProduct
    {
        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        [Index("UK_ExternalProductId", IsUnique = true)]
        public string MOCCPGoodsId { get; set; }

        [StringLength(15)]
        [Index("IX_InternalId", Order = 1)] // TODO make the index unique
        public string MOCCPOrganization { get; set; }

        [Index("IX_InternalId", Order = 2)]
        public int? MOCCPInternalId { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(64)]
        public string NameEN { get; set; }

        [StringLength(128)]
        public string ImageUrl { get; set; }

        [StringLength(32)]
        public string Volume { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "9")]
        public long ProductTypeId { get; set; }

        [ForeignKey("ProductTypeId")]
        public VendingProductType ProductType { get; set; }
        
        [Required]
        [SqlDefaultValue(DefaultValue = "19")]
        public long ProductBrandId { get; set; }

        [ForeignKey("ProductBrandId")]
        public VendingProductBrand ProductBrand { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int PointsExtra { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int PointsMultiplier { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int StampsMultiplier { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool IsWatersports { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int WatersportsCoins { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public double AveragePrice { get; set; }

        public long? DefaultStampTypeId { get; set; }

        public StampType DefaultStampType { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public VendingProductStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public VendingProduct()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = VendingProductStatus.Available;
            ProductTypeId = GlobalConst.ProductTypeId_Others;
            ProductBrandId = GlobalConst.ProductBrandId_Others;
            PointsExtra = 0;
            PointsMultiplier = 1;
            StampsMultiplier = 1;
            AveragePrice = 0;
            IsWatersports = false;
            WatersportsCoins = 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     StampTransaction Data Class
    ///     ===========================
    ///     
    ///     Data Class:   交易Log
    ///     
    ///     Purpose: 
    ///     Sales (vending) transactions log
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class SalesTransaction
    {
        public long Id { get; set; }

        [StringLength(64)]
        [Index("UIX_ReceiptNumber", IsUnique = true)]
        public string ReceiptNumber { get; set; }

        [StringLength(32)]
        // Unique index UIX_SalesTransaction_BuyBuyTransactionNr
        public string BuyBuyTransactionNr { get; set; }

        [ForeignKey("User")]
        public long? UID { get; set; }

        public UserData User { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public PurchaseType PurchaseType { get; set; }

        [Required]
        [Index("IX_Status_Machine", Order = 2)]     // Index for checking if the vending machine already has another open sales transaction (when starting an order)
        public long VendingMachineId { get; set; }
        public VendingMachine VendingMachine { get; set; }

        [Required]
        public long ProductId { get; set; }
        public VendingProduct Product { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        [StringLength(32)]
        [Index("UIX_QRToken", IsUnique = true)]
        public string QRToken { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        [Index("IX_Status_CreateTime", Order = 1)]  // Index for searching for expired sales transactions
        [Index("IX_Status_Machine", Order = 1)]     // Index for checking if the vending machine already has another open sales transaction (when starting an order)
        public SalesTransactionStatus Status { get; set; }        

        [Required]
        
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        [Index("IX_Status_CreateTime", Order = 2)]  // Index for searching for expired sales transactions
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public SalesTransaction()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = SalesTransactionStatus.Initiated;
            PurchaseType = Enum.PurchaseType.SmartphoneApp;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     NewsSubscription Data Class
    ///     ===========================
    ///     
    ///     Data Class:  News subscriptions
    ///     
    ///     Purpose: 
    ///     Stores user subscriptions to news articles
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class NewsSubscription : MaximaNewsModule.NewsSubscriptionBase
    {
        public NewsSubscription() : base() { }
    }
}

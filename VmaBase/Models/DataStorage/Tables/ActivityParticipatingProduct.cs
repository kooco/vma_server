﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     ActivityParticipatingProduct Class
    ///     ==================================
    ///     
    ///     Data Class: Participating products for the coupon-activity
    ///     
    ///     Purpose: 
    ///     Stores a list of products which are part of this coupon-activity.
    ///     With all these products stamps can be collected.
    ///     References to the table
    ///         CouponActivity .... the header of the coupon-activity
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class ActivityParticipatingProduct
    {
        public long Id { get; set; }

        [Required]
        [Index("IX_Activity")]
        [Index("UIX_Activity_Product", IsUnique = true, Order = 1)]
        public long ActivityId { get; set; }

        [ForeignKey("ActivityId")]
        public CouponActivity Activity { get; set; }

        [Required]
        [Index("UIX_Activity_Product", IsUnique = true, Order = 2)]
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public VendingProduct Product { get; set; }

        [Required]
        public long StampTypeId1 { get; set; }

        [ForeignKey("StampTypeId1")]
        public StampType StampType1 { get; set; }

        public long? StampTypeId2 { get; set; }

        [ForeignKey("StampTypeId2")]
        public StampType StampType2 { get; set; }

        public long? StampTypeId3 { get; set; }

        [ForeignKey("StampTypeId3")]
        public StampType StampType3 { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public bool Redeemable { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public ActivityParticipatingProduct()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = GeneralStatusEnum.Active;
            Redeemable = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryPriceAvailabilityItem Data Class
    ///     =======================================
    ///     
    ///     Data Class: Lottery Price Availabilities
    ///     
    ///     Purpose: 
    ///     This table is used for spreading the price items randomly over the lottery time range.
    ///     Every time the 'WinDate' is reached this item is considered as 'free to win'.
    ///     The next user will win this item then.
    ///     With that logic a random price winning is ensured and it's also ensured that
    ///     every price is given away until the end of the lottery game.
    ///     Also with this method it's possible to already see when every price will be given away before the lottery game starts
    ///     by the administrator.
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryPriceAvailabilityItem
    {
        public long Id { get; set; }

        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        [Index("IX_Game_WinPriceDate", Order = 2)]
        public LotteryGame Game { get; set; }

        public long LotteryPriceId { get; set; }

        [ForeignKey("LotteryPriceId")]
        public LotteryPrice Price { get; set; }

        [Index("IX_Game_WinPriceDate", Order = 2)]
        public DateTime WinFromTime { get; set; }

        public long? WonByUID { get; set; }

        [ForeignKey("WonByUID")]
        public UserData WonByUser { get; set; }

        public DateTime? WonTime { get; set; }

        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryPriceAvailabilityItem()
        {
            Status = GeneralStatusEnum.Active;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryPriceRedeemProduct Data Class
    ///     ====================================
    ///     
    ///     Data Class: Lottery Prices - Products that can be redeemed for
    ///     
    ///     Purpose: 
    ///     Stores a list of products that can be redeemed for the connected lottery price
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryPriceRedeemProduct
    {
        public long Id { get; set; }

        [Required]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame LotteryGame { get; set; }

        [Required]
        [Index("UIX_LotteryPrice_Product", IsUnique = true, Order = 1)]
        public long LotteryPriceId { get; set; }

        [ForeignKey("LotteryPriceId")]
        public LotteryPrice LotteryPrice { get; set; }

        [Required]
        [Index("UIX_LotteryPrice_Product", IsUnique = true, Order = 2)]
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public VendingProduct Product { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryPriceRedeemProduct()
        {
            Status = GeneralStatusEnum.Active;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

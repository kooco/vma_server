﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserPoint Class
    ///     ================
    ///     
    ///     Data Class: User-Points Table
    ///     
    ///     Purpose: 
    ///     Stores all coins the user gathered with expiry date
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserPoint
    {
        public long Id { get; set; }

        [Required]
        [Index("UserPoint_UIDStatus", Order = 1)]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public int OriginalAmount { get; set; }

        [Required]
        [Index("UserPoint_UIDStatus", Order = 2)]
        [SqlDefaultValue(DefaultValue = "1")]
        public UserPointStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime DateAquired { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ExpiryDate { get; set; }

        public UserPoint()
        {
            DateAquired = DateTime.UtcNow;
            ExpiryDate = Utils.GetPointsExpiryDate(DateTime.UtcNow);
            Status = UserPointStatus.Available;
        }
    }
}

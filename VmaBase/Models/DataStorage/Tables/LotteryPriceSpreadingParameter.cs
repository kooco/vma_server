﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryPriceSpreadingParameter Data Class
    ///     =========================================
    ///     
    ///     Data Class: Lottery Prices Spreading parameters
    ///     
    ///     Purpose: 
    ///     Stores a more detailed parameters list to control the lottery spreading logic
    ///     in a more detailed way
    ///     (how many of the available price items should be able to win in which period of time)
    ///     Using this parameters can be activated/deactivated with the "UseSpreadingParameters" column of the "LotteryPrices" table
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryPriceSpreadingParameter
    {
        public long Id { get; set; }

        [Required]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame LotteryGame { get; set; }

        [Required]
        public long LotteryPriceId { get; set; }

        [ForeignKey("LotteryPriceId")]
        public LotteryPrice Price { get; set; }

        [Required]
        public DateTime PeriodFrom { get; set; }

        [Required]
        public DateTime PeriodTo { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int SpreadCount { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryPriceSpreadingParameter()
        {
            SpreadCount = 0;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

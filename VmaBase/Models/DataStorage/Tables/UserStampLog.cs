﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserStampLog Class
    ///     ==================
    ///     
    ///     Data Class: UserCouponStamp Log-Table
    ///     
    ///     Purpose: 
    ///     Stores a log for every "stamp transaction" for the users.
    ///     A log line will be written if
    ///       - a stamp has been received by buying a product
    ///       - a stamp has been sent to a friend
    ///       - a stamp has been received by a friend
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserStampLog
    {
        public long Id { get; set; }

        [Required]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [Index("IX_CouponId")]
        public long UserStampId { get; set; }

        [ForeignKey("UserStampId")]
        public UserCouponStamp UserStamp { get; set; }

        [Required]
        public long StampTypeId { get; set; }

        [ForeignKey("StampTypeId")]
        public StampType StampType { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public UserStampLogType LogType { get; set; }

        // no db foreign key by purpose! The connected sales transaction log may be deleted before the stamp log is deleted.
        public long? SalesTransactionId { get; set; }

        public long? UserReferenceId { get; set; }

        [ForeignKey("UserReferenceId")]
        public UserBase UserReference { get; set; }

        [Required]
        [StringLength(128)]
        public string LogText { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue ="getutcdate()")]
        public DateTime LogTime { get; set; }

        public UserStampLog()
        {
            LogTime = DateTime.UtcNow;
            LogType = UserStampLogType.ReceivedByPurchase;
        }
    }
}

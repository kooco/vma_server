﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryGame Data Class
    ///     ======================
    ///     
    ///     Data Class: Lottery Games
    ///     
    ///     Purpose: 
    ///     Store lottery games
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryGame
    {
        public long Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Title { get; set; }
        public const int TitleMaxLength = 64;

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        [Required]
        public DateTime CouponsExpiryTime { get; set; }

        public DateTime? TestStartTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public LotteryTestMode TestMode { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public LotteryDrawType DrawType { get; set; }

        public long? ConnectedActivityId { get; set; }

        [ForeignKey("ConnectedActivityId")]
        public CouponActivity ConnectedActivity { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool CoinsCanExpire { get; set; }

        public DateTime? CoinsExpiryDate { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "2")]
        public ExpiryType CoinsExpiryType { get; set; } // not used yet

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int CoinsExpiryPeriod { get; set; }      // not used yet

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public LotteryPickupTokenMode PickupTokenMode { get; set; }

        // the pickup token, if the PickupTokenMode is set to "GameToken"
        [StringLength(PickupTokenMaxLength)]
        public string PickupToken { get; set; }
        public const int PickupTokenMaxLength = 12;

        // defines how many daily coins the users are getting every day automatically
        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int DailyCoins { get; set; }

        [SqlDefaultValue(DefaultValue = "1")]
        public LotteryGameStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryGame()
        {
            PickupTokenMode = LotteryPickupTokenMode.None;
            DrawType = LotteryDrawType.UseLotteryCoins;
            TestMode = LotteryTestMode.Off;
            Status = LotteryGameStatus.Inactive;
            CoinsCanExpire = false;
            CoinsExpiryType = ExpiryType.EndOfIssueMonth;
            CoinsExpiryPeriod = 1;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

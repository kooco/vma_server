﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryPrice Data Class
    ///     =======================
    ///     
    ///     Data Class: Lottery Prices
    ///     
    ///     Purpose: 
    ///     The prices that can be won by a lottery game
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryPrice
    {
        public long Id { get; set; }

        [Required]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame LotteryGame { get; set; }

        [Required]
        [StringLength(64)]
        public string Title { get; set; }
        public const int TitleMaxLength = 64;

        [Required]
        [StringLength(128)]
        public string ImageUrl { get; set; }
        public const int ImageUrlMaxLength = 128;

        [Required]
        [StringLength(4)]
        [SqlDefaultValue(DefaultValue = "N'個'")]
        public string Unit { get; set; }

        [Required]
        [StringLength(128)]
        public string PickupAddress { get; set; }
        public const int PickupAddressMaxLength = 128;

        [Required]
        [StringLength(32)]
        public string PickupPhone { get; set; }
        public const int PickupPhoneMaxLength = 32;

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        // Flag, if this price should become an activity drink coupon, if won
        public bool UseActivityCoupon { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool UseSpreadingParameters { get; set; }

        // if this product id is set to NULL, but UseActivityCoupon is set to true, then the products list
        // defined in LotteryPriceRedeemProducts will be used
        public long? ActivityCouponProductId { get; set; }

        [ForeignKey("ActivityCouponProductId")]
        public VendingProduct ActivityCouponProduct { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        // Flag, if the lottery redeem API should choose the correct product automatically
        // by the availability in the given vending machine. (choose from the products defined in LotteryPriceRedeemProducts)
        public bool AutoChooseFirstAvailProduct { get; set; }

        [Required]
        public int AmountAvailable { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int AmountWon { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int AmountPickedUp { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryPrice()
        {
            UseActivityCoupon = false;
            UseSpreadingParameters = false;
            AmountWon = 0;
            AmountPickedUp = 0;
            Unit = "個";
            Status = GeneralStatusEnum.Active;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

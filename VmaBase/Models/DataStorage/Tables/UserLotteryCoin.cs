﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserWatersportsCoin Class
    ///     =========================
    ///     
    ///     Data Class: User-Lottery-Coins Table (for all lottery type games, also for watersports)
    ///     
    ///     Purpose: 
    ///     Stores all the lottery coins the user has
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserLotteryCoin
    {
        public long Id { get; set; }

        [Required]
        [Index("UserLotteryCoin_GameUIDStatus", Order = 1)]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame Game { get; set; }

        [Required]
        [Index("UserLotteryCoin_GameUIDStatus", Order = 2)]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public LotteryCoinType Type { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public int OriginalAmount { get; set; }

        [Required]
        [Index("UserLotteryCoin_GameUIDStatus", Order = 3)]
        [SqlDefaultValue(DefaultValue = "1")]
        public LotteryCoinStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime DateAquired { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ExpiryDate { get; set; }
       
        public UserLotteryCoin()
        {
            DateAquired = DateTime.UtcNow;
            ExpiryDate = new DateTime(2099, 12, 31);
            Type = LotteryCoinType.Earned;
            Status = LotteryCoinStatus.Available;
        }
    }
}

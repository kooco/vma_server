﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     SingleTimeQRToken Data Class
    ///     ============================
    ///     
    ///     Data Class: Single Time QR Tokens
    ///     
    ///     Purpose: 
    ///     Stores a list of single-use QR token
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class SingleUseQRToken
    {
        public long Id { get; set; }
        
        [Required]
        [StringLength(64)]
        [Index("UIX_TypeToken", IsUnique = true, Order = 2)]
        public string Token { get; set; }
        
        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        [Index("UIX_TypeToken", IsUnique = true, Order = 1)]
        public SingleUseQRType Type { get; set; }

        [Index("IX_UseTime")]
        public DateTime UseTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public SingleUseQRStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        public SingleUseQRToken()
        {
            Type = SingleUseQRType.WatersportsLotteryCoin;
            Status = SingleUseQRStatus.Created;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryGame Data Class
    ///     ======================
    ///     
    ///     Data Class: Lottery Game Usage Statistics
    ///     
    ///     Purpose: 
    ///     Saves daily use statistics for the lottery game draws
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryGameUseStatistic
    {
        public long Id { get; set; }

        [Required]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame Game { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "cast(getutcdate() as date)")]
        [DataType(DataType.Date)]
        public DateTime DrawDate { get; set; }

        [Required]
        public int DrawCount { get; set; }

        public int WinCount { get; set; }

        public LotteryGameUseStatistic()
        {
            DrawDate = DateTime.UtcNow.Date;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     AutoCouponAssignment Data Class
    ///     ===============================
    ///     
    ///     Data Class: Log for all auto-coupon assignments
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class AutoCouponAssignment
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_Criteria_User", IsUnique = true, Order = 1)]
        public long CriteriaId { get; set; }

        [ForeignKey("CriteriaId")]
        public AutoCouponCriteria Criteria { get; set; }

        [Required]
        [Index("UIX_Criteria_User", IsUnique = true, Order = 2)]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime AssignmentTime { get; set; }

        public long? UserCouponId { get; set; }

        [ForeignKey("UserCouponId")]
        public UserCoupon UserCoupon { get; set; }

        public AutoCouponAssignment()
        {
            AssignmentTime = DateTime.UtcNow;
        }
    }
}

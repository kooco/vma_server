﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;
using Kooco.Framework.Shared.EntityFramework;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     StampType Data Class
    ///     ===================
    ///     
    ///     Data Class: 印花的分類
    ///     
    ///     Purpose: 
    ///     Store stamp types (which types of coupons/stamps the user can get)
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class StampType
    {
        public long Id { get; set; }

        // unique index manually create: UIX_Number (for Status <> -1)
        [Required]
        public int Number { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [Required]
        [StringLength(128)]
        public string ImageUrl { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "9")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime DateCreated { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime DateModified { get; set; }

        public StampType()
        {
            DateCreated = DateTime.UtcNow;
            DateModified = DateTime.UtcNow;
            Status = GeneralStatusEnum.Inactive;
        }
    }
}

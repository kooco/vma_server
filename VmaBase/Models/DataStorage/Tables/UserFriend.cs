﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserFriend Data Class
    ///     ======================
    ///     
    ///     Data Class: 朋友清單
    ///     
    ///     Purpose: 
    ///     Store the friend lists of the users
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserFriend
    {
        public long Id { get; set; }

        // EF automatically created index: IX_UID
        [Required]
        [ForeignKey("User")]
        public long UID { get; set; }

        public UserData User { get; set; }

        // EF automatically created index: IX_UIDFriend
        [Required]
        [ForeignKey("FriendUser")]
        public long UIDFriend { get; set; }

        public UserData FriendUser { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public UserFriendStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public UserFriend()
        {
            Status = UserFriendStatus.Pending;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

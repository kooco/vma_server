﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     VendingProductBrand Data Class
    ///     ==============================
    ///     
    ///     Data Class: 產品牌
    ///     
    ///     Purpose: 
    ///     Stores a list of brands to be assigned to the products then
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class VendingProductBrand
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_Name", IsUnique = true)]
        [StringLength(NameMaxLength)]
        public string Name { get; set; }
        public const int NameMaxLength = 64;

        [Required]        
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public VendingProductBrand()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = GeneralStatusEnum.Active;
        }
    }
}

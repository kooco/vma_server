﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     AutoCouponCriteriaMachine Class
    ///     ===============================
    ///     
    ///     Data Class: Vending machine assignment for the auto coupon criterias
    ///     
    ///     Purpose: 
    ///     If for an auto coupon criteria the vending machines should be limited
    ///     then in this table all the vending machines are saved, 
    ///     all the auto-created coupons (created by the criteria) are allowed to use for
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class AutoCouponCriteriaMachine
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_Criteria_Machine", IsUnique = true, Order = 1)]
        public long CriteriaId { get; set; }
        
        [ForeignKey("CriteriaId")]
        public AutoCouponCriteria Criteria { get; set; }

        [Required]
        public long MachineId { get; set; }

        [ForeignKey("MachineId")]
        [Index("UIX_Criteria_Machine", IsUnique = true, Order = 2)]
        public VendingMachine Machine { get; set; }

        public AutoCouponCriteriaMachine()
        {

        }
    }
}

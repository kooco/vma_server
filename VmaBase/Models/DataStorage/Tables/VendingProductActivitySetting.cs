﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     VendingProductActivitySettings Data Class
    ///     =========================================
    ///     
    ///     Data Class: 商品的活動設定
    ///     
    ///     Purpose: 
    ///     Stores extra settings on per activity base for a product
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class VendingProductActivitySetting
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_Product_Activity", IsUnique = true, Order = 1)]
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public VendingProduct Product { get; set; }

        [Required]
        [Index("UIX_Product_Activity", IsUnique = true, Order = 2)]
        public long ActivityId { get; set; }

        [ForeignKey("ActivityId")]
        public CouponActivity Activity { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int PointsMultiplier { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int StampsMultiplier { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int PointsExtra { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public VendingProductActivitySetting()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            PointsMultiplier = 1;
            StampsMultiplier = 1;
            PointsExtra = 0;
        }
    }
}

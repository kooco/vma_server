﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryDrawLog Data Class
    ///     =========================
    ///     
    ///     Data Class: Lottery draw log
    ///     
    ///     Purpose: 
    ///     Logs every draw done for this lottery game
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryDrawLog
    {
        public long Id { get; set; }

        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        [Index("IX_Game_DrawDate", Order = 1)]
        public LotteryGame Game { get; set; }

        [Required]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "cast(getutcdate() as date)")]
        [DataType(DataType.Date)]
        [Index("IX_Game_DrawDate", Order = 2)]
        // date only!
        public DateTime DrawDate { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime DrawTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool Won { get; set; }
        
        public long? LotteryPriceIdWon { get; set; }

        [ForeignKey("LotteryPriceIdWon")]
        public LotteryPrice PriceWon { get; set; }

        public long? LotteryPriceCouponId { get; set; }

        [ForeignKey("LotteryPriceCouponId")]
        public LotteryPriceCoupon LotteryPriceCoupon { get; set; }

        public LotteryDrawLog()
        {
            DrawDate = DateTime.UtcNow.Date;
            DrawTime = DateTime.UtcNow;
            Won = false;
        }
    }
}

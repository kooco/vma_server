﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     SalesList Data Class
    ///     =====================
    ///     
    ///     Data Class: 販賣列表
    ///     
    ///     Purpose: 
    ///     Stores lists of products sold at each veding machine
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class SalesList
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_MOCCPMerchandiseId", IsUnique = true, Order = 1)]
        [Index("UIX_MOCCPProductId", IsUnique = true, Order = 1)]
        public long VendingMachineId { get; set; }
        [ForeignKey("VendingMachineId")]
        public VendingMachine VendingMachine { get; set; }

        [Required]
        [StringLength(32)]
        [Index("UIX_MOCCPMerchandiseId", IsUnique = true, Order = 2)]
        public string MOCCPMerchandiseId { get; set; }

        public int? MOCCPInternalId { get; set; }

        [Required]
        [Index("UIX_MOCCPProductId", IsUnique = true, Order = 2)]
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public VendingProduct Product { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public double Price { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int RetrievablePoints { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int RetrievableStamps { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public SalesStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public SalesList()
        {
            Price = 0;
            RetrievableStamps = 0;
            Status = SalesStatus.OnSale;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

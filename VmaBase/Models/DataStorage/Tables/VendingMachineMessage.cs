﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     VendingMachineMessage Data Class
    ///     ================================
    ///     
    ///     Data Class: Messages sent to or received by the vending machines
    ///     
    ///     Purpose: 
    ///     Messages sent to or received by the vending machines
    ///     (excluding machine QR tokens sent!)
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class VendingMachineMessage
    {
        public long Id { get; set; }

        [Required]
        [Index("IX_StatusDirectionTypeAssetNo", Order = 2)]
        public VendingMachineDirection Direction { get; set; }

        [Required]
        [Index("IX_StatusDirectionTypeAssetNo", Order = 3)]
        public VendingMachineMessageType Type { get; set; }

        [Required]
        [Index("IX_StatusDirectionTypeAssetNo", Order = 4)]
        [StringLength(32)]
        public string AssetNo { get; set; }

        [Required]
        public long VendingMachineId { get; set; }
        public VendingMachine VendingMachine { get; set; }

        public long? ProductId { get; set; }
        public VendingProduct Product { get; set; }

        public long? SalesTransactionId { get; set; }
        public SalesTransaction SalesTransaction { get; set; }

        [StringLength(32)]
        public string CouponToken { get; set; }

        public long? CouponId { get; set; }
        public UserCoupon Coupon { get; set; }

        public long? LotteryPriceCouponId { get; set; }

        [ForeignKey("LotteryPriceCouponId")]
        public LotteryPriceCoupon LotteryPriceCoupon { get; set; }

        [StringLength(32)]
        public string BuyBuyTransactionNo { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        [Index("IX_StatusDirectionTypeAssetNo", Order = 1)]
        [Index("IX_StatusCreateTime", Order = 1)]
        public VendingMachineMessageStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        [Index("IX_StatusCreateTime", Order = 2)]
        public DateTime CreateTime { get; set; }

        public DateTime? QueryTime { get; set; }

        public VendingMachineMessage()
        {
            CreateTime = DateTime.UtcNow;
            Status = VendingMachineMessageStatus.Created;
        }
    }
}

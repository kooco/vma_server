﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     CouponStampDefinition Class
    ///     ===========================
    ///     
    ///     Data Class: Stamp-Definitions for a coupon-activity
    ///     
    ///     Purpose: 
    ///     Stores which stamps have to be collected for a coupon activity.
    ///     References to the table
    ///         CouponActivity .... the header of the coupon-activity
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class CouponStampDefinition
    {
        public long Id { get; set; }

        [Required]
        [Index("IX_Activity")]
        public long ActivityId { get; set; }

        [ForeignKey("ActivityId")]        
        public CouponActivity Activity { get; set; }

        public long? StampTypeId { get; set; }

        [ForeignKey("StampTypeId")]
        public StampType StampType { get; set; }

        public CouponStampDefinition()
        {

        }
    }
}

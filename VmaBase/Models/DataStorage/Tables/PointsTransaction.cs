﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     PointsTransaction Data Class
    ///     ============================
    ///     
    ///     Data Class: 積分異動紀錄
    ///     
    ///     Purpose: 
    ///     Store bonus points transactions
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class PointsTransaction
    {
        public long Id { get; set; }

        [ForeignKey("UserData")]
        [Index("IX_User")]
        public long UID { get; set; }

        [Required]
        public UserData UserData { get; set; }

        [Required]
        public PointsTransactionType TransactionType { get; set; }
       
        [Required]
        public double Amount { get; set; }
        
        /// <summary>
        ///     if this points are sent to another user or received by another user, then this field contains
        ///     the user who sent this point (if received) or will receive the points (if sent)
        /// </summary>
        public long? ReferenceUserId { get; set; }

        [ForeignKey("ReferenceUserId")]
        public UserData ReferenceUser { get; set; }

        /// <summary>
        ///     if this points are received or used from/for buying a product
        ///     then this is the sales transaction caused receving/loosing the points
        /// </summary>
        public long? SalesTransactionId { get; set; }

        [ForeignKey("SalesTransactionId")]
        public SalesTransaction SalesTransaction { get; set; }

        public long? ActivityId { get; set; }

        /// <summary>
        ///     If this points are received by a promotion (activity)
        ///     then this is the reference to this activity
        /// </summary>
        [ForeignKey("ActivityId")]
        public CouponActivity Activity { get; set; }

        /// <summary>
        ///     If this points are deducted by an exchanged promotion product
        ///     or received by a refunded promotion product
        ///     then this is the reference to the promotion order
        /// </summary>
        public long? PromotionOrderId { get; set; }

        [ForeignKey("PromotionOrderId")]
        public PromotionOrder PromotionOrder { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }
    }
}

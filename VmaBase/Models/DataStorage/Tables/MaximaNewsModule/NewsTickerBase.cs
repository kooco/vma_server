﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     NewsTickerBase Data Class
    ///     =========================
    ///     
    ///     Data Class:  新聞輪播 
    ///     
    ///     Purpose: 
    ///     Store assigned news ticker articles (新聞輪播)
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsTickerBase
    {
        public long Id { get; set; }

        [Required]
        public int BannerOrder { get; set; }

        [StringLength(128)]
        public string BannerImageUrl { get; set; }

        [Required]
        [ForeignKey("NewsHeader")]
        public long NewsId { get; set; }
        public NewsArticle NewsHeader { get; set; }

        [Required]
        public NewsTickerStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime UpdateTime { get; set; }

        public NewsTickerBase()
        {
            Status = NewsTickerStatus.Online;
        }
    }
}

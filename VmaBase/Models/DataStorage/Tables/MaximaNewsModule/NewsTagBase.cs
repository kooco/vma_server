﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     NewsTag Class
    ///     =============
    ///     
    ///     Data Class: 新聞Tag的設定
    ///     
    ///     Purpose: 
    ///     Store Tags to be used by news articles
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsTagBase
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public List<NewsArticle> News { get; set; }

        public NewsTagBase()
        {
            News = new List<NewsArticle>();
        }
    }
}

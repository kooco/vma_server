﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage.Tables;


namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     UserLike Data Class
    ///     ===================
    ///     
    ///     Data Class:  LOL News/Comment likes
    ///     
    ///     Purpose: 
    ///     Store the likes for each user and news message or comment
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserLikeBase
    {
        public long Id { get; set; }

        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData User { get; set; }

        public long? NewsId { get; set; }

        [ForeignKey("NewsId")]
        public NewsArticle News { get; set; }

        public long? MessageId { get; set; }

        [ForeignKey("MessageId")]
        public NewsMessage Message { get; set; }

        [Required]
        public GeneralStatusEnum Status { get; set; }

        public DateTime CreateTime { get; set; }

        public UserLikeBase()
        {
            Status = GeneralStatusEnum.Active;
            CreateTime = DateTime.UtcNow;
        }
    }
}

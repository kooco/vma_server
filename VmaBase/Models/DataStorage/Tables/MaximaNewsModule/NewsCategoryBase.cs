﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     News Category Class
    ///     ===================
    ///     
    ///     Data Class: 新聞分類
    ///     
    ///     Purpose: 
    ///     Store news categories (新聞分類)
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsCategoryBase
    {
        public long Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Code { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}

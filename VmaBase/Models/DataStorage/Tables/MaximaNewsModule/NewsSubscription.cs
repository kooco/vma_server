﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     NewsSubscription Data Class
    ///     ===========================
    ///     
    ///     Data Class: News Subscriptions
    ///     
    ///     Purpose: 
    ///     Subscriptions for news articles
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsSubscriptionBase
    {
        public long Id { get; set; }
        
        [Required]
        [Index("IX_UserNewsSubscriptions", IsUnique = true, Order = 1)]
        public long UID { get; set; }
        [ForeignKey("UID")]
        public UserData User { get; set; }

        [Index("IX_UserNewsSubscriptions", IsUnique = true, Order = 2)]
        public long NewsId { get; set; }
        [ForeignKey("NewsId")]
        public NewsArticle News { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }
    }
}

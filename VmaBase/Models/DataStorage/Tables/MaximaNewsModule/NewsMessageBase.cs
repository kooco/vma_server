﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     NewsMessageBase Data Class
    ///     ==========================
    ///     
    ///     Data Class:  新聞留言
    ///     
    ///     Purpose: 
    ///     Store news messages (新聞留言)
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsMessageBase
    {
        [Key]
        public long MessageId { get; set; }

        public long? ParentMessageId { get; set; }

        [ForeignKey("ParentMessageId")]
        public NewsMessage ParentMessage { get; set; }

        [Required]
        public long NewsId { get; set; }
        public NewsArticle NewsHeader { get; set; }

        [Required]
        public long UID { get; set; }
        [ForeignKey("UID")]
        public Kooco.Framework.Models.DataStorage.Tables.UserBase UserBase { get; set; }
        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [StringLength(200)]
        public string Message { get; set; }
		public const int MessageMaxLength = 200;

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public long LikeCount { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public long ShareCount { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "-1")]
        public NewsStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        public NewsMessageBase()
        {
            CreateTime = DateTime.UtcNow;
            Status = NewsStatus.Online;
            ShareCount = 0;
            LikeCount = 0;
        }
    }
}

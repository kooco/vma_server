﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables.MaximaNewsModule
{
    /// <summary>
    ///     NewsArticle Data Class
    ///     ======================
    ///     
    ///     Data Class: 新聞
    ///     
    ///     Purpose: 
    ///     Store news (新聞) articles (header records)
    ///     
    ///     ---
    ///     AUTO-GENERATED CLASS!
    ///     Part of the Maxima.NewsModuleBase NuGet Package!
    ///     THIS CLASS WILL BE OVERWRITTEN ON EVERY UPDATE!
    ///     ---
    /// </summary>
    public class NewsArticleBase
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(100)]
        public string ImageUrl { get; set; }

        [Index("IX_News_CategoryId", IsUnique = false)]
        public long? CategoryId { get; set; }

        public NewsCategory Category { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        [Display(Name = "內文")]
        public string Content { get; set; }

        public List<NewsTag> Tags { get; set; }


        [Display(Name = "作者")]
        public long? UIDAuthor { get; set; }

        [Display(Name = "作者")]
        [ForeignKey("UIDAuthor")]
        public Kooco.Framework.Models.DataStorage.Tables.UserBase Author { get; set; }


        [Display(Name = "責任編輯")]
        public long? UIDEditor { get; set; }

        [Display(Name = "責任編輯")]
        [ForeignKey("UIDEditor")]
        public Kooco.Framework.Models.DataStorage.Tables.UserBase Editor { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public long LikeCount { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public long ShareCount { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool ReadOnly { get; set; }

        public DateTime? LimitStartTime { get; set; }

        public DateTime? LimitEndTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "9")]
        public NewsStatus Status { get; set; }

        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        public List<NewsMessage> Messages { get; set; }

        public NewsArticleBase()
        {
            CreateTime = DateTime.UtcNow;
            Tags = new List<NewsTag>();
            Messages = new List<NewsMessage>();
            Status = NewsStatus.Offline;
            ReadOnly = false;
        }
    }
}

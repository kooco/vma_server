﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     AutoCouponUserState Data Class
    ///     ==============================
    ///     
    ///     Data Class: Custom settings for an auto-coupon rule per user
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class AutoCouponUserState
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_Criteria_User", IsUnique = true, Order = 1)]
        public long CriteriaId { get; set; }

        [ForeignKey("CriteriaId")]
        public AutoCouponCriteria Criteria { get; set; }

        [Required]
        [Index("UIX_Criteria_User", IsUnique = true, Order = 2)]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int Counter { get; set; }

        public DateTime? DateValue { get; set; }

        [StringLength(2048)]
        public string Settings { get; set; }
        public const int SettingsMaxLength = 2048;

        [Column(TypeName = "ntext")]
        public string Blob { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public AutoCouponUserStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public AutoCouponUserState()
        {
            Counter = 0;
            Status = AutoCouponUserStatus.Started;
        }
    }
}

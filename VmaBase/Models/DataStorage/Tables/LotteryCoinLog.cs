﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryCoinLog Data Class
    ///     =========================
    ///     
    ///     Data Class: Lottery coin log
    ///     
    ///     Purpose: 
    ///     Logs every coin received or used
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryCoinLog
    {
        public long Id { get; set; }

        [Required]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime TransactionTime { get; set; }

        [Required]
        public LotteryCoinLogType Type { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public int Coins { get; set; }

        public long? LotteryDrawLogId { get; set; }

        [ForeignKey("LotteryDrawLogId")]
        public LotteryDrawLog DrawLog { get; set; }

        public long? FriendUID { get; set; }

        [ForeignKey("FriendUID")]
        public UserData FriendUserData { get; set; }

        [StringLength(64)]
        public string QRTokenUsed { get; set; }

        public LotteryCoinLog()
        {
            TransactionTime = DateTime.UtcNow;
            Coins = 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     ImportLogMessage Class
    ///     ======================
    ///     
    ///     Data Class: Import Log Messages Table
    ///     
    ///     Purpose: 
    ///     Stores all messages for the import logs
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class ImportLogMessage
    {
        public long Id { get; set; }

        [Required]
        [Index("IX_ImportLogId")]
        public long ImportLogId { get; set; }

        [ForeignKey("ImportLogId")]
        public ImportLog ImportLog { get; set; }

        [Required]
        [StringLength(200)]
        public string Message { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime LogTime { get; set; }

        public ImportLogMessage()
        {
            LogTime = DateTime.UtcNow;
        }
    }
}

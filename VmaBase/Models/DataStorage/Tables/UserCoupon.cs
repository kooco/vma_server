﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserCoupon Class
    ///     =================
    ///     
    ///     Data Class: Status/Assignment of each user's coupon
    ///     
    ///     Purpose: 
    ///     Stores the coupon status and collected stamps (in child table UserCouponStamp)
    ///     for each participating user
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserCoupon
    {
        public long Id { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public UserCouponType Type { get; set; }

        // for coupons created by stamp collecting activities
        [Index("IX_ActivityUser", IsUnique = false, IsClustered = true, Order = 1)]
        public long? ActivityId { get; set; }

        [ForeignKey("ActivityId")]        
        public CouponActivity Activity { get; set; }
        
        // for coupons auto-created by "criterias" - this type of coupons will have no stamps!
        public long? AutoCouponCriteriaId { get; set; }

        [ForeignKey("AutoCouponCriteriaId")]
        public AutoCouponCriteria AutoCouponCriteria { get; set; }

        [Required]
        [Index("IX_ActivityUser", IsUnique = false, IsClustered = true, Order = 2)]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        // SenderUID will contain the UID of the user who have sent this coupon, if this coupon has been sent as a gift from another user
        [Index("IX_ActivitySenderUser")]
        public long? SenderUID { get; set; }

        [ForeignKey("SenderUID")]
        public UserData SenderUserData { get; set; }

        // Contains the UID of the user who collected and completed this coupon
        public long? CompletedBy { get; set; }

        [ForeignKey("CompletedBy")]
        public UserData CompletedByUserData { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        // collecting = 1, completed = 2, redeemed = 3, disabled = 9
        public UserCouponStatus Status { get; set; }

        [StringLength(32)]
        [Description("Unique token for this coupon used to send to the machine when redeeming the coupon. Will be created at coupon completion time.")]
        // Conditional index UIX_UserCoupon_CouponToken created in migration
        public string CouponToken { get; set; }

        // note: the ExpiryTime only is still there and set for compatibility reasons,
        //       but it does not have any effect anymore! The coupons always will expire with the "RedeemEndDate" set in the "CouponActivity" table!
        public DateTime ExpiryTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime StartTime { get; set; }

        public DateTime? CompletionTime { get; set; }

        public DateTime? RedeemTime { get; set; }

        public UserCoupon()
        {
            Type = UserCouponType.ActivityCoupon;
            StartTime = DateTime.UtcNow;
        }
    }
}

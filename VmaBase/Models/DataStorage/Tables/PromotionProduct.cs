﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     PromotionProduct Data Class
    ///     ===========================
    ///     
    ///     Data Class:  積分推廣的禮品
    ///     
    ///     Purpose: 
    ///     Stores a list of all available promotion products for points exchange
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class PromotionProduct
    {
        public long Id { get; set; }

        // unique product number for the API
        [Required]
        public int ProductNumber { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string ImageUrl { get; set; }

        [Required]
        public int Points { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int PayAdditional { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int InStock { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public PromotionProduct()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = GeneralStatusEnum.Active;
            Points = 100;
            ProductNumber = 1;
            InStock = 0;
            PayAdditional = 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     AutoCouponCriteria Class
    ///     ========================
    ///     
    ///     Data Class: Criteria and settings for auto-assigned coupons
    ///     
    ///     Purpose: 
    ///     Stores the coupon status and collected stamps (in child table UserCouponStamp)
    ///     for each participating user
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class AutoCouponCriteria
    {
        public long Id { get; set; }

        [Required]
        [StringLength(NameMaxLength)]
        public string Name { get; set; }
        public const int NameMaxLength = 64;

        // the title to be shown in the client app instead of the activity title (for the created coupons)
        [Required]
        [StringLength(CouponsTitleMaxLength)]        
        public string CouponsTitle { get; set; }
        public const int CouponsTitleMaxLength = 64;

        [StringLength(CouponsSubTitleMaxLength)]
        public string CouponsSubTitle { get; set; }
        public const int CouponsSubTitleMaxLength = 100;

        public long? ModuleId { get; set; }

        [Index("IX_UID_Status", Order = 1)]
        public long? UID { get; set; }

        [ForeignKey("UID")]
        public UserBase UserBase { get; set; }

        [ForeignKey("ModuleId")]
        public AutoCouponModule Module { get; set; }
        
        public DateTime? CouponsExpiryDate { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public bool CouponsForAllProducts { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public bool CouponsForAllMachines { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "dateadd(day, 7, getutcdate())")]
        public DateTime LimitStartTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "dateadd(day, 28, getutcdate())")]
        public DateTime LimitEndTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int CouponsLimit { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "9")]
        [Index("IX_UID_Status", Order = 2)]
        public AutoCouponCriteriaStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public AutoCouponCriteria()
        {
            CouponsForAllProducts = true;
            CouponsForAllMachines = true;
            Status = AutoCouponCriteriaStatus.Inactive;
            CouponsExpiryDate = null;
            CouponsLimit = 0;
            LimitStartTime = DateTime.UtcNow.AddDays(7);
            LimitEndTime = DateTime.UtcNow.AddDays(28);
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

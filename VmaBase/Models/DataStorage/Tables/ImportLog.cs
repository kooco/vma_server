﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Models.DataStorage.Tables;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     ImportLog Class
    ///     ================
    ///     
    ///     Data Class: Import Log Table
    ///     
    ///     Purpose: 
    ///     Stores all logs about imports ran
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class ImportLog
    {
        public long Id { get; set; }

        [Required]
        [Index("UIX_TypeRunNumber", IsUnique = true, Order = 1)]
        public ImportLogType ImportType { get; set; }

        [Required]
        [Index("UIX_TypeRunNumber", IsUnique = true, Order = 2)]
        public long RunNumber { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public ImportStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public ImportStartType StartType { get; set; }

        [Required]
        [Index("IX_StartTime")]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime StartTime { get; set; }

        public DateTime? CompletionTime { get; set; }

        // execution time in seconds
        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int ExecutionTime { get; set; }

        public ImportLog()
        {
            Status = ImportStatus.Running;
            StartType = ImportStartType.Automatic;
            StartTime = DateTime.UtcNow;
            ExecutionTime = 0;
        }
    }
}

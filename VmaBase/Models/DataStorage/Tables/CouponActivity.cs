﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     CouponActivity Class
    ///     ====================
    ///     
    ///     Data Class: Coupon-Activities
    ///     
    ///     Purpose: 
    ///     Stores the header for the coupon-type activities
    ///     This table depends on following child-tables:
    ///         CouponStampDefinition ............ all which stamps have to be collected for this coupon
    ///         ActivityParticipatingProduct ..... all products where at least one stamp can be retrieved for this copuon-activity
    ///         ActivityParticipatingMachines .... all vending machines where this activity is valid (where stamps can be collected) (if AllVendingMachines = false)
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class CouponActivity
    {
        public long Id { get; set; }

        [StringLength(64)]
        public string Title { get; set; }
        public const int TitleLength = 64;

        [StringLength(128)]
        public string ActivityText { get; set; }
        public const int ActivityTextLength = 128;

        [StringLength(255)]
        public string ActivityListImageUrl { get; set; }
        public const int ActivityListImageUrlLength = 255;

        // the product can be retrieved for free, if redeeming the coupon
        // if null = any product
        public long? ProductId { get; set; }

        [ForeignKey("ProductId")]
        public VendingProduct Product { get; set; }

        // OBSOLETE - later maybe remove this property! Currently this flag ALWAYS will be 0!
        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool AllVendingMachines { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime LimitStartTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "dateadd(month, 1, getutcdate())")]
        public DateTime LimitEndTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "dateadd(month, 1, getutcdate())")]
        public DateTime RedeemEndTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool NoStampsActivity { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public bool ShowOnPromotionMachinesList { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public bool SendingCouponsAllowed { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "90")]
        public int CouponExpiryDays { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public ActivityStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public CouponActivity()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;

            LimitStartTime = DateTime.UtcNow;
            LimitEndTime = DateTime.UtcNow.AddMonths(1);
            RedeemEndTime = DateTime.UtcNow.AddMonths(1);
            CouponExpiryDays = 90;
            AllVendingMachines = false;
            NoStampsActivity = false;
            SendingCouponsAllowed = true;
            Status = ActivityStatus.Active;
            ShowOnPromotionMachinesList = true;
        }
    }
}

﻿using System;
using System.Data.Entity.Spatial;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     VendingMachine Data Class
    ///     =========================
    ///     
    ///     Data Class: 販賣機設定檔
    ///     
    ///     Purpose: 
    ///     Stores a list of all available vending machines
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class VendingMachine
    {
        public long Id { get; set; }

        [Required]
        [StringLength(32)]
        [Index("UIX_MOCCPAssetNo", IsUnique = true)]
        public string MOCCPAssetNo { get; set; }

        [StringLength(15)]
        public string MOCCPOrganization { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }
        public const int NameMaxLength = 64;

        [Description("所在城市")]
        [MaxLength(50)]
        public string City { get; set; }
        public const int CityMaxLength = 50;

        [Required]
        [StringLength(128)]
        public string Location { get; set; }
        public const int LocationMaxLength = 128;

        [Required]
        // spatial index existing: IX_GeoLocation
        public DbGeography GeoLocation { get; set; }

        [Required]
        [Index("UIX_QRToken", IsUnique = true)]
        [Description("每5分鐘換一次Token 拿來用QRCode - every 5 minutes the token changes!")]
        [StringLength(32)]
        public string QRToken { get; set; }

        // Conditional index UIX_VendingMachine_QRTokenOld created in migration
        [Description("The last token before the current one - this still should be accepted")]
        [StringLength(32)]
        public string QRTokenOld { get; set; }

        [StringLength(128)]
        public string ImageUrl { get; set; }

        [StringLength(128)]
        public string RegistrationId { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool IsRecommended { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public VendingMachineStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public VendingMachineAppStatus AppStatus { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public VendingMachine()
        {
            ModifyTime = DateTime.UtcNow;
            CreateTime = DateTime.UtcNow; // default values unfortunately don't work in entity framework, if using entities (only in the database itself)
            IsRecommended = false;
            Status = VendingMachineStatus.NotReady;
            AppStatus = VendingMachineAppStatus.Show;
        }
    }
}

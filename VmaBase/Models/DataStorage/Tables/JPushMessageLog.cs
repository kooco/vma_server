﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     JPushMessageLog Data Class
    ///     ==========================
    ///     
    ///     Data Class: JPush Messages Log
    ///     
    ///     Purpose: 
    ///     Store results of sent jpush messages
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class JPushMessageLog
    {
        public long Id { get; set; }

        public long VendingMachineId { get; set; }

        [Required]
        [StringLength(128)]
        public string RegistrationId { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        [Index("IX_PushMessageDate", IsUnique = false)]
        public DateTime SentTime { get; set; }

        [Required]
        [StringLength(512)]
        public string Message { get; set; }
        public const int MessageMaxLength = 512;

        public long MessageId { get; set; }

        public long SendNo { get; set; }

        [StringLength(ResponseMaxLength)]
        public string Response { get; set; }
        public const int ResponseMaxLength = 512;

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public int RetryCount { get; set; }

        [Required]
        [Index("IX_PushMessageStatus", IsUnique = false)]
        public JPushMessageStatus Status { get; set; }

        public JPushMessageLog()
        {
            SentTime = DateTime.UtcNow;
            RetryCount = 0;
        }
    }
}

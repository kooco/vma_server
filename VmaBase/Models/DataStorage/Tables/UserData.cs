using System;
using System.Data.Entity.Spatial;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserData Data Class
    ///     ===================
    ///     
    ///     Data Class: 會員主表
    ///     
    ///     Purpose: 
    ///     Store profile data for the user
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserData
    {
        [Key]
        [ForeignKey("UserBase")]
        public long UID { get; set; }

        public Kooco.Framework.Models.DataStorage.Tables.UserBase UserBase { get; set; }

        [Index("UK_PhoneNrSearch7")]
        [StringLength(7)]
        public string PhoneNrSearch7 { get; set; }

        [StringLength(8)]
        // UK_MacauPassId (where not null)
        public string MacauPassId { get; set; }

        public DateTime? Birthday { get; set; }

        [Description("所在城市")]
        [MaxLength(50)]
        public string City { get; set; }
        public const int CityMaxLength = 50;

        [StringLength(128)]
        public string Location { get; set; }
        public const int LocationMaxLength = 128;

        public DbGeography GeoLocation { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "-1")]
        public GenderTypes Gender { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public double PointsTotal { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public double StampsTotal { get; set; }

        [StringLength(64)]
        public string PushToken { get; set; }
        public const int PushTokenMaxLength = 64;

        [StringLength(512)]
        public string ImageUrl { get; set; }
        public const int ImageUrlMaxLength = 512;

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public Kooco.Framework.Models.Enum.APILoginType LastLoginType { get; set; }

        public long? RecommendedByUID { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "-1")]
        public UserDataStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "0")]
        public bool IsTestUser { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public UserData()
        {
            Status = UserDataStatus._None;
            ModifyTime = DateTime.UtcNow;
            CreateTime = DateTime.UtcNow; // default values unfortunately don't work in entity framework, if using entities (only in the database itself)
            PointsTotal = 0;
            StampsTotal = 0;
            Gender = GenderTypes._None;
            IsTestUser = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     PromotionOrder Data Class
    ///     =========================
    ///     
    ///     Data Class:  積分推廣 - 禮品兌換
    ///     
    ///     Purpose: 
    ///     Stores all promotion product exchanges done by the users
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class PromotionOrder
    {
        public long Id { get; set; }

        [Required]
        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData User { get; set; }

        [Required]
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public PromotionProduct Product { get; set; }

        [Index("UIX_OrderNumber", IsUnique = true)]
        [Required]
        [StringLength(12)]
        public string OrderNumber { get; set; }

        [Required]
        public int PointsExchanged { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public PromotionOrderStatus Status { get; set; }

        [StringLength(255)]
        public string OrderNotes { get; set; }
        public const int OrderNotesMaxLength = 255;

        public PromotionOrderRefundReason RefundReason { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ExchangeTime { get; set; }

        [Required]
        public DateTime ExpiryTime { get; set; }

        public DateTime? PickupTime { get; set; }

        public DateTime? RefundTime { get; set; }

        public PromotionOrder()
        {
            Status = PromotionOrderStatus.PointsExchanged;
            ExchangeTime = DateTime.UtcNow;
            ExpiryTime = DateTime.UtcNow.AddMonths(ConfigManager.PromotionOrderExpiryInMonths);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     ActivityParticipatingMachine Class
    ///     ==================================
    ///     
    ///     Data Class: Participating vending machines for the coupon-activity
    ///     
    ///     Purpose: 
    ///     Stores a list of vending machines which are part of this coupon-activity.
    ///     At all these vending machines stamps can be collected for the coupon-activity.
    ///     References to the table
    ///         CouponActivity .... the header of the coupon-activity
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class ActivityParticipatingMachine
    {
        public long Id { get; set; }

        [Required]
        [Index("IX_Activity")]
        [Index("UIX_Activity_Machine", IsUnique = true, Order = 1)]
        public long ActivityId { get; set; }

        [ForeignKey("ActivityId")]
        public CouponActivity Activity { get; set; }

        [Required]
        [Index("UIX_Activity_Machine", IsUnique = true, Order = 2)]
        [Index("IX_VendingMachines")] // well, usually EF will create an index automatically for foreign keys, but in this case it didn't do for some reason, so we create it manually now..
        public long VendingMachineId { get; set; }

        [ForeignKey("VendingMachineId")]        
        public VendingMachine VendingMachine { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "1")]
        public GeneralStatusEnum Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public ActivityParticipatingMachine()
        {
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
            Status = GeneralStatusEnum.Active;
        }
    }
}

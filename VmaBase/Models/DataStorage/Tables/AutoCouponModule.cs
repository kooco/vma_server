﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     AutoCouponModule Class
    ///     ======================
    ///     
    ///     Data Class: Stored C# modules to be used with the AutoCouponCriteria's
    ///     
    ///     Purpose: 
    ///     Stores all C# modules with a name which can be (re)used for criterias
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class AutoCouponModule
    {
        public long Id { get; set; }

        [Required]
        [StringLength(NameMaxLength)]
        public string Name { get; set; }
        public const int NameMaxLength = 64;

        [Required]
        [StringLength(DescriptionMaxLength)]
        public string Description { get; set; }
        public const int DescriptionMaxLength = 200;

        [Required]
        [Column(TypeName = "ntext")]
        public string CsharpCode { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "9")]
        [Index("IX_Status")]
        public CSharpModuleStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        public long? CreateUserUID { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public long? ModifyUserUID { get; set; }

        public AutoCouponModule()
        {
            Status = CSharpModuleStatus.Inactive;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

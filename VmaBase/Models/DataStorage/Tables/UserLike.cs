﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserLike Data Class
    ///     =====================
    ///     
    ///     Data Class:  Likes for news and news messages
    ///     
    ///     Purpose: 
    ///     Store likes for news articles and news messages
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserLike : MaximaNewsModule.UserLikeBase
    {
        public UserLike() : base() { }
    }
}

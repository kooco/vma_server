﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     LotteryPriceCoupon Data Class
    ///     =============================
    ///     
    ///     Data Class: Lottery Price Coupons
    ///     
    ///     Purpose: 
    ///     Contains the coupons for the prices won by the users
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class LotteryPriceCoupon
    {
        public long Id { get; set; }

        [Index("UIX_PickupNumber_Game", IsUnique = true, Order = 1)]
        public long LotteryGameId { get; set; }

        [ForeignKey("LotteryGameId")]
        public LotteryGame Game { get; set; }

        public long LotteryPriceId { get; set; }

        [ForeignKey("LotteryPriceId")]
        public LotteryPrice Price { get; set; }

        public long UID { get; set; }

        [ForeignKey("UID")]
        public UserData UserData { get; set; }

        [Required]
        [StringLength(12)]
        [Index("UIX_PickupNumber_Game", IsUnique = true, Order = 2)]
        public string PickupNumber { get; set; }

        public long? ActivityCouponId { get; set; }

        [ForeignKey("ActivityCouponId")]
        public UserCoupon ActivityCoupon { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime TimeWon { get; set; }
        
        public DateTime? TimeRedeemed { get; set; }

        [SqlDefaultValue(DefaultValue = "1")]
        public LotteryPriceCouponStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public LotteryPriceCoupon()
        {
            TimeWon = DateTime.UtcNow;
            Status = LotteryPriceCouponStatus.Won;
            CreateTime = DateTime.UtcNow;
            ModifyTime = DateTime.UtcNow;
        }
    }
}

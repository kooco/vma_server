﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage.LambdaViewModels;

namespace VmaBase.Models.DataStorage
{
    internal class _LambdaViews
    {
        private ApplicationDbContext _DbContext;
        public _LambdaViews(ApplicationDbContext Context)
        {
            _DbContext = Context;
        }

        /// <summary>
        ///     LINQ UserView Statement
        /// </summary>
        public IQueryable<UserView> UserView()
        {
            return (from userbase in _DbContext.UserBases
                    join userdata in _DbContext.UserDatas on userbase.UID equals userdata.UID
                    select new UserView
                    {
                        UID = userbase.UID,
                        Phone = userbase.Phone,
                        Account = userbase.Account,
                        Name = userbase.Name,
						ImageUrl = userdata.ImageUrl
                    });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataStorage.LambdaViewModels
{
    /// <summary>
    ///     Example model for the example UserView lambda view
    /// </summary>
    public class UserView
    {
        public long UID { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
		public string ImageUrl { get; set; }
    }
}

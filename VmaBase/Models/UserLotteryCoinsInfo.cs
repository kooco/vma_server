﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models
{
    public class UserLotteryCoinsInfo
    {
        public int DailyCoins { get; set; }
        public int EarnedCoins { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models
{
    public class ImportTicket
    {
        public long Id { get; set; }

        public DateTime DateStarted { get; set; }

        public long RunNumber { get; set; }
    }
}

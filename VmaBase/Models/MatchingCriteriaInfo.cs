﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage.Tables;

namespace VmaBase.Models
{
    public class MatchingCriteriaInfo
    {
        public AutoCouponCriteria Criteria { get; set; }

        public int CouponsCount { get; set; }

        public MatchingCriteriaInfo()
        {
            Criteria = null;
            CouponsCount = 1;
        }

        public MatchingCriteriaInfo(AutoCouponCriteria parCriteria, int Count)
        {
            Criteria = parCriteria;
            CouponsCount = Count;
        }
    }
}

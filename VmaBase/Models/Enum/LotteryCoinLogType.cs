﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum LotteryCoinLogType
    {
        DailyCoinsReceive = 1,
        RecommendedFriend = 2,
        ProductBuyReceive = 3,
        SingleUseQRReceive = 4,
        DrawGame = 10,
        TestCoinsAdded = 100
    }
}

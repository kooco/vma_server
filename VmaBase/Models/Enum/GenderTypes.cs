﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum GenderTypes
    {
        [EnumDisplayName("不指定")]
        _None = -1,
        [EnumDisplayName("男性")]
        Male = 0,
        [EnumDisplayName("女性")]
        Female = 1
    }
}

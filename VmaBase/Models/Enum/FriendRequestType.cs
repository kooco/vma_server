﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum FriendRequestType
    {
        Sent = 1,
        Received = 2,
        Confirmed = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum LotteryCoinStatus
    {
        Available = 1,
        Used = 0,
        Expired = -1
    }
}

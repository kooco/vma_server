﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum ActivityStatus
    {
        Deleted = -1,
        _None = 0,

        [EnumDisplayName("Online")]
        Active = 1,

        [EnumDisplayName("活動結束")]
        Closed = 8,

        [EnumDisplayName("活動下架")]
        Inactive = 9
    }
}

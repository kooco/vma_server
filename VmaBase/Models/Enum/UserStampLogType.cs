﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum UserStampLogType
    {
        ReceivedByPurchase = 1,
        SentToFriend = 2,
        ReceivedByFriend = 3,
        ReceivedByAdmin = 4
    }
}

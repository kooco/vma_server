﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum AutoCouponUserStatus
    {
        Started = 1,
        Completed = 2,
        Cancelled = 8,
        Blocked = 9
    }
}

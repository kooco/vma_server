﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum PointsTransactionType
    {
        PromotionReward = 1,              // reward from a promotion or extra points via a promotion for buying a product
        VendingProductReward = 2,         // got points by buying a product from a vending machine with the app
        ApplessVendingProductReward = 3,  // got points by scanning the QR code after buying the product without the app   
        RegistrationReward = 4,           // got points at registration 
        ReceivedFromFriend = 5,           // got points from a friend
        PromotionProductExchange = 10,    // exchanged the points for a exchange product
        VendingProductExchange = 11,      // the points have been exchanged for a vending machine product
        PromotionProductRefund = 12,      // exchanged points get refunded by an refunded/cancelled product exchange
        SentToFriend = 15,                // sent points to a friend
        TestPointsAdded = 100,
        PointsAddedFromAdmin = 101
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum ImportStatus
    {
        Running = 0,
        Completed = 1,
        Failed = -1,
        Cancelled = 9
    }
}

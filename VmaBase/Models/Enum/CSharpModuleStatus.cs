﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum CSharpModuleStatus
    {
        [EnumDisplayName("有效")]
        Active = 1,

        [EnumDisplayName("程序代碼無效")]
        CompileError = 8,

        [EnumDisplayName("停用")]
        Inactive = 9,

        [EnumNoDisplay]
        Deleted = -1
    }
}

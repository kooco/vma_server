﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum VendingMachineStatus
    {
        [EnumDisplayName("還沒準備")]
        NotReady = 0,
        [EnumDisplayName("Online")]
        Active = 1,
        [EnumDisplayName("不可用")]
        Broken = -1,
        [EnumDisplayName("保養")]
        Maintainance = -2,
        [EnumDisplayName("賣完了")]
        SoldOut = -3,
        [EnumDisplayName("取消")]
        Removed = -4
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum VendingProductTypeOld
    {
        [EnumDisplayName("水")]
        Water = 0,
        [EnumDisplayName("茶類")]
        Tea = 1,
        [EnumDisplayName("汽水")]
        Softdrink = 2,
        [EnumDisplayName("不提供")]
        NotSpecified = 9
    }
}

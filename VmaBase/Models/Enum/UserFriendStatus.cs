﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum UserFriendStatus
    {
        Pending = 0,
        Confirmed = 1,
        Declined = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum LotteryCoinType
    {
        Earned = 0,
        Daily = 1
    }
}

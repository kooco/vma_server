﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum PushActionCode
    {
        FriendAdded = 1,
        CouponStampsReceived = 2,
        CouponReceived = 3,
        PointsReceived = 4,
        OrderCompleted = 5,
        OrderCancelled = 6,
        RecommendedFriendRegistered = 7,
        WatersportsCoinReceived = 8
    }
}

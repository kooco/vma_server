﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum ImportStartType
    {
        Automatic = 1,
        Manually = 2
    }
}

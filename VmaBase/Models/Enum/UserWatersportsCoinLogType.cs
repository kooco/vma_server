﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum UserWatersportsCoinLogType
    {
        [EnumNoDisplay]
        Deleted = -1,

        [EnumDisplayName("買活動的產品")]
        ReceivedByPurchase = 1,

        [EnumDisplayName("用了，玩遊戲")]
        UsedForPlayingGame = 2
    }
}

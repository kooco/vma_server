﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum PurchaseType
    {
        [EnumDisplayName("用手機")]
        SmartphoneApp = 1,
        [EnumDisplayName("直接用販賣機")]
        Manually = 2
    }
}

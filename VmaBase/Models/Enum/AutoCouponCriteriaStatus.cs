﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum AutoCouponCriteriaStatus
    {
        [EnumDisplayName("啟用")]
        Active = 1,

        [EnumDisplayName("所有飲料捲已被發送")]
        CouponsLimitExceeded = 6,

        // usually if the connected module is inactive, but the criteria actually was active before
        [EnumDisplayName("暫時停用")]
        TemporaryInactive = 7,

        [EnumDisplayName("無效")]
        ModuleInvalid = 8,

        [EnumDisplayName("停用")]
        Inactive = 9,

        [EnumNoDisplay]
        Deleted = -1
    }
}

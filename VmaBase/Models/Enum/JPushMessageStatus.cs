﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum JPushMessageStatus
    {
        SentSuccessfully = 1,
        FailedRetry = 0,
        SendFailed = -1,
        Exception = -2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum SingleUseQRStatus
    {
        [EnumDisplayName("可用")]
        Created = 1,

        [EnumDisplayName("用")]
        Used = 2,

        [EnumNoDisplay]
        Deleted = -1
    }
}

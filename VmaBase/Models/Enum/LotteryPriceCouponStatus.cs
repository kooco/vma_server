﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum LotteryPriceCouponStatus
    {
        [EnumDisplayName("尚未領取")]
        Won = 1,
        [EnumDisplayName("已領取")]
        PickedUp = 2,
        [EnumDisplayName("到期")]
        Expired = 9,
        [EnumDisplayName("取消")]
        Cancelled = -1
    }
}

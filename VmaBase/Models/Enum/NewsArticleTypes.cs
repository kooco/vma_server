﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    /// <summary>
    ///     Enum for article types can be used with likes and/or shares
    /// </summary>
    public enum NewsArticleTypes
    {
        NewsArticle = 1,
        NewsMessage = 2
    }
}

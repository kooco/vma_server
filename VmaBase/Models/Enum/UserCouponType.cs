﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum UserCouponType
    {
        [EnumDisplayName("活動")]
        ActivityCoupon = 1,

        [EnumDisplayName("自動發送")]
        AutoCoupon = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum LotteryGameStatus
    {
        [EnumDisplayName("未公佈")]
        Inactive = 0,
        [EnumDisplayName("準備使用")]
        Ready = 1,
        [EnumDisplayName("運行")]
        Started = 2,
        [EnumDisplayName("完成")]
        Finished = 8,
        [EnumDisplayName("停用")]
        Stopped = 9
    }
}

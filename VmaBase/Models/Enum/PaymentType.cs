﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum PaymentType
    {
        ExternalSystem = 0,
        Cash,
        CreditCard,
        Points,
        Stamps
    }
}

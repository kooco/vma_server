﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum QRActionType
    {
        _None = 0,
        AddFriend = 1,
        VendingMachineQR = 2,
        ProductStampsQR = 3,
        WatersportsQR = 4,
        WatersportsCoinReceived = 5
    }
}

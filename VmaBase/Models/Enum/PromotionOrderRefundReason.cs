﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum PromotionOrderRefundReason
    {
        [EnumDisplayName("禮品不提供")]
        ProductNotAvailable = 1,
        [EnumDisplayName("會員請求")]
        UserRequest = 2,
        [EnumDisplayName("別的原因")]
        OtherReason = 10
    }
}

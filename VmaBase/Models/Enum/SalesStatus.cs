﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum SalesStatus
    {
        CanNotSell = -1,
        SoldOut = 0,
        OnSale = 1
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum VendingMachineMessageStatus
    {
        Created = 0,
        Queried = 1,
        Timeout = -1
    }
}

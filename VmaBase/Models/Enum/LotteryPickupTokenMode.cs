﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    /// <summary>
    ///     Possible token modes for how picking up the prices, 
    ///     the users won, works
    /// </summary>
    public enum LotteryPickupTokenMode
    {
        None = 0,               // no token needed (for example using activity coupons)
        GameToken = 1,          // for picking up the price a general game token needs to be entered (secret code, entered by the staff)
                                // in this mode giving the game token to the redeem_coupno API is required!
        PriceCouponToken = 2    // for picking up the price a pickup token is used. this token does not need to be entered when redeem, but is used by the staff on pick-up to verify it's validity
                                // in this mode giving a token to the redeem_coupon API is optional. if it is given, it will be checked for validity!
    }
}

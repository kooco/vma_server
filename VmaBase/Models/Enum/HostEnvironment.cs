﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum HostEnvironment
    {
        Development = 0,
        Test = 1,
        Production = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum StatisticsItems
    {
        Users_LoginCount = 1,
        Users_NewRegistrations = 2,
        Users_UserCount = 3,
        Users_RetentionRate = 4,

        Lottery_DrawedGames = 100,
        Lottery_PlayersCount = 101,
        Lottery_WonPricesCount = 102,
        Lottery_RedeemedPricesCount = 103,

        Sales_SoldItemsCount = 200,
        Sales_SoldItemsCountUsingApp = 201,

        Activity_CollectedStampsCount = 300,
        Activity_CompletedCouponsCount = 301,
        Activity_RedeemedCouponsCount = 302,
        Activity_UsersCountCollectingStamps = 303,

        Watersports_DrawedGames = 400,
        Watersports_PlayersCount = 401,
        Watersports_WonPricesCount = 402,
        Watersports_RedeemedPricesCount = 403,
    }

    public enum CustomStatisticItems
    {
        Users_RetentionRates = 1 // only last day, last 3 days, last 7 days and last 30 days
    }
}

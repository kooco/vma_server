﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum CouponStampStatus
    {
        NotCollected = 0,
        Collected = 1
    }
}

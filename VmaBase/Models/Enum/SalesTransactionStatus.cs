﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum SalesTransactionStatus
    {
        [EnumDisplayName("剛開始")]
        Initiated = 0,
        [EnumDisplayName("買完")]
        Completed = 1,
        [EnumDisplayName("收到印花/積分")]
        StampsCollected = 2,
        [EnumDisplayName("取消")]
        Cancelled = -1
    }
}

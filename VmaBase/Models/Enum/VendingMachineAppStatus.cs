﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum VendingMachineAppStatus
    {
        [EnumDisplayName("顯示")]
        Show = 1,
        [EnumDisplayName("不顯示")]
        Hide = 0,
        [EnumDisplayName("測試會員用")]
        TestUsersOnly = 2
    }
}

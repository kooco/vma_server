﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum PromotionOrderStatus
    {
        [EnumDisplayName("兌換")]
        PointsExchanged = 1,
        [EnumDisplayName("已領取")]
        PickedUp = 2,
        [EnumDisplayName("到期了")]
        Expired = 3,
        [EnumDisplayName("退換")]
        Refund = 4
    }
}

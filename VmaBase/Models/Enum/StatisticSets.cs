﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum StatisticSets
    {
        Users = 1,
        Lottery = 2,
        Sales = 3,
        Activity = 4
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum UserCouponStatus
    {
        [EnumDisplayName("在集印花")]
        Collecting = 1,
        [EnumDisplayName("完成")]
        Completed = 2,
        [EnumDisplayName("兌換了")]
        Redeemed = 3,
        [EnumDisplayName("停用")]
        Disabled = 9
    }
}

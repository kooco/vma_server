﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum VendingMachineMessageType
    {
        ProductChosen = 10,
        RedeemCoupon = 11,
        TransactionFinished = 20
    }
}

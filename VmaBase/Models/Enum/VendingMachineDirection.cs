﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum VendingMachineDirection
    {
        Outgoing = 0,
        Incoming = 1
    }
}

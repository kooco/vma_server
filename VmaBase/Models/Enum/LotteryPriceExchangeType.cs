﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum LotteryPriceExchangeType // 兌換地點
    {
        _None = 0,
        [EnumDisplayName("客服中心")]
        PickupProduct = 1,
        [EnumDisplayName("販賣機")]
        VendingMachineProduct = 2
    }
}

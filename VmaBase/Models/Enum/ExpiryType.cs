﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum ExpiryType
    {
        [EnumDisplayName(DisplayName: "接收日期的一天結束")]
        EndOfIssueDay = 0,
        [EnumDisplayName(DisplayName: "接收日期的一周結束")]
        EndOfIssueWeek = 1,
        [EnumDisplayName(DisplayName: "接收日期的一月結束")]
        EndOfIssueMonth = 2,
        [EnumDisplayName(DisplayName: "接收日期x天後")]
        DaysAfterIssue = 10,
        [EnumDisplayName(DisplayName: "接收日期x星期後")]
        WeeksAfterIssue = 11,
        [EnumDisplayName(DisplayName: "接收日期x月後")]
        MonthsAfterIssue = 12
    }
}

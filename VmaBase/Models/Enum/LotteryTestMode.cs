﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum LotteryTestMode
    {
        Off = 0,                    // productive mode
        GenericTestMode = 1,        // general test mode. the chance to win is equal to all items.
        RegurarelyWinTestMode = 2   // extended test mode. there is a more high chance to also win "rare" items.
    }
}

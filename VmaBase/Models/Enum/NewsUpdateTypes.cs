﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.Enum
{
    public enum NewsUpdateTypes
    {
        Add = 1,
        Modify = 2,
        Delete = 3
    }
}

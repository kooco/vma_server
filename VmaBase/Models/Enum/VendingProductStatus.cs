﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.Enum
{
    public enum VendingProductStatus
    {
        Deleted = -1,
        [EnumDisplayName("提供")]
        Available = 1,
        [EnumDisplayName("不提供")]
        NotAvailable = 9
    }
}

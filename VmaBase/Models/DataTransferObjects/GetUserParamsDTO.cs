﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetUserParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "userId")]
        [SwaggerDescription(Description = "[green](fastest) User Id to [b]search[/b] for[/green]", Example = "#int:1")]
        public long? UID { get; set; }

        [JsonProperty(PropertyName = "account")]
        [SwaggerDescription(Description = "Account name to search for", Example = "TestUser")]
        public string Account { get; set; }

        [JsonProperty(PropertyName = "socialLoginType")]
        [SwaggerDescription(Description = "If you want to get the user profile information with the userToken, then you need to set the social login type here.\nWeChat = 1,Facebook = 2,Google = 3,Twitter = 4,QQ = 5", Example = "#null")]
        public SocialLoginTypes SocialLoginType { get; set; }

        [JsonProperty(PropertyName = "userToken")]
        [SwaggerDescription(Description = "If you want to search the user by his user token, then you can do that with setting this 'userToken' parameter together with 'socialLoginType'", Example = "#null")]
        public string UserToken { get; set; }

        [JsonProperty(PropertyName = "phone")]
        [SwaggerDescription(Description = "You can also use the phone number for getting the user profile information.\nThe phone number must match exactly the phone number given at registration!", Example = "#null")]
        public string Phone { get; set; }
    }
}

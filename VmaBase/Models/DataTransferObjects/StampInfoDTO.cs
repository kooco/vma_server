﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class StampInfoDTO
    {
        [JsonProperty(PropertyName = "stampTypeNumber")]
        [SwaggerDescription(Description = "Unique number for this stamp type. This number is also used for building the image filename", Example = "01")]
        public string StampTypeNumber { get; set; }

        [JsonProperty(PropertyName = "stampTypeName")]
        [SwaggerDescription(Description = "Name of the stamp type", Example = "#int:1")]
        public string StampTypeName { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Url of the image for the stamp type", Example = "#null")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "amount")]
        [SwaggerDescription(Description = "Amount of stamps for this type", Example = "#int:1")]
        public int Amount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Models.Enum;
using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class CheckAppVersionParamsDTO
    {
        [JsonProperty(PropertyName = "appVersion")]
        [SwaggerDescription(Description = "The version number of the calling client app", Example = "1.2", Required = true)]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "mobileDeviceType")]
        [SwaggerDescription(Description = "The type of mobile device used.\n1 = Android, 2 = iOS", Example = "#int:1", Required = true)]
        public MobileDeviceType DeviceType { get; set; }
    }
}

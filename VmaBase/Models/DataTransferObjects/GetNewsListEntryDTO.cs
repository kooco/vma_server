﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataTransferObjects
{
    public class GetNewsListEntryDTO : MaximaNewsModule.GetNewsListEntryBaseDTO
    {
        [JsonProperty(PropertyName = "onSubscription")]
        [HideInDocs]
        public new bool OnSubscription { get; set; }

        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity (coupon) bound to this news entry.", Example = "#null")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "detailImageUrl", NullValueHandling = NullValueHandling.Ignore)]
        [HideInDocs]
        public string DetailImageUrl { get; set; }

        public ActivityStatus? ActivityStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class NewsDetailsDTO : MaximaNewsModule.NewsDetailsBaseDTO
    {
        [JsonProperty(PropertyName = "listImageUrl", NullValueHandling = NullValueHandling.Ignore)]
        [HideInDocs]
        public string ListImageUrl { get; set; }

        [JsonProperty(PropertyName = "activityId", NullValueHandling = NullValueHandling.Ignore)]
        [HideInDocs]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "activityStatus")]
        [HideInDocs]
        public ActivityStatus ActivityStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using Newtonsoft.Json;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class AddStampsApiResultDTO 
    {
        [JsonProperty(PropertyName = "activity")]
        [SwaggerDescription(Description = "If the stamp has been added successfully, then this field will contain the coupon info with the list of collected (and to be collected) stamps for the activity the stamp has been added to")]
        public CouponActivityDTO Activity { get; set; }

        [JsonProperty(PropertyName = "couponSelectionList")]
        [JsonIgnore]
        [SwaggerDescription(Description = "If the scanned stamp QR can be used for more than one activity, then a list of matching activities will be returned in this field with the ReturnCode 3 (Operation Pending), so that the user can select to which activity to add the stamp to")]
        public List<CouponSelectionEntryDTO> CouponSelectionList { get; set; }

        [JsonProperty(PropertyName = "pointsAdded")]
        [SwaggerDescription(Description = "Returns the amoint of points added with this QR code. If no points have been added then 'pointsAdded' will be 0", Example = "#int:200")]
        public int PointsAdded { get; set; }

        [JsonProperty(PropertyName = "stampsAdded")]
        [SwaggerDescription(Description = "Returns the amount of stamps that have been added with this QR code. If no stamps have been added the 'stampsAdded' will be 0", Example = "#int:1")]
        public int StampsAdded { get; set; }

        [JsonProperty(PropertyName = "watersportsCoinsAdded")]
        [SwaggerDescription(Description = "Returns the amount of watersports coins that have been added with this QR code. If no coins have been added, the 'watersportsCoinsAdded' will be 0", Example = "#int:0")]
        public int WatersportsCoinsAdded { get; set; }
    }
}

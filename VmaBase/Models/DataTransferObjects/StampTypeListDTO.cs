﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class StampTypeListDTO
    {
        [JsonProperty(PropertyName = "stampTypes")]
        [SwaggerDescription(Description = "Contains a list of all currently available stamp types.")]
        public List<StampTypeDTO> StampTypes { get; set; }

        [JsonProperty(PropertyName = "stampTypesCount")]
        [SwaggerDescription(Description = "The amount of stamp tpyes in the list")]
        public int StampTypesCount { get; set; }

        [JsonProperty(PropertyName = "baseImageUrl")]
        [SwaggerDescription(Description = "Contains the base url for every stamp type image.\nThe image url of the stickers will be built in the following way then:\nBaseUrl/stamptype_##.jpg\n## = Field 'number' of the stamp type.", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/")]
        public string BaseImageUrl { get; set; }
    }
}

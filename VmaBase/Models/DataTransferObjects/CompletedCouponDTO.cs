﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class CompletedCouponDTO : CouponDTO
    {
        [JsonIgnore]
        public long UID { get; set; }

        [JsonProperty(PropertyName = "receivedByFriend")]
        [SwaggerDescription(Description = "true = this (completed) coupon has been received as a gift by a friend user.\nfalse = this coupon is collected by the user him/herself", Example = "#bool:false")]
        public bool ReceivedByFriend { get; set; }

        [JsonProperty(PropertyName = "receivedByUID")]
        [SwaggerDescription(Description = "The Id of the user this coupon has been sent from, if this coupon is a gift from another user", Example = "#null")]
        public long? ReceivedByUID { get; set; }

        [JsonProperty(PropertyName = "receivedByAccount")]
        [SwaggerDescription(Description = "The account name (account id) of the user this coupon has been sent from, if this coupon is a gift from another user", Example = "#null")]
        public string ReceivedByAccount { get; set; }

        [JsonProperty(PropertyName = "receivedByNickname")]
        [SwaggerDescription(Description = "The nickname of the user this coupon has been sent from, if this coupon is a gift from another user", Example = "#null")]
        public string ReceivedByNickname { get; set; }

        [JsonProperty(PropertyName = "startTime")]
        [SwaggerDescription(Description = "Date+time the user began to collect stamps for this coupon")]
        public DateTime StartTime { get; set; }

        [JsonProperty(PropertyName = "completionTime")]
        [SwaggerDescription(Description = "Date+time the user has completed collecting all stamps for this coupon")]
        public DateTime CompletionTime { get; set; }

        [JsonProperty(PropertyName = "stamps", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "IF 'includeStaps' is set to true, then this property will contain a list of stamps collected for this coupon.\nThis field will ONLY be returned if 'includeStamps' is set to true!")]
        public List<StampTypeDTO> Stamps { get; set; }

        [JsonProperty(PropertyName = "sendAllowed")]
        [SwaggerDescription(Description = "true = sending coupons is allowed for this activity.\nfalse = sending coupons to friends is NOT allowed for this activity", Example = "#bool:true")]
        public bool SendAllowed { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class TestAddLotteryCoinsParamsDTO : BaseAPIParamsDTO
    {
        [JsonProperty(PropertyName = "lotteryGameId")]
        [SwaggerDescription(Description = "Id of the lottery game to add coins for", Example = "#int:2")]
        public long LotteryGameId { get; set; }

        [JsonProperty(PropertyName = "userId")]
        [SwaggerDescription(Description = "The Id of the user to add the lottery coins for", Example = "#int:6")]
        public long UID { get; set; }

        [JsonProperty(PropertyName = "coins")]
        [SwaggerDescription(Description = "The amount of (earned) lottery coins to add", Example = "#int:5")]
        public int Coins { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetVendingMachinesParamsDTO : BaseAPIAuthPagingParamsDTO
    {
        [JsonProperty(PropertyName = "onlyActive", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "true (Default) = return only currently useable (available) vending machines\nfalse = also return vending machines which are currently not available", Example = "#bool:true")]
        public bool? OnlyActive { get; set; }

        [JsonProperty(PropertyName = "onlyRecommended", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "true = return only recommended vending machines\nfalse (Default) = also return vending machines which are not explicitelly recommended", Example = "#bool:false")]
        public bool? OnlyRecommended { get; set; }

        [JsonProperty(PropertyName = "activityId", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "OPTIONAL. CAN be omitted!\nIf set: activityId to filter the vending machines for.\nOnly vending machines will be returned then, which are part of this activity.\nnull = do NOT filter the machines by activity.", Example = "#null")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "criteriaId", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "OPTIONAL. CAN be omitted!\nIf set: criteriaId to filter the vending machines for.\nOnly vending machines will be returned then, which are part of this auto-coupon rule/criteria.\nnull = do NOT filter the machines by auto-coupon criteria/rule.", Example = "#null")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "latitude", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The geo location (position) - latitude part - of the position from where to search for the vending machines.\nIf the parameters 'latitude', 'longitude' and 'radiusMeters' are not set or set to null then all vending machines within 100km will be returned!", Example = "#double:22.158822")]
        public decimal? Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The geo location (position) - longitude part - of the position from where to search for the vending machines.\nIf the parameters 'latitude', 'longitude' and 'radiusMeters' are not set or set to null then all vending machines within 100km will be returned!", Example = "#double:113.550127")]
        public decimal? Longitude { get; set; }

        [JsonProperty(PropertyName = "radiusMeters", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The radius in meters around the given position (latitude + longitude) to search for the machines.\nIf 'latitude', 'longitude' AND 'radiusMeters' are correctly set, AND 'radiusMeters' is bigger than 0,\nthen only vending machines around the given position within the 'radiusMeters' radius are returned by this API", Example = "#int:500")]
        public long? RadiusMeters { get; set; }
    }
}

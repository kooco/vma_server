﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class AppUserAddCoinsAdminParamsDTO
    {
        public long UID { get; set; }
        public int CoinsToAdd { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Providers;

namespace VmaBase.Models.DataTransferObjects
{
    public class BaseAPIAuthPagingParamsDTO : Kooco.Framework.Models.DataTransferObjects.Input.BaseAuthPagingParamsDTO<ApplicationDbContext>
    {
        public BaseResponseDTO CheckPagingAndAuthorization(BaseProvider provider)
        {
            return new BaseResponseDTO(base.CheckPagingAndAuthorization(provider));
        }

        public BaseResponseDTO CheckAppAuthorization(BaseProvider provider)
        {
            return new BaseResponseDTO(base.CheckAppAuthorization(provider));
        }
    }
}

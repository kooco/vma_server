﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.DataStorage;


namespace VmaBase.Models.DataTransferObjects
{
    public class SearchUsersByPhoneParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "phoneNumbers")]
        [SwaggerDescription(Description = "A list of phone numbers to search for.")]
        public List<string> PhoneNumbers { get; set; }
    }
}

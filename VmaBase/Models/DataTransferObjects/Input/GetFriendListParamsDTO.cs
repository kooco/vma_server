﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetFriendListParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "recentAlso", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "true = return also recently added friends.\nfalse (Default) = do NOT return recently added friends.\nThis is to match the get_friendrequests_pending API.\nIn get_friendrequests_pending usually all already confirmed friends will be returned which have been added within the last 7 days, so [u]this[/u] API will usually return no friends added within the last 7 days.", Example = "#null")]
        public bool? RecentAlso { get; set; }
    }
}

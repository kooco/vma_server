﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetCompletedCouponsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the coupon (activity) to filter for.\nnull (or omit) = return ALL useable coupons, no matter from which activity.", Example = "#null")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "criteriaId")]
        [SwaggerDescription(Description = "Id of the coupon (auto-coupon criteria/rule) to filter for.\nnull (or omit) = return ALL useable coupons, no matter from which criteria/rule.", Example = "#null")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "includeStamps")]
        [SwaggerDescription(Description = "true = also include a list of stamps collected for this completed coupons.\nfalse (Default) = do not return any stamps, only the coupon", Example = "#bool:false")]
        public bool IncludeStamps { get; set; }
    }
}

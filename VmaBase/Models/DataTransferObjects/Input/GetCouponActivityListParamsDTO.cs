﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetCouponActivityListParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty("onlyCompleted")]
        [SwaggerDescription(Description = "true = return only activities where the user has at least one completed coupon (totalAmount > 0)\nfalse (Default) = return also activities where the user so far don't have any completed coupon (totalAmount = 0)", Example = "#bool:false")]
        public bool? OnlyCompleted { get; set; }

        [JsonProperty("forPromotionMachinesList")]
        [SwaggerDescription(Description = "true = return only activities which are shown on the promotion machines list (顯示在販賣機地圖的推廣頁面的活動)\nfalse (Default) = also return activities which are not shown on the promotion machines list", Example = "#bool:false")]
        public bool? ForPromotionMachinesList { get; set; }

        [JsonProperty("vendingMachineId")]
        [SwaggerDescription(Description = "Id of the vending machine to filter for. Default = null. CAN be omitted!\nIf set: only activitie's are returned which are having this vending machine as part of the activity.\nnull or ommit = do not filter for the vending machine", Example = "#int:2")]
        public long? VendingMachineId { get; set; }
    }
}

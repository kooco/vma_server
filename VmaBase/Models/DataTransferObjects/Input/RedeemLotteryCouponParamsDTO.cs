﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class RedeemLotteryCouponParamsDTO : BaseLotteryParamsDTO
    {
        [JsonProperty(PropertyName = "vendingMachineId")]
        [SwaggerDescription(Description = "Id of the vending machine which should drop the product (only necessary, if the price is a vending machine product)", Example = "#int:22")]
        public long VendingMachineId { get; set; }

        [JsonProperty(PropertyName = "lotteryPriceId")]
        [SwaggerDescription(Description = "Id of the lottery price a won coupon should be used for dropping the won product (mandatory!) at the vending machine or for confirming the pickup for a non-vending-machine price", Example = "#int:1")]
        public long LotteryPriceId { get; set; }

        [JsonProperty(PropertyName = "token")]
        [SwaggerDescription(Description = "Token for pick up. REQUIRED for the watersports game! Optional for the standard lottery game.", Required = false, Example = "874569")]
        public string Token { get; set; }

        [JsonProperty(PropertyName = "productId")]
        [SwaggerDescription(Description = "Id of the selected product to redeem the price for. Only necessary if the price is a vending machine product and the lottery game does not have a fixed product defined for this price.\nFor the watersports game its necessary for the drink coupon price!", Required = false, Example = "#null")]
        public long ProductId { get; set; }
    }
}

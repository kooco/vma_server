﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetUncompletedCouponsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "firstActivityId")]
        [SwaggerDescription(Description = "Id of the coupon (activity) which should be first in the list and in any case should be included.\nThis activity will also be included in the list if it's already completed or redeemed!\nCan be null, if it all doesn't matter :)", Example = "#int:200")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "noStamps")]
        [SwaggerDescription(Description = "true = do not include stamps in the coupon list. (can increase performance)\nfalse (Default) = also include stamps in the coupon list", Example = "#bool:false")]
        public bool? NoStamps { get; set; }
    }
}

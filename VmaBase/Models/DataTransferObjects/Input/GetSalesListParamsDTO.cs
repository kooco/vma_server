﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetSalesListParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty("onlyAvailableProducts")]
        [SwaggerDescription(Description = "true (Default) = return only products which are currently available\nfalse = return also products which are sold out", Example = "#bool:true")]
        public bool? OnlyAvailableProducts { get; set; }

        [JsonProperty("includeImages")]
        [SwaggerDescription(Description = "true (Default) = also return the image urls for the product images.\nfalse = do not return the image urls. This can reduce the response size.", Example = "#bool:true")]
        public bool? IncludeImages { get; set; }

        [JsonProperty("vendingMachineId")]
        [SwaggerDescription(Description = "Id of the vending machine to get the sales list for", Example = "#int:2")]
        public long VendingMachineId { get; set; }

        [JsonProperty("activityId")]
        [SwaggerDescription(Description = "Id of the activity to filter the products list for. Default = NULL.\nIf set: only products are returned, which are part of the given activity.\nnull or ommit: do not filter the activity", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty("criteriaId")]
        [SwaggerDescription(Description = "Id of the criteria (auto-coupon rule) to filter the products list for. Default = NULL.\nIf set: only products are returned, which are allowed to use with a coupon of this criteria/rule.", Example = "#int:1")]
        public long? CriteriaId { get; set; }

        [JsonProperty("forRedeem")]
        [SwaggerDescription(Description = "true = filter the returned products list by only products that can be redeemed for the given activity\nfalse (Default) = don't filter by redeemable or not", Example = "#bool:false")]
        public bool? ForRedeem { get; set; }

        [JsonProperty("lotteryPriceId")]
        [SwaggerDescription(Description = "If set, then the sales list is filtered for products that can be redeemed with that given price. If the given price is not a vending machine price, no filter will be applied!\nNote: Don't confuse it with 'forRedeem'!\n'forRedeem' will filter products that are generally defined as redeemable FOR THE ACTIVITY,\nwhile 'lotteryPriceId' will filter products that are defined as redeemable FOR THIS PRICE.\n'forRedeem' and 'lotteryPriceId' can be combined.")]
        public long? LotteryPriceId { get; set; }
    }
}

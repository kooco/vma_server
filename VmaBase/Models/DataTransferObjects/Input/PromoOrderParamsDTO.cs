﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromoOrderParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "productId")]
        [SwaggerDescription(Description = "Id of the product to exchange the points for", Example = "#int:1")]
        public long ProductId { get; set; }
    }
}

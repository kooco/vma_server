﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class TestAddPointsParamsDTO : BaseAPIParamsDTO
    {
        [JsonProperty(PropertyName = "userId")]
        [SwaggerDescription(Description = "The Id of the user to add the lottery coins for", Example = "#int:6")]
        public long UID { get; set; }

        [JsonProperty(PropertyName = "points")]
        [SwaggerDescription(Description = "The amount of points to add to the user account", Example = "#int:5000")]
        public int Points { get; set; }
    }
}

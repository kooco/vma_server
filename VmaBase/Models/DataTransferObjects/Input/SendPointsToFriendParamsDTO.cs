﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class SendPointsToFriendParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "friendUserId")]
        [SwaggerDescription(Description = "(Preferred) User id of the friend who should receive the points.\nOnly one of the parameters 'friendUserId' or 'friendAccount' is needed! Recommended is 'friendUserId'.", Example = "#int:4")]
        public long? FriendUID { get; set; }

        [JsonProperty(PropertyName = "friendAccount")]
        [SwaggerDescription(Description = "(Second choice) User account (account id) of the friend who should receive the points.\nOnly one of the parameters 'friendUserId' or 'friendAccount' is needed! Recommended is 'friendUserId'.", Example = "#null")]
        public string FriendAccount { get; set; }

        [JsonProperty(PropertyName = "points")]
        [SwaggerDescription(Description = "Amount of points to send to the friend", Example = "#int:200")]
        public int Points { get; set; }
    }
}

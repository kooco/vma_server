﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class AddStampToUserAdminParamsDTO
    {
        public long UID { get; set; }

        public long ActivityId { get; set; }

        public long StampTypeId { get; set; }
    }
}

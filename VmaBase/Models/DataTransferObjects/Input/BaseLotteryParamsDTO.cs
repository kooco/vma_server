﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class BaseLotteryParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "lotteryGameId")]
        [SwaggerDescription(Description = "CAN BE OMITTED for the food festival!\nIf only one lottery game exists, then this parameter is NOT necessary!\nIf more than one lottery game existing in the same time: the id of the lottery game to retrieve the data for or draw the game.", Example = "#int:2")]
        public long? LotteryGameId { get; set; }

        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "CAN BE OMITTED for the food festival!\nIf only one lottery game exists, then this parameter is NOT necessary!\nIf more than one lottery game existing in the same time: the id of the activity to retrieve the data or draw the game for. Only 'lotteryGameId' OR 'activityId' is needed then.", Example = "#null")]
        public long? ActivityId { get; set; }
    }
}

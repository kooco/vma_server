﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class QRActionParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "qrToken")]
        [SwaggerDescription(Description = "QR token to execute the action for.\nThe QR token is the string contained in the scanned QR code", Example = "USER_283473894", Required = true)]
        public string QRToken { get; set; }

        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity (mainly used for redeeming coupons). If a machine QR code is passed, then the Api will also return, if the machine is part of this activity or not", Example = "#null")]
        public long? ActivityId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class TestResetLotteryCouponsParamsDTO : BaseAPIParamsDTO
    {
        [JsonProperty(PropertyName = "lotteryGameId")]
        [SwaggerDescription(Description = "Id of the lottery game to reset the coupons for.", Required = false, Example = "#int:2")]
        public long LotteryGameId { get; set; }

        [JsonProperty(PropertyName = "userId")]
        [SwaggerDescription(Description = "Id of the user to reset (DELETE!) the lottery coupons for", Example = "#int:0")]
        public long UID { get; set; }
    }
}

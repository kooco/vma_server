﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetExchangedProductsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "maxAgeDays")]
        [SwaggerDescription(Description = "Maximum in days for already picked up products which should be in the list.\nAll already picked up orders which are more old than this given amount of days (the pick up date is used!) will not be returned anymore.", Example = "#int:90")]
        public int? MaxAgeDays { get; set; }

        [JsonProperty(PropertyName = "includePickedUp")]
        [SwaggerDescription(Description = "true (Default) = also include orders, which already have been completed (picked up)\nfalse = do NOT include any already completed (picked up) order", Example = "#bool:true")]
        public bool? IncludePickedUp { get; set; }
    }
}

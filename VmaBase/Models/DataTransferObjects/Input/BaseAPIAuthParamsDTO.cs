using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage;
using Kooco.Framework.Models.Interfaces;
using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.DataTransferObjects.Response;


namespace VmaBase.Models.DataTransferObjects
{    
    public class BaseAPIAuthParamsDTO : BaseAPIAuthParamsDTO<ApplicationDbContext>
    {
        #region SessionToken (override)
        [JsonProperty(PropertyName = "sessionToken")]
        [SwaggerDescription(Description = "sessionToken retrieved by the user_login API", Example = "exb3b6bb-1875-4a1f-8c5c-ee3ac150a7db", InputOnly = true)]
        public new string SessionToken
        {
            get
            {
                return base.SessionToken;
            }
            set
            {
                base.SessionToken = value;
            }
        }
        #endregion

        // possible return codes: 1,-10001,-10200,-10203
        public new BaseResponseDTO CheckAppAuthorization(IMaximaProvider Provider)
        {
            return new BaseResponseDTO(base.CheckAppAuthorization(Provider));
        }
    }
}

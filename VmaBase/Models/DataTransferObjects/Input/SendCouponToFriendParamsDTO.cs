﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class SendCouponToFriendParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty("activityId")]
        [SwaggerDescription(Description = "Id of the coupon (=activity) to send to a friend user", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty("criteriaId")]
        [SwaggerDescription(Description = "Id of the auto-coupon criteria (rule) to send a coupon from, to a friend user", Example = "#int:1")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "userIdReceiver")]
        [SwaggerDescription(Description = "UserId of the User who shall receive the coupon sent.\nYou need to set either 'userIdReceiver' OR 'userAccountReceiver'.", Example = "#int:6")]
        public long? UIDReceiver { get; set; }

        [JsonProperty(PropertyName = "userAccountReceiver")]
        [SwaggerDescription(Description = "Account id (name) of the user who shall receive the coupon sent.\nYou need to set either 'userIdReceiver' OR 'userAccountReceiver'.", Example = "1218892")]
        public string AccountReceiver { get; set; }
    }
}

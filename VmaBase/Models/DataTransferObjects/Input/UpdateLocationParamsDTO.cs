﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class UpdateLocationParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "latitude")]
        [SwaggerDescription(Description = "Latitude - coordinate part 'Latitude' of the current user's position.\nInput: passing this parameter as a string also will be accepted!", Example = "22.158242")]
        public decimal? Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        [SwaggerDescription(Description = "Longitude - coordinate part 'Longitude' of the current user's position\nInput: passing this parameter as a string also will be accepted!", Example = "113.549457")]
        public decimal? Longitude { get; set; }
    }
}

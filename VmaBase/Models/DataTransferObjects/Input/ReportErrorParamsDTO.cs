﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using Newtonsoft.Json;

namespace VmaBase.Models.DataTransferObjects.Input
{
    public class ReportErrorParamsDTO
    {
        [JsonProperty(PropertyName = "appVersion")]
        [SwaggerDescription(Description = "Version info. Which app version does the client have installed?", Example = "1.0")]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "userInfo")]
        [SwaggerDescription(Description = "Please send the user-id, phone-number or wechat or facebook token here, to identify the user, who tried to login or was logged in.", Example = "+886123456789")]
        public string UserInfo { get; set; }

        [JsonProperty(PropertyName = "description")]
        [SwaggerDescription(Description = "If the user entered an error description (like for example: what did not work? what did he do?), please send this description in this field.", Example = "Login failed. Pressed facebook login button several times, but can not login..")]
        public string Description { get; set; }

        [SwaggerDescription(Description = "Send all data to be logged here. Like the client log data. All infos that can help to identify the error", Example = "technical error log data")]
        public string ReportData { get; set; }
    }
}

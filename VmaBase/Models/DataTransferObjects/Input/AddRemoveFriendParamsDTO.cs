﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class AddRemoveFriendParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty("accountFriend")]
        [SwaggerDescription(Description = "Account name of the friend user to be added/removed/accepted/declined\nEither 'userIdFriend' OR 'accountFriend' must be set.", Example = "TestUser")]
        public string AccountFriend { get; set; }

        [JsonProperty("userIdFriend")]
        [SwaggerDescription(Description = "[color:green] (fastest) User Id of the friend user to be added/removed/accepted/declined.\nEither 'userIdFriend' OR 'accountFriend' must be set.[/color]", Example = "#int:1")]
        public long? UIDFriend { get; set; }
    }
}

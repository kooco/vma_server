﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class TestRespreadLotteryPricesParamsDTO : BaseAPIParamsDTO
    {
        [JsonProperty(PropertyName = "lotteryGameId")]
        [SwaggerDescription(Description = "Id of the lottery game to respread the prices for", Required = false, Example = "#int:2")]
        public long LotteryGameId { get; set; }

        [JsonProperty(PropertyName = "alsoResetPriceAvailability")]
        [SwaggerDescription(Description = "true = also reset the price availabilities. Means, that the counter for the amount of prices already won also will be reset to 0, so that all prices are available again.\nNote: this will not reset/delete the already won coupons the users have! To delete the coupons use [url:api/vma/lottery/test_reset_lottery_coupons]test_reset_lottery_coupons[/url]", Example = "#bool:true")]
        public bool AlsoResetPriceAvailability { get; set; }
    }
}

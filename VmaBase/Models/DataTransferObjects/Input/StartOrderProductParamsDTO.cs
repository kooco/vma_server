﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class StartOrderProductParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "VendingMachineId")]
        [SwaggerDescription(Description = "The id of the vending machine to start the product order from", Example = "#int:20")]
        public long VendingMachineId { get; set; }

        [JsonProperty(PropertyName = "ProductId")]
        [SwaggerDescription(Description = "The id of the product to start the order from", Example = "#int:6")]
        public long ProductId { get; set; }
    }
}

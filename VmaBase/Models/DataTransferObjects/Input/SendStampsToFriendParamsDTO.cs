﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class SendStampsToFriendParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity (coupon) to sent stamps to/from", Example = "#int:1")]
        public long ActivityId { get; set; }

        [JsonProperty(PropertyName = "userIdReceiver")]
        [SwaggerDescription(Description = "UserId of the User who shall receive the stamps sent.\nYou need to set either 'userIdReceiver' OR 'userAccountReceiver'.", Example = "#int:6")]
        public long? UIDReceiver { get; set; }

        [JsonProperty(PropertyName = "userAccountReceiver")]
        [SwaggerDescription(Description = "Account id (name) of the user who shall receive the stamps sent.\nYou need to set either 'userIdReceiver' OR 'userAccountReceiver'.", Example = "1218892")]
        public string AccountReceiver { get; set; }

        [JsonProperty(PropertyName = "stamps")]
        [SwaggerDescription(Description = "Contains a list of stamps to send to the friend.\nIt's a list of stamp type 'number's.\nThe same stamp type can occur also more than one time in the list, if the user wants to send more than 1 sticker of the same type, or you set the 'amount' field.")]
        public List<SendStampTypeDTO> Stamps { get; set; }
    }
}

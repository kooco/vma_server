﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetLotteryCouponListParamsDTO : BaseLotteryParamsDTO
    {
        [JsonProperty(PropertyName = "onlyUsable")]
        [SwaggerDescription(Description = "true (Default) = only return the coupons that still can be redemmed.\nfalse = return also already redeemed coupons (count)", Example = "#bool:false")]
        public bool? OnlyUsable { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class AddStampsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "qrtoken")]
        [SwaggerDescription(Description = "QR token of the scanned stamps QR code. This will identify the sales transaction the stamps belong to.", Example = "#null")]
        public string QRToken { get; set; }

        [HideInDocs]
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity this stamp QR should be used for. If this activity is set, the API will return an error if this activity id does not match the QR code scanned.\nCurrently this parameter is REQUIRED", Example = "#null")]
        public long? ActivityId { get; set; }

        public AddStampsParamsDTO()
        {
            ActivityId = null;
        }
    }
}

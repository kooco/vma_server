﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class SearchNewsParamsDTO : MaximaNewsModule.SearchNewsParamsBaseDTO
    {
        [JsonProperty(PropertyName = "tags")]
        [HideInDocs]
        public new List<string> Tags { get; set; }

        [JsonProperty(PropertyName = "onlySubscribed")]
        [HideInDocs]
        public new bool? OnlySubscribed { get; set; }

        [JsonProperty(PropertyName = "category")]
        [SwaggerDescription(Description = "News Category (ACT (Activity), DSC (Discount))", Example = "ACT")]
        public new string Category { get; set; }
    }
}

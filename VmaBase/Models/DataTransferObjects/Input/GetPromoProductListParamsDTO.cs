﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetPromoProductListParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "alsoNotAvailable")]
        [SwaggerDescription(Description = "Flag, if also not available promotion products should be returned.\ntrue = return also products which are not available\nfalse (Default) = return only available products", Example = "#bool:false")]
        public bool? AlsoNotAvailable { get; set; }
    }
}

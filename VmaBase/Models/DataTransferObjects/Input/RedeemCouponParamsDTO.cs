﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class RedeemCouponParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "The id of the activity to redeem a completed coupon for to get a free drink.\nEither 'ActivityId' or 'CriteriaId' must be set!", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "criteriaId")]
        [SwaggerDescription(Description = "The id of the auto-coupon criteria (rule) to redeem a completed coupon for to get a free drink\nEither 'ActivityId' or 'CriteriaId' must be set!", Example = "#int:1")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "vendingMachineId")]
        [SwaggerDescription(Description = "Id of the vending machine which should drop the product", Example = "#int:22")]
        public long VendingMachineId { get; set; }

        [JsonProperty(PropertyName = "productId")]
        [SwaggerDescription(Description = "The id of the product to drop", Example = "#int:32")]
        public long ProductId { get; set; }
    }
}

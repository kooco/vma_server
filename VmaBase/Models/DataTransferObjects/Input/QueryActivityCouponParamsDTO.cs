﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class QueryActivityCouponParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the coupon (activity) to query\nEither 'activityId' or 'newsId' must be set!", Example = "#int:200")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "newsId")]
        [SwaggerDescription(Description = "Id of the news article for which to query the coupons for.\nThis article must be a category 'ACT' news article containing an activity (coupon).", Example = "#null")]
        public long? NewsId { get; set; }
    }
}

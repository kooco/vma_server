﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class SendTestPushNotificationParamsDTO : BaseAPIParamsDTO
    {
        [JsonProperty("account")]
        [SwaggerDescription(Description = "Account Id of the user to which to send the push notification", Example = "12183044")]
        public string Account { get; set; }

        [JsonProperty("uid")]
        [SwaggerDescription(Description = "User Id (UID) - the user to which to send the push notification.\n-100: send to all users", Example = "#null")]
        public long UID { get; set; }

        [JsonProperty("title")]
        [SwaggerDescription(Description = "Title for the push popup message", Example = "Test Title")]
        public string Title { get; set; }

        [JsonProperty("message")]
        [SwaggerDescription(Description = "Message to send", Example = "test message")]
        public string Message { get; set; }
    }
}

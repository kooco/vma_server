﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class GetFriendRequestsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty(PropertyName = "onlyReceived")]
        [SwaggerDescription(Description = "true = return only [u]received[/u] friend requests from other users.\nNo friend requests sent to other users.\nfalse (Default) = don't care if the friend request is a received one or not", Example = "#bool:false")]
        public bool? OnlyReceived { get; set; }

        [JsonProperty(PropertyName = "onlySent")]
        [SwaggerDescription(Description = "true = return only [u]sent[/u] friend requests TO other users.\nNo friend requests received by other users.\nfalse (Default) = don't care if the friend request is a sent one or not", Example = "#bool:false")]
        public bool? OnlySent { get; set; }

        [JsonProperty(PropertyName = "noAccepted")]
        [SwaggerDescription(Description = "true = do NOT return any already accepted friend requests. Only pending or declined ones.\nfalse (Default) = return also friend requests of already added friends for the last week", Example = "#bool:false")]
        public bool? NoAccepted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class GetCouponStampsParamsDTO : BaseAPIAuthParamsDTO
    {
        [JsonProperty]
        [SwaggerDescription(Description = "The id of the coupon (activity) to retrieve the stamps for", Example = "#int:1")]
        public long ActivityId { get; set; }
    }
}

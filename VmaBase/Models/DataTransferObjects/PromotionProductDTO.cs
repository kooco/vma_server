﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromotionProductDTO
    {
        [JsonProperty(PropertyName = "id")]
        [SwaggerDescription(Description = "Id of the product", Example = "#int:1")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "productNumber")]
        [SwaggerDescription(Description = "A unique number for this product, which should stay the same on every environment (stage, production, ..)\n" +
            "Can be used for building the image url. The url will then be as follows:\n[blue]baseImageUrl[/blue][green]promoprod_##.jpg[/green]\n## = uniqueNumber", Example = "01")]
        public string ProductNumber { get; set; }

        [JsonProperty(PropertyName = "name")]
        [SwaggerDescription(Description = "Name of the product", Example = "可口可樂毛巾")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Full url of the product image", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/promoprod_02.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "points")]
        [SwaggerDescription(Description = "Points necessary for exchanging this product", Example = "#int:200")]
        public int Points { get; set; }

        [JsonProperty(PropertyName = "payAdditional")]
        [SwaggerDescription(Description = "Price to pay additional to get this promotion product. 0 = nothing to pay additionally.")]
        public int PayAdditional { get; set; }

        [JsonProperty(PropertyName = "inStock")]
        [SwaggerDescription(Description = "The amount of items currently available for this product", Example = "#int:538")]
        public int InStock { get; set; }

        [JsonProperty(PropertyName = "pickupLocation")]
        [SwaggerDescription(Description = "Location where to pick up this promotion product", Example = "")]
        public string PickupLocation { get; set; }

        [JsonProperty(PropertyName = "available")]
        [SwaggerDescription(Description = "true = available.\nfalse = not available", Example = "#bool:true")]
        public bool Available { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryGameInfoDTO
    {
        [JsonProperty(PropertyName = "lotteryGameId")]
        [SwaggerDescription(Description = "Id of the lottery game", Example = "#int:1")]
        public long LotteryGameId { get; set; }

        [JsonProperty(PropertyName = "title")]
        [SwaggerDescription(Description = "Title for the lottery game", Example = "可口可樂幸運汽水機")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "lotteryEndDate")]
        [SwaggerDescription(Description = "Date+time this lottery game can be played until. After that date/time the game can not be played anymore")]
        public DateTime LotteryEndDate { get; set; }

        [JsonProperty(PropertyName = "coins")]
        [SwaggerDescription(Description = "Amount of coins the user have for playing a lottery draw", Example = "#int:2")]
        public int Coins { get; set; }
    }
}

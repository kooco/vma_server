﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class NewsActivitySaveDTO : CouponActivityAdminDTO
    {
        public long NewsId { get; set; }

        public ActivityStatus Status { get; set; }        

        public List<ActivityParticipatingProductAdminDTO> Products { get; set; }

        public List<ActivityParticipatingMachineAdminDTO> Machines { get; set; }
    }
}

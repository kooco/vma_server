﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class SendStampTypeDTO
    {
        [JsonProperty(PropertyName = "stampTypeNumber")]
        [SwaggerDescription(Description = "Number of the stamp type to be sent.\nIf more than one stamp of the same stamp type should be sent to the friend,\nthen you can either set the 'amount' field or add two or more stamp types with the same stamp type number.", Example = "01")]
        public string StampTypeNumber { get; set; }

        [JsonProperty(PropertyName = "amount")]
        [SwaggerDescription(Description = "Amount of stamps of this stamp type to send to the friend user.\nCan be omitted or set to null. The Default value will be '1' then.", Example = "#int:1")]
        public int? Amount { get; set; }
    }
}

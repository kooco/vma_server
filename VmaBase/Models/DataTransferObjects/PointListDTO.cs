﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class PointListDTO
    {
        [JsonProperty(PropertyName = "userId")]
        [SwaggerDescription(Description = "User Id", Example = "#int:1")]
        public long UID { get; set; }

        [JsonProperty(PropertyName = "userAccount")]
        [SwaggerDescription(Description = "Account name (account id) of the user", Example = "#int:1218091")]
        public string UserAccount { get; set; }

        [JsonProperty(PropertyName = "userName")]
        [SwaggerDescription(Description = "Full name of the user", Example = "Carrie Brown")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "pointsTotal")]
        [SwaggerDescription(Description = "The total amount of available points for this user", Example = "#int:4800")]
        public long PointsTotal { get; set; }

        [JsonProperty(PropertyName = "pointsList")]
        [SwaggerDescription(Description = "The list of collected points per month")]
        public List<PointMonthInfoDTO> PointsList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromotionProductAdminDTO
    {
        public long Id { get; set; }

        public int ProductNumber { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public int Points { get; set; }

        public int PayAdditional { get; set; }

        public int InStock { get; set; }

        public GeneralStatusEnum Status { get; set; }

        public DateTime CreateTime { get; set; }
    }
}

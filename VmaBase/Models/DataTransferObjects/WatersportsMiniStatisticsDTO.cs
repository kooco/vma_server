﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class WatersportsMiniStatisticsDTO
    {
        public List<WatersportsDayStatisticsDTO> Days { get; set; }
        public WatersportsDayStatisticsDTO Total { get; set; }

        public WatersportsMiniStatisticsDTO()
        {
            Total = new WatersportsDayStatisticsDTO();
            Days = new List<WatersportsDayStatisticsDTO>();
        }

        public WatersportsMiniStatisticsDTO(int iDays)
        {
            Total = new WatersportsDayStatisticsDTO();

            Days = new List<WatersportsDayStatisticsDTO>();
            for (int iNr = 1; iNr <= iDays; iNr++)
                Days.Add(new WatersportsDayStatisticsDTO());
        }
    }
}

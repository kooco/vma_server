﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.Reporting
{
    public class AutoCouponRedeemReportEntryDTO
    {
        public long AssignmentId { get; set; }

        public long MachineId { get; set; }

        public string MachineAssetNo { get; set; }

        public string MachineName { get; set; }

        public string MachineLocation { get; set; }

        public long ProductId { get; set; }

        public string ProductName { get; set; }

        public DateTime RedeemTime { get; set; }

        public long UID { get; set; }

        public string UserAccountId { get; set; }

        public string UserName { get; set; }

        public long? CriteriaId { get; set; }

        public string CriteriaName { get; set; }

        public string CriteriaCouponsTitle { get; set; }

        public long? ActivityId { get; set; }

        public string ActivityTitle { get; set; }
    }
}

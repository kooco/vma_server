﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromotionProductListDTO
    {
        [JsonProperty(PropertyName = "products")]
        [SwaggerDescription(Description = "Contains a list of all currently available (or all, if alsoNotAvailable = true) products (禮品) for points exchange")]
        public List<PromotionProductDTO> Products { get; set; }

        [JsonProperty(PropertyName = "productsCount")]
        [SwaggerDescription(Description = "The amount of products in the list")]
        public int ProductsCount { get; set; }

        [JsonProperty(PropertyName = "baseImageUrl")]
        [SwaggerDescription(Description = "Contains the base url for every product image.\nThe image url of the products can be built in the following way then:\n[blue]BaseUrl[/blue][green]promoprod_##.jpg[/green]\n## = Field 'productNumber' of the product.",
            Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/")]
        public string BaseImageUrl { get; set; }
    }
}

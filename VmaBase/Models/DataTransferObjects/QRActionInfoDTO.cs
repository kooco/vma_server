﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class QRActionInfoDTO
    {
        [JsonProperty(PropertyName = "actionType")]
        [SwaggerDescription(Description = "The action executed by this QR code\n1 = Added user to friend list\n2 = Vending machine QR code. The id of the vending machine is returned in the field 'vendingMachineId'" +
            "\n3 = Added stamps / points to the user account (vending machine stamps QR code)", Example = "#int:1")]
        public QRActionType ActionType { get; set; }

        [JsonProperty(PropertyName = "userAccount")]
        [SwaggerDescription(Description = "If the action code = 1, then this field will contain the account name (account id) of the friend added", Example = "8983647231")]
        public string UserAccount { get; set; }

        [JsonProperty(PropertyName = "userName")]
        [SwaggerDescription(Description = "If the action code = 1, then this field will contain the name of the user added to the friend list", Example = "Max Sample")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "userNickname")]
        [SwaggerDescription(Description = "If the action code = 1, then this field will contain the nickname of the user added to the friend list", Example = "Maxi")]
        public string UserNickname { get; set; }

        [JsonProperty(PropertyName = "userImageUrl")]
        [SwaggerDescription(Description = "If the action code = 1, then this field will contain the image url of the profile picture for the friend added to the friend list", Example = "#null")]
        public string UserImageUrl { get; set; }

        [JsonProperty(PropertyName = "pointsAmountAdded")]
        [SwaggerDescription(Description = "If the action code = 3, then this field will contain the amount of points added to the users account", Example = "#null")]
        public long? pointsAmountAdded { get; set; }

        [JsonProperty(PropertyName = "stampsAmountAdded")]
        [SwaggerDescription(Description = "If the action code = 3, then this field will contain the amount of stamps added to the users account", Example = "#null")]
        public long? stampsAmountAdded { get; set; }

        [JsonProperty(PropertyName = "pointsTotal")]
        [SwaggerDescription(Description = "The current total amount of points for this user (if action code = 2: after points have been added)", Example = "#int:221")]
        public long? pointsTotal { get; set; }

        [JsonProperty(PropertyName = "lotteryCoinsTotal")]
        [SwaggerDescription(Description = "Amount of lottery coins the user has (if action code = 4)")]
        public int? lotteryCoinsTotal { get; set; }

        [JsonProperty(PropertyName = "lotteryCoinsAdded")]
        [SwaggerDescription(Description = "Amount of lottery coins added (if action code = 4)")]
        public int? lotteryCoinsAdded { get; set; }

        [JsonProperty(PropertyName = "vendingMachineId")]
        [SwaggerDescription(Description = "If action code = 2: The id of the vending machine scanned", Example = "#int:1")]
        public long? VendingMachineId { get; set; }

        [JsonProperty(PropertyName = "vendingMachineStatus")]
        [SwaggerDescription(Description = "For action code 2: Status of the vending machine.\n1 = Active, 0 = not ready yet, -1 = the machine is broken or currently not available (network problems), -3 = Sold out\nEvery number below 1 means: this vending machine (currently) can not be used!", Example = "#int:1")]
        public VendingMachineStatus VendingMachineStatus { get; set; }

        [JsonProperty(PropertyName = "machineMatchesActivity")]
        [SwaggerDescription(Description = "only will be returned if the parameter 'activityId' is set and the QR code is a machine QR code!\nnull = 'activityId' not set or the QR code is not a machine QR\ntrue = this machine is part of the given activity\nfalse = this machine is NOT part of the given activity", Example = "#null")]
        public bool? MachineMatchesActivity { get; set; }

        [JsonProperty(PropertyName = "activity")]
        [SwaggerDescription(Description = "For action code 3: Info about the activity for a stamps/points QR, if the product the user bought at this vending machine is part of an activity. Otherwise this property is null")]
        public CouponActivityDTO Activity { get; set; }

        [JsonProperty(PropertyName = "activityCouponAvailable")]
        [SwaggerDescription(Description = "For action code 2: true = this vending machine is part of an activity and the user has at least one completed coupon for it\nfalse = this vending machine is not part of an activity OR the user does not have any completed coupon for any of these activities\nFor other action codes than 2 this will be always null!", Example = "#bool:false")]
        public bool? ActivityCouponAvailable { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryDayStatisticsDTO
    {
        public int PricesWonTotal { get; set; }
        public int PricesExchangedTotal { get; set; }
        public int PricesWonCola { get; set; }
        public int PricesWonStallGames { get; set; }
        public int PricesWonTowel { get; set; }
        public int PricesWonMiniFridge { get; set; }
        public int PricesWonGrill { get; set; }
        public int PricesWonTravelSet { get; set; }
        public int PricesWonCeramicBowl { get; set; }
        public int PricesWonStorageTank { get; set; }
        public int GameDrawsCount { get; set; }
        public int UserDrawedGameCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromotionOrderListDTO
    {
        [JsonProperty(PropertyName = "orders")]
        [SwaggerDescription(Description = "Contains a list of all orders (exchanges - 兌換) for this user")]
        public List<PromotionOrderDTO> Orders { get; set; }

        [JsonProperty(PropertyName = "ordersCount")]
        [SwaggerDescription(Description = "The amount of orders in the list")]
        public int OrdersCount { get; set; }

        [JsonProperty(PropertyName = "baseImageUrl")]
        [SwaggerDescription(Description = "Contains the base url for every product image.\nThe image url of the products can be built in the following way then:\n[blue]BaseUrl[/blue][green]promoprod_##.jpg[/green]\n## = Field 'productNumber' of the product.",
            Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/")]
        public string BaseImageUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryPriceCouponDTO
    {
        [JsonProperty(PropertyName = "priceId")]
        [SwaggerDescription(Description = "Unique id of the lottery price item (NOT of the coupon)", Example = "#int:2")]
        public long PriceId { get; set; }

        [JsonProperty(PropertyName = "title")]
        [SwaggerDescription(Description = "Title of this price", Example = "可口可樂冰沙機")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "unit")]
        [SwaggerDescription(Description = "Unit of the price item", Example = "個")]
        public string Unit { get; set; }

        [JsonProperty(PropertyName = "pickupAddress")]
        [SwaggerDescription(Description = "The address the user can pick up this price at", Example = "澳門旅遊塔外圓形地廣場-場內指定可口可樂®攤位")]
        public string PickupAddress { get; set; }

        [JsonProperty(PropertyName = "pickupPhone")]
        [SwaggerDescription(Description = "Phone number for further pickup info", Example = "(853)2823 4567")]
        public string PickupPhone { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Url of the image for this price (currently empty image! If needed, will later added to the admin site)", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/lotteryprice_01.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "pickupNumber")]
        [SwaggerDescription(Description = "Unique number to show the staff for pickup, if the won price is not a drink coupon", Example = "5812328891")]
        public string PickupNumber { get; set; }

        [JsonProperty(PropertyName = "isVendingMachinePrice")]
        [SwaggerDescription(Description = "true = this price can be redeemed directly at a vending machine (every vending machine which is part of the activity bound to this lottery game)\nfalse = this price needs to be picked up at the given address", Example = "#bool:false")]
        public bool IsVendingMachinePrice { get; set; }

        [JsonProperty(PropertyName = "isUsable")]
        [SwaggerDescription(Description = "True = This coupon is still usable.\nFalse = this coupon already has been redeemed", Example = "#bool:true")]
        public bool IsUsable { get; set; }
    }
}

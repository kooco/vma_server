﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class CouponActivityDTO : CouponDTO
    {
        [JsonProperty(PropertyName = "limitStartTime")]
        [SwaggerDescription(Description = "Starting time for this activity")]
        public DateTime LimitStartTime { get; set; }

        [JsonProperty(PropertyName = "limitEndTime")]
        [SwaggerDescription(Description = "Ending time for this activity")]
        public DateTime LimitEndTime { get; set; }

        [JsonProperty(PropertyName = "stamps")]
        [SwaggerDescription(Description = "List of stamps to be collected or already collected")]
        public List<CouponStampDTO> Stamps { get; set; }

        [JsonProperty(PropertyName = "stampsCount")]
        [SwaggerDescription(Description = "The amount of stamps need to be collected", Example = "#int:0")]
        public int StampsCount { get; set; }

        [JsonProperty(PropertyName = "isNoStampsActivity")]
        [SwaggerDescription(Description = "true = this activity is a 'NoStamps activity', which means no stamps can be collected for this coupon! Also no stamps should be shown.\nThis is a special activity type for using the activity coupon system for prices won (for example), to be able to use the same redeem system.\nfalse = stamps can be collected for this activity. This is the case for most types of activities.", Example = "#bool:false")]
        public bool IsNoStampsActivity { get; set; }

        [JsonProperty(PropertyName = "sendAllowed")]
        [SwaggerDescription(Description = "true = sending coupons is allowed for this activity.\nfalse = sending coupons to friends is NOT allowed for this activity", Example = "#bool:true")]
        public bool SendAllowed { get; set; }
    }
}

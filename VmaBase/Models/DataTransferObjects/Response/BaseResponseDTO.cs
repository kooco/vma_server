﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Newtonsoft.Json;

using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Shared;


// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace VmaBase.Models.DataTransferObjects.Response
{
    /// <summary>
    ///     BaseResponseDTO class
    ///     ---------------------
    ///     Base-Response class for API and AJAX responses
    ///     
    ///     auto-generated by the Maxima Framework.
    /// </summary>
    public class BaseResponseDTO
    {
        [JsonProperty(PropertyName = "returnCode")]
        public ReturnCodes ReturnCode { get; set; }

        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty(PropertyName = "data", NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "totalPages", NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalPages { get; set; }

        [JsonProperty(PropertyName = "count", NullValueHandling = NullValueHandling.Ignore)]
        public int? Count { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ResponseSortMappingDTO> SortMappings { get; set; }

        public void SetError(ReturnCodes parReturnCode, string sErrorMessage)
        {
            ReturnCode = parReturnCode;
            ErrorMessage = sErrorMessage;
            Data = null;

            if (string.IsNullOrWhiteSpace(sErrorMessage))
            {
                FieldInfo field_info = typeof(ReturnCodes).GetField(parReturnCode.ToString());
                try
                {
                    Kooco.Framework.Shared.DefaultErrorMessageAttribute attr = (Kooco.Framework.Shared.DefaultErrorMessageAttribute)field_info.GetCustomAttribute(typeof(Kooco.Framework.Shared.DefaultErrorMessageAttribute));
                    if (attr != null)
                        ErrorMessage = attr.Message;
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "Error while trying the retrieve the DefaultErrorMessage attribute for the ReturnCode " + parReturnCode.ToString());
                }
            }
        }
        public void SetError(ReturnCodes parReturnCode)
        {
            SetError(parReturnCode, string.Empty);
        }

        public BaseResponseDTO()
        {
            ReturnCode = ReturnCodes.Success;
            ErrorMessage = string.Empty;
            Data = null;
        }

        public BaseResponseDTO(ReturnCodes parReturnCode)
        {
            SetError(parReturnCode);
        }

        public BaseResponseDTO(ReturnCodes parReturnCode, string sErrorMessage)
        {
            SetError(parReturnCode, sErrorMessage);
            Data = null;
        }

        public BaseResponseDTO(object ResultObject)
        {
            ReturnCode = ReturnCodes.Success;
            ErrorMessage = string.Empty;
            Data = ResultObject;
        }

        public BaseResponseDTO(Kooco.Framework.Models.DataTransferObjects.Response.FrameworkResponseDTO FrameworkResponse)
        {
            ReturnCode = (ReturnCodes)((int)FrameworkResponse.ReturnCode);
            ErrorMessage = FrameworkResponse.ErrorMessage;
            TotalPages = FrameworkResponse.TotalPages;            
            Data = FrameworkResponse.Data;
            SortMappings = FrameworkResponse.SortMappings;
            Count = FrameworkResponse.Count;
        }

        public static BaseResponseDTO FromFrameworkResponse(Kooco.Framework.Models.DataTransferObjects.Response.FrameworkResponseDTO FrameworkResponse)
        {
            return new BaseResponseDTO(FrameworkResponse);
        }

        public void AESEncrypt()
        {
            if (Data != null)
            {
                string sJson = JsonConvert.SerializeObject(Data);
                Data = Utils.BuyBuyAESEncrypt(sJson);
            }            
        }
    }
}

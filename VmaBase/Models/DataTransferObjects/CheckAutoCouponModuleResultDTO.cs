﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects
{
    public class CheckAutoCouponModuleResultDTO
    {
        public bool Ok { get; set; }

        public string ErrorMessage { get; set; }

        public string Messages { get; set; }

        [JsonIgnore]
        internal DynamicFunctionHelper DynamicHelper { get; set; }

        public long ModuleId { get; set; }

        public string ModuleName { get; set; }

        public bool ReceivesCoupon { get; set; }

        public int CouponsCount { get; set; }

        #region Constructor
        public CheckAutoCouponModuleResultDTO()
        {
            Ok = false;
            ErrorMessage = string.Empty;
            DynamicHelper = null;
            Messages = string.Empty;
        }
        public CheckAutoCouponModuleResultDTO(bool Success, string ErrorMsg = "")
        {
            Ok = Success;
            ErrorMessage = ErrorMsg;
            Messages = string.Empty;
        }
        public CheckAutoCouponModuleResultDTO(bool Success, string ErrorMsg, DynamicFunctionHelper Helper, bool receivesCoupon, int Count)
        {
            Ok = Success;
            ErrorMessage = ErrorMsg;
            DynamicHelper = Helper;
            Messages = string.Empty;
            ReceivesCoupon = receivesCoupon;
            CouponsCount = Count;
        }
        #endregion
    }
}

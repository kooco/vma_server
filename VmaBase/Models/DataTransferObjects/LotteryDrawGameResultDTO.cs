﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryDrawGameResultDTO
    {
        [JsonProperty(PropertyName = "coinsLeft")]
        [SwaggerDescription(Description = "Amount of coins left for drawing a new game", Example = "#int:1")]
        public int CoinsLeft { get; set; }

        [JsonProperty(PropertyName = "won")]
        [SwaggerDescription(Description = "true = the user won something.\nfalse = the user did not win anything", Example = "#bool:true")]
        public bool Won { get; set; }

        [JsonProperty(PropertyName = "priceWon")]
        [SwaggerDescription(Description = "If the user won something, then this field will contain the coupon data for the price the user won. Otherwise this field is null.")]
        public LotteryPriceCouponDTO PriceWon { get; set; }
    }
}

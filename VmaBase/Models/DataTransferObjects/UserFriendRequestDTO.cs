﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;


namespace VmaBase.Models.DataTransferObjects
{
    public class UserFriendRequestDTO : UserSearchResultDTO
    {
        [JsonProperty("requestState", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "State of the friend request.\n0 = Pending, 1 = Confirmed, 2 = Rejected", Example = "#int:0")]
        public UserFriendStatus? RequestState { get; set; }

        [JsonProperty("requestType", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "Type of the friend request.\n1 = Sent request, 2 = Received request, 3 = already confirmed request", Example = "#int:1")]
        public FriendRequestType? RequestType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class WatersportsDayStatisticsDTO
    {
        public int PricesWonTotal { get; set; }
        public int PricesExchangedTotal { get; set; }
        public int PricesWonAqua { get; set; }
        public int PricesWonGarmin { get; set; }
        public int PricesWonMoney { get; set; }
        public int GameDrawsCount { get; set; }
        public int UserDrawedGameCount { get; set; }
    }
}

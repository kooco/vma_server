﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryMiniStatisticsDTO
    {
        public List<LotteryDayStatisticsDTO> Days { get; set; }
        public LotteryDayStatisticsDTO Total { get; set; }

        public LotteryMiniStatisticsDTO()
        {
            Total = new LotteryDayStatisticsDTO();
            Days = new List<LotteryDayStatisticsDTO>();
        }

        public LotteryMiniStatisticsDTO(int iDays)
        {
            Total = new LotteryDayStatisticsDTO();

            Days = new List<LotteryDayStatisticsDTO>();
            for (int iNr = 1; iNr <= iDays; iNr++)
                Days.Add(new LotteryDayStatisticsDTO());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class PointMonthInfoDTO
    {
        [JsonProperty("month")]
        [SwaggerDescription(Description = "Month (integer value)", Example = "#int:7")]
        public int Month { get; set; }

        [JsonProperty("year")]
        [SwaggerDescription(Description = "Year", Example = "#int:2017")]
        public int Year { get; set; }

        [JsonProperty("monthTerm")]
        [SwaggerDescription(Description = "Short name for the month", Example = "JUL")]
        public string MonthTerm { get; set; }

        [JsonProperty("points")]
        [SwaggerDescription(Description = "The amount of points collected (and still available) in this month", Example = "#int:521")]
        public long Points { get; set; }

        [JsonProperty("expiryDate")]
        [SwaggerDescription(Description = "Date+time these points will expire at", Example = "#date:2018/07/31")]
        public DateTime ExpiryDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class LotteryPriceItemDTO
    {
        [JsonProperty(PropertyName = "priceId")]
        [SwaggerDescription(Description = "Unique id of the lottery price item", Example = "#int:1")]
        public long PriceId { get; set; }

        [JsonProperty(PropertyName = "title")]
        [SwaggerDescription(Description = "Title of this price", Example = "冰水機")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Url of the image for this price", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/lotteryprice_01.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "amountAvailable")]
        [SwaggerDescription(Description = "The amount of this price item that can be won. (共amount名)", Example = "#int:5")]
        public int AmountAvailable { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;


namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyEncryptedParamsDTO
    {
        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }

        public BuyBuyEncryptedParamsDTO()
        {
            Data = string.Empty;
        }
        public BuyBuyEncryptedParamsDTO(object objData)
        {
            string sSerialized = JsonConvert.SerializeObject(objData);
        }
    }
}

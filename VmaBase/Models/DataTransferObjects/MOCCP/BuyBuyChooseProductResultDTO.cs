﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyChooseProductResultDTO
    {
        [JsonProperty(PropertyName = "MerchandiseID")]
        [SwaggerDescription(Description = "ID of the product the user has selected", Example = "099N")]
        public string MerchandiseID { get; set; }

        [JsonProperty(PropertyName = "InternalId")]
        [SwaggerDescription(Description = "Internal id of this product", Example = "#int:219")]
        public int InternalId { get; set; }

        [JsonProperty(PropertyName = "TransactionId")]
        [SwaggerDescription(Description = "Id of the VMA transaction for this order or -1", Example = "#int:231")]
        public long TransactionId { get; set; }
    }
}

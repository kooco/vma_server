﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyValidateCouponResultDTO
    {
        [JsonProperty(PropertyName = "internalId")]
        [SwaggerDescription(Description = "Internal Id of the product this coupon is for (can be ignored for the lottery coupon, if the product can be chosen by the user).\nnull = no product bound to the coupon", Example = "#int:2")]
        public int? InternalId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyGetVendingListEntry
    {
        public string Organization { get; set; }
        public string AssetNo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Location { get; set; }
        public int AssetType { get; set; }
        public string RegistrationID { get; set; }

        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }

        public DateTime LastResponseTime { get; set; }
    }
}

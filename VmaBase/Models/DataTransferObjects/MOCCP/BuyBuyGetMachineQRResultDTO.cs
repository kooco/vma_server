﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{    
    public class BuyBuyGetMachineQRResultDTO
    {
        [JsonProperty(PropertyName = "qrToken")]
        [SwaggerDescription(Description = "The current QR code for this machine", Example = "2874834-8382")]
        public string QRToken { get; set; }
    }
}

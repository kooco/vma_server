﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyValidateCouponParamsDTO : BuyBuyMachineParamsDTO
    {
        [JsonProperty("couponToken")]
        [SwaggerDescription(Description = "The lottery price coupon token to be validated", Example = "932283790015")]
        public string CouponToken { get; set; }
    }
}

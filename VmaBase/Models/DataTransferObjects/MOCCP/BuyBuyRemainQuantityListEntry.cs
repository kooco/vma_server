﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyRemainQuantityListEntry
    {
        public string MerchandiseID { get; set; }

        public int RemainQuantity { get; set; }

        public double Price { get; set; }
    }
}

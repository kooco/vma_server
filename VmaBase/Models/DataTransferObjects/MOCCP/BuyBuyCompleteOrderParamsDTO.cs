﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyCompleteOrderParamsDTO : BuyBuyMachineParamsDTO
    {
        [JsonProperty(PropertyName = "internalId")]
        [SwaggerDescription(Description = "Id of the product ordered (required for non-vma transactions!)", Example = "143")]
        public int? InternalId { get; set; }

        [JsonProperty(PropertyName = "vmaTransactionId")]
        [SwaggerDescription(Description = "Id of the Vma transaction for this order (retrieved by QueryMerchSelection) or null (omitted)")]
        public long? VmaTransactionId { get; set; }

        [JsonProperty(PropertyName = "buybuyTransactionNr")]
        [SwaggerDescription(Description = "BuyBuy-Id of this transaction, if the transaction is not a Vma order, or null (omitted)", Example = "1234")]
        public string BuyBuyTransactionNr { get; set; }

        [JsonProperty(PropertyName = "cancel")]
        [SwaggerDescription(Description = "1 = Cancel transaction instead of completing it (user didn't buy anything).\n0 = Complete transaction normally", Example = "#int:0")]
        public int Cancel { get; set; }
    }
}

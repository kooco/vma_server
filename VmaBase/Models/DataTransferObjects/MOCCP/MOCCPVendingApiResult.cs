﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class MOCCPVendingApiResult
    {
        [JsonProperty(PropertyName = "code")]
        public int ReturnCode { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string EncryptedData { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}

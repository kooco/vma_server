﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyGetMerchandiseListEntry
    {
        public string Organization { get; set; }
        public string MerchandiseID { get; set; }
        public int InternalId { get; set; }
        public string MerchandiseName { get; set; }
        public int MerchandiseType { get; set; }

        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }

        public string Spec { get; set; }
        public string PictureUrl { get; set; }
    }
}

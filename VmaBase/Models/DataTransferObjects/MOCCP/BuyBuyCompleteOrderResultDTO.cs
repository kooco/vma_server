﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{    
    public class BuyBuyCompleteOrderResultDTO
    {
        [JsonProperty(PropertyName = "qrToken")]
        [SwaggerDescription(Description = "The QR token for getting stamps and/or points.\nThis field can be null or empty, if no (additional) points and stamps can be collected for this transaction.", Example = "2874834-8382")]
        public string QRToken { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyPushTypeDTO
    {
        //type:F 重啟, 1 售賣機後台, 2 點指buybuy選產品, 3 點指buybuy出貨
        public string type { get; set; }
        public string content { get; set; }
    }
}

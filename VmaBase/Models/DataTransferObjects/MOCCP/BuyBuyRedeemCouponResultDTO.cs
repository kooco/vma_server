﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyRedeemCouponResultDTO
    {
        [JsonProperty(PropertyName = "MerchandiseID")]
        [SwaggerDescription(Description = "ID of the product the user wants to get", Example = "099N")]
        public string MerchandiseID { get; set; }

        [JsonProperty(PropertyName = "InternalId")]
        [SwaggerDescription(Description = "Internal id of the product", Example = "#int:219")]
        public int InternalId { get; set; }

        [JsonProperty(PropertyName = "CouponId")]
        [SwaggerDescription(Description = "Vma Id of the coupon used to get the free drink", Example = "#int:231")]
        public long CouponId { get; set; }
    }
}

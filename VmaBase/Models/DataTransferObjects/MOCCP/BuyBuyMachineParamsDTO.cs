﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;


namespace VmaBase.Models.DataTransferObjects.MOCCP
{
    public class BuyBuyMachineParamsDTO
    {
        [JsonProperty(PropertyName = "organization")]
        [SwaggerDescription(Description = "Organization this machine is part of", Example = "XX")]
        public string Organization { get; set; }

        [JsonProperty(PropertyName = "assetNo")]
        [SwaggerDescription(Description = "asset number of this machine", Example = "SVB-001")]
        public string AssetNo { get; set; }

        [JsonProperty(PropertyName = "apiKey")]
        [SwaggerDescription(Description = "API Key", Example = "KXA2Y3A29")]
        public string ApiKey { get; set; }

        [JsonProperty(PropertyName = "currentDate")]
        [SwaggerDescription(Description = "Current time (as a security check) in format YYYYMMDDHHIISS", Example = "#currentutcdate_encr#")]
        public string CurrentDate { get; set; }

        public BaseResponseDTO CheckValidity()
        {
            if (ApiKey != GlobalConst.BuyBuyApiKey)
                return new BaseResponseDTO(ReturnCodes.InvalidAPIToken);

            DateTime setDate = DateTime.MinValue;
            try
            {
                setDate = new DateTime(Convert.ToInt32(CurrentDate.Substring(0, 4)), Convert.ToInt32(CurrentDate.Substring(4, 2)), Convert.ToInt32(CurrentDate.Substring(6, 2)), Convert.ToInt32(CurrentDate.Substring(8, 2)), Convert.ToInt32(CurrentDate.Substring(10, 2)), Convert.ToInt32(CurrentDate.Substring(12, 2)));                
            }
            catch (Exception)
            {
                Utils.LogError("BuyBuy CheckValidity Error: Invalid 'CurrentDate' property! CurrentDate was: '" + CurrentDate + "'!");
                return new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError);
            }

            if (setDate < DateTime.UtcNow.AddMinutes(-5) || setDate > DateTime.UtcNow.AddMinutes(5))
            {
                Utils.LogError("BuyBuy CheckValidity Error: 'CurrentDate' out of range! CurrentDate was: '" + setDate.ToShortDateString() + " " + setDate.ToShortTimeString() + "'. Server UTC date: " + DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.ToShortTimeString());
                return new BaseResponseDTO(ReturnCodes.BuyBuyDecryptionError);
            }

            return new BaseResponseDTO(ReturnCodes.Success);
        }
    }
}

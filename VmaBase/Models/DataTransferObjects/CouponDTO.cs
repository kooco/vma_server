﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class CouponDTO
    {
        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of this activity (coupon)", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "criteriaId")]
        [SwaggerDescription(Description = "Id of the criteria (auto-coupon rule) created this coupon", Example = "#int:1")]
        public long? CriteriaId { get; set; }

        [JsonProperty(PropertyName = "couponsType")]
        [SwaggerDescription(Description = "Type of coupon. 1 = Activity-Coupon. 2 = Auto-created coupon (by criteria/rule)", Example = "#int:1")]
        public UserCouponType Type { get; set; }

        [JsonProperty(PropertyName = "title")]
        [SwaggerDescription(Description = "Title of the activity", Example = "Get 6 sprites!!")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "activityText")]
        [SwaggerDescription(Description = "further description for the activity", Example = "collect 6 sprite stickers and get 6 sprites for free!")]
        public string ActivityText { get; set; }

        [JsonProperty(PropertyName = "productId", NullValueHandling = NullValueHandling.Ignore)]
        //[SwaggerDescription(Description = "Id of the product that can get for free if this coupon is completed.\nnull = with this coupon the user can get any product for free", Example = "#int:2")]
        [HideInDocs]
        public long? ProductId { get; set; }

        [JsonProperty(PropertyName = "productUniqueNumber", NullValueHandling = NullValueHandling.Ignore)]
        //[SwaggerDescription(Description = "Global unique number (product field 'uniqueNumber') of the product the user can get for free with this coupon.\nIf it's a 'free choice' coupon, then this field will be empty!", Example = "00002")]
        [HideInDocs]
        public string ProductUniqueNumber { get; set; }

        [JsonProperty(PropertyName = "productName", NullValueHandling = NullValueHandling.Ignore)]
        //[SwaggerDescription(Description = "Name of the product the user can get with a completed coupon.\nif with this product the user can get any product for free (free choice), then the name will be empty!", Example = "雪碧")]
        [HideInDocs]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "productImageUrl", NullValueHandling = NullValueHandling.Ignore)]
        //[SwaggerDescription(Description = "Url of the image for the product which can get for free with this coupon.\nIf this coupon is a 'free choice' coupon then this field will be empty!", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/vendingproduct_00002.jpg")]
        [HideInDocs]
        public string ProductImageUrl { get; set; }

        [JsonProperty(PropertyName = "expiryDate")]
        [SwaggerDescription(Description = "Expiry date for this coupon")]
        public DateTime ExpiryDate { get; set; }

        [JsonProperty(PropertyName = "status")]
        [SwaggerDescription(Description = "1 = collecting, 2 = completed, 3 = redeemed, 4 = disabled")]
        public UserCouponStatus Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class PromotionOrderDTO
    {
        [JsonProperty(PropertyName = "orderNumber")]
        [SwaggerDescription(Description = "Pick-Up number (order number) for this promotion product exchange", Example = "23115089")]
        public long OrderNumber { get; set; }

        [JsonProperty(PropertyName = "productNumber")]
        [SwaggerDescription(Description = "Number of the product exchanged", Example = "01")]
        public string ProductNumber { get; set; }

        [JsonProperty(PropertyName = "productName")]
        [SwaggerDescription(Description = "Name of the product exchanged", Example = "可口可樂毛巾")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "productImageUrl")]
        [SwaggerDescription(Description = "Url of the image for the product exchanged", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/promoprod_02.jpg")]
        public string ProductImageUrl { get; set; }

        [JsonProperty(PropertyName = "status")]
        [SwaggerDescription(Description = "Order status.\n1 = product exchanged, but not yet picked up\n2 = Order completed, already picked up\n3 = not picked up and already expired\n4 = Order cancelled and points refunded")]
        public PromotionOrderStatus Status { get; set; }

        [JsonProperty(PropertyName = "refundReason")]
        [SwaggerDescription(Description = "If the order has been cancelled, then this will contain the general reason for the cancellation. (int code)\n1 = Product not available (anymore)\n2 = cancelled on request of the user\n10 = other reason (usually the reason should be written in the 'orderNotes' field then)", Example = "#null")]
        public PromotionOrderRefundReason RefundReason { get; set; }

        [JsonProperty(PropertyName = "orderNotes")]
        [SwaggerDescription(Description = "Additional order notes from the admin site. Mainly meant to be used for further notes if the order has been cancelled.", Example = "#null")]
        public string OrderNotes { get; set; }

        [JsonProperty(PropertyName = "payAdditional")]
        [SwaggerDescription(Description = "Price to pay additional to get this promotion product. 0 = nothing to pay additionally.")]
        public int PayAdditional { get; set; }

        [JsonProperty("expiryDate")]
        [SwaggerDescription(Description = "Date+time this order will expire at. After that date the order can not be picked up anymore!")]
        public DateTime ExpiryTime { get; set; }

        [JsonProperty("exchangeDate")]
        [SwaggerDescription(Description = "Date+time this exchange was started at")]
        public DateTime ExchangeTime { get; set; }
    }
}

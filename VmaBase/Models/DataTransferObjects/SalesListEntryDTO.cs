﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class SalesListEntryDTO
    {
        [JsonProperty(PropertyName = "id")]
        [SwaggerDescription(Description = "Id of this sales list entry", Example = "#int:1")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "vendingMachineId")]
        [SwaggerDescription(Description = "Id of the vending machine", Example = "#int:1")]
        public long VendingMachineId { get; set; }

        [JsonProperty(PropertyName = "productId")]
        [SwaggerDescription(Description = "Id of the product", Example = "#int:1")]
        public long ProductId { get; set; }

        [JsonProperty(PropertyName = "uniqueNumber")]
        [SwaggerDescription(Description = "A unique number for this product (from the BuyBuy API), which stays the same on every environment (stage, production, ..)\n" +
            "Can be used for building the image url. The url will then be as follows:\n[blue]baseImageUrl[/blue][green]vendingproduct_#####.jpg[/green]\n##### = uniqueNumber", Example = "00001")]
        public string UniqueNumber { get; set; }

        [JsonProperty(PropertyName = "internalId")]
        [SwaggerDescription(Description = "Internal id of this product used for vending machines (not used for the normal VMA API's!)", Example = "143")]
        public int InternalId { get; set; }

        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "Name of the product", Example = "可口可樂")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "imageUrl", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "Url of the image for this product", Example = "http://vmadev.oss-cn-shanghai.aliyuncs.com/vendingproduct_00005.jpg")]
        public string ProductImageUrl { get; set; }

        [JsonIgnore]
        public double Price { get; set; }

        [JsonProperty(PropertyName = "status")]
        [SwaggerDescription(Description = "Status of the product in this machine.\n1 = On sale, 0 = sold out (not available)", Example = "#int:1")]
        public SalesStatus Status { get; set; }
    }
}

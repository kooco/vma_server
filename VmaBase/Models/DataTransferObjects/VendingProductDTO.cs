﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class VendingProductDTO
    {
        [JsonProperty(PropertyName = "id")]
        [SwaggerDescription(Description = "Id of the product", Example = "#int:1")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "uniqueNumber")]
        [SwaggerDescription(Description = "A unique number for this product (from the BuyBuy API), which stays the same on every environment (stage, production, ..)\n" + 
            "Can be used for building the image url. The url will then be as follows:\n[blue]baseImageUrl[/blue][green]vendingproduct_#####.jpg[/green]\n##### = uniqueNumber", Example = "00001")]
        public string UniqueNumber { get; set; }

        [JsonProperty(PropertyName = "name")]
        [SwaggerDescription(Description = "Name of the product", Example = "可口可樂")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "volume")]
        [SwaggerDescription(Description = "Product volume", Example = "230ml")]
        public string Volume { get; set; }

        [JsonProperty(PropertyName = "nameEn")]
        [SwaggerDescription(Description = "English name of the product", Example = "Coca Cola")]
        public string NameEN { get; set; }

        [JsonProperty(PropertyName = "productType")]
        [SwaggerDescription(Description = "Id of the product type")]
        public long ProductTypeId { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        [SwaggerDescription(Description = "Url of the product image")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "available")]
        [SwaggerDescription(Description = "true = available.\nfalse = not available", Example = "#bool:true")]
        public bool Available { get; set; }
    }
}

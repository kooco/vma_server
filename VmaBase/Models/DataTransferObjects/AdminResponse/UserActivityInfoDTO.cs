﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class UserActivityInfoDTO
    {
        public int CompletedCouponsCount { get; set; }

        public int CurrentCouponCollectedStamps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class VendingProductActivityAdminDTO
    {
        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.Id")]
        public long Id { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.MOCCPGoodsId")]
        public string MOCCPGoodsId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.Name")]
        public string Name { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.NameEN")]
        public string NameEN { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.Volume")]
        public string Volume { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.ProductType")]
        public long Type { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.ImageUrl")]
        public string ImageUrl { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.AveragePrice")]
        public double AveragePrice { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.DefaultStampTypeId")]
        public long? DefaultStampTypeId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.DefaultStampTypeImageUrl")]
        public string DefaultStampTypeImageUrl { get; set; }

        public int PointsExtra { get; set; }

        public int PointsMultiplier { get; set; }

        public int StampsMultiplier { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.Status")]
        public VendingProductStatus Status { get; set; }

        public long? ActivityId { get; set; }
    }
}

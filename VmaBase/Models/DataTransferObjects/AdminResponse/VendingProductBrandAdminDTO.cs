﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;
using Kooco.Framework.Shared.Attributes;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class VendingProductBrandAdminDTO
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public GeneralStatusEnum Status { get; set; }

        public string StatusDisplayText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(GeneralStatusEnum), Status.ToString());
            }
        }

        [SortMapping(PropertyName = "Status")]
        public bool CanDelete { get; set; }
    }
}

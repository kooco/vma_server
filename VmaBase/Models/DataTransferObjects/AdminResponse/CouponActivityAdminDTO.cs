﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class CouponActivityAdminDTO
    {
        public long ActivityId { get; set; }

        public string Title { get; set; }

        public string ActivityText { get; set; }

        public string ActivityListImageUrl { get; set; }

        public bool AllVendingMachines { get; set; }

        public int CouponExpiryDays { get; set; }

        public int StampsCount { get; set; }

        public bool HasUserCoupons { get; set; }

        public bool NoStampsActivity { get; set; }

        public bool SendingCouponsAllowed { get; set; }

        public bool ShowOnPromotionMachinesList { get; set; }

        public DateTime RedeemEndTime { get; set; }
    }
}

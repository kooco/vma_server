﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class LotteryPriceItemAdminDTO
    {
        public long Id { get; set; }

        public long LotteryGameId { get; set; }

        public long LotteryPriceId { get; set; }

        [SortMapping(PropertyName = "Price.Title")]
        public string PriceTitle { get; set; }

        [SortMapping(PropertyName = "Price.ImageUrl")]
        public string PriceImageUrl { get; set; }

        public DateTime WinFromTime { get; set; }

        [SortMapping(PropertyName = "WonTime")]
        public bool IsWon { get; set; }

        public DateTime? WonTime { get; set; }

        public long? WonByUID { get; set; }

        [SortMapping(PropertyName = "WonByUser.UserBase.Account", AlsoDTOMapping = true)]
        public string WonByAccount { get; set; }

        [SortMapping(PropertyName = "WonByUser.UserBase.Nickname", AlsoDTOMapping = true)]
        public string WonByNickname { get; set; }

        public GeneralStatusEnum Status { get; set; }
    }
}

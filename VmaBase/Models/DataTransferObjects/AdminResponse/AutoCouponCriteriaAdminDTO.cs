﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class AutoCouponCriteriaAdminDTO
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string CouponsTitle { get; set; }

        public string CouponsSubTitle { get; set; }

        public long? ModuleId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Module.Name")]
        public string ModuleName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CSharpCode { get; set; }

        public long? UID { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Account")]
        public string UserAccount { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Nickname")]
        public string UserNickname { get; set; }

        public DateTime? CouponsExpiryDate { get; set; }

        public bool CouponsForAllProducts { get; set; }

        public bool CouponsForAllMachines { get; set; }

        public DateTime LimitStartTime { get; set; }

        public DateTime LimitEndTime { get; set; }

        public int CouponsLimit { get; set; }

        public int CouponsIssued { get; set; }

        public AutoCouponCriteriaStatus Status { get; set; }

        public string StatusDisplayText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(AutoCouponCriteriaStatus), Status.ToString());
            }
        }

        public DateTime CreateTime { get; set; }

        public DateTime ModifyTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<CouponCriteriaProductAdminDTO> Products { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<CouponCriteriaMachineAdminDTO> Machines { get; set; }

        public bool IsReadOnly
        {
            get
            {
                return (Id == 1);
            }
        }
    }
}

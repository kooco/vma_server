﻿using Kooco.Framework.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class LotteryGameAdminDTO
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime CouponsExpiryTime { get; set; }

        public DateTime? TestStartTime { get; set; }

        public LotteryTestMode TestMode { get; set; }

        public LotteryDrawType DrawType { get; set; }

        public long? ConnectedActivityId { get; set; }

        public CouponActivity ConnectedActivity { get; set; }

        public bool CoinsCanExpire { get; set; }

        public DateTime? CoinsExpiryDate { get; set; }

        public ExpiryType CoinsExpiryType { get; set; } // not used yet

        public int CoinsExpiryPeriod { get; set; }      // not used yet

        public LotteryPickupTokenMode PickupTokenMode { get; set; }

        public string PickupToken { get; set; }

        public LotteryGameStatus Status { get; set; }
    }
}

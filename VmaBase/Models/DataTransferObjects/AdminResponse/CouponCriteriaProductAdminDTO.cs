﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class CouponCriteriaProductAdminDTO
    {
        public long ProductId { get; set; }

        public string ProductName { get; set; }

        public string MerchandiseId { get; set; }

        public string MOCCPOrganization { get; set; }

        public int? InternalId { get; set; }

        public string Volume { get; set; }

        public string ProductImageUrl { get; set; }

        public long? StampTypeId1 { get; set; }

        public string StampType1ImageUrl { get; set; }

        public VendingProductStatus Status { get; set; }
    }
}

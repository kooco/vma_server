﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

using Kooco.Framework.Shared.Attributes;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class UserProfileAdminDTO
    {
        public long UID { get; set; }

        public long ButtonsId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Account")]
        public string Account { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Nickname")]
        public string Nickname { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Name")]
        public string Name { get; set; }

        public GenderTypes? Gender { get; set; }

        [SortMapping(PropertyName = "Gender")]
        public string GenderDisplayText
        {
            get
            {
                if (Gender != null)
                    return Utils.GetEnumDisplayName(typeof(GenderTypes), Gender.Value.ToString());
                else
                    return string.Empty;
            }
        }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Email")]
        public string Email { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Phone")]
        public string Phone { get; set; }

        [SortMapping(PropertyName = "UserBase.LastLoginTime")]
        public DateTime? LastLoginTime { get; set; }

        public string City { get; set; }
        public string Location { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? Birthday { get; set; }

        [SortMapping(PropertyName = "Birthday")]
        public string Age
        {
            get
            {
                if (Birthday == null)
                    return "-";
                else
                {
                    int Age = DateTime.Now.Year - Birthday.Value.Year;
                    if (Birthday > DateTime.Now.AddYears(-Age))
                        Age--;

                    return Age.ToString();
                }
            }
        }

        public double PointsTotal { get; set; }
        public int LotteryDailyCoins { get; set; }
        public int LotteryEarnedCoins { get; set; }
        public int LotteryCoinsTotal
        {
            get
            {
                return LotteryDailyCoins + LotteryEarnedCoins;
            }
        }
        public int WatersportsCoinsTotal { get; set; }
        public int WatersportsPricesWon { get; set; }

        public DateTime CreateTime { get; set; }

        public int CompletedCouponsCount { get; set; }
        public int LotteryPricesWon { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserBase.Status")]
        public Kooco.Framework.Models.Enum.UserStatus Status { get; set; }
        
    }
}

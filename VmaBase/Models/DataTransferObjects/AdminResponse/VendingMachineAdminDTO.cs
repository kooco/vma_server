﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class VendingMachineAdminDTO
    {
        public long Id { get; set; }

        [SortMapping(PropertyName = "MOCCPOrganization", AlsoDTOMapping = true)]
        public string Organization { get; set; }

        [SortMapping(PropertyName = "MOCCPAssetNo", AlsoDTOMapping = true)]
        public string AssetNo { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Location { get; set; }

        [SortMapping(PropertyName = "Location")]
        public decimal Longitude { get; set; }

        [SortMapping(PropertyName = "Location")]
        public decimal Latitude { get; set; }

        public bool IsRecommended { get; set; }

        public VendingMachineStatus Status { get; set; }

        [SortMapping(PropertyName = "Status")]
        public string StatusText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(VendingMachineStatus), Status.ToString());
            }
        }

        public VendingMachineAppStatus AppStatus { get; set; }
    }
}

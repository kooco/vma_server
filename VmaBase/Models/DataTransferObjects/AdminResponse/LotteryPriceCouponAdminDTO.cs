﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class LotteryPriceCouponAdminDTO
    {
        public long Id { get; set; }

        public long LotteryGameId { get; set; }

        public long UID { get; set; }

        public long LotteryPriceId { get; set; }

        [SortMapping(PropertyName = "Price.Title", AlsoDTOMapping = true)]
        public string PriceTitle { get; set; }

        [SortMapping(PropertyName = "Price.ImageUrl", AlsoDTOMapping = true)]
        public string PriceImageUrl { get; set; }

        [SortMapping(PropertyName = "Price.UseActivityCoupon")]
        public LotteryPriceExchangeType PriceExchangeType { get; set; }

        [SortMapping(PropertyName = "Price.UseActivityCoupon")]
        public string PriceExchangeTypeText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(LotteryPriceExchangeType), PriceExchangeType.ToString());
            }
        }

        [SortMapping(PropertyName = "UserData.UserBase.Account", AlsoDTOMapping = true)]
        public string UserAccount { get; set; }

        [SortMapping(PropertyName = "UserData.UserBase.Nickname", AlsoDTOMapping = true)]
        public string UserNickname { get; set; }

        [SortMapping(PropertyName = "UserData.UserBase.Name", AlsoDTOMapping = true)]
        public string UserName { get; set; }

        [SortMapping(PropertyName = "UserData.UserBase.Phone", AlsoDTOMapping = true)]
        public string UserPhone { get; set; }

        [SortMapping(PropertyName = "UserData.Location", AlsoDTOMapping = true)]
        public string UserAddress { get; set; }

        public string PickupNumber { get; set; }

        public DateTime TimeWon { get; set; }

        public DateTime? TimeRedeemed { get; set; }

        public LotteryPriceCouponStatus Status { get; set; }

        [SortMapping(PropertyName = "Status")]
        public string StatusText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(LotteryPriceCouponStatus), Status.ToString());
            }
        }
    }
}

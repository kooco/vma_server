﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class AutoCouponCriteriaListEntry
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class StampTypeAdminDTO
    {
        public long Id { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public GeneralStatusEnum Status { get; set; }

        public DateTime DateCreated { get; set; }
    }
}

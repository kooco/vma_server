﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class ActivityParticipatingMachineAdminDTO
    {
        public long Id { get; set; }

        public long VendingMachineId { get; set; }

        public string MOCCPAssetNo { get; set; }

        public string VendingMachineName { get; set; }

        public string VendingMachineLocation { get; set; }

        public GeneralStatusEnum Status { get; set; }

        public VendingMachineAppStatus AppStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class ActivityParticipatingProductAdminDTO
    {
        public long Id { get; set; }

        public long ProductId { get; set; }

        public int InternalId { get; set; }

        public string Volume { get; set; }

        public string MerchandiseId { get; set; }

        public string ProductName { get; set; }

        public string ProductDisplayName { get; set; }

        public string ProductImageUrl { get; set; }

        public string Organization { get; set; }

        public long? StampTypeId1 { get; set; }

        public string StampType1Name { get; set; }

        public string StampType1ImageUrl { get; set; }

        public string VendingMachineIdsAvailable { get; set; }

        public bool Redeemable { get; set; }

        public GeneralStatusEnum Status { get; set; }
    }
}

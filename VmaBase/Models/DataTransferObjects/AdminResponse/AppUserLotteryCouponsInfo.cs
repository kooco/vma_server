﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class AppUserLotteryCouponsInfo
    {
        public long UID { get; set; }
        public int LotteryPricesWon { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class CouponCriteriaMachineAdminDTO
    {
        public long VendingMachineId { get; set; }

        public string VendingMachineName { get; set; }

        public string MOCCPAssetNo { get; set; }

        public string MOCCPOrganization { get; set; }

        public string VendingMachineLocation { get; set; }

        public VendingMachineStatus Status { get; set; }
    }
}

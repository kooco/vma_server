﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;
using VmaBase.Shared;


namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class PromotionOrderAdminDTO
    {
        public long Id { get; set; }

        public PromotionOrderStatus Status { get; set; }

        public long UID { get; set; }

        [SortMapping(PropertyName = "User.UserBase.Account", AlsoDTOMapping = true)]
        public string UserAccount { get; set; }

        [SortMapping(PropertyName = "User.UserBase.Nickname", AlsoDTOMapping = true)]
        public string UserNickname { get; set; }

        [JsonIgnore]
        public string UserName { get; set; }

        [JsonIgnore]
        public string UserAddress { get; set; }

        [JsonIgnore]
        public string UserPhone { get; set; }

        public string OrderNumber { get; set; }

        [SortMapping(PropertyName = "ProductId", AlsoDTOMapping = true)]
        public long PromotionProductId { get; set; }

        [SortMapping(PropertyName = "Product.Name", AlsoDTOMapping = true)]
        public string PromotionProductName { get; set; }

        [SortMapping(PropertyName = "Product.ImageUrl", AlsoDTOMapping = true)]
        public string PromotionProductImageUrl { get; set; }

        [SortMapping(PropertyName = "Product.PayAdditional", AlsoDTOMapping = true)]
        public int PayAdditional { get; set; }

        public int PointsExchanged { get; set; }

        public string OrderNotes { get; set; }

        public PromotionOrderRefundReason RefundReason { get; set; }

        public DateTime ExchangeTime { get; set; }

        public DateTime? PickupTime { get; set; }

        public DateTime? RefundTime { get; set; }

        [SortMapping(PropertyName = "Status")]
        public string StatusText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(PromotionOrderStatus), Status.ToString());
            }
        }
    }
}

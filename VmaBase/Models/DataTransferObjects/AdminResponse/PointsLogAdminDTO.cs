﻿using Kooco.Framework.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class PointsLogAdminDTO
    {
        public long UID { get; set; }

        public string UserAccount { get; set; }

        public string UserNickname { get; set; }

        public PointsTransactionType TransactionType { get; set; }

        public string TransactionTypeText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(PointsTransactionType), TransactionType.ToString());
            }
        }

        public long Amount { get; set; }

        public DateTime TransactionTime { get; set; }

        public string Text { get; set; }

        #region GetPointsTransactionText
        public static string GetPointsTransactionText(PointsTransaction TransactionDb)
        {
            string sText = string.Empty;

            switch(TransactionDb.TransactionType)
            {
                case PointsTransactionType.ApplessVendingProductReward:
                    sText = "買產品" + (TransactionDb.SalesTransaction != null && TransactionDb.SalesTransaction.Product != null ? "【" + TransactionDb.SalesTransaction.Product.Name + "】 (" + TransactionDb.SalesTransaction.Product.MOCCPGoodsId + ")" : string.Empty) +
                            ",直接用販賣機";
                    break;

                case PointsTransactionType.PointsAddedFromAdmin:
                    sText = "在後台送給會員";
                    break;

                case PointsTransactionType.PromotionProductExchange:
                    sText = "兌換到禮品" + (TransactionDb.PromotionOrder != null && TransactionDb.PromotionOrder.Product != null ? "【" + TransactionDb.PromotionOrder.Product.Name + "】" : string.Empty);
                    break;

                case PointsTransactionType.PromotionProductRefund:
                    sText = "在後台退還禮品" + (TransactionDb.PromotionOrder != null && TransactionDb.PromotionOrder.Product != null ? "【" + TransactionDb.PromotionOrder.Product.Name + "】" : string.Empty);
                    break;

                case PointsTransactionType.PromotionReward:
                    sText = "活動" + (TransactionDb.Activity != null ? "【" + TransactionDb.Activity.ActivityText + "】" : string.Empty) + " - 買產品" +
                            (TransactionDb.SalesTransaction != null && TransactionDb.SalesTransaction.Product != null ? "【" + TransactionDb.SalesTransaction.Product.Name + "】 (" + TransactionDb.SalesTransaction.Product.MOCCPGoodsId + ")" : string.Empty);
                    break;

                case PointsTransactionType.ReceivedFromFriend:
                    sText = "好友" + (TransactionDb.ReferenceUser != null && TransactionDb.ReferenceUser.UserBase != null ? "【" + TransactionDb.ReferenceUser.UserBase.Nickname + "】 (" + TransactionDb.ReferenceUser.UserBase.Account + ")" : string.Empty) +
                            "送給他幾分";
                    break;

                default:
                    sText = "Uknown or not implemented yet";
                    break;
            }

            return sText;
        }
        #endregion
    }
}

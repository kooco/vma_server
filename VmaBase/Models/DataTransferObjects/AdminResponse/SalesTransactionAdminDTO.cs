﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;
using VmaBase.Shared;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class SalesTransactionAdminDTO
    {
        public long Id { get; set; }

        public string ReceiptNumber { get; set; }

        public string BuyBuyTransactionNr { get; set; }

        public long? UID { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "User.UserBase.Account")]
        public string UserAccount { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "User.UserBase.Nickname")]
        public string UserNickname { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "User.UserBase.Name")]
        public string UserName { get; set; }

        public PurchaseType PurchaseType { get; set; }

        [SortMapping(PropertyName = "PurchaseType")]
        public string PurchaseTypeText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(PurchaseType), PurchaseType.ToString());
            }
        }

        public long VendingMachineId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "VendingMachine.Name")]
        public string VendingMachineName { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "VendingMachine.MOCCPAssetNo")]
        public string VendingMachineAssetNo { get; set; }

        public long ProductId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.Name")]
        public string ProductName { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "Product.MOCCPGoodsId")]        
        public string ProductMerchandiseId { get; set; }

        public SalesTransactionStatus Status { get; set; }

        [SortMapping(PropertyName = "Status")]
        public string StatusText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(SalesTransactionStatus), Status.ToString());
            }
        }

        public DateTime CreateTime { get; set; }
    }
}

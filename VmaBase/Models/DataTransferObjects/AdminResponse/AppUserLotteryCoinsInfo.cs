﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class AppUserLotteryCoinsInfo
    {
        public long UID { get; set; }
        public int LotteryDailyCoins { get; set; }
        public int LotteryEarnedCoins { get; set; }
        public int WatersportsCoins { get; set; }
    }
}

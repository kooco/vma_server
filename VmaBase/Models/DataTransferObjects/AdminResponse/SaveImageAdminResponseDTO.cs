﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class SaveImageAdminResponseDTO
    {
        public string ImageUrl { get; set; }

        public string ListImageUrl { get; set; }

        public string ActivityListImageUrl { get; set; }
    }
}

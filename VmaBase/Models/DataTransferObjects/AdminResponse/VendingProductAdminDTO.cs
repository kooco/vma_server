﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class VendingProductAdminDTO
    {
        public long Id { get; set; }

        public string MOCCPGoodsId { get; set; }

        public string Name { get; set; }

        public string NameEN { get; set; }

        public string Volume { get; set; }

        public long ProductTypeId { get; set; }

        public long ProductBrandId { get; set; }

        public string ImageUrl { get; set; }

        public double AveragePrice { get; set; }

        public long? DefaultStampTypeId { get; set; }

        public string DefaultStampTypeImageUrl { get; set; }

        public int PointsExtra { get; set; }

        public int PointsMultiplier { get; set; }

        public int StampsMultiplier { get; set; }

        public VendingProductStatus Status { get; set; }

        public long? ActivityId { get; set; }

        public long? ProductActivityId { get; set; }
    }
}

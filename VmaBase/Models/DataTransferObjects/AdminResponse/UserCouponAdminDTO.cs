﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Models.DataTransferObjects.AdminResponse
{
    public class UserCouponAdminDTO
    {
        public long Id { get; set; }

        public long? ActivityId { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "AutoCouponCriteriaId")]
        public long? CriteriaId { get; set; }

        public UserCouponType Type { get; set; }

        [SortMapping(PropertyName = "Type")]
        public string TypeDisplayText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(UserCouponType), Type.ToString());
            }
        }

        public long UID { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserData.UserBase.Account")]
        public string UserAccount { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserData.UserBase.Nickname")]
        public string UserNickname { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "UserData.UserBase.Name")]
        public string UserName { get; set; }

        public long SenderUID { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "SenderUserData.UserBase.Account")]
        public string SenderUserAccount { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "SenderUserData.UserBase.Nickname")]
        public string SenderUserNickname { get; set; }

        [SortMapping(AlsoDTOMapping = true, PropertyName = "SenderUserData.UserBase.Name")]
        public string SenderUserName { get; set; }

        public string ActivityName { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? CompletionTime { get; set; }

        public DateTime? RedeemTime { get; set; }

        public DateTime ExpiryTime { get; set; }

        public UserCouponStatus Status { get; set; }

        [SortMapping(PropertyName = "Status")]
        public string StatusDisplayText
        {
            get
            {
                return Utils.GetEnumDisplayName(typeof(UserCouponStatus), Status.ToString());
            }
        }
    }
}

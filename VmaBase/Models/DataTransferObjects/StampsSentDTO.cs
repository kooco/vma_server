﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

namespace VmaBase.Models.DataTransferObjects
{
    public class StampsSentDTO
    {
        [JsonProperty(PropertyName = "stampsCountSent")]
        [SwaggerDescription(Description = "Amount of stamps successfully sent to the friend user", Example = "#int:2")]
        public int StampsCountSent { get; set; }

        [JsonProperty(PropertyName = "stampsCountNotSent")]
        [SwaggerDescription(Description = "Amount of stamps NOT sent to the friend user, because this user already have enough of this stamp type", Example = "#int:1")]
        public int StampsCountNotSent { get; set; }

        [JsonProperty(PropertyName = "stampsSent")]
        [SwaggerDescription(Description = "List of stamp types successfully sent to the friend user")]
        public List<SendStampTypeDTO> StampsSent { get; set; }

        [JsonProperty(PropertyName = "stampsNotSent")]
        [SwaggerDescription(Description = "List of stamp types NOT sent to the friend user (because the user already have it)")]
        public List<SendStampTypeDTO> StampsNotSent { get; set; }

        [JsonProperty(PropertyName = "sentToFriendInfo")]
        [SwaggerDescription(Description = "Contains information about the sent stamps and points the user has sent to this friend in sum")]
        public SentToFriendInfoDTO SentToFriendInfo { get; set; }
    }
}

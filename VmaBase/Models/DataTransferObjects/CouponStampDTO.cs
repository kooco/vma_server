﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class CouponStampDTO : StampTypeDTO
    {
        [JsonProperty(PropertyName = "stampTypeId")]
        [SwaggerDescription(Description = "Id of the stamp type which needs to be collected", Example = "#int:1")]
        public long? StampTypeId { get; set; }

        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "The id of the activity this stamp is assigned to", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "status")]
        [SwaggerDescription(Description = "Status of the stamp for this coupon.\n0 = not yet collected (empty)\n1 = collected (or received by friend)", Example = "#int:0")]
        public CouponStampStatus Status { get; set; }

        [JsonProperty(PropertyName = "receivedByAccount")]
        [SwaggerDescription(Description = "Account id of the user who sent this stamp to this user (sent as a gift).\nnull or blank = collected by this user him/herself", Example = "#null")]
        public string ReceivedByAccount { get; set; }

        [JsonProperty(PropertyName = "receivedByNickname")]
        [SwaggerDescription(Description = "Nickname of the user who sent this stamp (sent as a gift).\nnull or blank = collected by this user him/herself", Example = "#null")]
        public string ReceivedByNickname { get; set; }

        [JsonProperty(PropertyName = "dateCollected")]
        [SwaggerDescription(Description = "The date and time this sticker has been collected at.")]
        public DateTime? DateCollected { get; set; }
    }
}

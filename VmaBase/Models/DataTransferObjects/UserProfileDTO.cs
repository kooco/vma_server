using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class UserProfileDTO : Kooco.Framework.Models.DataTransferObjects.APIUserProfileDTO<ApplicationDbContext>
    {
        #region SessionToken (override)
        [JsonProperty(PropertyName = "sessionToken")]
        [SwaggerDescription(Description = "sessionToken retrieved by the user_login API", Example = "exb3b6bb-1875-4a1f-8c5c-ee3ac150a7db", InputOnly = true)]
        public new string SessionToken
        {
            get
            {
                return base.SessionToken;
            }
            set
            {
                base.SessionToken = value;
            }
        }
        #endregion

        [JsonProperty("userId")]
        [SwaggerDescription(Description = "Unique id of the user (UID)", Example = "#int:6", OutputOnly = true)]
        public long UID { get; set; }

        [JsonProperty("accountIdRecommendedBy")]
        [SwaggerDescription(Description = "Account id of the user who recommended this user for registration. ONLY for registration, NOT for update!\nThis user will get a (non-expiring = 'earned') lottery coin then", Example = "12345678", InputOnly = true)]
        public string AccountIdRecommendedBy { get; set; }

        [JsonProperty(PropertyName = "gender")]
        [SwaggerDescription(Description = "-1 = not specified\n0 = male\n1 = female", Example = "#int:0")]
        public GenderTypes? Gender { get; set; }

        [JsonProperty(PropertyName = "account")]
        [SwaggerDescription(Description = "Account id (account name) of the user (auto-generated).", Example = "#int:12345678", OutputOnly = true)]
        public new string Account { get; set; }

        [JsonProperty(PropertyName = "macauPassId", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "Macao pass id for this user (optional)", Example = "12345678", InputOnly = true)]
        public string MacauPassId { get; set; }

        [JsonProperty(PropertyName = "city")]
        [SwaggerDescription(Description = "Name of the city the user lives in", Example = "Macao")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "location")]
        [SwaggerDescription(Description = "Current location of the user (address/place)", Example = "My Place")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        [SwaggerDescription(Description = "Latitude - coordinate part 'Latitude' of the current user's position.\nInput: passing this parameter as a string also will be accepted!", Example = "22.158242")]
        public decimal? Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        [SwaggerDescription(Description = "Longitude - coordinate part 'Longitude' of the current user's position\nInput: passing this parameter as a string also will be accepted!", Example = "113.549457")]
        public decimal? Longitude { get; set; }

        [JsonProperty(PropertyName = "imageUrl", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "Url of the user profile image", Example = "http://shanghai.cdn.aliyun.com/vmapictures/myprofile001.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "birthday")]
        [SwaggerDescription(Description = "Birthdate of the user", Example = "#date:1988-02-03")]
        public DateTime? Birthday { get; set; }

        [JsonProperty(PropertyName = "pointsTotal", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The current amoint of points the user has.", Title = "#int:0", OutputOnly = true)]
        public double PointsTotal { get; set; }

        [JsonProperty(PropertyName = "stampsTotal", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The current amoint of stamps (total, [u]no matter the type of stamp[/u]) the user has.", Title = "#int:0", OutputOnly = true)]
        public double StampsTotal { get; set; }

        [JsonProperty(PropertyName = "qrToken", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The QR token to be used with this user for generating a QR code for this user\nWith this QR token a friend can be added to the friend list", Example = "USER_1893894")]
        public string QRToken
        {
            get
            {
                return "USER_" + Account;
            }
        }

        [JsonProperty(PropertyName = "sentToPoints", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The amount of points the current user ever has sent to this user", Example = "#int:20")]
        public int? SentToPoints { get; set; }

        [JsonProperty(PropertyName = "sentToStamps", NullValueHandling = NullValueHandling.Ignore)]
        [SwaggerDescription(Description = "The amount of stamps the current user ever has sent to this user", Example = "#int:2")]
        public int? SentToStamps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataStorage;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class SentToFriendInfoDTO
    {
        [JsonProperty(PropertyName = "points")]
        [SwaggerDescription(Description = "Amount of points sent to this friend in total", Example = "#int:200")]
        public int Points { get; set; }

        [JsonProperty(PropertyName = "stamps")]
        [SwaggerDescription(Description = "Amount of stamps sent to this friend in total", Example = "#int:5")]
        public int Stamps { get; set; }

        public SentToFriendInfoDTO()
        {
            Points = 0;
            Stamps = 0;
        }

        public SentToFriendInfoDTO(ApplicationDbContext DbContext, long UID, long FriendUID)
        {
            List<int> ResultPoints = 
                DbContext.Database.SqlQuery<int>("SELECT Cast(IsNull(Sum([Amount]), 0) as int) FROM [dbo].[PointsTransactions] WHERE [UID] = @UID AND [TransactionType] = @TransactionType AND [ReferenceUserId] = @FriendUID",
                new SqlParameter("@UID", UID),
                new SqlParameter("@TransactionType", ((int)PointsTransactionType.SentToFriend).ToString()),
                new SqlParameter("@FriendUID", FriendUID)).ToList();

            List<int> ResultStamps =
                DbContext.Database.SqlQuery<int>("SELECT Cast(IsNull(Count([UserStampId]), 0) as int) FROM [dbo].[UserStampLogs] log WHERE [UID] = @UID AND [LogType] = @StampLogType AND [UserReferenceId] = @FriendUID",
                new SqlParameter("@UID", UID),
                new SqlParameter("@StampLogType", ((int)UserStampLogType.SentToFriend).ToString()),
                new SqlParameter("@FriendUID", FriendUID)).ToList();

            Stamps = ResultStamps.Count > 0 ? ResultStamps[0] : 0;
            Points = ResultPoints.Count > 0 ? -(ResultPoints[0]) : 0;
        }
    }
}

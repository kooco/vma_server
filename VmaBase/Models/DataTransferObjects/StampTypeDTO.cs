﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;


namespace VmaBase.Models.DataTransferObjects
{
    public class StampTypeDTO
    {
        [JsonProperty(PropertyName = "number")]
        [SwaggerDescription(Description = "Unique number of this stamp type. This number is always 2 digits long (01, 02, ..) and will be used for building the image filename of the stamp type", Example = "01")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "name")]
        [SwaggerDescription(Description = "Nmae of the stamp type")]
        public string Name { get; set; }
    }
}

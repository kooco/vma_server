﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kooco.Framework.Shared.Attributes;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects
{
    public class VendingMachineDTO
    {
        [JsonProperty(PropertyName = "id")]
        [SwaggerDescription(Description = "Id of the vending machine", Example = "#int:1")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "assetNo")]
        //[JsonIgnore]
        [SwaggerDescription(Description = "Unique 'asset number' from the BuyBuy system", Example = "SVB-001")]
        public string MOCCPAssetNo { get; set; }

        [JsonProperty(PropertyName = "org")]
        //[JsonIgnore]
        [SwaggerDescription(Description = "Organization this machine belongs to (part of the BuyBuy API data)", Example = "#null")]
        public string MOCCPOrganization { get; set; }

        [JsonProperty(PropertyName = "name")]
        [SwaggerDescription(Description = "Name of the vending machine", Example = "南京古林路口")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "city")]
        [SwaggerDescription(Description = "City, the vending machine is placed in", Example = "澳門")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "location")]
        [SwaggerDescription(Description = "The location (address) of the vending machine", Example = "澳門廣東大馬路231號")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        [SwaggerDescription(Description = "The latitude position of the vending machine", Example = "22.158822")]
        public decimal Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        [SwaggerDescription(Description = "The longitude position of the vending machine", Example = "113.550127")]
        public decimal Longitude { get; set; }

        [JsonProperty(PropertyName = "distanceMeters")]
        [SwaggerDescription(Description = "The distance of this vending machine from the current users position in meters or the given search position (parameters 'longitude' and 'latitude').\nThis only will work if the user position is periodically updated with the users/location_update API OR the location to search for is sent with the API call !", Example = "#int:38")]
        public long DistanceMeters { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        //[JsonIgnore]
        [SwaggerDescription(Description = "Url to the image of this vending machine", Example = "http://vmatest-shanghai.oss.aliyun.com/myvendingmachine_x1.jpg")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "activityId")]
        [SwaggerDescription(Description = "Id of the activity this machine is part of. ONLY returned if the parameter 'onlyRecommended' is set to true! Otherwise this value will be always null.", Example = "#int:1")]
        public long? ActivityId { get; set; }

        [JsonProperty(PropertyName = "activityTitle")]
        //[JsonIgnore]
        [SwaggerDescription(Description = "Title of the activity this machine is part of. ONLY returned if the parameter 'onlyRecommended' is set to true! Otherwise this value will be always null.", Example = "My activity")]
        public string ActivityTitle { get; set; }

        [JsonProperty(PropertyName = "qrToken", NullValueHandling = NullValueHandling.Ignore)]
        //[JsonIgnore]
        public string QRToken { get; set; }

        [JsonProperty(PropertyName = "recommended")]
        [SwaggerDescription(Description = "true = this machine is a recommended one (part of an activity)\nfalse = this machine is a 'normal' one", Example = "#bool:false")]
        public bool IsRecommended { get; set; }

        [JsonProperty(PropertyName = "status")]
        [SwaggerDescription(Description = "Status of the vending machine.\n1 = Active, 0 = not ready yet, -3 = Sold out\nEvery number below 1 means: this vending machine (currently) can not be used!", Example = "#int:1")]
        public VendingMachineStatus Status { get; set; }
    }
}

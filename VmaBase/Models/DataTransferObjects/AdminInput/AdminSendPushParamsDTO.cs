﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class AdminSendPushParamsDTO
    {
        public long? UID { get; set; }
        public string Message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class GetProductChoiceListParamsDTO
    {
        public string VendingMachineIdsFilter { get; set; }

        public bool ForAutoCoupons { get; set; }

        public bool AllMachines { get; set; }

        public GetProductChoiceListParamsDTO()
        {
            VendingMachineIdsFilter = string.Empty;
            ForAutoCoupons = false;
            AllMachines = false;
        }
    }
}

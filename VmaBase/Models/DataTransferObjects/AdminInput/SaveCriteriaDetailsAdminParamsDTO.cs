﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class SaveCriteriaDetailsAdminParamsDTO
    {
        public long CriteriaId { get; set; }

        public AutoCouponCriteriaStatus Status { get; set; }

        public bool CouponsForAllProducts { get; set; }

        public bool CouponsForAllMachines { get; set; }

        public DateTime? CouponsExpiryDate { get; set; }

        public List<CouponCriteriaProductAdminDTO> Products { get; set; }

        public List<CouponCriteriaMachineAdminDTO> Machines { get; set; }
    }
}

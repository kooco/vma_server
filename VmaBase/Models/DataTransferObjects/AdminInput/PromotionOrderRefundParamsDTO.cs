﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class PromotionOrderRefundParamsDTO
    {
        public long PromotionOrderId { get; set; }

        public PromotionOrderRefundReason RefundReason { get; set; }

        public string OrderNotes { get; set; }
    }
}

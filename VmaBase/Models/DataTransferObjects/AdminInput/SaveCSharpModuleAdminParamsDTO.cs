﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class SaveCSharpModuleAdminParamsDTO
    {
        public long CriteriaId { get; set; }

        public bool NewModule { get; set; }

        public long? ModuleId { get; set; }

        public string ModuleName { get; set; }

        public string CSharpCode { get; set; }

        public bool Activate { get; set; }
    }
}

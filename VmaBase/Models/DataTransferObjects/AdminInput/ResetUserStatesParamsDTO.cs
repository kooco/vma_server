﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class ResetUserStatesParamsDTO
    {
        public long CriteriaId { get; set; }

        public string Account { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects.AdminInput
{
    public class CheckCSharpModuleParamsDTO
    {
        public long CriteriaId { get; set; }

        public string ModuleCode { get; set; }

        public string ModuleName { get; set; }

        public long TestuserUID { get; set; }

        public bool SaveOnSuccess { get; set; }

        public bool CreateNewModule { get; set; }

        public long ModuleId { get; set; }
    }
}

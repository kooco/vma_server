﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmaBase.Models.DataTransferObjects
{
    public class CouponSelectionEntryDTO
    {
        public long ActivityId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public DateTime LimitStartTime { get; set; }
        public DateTime LimitEndTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using VmaBase.Models.Enum;

namespace VmaBase.Models
{
    public class PushAction : Kooco.Framework.Models.PushAction
    {
        public static Kooco.Framework.Models.PushAction Create(PushActionCode Code, string Parameter, params KeyValuePair<string, object>[] AdditionalParameters)
        {
            return Kooco.Framework.Models.PushAction.Create((int)Code, Parameter, AdditionalParameters);
        }
    }
}

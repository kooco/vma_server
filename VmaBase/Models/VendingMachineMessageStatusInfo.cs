﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models
{
    public class VendingMachineMessageStatusInfo
    {
        public long Id { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime CurrentSqlServerTime { get; set; }
        public VendingMachineMessageStatus Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.Enum;

namespace VmaBase.Models
{
    public class QRActionInfo
    {
        public QRActionType ActionType { get; set; }
        public string Id { get; set; }
        public bool IsValid { get; set; }
    }
}

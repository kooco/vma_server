﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataStorage;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaBase.Providers.Admin
{
    public class BaseListDataProvider<TDbEntity, TDTOEntity> : Kooco.Framework.Providers.BaseListDataProvider<ApplicationDbContext, TDbEntity, TDTOEntity>
        where TDbEntity : class
    {
        #region Constructor
        public BaseListDataProvider() : base()
        {

        }
        #endregion

        #region DbContext
        public new ApplicationDbContext DbContext
        {
            get
            {
                return (ApplicationDbContext)base.DbContext;
            }
        }
        #endregion

        #region GetPagedList
        public new BaseResponseDTO GetPagedList(StandardSearchParamsDTO<TDTOEntity> Params, IQueryable<TDbEntity> Query)
        {
            return BaseResponseDTO.FromFrameworkResponse(base.GetPagedList(Params, Query));
        }
        #endregion

        #region ResultObject
        public new BaseResponseDTO ResultObject(object objResult)
        {
            return new BaseResponseDTO(base.ResultObject(objResult));
        }
        public FrameworkResponseDTO FrameworkResultObject(object objResult)
        {
            return base.ResultObject(objResult);
        }
        #endregion
        #region Ok
        public new BaseResponseDTO Ok()
        {
            return new BaseResponseDTO(ReturnCodes.Success);
        }
        #endregion
        #region Error
        public BaseResponseDTO Error(ReturnCodes ErrorCode)
        {
            return new BaseResponseDTO(ErrorCode);
        }
        public BaseResponseDTO Error(ReturnCodes ErrorCode, string ErrorMessage)
        {
            return new BaseResponseDTO(ErrorCode, ErrorMessage);
        }
        #endregion
    }
}

﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.Enum;

namespace VmaBase.Providers
{
    /// <summary>
    ///     Special provider for vending products with a selected activity (activity settings included)
    /// </summary>
    public class VendingActivityProductAdminProvider : BaseListDataProvider<VendingProductActivitySetting, VendingProductActivityAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<VendingProductAdminDTO> Params)
        {            
            PermissionsProvider.RequestAdminRole();

            StandardSearchParamsDTO<VendingProductActivityAdminDTO> SearchParamsConverted = new StandardSearchParamsDTO<VendingProductActivityAdminDTO>();
            SearchParamsConverted.Filter = Params.Filter;
            SearchParamsConverted.MaxItems = Params.MaxItems;
            SearchParamsConverted.OrderBy = Params.OrderBy;
            SearchParamsConverted.Page = Params.Page;

            long? ActivityIdFilter = null;
            foreach (StandardSearchFilterCriteria Criteria in Params.FilterCriterias)
            {
                if (Criteria.DTOPropertyName == "ActivityId")
                    ActivityIdFilter = Convert.ToInt64(Criteria.Value);
            }

            if (ActivityIdFilter == null)
                throw new Exception("If using the VendingActivityProductAdminProvider (Procuts with activity settings) the ActivityId must be filtered!");

            // create missing activity settings first, to ensure that all products are having activity settings for the given activity
            string sSQL = "INSERT INTO [dbo].[VendingProductActivitySettings] ([ProductId], [ActivityId], [PointsMultiplier], [StampsMultiplier], [PointsExtra])" +
                                                                    " SELECT [Id], " + ActivityIdFilter.Value.ToString() + ", [PointsMultiplier], [StampsMultiplier], [PointsExtra]" +
                                                                    " FROM [dbo].[VendingProducts] WHERE [Status] <> " + ((int)VendingProductStatus.Deleted).ToString() +
                                                                    "  AND NOT Exists(SELECT 1 FROM [dbo].[VendingProductActivitySettings] setq where setq.[ProductId] = [VendingProducts].[Id] AND [ActivityId] = " + ActivityIdFilter.Value.ToString() + ")";
            DbContext.Database.ExecuteSqlCommand(sSQL);

            // query products with activity settings
            IQueryable<VendingProductActivitySetting> Query = DbContext.VendingProductActivitySettings.Include(itm => itm.Product).Include(itm => itm.Product.DefaultStampType).Where(itm => itm.Product.Status != Models.Enum.VendingProductStatus.Deleted);

            BaseResponseDTO Response = GetPagedList(SearchParamsConverted, Query);
            if (Response.ReturnCode == Shared.ReturnCodes.Success)
            {
                List<VendingProductActivityAdminDTO> DataList = (List<VendingProductActivityAdminDTO>)Response.Data;
                Response.Data = DataList.Select(Mapper.Map<VendingProductActivityAdminDTO, VendingProductAdminDTO>).ToList();
            }

            return Response;
        }

        public long GetProductIdByActivityProductSettingId(long Id)
        {
            VendingProductActivitySetting SettingDb = DbContext.VendingProductActivitySettings.Find(Id);
            if (SettingDb == null)
                throw new Exception("There is no VendingProduct Activity-Setting with the id " + Id.ToString() + "!");

            return SettingDb.ProductId;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Web.Mvc;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Models.DataTransferObjects.AdminResponse;


namespace VmaBase.Providers
{
    /// <summary>
    ///     AppUsersAdminProvider
    ///     ---------------------
    ///     
    ///     Provider class for managing app users
    ///     for: Admin Interface
    /// </summary>
    public class AppUsersAdminProvider : BaseListDataProvider<UserData, UserProfileAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<UserProfileAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<UserData> Query = DbContext.UserDatas.Include(itm => itm.UserBase);
            return GetPagedList(Params, Query, DataPrepared, HandleSearchCriterias);
        }

        private void HandleSearchCriterias(ref IQueryable<UserData> Query, ref StandardSearchParamsDTO<UserProfileAdminDTO> Params)
        {
            // because we have no age in the database but a birthday
            // we have to convert the age filter to a birthday filter
            StandardSearchFilterCriteria CriteriaAgeFrom = Params.FilterCriterias.Where(itm => itm.DTOPropertyName == "Age" && itm.SearchType == StandardSearchFilterTypes.GreaterOrEqualThan).FirstOrDefault();
            StandardSearchFilterCriteria CriteriaAgeTo = Params.FilterCriterias.Where(itm => itm.DTOPropertyName == "Age" && itm.SearchType == StandardSearchFilterTypes.LessOrEqualThan).FirstOrDefault();

            if (CriteriaAgeFrom != null)
            {
                Params.FilterCriterias.Add(new StandardSearchFilterCriteria() {
                    DTOPropertyName = "Birthday",
                    DbPropertyName = "Birthday",
                    SearchType = StandardSearchFilterTypes.LessOrEqualThan,
                    PropertyType = typeof(DateTime?),
                    Value = DateTime.Now.Date.AddYears(-Convert.ToInt32(CriteriaAgeFrom.Value))
                });

                Params.FilterCriterias.Remove(CriteriaAgeFrom);
            }

            if (CriteriaAgeTo != null)
            {
                Params.FilterCriterias.Add(new StandardSearchFilterCriteria()
                {
                    DTOPropertyName = "Birthday",
                    DbPropertyName = "Birthday",
                    SearchType = StandardSearchFilterTypes.GreaterOrEqualThan,
                    PropertyType = typeof(DateTime?),
                    Value = DateTime.Now.Date.AddYears(-Convert.ToInt32(CriteriaAgeTo.Value)).AddYears(-1).AddDays(1)
                });

                Params.FilterCriterias.Remove(CriteriaAgeTo);
            }
        }

        public ActionResult GetExcel()
        {
            IQueryable<UserData> Query = DbContext.UserDatas.Include(itm => itm.UserBase);
            return GetExcel(Query, DataPrepared, HandleSearchCriterias);
        }

        public Kooco.Framework.Shared.FrameworkReturnCodes DataPrepared(ref List<UserProfileAdminDTO> UserList)
        {
            List<long> UIDList = UserList.Select(itm => itm.UID).ToList();
            UserProfileAdminDTO UserEntry = null;
            string sUIDList = string.Join(",", UIDList.ToArray());
            if (sUIDList.EndsWith(","))
                sUIDList = sUIDList.Substring(1, sUIDList.Length - 1);
            if (string.IsNullOrWhiteSpace(sUIDList))
                sUIDList = "0";

            // add coupons count to user profiles
            List<AppUserActivityInfo> CouponsInfoList =
                DbContext.Database.SqlQuery<AppUserActivityInfo>(
                    @"SELECT usrbase.[UID], IsNull(Count(usrcpn.[Id]), 0) [CompletedCouponsCount]
                        FROM [dbo].[UserBases] usrbase
                  INNER JOIN [dbo].[UserCoupons] usrcpn ON usrcpn.[UID] = usrbase.[UID] AND usrcpn.[Status] = " + ((int)UserCouponStatus.Completed).ToString() + @"
                  INNER JOIN [dbo].[CouponActivities] act ON act.[Id] = usrcpn.[ActivityId]
                  INNER JOIN [dbo].[NewsArticles] news ON news.[ActivityId] = act.[Id]
                  INNER JOIN [dbo].[NewsCategories] cat ON cat.[Id] = news.[CategoryId]
                       WHERE usrbase.[UID] IN (" + sUIDList + @")
                         AND act.[Status] = 1
                         AND cat.[Code] = '" + GlobalConst.NewsCategory_Activities + @"'
                         AND getutcdate() >= news.[LimitStartTime] AND getutcdate() <= news.[LimitEndTime]
                         AND news.[Status] <> -1
                    GROUP BY usrbase.[UID]").ToList();

            foreach (AppUserActivityInfo ActivityInfo in CouponsInfoList)
                UserList.First(itm => itm.UID == ActivityInfo.UID).CompletedCouponsCount = ActivityInfo.CompletedCouponsCount;

            // add lottery prices won to user profiles
            string sLotteryCouponsSQL = @"SELECT usrbase.[UID], IsNull(Count(cpn.[Id]), 0) [LotteryPricesWon]
                        FROM [dbo].[UserBases] usrbase
                  INNER JOIN [dbo].[LotteryPriceCoupons] cpn ON cpn.[UID] = usrbase.[UID] AND cpn.[Status] IN (" + ((int)LotteryPriceCouponStatus.Won) + ", " + ((int)LotteryPriceCouponStatus.PickedUp) + @")
                  INNER JOIN [dbo].[LotteryGames] game ON game.[Id] = cpn.[LotteryGameId]
                       WHERE usrbase.[UID] IN (" + sUIDList + @")
                         AND getutcdate() >= (case when game.[TestMode] = 1 then game.[TestStartTime] else game.[StartTime] end)
                         AND getutcdate() <= game.[EndTime]
                         AND cpn.[TimeWon] >= '2018-10-01'
                         AND game.[Status] <> " + ((int)LotteryGameStatus.Finished).ToString() + @"
                         AND game.[Id] = @LotteryGameId
                    GROUP BY usrbase.[UID]";

            List<AppUserLotteryCouponsInfo> LotteryInfoList =
                DbContext.Database.SqlQuery<AppUserLotteryCouponsInfo>(
                    sLotteryCouponsSQL.Replace("@LotteryGameId", GlobalConst.LotteryGameId_StandardLottery.ToString())).ToList();

            foreach (AppUserLotteryCouponsInfo LotteryInfo in LotteryInfoList)
                UserList.First(itm => itm.UID == LotteryInfo.UID).LotteryPricesWon = LotteryInfo.LotteryPricesWon;

            // add lottery and watersports coins info to the user profiles
            List<AppUserLotteryCoinsInfo> LotteryCoinsList =
                DbContext.Database.SqlQuery<AppUserLotteryCoinsInfo>(
                    @"select [UID], 
	                         Sum(case when [LotteryGameId] = 1 AND [Type] = 0 then [Amount] else 0 end) LotteryEarnedCoins,
	                         Sum(case when [LotteryGameId] = 1 AND [Type] = 1 then [Amount] else 0 end) LotteryDailyCoins,
	                         Sum(case when [LotteryGameId] = 2 then [Amount] else 0 end) WatersportsCoins 
                        from dbo.UserLotteryCoins 
                       where [UID] in (" + sUIDList + @")
                    group by [UID]").ToList();

            foreach (AppUserLotteryCoinsInfo CoinsInfo in LotteryCoinsList)
            {
                UserEntry = UserList.First(itm => itm.UID == CoinsInfo.UID);
                UserEntry.LotteryDailyCoins = CoinsInfo.LotteryDailyCoins;
                UserEntry.LotteryEarnedCoins = CoinsInfo.LotteryEarnedCoins;
                UserEntry.WatersportsCoinsTotal = CoinsInfo.WatersportsCoins;
            }

            // add watersports prices won to user profiles
            LotteryInfoList = DbContext.Database.SqlQuery<AppUserLotteryCouponsInfo>(
                    sLotteryCouponsSQL.Replace("@LotteryGameId", GlobalConst.LotteryGameId_Watersports.ToString())).ToList();

            foreach (AppUserLotteryCouponsInfo LotteryInfo in LotteryInfoList)
                UserList.First(itm => itm.UID == LotteryInfo.UID).WatersportsPricesWon = LotteryInfo.LotteryPricesWon;

            return Kooco.Framework.Shared.FrameworkReturnCodes.Success;
        }

        public BaseResponseDTO AddPoints(AppUserAddPointsAdminParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");

            DbContext.SP_AddUserPoints(Params.UID, Params.PointsToAdd, PointsTransactionType.PointsAddedFromAdmin, null, null, null);
            int NewPoints = (int) DbContext.UserDatas.Where(itm => itm.UID == Params.UID).Select(itm => itm.PointsTotal).FirstOrDefault();

            return ResultObject(NewPoints);
        }

        public BaseResponseDTO AddLotteryCoins(AppUserAddCoinsAdminParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");

            LotteryGameProvider GameProvider = new LotteryGameProvider();
            TestAddLotteryCoinsParamsDTO AddCoinsParams = new TestAddLotteryCoinsParamsDTO();
            AddCoinsParams.AppToken = GlobalConst.ApiKey;
            AddCoinsParams.UID = Params.UID;
            AddCoinsParams.LotteryGameId = GlobalConst.LotteryGameId_StandardLottery;
            AddCoinsParams.Coins = Params.CoinsToAdd;

            BaseResponseDTO Response = GameProvider.TestAddLotteryCoins(AddCoinsParams);
            if (Response.ReturnCode == ReturnCodes.Success)
                return ResultObject(DbContext.UserLotteryCoins.Where(itm => itm.UID == Params.UID && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery && itm.Status == LotteryCoinStatus.Available).Select(itm => itm.Amount).DefaultIfEmpty(0).Sum());
            else
                return Response;
        }

        public BaseResponseDTO AddWatersportsCoins(AppUserAddCoinsAdminParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");

            LotteryGameProvider GameProvider = new LotteryGameProvider();
            TestAddLotteryCoinsParamsDTO AddCoinsParams = new TestAddLotteryCoinsParamsDTO();
            AddCoinsParams.AppToken = GlobalConst.ApiKey;
            AddCoinsParams.UID = Params.UID;
            AddCoinsParams.LotteryGameId = GlobalConst.LotteryGameId_Watersports;
            AddCoinsParams.Coins = Params.CoinsToAdd;

            BaseResponseDTO Response = GameProvider.TestAddLotteryCoins(AddCoinsParams);
            if (Response.ReturnCode == ReturnCodes.Success)
                return ResultObject(DbContext.UserLotteryCoins.Where(itm => itm.UID == Params.UID && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports && itm.Status == LotteryCoinStatus.Available).Select(itm => itm.Amount).DefaultIfEmpty(0).Sum());
            else
                return Response;
        }

        public BaseResponseDTO GetActivityDropDownList()
        {
            List<CouponActivityListEntryDTO> ActivityList =
                DbContext.NewsArticles.Include(itm => itm.Category).Include(itm => itm.Activity)
                    .Where(itm => itm.Status == NewsStatus.Online &&
                                  DateTime.UtcNow >= itm.LimitStartTime && DateTime.UtcNow <= itm.LimitEndTime &&
                                  itm.Category.Code == GlobalConst.NewsCategory_Activities &&
                                  itm.Activity.Status == ActivityStatus.Active &&
                                  itm.Activity.NoStampsActivity == false)
                    .Select(itm => new CouponActivityListEntryDTO() { Id = itm.Activity.Id, Title = itm.Activity.Title }).ToList();

            return ResultObject(ActivityList);
        }

        public BaseResponseDTO GetUserActivityInfo(GetUserActivityInfoParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");
            if (Params.ActivityId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'ActivityId' is missing!");

            UserCoupon CurrentCouponDb = DbContext.UserCoupons.Where(itm => itm.UID == Params.UID && itm.ActivityId == Params.ActivityId && itm.Status == UserCouponStatus.Collecting).FirstOrDefault();

            UserActivityInfoDTO ResponseDTO = new UserActivityInfoDTO();
            ResponseDTO.CompletedCouponsCount = DbContext.UserCoupons.Where(itm => itm.UID == Params.UID && itm.ActivityId == Params.ActivityId && itm.Status == UserCouponStatus.Completed).Count();
            ResponseDTO.CurrentCouponCollectedStamps = (CurrentCouponDb == null ? 0 : DbContext.UserCouponStamps.Where(itm => itm.UserCouponId == CurrentCouponDb.Id && (itm.Status == UserStampStatus.Collected || itm.Status == UserStampStatus.ReceivedByFriend)).Count());

            return ResultObject(ResponseDTO);
        }

        public BaseResponseDTO AddStampToUser(AddStampToUserAdminParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");
            if (Params.ActivityId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'ActivityId' is missing!");
            if (Params.StampTypeId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'stampTypeId' is missing!");

            if (DbContext.UserDatas.Where(itm => itm.UID == Params.UID).Select(itm => itm.UID).FirstOrDefault() != Params.UID)
                return Error(ReturnCodes.RecordNotFound, "There is no user with the UID " + Params.UID.ToString() + " in the database!");
            if (DbContext.CouponActivities.Where(itm => itm.Id == Params.ActivityId && itm.Status != ActivityStatus.Deleted).Select(itm => itm.Id).FirstOrDefault() != Params.ActivityId)
                return Error(ReturnCodes.RecordNotFound, "There is no activity with the Id " + Params.ActivityId.ToString() + " in the database!");
            if (DbContext.StampTypes.Where(itm => itm.Id == Params.StampTypeId).Select(itm => itm.Id).FirstOrDefault() != Params.StampTypeId)
                return Error(ReturnCodes.RecordNotFound, "There is no stamp type with the Id " + Params.StampTypeId.ToString() + " in the database!");

            // get necesary amount of stamps
            int StampsCountNeeded = DbContext.CouponStampDefinitions.Where(itm => itm.ActivityId == Params.ActivityId).Count();

            // get current open activity coupon
            UserCoupon CurrentCouponDb = DbContext.UserCoupons.Where(itm => itm.UID == Params.UID && itm.ActivityId == Params.ActivityId && itm.Status == UserCouponStatus.Collecting).FirstOrDefault();
            if (CurrentCouponDb == null)
            {
                // if no currently collecting coupon exists so far -> create one
                CurrentCouponDb = new UserCoupon();
                CurrentCouponDb.ActivityId = Params.ActivityId;
                CurrentCouponDb.ExpiryTime = DbContext.CouponActivities.Where(itm => itm.Id == Params.ActivityId).Select(itm => itm.LimitEndTime).First();
                CurrentCouponDb.StartTime = DateTime.UtcNow;
                CurrentCouponDb.Status = UserCouponStatus.Collecting;
                CurrentCouponDb.UID = Params.UID;
                DbContext.UserCoupons.Add(CurrentCouponDb);
                DbContext.SaveChanges();
            }

            // get the current amount of collected stamps
            int StampsCollected = DbContext.UserCouponStamps.Where(itm => itm.UserCouponId == CurrentCouponDb.Id && (itm.Status == UserStampStatus.Collected || itm.Status == UserStampStatus.ReceivedByFriend)).Count();
            if (StampsCollected == StampsCountNeeded)
                return Error(ReturnCodes.InternalServerError, "Inconsistent coupons state - There is a 'Collecting' state coupon for this user in the database which already has all stamps collected, but the state is incorrect! (It should be 'Completed'!)");

            UserCouponStamp NewStamp = new UserCouponStamp();
            NewStamp.UserCouponId = CurrentCouponDb.Id;
            NewStamp.DateCollected = DateTime.UtcNow;
            NewStamp.StampTypeId = Params.StampTypeId;
            NewStamp.Status = UserStampStatus.Collected;
            DbContext.UserCouponStamps.Add(NewStamp);

            UserStampLog StampLog = new UserStampLog();
            StampLog.LogType = UserStampLogType.ReceivedByAdmin;
            StampLog.StampTypeId = Params.StampTypeId;
            StampLog.UID = Params.UID;
            StampLog.UserReferenceId = AuthenticationManager.CurrentUser.UID;
            StampLog.UserStamp = NewStamp;
            StampLog.LogTime = DateTime.UtcNow;
            StampLog.LogText = "後台Admin送給印花";
            DbContext.UserStampLogs.Add(StampLog);

            if ((StampsCollected + 1) == StampsCountNeeded)
            {
                CurrentCouponDb.Status = UserCouponStatus.Completed;
                CurrentCouponDb.CompletedBy = CurrentCouponDb.UID;
                CurrentCouponDb.CompletionTime = DateTime.UtcNow;

                UserCoupon NewCouponDb = new UserCoupon();
                NewCouponDb.ActivityId = Params.ActivityId;
                NewCouponDb.ExpiryTime = DbContext.CouponActivities.Where(itm => itm.Id == Params.ActivityId).Select(itm => itm.LimitEndTime).First();
                NewCouponDb.StartTime = DateTime.UtcNow;
                NewCouponDb.Status = UserCouponStatus.Collecting;
                NewCouponDb.UID = Params.UID;
                DbContext.UserCoupons.Add(NewCouponDb);
            }

            DbContext.SaveChanges();

            // get new statistics
            UserActivityInfoDTO ResponseDTO = new UserActivityInfoDTO();
            ResponseDTO.CompletedCouponsCount = DbContext.UserCoupons.Where(itm => itm.UID == Params.UID && itm.ActivityId == Params.ActivityId && itm.Status == UserCouponStatus.Completed).Count();
            ResponseDTO.CurrentCouponCollectedStamps = DbContext.UserCouponStamps.Where(itm => itm.UserCouponId == CurrentCouponDb.Id && (itm.Status == UserStampStatus.Collected || itm.Status == UserStampStatus.ReceivedByFriend)).Count();

            return ResultObject(ResponseDTO);
        }

        public BaseResponseDTO RemoveUser(long UID)
        {
            PermissionsProvider.RequestAdminRole();

            if (UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");

            UserData UserDataDb = DbContext.UserDatas.Find(UID);
            if (UserDataDb == null)
                return Error(ReturnCodes.UserNotFound);

            DbContext.Database.ExecuteSqlCommand(@"
                update [dbo].[UserLoginLogs] set UID = null where UID = @UID
                delete from APIUserSessions where UID = @UID
                delete from PointsTransactions where UID = @UID
                delete from UserPoints where UID = @UID
                delete from UserLikes where UID = @UID
                delete from NewsSubscriptions where UID = @UID
                delete from NewsMessages where UID = @UID
                delete from VendingMachineMessages where SalesTransactionId IN (select Id from SalesTransactions WHERE UID = @UID)
                delete from SalesTransactions where UID = @UID
                delete from LotteryCoinLogs where UID = @UID
                delete from UserLotteryCoins where UID = @UID
                delete from LotteryDrawLogs where UID = @UID
                update LotteryPriceAvailabilityItems set WonTime = null, WonByUID = 0, Status = 1 where WonByUID = @UID
                delete from LotteryPriceCoupons where UID = @UID
                delete from UserPushNotificationLogs where UID = @UID
                delete from UserPushTokens where UID = @UID
                delete from UserStampLogs where UID = @UID
                delete from AutoCouponAssignments where UserCouponId IN (select [Id] from UserCoupons where [UID] = @UID)
                delete from UserCouponStamps from UserCouponStamps st inner join UserCoupons cp on st.UserCouponId = cp.Id where cp.UID = @UID
                delete from UserCoupons where UID = @UID
                delete from PromotionOrders where UID = @UID
                delete from UserFriends where UID = @UID OR UIDFriend = @UID
                delete from UserValueChangeRequests where UID = @UID
                update UserDatas set RecommendedByUID = null where RecommendedByUID = @UID
                update PointsTransactions set ReferenceUserId = null where ReferenceUserId = @UID
                delete from UserDatas where UID = @UID
                delete from UserBases where UID = @UID
            ", new SqlParameter("@UID", UserDataDb.UID));

            return Ok();
        }

        public BaseResponseDTO GetPricesWonLog(long UID)
        {
            List<string> LogLines = new List<string>();

            List<LotteryPriceCoupon> CouponsWonDb = DbContext.LotteryPriceCoupons.Include(itm => itm.Price).Where(itm => itm.UID == UID && itm.Status != LotteryPriceCouponStatus.Cancelled).OrderByDescending(itm => itm.TimeWon).ToList();
            foreach(LotteryPriceCoupon CouponWon in CouponsWonDb)
            {
                LogLines.Add(CouponWon.TimeWon.ToString(GlobalConst.CultureTW.DateTimeFormat.ShortDatePattern, GlobalConst.CultureTW) + " " + CouponWon.TimeWon.ToString(GlobalConst.CultureTW.DateTimeFormat.ShortTimePattern, GlobalConst.CultureTW) + " - " +
                             CouponWon.Price.Title +
                             (CouponWon.Status == LotteryPriceCouponStatus.Expired ? " (到期了)" : (CouponWon.Status == LotteryPriceCouponStatus.PickedUp ? " (已領取)" : " (尚未領取)")));
            }

            return ResultObject(LogLines);
        }

        public BaseResponseDTO GetPointsLog(long UID)
        {
            List<string> LogLines = new List<string>();

            List<PointsTransaction> PointsTransactionsDb = DbContext.PointsTransactions
                                                            .Include(itm => itm.SalesTransaction).Include(itm => itm.SalesTransaction.Product)
                                                            .Include(itm => itm.ReferenceUser)
                                                            .Include(itm => itm.PromotionOrder)
                                                            .Include(itm => itm.Activity)
                                                            .Where(itm => itm.UID == UID)
                                                            .OrderByDescending(itm => itm.CreateTime)
                                                            .ToList();

            int iNr = 0;
            foreach(PointsTransaction Transaction in PointsTransactionsDb)
            {
                string sNum = Transaction.Amount.ToString("###,###,##0").Replace("-", string.Empty);
                string sLogLine = Transaction.CreateTime.ToString(GlobalConst.CultureTW.DateTimeFormat.ShortDatePattern, GlobalConst.CultureTW) + " " + Transaction.CreateTime.ToString(GlobalConst.CultureTW.DateTimeFormat.ShortTimePattern, GlobalConst.CultureTW) + ": ";

                sLogLine += (Transaction.Amount > 0 ? "+" : "-");
                if (sNum.Length >= 11)
                    sLogLine += sNum;
                else
                    sLogLine += new String(' ', 11 - sNum.Length) + sNum;

                sLogLine += "    " + Utils.GetEnumDisplayName(typeof(PointsTransactionType), Transaction.TransactionType.ToString());
                // TODO add more info and EnumDisplayName's for the transaction types

                iNr++;
                if (iNr >= 100)
                    break;
            }

            return ResultObject(LogLines);
        }

        public BaseResponseDTO GetUserId(string Account)
        {
            if (string.IsNullOrWhiteSpace(Account))
                return Error(ReturnCodes.MissingArgument, "The parameter 'Account' is missing!");

            long UserId = DbContext.UserBases.Where(itm => itm.Account == Account).Select(itm => itm.UID).FirstOrDefault();
            if (UserId <= 0)
                return Error(ReturnCodes.UserNotFound, "There is no user with the id '" + Account + "'!");

            return ResultObject(UserId);
        }

        public BaseResponseDTO GetUserDetails(GetUserDetailsParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No arguments given!");
            if (Params.UID <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'UID' is missing!");

            UserData UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).Where(itm => itm.UID == Params.UID).FirstOrDefault();
            if (UserDb != null)
            {
                UserProfileAdminDTO UserDTO = Mapper.Map<UserData, UserProfileAdminDTO>(UserDb);
                return ResultObject(UserDTO);
            }
            else
                return Error(ReturnCodes.RecordNotFound);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;
using Kooco.Framework.Providers;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataStorage.Tables;

namespace VmaBase.Providers
{
    public class SocialLoginProvider : SocialLoginProviderBase<ApplicationDbContext>
    {
		// here you can do some customizations for the social login provider, if needed
    }
}

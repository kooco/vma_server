﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AutoMapper;
using Newtonsoft.Json;
using OfficeOpenXml;

using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.AdminResponse;


namespace VmaBase.Providers
{
    /// <summary>
    ///     LotteryPriceCouponsAdminProvider
    ///     --------------------------------
    ///     
    ///     Provider class for reporting the already won lottery price coupons
    ///     for: Admin Interface
    /// </summary>
    public class LotteryPriceCouponsAdminProvider : BaseListDataProvider<LotteryPriceCoupon, LotteryPriceCouponAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<LotteryPriceCouponAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<LotteryPriceCoupon> Query = DbContext.LotteryPriceCoupons.Include(itm => itm.UserData).Include(itm => itm.UserData.UserBase).Include(itm => itm.Price);
            return GetPagedList(Params, Query, null, HandleSearchCriterias);
        }

        private void HandleSearchCriterias(ref IQueryable<LotteryPriceCoupon> Query, ref StandardSearchParamsDTO<LotteryPriceCouponAdminDTO> Params)
        {
            // customized search criterias
            StandardSearchFilterCriteria CriteriaToRemove = null;
            foreach (StandardSearchFilterCriteria Criteria in Params.FilterCriterias)
            {
                if (Criteria.DTOPropertyName == "PriceExchangeType")
                {
                    if (Criteria.Value.ToString() == ((int)LotteryPriceExchangeType.PickupProduct).ToString())
                        Query = Query.Where(itm => itm.Price.UseActivityCoupon == false);
                    else if (Criteria.Value.ToString() == ((int)LotteryPriceExchangeType.VendingMachineProduct).ToString())
                        Query = Query.Where(itm => itm.Price.UseActivityCoupon == true);

                    CriteriaToRemove = Criteria;
                }
            }
            if (CriteriaToRemove != null)
                Params.FilterCriterias.Remove(CriteriaToRemove);
        }

        #region GetExcel
        public ActionResult GetExcel()
        {
            IQueryable<LotteryPriceCoupon> Query = DbContext.LotteryPriceCoupons.Include(itm => itm.UserData).Include(itm => itm.UserData.UserBase).Include(itm => itm.Price);
            return GetExcel(Query, null, HandleSearchCriterias);
        }
        #endregion

        #region GetPricesDropDownList
        public BaseResponseDTO GetPricesDropDownList()
        {
            return ResultObject(DbContext.LotteryPrices.Where(itm => itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active)
                                    .Select(Mapper.Map<LotteryPrice, LotteryPriceItemDTO>).ToList());
        }
        #endregion
        #region GetGamesDropDownList
        public BaseResponseDTO GetGamesDropDownList()
        {
            return ResultObject(DbContext.LotteryGames.Where(itm => itm.Status != LotteryGameStatus.Inactive)
                                    .Select(Mapper.Map<LotteryGame, LotteryGameAdminDTO>).ToList());
        }
        #endregion
        #region PriceUserPickup
        public BaseResponseDTO PriceUserPickup(long PriceCouponId)
        {
            PermissionsProvider.RequestAdminRole();

            if (PriceCouponId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'LotteryPriceCouponId' is missing!");

            LotteryPriceCoupon CouponDb = DbContext.LotteryPriceCoupons.Find(PriceCouponId);
            if (CouponDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no lottery price coupon with the id " + PriceCouponId.ToString() + " in the database!");

            CouponDb.TimeRedeemed = DateTime.UtcNow;
            CouponDb.Status = LotteryPriceCouponStatus.PickedUp;
            DbContext.SaveChanges();

            DbContext.Entry(CouponDb).Reload();
            return ResultObject(Mapper.Map<LotteryPriceCoupon, LotteryPriceCouponAdminDTO>(CouponDb));
        }
        #endregion
    }
}

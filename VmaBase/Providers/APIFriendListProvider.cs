﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Providers;

using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models;

namespace VmaBase.Providers
{
    public class APIFriendListProvider : BaseProvider
    {
        #region AddFriend
        public BaseResponseDTO AddFriend(AddRemoveFriendParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (((Params.UIDFriend == null) || (Params.UIDFriend <= 0)) &&
                (string.IsNullOrWhiteSpace(Params.AccountFriend)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'userIdFriend' or 'accountFriend' must be set!");

            if (((Params.UIDFriend != null) && (Params.UIDFriend == Params.AuthenticatedUser.UID)) ||
                ((!string.IsNullOrWhiteSpace(Params.AccountFriend)) && (Params.AccountFriend == Params.AuthenticatedUser.Account)))
                return Error(ReturnCodes.InvalidArguments, "A user can not add him/herself as a friend!");

            UserData FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => (Params.UIDFriend == null || Params.UIDFriend <= 0 || Params.UIDFriend == itm.UID) && (Params.AccountFriend == null || Params.AccountFriend == "" || itm.UserBase.Account == Params.AccountFriend) && itm.Status != UserDataStatus.Blocked);
            if (FriendUserDb == null)
                return Error(Shared.ReturnCodes.RecordNotFound, "There no user with the account name '" + Params.AccountFriend + "'!");

            if (FriendUserDb.UserBase.Status == Kooco.Framework.Models.Enum.UserStatus.Pending)
                return Error(ReturnCodes.UserStatusPending, "This user can not be added as a friend because this user account has not been confirmed yet!");
            if (FriendUserDb.UserBase.Status == Kooco.Framework.Models.Enum.UserStatus.Banned)
                return Error(ReturnCodes.UserBlocked, "This user can not be added to the friend list because this user has been banned!");

            // did this user already receive a friend request?
            UserFriend FriendDb = DbContext.UserFriends.Include(itm => itm.FriendUser).FirstOrDefault(itm => itm.FriendUser.UID == Params.AuthenticatedUser.UID && itm.UID == FriendUserDb.UID);
            if (FriendDb != null)
            {
                if (FriendDb.Status == UserFriendStatus.Confirmed) // actually already accepted .. return error
                    return Error(ReturnCodes.FriendAlreadyAdded);
                if (FriendDb.Status == UserFriendStatus.Declined)  // this user declined this friend request in the past and now wants to add this user as friend -> ok then delete the declined friend request
                    DbContext.UserFriends.Remove(FriendDb);
                if (FriendDb.Status == UserFriendStatus.Pending)   // aah the other user also wants to add him/her as friend -> so then immediatelly accept the friend request
                {
                    AddRemoveFriendParamsDTO AcceptParams = new AddRemoveFriendParamsDTO();
                    AcceptParams.UIDFriend = FriendUserDb.UID;
                    AcceptParams.AppToken = Params.AppToken;
                    AcceptParams.SessionToken = Params.SessionToken;
                    return SetFriendRequestStatus(AcceptParams, true);
                }
            }

            // is there already a sent friend request?
            FriendDb = DbContext.UserFriends.Include(itm => itm.FriendUser).
                FirstOrDefault(itm => itm.UID == Params.AuthenticatedUser.UID && itm.FriendUser.UID == FriendUserDb.UID);
            if (FriendDb == null)
            {
                FriendDb = new UserFriend();
                FriendDb.UID = Params.AuthenticatedUser.UID;
                FriendDb.UIDFriend = FriendUserDb.UID;
                FriendDb.Status = Models.Enum.UserFriendStatus.Pending;
                FriendDb.ModifyTime = DateTime.UtcNow;
                FriendDb.CreateTime = DateTime.UtcNow;
                DbContext.UserFriends.Add(FriendDb);
                DbContext.SaveChanges();

                PushNotificationProvider.Current.SendTextNotification(FriendUserDb.UID, "你有一個交友邀請", Params.AuthenticatedUser.Nickname + " (ID " + Params.AuthenticatedUser.Account + ") 想加你好友", PushAction.Create(PushActionCode.FriendAdded, Params.AuthenticatedUser.Account));

                return OperationPending("Friend request have been sent. Friend request pending.");
            }
            else
            {
                if (FriendDb.Status == UserFriendStatus.Confirmed)
                    return Error(Shared.ReturnCodes.RecordAlreadyExists, "The friend to be added is already on this users friend list!");
                else if (FriendDb.Status == UserFriendStatus.Pending)
                    return Error(Shared.ReturnCodes.PendingFriendRequest, "There is already a pending friend request for this user!");
                else
                    return Error(Shared.ReturnCodes.FriendRequestDeclined, "The last friend request for this user already have been declined!");
            }
        }
        #endregion
        #region RemoveFriend
        public BaseResponseDTO RemoveFriend(AddRemoveFriendParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (((Params.UIDFriend == null) || (Params.UIDFriend <= 0)) &&
                (string.IsNullOrWhiteSpace(Params.AccountFriend)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'userIdFriend' or 'accountFriend' must be set!");

            UserData FriendUserDb = null;
            if ((Params.UIDFriend != null) && (Params.UIDFriend > 0))
            {
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UserBase.UID == Params.UIDFriend);
                if (FriendUserDb == null)
                    return Error(Shared.ReturnCodes.RecordNotFound, "There no user with the user id '" + Params.UIDFriend.ToString() + "'!");
            }
            else
            {
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UserBase.Account == Params.AccountFriend);
                if (FriendUserDb == null)
                    return Error(Shared.ReturnCodes.RecordNotFound, "There no user with the account name '" + Params.AccountFriend + "'!");
            }

            UserFriend FriendDb = DbContext.UserFriends.Include(itm => itm.FriendUser)
                .FirstOrDefault(itm => itm.UID == Params.AuthenticatedUser.UID && itm.FriendUser.UID == FriendUserDb.UID);
            if (FriendDb != null)
                DbContext.UserFriends.Remove(FriendDb);

            UserFriend FriendOtherUserDb = DbContext.UserFriends.Include(itm => itm.FriendUser)
                .FirstOrDefault(itm => itm.FriendUser.UID == Params.AuthenticatedUser.UID && itm.UID == FriendUserDb.UID);
            if (FriendOtherUserDb != null)
                DbContext.UserFriends.Remove(FriendOtherUserDb);

            DbContext.SaveChanges();
            return Ok();
        }
        #endregion
        #region GetFriendList
        public BaseResponseDTO GetFriendList(GetFriendListParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            DateTime BeginningDate = DateTime.UtcNow.AddDays(-7);

            List<UserFriend> FriendListDb = DbContext.UserFriends.Include(itm => itm.FriendUser).Include(itm => itm.FriendUser.UserBase).
                Where(itm => itm.UID == Params.AuthenticatedUser.UID && itm.Status == Models.Enum.UserFriendStatus.Confirmed &&
                        ((Params.RecentAlso != null && Params.RecentAlso.Value == true) || (itm.CreateTime < BeginningDate))).ToList();

            return ResultObject(FriendListDb.Select(Mapper.Map<UserFriend, UserSearchResultDTO>).ToList());
        }
        #endregion

        #region GetPendingFriendRequests
        public BaseResponseDTO GetPendingFriendRequests(GetFriendRequestsParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            DateTime BeginningDate = DateTime.UtcNow.AddDays(-7);
            if ((Params.OnlyReceived == true) || (Params.OnlySent == true))
                Params.NoAccepted = true;

            List<UserFriend> FriendListDb = DbContext.UserFriends.Include(itm => itm.FriendUser).Include(itm => itm.FriendUser.UserBase).Include(itm => itm.User).Include(itm => itm.User.UserBase)
                .Where(itm => (((itm.FriendUser.UID == Params.AuthenticatedUser.UID || itm.UID == Params.AuthenticatedUser.UID) && (itm.Status != UserFriendStatus.Confirmed)) || 
                               (itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserFriendStatus.Confirmed)) && 
                            (itm.Status == UserFriendStatus.Pending || (itm.CreateTime >= BeginningDate && (Params.NoAccepted != true))) &&
                            (Params.OnlyReceived == null || Params.OnlyReceived.Value == false || itm.FriendUser.UID == Params.AuthenticatedUser.UID || itm.Status == UserFriendStatus.Confirmed) &&
                            (Params.OnlySent == null || Params.OnlySent.Value == false || itm.UID == Params.AuthenticatedUser.UID || itm.Status == UserFriendStatus.Confirmed)).ToList();

            List<UserFriendRequestDTO> FriendRequests = new List<UserFriendRequestDTO>();
            foreach(UserFriend FriendDb in FriendListDb)
            {
                FriendRequestType RequestType = (FriendDb.Status == UserFriendStatus.Confirmed ? FriendRequestType.Confirmed : (FriendDb.UID == Params.AuthenticatedUser.UID ? FriendRequestType.Sent : FriendRequestType.Received));
                bool bIsSentRequest = RequestType == FriendRequestType.Confirmed || RequestType == FriendRequestType.Sent;

                FriendRequests.Add(new UserFriendRequestDTO()
                {
                    RequestType = RequestType,
                    UID = (bIsSentRequest ? FriendDb.FriendUser.UID : FriendDb.User.UID),
                    Account = (bIsSentRequest ? FriendDb.FriendUser.UserBase.Account : FriendDb.User.UserBase.Account),
                    Name = (bIsSentRequest ? FriendDb.FriendUser.UserBase.Name : FriendDb.User.UserBase.Name),
                    Nickname = (bIsSentRequest ? FriendDb.FriendUser.UserBase.Nickname : FriendDb.User.UserBase.Nickname),
                    Phone = (bIsSentRequest ? FriendDb.FriendUser.UserBase.Phone : FriendDb.User.UserBase.Phone),
                    Email = (bIsSentRequest ? FriendDb.FriendUser.UserBase.Email : FriendDb.User.UserBase.Email),
                    ImageUrl = (bIsSentRequest ? FriendDb.FriendUser.ImageUrl : FriendDb.User.ImageUrl),
                    RequestState = FriendDb.Status,                    
                });
            }

            return ResultObject(FriendRequests);
        }
        #endregion
        #region GetMyFriendRequest
        public BaseResponseDTO GetMyFriendRequests(BaseAPIAuthParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            List<UserFriend> FriendListDb = DbContext.UserFriends.Include(itm => itm.FriendUser).Include(itm => itm.FriendUser.UserBase).
                Where(itm => itm.UID == Params.AuthenticatedUser.UID && (itm.Status == UserFriendStatus.Pending || itm.Status == UserFriendStatus.Declined)).ToList();

            return ResultObject(FriendListDb.Select(Mapper.Map<UserFriend, UserFriendRequestDTO>).ToList());
        }
        #endregion
        #region SetFriendRequestStatus
        /// <summary>
        ///     Answers a pending friend request
        /// </summary>
        public BaseResponseDTO SetFriendRequestStatus(AddRemoveFriendParamsDTO Params, bool bConfirm)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (((Params.UIDFriend == null) || (Params.UIDFriend <= 0)) && (string.IsNullOrWhiteSpace(Params.AccountFriend)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'accountFriend' or 'uidFriend' must be set!");

            UserFriend FriendDb = null;
            if ((Params.UIDFriend != null) && (Params.UIDFriend > 0))
            {
                FriendDb = DbContext.UserFriends.Include(itm => itm.FriendUser).
                    FirstOrDefault(itm => itm.FriendUser.UID == Params.AuthenticatedUser.UID && itm.UID == Params.UIDFriend);

                if (FriendDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no pending friend request from the user id '" + Params.UIDFriend.Value.ToString() + "' (authenticated user: " + Params.AuthenticatedUser.UID.ToString() + ")!");
            }
            else
            {
                FriendDb = DbContext.UserFriends.Include(itm => itm.FriendUser).Include(itm => itm.User).Include(itm => itm.User.UserBase).
                    FirstOrDefault(itm => itm.FriendUser.UID == Params.AuthenticatedUser.UID && itm.User.UserBase.Account == Params.AccountFriend);

                if (FriendDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no pending friend request from user '" + Params.AccountFriend + "' (authenticated user: " + Params.AuthenticatedUser.Account + ")!");
            }

            if (FriendDb.Status == UserFriendStatus.Pending)
            {
                FriendDb.Status = (bConfirm ? UserFriendStatus.Confirmed : UserFriendStatus.Declined);

                if (bConfirm)
                {
                    UserFriend FriendDbThisUser = new UserFriend();
                    FriendDbThisUser.UID = Params.AuthenticatedUser.UID;
                    FriendDbThisUser.UIDFriend = FriendDb.UID;
                    FriendDbThisUser.Status = UserFriendStatus.Confirmed;
                    DbContext.UserFriends.Add(FriendDbThisUser);
                }

                DbContext.SaveChanges();
            }
            else
            {
                if (FriendDb.Status == UserFriendStatus.Confirmed)
                    return Ok();
                if (FriendDb.Status == UserFriendStatus.Declined)
                    return Error(ReturnCodes.FriendRequestDeclined, "This friend request already has been declined and can not be confirmed anymore! (Sending a friend request to this user can undo the declined request)");
            }

            return Ok();
        }
        #endregion
    }
}

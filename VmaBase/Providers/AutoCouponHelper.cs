﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class DynamicModulesAutoCouponHelper
    {
        public string LogfileName = string.Empty;
        public UserData User { get; set; }
        public ApplicationDbContext DbContext { get; set; }
        public long CriteriaId { get; set; }
        private AutoCouponUserState _UserState { get; set; }
        Dictionary<string, string> CustomSettings { get; set; }
        public int LastSettingsLength { get; set; }

        #region DebugMessage
        public void DebugMessage(object Message)
        {
            if ((Message != null) && (Message != System.DBNull.Value))
            {
                if (!string.IsNullOrWhiteSpace(LogfileName))
                    File.AppendAllLines(LogfileName, new List<string>() { Message.ToString() });
            }
        }
        #endregion

        #region Constructor
        public DynamicModulesAutoCouponHelper(ApplicationDbContext dbContext, UserData user)
        {
            DbContext = dbContext;
            User = user;
        }
        #endregion
        #region InitCriteria
        public void InitCriteria(long parCriteriaId)
        {
            CriteriaId = parCriteriaId;
            _UserState = null;
            CustomSettings = null;
        }
        #endregion

        #region EnsureUserState
        private void EnsureUserState()
        {
            if (_UserState == null)
            {
                _UserState = DbContext.AutoCouponUserStates.Where(itm => itm.CriteriaId == CriteriaId && itm.UID == User.UID).FirstOrDefault();

                bool bNew = false;
                if (_UserState == null)
                {
                    _UserState = new AutoCouponUserState();
                    _UserState.CreateTime = DateTime.UtcNow;
                    _UserState.CriteriaId = CriteriaId;
                    _UserState.UID = User.UID;
                    bNew = true;
                }

                _UserState.ModifyTime = DateTime.UtcNow;

                if (bNew)
                    DbContext.AutoCouponUserStates.Add(_UserState);
            }
        }
        #endregion
        #region EnsureCustomSettings
        private void EnsureCustomSettings()
        {
            EnsureUserState();

            if (CustomSettings == null)
            {
                string Settings = (string.IsNullOrWhiteSpace(_UserState.Settings) ? _UserState.Blob : _UserState.Settings);
                LastSettingsLength = (Settings == null ? 0 : Settings.Length);
                CustomSettings = Utils.GetParameterListFromString((Settings == null ? string.Empty : Settings), true);
            }
        }
        #endregion

        #region Property: UserState
        public AutoCouponUserState UserState
        {
            get
            {
                EnsureUserState();
                return _UserState;
            }
        }
        #endregion

        #region GetUserLoginsCount
        public long GetUserLoginsCount(DateTime? DateFrom = null, DateTime? DateTo = null)
        {
            if (DateFrom == null)
                DateFrom = new DateTime(1970, 1, 1);
            if (DateTo == null)
                DateTo = new DateTime(2099, 12, 31);

            long? Count = DbContext.UserLoginLogs.Where(itm => itm.UID == User.UID && itm.Time >= DateFrom && itm.Time <= DateTo && itm.Type == Kooco.Framework.Models.Enum.UserLoginLogType.LoginSuccessful).Count();
            if (Count == null)
                Count = 0;


            return Count.Value;
        }
        #endregion
        #region GetUserLastLoginTime
        public DateTime? GetUserLastLoginTime()
        {
            return DbContext.UserLoginLogs.Where(itm => itm.UID == User.UID && itm.Type == Kooco.Framework.Models.Enum.UserLoginLogType.LoginSuccessful).OrderByDescending(itm => itm.Time).Select(itm => itm.Time).FirstOrDefault();
        }
        #endregion

        #region GetAutoAssignmentsCount
        public int GetAutoAssignmentsCount()
        {
            long? Count = DbContext.AutoCouponAssignments.Where(itm => itm.CriteriaId == CriteriaId && itm.UID == User.UID).Count();
            if (Count == null)
                Count = 0;

            return (int) Count.Value;
        }
        #endregion
        #region AlreadyAssigned
        public bool AlreadyAssigned()
        {
            return GetAutoAssignmentsCount() > 0;
        }
        #endregion

        #region SetCounter
        public void SetCounter(int CounterValue, bool Increase = false)
        {
            EnsureUserState();
            _UserState.Counter = (Increase ? _UserState.Counter + CounterValue : CounterValue);
        }
        #endregion
        #region IncreaseCounter
        public void IncreaseCounter(int IncreaseBy = 1)
        {
            SetCounter(IncreaseBy, true);
        }
        #endregion

        #region SetCustomSetting
        public void SetCustomSetting(string SettingName, string SettingValue)
        {
            EnsureCustomSettings();

            if (CustomSettings.ContainsKey(SettingName.Trim().ToLower()))
                CustomSettings[SettingName.Trim().ToLower()] = SettingValue;
            else
                CustomSettings.Add(SettingName.Trim().ToLower(), SettingValue);
        }
        #endregion
        #region GetCustomSetting
        public string GetCustomSetting(string SettingName)
        {
            EnsureCustomSettings();

            if (CustomSettings.ContainsKey(SettingName.Trim().ToLower()))
                return CustomSettings[SettingName.Trim().ToLower()];
            else
                return string.Empty;
        }
        #endregion
        #region RemoveCustomSetting
        public void RemoveCustomSetting(string SettingName)
        {
            EnsureCustomSettings();

            if (CustomSettings.ContainsKey(SettingName.Trim().ToLower()))
                CustomSettings.Remove(SettingName.Trim().ToLower());
        }
        #endregion

        #region SendPushNotification
        public bool SendPushNotification(string Title, string Message, int ActionCode = 0, string ActionParameter = "")
        {
            try
            {
                Kooco.Framework.Models.PushAction Action = null;
                if (ActionCode > 0)
                {
                    Action = new Kooco.Framework.Models.PushAction();
                    Action.ActionCode = ActionCode;
                    Action.ActionParameter = ActionParameter;
                }

                return Kooco.Framework.Providers.PushNotificationProvider.Current.SendTextNotification(User.UID, Title, Message, Action);
            }
            catch (Exception Exc)
            {
                Utils.LogError(Exc, "Error in AutoCouponHelper.SendPushNotification. Title: " + Utils.SafeString(Title) + ", Message: " + Utils.SafeString(Message) + ", ActionCode: " + ActionCode.ToString() + ", ActionParameter: " + ActionParameter);
                return false;
            }
        }
        #endregion

        #region ApplyChanges
        public void ApplyChanges()
        {
            if (CustomSettings != null)
            {
                string ParametersString = string.Empty;
                foreach(string Key in CustomSettings.Keys)
                {
                    ParametersString += Key + "=" + (CustomSettings[Key].Replace(";", ";;")) + ";";
                }

                if (ParametersString.EndsWith(";"))
                    ParametersString = ParametersString.Substring(0, ParametersString.Length - 1);

                if (ParametersString.Length <= AutoCouponUserState.SettingsMaxLength)
                {
                    if (LastSettingsLength > AutoCouponUserState.SettingsMaxLength)
                        _UserState.Blob = null;
                    _UserState.Settings = ParametersString;
                }
                else
                {
                    _UserState.Settings = null;
                    _UserState.Blob = ParametersString;
                }
            }

            if (_UserState != null)
                DbContext.SaveChanges();
        }
        #endregion
    }
}

﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Providers;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class VendingProductsAdminProvider : BaseListDataProvider<VendingProduct, VendingProductAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<VendingProductAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<VendingProduct> Query = DbContext.VendingProducts.Include(itm => itm.DefaultStampType).Where(itm => itm.Status != Models.Enum.VendingProductStatus.Deleted);

            return GetPagedList(Params, Query);
        }

        public override FrameworkResponseDTO SaveProperty(SavePropertyParamsDTO Params, BeforeSavePropertyFunction BeforeSave = null)
        {
            return base.SaveProperty(Params, BeforeSave);
        }

        private Kooco.Framework.Shared.FrameworkReturnCodes BeforeSaveProperty(VendingProduct DbData, SavePropertyParamsDTO Params)
        {
            if (Params.Name == "PointsExtra")
            {
                // as long as "PointsExtra" is not used in the admin => always update it from the base settings
                DbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[VendingProductActivitySettings] SET [PointsExtra] = " + Params.Value + " WHERE [ProductId] = " + Params.Id);
            }

            return Kooco.Framework.Shared.FrameworkReturnCodes.Success;
        }

        public BaseResponseDTO CreateNewProduct()
        {
            string MOCCPGoodsId = "00001";

            while(DbContext.VendingProducts.FirstOrDefault(itm => itm.MOCCPGoodsId == MOCCPGoodsId) != null)
            {
                MOCCPGoodsId = (int.Parse(MOCCPGoodsId) + 1).ToString("00000");
            }

            VendingProduct Product = new VendingProduct();
            Product.Name = "<名字>";
            Product.NameEN = string.Empty;
            Product.ProductTypeId = GlobalConst.ProductTypeId_Others;
            Product.ProductBrandId = GlobalConst.ProductBrandId_Others;
            Product.Volume = "瓶子";
            Product.Status = Models.Enum.VendingProductStatus.NotAvailable;
            Product.MOCCPGoodsId = MOCCPGoodsId;
            DbContext.VendingProducts.Add(Product);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<VendingProduct, VendingProductAdminDTO>(Product));
        }

        public BaseResponseDTO GetStampsSelectionList()
        {
            return ResultObject(DbContext.StampTypes.Where(itm => itm.Status == GeneralStatusEnum.Active).Select(Mapper.Map<StampType, StampTypeAdminDTO>).ToList());
        }
    }
}

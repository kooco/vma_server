﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Providers;
using Kooco.Framework.Models.Enum;

using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models;


namespace VmaBase.Providers
{
    public class APISystemProvider : BaseProvider
    {
        #region CheckAppVersion
        public BaseResponseDTO CheckAppVersion(CheckAppVersionParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No arguments given!");
            if (string.IsNullOrWhiteSpace(Params.AppVersion))
                return Error(ReturnCodes.MissingArgument, "The parameter 'appVersion' is missing!");

            if (Params.AppVersion.IndexOf(".") < 0)
                Params.AppVersion += ".0";

            Version AppVersion = new Version(Params.AppVersion);
            Version MinimumVersion = (Params.DeviceType == MobileDeviceType.Android ? new Version(ConfigManager.MinimumAppVersionAndroid) : new Version(ConfigManager.MinimumAppVersionIOS));

            if (AppVersion < MinimumVersion)
                return ResultObject(false);
            else
                return ResultObject(true);
        }
        #endregion
    }
}

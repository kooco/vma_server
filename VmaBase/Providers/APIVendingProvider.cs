﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using cn.jpush.api;
using cn.jpush.api.push.mode;
using cn.jpush.api.push;
using Newtonsoft.Json;
using AutoMapper;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.MOCCP;
using VmaBase.Models.Enum;

namespace VmaBase.Providers
{
    public class APIVendingProvider : BaseProvider
    {
        #region GetVendingMachineList
        public BaseResponseDTO GetVendingMachineList(GetVendingMachinesParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckPagingAndAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            int? TotalPages = null;
            int iPageNr = (Params.Paging == true && Params.Page != null && Params.Page.Value > 0 ? Params.Page.Value : 1);
            int iPageSize = (Params.MaxItems != null ? Params.MaxItems.Value : 99999);

            if (((Params.RadiusMeters != null) && (Params.RadiusMeters > 0)) || (Params.Latitude != null) || (Params.Longitude != null))
            {
                if ((Params.Latitude == null) || (Params.Longitude == null))
                    return Error(ReturnCodes.MissingArgument, "If you set one of the parameter 'latitude', 'longitude' or 'radiusMeters' then you also need to set BOTH 'latitude' and 'longitude'!");
            }

            // DEBUG - temporary maximium limit of 50 items
            if (iPageSize > 48)
                iPageSize = 48;

            if (Params.RadiusMeters == 0)
                Params.RadiusMeters = null;

            bool bIsTestUser = false;
            if ((Params.Latitude == null) || (Params.Longitude == null))
            {
                UserData UserDb = DbContext.UserDatas.Find(Params.AuthenticatedUser.UID);
                if (UserDb.GeoLocation != null)
                {
                    Params.Latitude = (decimal?)UserDb.GeoLocation.Latitude;
                    Params.Longitude = (decimal?)UserDb.GeoLocation.Longitude;
                }
                bIsTestUser = UserDb.IsTestUser;
            }
            else
                bIsTestUser = DbContext.UserDatas.Where(itm => itm.UID == Params.AuthenticatedUser.UID).Select(itm => itm.IsTestUser).FirstOrDefault();
            
            List<VendingMachineDTO> MachinesList = DbContext.SP_GetVendingMachineList((Params.OnlyActive == null ? true : Params.OnlyActive.Value), (Params.OnlyRecommended == null ? false : Params.OnlyRecommended.Value), true, Params.ActivityId, Params.CriteriaId, Params.Latitude, Params.Longitude, (Params.RadiusMeters == null ? 10000000 : Params.RadiusMeters.Value), bIsTestUser, iPageNr, iPageSize);

            long iTotalRows = 0;
            if (Params.Paging == true)
            {
                iTotalRows = DbContext.SP_GetVendingMachineListCount((Params.OnlyActive == null ? true : Params.OnlyActive.Value), (Params.OnlyRecommended == null ? false : Params.OnlyRecommended.Value), true, Params.ActivityId, Params.CriteriaId, Params.Latitude, Params.Longitude, (Params.RadiusMeters == null ? 10000000 : Params.RadiusMeters.Value), bIsTestUser);
                TotalPages = (int?)Math.Ceiling((double)((double)iTotalRows / (double)Params.MaxItems.Value));
            }
            else
                iTotalRows = MachinesList.Count;

            return ResultObject(MachinesList, RecordsCount: (int)iTotalRows, TotalPages: TotalPages);
        }
        #endregion
        #region GetTestMachineQR
        /// <summary>
        ///     Returns a random QR code of a machine that can be queried
        /// </summary>
        public BaseResponseDTO GetTestMachineQR()
        {
            VendingMachine MachineDb = DbContext.VendingMachines.OrderBy(itm => Guid.NewGuid()).FirstOrDefault();
            if (MachineDb != null)
            {
                BuyBuyMachineParamsDTO Params = new BuyBuyMachineParamsDTO();
                Params.ApiKey = GlobalConst.BuyBuyApiKey;
                Params.Organization = MachineDb.MOCCPOrganization;
                Params.AssetNo = MachineDb.MOCCPAssetNo;

                MOCCPMachineQueryProvider QP = new MOCCPMachineQueryProvider();
                return QP.GetMachineQR(Params);
            }
            else
                return ResultObject(string.Empty);
        }
        #endregion
        #region GetTestMachine
        public BaseResponseDTO GetTestMachine(long? VendingMachineId)
        {
            VendingMachine MachineDb = null;
            if ((VendingMachineId != null) && (VendingMachineId.Value > 0))
                MachineDb = DbContext.VendingMachines.Find(VendingMachineId.Value);
            else
                MachineDb = DbContext.VendingMachines.OrderBy(itm => Guid.NewGuid()).FirstOrDefault();
            DbContext.Entry(MachineDb).Reload();

            if (MachineDb != null)
            {
                VendingMachineDTO MachineDTO = Mapper.Map<VendingMachine, VendingMachineDTO>(MachineDb);
                MachineDTO.QRToken = MachineDb.QRToken;
                return ResultObject(MachineDTO);
            }
            else
                return Error(ReturnCodes.RecordNotFound);
        }
        #endregion
        #region GetProductList
        public BaseResponseDTO GetProductList(GetProductListParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            VendingProductListDTO DTO = new VendingProductListDTO();
            List<VendingProduct> ProductListDb = null;
            if (Params.AlsoNotAvailable == true)
                ProductListDb = DbContext.VendingProducts.Where(itm => itm.Status != Models.Enum.VendingProductStatus.Deleted).ToList();
            else
                ProductListDb = DbContext.VendingProducts.Where(itm => itm.Status == Models.Enum.VendingProductStatus.Available).ToList();

            DTO.ProductsCount = ProductListDb.Count;
            DTO.BaseImageUrl = (ProductListDb.Count > 0 ? ProductListDb[0].ImageUrl.Substring(0, ProductListDb[0].ImageUrl.LastIndexOf('/') + 1) : string.Empty);
            DTO.Products = ProductListDb.Select(Mapper.Map<VendingProduct, VendingProductDTO>).ToList();

            return ResultObject(DTO);
        }
        #endregion
        #region CheckSalesListUpdate
        private static Dictionary<long, DateTime> LastSalesListUpdateTimes = null;
        private static string CheckSalesListLock = "_check_saleslist_update";

        private bool CheckSalesListUpdate(long VendingMachineId)
        {
            try
            {
                lock (CheckSalesListLock)
                {
                    bool bCacheKeyExists = false;

                    if (System.Web.HttpContext.Current != null)
                        LastSalesListUpdateTimes = (Dictionary<long, DateTime>)System.Web.HttpContext.Current.Cache["saleslist_lastupdate_dates"];
                    if (LastSalesListUpdateTimes == null)
                        LastSalesListUpdateTimes = new Dictionary<long, DateTime>();
                    else
                        bCacheKeyExists = true;

                    DateTime LastUpdate = DateTime.MinValue;
                    if (LastSalesListUpdateTimes.ContainsKey(VendingMachineId))
                        LastUpdate = LastSalesListUpdateTimes[VendingMachineId];
                    else
                        LastSalesListUpdateTimes.Add(VendingMachineId, DateTime.UtcNow);

                    if (LastUpdate < DateTime.UtcNow.AddSeconds(-(ConfigManager.SalesListAvailabilityUpdateInterval)))
                    {
                        LastSalesListUpdateTimes[VendingMachineId] = DateTime.UtcNow;

                        if (System.Web.HttpContext.Current != null)
                        {
                            if (bCacheKeyExists)
                                System.Web.HttpContext.Current.Cache["saleslist_lastupdate_dates"] = LastSalesListUpdateTimes;
                            else
                                System.Web.HttpContext.Current.Cache.Insert("saleslist_lastupdate_dates", LastSalesListUpdateTimes, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(7, 0, 0, 0), System.Web.Caching.CacheItemPriority.High, null);
                        }
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "in CheckSalesListUpdate for vending machine id " + VendingMachineId.ToString());
                return true;
            }
        }
        #endregion
        #region GetSalesList
        public class VendingMachineInfo
        {
            public string Organization { get; set; }

            public string AssetNo { get; set; }
        }

        public BaseResponseDTO GetSalesList(GetSalesListParamsDTO Params)
        {
            bool bMachineProcessingOtherOrder = false;
            bool bMachineNotYetProcessedLastMessage = false;
            bool bDoAvailabilityUpdate = false;
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (Params.VendingMachineId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'vendingMachineId' is missing!");

            VendingMachineInfo MachineInfo = DbContext.VendingMachines.Where(itm => itm.Id == Params.VendingMachineId && itm.Status != Models.Enum.VendingMachineStatus.Removed)
                                                .Select(itm => new VendingMachineInfo() { Organization = itm.MOCCPOrganization, AssetNo = itm.MOCCPAssetNo }).FirstOrDefault();
            if ((MachineInfo == null) || (string.IsNullOrWhiteSpace(MachineInfo.AssetNo)))
                return Error(ReturnCodes.RecordNotFound, "There is no vending machine with the id " + Params.VendingMachineId.ToString() + " in the database!");

            if ((Params.ActivityId != null) && (Params.ActivityId > 0))
            {
                if (DbContext.CouponActivities.Where(itm => itm.Id == Params.ActivityId && itm.Status == Models.Enum.ActivityStatus.Active || itm.Status == Models.Enum.ActivityStatus.Closed).Select(itm => itm.Id).FirstOrDefault() != Params.ActivityId)
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString() + " in the database!");
            }

            if ((Params.CriteriaId != null) && (Params.CriteriaId > 0))
            {
                if (DbContext.AutoCouponCriterias.Where(itm => itm.Id == Params.CriteriaId && itm.Status != Models.Enum.AutoCouponCriteriaStatus.Deleted).Select(itm => itm.Id).FirstOrDefault() != Params.CriteriaId)
                    return Error(ReturnCodes.RecordNotFound, "There is no criteria (auto-coupon rule) with the id " + Params.CriteriaId.ToString() + " in the database!");
            }

            bDoAvailabilityUpdate = CheckSalesListUpdate(Params.VendingMachineId);
            string sAvailabilityList = string.Empty;

            if (bDoAvailabilityUpdate)
            {
                MOCCPImportProvider IP = new MOCCPImportProvider();
                sAvailabilityList = IP.GetMerchandiseAvailability(MachineInfo.Organization, MachineInfo.AssetNo);
                if (sAvailabilityList != null && sAvailabilityList.StartsWith("ERR:"))
                    return Error(ReturnCodes.BuyBuyApiConnectionError, sAvailabilityList.Substring(4));

                Utils.LogTrace("Sales List Availability update for vending machine " + Params.VendingMachineId.ToString(), Module: "APIVending");
            }

            List<SalesListEntryDTO> SalesList = DbContext.SP_UpdateSalesList(Params.VendingMachineId, sAvailabilityList, Params.AuthenticatedUser.UID, out bMachineProcessingOtherOrder, out bMachineNotYetProcessedLastMessage, (Params.OnlyAvailableProducts == null) || (Params.OnlyAvailableProducts == true), Params.ActivityId, Params.CriteriaId, UpdateAvailabilityList: bDoAvailabilityUpdate, ForRedeem: (Params.ForRedeem != null ? Params.ForRedeem.Value : false), LotteryPriceId: Params.LotteryPriceId);
            if (Params.IncludeImages == false)
            {
                foreach (SalesListEntryDTO Entry in SalesList)
                    Entry.ProductImageUrl = null;
            }

            Response = ResultObject(SalesList);
            if (bMachineProcessingOtherOrder)
                Response.ReturnCode = ReturnCodes.VendingMachineAlreadyHasOrder;
            if (bMachineNotYetProcessedLastMessage)
                Response.ReturnCode = ReturnCodes.VendingMachineHasOpenOrderMessage;
            return Response;
        }
        #endregion
        #region StartOrderProduct
        public BaseResponseDTO StartOrderProduct(StartOrderProductParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (Params.VendingMachineId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'vendingMachineId' is missing!");
            if (Params.ProductId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'productId' is missing!");

            Response = DbContext.SP_StartOrderProduct(Params.AuthenticatedUser.UID, Params.VendingMachineId, Params.ProductId);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                string JPushRegistrationId = DbContext.VendingMachines.Where(itm => itm.Id == Params.VendingMachineId).Select(itm => itm.RegistrationId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(JPushRegistrationId))
                    SendBuyBuyJPush(Params.VendingMachineId, JPushRegistrationId, GlobalConst.BuyBuyJPushType_ProductChosen, Params.ProductId.ToString());
            }

            return Response;
        }
        #endregion

        #region UpdateAllSalesLists
        public void UpdateAllSalesLists()
        {
            MOCCPImportProvider IP = new MOCCPImportProvider();
            string sAvailabilityList = string.Empty;
            bool bDummy = false;

            List<VendingMachine> MachinesDb = DbContext.VendingMachines.Where(itm => itm.Status == Models.Enum.VendingMachineStatus.Active || itm.Status == Models.Enum.VendingMachineStatus.Broken || itm.Status == Models.Enum.VendingMachineStatus.SoldOut).ToList();
            foreach(VendingMachine MachineDb in MachinesDb)
            {
                sAvailabilityList = IP.GetMerchandiseAvailability(MachineDb.MOCCPOrganization, MachineDb.MOCCPAssetNo);
                if (sAvailabilityList != null && sAvailabilityList.StartsWith("ERR:"))
                {
                    if (sAvailabilityList.Length > 4)
                        Utils.LogError("Error while retrieving sales list of vending machine " + MachineDb.MOCCPAssetNo + " (Org " + MachineDb.MOCCPOrganization + "). The service returned: " + sAvailabilityList.Substring(4));
                }
                else
                    DbContext.SP_UpdateSalesList(MachineDb.Id, sAvailabilityList, 0, out bDummy, out bDummy, false, null, null, true, false);
            }
        }
        #endregion

        #region CheckMachinesRecommended
        public void CheckMachinesRecommended()
        {
            DbContext.SP_CheckMachinesRecommended();
        }
        #endregion
        #region CheckMachinesRecommended
        public void CheckMachineMessagesExpiry()
        {
            DbContext.SP_CheckMachineMessagesExpiry();
        }
        #endregion

        #region SendBuyBuyJPush
        public void SendBuyBuyJPush(long VendingMachineId, string registrationid, string type, string content, int RetryCount = 0, long LogId = 0)
        {
            BuyBuyPushTypeDTO _data = new BuyBuyPushTypeDTO();
            _data.type = type;
            _data.content = content;

            try
            {
                JPushClient client = new JPushClient(GlobalConst.BuyBuyJpushKey, GlobalConst.BuyBuyJpushSec);
                PushPayload pushPayload = new PushPayload();

                pushPayload.platform = Platform.all();
                pushPayload.audience = Audience.s_registrationId(registrationid);

                pushPayload.message = Message.content(JsonConvert.SerializeObject(_data, Formatting.Indented).ToString());
                pushPayload.message.title = "Smart Vending System Message";
                pushPayload.message.content_type = "application/json";

                pushPayload.options.apns_production = true; // 设置 iOS 推送生产环境。不设置默认为开发环境。

                var response = client.SendPush(pushPayload);
                string str = response.ResponseResult.responseContent;

                LogJPushMessage(VendingMachineId, registrationid, pushPayload.message.msg_content, response, string.Empty, 0);
            }
            catch (Exception Exc)
            {
                Utils.LogError(Exc, "Exception while trying to send a JPush message to the registration id " + registrationid + " (type " + type + ")");
                LogJPushMessage(VendingMachineId, registrationid, JsonConvert.SerializeObject(_data, Formatting.Indented).ToString(), null, Exc.Message, RetryCount, LogId);
            }
        }
        #endregion
        #region LogJPushMessage
        private void LogJPushMessage(long VendingMachineId, string registrationId, string Json, MessageResult Result, string ExceptionMessage, int RetryCount = 0, long LogId = 0)
        {
            JPushMessageLog LogDb = null;
            bool IsNew = true;

            if (LogId <= 0)
                LogDb = new JPushMessageLog();
            else
            {
                LogDb = DbContext.JPushMessageLogs.FirstOrDefault(itm => itm.Id == LogId);
                if (LogDb != null)
                    IsNew = false;
                else
                    LogDb = new JPushMessageLog();
            }

            LogDb.Message = Json;
            LogDb.MessageId = (Result != null ? Result.msg_id : 0);
            LogDb.RegistrationId = registrationId;
            LogDb.Response = (Result != null && Result.ResponseResult != null ? Result.ResponseResult.responseContent : string.Empty);
            LogDb.RetryCount = RetryCount;
            LogDb.SendNo = (Result != null ? Result.sendno : 0);
            LogDb.SentTime = DateTime.UtcNow;
            LogDb.VendingMachineId = VendingMachineId;
            
            if (! string.IsNullOrWhiteSpace(ExceptionMessage))
            {
                LogDb.Response = "EXCEPTION! Message: " + ExceptionMessage;
                LogDb.Status = JPushMessageStatus.Exception;
            }
            else
            {
                if (Result != null)
                {
                    if (Result.isResultOK())
                        LogDb.Status = JPushMessageStatus.SentSuccessfully;
                    else
                        LogDb.Status = JPushMessageStatus.SendFailed;
                }
                else
                {
                    LogDb.Response = "no result!!";
                    LogDb.Status = JPushMessageStatus.SendFailed;
                }

                if ((LogDb.Status == JPushMessageStatus.SendFailed) && (RetryCount < 3))
                    LogDb.Status = JPushMessageStatus.FailedRetry;                    
            }

            if (IsNew)
                DbContext.JPushMessageLogs.Add(LogDb);

            DbContext.SaveChanges();
        }
        #endregion
    }
}

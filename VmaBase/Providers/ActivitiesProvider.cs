﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using AutoMapper;

using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataStorage.Resultsets;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models;


namespace VmaBase.Providers
{
    public class ActivitiesProvider : BaseProvider
    {
        #region GetStampTypes
        public BaseResponseDTO GetStampTypesList(BaseAPIParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            StampTypeListDTO ListDTO = new StampTypeListDTO();
            ListDTO.StampTypes = DbContext.StampTypes.Where(itm => itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active).Select(Mapper.Map<StampType, StampTypeDTO>).ToList();
            ListDTO.StampTypesCount = ListDTO.StampTypes.Count;

            if (ListDTO.StampTypesCount > 0)
            {
                string ImageUrlBase = DbContext.StampTypes.First(itm => itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active).ImageUrl;
                int iPos = ImageUrlBase.LastIndexOf('/');
                if (iPos > 0)
                    ImageUrlBase = ImageUrlBase.Substring(0, iPos + 1);

                ListDTO.BaseImageUrl = ImageUrlBase;
            }
            else
                ListDTO.BaseImageUrl = string.Empty;

            return ResultObject(ListDTO);
        }
        #endregion
        #region GetUncompletedCoupons
        public BaseResponseDTO GetUncompletedCoupons(GetUncompletedCouponsParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            List<CouponActivity> ActivitiesDb = null;
            List<UserCoupon> UserCouponsDb = null;
            List<CouponStampDefinition> StampDefinitionsDb = null;
            List<UserCouponStamp> UserCouponStampsDb = null;

            List<long> ActivityIds = DbContext.NewsArticles.Include(itm => itm.Category).Include(itm => itm.Activity)
                .Where(itm => itm.Status == NewsStatus.Online && itm.ActivityId != null && 
                              (!itm.Activity.NoStampsActivity) &&
                              itm.Activity.Status == ActivityStatus.Active &&
                              DateTime.UtcNow >= itm.Activity.LimitStartTime && DateTime.UtcNow <= itm.Activity.LimitEndTime && 
                              itm.Category.Code == GlobalConst.NewsCategory_Activities)
                .Select(itm => itm.Activity.Id).ToList();

            ActivitiesDb = DbContext.CouponActivities.Include(itm => itm.Product).Where(itm => ActivityIds.Contains(itm.Id)).ToList();
            UserCouponsDb = DbContext.UserCoupons.Include(itm => itm.SenderUserData).Include(itm => itm.SenderUserData.UserBase).Where(itm => itm.ActivityId != null && ActivityIds.Contains(itm.ActivityId.Value) && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Collecting).ToList();
            StampDefinitionsDb = DbContext.CouponStampDefinitions.Include(itm => itm.StampType).Where(itm => ActivityIds.Contains(itm.ActivityId)).ToList();

            List<long> UserCouponIds = UserCouponsDb.Select(itm => itm.Id).ToList();
            if (Params.NoStamps != true)
                UserCouponStampsDb = DbContext.UserCouponStamps.Include(itm => itm.UserCoupon).Include(itm => itm.StampType).Include(itm => itm.ReceivedByUser).Where(itm => UserCouponIds.Contains(itm.UserCouponId)).ToList();

            GetActivityCouponParamsDTO CouponParams = new GetActivityCouponParamsDTO();
            CouponParams.AppToken = Params.AppToken;
            CouponParams.SetAuthenticatedUser(Params.AuthenticatedUser);
            CouponParams.SessionToken = Params.SessionToken;
            CouponParams.NoStamps = (Params.NoStamps == null ? false : Params.NoStamps.Value);
            CouponParams.NewsId = null;

            List<CouponActivityDTO> ActivitiesList = new List<CouponActivityDTO>();
            foreach(long ActivityId in ActivityIds)
            {
                CouponParams.ActivityId = ActivityId;
                BaseResponseDTO CouponResponse = GetActivityCoupon(CouponParams, ActivitiesDb, UserCouponsDb, StampDefinitionsDb, UserCouponStampsDb);
                if (CouponResponse.ReturnCode != ReturnCodes.Success)
                    return CouponResponse;

                ActivitiesList.Add((CouponActivityDTO)CouponResponse.Data);
            }

            if (Params.ActivityId != null)
            {
                CouponActivityDTO ActivityMoveTop = ActivitiesList.FirstOrDefault(itm => itm.ActivityId == Params.ActivityId);
                if (ActivityMoveTop != null)
                {
                    ActivitiesList.Remove(ActivityMoveTop);
                    ActivitiesList.Insert(0, ActivityMoveTop);
                }
                else
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString() + " in the activities list!");
            }

            return ResultObject(ActivitiesList, RecordsCount: ActivitiesList.Count);
        }
        #endregion
        #region GetActivityCoupon
        public BaseResponseDTO GetActivityCoupon(GetActivityCouponParamsDTO Params, 
            List<CouponActivity> ActivitiesBase = null, List<UserCoupon> UserCouponsBase = null, List<CouponStampDefinition> StampDefinitionsBase = null, List<UserCouponStamp> UserCouponStampsBase = null)
        {
            if (Params.AuthenticatedUser == null)
            {
                BaseResponseDTO Response = Params.CheckAppAuthorization(this);
                if (Response.ReturnCode != ReturnCodes.Success)
                    return Response;
            }

            if ((Params.UserCouponId == null) && ((Params.ActivityId == null) && (Params.ActivityId <= 0) && (Params.NewsId == null) && (Params.NewsId <= 0)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'newsId' or 'activityId' must be set!");

            long CouponId = 0;
            if ((Params.ActivityId != null) && (Params.ActivityId > 0))
            {
                CouponId = Params.ActivityId.Value;
            }
            else if (Params.UserCouponId == null)
            {
                NewsArticle NewsDb = DbContext.NewsArticles.Include(itm => itm.Category).FirstOrDefault(itm => itm.Id == Params.NewsId.Value);
                if (NewsDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no news article with the id " + Params.NewsId.ToString() + "!");

                if (NewsDb.ActivityId == null)
                    return Error(ReturnCodes.InvalidArguments, "The news with the id " + Params.NewsId.ToString() + " is not an activity or does not have an activity assigned!");
                if (NewsDb.Category.Code != GlobalConst.NewsCategory_Activities)
                    return Error(ReturnCodes.InvalidArguments, "The news with the id " + Params.NewsId.ToString() + " is not an activity!");

                CouponId = NewsDb.ActivityId.Value;
            }

            CouponActivity ActivityDb = null;
            if (Params.UserCouponId == null)
            {
                if (ActivitiesBase == null)
                    ActivityDb = DbContext.CouponActivities.Include(itm => itm.Product).FirstOrDefault(itm => itm.Id == CouponId);
                else
                    ActivityDb = ActivitiesBase.FirstOrDefault(itm => itm.Id == CouponId);

                if ((ActivityDb == null) || (ActivityDb.Status != ActivityStatus.Active && ActivityDb.Status != ActivityStatus.Closed))
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + CouponId.ToString() + "!");
            }

            UserCoupon UserCouponDb = null;
            if (Params.UserCouponId != null)
            {
                UserCouponDb = DbContext.UserCoupons.Find(Params.UserCouponId.Value);
                if (UserCouponDb == null)
                    throw new Exception("Could not find a user coupon with the id " + Params.UserCouponId.Value.ToString());

                ActivityDb = DbContext.CouponActivities.Find(UserCouponDb.ActivityId);
                CouponId = ActivityDb.Id;
            }
            else
            {
                if (UserCouponsBase == null)
                    UserCouponDb = DbContext.UserCoupons.Include(itm => itm.SenderUserData).Include(itm => itm.SenderUserData.UserBase).FirstOrDefault(itm => itm.ActivityId == ActivityDb.Id && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Collecting);
                else
                    UserCouponDb = UserCouponsBase.FirstOrDefault(itm => itm.ActivityId == Params.ActivityId && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Collecting);

                if (UserCouponDb == null)
                {
                    UserCouponDb = new UserCoupon();
                    UserCouponDb.ActivityId = ActivityDb.Id;
                    UserCouponDb.UID = Params.AuthenticatedUser.UID;
                    UserCouponDb.StartTime = DateTime.UtcNow;
                    UserCouponDb.ExpiryTime = ActivityDb.LimitEndTime;
                    UserCouponDb.Status = UserCouponStatus.Collecting;
                    DbContext.UserCoupons.Add(UserCouponDb);
                    DbContext.SaveChanges();
                }
            }

            CouponActivityDTO ActivityDTO = Mapper.Map<CouponActivity, CouponActivityDTO>(ActivityDb);
            ActivityDTO.ActivityId = ActivityDb.Id;
            ActivityDTO.StampsCount = 0;
            ActivityDTO.ExpiryDate = ActivityDb.LimitEndTime; //UserCouponDb.ExpiryTime;
            ActivityDTO.Status = UserCouponDb.Status;
            ActivityDTO.IsNoStampsActivity = ActivityDb.NoStampsActivity;
            ActivityDTO.SendAllowed = ActivityDb.SendingCouponsAllowed;

            if ((Params.NoStamps != true) && (!ActivityDb.NoStampsActivity))
            {
                GetCouponStampsParamsDTO GetStampsParams = new GetCouponStampsParamsDTO();
                GetStampsParams.AppToken = Params.AppToken;
                GetStampsParams.SessionToken = Params.SessionToken;
                GetStampsParams.SetAuthenticatedUser(Params.AuthenticatedUser);
                GetStampsParams.ActivityId = ActivityDb.Id;

                BaseResponseDTO StampsResponse = GetCouponStamps(GetStampsParams, UserCouponDb.Id, ActivityDb, StampDefinitionsBase, UserCouponStampsBase);
                if (StampsResponse.ReturnCode != ReturnCodes.Success)
                    return StampsResponse;

                ActivityDTO.Stamps = (List<CouponStampDTO>)StampsResponse.Data;
                ActivityDTO.StampsCount = ActivityDTO.Stamps.Count;
            }

            if ((Params.NoStamps != true) && (ActivityDb.NoStampsActivity))
            {
                ActivityDTO.Stamps = new List<CouponStampDTO>();
                ActivityDTO.StampsCount = 0;
            }

            return ResultObject(ActivityDTO);
        }
        #endregion
        #region GetCouponStamps
        public BaseResponseDTO GetCouponStamps(GetCouponStampsParamsDTO Params, long? UserCouponId = null, CouponActivity ActivityRecord = null, List<CouponStampDefinition> StampDefinitionsBase = null, List<UserCouponStamp> UserCouponStampsBase = null)
        {
            if (Params.AuthenticatedUser == null)
            {
                BaseResponseDTO Response = Params.CheckAppAuthorization(this);
                if (Response.ReturnCode != ReturnCodes.Success)
                    return Response;
            }

            CouponActivity Coupon = (ActivityRecord != null ? ActivityRecord : DbContext.CouponActivities.Find(Params.ActivityId));
            if (Coupon == null)
                return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString());

            List<CouponStampDefinition> StampDefList = null;
            List<UserCouponStamp> UserStampList = null;

            if (!Coupon.NoStampsActivity)
            {                
                if (StampDefinitionsBase == null)
                    StampDefList = DbContext.CouponStampDefinitions.Include(itm => itm.StampType).Where(itm => itm.ActivityId == Params.ActivityId).ToList();
                else
                    StampDefList = StampDefinitionsBase.Where(itm => itm.ActivityId == Params.ActivityId).ToList();
                
                if (UserStampList == null)
                {
                    if (UserCouponId == null)
                    {
                        UserCoupon UserCouponDb = DbContext.UserCoupons.Include(itm => itm.Activity).OrderByDescending(itm => itm.StartTime).FirstOrDefault(itm => itm.ActivityId == Params.ActivityId && itm.UID == Params.AuthenticatedUser.UID);
                        if (UserCouponDb != null)
                            UserCouponId = UserCouponDb.Id;
                    }

                    UserStampList = DbContext.UserCouponStamps.Include(itm => itm.UserCoupon).Include(itm => itm.StampType).Include(itm => itm.ReceivedByUser)
                                            .Where(itm => itm.UserCoupon.Id == UserCouponId && itm.Status != Models.Enum.UserStampStatus.SentToFriend).ToList();
                }
                else
                {
                    UserStampList = UserCouponStampsBase.Where(itm => itm.UserCoupon.ActivityId == Params.ActivityId && itm.UserCoupon.UID == Params.AuthenticatedUser.UID).ToList();
                }
            }

            List<CouponStampDTO> DTOList = null;
            if (!Coupon.NoStampsActivity)
            {
                DTOList = StampDefList.Select(Mapper.Map<CouponStampDefinition, CouponStampDTO>).ToList();

                // first assign the "typed" stamps
                foreach (CouponStampDTO Stamp in DTOList.Where(itm => itm.StampTypeId != null).OrderBy(itm => itm.Number)) // with OrderByDescending the null values should come last
                {
                    UserCouponStamp UserStamp = UserStampList.FirstOrDefault(itm => itm.StampTypeId == Stamp.StampTypeId);
                    Stamp.Status = Models.Enum.CouponStampStatus.NotCollected;
                    if (UserStamp != null)
                    {
                        Stamp.Status = Models.Enum.CouponStampStatus.Collected;
                        if (UserStamp.Status == Models.Enum.UserStampStatus.ReceivedByFriend)
                        {
                            Stamp.ReceivedByAccount = UserStamp.ReceivedByUser.Account;
                            Stamp.ReceivedByNickname = UserStamp.ReceivedByUser.Nickname;
                        }

                        Stamp.DateCollected = UserStamp.DateCollected;

                        UserStampList.Remove(UserStamp);
                    }
                }

                // then assign the "any type" stamps
                foreach (CouponStampDTO Stamp in DTOList.Where(itm => itm.StampTypeId == null).OrderBy(itm => itm.Number)) // with OrderByDescending the null values should come last
                {
                    UserCouponStamp UserStamp = UserStampList.FirstOrDefault();
                    Stamp.Status = Models.Enum.CouponStampStatus.NotCollected;
                    if (UserStamp != null)
                    {
                        Stamp.Status = Models.Enum.CouponStampStatus.Collected;
                        if (UserStamp.Status == Models.Enum.UserStampStatus.ReceivedByFriend)
                        {
                            Stamp.ReceivedByAccount = UserStamp.ReceivedByUser.Account;
                            Stamp.ReceivedByNickname = UserStamp.ReceivedByUser.Nickname;
                        }

                        Stamp.Number = UserStamp.StampType.Number.ToString("00");
                        Stamp.Name = UserStamp.StampType.Name;
                        Stamp.DateCollected = UserStamp.DateCollected;

                        UserStampList.Remove(UserStamp);
                    }
                }
            }
            else
            {
                // NoStampsActivity => return empty stamps list :)
                UserStampList = new List<UserCouponStamp>();
                DTOList = new List<CouponStampDTO>();
            }

            return ResultObject(DTOList.OrderByDescending(itm => itm.Status).ThenBy(itm => itm.DateCollected).ToList(), 
                RecordsCount: UserStampList.Count);
        }
        #endregion
        #region SendStampsToFriend
        public BaseResponseDTO SendStampsToFriend(SendStampsToFriendParamsDTO Params)
        {
            UserBase FriendUserDb = null;

            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (((Params.UIDReceiver == null) || (Params.UIDReceiver <= 0)) && (string.IsNullOrWhiteSpace(Params.AccountReceiver)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'userIdReceiver' or 'userAccountReceiver' must be set!");

            if ((Params.Stamps == null) || (Params.Stamps.Count <= 0))
                return Error(ReturnCodes.InvalidArguments, "The parameter 'stamps' is missing or does not contain any stamps!");

            // retrieve base data and do validations before
            CouponActivity CouponDb = DbContext.CouponActivities.Find(Params.ActivityId);
            if (CouponDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString());
            if (CouponDb.NoStampsActivity)
                return Error(ReturnCodes.StampsDisabled);
            List<CouponStampDefinition> StampsToCollect = DbContext.CouponStampDefinitions.Include(itm => itm.StampType).Where(itm => itm.ActivityId == CouponDb.Id).ToList();

            UserCoupon UserCouponDb = DbContext.UserCoupons.Where(itm => itm.ActivityId == Params.ActivityId && itm.UID == Params.AuthenticatedUser.UID).OrderByDescending(itm => itm.StartTime).FirstOrDefault();
            if (UserCouponDb == null)
                return Error(ReturnCodes.CouponNoStampsYet);
            if (UserCouponDb.Status == Models.Enum.UserCouponStatus.Disabled)
                return Error(ReturnCodes.CouponDisabled);
            if (UserCouponDb.Status == Models.Enum.UserCouponStatus.Completed || UserCouponDb.Status == Models.Enum.UserCouponStatus.Redeemed)
                return Error(ReturnCodes.CouponAlreadyCompleted);

            if ((Params.UIDReceiver != null) && (Params.UIDReceiver > 0))
                FriendUserDb = DbContext.UserBases.Find(Params.UIDReceiver);
            else
                FriendUserDb = DbContext.UserBases.FirstOrDefault(itm => itm.Account == Params.AccountReceiver && itm.Status != Kooco.Framework.Models.Enum.UserStatus.Blocked);
            if (FriendUserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the " + (Params.UIDReceiver > 0 ? "userId " + Params.UIDReceiver.ToString() : "Account id '" + Params.AccountReceiver + "'!"));
            if (FriendUserDb.Status == Kooco.Framework.Models.Enum.UserStatus.Banned)
                return Error(ReturnCodes.UserBlocked, "The friend user has been banned!");

            if (FriendUserDb.UID == Params.AuthenticatedUser.UID)
                return Error(ReturnCodes.CanNotSendStampToSelf);

            UserCoupon FriendCouponDb = DbContext.UserCoupons.FirstOrDefault(itm => itm.ActivityId == Params.ActivityId && itm.UID == FriendUserDb.UID && itm.Status == UserCouponStatus.Collecting);
            if (FriendCouponDb == null)
            {
                // the friend did not start with a coupon yet -> starting a new one for this user
                FriendCouponDb = new UserCoupon();
                FriendCouponDb.UID = FriendUserDb.UID;
                FriendCouponDb.StartTime = DateTime.UtcNow;
                FriendCouponDb.Status = UserCouponStatus.Collecting;
                FriendCouponDb.ExpiryTime = CouponDb.LimitEndTime;
                FriendCouponDb.ActivityId = CouponDb.Id;
                DbContext.UserCoupons.Add(FriendCouponDb);
            }
            if (FriendCouponDb.Status == Models.Enum.UserCouponStatus.Disabled)
                return Error(ReturnCodes.FriendCouponDisabled);
            if (FriendCouponDb.Status == Models.Enum.UserCouponStatus.Completed || FriendCouponDb.Status == Models.Enum.UserCouponStatus.Redeemed)
                return Error(ReturnCodes.FriendCouponAlreadyCompleted);

            List<UserCouponStamp> StampsCollected = DbContext.UserCouponStamps.Include(itm => itm.StampType).Where(itm => itm.UserCouponId == UserCouponDb.Id && itm.Status != UserStampStatus.SentToFriend).ToList();
            if (StampsCollected.Count <= 0)
                return Error(ReturnCodes.CouponNotEnoughStamps);

            List<UserCouponStamp> FriendStampsCollected = null;
            if (FriendCouponDb == null)
                FriendStampsCollected = new List<UserCouponStamp>();
            else
                FriendStampsCollected = DbContext.UserCouponStamps.Include(itm => itm.StampType).Where(itm => itm.UserCouponId == FriendCouponDb.Id && itm.Status != UserStampStatus.SentToFriend).ToList();

            // prepare the result lists
            StampsSentDTO DTOResult = new StampsSentDTO();
            DTOResult.StampsSent = new List<SendStampTypeDTO>();
            DTOResult.StampsNotSent = new List<SendStampTypeDTO>();
            DTOResult.StampsCountSent = 0;
            DTOResult.StampsCountNotSent = 0;
            DTOResult.SentToFriendInfo = new SentToFriendInfoDTO(DbContext, Params.AuthenticatedUser.UID, FriendUserDb.UID);

            // create a list of stamp types (without amount) to be sent
            List<SendStampTypeDTO> StampsToSend = new List<SendStampTypeDTO>();
            foreach(SendStampTypeDTO StampToSend in Params.Stamps)
            {               
                if ((StampToSend.Amount == null) || (StampToSend.Amount <= 1))
                {
                    SendStampTypeDTO Stamp = new SendStampTypeDTO();
                    Stamp.Amount = 1;
                    Stamp.StampTypeNumber = StampToSend.StampTypeNumber;
                    StampsToSend.Add(Stamp);
                }
                else
                {
                    for(int iNr = 1; iNr <= StampToSend.Amount; iNr++)
                    {
                        SendStampTypeDTO Stamp = new SendStampTypeDTO();
                        Stamp.Amount = 1;
                        Stamp.StampTypeNumber = StampToSend.StampTypeNumber;
                        StampsToSend.Add(Stamp);
                    }
                }
            }

            if (StampsToSend.Count > StampsCollected.Count)
                return Error(ReturnCodes.CouponNotEnoughStamps);

            // check if the user have enough stamps of every stamp type he wants to send
            Dictionary<string, int> StampTypeAmounts = new Dictionary<string, int>();
            foreach(SendStampTypeDTO StampToSend in StampsToSend)
            {
                if (StampTypeAmounts.ContainsKey(StampToSend.StampTypeNumber))
                    StampTypeAmounts[StampToSend.StampTypeNumber]++;
                else
                    StampTypeAmounts.Add(StampToSend.StampTypeNumber, 1);
            }

            foreach(string StampTypeNumber in StampTypeAmounts.Keys)
            {
                if (StampsCollected.Count(itm => itm.StampType.Number.ToString("00") == StampTypeNumber) < StampTypeAmounts[StampTypeNumber])
                    return Error(ReturnCodes.CouponNotEnoughStamps, "The user does not have enough stamps of the stamp type number '" + StampTypeNumber + "'!");
            }

            // check which stamp types the friend user still needs
            // --> 1st create a list of all stamp types to be collected
            Dictionary<string, int> Friend_NeededStampTypes = new Dictionary<string, int>();
            foreach(CouponStampDefinition StampDef in StampsToCollect)
            {
                string StampTypeNumber = (StampDef.StampType != null ? StampDef.StampType.Number.ToString("00") : "00");

                if (Friend_NeededStampTypes.ContainsKey(StampTypeNumber))
                    Friend_NeededStampTypes[StampTypeNumber]++;
                else
                    Friend_NeededStampTypes.Add(StampTypeNumber, 1);
            }

            // --> and check which stamp types already have been collected by the friend user
            foreach(UserCouponStamp StampCollectedFriend in FriendStampsCollected)
            {
                string StampTypeNumber = StampCollectedFriend.StampType.Number.ToString("00");

                if (Friend_NeededStampTypes.ContainsKey(StampTypeNumber) && Friend_NeededStampTypes[StampTypeNumber] > 0)
                    Friend_NeededStampTypes[StampTypeNumber]--;
                else
                    Friend_NeededStampTypes["00"]--;
            }

            // now send all the stamps the friend user can use
            foreach(SendStampTypeDTO StampToSend in StampsToSend)
            {
                UserCouponStamp UserStampDb = StampsCollected.First(itm => itm.StampType.Number.ToString("00") == StampToSend.StampTypeNumber && itm.Status != UserStampStatus.SentToFriend);
                SendStampTypeDTO StampReportDTO = new SendStampTypeDTO();
                string StampTypeNumber = UserStampDb.StampType.Number.ToString("00");

                StampReportDTO.Amount = 1;
                StampReportDTO.StampTypeNumber = StampTypeNumber;

                if (((Friend_NeededStampTypes.ContainsKey(StampTypeNumber) && (Friend_NeededStampTypes[StampTypeNumber] > 0)) || (Friend_NeededStampTypes.ContainsKey("00") && Friend_NeededStampTypes["00"] > 0)))
                {
                    // mark collected stamp as "sent to friend"
                    UserStampDb.Status = UserStampStatus.SentToFriend;

                    // add stamp to friend's coupon
                    UserCouponStamp NewFriendStamp = new UserCouponStamp();
                    NewFriendStamp.UserCouponId = FriendCouponDb.Id;
                    NewFriendStamp.StampTypeId = UserStampDb.StampTypeId;
                    NewFriendStamp.ReceivedByUserId = Params.AuthenticatedUser.UID;
                    NewFriendStamp.DateCollected = DateTime.UtcNow;
                    NewFriendStamp.Status = UserStampStatus.ReceivedByFriend;
                    DbContext.UserCouponStamps.Add(NewFriendStamp);

                    // write transfer log for sending user
                    UserStampLog StampLog = new UserStampLog();
                    StampLog.LogTime = DateTime.UtcNow;
                    StampLog.LogType = UserStampLogType.SentToFriend;
                    StampLog.UID = Params.AuthenticatedUser.UID;
                    StampLog.StampTypeId = UserStampDb.StampTypeId;
                    StampLog.LogText = Params.AuthenticatedUser.Account + " 送給 " + FriendUserDb.Account + " 1個 '" + UserStampDb.StampType.Number + "' (" + UserStampDb.StampType.Name + ") 印花 (Activity " + CouponDb.Id.ToString() + ")";
                    StampLog.UserReferenceId = FriendUserDb.UID;
                    StampLog.UserStamp = UserStampDb;
                    DbContext.UserStampLogs.Add(StampLog);

                    // write transfer log for receiving user
                    UserStampLog StampLogReceiver = new UserStampLog();
                    StampLogReceiver.LogTime = DateTime.UtcNow;
                    StampLogReceiver.LogType = UserStampLogType.ReceivedByFriend;
                    StampLogReceiver.StampTypeId = UserStampDb.StampTypeId;
                    StampLogReceiver.LogText = FriendUserDb.Account + " 收到 " + Params.AuthenticatedUser.Account + "'s 1個 '" + UserStampDb.StampType.Number + "' (" + UserStampDb.StampType.Name + ") 印花 (Activity " + CouponDb.Id.ToString() + ")";
                    StampLogReceiver.UID = FriendUserDb.UID;
                    StampLogReceiver.UserReferenceId = Params.AuthenticatedUser.UID;
                    StampLogReceiver.UserStamp = NewFriendStamp;
                    DbContext.UserStampLogs.Add(StampLogReceiver);

                    if (Friend_NeededStampTypes.ContainsKey(StampTypeNumber) && (Friend_NeededStampTypes[StampTypeNumber] > 0))
                        Friend_NeededStampTypes[StampTypeNumber]--;
                    else
                        Friend_NeededStampTypes["00"]--;

                    DTOResult.StampsCountSent++;
                    DTOResult.StampsSent.Add(StampReportDTO);
                }
                else
                {
                    DTOResult.StampsCountNotSent++;
                    DTOResult.StampsNotSent.Add(StampReportDTO);
                }
            }

            int iStillNeeded = 0;
            foreach(string StampTypeNumber in Friend_NeededStampTypes.Keys)
                iStillNeeded += Friend_NeededStampTypes[StampTypeNumber];

            if (iStillNeeded <= 0)
            {
                FriendCouponDb.Status = UserCouponStatus.Completed;
                FriendCouponDb.CompletionTime = DateTime.UtcNow;
            }

            DbContext.SaveChanges();

            PushNotificationProvider.Current.SendTextNotification(FriendUserDb.UID, "你的朋友送你印花了", "你的朋友" + Params.AuthenticatedUser.Nickname + "剛剛送你" + StampsToSend.Count.ToString() + "個印花。來看看你的飲料卷！", 
                PushAction.Create(PushActionCode.CouponStampsReceived, CouponDb.Id.ToString()));

            return ResultObject(DTOResult);
        }
        #endregion
        #region GetCompletedCoupons
        public BaseResponseDTO GetAllCompletedCouponsList(GetCompletedCouponsParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            List<CompletedCouponDTO> CouponList = DbContext.UserCoupons.Include(itm => itm.Activity).Include(itm => itm.Activity.Product).Include(itm => itm.SenderUserData).Include(itm => itm.SenderUserData.UserBase)
                .Where(itm => (itm.UID == Params.AuthenticatedUser.UID) && DateTime.UtcNow >= itm.Activity.LimitStartTime && DateTime.UtcNow <= itm.Activity.RedeemEndTime && (itm.Activity.Status == ActivityStatus.Active || itm.Activity.Status == ActivityStatus.Closed) &&
                               itm.Status == UserCouponStatus.Completed && (Params.ActivityId == null || itm.ActivityId == Params.ActivityId.Value))
                .Select(Mapper.Map<UserCoupon, CompletedCouponDTO>).ToList();

            List<CompletedCouponDTO> AutoCouponList = DbContext.UserCoupons.Include(itm => itm.AutoCouponCriteria).Include(itm => itm.SenderUserData).Include(itm => itm.SenderUserData.UserBase)
                .Where(itm => (itm.UID == Params.AuthenticatedUser.UID) && 
                              (((itm.AutoCouponCriteria.CouponsExpiryDate == null) && (DateTime.UtcNow <= itm.AutoCouponCriteria.LimitEndTime)) ||
                               ((itm.AutoCouponCriteria.CouponsExpiryDate != null) && (DateTime.UtcNow <= itm.AutoCouponCriteria.CouponsExpiryDate))) &&
                               itm.AutoCouponCriteria.Status != AutoCouponCriteriaStatus.Deleted &&
                               itm.Status == UserCouponStatus.Completed && (Params.CriteriaId == null || itm.AutoCouponCriteriaId == Params.CriteriaId.Value))
                .Select(Mapper.Map<UserCoupon, CompletedCouponDTO>).ToList();

            CouponList.AddRange(AutoCouponList);

            if (Params.IncludeStamps == true)
            {
                foreach(CompletedCouponDTO Coupon in CouponList)
                {
                    if (Coupon.Type == UserCouponType.ActivityCoupon)
                    {
                        Coupon.Stamps = DbContext.UserCouponStamps.Include(itm => itm.UserCoupon).Include(itm => itm.StampType)
                            .Where(itm => itm.UserCoupon.ActivityId == Coupon.ActivityId && itm.UserCoupon.UID == Coupon.UID)
                            .OrderBy(itm => itm.DateCollected)
                            .Select(Mapper.Map<UserCouponStamp, StampTypeDTO>).ToList();
                    }
                }
            }

            return ResultObject(CouponList, RecordsCount: CouponList.Count);
        }
        #endregion
        #region GetCouponActivityList
        public BaseResponseDTO GetCouponActivityList(GetCouponActivityListParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if ((Params.VendingMachineId != null) && (Params.VendingMachineId > 0))
            {
                if (DbContext.VendingMachines.Where(itm => itm.Id == Params.VendingMachineId && itm.Status != VendingMachineStatus.Removed).Select(itm => itm.Id).FirstOrDefault() != Params.VendingMachineId)
                    return Error(ReturnCodes.RecordNotFound, "There is no vending machine with the id " + Params.VendingMachineId.ToString() + " in the database!");
            }

            List<GetCouponActivityListResultDTO> List = DbContext.SP_GetCouponActivityList(Params.AuthenticatedUser.UID, (Params.OnlyCompleted == null ? false : Params.OnlyCompleted.Value), (Params.ForPromotionMachinesList == null ? false : Params.ForPromotionMachinesList.Value), Params.VendingMachineId);
            return ResultObject(List, RecordsCount: List.Count);
        }
        #endregion
        #region SendCouponToFriend
        public BaseResponseDTO SendCouponToFriend(SendCouponToFriendParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (((Params.ActivityId == null) || (Params.ActivityId.Value <= 0)) && ((Params.CriteriaId == null) || (Params.CriteriaId.Value <= 0)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'activityId' or 'criteriaId' must be set!");
            if ((Params.ActivityId != null) && (Params.ActivityId > 0) && (Params.CriteriaId != null) && (Params.CriteriaId > 0))
                return Error(ReturnCodes.InvalidArguments, "'activityId' and 'criteriaId' can not both be set! Please set only one of the parameters 'criteriaId' OR 'activityId'!");

            CouponActivity ActivityDb = null;
            AutoCouponCriteria CriteriaDb = null;

            if ((Params.ActivityId != null) && (Params.ActivityId > 0))
            {
                ActivityDb = DbContext.CouponActivities.Find(Params.ActivityId);
                if ((ActivityDb == null) || (ActivityDb.Status != ActivityStatus.Active && ActivityDb.Status != ActivityStatus.Closed))
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString());
                if (DateTime.UtcNow >= ActivityDb.LimitEndTime)
                    return Error(ReturnCodes.CouponExpired);
                if (!ActivityDb.SendingCouponsAllowed)
                    return Error(ReturnCodes.SendingCouponsNotAllowed);
            }
            
            if ((Params.CriteriaId != null) && (Params.CriteriaId > 0))
            {
                CriteriaDb = DbContext.AutoCouponCriterias.Find(Params.CriteriaId);
                if ((CriteriaDb == null) || (CriteriaDb.Status == AutoCouponCriteriaStatus.Deleted))
                    return Error(ReturnCodes.RecordNotFound, "There is no auto-coupon criteria (rule) with the id " + Params.CriteriaId.ToString());
                if (((CriteriaDb.CouponsExpiryDate != null) && (DateTime.UtcNow >= CriteriaDb.CouponsExpiryDate)) || ((CriteriaDb.CouponsExpiryDate == null) && (DateTime.UtcNow >= CriteriaDb.LimitEndTime)))
                    return Error(ReturnCodes.CouponExpired);
            }

            if (((Params.UIDReceiver == null) || (Params.UIDReceiver <= 0)) && (string.IsNullOrWhiteSpace(Params.AccountReceiver)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'userIdReceiver' or 'userAccountReceiver' must be set!");

            UserCoupon UserCouponDb = null;

            if (ActivityDb != null)
                UserCouponDb = DbContext.UserCoupons.OrderBy(itm => itm.SenderUID).FirstOrDefault(itm => itm.ActivityId == ActivityDb.Id && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Completed);
            else
                UserCouponDb = DbContext.UserCoupons.OrderBy(itm => itm.SenderUID).FirstOrDefault(itm => itm.AutoCouponCriteriaId == CriteriaDb.Id && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Completed);

            if ((UserCouponDb == null) || (UserCouponDb.Status == UserCouponStatus.Collecting))
                return Error(ReturnCodes.CouponNotCompleted);
            if (UserCouponDb.Status == UserCouponStatus.Disabled)
                return Error(ReturnCodes.CouponDisabled);

            if (Params.UIDReceiver == Params.AuthenticatedUser.UID)
                return Error(ReturnCodes.CanNotSendStampToSelf, "Can not send a coupon to the user him/herself!");

            UserData FriendUserDb = null;

            if ((Params.UIDReceiver != null) && (Params.UIDReceiver > 0))
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UID == Params.UIDReceiver);
            else
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UserBase.Account == Params.AccountReceiver && itm.UserBase.Status != Kooco.Framework.Models.Enum.UserStatus.Blocked);
            if (FriendUserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the " + (Params.UIDReceiver > 0 ? "userId " + Params.UIDReceiver.ToString() : "Account id '" + Params.AccountReceiver + "'!"));
            if (FriendUserDb.UserBase.Status == Kooco.Framework.Models.Enum.UserStatus.Banned)
                return Error(ReturnCodes.UserBlocked, "The friend user has been banned!");

            // send coupon to friend
            UserCouponDb.UID = FriendUserDb.UID;
            UserCouponDb.SenderUID = Params.AuthenticatedUser.UID;
            DbContext.SaveChanges();

            PushNotificationProvider.Current.SendTextNotification(FriendUserDb.UID, "你的朋友送你一張免費飲料卷", "你的朋友 " + Params.AuthenticatedUser.Nickname + " 送給你一張免費飲料卷。剛快去兌換吧！", PushAction.Create(PushActionCode.CouponReceived, (ActivityDb != null ? ActivityDb.Id.ToString() : CriteriaDb.Id.ToString()), new KeyValuePair<string, object>[] { new KeyValuePair<string, object>("CouponsType", (ActivityDb != null ? 1 : 2)) } ));

            return Ok();
        }
        #endregion

        #region UserHaveCompletedCoupons
        public BaseResponseDTO UserHaveCompletedCoupons(QueryActivityCouponParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (((Params.ActivityId == null) || (Params.ActivityId <= 0)) && ((Params.NewsId == null) || (Params.NewsId <= 0)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'newsId' or 'activityId' must be set!");

            long CouponId = 0;
            if ((Params.ActivityId != null) && (Params.ActivityId > 0))
                CouponId = Params.ActivityId.Value;
            else
            {
                NewsArticle NewsDb = DbContext.NewsArticles.Include(itm => itm.Category).FirstOrDefault(itm => itm.Id == Params.NewsId.Value);
                if (NewsDb == null) 
                    return Error(ReturnCodes.RecordNotFound, "There is no news article with the id " + Params.NewsId.ToString() + "!");

                if ((NewsDb.ActivityId == null) || (NewsDb.Category.Code != GlobalConst.NewsCategory_Activities))
                    return Error(ReturnCodes.InvalidArguments, "The news with the id " + Params.NewsId.ToString() + " is not an activity or does not have an activity assigned!");

                CouponId = NewsDb.ActivityId.Value;
            }

            CouponActivity ActivityDb = DbContext.CouponActivities.Find(CouponId);
            if (ActivityDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no activity with the activity id " + CouponId.ToString() + "!");

            if (DbContext.UserCoupons.FirstOrDefault(itm => itm.ActivityId == CouponId && itm.UID == Params.AuthenticatedUser.UID && itm.Status == UserCouponStatus.Completed) == null)
                return ResultObject(false);
            else
                return ResultObject(true);
        }
        #endregion
        #region AddStamps
        public BaseResponseDTO AddStamps(AddStampsParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (string.IsNullOrWhiteSpace(Params.QRToken))
                return Error(ReturnCodes.MissingArgument, "The parameter 'qrtoken' is missing!");

            QRActionInfo QRInfo = Utils.GetQRContent(Params.QRToken);
            if (QRInfo.ActionType != QRActionType.ProductStampsQR)
                return Error(ReturnCodes.InvalidQRcode);

            return DbContext.SP_AcquireOrderStamps(Params.AuthenticatedUser, Params.QRToken, Params.ActivityId); // Params.ActivityId currently always is null - the SP will determine the ActivityId itself
        }
        #endregion
        #region RedeemCoupon
        public BaseResponseDTO RedeemCoupon(RedeemCouponParamsDTO Params)
        {
            bool bMachineProcessingOtherOrder = false;
            bool bMachineNotYetProcessedLastMessage = false;

            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No arguments given!");

            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (((Params.ActivityId == null) || (Params.ActivityId.Value <= 0)) && ((Params.CriteriaId == null) || (Params.CriteriaId.Value <= 0)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'activityId' or 'criteriaId' must be set!");
            if ((Params.ActivityId != null) && (Params.ActivityId > 0) && (Params.CriteriaId != null) && (Params.CriteriaId > 0))
                return Error(ReturnCodes.InvalidArguments, "'activityId' and 'criteriaId' can not both be set! Please set only one of the parameters 'criteriaId' OR 'activityId'!");
            if (Params.VendingMachineId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'vendingMachineId' is missing!");
            if (Params.ProductId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'productId' is missing!");

            CouponActivity ActivityDb = null;
            AutoCouponCriteria CriteriaDb = null;

            if ((Params.ActivityId != null) && (Params.ActivityId > 0))
            {
                ActivityDb = DbContext.CouponActivities.Find(Params.ActivityId);
                if ((ActivityDb == null) || (ActivityDb.Status != ActivityStatus.Active && ActivityDb.Status != ActivityStatus.Closed))
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + Params.ActivityId.ToString() + "!");
                if (DateTime.UtcNow >= ActivityDb.RedeemEndTime)
                    return Error(ReturnCodes.CouponExpired);
            }

            if ((Params.CriteriaId != null) && (Params.CriteriaId > 0))
            {
                CriteriaDb = DbContext.AutoCouponCriterias.Find(Params.CriteriaId);
                if ((CriteriaDb == null) || (CriteriaDb.Status == AutoCouponCriteriaStatus.Deleted))
                    return Error(ReturnCodes.RecordNotFound, "There is no auto-coupon criteria (rule) with the id " + Params.CriteriaId.ToString() + "!");
                if (((CriteriaDb.CouponsExpiryDate != null) && (DateTime.UtcNow >= CriteriaDb.CouponsExpiryDate)) || ((CriteriaDb.CouponsExpiryDate == null) && (DateTime.UtcNow >= CriteriaDb.LimitEndTime)))
                    return Error(ReturnCodes.CouponExpired);
            }

            VendingMachine MachineDb = DbContext.VendingMachines.Find(Params.VendingMachineId);
            if (MachineDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no vending machine with the id " + Params.VendingMachineId.ToString() + "!");
            if (MachineDb.Status != Models.Enum.VendingMachineStatus.Active)
                return Error(ReturnCodes.VendingMachineNotAvailable);

            // check the product availability
            MOCCPImportProvider IP = new MOCCPImportProvider();
            string sAvailabilityList = IP.GetMerchandiseAvailability(MachineDb.MOCCPOrganization, MachineDb.MOCCPAssetNo);
            if (sAvailabilityList != null && sAvailabilityList.StartsWith("ERR:"))
                return Error(ReturnCodes.BuyBuyApiConnectionError, sAvailabilityList.Substring(4));

            List<SalesListEntryDTO> SalesList = DbContext.SP_UpdateSalesList(Params.VendingMachineId, sAvailabilityList, Params.AuthenticatedUser.UID, out bMachineProcessingOtherOrder, out bMachineNotYetProcessedLastMessage, true, ActivityIdFilter: Params.ActivityId, CriteriaIdFilter: Params.CriteriaId, ForRedeem: true);
            if (SalesList.FirstOrDefault(itm => itm.ProductId == Params.ProductId) == null)
                return Error(ReturnCodes.ProductNotAvailable);

            // update status of expired orders
            DbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[SalesTransactions] SET [Status] = " + ((int)SalesTransactionStatus.Cancelled).ToString() + 
                                                 " WHERE [Status] = " + ((int)SalesTransactionStatus.Initiated).ToString() + 
                                                   " AND [CreateTime] < dateadd(minute, -" + GlobalConst.SalesTransactionsExpiryTime.ToString() + ", getutcdate())");

            // check if there is still another order currrently processed
            List<long> SalesTransactionIdsConflicting =
                DbContext.Database.SqlQuery<long>("SELECT [Id] FROM [dbo].[SalesTransactions] " +
                                                 " WHERE [Status] = " + ((int)SalesTransactionStatus.Initiated).ToString() +
                                                   " AND [VendingMachineId] = @MachineId"
                                                 , new SqlParameter("@MachineId", MachineDb.Id)).ToList();

            if ((SalesTransactionIdsConflicting != null) && (SalesTransactionIdsConflicting.Count > 0) && (SalesTransactionIdsConflicting[0] > 0))
                return Error(ReturnCodes.VendingMachineAlreadyHasOrder);

            // check if the machine still has not processed the last coupon or is still processing a product selection
            // (because timing is important here - use a direct sql statement to compare the SQL server times for the timeout!)
            List<VendingMachineMessageStatusInfo> LastMessageInfoList =
                DbContext.Database.SqlQuery<VendingMachineMessageStatusInfo>(@"
                        SELECT msg.[Id], msg.[CreateTime], msg.[Status], getutcdate() [CurrentSqlServerTime]
                          FROM [dbo].[VendingMachineMessages] msg
                         WHERE [AssetNo] = @AssetNo
                           AND [Direction] = " + ((int)VendingMachineDirection.Outgoing).ToString() + @"
                           AND [Type] IN (@TypeRedeemCoupon, @TypeProductChosen)
                           AND [Status] = " + ((int)VendingMachineMessageStatus.Created).ToString(),                    
                    new SqlParameter("@AssetNo", MachineDb.MOCCPAssetNo),
                    new SqlParameter("@TypeRedeemCoupon", (int)VendingMachineMessageType.RedeemCoupon),
                    new SqlParameter("@TypeProductChosen", (int)VendingMachineMessageType.ProductChosen)).ToList();

            if ((LastMessageInfoList != null) && (LastMessageInfoList.Count > 0) && (LastMessageInfoList[0].CreateTime >= LastMessageInfoList[0].CurrentSqlServerTime.AddMinutes(-(GlobalConst.SalesTransactionsExpiryTime))))
                return Error(ReturnCodes.VendingMachineAlreadyHasOrder);
            if ((LastMessageInfoList != null) && (LastMessageInfoList.Count > 0))
            {
                DbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[VendingMachineMessages] SET [Status] = @TimeoutStatus WHERE [Id] = @Id", new SqlParameter("@Id", LastMessageInfoList[0].Id), new SqlParameter("@TimeoutStatus", (int)VendingMachineMessageStatus.Timeout));
            }

            // check if the machine (and product) participates in this activity
            if (ActivityDb != null)
            {
                if ((!ActivityDb.AllVendingMachines) &&
                    (DbContext.ActivityParticipatingMachines.FirstOrDefault(itm => itm.ActivityId == ActivityDb.Id && itm.VendingMachineId == MachineDb.Id && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active) == null))
                    return Error(ReturnCodes.VendingMachineNotValidForActivity);
            }
            else
            {
                if ((!CriteriaDb.CouponsForAllMachines) &&
                    (DbContext.AutoCouponCriteriaMachines.FirstOrDefault(itm => itm.CriteriaId == CriteriaDb.Id && itm.MachineId == MachineDb.Id) == null))
                    return Error(ReturnCodes.VendingMachineNotValidForActivity);

                if ((!CriteriaDb.CouponsForAllProducts) &&
                    (DbContext.AutoCouponCriteriaProducts.FirstOrDefault(itm => itm.CriteriaId == CriteriaDb.Id && itm.ProductId == Params.ProductId) == null))
                    return Error(ReturnCodes.ProductNotValidForActivity);
            }

            // get first coupon we can use
            UserCoupon UserCouponDb = null;
            if (ActivityDb != null)
            {
                UserCouponDb = DbContext.UserCoupons.Include(itm => itm.Activity)
                                                    .FirstOrDefault(itm => itm.ActivityId == Params.ActivityId &&
                                                                    (itm.UID == Params.AuthenticatedUser.UID) &&
                                                                    itm.Status == Models.Enum.UserCouponStatus.Completed &&
                                                                    (itm.Activity.Status == ActivityStatus.Active || itm.Activity.Status == ActivityStatus.Closed)
                                                                    );
            }
            else
            {
                UserCouponDb = DbContext.UserCoupons.Include(itm => itm.AutoCouponCriteria)
                                                    .FirstOrDefault(itm => itm.AutoCouponCriteriaId == Params.CriteriaId &&
                                                                    (itm.UID == Params.AuthenticatedUser.UID) &&
                                                                    itm.Status == Models.Enum.UserCouponStatus.Completed &&
                                                                    (itm.AutoCouponCriteria.Status != AutoCouponCriteriaStatus.Deleted)
                                                                    );
            }

            if (UserCouponDb == null)
                return Error(ReturnCodes.NoCompletedCoupon);

            // insert redeem coupon message
            VendingMachineMessage MessageDb = new VendingMachineMessage();
            MessageDb.AssetNo = MachineDb.MOCCPAssetNo;
            MessageDb.CouponId = UserCouponDb.Id;
            MessageDb.ProductId = Params.ProductId;
            MessageDb.VendingMachineId = Params.VendingMachineId;
            MessageDb.Type = Models.Enum.VendingMachineMessageType.RedeemCoupon;
            MessageDb.Direction = Models.Enum.VendingMachineDirection.Outgoing;
            MessageDb.Status = Models.Enum.VendingMachineMessageStatus.Created;
            MessageDb.CreateTime = DateTime.UtcNow;
            DbContext.VendingMachineMessages.Add(MessageDb);
            DbContext.SaveChanges();

            if (!string.IsNullOrWhiteSpace(MachineDb.RegistrationId))
            {
                APIVendingProvider VendingProvider = new APIVendingProvider();
                VendingProvider.SendBuyBuyJPush(MachineDb.Id, MachineDb.RegistrationId, GlobalConst.BuyBuyJPushType_CouponRedeem, UserCouponDb.Id.ToString());
            }

            return Ok();
        }
        #endregion
    }
}

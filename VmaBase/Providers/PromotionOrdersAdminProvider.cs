﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web.Mvc;

using AutoMapper;
using OfficeOpenXml;
using Newtonsoft.Json;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Providers;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Models.Enum;


namespace VmaBase.Providers
{
    public class PromotionOrdersAdminProvider : BaseListDataProvider<PromotionOrder, PromotionOrderAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<PromotionOrderAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<PromotionOrder> Query = DbContext.PromotionOrders.Include(itm => itm.User).Include(itm => itm.User.UserBase).Include(itm => itm.Product);
            return GetPagedList(Params, Query);
        }

        public ActionResult GetExcel()
        {
            IQueryable<PromotionOrder> Query = DbContext.PromotionOrders.Include(itm => itm.User).Include(itm => itm.User.UserBase).Include(itm => itm.Product);
            return GetExcel(Query);
        }

        public BaseResponseDTO MarkPickedUp(long PromotionOrderId)
        {
            PermissionsProvider.RequestAdminRole();

            if (PromotionOrderId <= 0)
                return Error(Shared.ReturnCodes.MissingArgument, "The parameter 'PromotionOrderId' is missing!");

            PromotionOrder OrderDb = DbContext.PromotionOrders.Find(PromotionOrderId);
            if (OrderDb == null)
                return Error(Shared.ReturnCodes.RecordNotFound, "There is no promotion order with the id " + PromotionOrderId.ToString() + " in the database!");

            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.PickedUp)
                return Error(Shared.ReturnCodes.OrderAlreadyPickedUp);
            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.Expired)
                return Error(Shared.ReturnCodes.OrderExpired);
            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.Refund)
                return Error(Shared.ReturnCodes.OrderAlreadyRefund);

            OrderDb.PickupTime = DateTime.UtcNow;
            OrderDb.Status = Models.Enum.PromotionOrderStatus.PickedUp;
            DbContext.SaveChanges();

            DbContext.Entry(OrderDb).Reload();
            return ResultObject(Mapper.Map<PromotionOrder, PromotionOrderAdminDTO>(OrderDb));
        }

        public BaseResponseDTO Refund(PromotionOrderRefundParamsDTO Params)
        {
            PermissionsProvider.RequestAdminRole();

            if (Params.PromotionOrderId <= 0)
                return Error(Shared.ReturnCodes.MissingArgument, "The parameter 'PromotionOrderId' is missing!");

            PromotionOrder OrderDb = DbContext.PromotionOrders.Include(itm => itm.Product).FirstOrDefault(itm => itm.Id == Params.PromotionOrderId);
            if (OrderDb == null)
                return Error(Shared.ReturnCodes.RecordNotFound, "There is no promotion order with the id " + Params.PromotionOrderId.ToString() + " in the database!");

            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.PickedUp)
                return Error(Shared.ReturnCodes.OrderAlreadyPickedUp, "This order already has been picked up and can not be refund anymore!");
            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.Expired)
                return Error(Shared.ReturnCodes.OrderExpired, "This order already is expired. A refund is not possible anymore!");
            if (OrderDb.Status == Models.Enum.PromotionOrderStatus.Refund)
                return Error(Shared.ReturnCodes.OrderAlreadyRefund, "This order already has been refund!");

            OrderDb.OrderNotes = Params.OrderNotes;
            OrderDb.RefundReason = Params.RefundReason;
            OrderDb.RefundTime = DateTime.UtcNow;
            OrderDb.Status = Models.Enum.PromotionOrderStatus.Refund;
            DbContext.SaveChanges();

            try
            {
                DbContext.SP_AddUserPoints(OrderDb.UID, OrderDb.PointsExchanged, Models.Enum.PointsTransactionType.PromotionProductRefund, null, null, null);
            }
            catch (Exception exc)
            {
                OrderDb.Status = Models.Enum.PromotionOrderStatus.PointsExchanged;
                OrderDb.RefundTime = null;
                DbContext.SaveChanges();

                throw new Exception("Error while trying to refund points for the promotion order!", exc);
            }

            PushNotificationProvider.Current.SendTextNotification(OrderDb.UID, "兌換取消了", "非常抱歉！無法兌換您所要的禮品【" + OrderDb.Product.Name + "】。已將積分退還到您的帳戶。" +
                " 原因: " + Utils.GetEnumDisplayName(typeof(PromotionOrderRefundReason), OrderDb.RefundReason.ToString()) + (OrderDb.RefundReason == PromotionOrderRefundReason.OtherReason ? " (" + OrderDb.OrderNotes + ")" : string.Empty));

            DbContext.Entry(OrderDb).Reload();
            return ResultObject(Mapper.Map<PromotionOrder, PromotionOrderAdminDTO>(OrderDb));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Providers;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class VendingProductBrandsAdminProvider : BaseListDataProvider<VendingProductBrand, VendingProductBrandAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<VendingProductBrandAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<VendingProductBrand> Query = DbContext.VendingProductBrands.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.Id);

            return GetPagedList(Params, Query, DataPrepared);
        }

        public Kooco.Framework.Shared.FrameworkReturnCodes DataPrepared(ref List<VendingProductBrandAdminDTO> ProductBrandsList)
        {
            foreach(VendingProductBrandAdminDTO ProductBrandDTO in ProductBrandsList)
            {
                ProductBrandDTO.CanDelete = DbContext.VendingProducts.Where(itm => itm.ProductBrandId == ProductBrandDTO.Id).Count() <= 0;
            }

            return Kooco.Framework.Shared.FrameworkReturnCodes.Success;
        }

        public override FrameworkResponseDTO SaveProperty(SavePropertyParamsDTO Params, BeforeSavePropertyFunction BeforeSave = null)
        {
            long ProductBrandId = Convert.ToInt64(Params.Id);
            if (Params.Name == "Name")
            {
                if (DbContext.VendingProductBrands.Where(itm => itm.Name == Params.Value && itm.Id != ProductBrandId).Count() > 0)
                    return new FrameworkResponseDTO(Kooco.Framework.Shared.FrameworkReturnCodes.AdminInputError, "這個名稱已使用,不可以用一樣的名稱！");
            }

            return base.SaveProperty(Params, BeforeSave);
        }

        public BaseResponseDTO CreateNewProductBrand()
        {
            VendingProductBrand ProductBrandDb = new VendingProductBrand();
            ProductBrandDb.Name = "<請輸入>";
            ProductBrandDb.Status = GeneralStatusEnum.Active;
            DbContext.VendingProductBrands.Add(ProductBrandDb);
            DbContext.SaveChanges();

            return new BaseResponseDTO();
        }

        public BaseResponseDTO RemoveProductBrand(RemoveRecordParamsDTO Params)
        {
            if (Params == null)
                return Error(Shared.ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.Id <= 0)
                return Error(Shared.ReturnCodes.MissingArgument, "The parameter 'Id' is missing!");

            VendingProductBrand ProductBrandDb = DbContext.VendingProductBrands.FirstOrDefault(itm => itm.Id == Params.Id);

            if (DbContext.VendingProducts.Where(itm => itm.ProductBrandId == Params.Id).Count() > 0)
                return Error(Shared.ReturnCodes.AdminInputError, "因為品牌【" + (ProductBrandDb != null ? ProductBrandDb.Name : "") + "】已使用，所以不可以刪除！");

            if (ProductBrandDb != null)
            {
                ProductBrandDb.Status = GeneralStatusEnum.Deleted;
                ProductBrandDb.Name = Utils.MaxLengthString("(DELETED" + ProductBrandDb.Id.ToString() + ") " + ProductBrandDb.Name, VendingProductBrand.NameMaxLength);
                DbContext.SaveChanges();
            }

            return Ok();
        }

        public BaseResponseDTO GetDropDownList()
        {
            List<VendingProductBrandAdminDTO> DDList = DbContext.VendingProductBrands.Where(itm => itm.Status != GeneralStatusEnum.Deleted)
                                                        .Select(Mapper.Map<VendingProductBrand, VendingProductBrandAdminDTO>).ToList();
            return ResultObject(DDList);
        }
    }
}

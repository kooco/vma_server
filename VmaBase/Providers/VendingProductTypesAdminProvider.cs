﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.Enum;
using Kooco.Framework.Providers;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class VendingProductTypesAdminProvider : BaseListDataProvider<VendingProductType, VendingProductTypeAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<VendingProductTypeAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<VendingProductType> Query = DbContext.VendingProductTypes.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.Id);

            return GetPagedList(Params, Query, DataPrepared);
        }

        public Kooco.Framework.Shared.FrameworkReturnCodes DataPrepared(ref List<VendingProductTypeAdminDTO> ProductTypesList)
        {
            foreach(VendingProductTypeAdminDTO ProductTypeDTO in ProductTypesList)
            {
                ProductTypeDTO.CanDelete = DbContext.VendingProducts.Where(itm => itm.ProductTypeId == ProductTypeDTO.Id).Count() <= 0;
            }

            return Kooco.Framework.Shared.FrameworkReturnCodes.Success;
        }

        public override FrameworkResponseDTO SaveProperty(SavePropertyParamsDTO Params, BeforeSavePropertyFunction BeforeSave = null)
        {
            long ProductTypeId = Convert.ToInt64(Params.Id);
            if (Params.Name == "Name")
            {
                if (DbContext.VendingProductTypes.Where(itm => itm.Name == Params.Value && itm.Id != ProductTypeId).Count() > 0)
                    return new FrameworkResponseDTO(Kooco.Framework.Shared.FrameworkReturnCodes.AdminInputError, "這個名稱已使用,不可以用一樣的名稱！");
            }

            return base.SaveProperty(Params, BeforeSave);
        }

        public BaseResponseDTO CreateNewProductType()
        {
            VendingProductType ProductTypeDb = new VendingProductType();
            ProductTypeDb.Name = "<請輸入>";
            ProductTypeDb.Status = GeneralStatusEnum.Active;
            DbContext.VendingProductTypes.Add(ProductTypeDb);
            DbContext.SaveChanges();

            return new BaseResponseDTO();
        }

        public BaseResponseDTO RemoveProductType(RemoveRecordParamsDTO Params)
        {
            if (Params == null)
                return Error(Shared.ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.Id <= 0)
                return Error(Shared.ReturnCodes.MissingArgument, "The parameter 'Id' is missing!");

            VendingProductType ProductTypeDb = DbContext.VendingProductTypes.FirstOrDefault(itm => itm.Id == Params.Id);

            if (DbContext.VendingProducts.Where(itm => itm.ProductTypeId == Params.Id).Count() > 0)
                return Error(Shared.ReturnCodes.AdminInputError, "因為產品類型【" + (ProductTypeDb != null ? ProductTypeDb.Name : "") + "】已使用，所以不可以刪除！");

            if (ProductTypeDb != null)
            {
                ProductTypeDb.Status = GeneralStatusEnum.Deleted;
                ProductTypeDb.Name = Utils.MaxLengthString("(DELETED" + ProductTypeDb.Id.ToString() + ") " + ProductTypeDb.Name, VendingProductType.NameMaxLength);
                DbContext.SaveChanges();
            }

            return Ok();
        }

        public BaseResponseDTO GetDropDownList()
        {
            List<VendingProductTypeAdminDTO> DDList = DbContext.VendingProductTypes.Where(itm => itm.Status != GeneralStatusEnum.Deleted)
                                                        .Select(Mapper.Map<VendingProductType, VendingProductTypeAdminDTO>).ToList();
            return ResultObject(DDList);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaBase.Providers
{
    public class StampTypesAdminProvider : BaseListDataProvider<StampType, StampTypeAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<StampTypeAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<StampType> Query = DbContext.StampTypes.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.Number);

            return GetPagedList(Params, Query);
        }

        public BaseResponseDTO CreateNewStampType()
        {
            int iNumber = 1;

            if (DbContext.StampTypes.Count() > 0)
                iNumber = Convert.ToInt32(DbContext.StampTypes.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.Number).Max(itm => itm.Number)) + 1;

            StampType Stamp = new StampType();
            Stamp.DateCreated = DateTime.Now;
            Stamp.Description = string.Empty;
            Stamp.Number = iNumber;
            Stamp.Name = "<名字>";
            Stamp.ImageUrl = "/images/noimage.png";
            Stamp.Status = GeneralStatusEnum.Inactive;
            DbContext.StampTypes.Add(Stamp);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<StampType, StampTypeAdminDTO>(Stamp));
        }
    }
}

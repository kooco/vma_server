﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AutoMapper;
using OfficeOpenXml;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class LotteryPricesAdminProvider : BaseListDataProvider<LotteryPrice, LotteryPriceItemDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<LotteryPriceItemDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<LotteryPrice> Query = DbContext.LotteryPrices.Where(itm => itm.Status != GeneralStatusEnum.Deleted);

            return GetPagedList(Params, Query);
        }

        /*
        public BaseResponseDTO CreateNewProduct()
        {
            int iNumber = 1;

            if (DbContext.PromotionProducts.Count() > 0)
                iNumber = Convert.ToInt32(DbContext.PromotionProducts.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.ProductNumber).Max(itm => itm.ProductNumber)) + 1;

            PromotionProduct Product = new PromotionProduct();
            Product.CreateTime = DateTime.Now;
            Product.ProductNumber = iNumber;
            Product.Name = "<名字>";
            Product.ImageUrl = "/images/noimage.png";
            Product.Points = 0;
            Product.Status = GeneralStatusEnum.Inactive;
            DbContext.PromotionProducts.Add(Product);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<PromotionProduct, PromotionProductAdminDTO>(Product));
        }*/

        public BaseResponseDTO GetLotteryBaseStatistics()
        {
            PermissionsProvider.RequestAdminRole();

            long PriceIdCola = DbContext.LotteryPrices.Where(itm => itm.Title == "「加系可口可樂」330mL汽水").Select(itm => itm.Id).FirstOrDefault();
            long PriceIdTravelSet = DbContext.LotteryPrices.Where(itm => itm.Title == "「可口可樂」旅行套裝").Select(itm => itm.Id).FirstOrDefault();
            long PriceIdCeramicBowl = DbContext.LotteryPrices.Where(itm => itm.Title == "「可口可樂」陶瓷碗套裝 (2件)").Select(itm => itm.Id).FirstOrDefault();
            long PriceIdStorageTank = DbContext.LotteryPrices.Where(itm => itm.Title == "「可口可樂」儲物罐").Select(itm => itm.Id).FirstOrDefault();
            DateTime LotteryDrawsStartTime = new DateTime(2018, 10, 1);

            int iDays = 7;
            LotteryMiniStatisticsDTO StatisticsDTO = new LotteryMiniStatisticsDTO(iDays);

            for (int iDay = iDays; iDay >= 1; iDay--)
            {
                int iDaysInPast = iDays - iDay;
                DateTime DayBegin = DateTime.Now.AddDays(-iDaysInPast).Date.ToUniversalTime(); // create local time from UTC time when the day starts
                DateTime DayEnd = DateTime.Now.AddDays(-iDaysInPast).Date.ToUniversalTime().AddHours(24);  // create local time from UTC time when the day ends

                StatisticsDTO.Days[iDay-1].GameDrawsCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DayBegin && itm.DrawTime <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].UserDrawedGameCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DayBegin && itm.DrawTime <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Select(itm => itm.UID).Distinct().Count();
                StatisticsDTO.Days[iDay-1].PricesWonTotal = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].PricesWonCola = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdCola && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].PricesWonTravelSet = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdTravelSet && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].PricesWonCeramicBowl = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdCeramicBowl && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].PricesWonStorageTank = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdStorageTank && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                StatisticsDTO.Days[iDay-1].PricesExchangedTotal = DbContext.LotteryPriceCoupons.Where(itm => itm.Status == Models.Enum.LotteryPriceCouponStatus.PickedUp && itm.TimeRedeemed >= DayBegin && itm.TimeRedeemed <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
            }

            StatisticsDTO.Total.GameDrawsCount = DbContext.LotteryDrawLogs.Where(itm => itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery && itm.DrawTime >= LotteryDrawsStartTime).Count();
            StatisticsDTO.Total.UserDrawedGameCount = DbContext.LotteryDrawLogs.Where(itm => itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery && itm.DrawTime >= LotteryDrawsStartTime).Select(itm => itm.UID).Distinct().Count();
            StatisticsDTO.Total.PricesWonTotal = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery)).Count();
            StatisticsDTO.Total.PricesWonCola = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdCola && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
            StatisticsDTO.Total.PricesWonTravelSet = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdTravelSet && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
            StatisticsDTO.Total.PricesWonCeramicBowl = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdCeramicBowl && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
            StatisticsDTO.Total.PricesWonStorageTank = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdStorageTank && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
            StatisticsDTO.Total.PricesExchangedTotal = DbContext.LotteryPriceCoupons.Where(itm => itm.Status == Models.Enum.LotteryPriceCouponStatus.PickedUp && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();

            return ResultObject(StatisticsDTO);
        }

        public BaseResponseDTO GetWatersportsBaseStatistics()
        {
            PermissionsProvider.RequestAdminRole();

            long PriceIdAqua = DbContext.LotteryPrices.Where(itm => itm.Title == "「水動樂」系列飲品1支").Select(itm => itm.Id).FirstOrDefault();
            long PriceIdGarmin = DbContext.LotteryPrices.Where(itm => itm.Title == "\"GARMIN\"運動手錶").Select(itm => itm.Id).FirstOrDefault();
            long PriceIdMoney = DbContext.LotteryPrices.Where(itm => itm.Title == "運動用品現金券$5,000").Select(itm => itm.Id).FirstOrDefault();

            int iDays = 7;
            WatersportsMiniStatisticsDTO StatisticsDTO = new WatersportsMiniStatisticsDTO(iDays);

            for (int iDay = iDays; iDay >= 1; iDay--)
            {
                int iDaysInPast = iDays - iDay;
                DateTime DayBegin = DateTime.Now.AddDays(-iDaysInPast).Date.ToUniversalTime(); // create local time from UTC time when the day starts
                DateTime DayEnd = DateTime.Now.AddDays(-iDaysInPast).Date.ToUniversalTime().AddHours(24);  // create local time from UTC time when the day ends

                StatisticsDTO.Days[iDay - 1].GameDrawsCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DayBegin && itm.DrawTime <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                StatisticsDTO.Days[iDay - 1].UserDrawedGameCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DayBegin && itm.DrawTime <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Select(itm => itm.UID).Distinct().Count();
                StatisticsDTO.Days[iDay - 1].PricesWonTotal = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                StatisticsDTO.Days[iDay - 1].PricesWonAqua = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdAqua && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                StatisticsDTO.Days[iDay - 1].PricesWonGarmin = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdGarmin && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                StatisticsDTO.Days[iDay - 1].PricesWonMoney = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.TimeWon >= DayBegin && itm.TimeWon <= DayEnd && itm.LotteryPriceId == PriceIdMoney && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                StatisticsDTO.Days[iDay - 1].PricesExchangedTotal = DbContext.LotteryPriceCoupons.Where(itm => itm.Status == Models.Enum.LotteryPriceCouponStatus.PickedUp && itm.TimeRedeemed >= DayBegin && itm.TimeRedeemed <= DayEnd && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
            }

            StatisticsDTO.Total.GameDrawsCount = DbContext.LotteryDrawLogs.Where(itm => itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
            StatisticsDTO.Total.UserDrawedGameCount = DbContext.LotteryDrawLogs.Where(itm => itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Select(itm => itm.UID).Distinct().Count();
            StatisticsDTO.Total.PricesWonTotal = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports)).Count();
            StatisticsDTO.Total.PricesWonAqua = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdAqua && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
            StatisticsDTO.Total.PricesWonGarmin = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdGarmin && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
            StatisticsDTO.Total.PricesWonMoney = DbContext.LotteryPriceCoupons.Where(itm => (itm.Status != Models.Enum.LotteryPriceCouponStatus.Expired && itm.Status != Models.Enum.LotteryPriceCouponStatus.Cancelled) && itm.LotteryPriceId == PriceIdMoney && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
            StatisticsDTO.Total.PricesExchangedTotal = DbContext.LotteryPriceCoupons.Where(itm => itm.Status == Models.Enum.LotteryPriceCouponStatus.PickedUp && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();

            return ResultObject(StatisticsDTO);
        }
    }
}

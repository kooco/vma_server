﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using AutoMapper;

using Kooco.Framework.Shared;
using Kooco.Framework.Shared.EntityFramework;
using Kooco.Framework.Authentication;
using Kooco.Framework.Providers;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataStorage.Resultsets;
using VmaBase.Models.DataStorage;

namespace VmaBase.Providers.MaximaNewsModule
{
    public class NewsProviderBase : BaseProvider
    {
        private static object LockLikeUpdateObject = new object();
        private static object LockShareUpdateObject = new object();

        // News Categories
        // ---------------

        #region GetCategories
        public virtual BaseResponseDTO GetCategories()
        {
            return ResultObject(DbContext.NewsCategories.Select(Mapper.Map<NewsCategory, NewsCategoryDTO>).DistinctBy(itm => itm.Code));
        }
        #endregion
        #region AddCategory
        /// <summary>
        ///     新增新的新聞分類。
        /// </summary>
        /// <param name="sCategoryName"></param>
        /// <returns>
        ///     新的NewsCategory object
        /// </returns>
        public virtual BaseResponseDTO AddCategory(string sCategoryName)
        {
            NewsCategory Category = new NewsCategory();
            Category.Name = sCategoryName;

            DbContext.NewsCategories.Add(Category);
            DbContext.SaveChanges();

            return ResultObject(Category);
        }
        #endregion
        #region RemoveCategory
        public virtual BaseResponseDTO RemoveCategory(int CategoryId)
        {
            NewsCategory Category = DbContext.NewsCategories.FirstOrDefault(cat => cat.Id == CategoryId);
            if (Category != null)
                DbContext.NewsCategories.Remove(Category);
            else
                return Error(ReturnCodes.NewsCategoryDoesNotExist);

            DbContext.SaveChanges();
            return Ok();
        }
        #endregion

        // News Tags
        // ---------

        #region AddTagsToNews
        /// <summary>
        ///     Adds the given list of tags to the given news entry.
        ///     For API access
        /// </summary>
        public virtual BaseResponseDTO AddTagsToNews(UpdateNewsTagsParamsDTO Params)
        {
            BaseResponseDTO Result = Params.CheckAppAuthorization(this);
            if (Result.ReturnCode == ReturnCodes.Success)
                return AddTagsToNews(Params.NewsId, Params.Tags.ToArray());
            else
                return Result;
        }

        /// <summary>
        ///     Adds the given list of tag id's to the given news entry
        ///     For internal use / Admin interface
        /// </summary>
        public virtual BaseResponseDTO AddTagsToNews(long NewsId, int[] lstTagIds)
        {
            NewsArticle NewsEntry = DbContext.NewsArticles.FirstOrDefault(news => news.Id == NewsId);
            if (NewsEntry == null)
                return Error(ReturnCodes.InternalServerError);

            NewsEntry.Tags.AddRange(DbContext.NewsTags.Where(itm => lstTagIds.Contains(itm.Id)));
            DbContext.SaveChanges();

            return Ok();
        }

        /// <summary>
        ///     Adds the given list of tag names to the given news entry.
        ///     If one or more tag entries don't exist in the database, then new entries will be created.
        ///     For internal use / Admin interface
        /// </summary>
        public virtual BaseResponseDTO AddTagsToNews(long NewsId, string[] lstTagNames)
        {
            NewsArticle NewsEntry = DbContext.NewsArticles.Include(news => news.Tags).FirstOrDefault(news => news.Id == NewsId);
            return AddTagsToNews(NewsEntry, lstTagNames, true);
        }

        /// <summary>
        ///     Adds the given list of tag names to the given news entry.
        ///     If one or more tag entries don't exist in the database, then new entries will be created.
        ///     PLEASE MAKE SURE THAT THE Tags PROPERTY OF THE NewsEntry OBJECT IS LOADED!
        ///     For internal use / Admin interface
        /// </summary>
        public virtual BaseResponseDTO AddTagsToNews(NewsArticle NewsEntry, string[] lstTagNames)
        {
            return AddTagsToNews(NewsEntry, lstTagNames, true);
        }

        /// <summary>
        ///     Adds the given list of tag names to the given news entry.
        ///     If one or more tag entries don't exist in the database, then new entries will be created.
        ///     All tags which are not already attached to the News article also will be added to the News article.
        ///     Attention: No Tags will be removed!
        ///     PLEASE MAKE SURE THAT THE Tags PROPERTY OF THE NewsEntry OBJECT IS LOADED!
        ///     For internal use / Admin interface
        /// </summary>
        /// <param name="SaveChanges">
        ///     Flag, if the changes made already should be saved into the database. (Default: true)
        /// </param>
        public virtual BaseResponseDTO AddTagsToNews(NewsArticle NewsEntry, string[] lstTagNames, bool SaveChanges)
        {
            NewsTag TagEntry = null;
            if (NewsEntry == null)
                return Error(ReturnCodes.InternalServerError);

            foreach (string sTagName in lstTagNames)
            {
                if (NewsEntry.Tags.Count(tags => tags.Name.ToLower() == sTagName.ToLower().Trim()) <= 0)
                {
                    TagEntry = DbContext.NewsTags.FirstOrDefault(tag => tag.Name.ToLower() == sTagName.ToLower().Trim());
                    if (TagEntry != null)
                        NewsEntry.Tags.Add(TagEntry);
                    else
                    {
                        TagEntry = new NewsTag();
                        TagEntry.Name = sTagName.Trim();
                        DbContext.NewsTags.Add(TagEntry);
                        NewsEntry.Tags.Add(TagEntry);
                    }
                }
            }

            if (SaveChanges)
                DbContext.SaveChanges();

            return Ok();
        }
        #endregion
        #region RemoveTagsFromNews
        /// <summary>
        ///     Removes the given list of tags from the given news entry.
        ///     For API access
        /// </summary>
        public virtual BaseResponseDTO RemoveTagsFromNews(UpdateNewsTagsParamsDTO Params)
        {
            BaseResponseDTO Result = Params.CheckAppAuthorization(this);
            if (Result.ReturnCode == ReturnCodes.Success)
                return RemoveTagsFromNews(Params.NewsId, Params.Tags.ToArray());
            else
                return Result;
        }

        public virtual BaseResponseDTO RemoveTagsFromNews(long NewsId, string[] sTagNames)
        {
            NewsArticle NewsEntry = DbContext.NewsArticles.Include(news => news.Tags).FirstOrDefault(news => news.Id == NewsId);
            List<NewsTag> TagsToDelete = new List<NewsTag>();
            if (NewsEntry == null)
                return Error(ReturnCodes.InternalServerError);

            foreach (NewsTag TagEntry in NewsEntry.Tags)
            {
                if (sTagNames.Contains(TagEntry.Name, StringComparer.OrdinalIgnoreCase))
                    TagsToDelete.Add(TagEntry);
            }

            foreach (NewsTag TagDelete in TagsToDelete)
                NewsEntry.Tags.Remove(TagDelete);

            DbContext.SaveChanges();

            return Ok();
        }
        #endregion

        // News and News Tickers
        // ---------------------

        #region SaveNews
        /// <summary>
        ///     Adds or updates the given news article in the database.
        ///     Only for internal use (Admin UI), NOT to be used by an API call!
        /// </summary>
        /// <param name="NewsArticle"></param>
        /// <returns></returns>
        public virtual BaseResponseDTO SaveNews(NewsDetailsDTO NewsArticle)
        {
            try
            {
                NewsArticle NewsEntry = null;
                List<string> lstTags = NewsArticle.Tags;
                List<NewsTag> lstTagsToRemove = new List<NewsTag>();
                NewsArticle.Tags = null;
                BaseResponseDTO Result = null;

                if (NewsArticle.Id <= 0)
                {
                    NewsEntry = Mapper.Map<NewsDetailsDTO, NewsArticle>(NewsArticle);
                    NewsEntry.CreateTime = DateTime.UtcNow;
                    DbContext.NewsArticles.Add(NewsEntry);

                    if (lstTags != null)
                    {
                        Result = AddTagsToNews(NewsEntry, lstTags.ToArray(), SaveChanges: false);
                        if (Result.ReturnCode != ReturnCodes.Success)
                            return Result;
                    }
                }
                else
                {
                    NewsEntry = DbContext.NewsArticles.Include(news => news.Tags).FirstOrDefault(news => news.Id == NewsArticle.Id);
                    if (NewsEntry != null)
                    {
                        Mapper.Map<NewsDetailsDTO, NewsArticle>(NewsArticle, NewsEntry);

                        if (lstTags != null)
                        {
                            // prepare removing not used tags
                            foreach (NewsTag tag in NewsEntry.Tags)
                                if ((lstTags != null) && (!lstTags.Contains(tag.Name, StringComparer.OrdinalIgnoreCase)))
                                    lstTagsToRemove.Add(tag); // Tag not in the tags list anymore -> remove from news article

                            // add new tags
                            Result = AddTagsToNews(NewsEntry, lstTags.ToArray(), SaveChanges: false);
                            if (Result.ReturnCode != ReturnCodes.Success)
                                return Result;
                        }

                        // now remove the tags to be removed :)
                        foreach (NewsTag tag in lstTagsToRemove)
                            NewsEntry.Tags.Remove(tag);
                    }
                    else
                        return Error(ReturnCodes.RecordNotFound);
                }

                DbContext.SaveChanges();

                return ResultObject(Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsEntry));
            }
            catch (Exception exc)
            {
                VmaBase.Shared.Utils.LogError(exc);
                return Error(ReturnCodes.InternalServerError);
            }
        }
        #endregion

        #region AddNews (Admin)
        public virtual BaseResponseDTO AddNews(NewsDetailsDTO NewsEntry)
        {
            PermissionsProvider.RequestRole(BaseApplicationRoles.Admin);

            NewsCategory CategoryDb = DbContext.NewsCategories.FirstOrDefault(itm => itm.Code == NewsEntry.Category);
            if (CategoryDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news category for the category code '" + NewsEntry.Category + "'!");

            NewsArticle NewsDb = new NewsArticle();
            NewsDb.CategoryId = CategoryDb.Id;
            NewsDb.Content = NewsEntry.Content;
            NewsDb.CreateTime = DateTime.UtcNow;
            NewsDb.ImageUrl = NewsEntry.ImageUrl;
            NewsDb.LikeCount = 0;
            NewsDb.ShareCount = 0;
            NewsDb.Status = NewsEntry.Online ? NewsStatus.Online : NewsStatus.Offline;
            NewsDb.Title = NewsEntry.Title;
            NewsDb.UIDAuthor = AuthenticationManager.CurrentUser.UID;
            NewsDb.LimitStartTime = NewsEntry.LimitStartTime;
            NewsDb.LimitEndTime = NewsEntry.LimitEndTime;
            DbContext.NewsArticles.Add(NewsDb);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsDb));
        }
        #endregion
        #region ModifyNews (Admin)
        public virtual BaseResponseDTO ModifyNews(NewsDetailsDTO NewsEntry)
        {
            PermissionsProvider.RequestRole(BaseApplicationRoles.Admin);

            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsEntry.Id);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsEntry.Id.ToString() + " in the database!");

            NewsCategory CategoryDb = DbContext.NewsCategories.FirstOrDefault(itm => itm.Code == NewsEntry.Category);
            if (CategoryDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news category for the category code '" + NewsEntry.Category + "'!");

            NewsDb.Title = NewsEntry.Title;
            NewsDb.Content = NewsEntry.Content;
            NewsDb.LimitStartTime = NewsEntry.LimitStartTime;
            NewsDb.LimitEndTime = NewsEntry.LimitEndTime;
            if (NewsDb.UIDAuthor != null)
            {
                NewsDb.CategoryId = CategoryDb.Id;
                NewsDb.ImageUrl = (string.IsNullOrWhiteSpace(NewsEntry.ImageUrl) ? NewsDb.ImageUrl : NewsEntry.ImageUrl);
            }

            NewsDb.Status = NewsEntry.Online ? NewsStatus.Online : NewsStatus.Offline;
            if ((NewsDb.LimitStartTime != null) || (NewsDb.LimitEndTime != null))
                NewsDb.Status = (((NewsDb.LimitStartTime == null) || (DateTime.UtcNow >= NewsDb.LimitStartTime)) && ((NewsDb.LimitEndTime == null) || (DateTime.UtcNow <= NewsDb.LimitEndTime)) ? NewsStatus.Online : NewsStatus.Offline);

            NewsDb.UIDEditor = AuthenticationManager.CurrentUser.UID;

            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsDb));
        }
        #endregion
        #region ChangeNewsStatus (Admin)
        public virtual BaseResponseDTO ChangeNewsStatus(long NewsId, bool Online)
        {
            PermissionsProvider.RequestRole(BaseApplicationRoles.Admin);

            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId.ToString() + " in the database!");

            NewsDb.Status = Online ? NewsStatus.Online : NewsStatus.Offline;
            DbContext.SaveChanges();

            return Ok();
        }
        #endregion
        #region DeleteNews (Admin)
        public virtual BaseResponseDTO DeleteNews(long NewsId)
        {
            PermissionsProvider.RequestRole(BaseApplicationRoles.Admin);

            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId.ToString() + " in the database!");

            if (NewsDb.UIDAuthor == null)
                return Error(ReturnCodes.PermissionDenied, "Deletion not possible because this news article is not created with the admin website! Automatically imported news can not be deleted!");

            NewsDb.Status = NewsStatus.Deleted;
            DbContext.SaveChanges();

            return Ok();
        }
        #endregion
        #region SaveNewsImage (Admin)
        /// <summary>
        ///     Changes or adds the image of the news ticker for the given news
        /// </summary>
        /// <returns></returns>
        public virtual BaseResponseDTO SaveNewsImage(long NewsId, string ContentType, string FileExtension, byte[] Data)
        {
            string ImageFilename = "max_news_image_" + NewsId.ToString("0000000");
            string ImageUrl = string.Empty;

            PermissionsProvider.RequestAdminRole();

            NewsArticle NewsDb = DbContext.NewsArticles.FirstOrDefault(itm => itm.Id == NewsId);

            if (NewsDb != null)
            {
                NewsDb.ImageUrl = AliyunUtils.StoreSizedImage(ImageFilename, Data);

                if (!string.IsNullOrWhiteSpace(NewsDb.ImageUrl))
                {
                    DbContext.SaveChanges();
                    return ResultObject(NewsDb.ImageUrl);
                }
                else
                    return Error(ReturnCodes.InternalServerError, "Error while uploading the image to AliYun");
            }
            else
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId.ToString() + "!");
        }
        #endregion

        #region GetNews
        public virtual BaseResponseDTO GetNews(SearchNewsParamsDTO Params)
        {
            int? TotalPages = null;
            //DateTime datTime = DateTime.Now;
            //TimeSpan rawDbAccessTime;
            int iPageNr = (Params.Paging == true && Params.Page != null && Params.Page.Value > 0 ? Params.Page.Value : 1);
            int iPageSize = (Params.MaxItems != null ? Params.MaxItems.Value : 99999);
            bool bOnlyAvailable = Params.OnlyActive == false ? false : true;
            DateTime? DateFrom = Params.DateFrom;
            DateTime? DateTo = Params.DateTo;

            if ((Params.Month != null) && (Params.Year == null))
                return Error(ReturnCodes.MissingArgument, "If the parameter 'month' is set then also 'year' must be set!");
            if (Params.Month != null)
            {
                if ((Params.Month < 1) || (Params.Month > 12))
                    return Error(ReturnCodes.InvalidArguments, "The parameter 'month' is invalid! It must a value between 1 and 12, but '" + Params.Month.Value.ToString() + "' was given!");
                DateFrom = new DateTime(Params.Year.Value, Params.Month.Value, 1);
                DateTo = new DateTime(Params.Year.Value, Params.Month.Value, DateTime.DaysInMonth(Params.Year.Value, Params.Month.Value));
            }
            else if (Params.Year != null)
            {
                DateFrom = new DateTime(Params.Year.Value, 1, 1);
                DateTo = new DateTime(Params.Year.Value, 12, 31);
            }

            RegisterUpdateNewsStatus();

            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No parameters given!");

            if (!string.IsNullOrWhiteSpace(Params.SessionToken))
            {
                BaseResponseDTO Response = Params.CheckAppAuthorization(this);
                if (Response.ReturnCode != ReturnCodes.Success)
                    return Response;
            }
            if ((Params.OnlySubscribed == true) && (string.IsNullOrWhiteSpace(Params.SessionToken)))
                return Error(ReturnCodes.MissingArgument, "The parameter 'sessionToken' is missing. If you set 'onlySubscribed'=true then 'sessionToken' must be set!");

            if (Params.IncludeNewsInTickerList == null)
                Params.IncludeNewsInTickerList = true;
            if (Params.IncludeContent == null)
                Params.IncludeContent = false;

            if (Params.Paging == true && Params.MaxItems == null)
                return Error(ReturnCodes.MissingArgument, "If 'Paging' is set to true then also the parameter 'MaxItems' need to be set!");

            if ((DateFrom != null) && (Params.UseCST == true))
                DateFrom = DateFrom.Value.AddHours(-8);
            if ((DateTo != null) && (Params.UseCST == true))
                DateTo = DateTo.Value.AddHours(-8);

            List<GetNewsListResult> NewsEntries = DbContext.SP_GetNewsList(Params.Category,
                                                                           (Params.Tags != null ? Params.Tags.ToArray() : new string[] { }),
                                                                           Params.IncludeNewsInTickerList.Value,
                                                                           Params.IncludeContent.Value,
                                                                           (Params.OnlySubscribed != null ? Params.OnlySubscribed.Value : false),
                                                                           bOnlyAvailable,
                                                                           (string.IsNullOrWhiteSpace(Params.SessionToken) ? (long?)null : (long?)Params.AuthenticatedUser.UID),
                                                                           iPageNr, iPageSize,
																		   Categories: Params.Categories, DateFrom: Params.DateFrom, DateTo: Params.DateTo);
            //rawDbAccessTime = (DateTime.Now - datTime);

            if (Params.Paging == true)
            {
                long iTotalRows = DbContext.SP_GetNewsListCount(Params.Category, (Params.Tags != null ? Params.Tags.ToArray() : new string[] { }), Params.IncludeNewsInTickerList.Value, (Params.OnlySubscribed != null ? Params.OnlySubscribed.Value : false), bOnlyAvailable, (string.IsNullOrWhiteSpace(Params.SessionToken) ? (long?)null : (long?)Params.AuthenticatedUser.UID), Categories: Params.Categories, DateFrom: Params.DateFrom, DateTo: Params.DateTo);
                TotalPages = (int?)Math.Ceiling((double)((double)iTotalRows / (double)Params.MaxItems.Value));
            }

            //Utils.LogError("Performance Trace for GetNews API - Database/LINQ access took " + (DateTime.Now - datTime).TotalMilliseconds.ToString() + "ms (raw db access time: " + rawDbAccessTime.TotalMilliseconds.ToString() + ")");

            List<GetNewsListEntryDTO> ResultEntries = NewsEntries.Select(Mapper.Map<GetNewsListResult, GetNewsListEntryDTO>).ToList();
            if (Params.UseCST == true)
            {
                foreach(GetNewsListEntryDTO ResultEntry in ResultEntries)
                {
                    ResultEntry.CreateTime = ResultEntry.CreateTime.AddHours(8);
                    ResultEntry.LimitStartTime = (ResultEntry.LimitStartTime == null ? (DateTime?) null : ResultEntry.LimitStartTime.Value.AddHours(8));
                    ResultEntry.LimitEndTime = (ResultEntry.LimitEndTime == null ? (DateTime?)null : ResultEntry.LimitEndTime.Value.AddHours(8));
                }
            }

            return ResultObject(ResultEntries, TotalPages);
        }
        #endregion
        #region GetNewsDetails
        public virtual BaseResponseDTO GetNewsDetails(GetNewsDetailsParamsDTO Params)
        {
            if (Params.AppToken != VmaBase.Shared.GlobalConst.ApiKey)
                return Error(ReturnCodes.InvalidAPIToken);
            if (!string.IsNullOrWhiteSpace(Params.SessionToken))
            {
                BaseResponseDTO Response = Params.CheckAppAuthorization(this);
                if (Response.ReturnCode != ReturnCodes.Success)
                    return Response;
            }

            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.NewsId == null)
                return Error(ReturnCodes.MissingArgument, "newsId parameter is missing!");

            BaseResponseDTO Result = GetNewsDetails(Params.NewsId.Value, (string.IsNullOrWhiteSpace(Params.SessionToken) ? (long?)null : Params.AuthenticatedUser.UID));
            if (Result.ReturnCode == ReturnCodes.Success)
            {
                if (Params.ReadMessages)
                {
                    GetNewsMessagesParamsDTO GetMsgParams = new GetNewsMessagesParamsDTO();
                    GetMsgParams.NewsId = Params.NewsId.Value;
                    GetMsgParams.ParentId = null;
                    GetMsgParams.Recursive = Params.MessagesRecursive;

                    GetMsgParams.AppToken = Params.AppToken;
                    GetMsgParams.SessionToken = Params.SessionToken;
                    GetMsgParams.SetAuthenticatedUser(Params.AuthenticatedUser);

                    NewsDetailsDTO NewsDetails = (NewsDetailsDTO)Result.Data;
                    BaseResponseDTO GetMsgResult = GetMessages(GetMsgParams);

                    if (GetMsgResult.ReturnCode == ReturnCodes.Success)
                    {
                        NewsDetails.Messages = (List<NewsMessageDTO>)GetMsgResult.Data;
                        NewsDetails.MessagesCount = NewsDetails.Messages.Count;
                        return Result;
                    }
                    else
                        return GetMsgResult;
                }
                else
                    ((NewsDetailsDTO)Result.Data).MessagesCount = DbContext.NewsMessages.Where(itm => itm.NewsId == Params.NewsId.Value && itm.Status == NewsStatus.Online).Count();

                return Result;
            }
            else
                return Result;
        }

        public virtual BaseResponseDTO GetNewsDetails(long NewsId, long? UID)
        {
            NewsArticle NewsEntry = DbContext.NewsArticles.Include(itm => itm.Tags).Include(itm => itm.Author).Include(itm => itm.Category).FirstOrDefault(news => news.Id == NewsId && news.Status != NewsStatus.Deleted);
            if (NewsEntry == null)
                return Error(ReturnCodes.RecordNotFound);

            NewsDetailsDTO NewsDetails = Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsEntry);
            if (UID != null)
            {
                NewsDetails.UserLike = (DbContext.UserLikes.FirstOrDefault(itm => itm.UID == UID.Value && itm.NewsId == NewsId && itm.Status == GeneralStatusEnum.Active) != null);
                NewsDetails.OnSubscription = (DbContext.NewsSubscriptions.FirstOrDefault(itm => itm.UID == UID.Value && itm.NewsId == NewsId) != null);
            }

            return ResultObject(NewsDetails);
        }
        #endregion

        #region SaveTickerBanner
        /// <summary>
        ///     Changes or adds the banner image of the news ticker for the given news banner (ticker)
        /// </summary>
        /// <returns></returns>
        public virtual BaseResponseDTO SaveTickerBanner(long NewsId, string ContentType, string FileExtension, byte[] Data)
        {
            string ImageFilename = "max_newsbanner" + NewsId.ToString("0000000");

            PermissionsProvider.RequestAdminRole();

            NewsTicker NewsTickerDb = DbContext.NewsTickers.FirstOrDefault(itm => itm.NewsId == NewsId);

            if (NewsTickerDb != null)
            {
                NewsTickerDb.BannerImageUrl = AliyunUtils.StoreSizedImage(ImageFilename, Data);
                if (!string.IsNullOrWhiteSpace(NewsTickerDb.BannerImageUrl))
                {
                    DbContext.SaveChanges();
                    return ResultObject(NewsTickerDb.BannerImageUrl);
                }
                else
                    return Error(ReturnCodes.InternalServerError, "Error while saving the image to AliYun");
            }
            else
                return Error(ReturnCodes.RecordNotFound, "There is no news ticker for the news id " + NewsId.ToString() + "!");
        }
        #endregion

        #region GetNewsTicker
        /// <summary>
        ///     Returns a list of news tickers
        ///     For use with API and admin (ajax)
        /// </summary>
        public virtual BaseResponseDTO GetNewsTicker(bool GetOfflineToo = false)
        {
            if (GetOfflineToo)
                PermissionsProvider.RequestAdminRole();

            List<NewsTicker> Tickers = DbContext.NewsTickers.Include(ticker => ticker.NewsHeader).Where(ticker => ticker.Status == NewsTickerStatus.Online && ticker.NewsHeader.Status == NewsStatus.Online).OrderBy(ticker => ticker.BannerOrder).ToList();
            List<NewsTicker> OfflineTickers = null;
            List<NewsTickerDTO> lstTickerDTO = new List<NewsTickerDTO>();

            if (GetOfflineToo)
            {
                OfflineTickers = DbContext.NewsTickers.Include(ticker => ticker.NewsHeader).Where(ticker => ticker.Status == NewsTickerStatus.Offline).OrderByDescending(ticker => ticker.NewsHeader.CreateTime).ToList();
                foreach (NewsTicker OfflineTicker in OfflineTickers)
                    Tickers.Add(OfflineTicker);
            }

            foreach (NewsTicker Ticker in Tickers)
            {
                NewsTickerDTO TickerDTO = Mapper.Map<NewsTicker, NewsTickerDTO>(Ticker);
                TickerDTO.BannerImageUrl = GetBannerUrl(Ticker.BannerImageUrl, Ticker.NewsHeader.ImageUrl);

                lstTickerDTO.Add(TickerDTO);
            }

            return ResultObject(lstTickerDTO);
        }
        #endregion
        #region SaveTickerList
        /// <summary>
        ///     Replaces the current ticker list by the given one
        ///     For use with admin site only!
        /// </summary>
        public virtual BaseResponseDTO SaveTickerList(List<NewsTickerDTO> TickerList)
        {
            List<NewsTicker> TickerDbList = DbContext.NewsTickers.ToList();
            List<NewsTicker> lstToDelete = new List<NewsTicker>();

            // check if there are tickers to be deleted
            foreach (NewsTicker TickerDb in TickerDbList)
            {
                if (TickerList.Count(ticker => ticker.NewsId == TickerDb.NewsId) <= 0)
                    lstToDelete.Add(TickerDb);
            }

            // add new tickers and modify existing ones
            foreach (NewsTickerDTO Ticker in TickerList)
            {
                NewsTicker TickerDb = TickerDbList.FirstOrDefault(tickerdb => tickerdb.NewsId == Ticker.NewsId);

                if (TickerDb == null)
                    DbContext.NewsTickers.Add(new NewsTicker()
                    {
                        NewsId = Ticker.NewsId,
                        BannerOrder = Ticker.BannerOrder,
                        UpdateTime = DateTime.UtcNow
                    });
                else
                {
                    if ((TickerDb.BannerOrder != Ticker.BannerOrder) || (TickerDb.NewsId != Ticker.NewsId))
                    {
                        TickerDb.BannerOrder = Ticker.BannerOrder;
                        TickerDb.NewsId = Ticker.NewsId;
                        TickerDb.UpdateTime = DateTime.UtcNow;
                    }
                }
            }

            // remove to be deleted tickers
            foreach (NewsTicker TickerToDelete in lstToDelete)
                DbContext.NewsTickers.Remove(TickerToDelete);

            DbContext.SaveChanges();

            // return updated ticker list
            TickerDbList = DbContext.NewsTickers.Include(ticker => ticker.NewsHeader).AsNoTracking().ToList();
            List<NewsTickerDTO> TickersDTOList = new List<NewsTickerDTO>();
            foreach (NewsTicker Ticker in TickerDbList)
            {
                TickersDTOList.Add(new NewsTickerDTO()
                {
                    BannerOrder = Ticker.BannerOrder,
                    NewsId = Ticker.NewsId,
                    UpdateTime = Ticker.UpdateTime,
                    Title = Ticker.NewsHeader.Title,
                    ImageUrl = Ticker.NewsHeader.ImageUrl
                });
            }

            return ResultObject(TickersDTOList);
        }
        #endregion
        #region AddNewsTicker
        public virtual BaseResponseDTO AddNewsTicker(long NewsId, int BannerOrder = 0)
        {
            PermissionsProvider.RequestAdminRole();

            List<NewsTicker> TickerDbList = DbContext.NewsTickers.Where(itm => itm.Status == NewsTickerStatus.Online).ToList();
            bool bFound = false;

            if (TickerDbList.FirstOrDefault(itm => itm.NewsId == NewsId) != null)
                return Error(ReturnCodes.RecordAlreadyExists, "There is already a news ticker for this news article!");
            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId);

            if (BannerOrder > 0)
            {
                // insert into existing banner order position
                foreach (NewsTicker TickerDb in TickerDbList)
                {
                    if (TickerDb.BannerOrder >= BannerOrder)
                    {
                        TickerDb.BannerOrder++;
                        bFound = true;
                    }
                }

                if (!bFound)
                    BannerOrder = (TickerDbList.OrderBy(itm => itm.BannerOrder).Last().BannerOrder + 1);
            }
            else
                BannerOrder = (TickerDbList.Count > 0 ? (TickerDbList.OrderBy(itm => itm.BannerOrder).Last().BannerOrder + 1) : 1);

            NewsTicker NewTicker = new NewsTicker();
            NewTicker.BannerOrder = BannerOrder;
            NewTicker.NewsId = NewsId;
            NewTicker.UpdateTime = DateTime.UtcNow;
            DbContext.NewsTickers.Add(NewTicker);

            DbContext.SaveChanges();
            DbContext.SP_UpdateNewsStatus();

            NewTicker.BannerImageUrl = GetBannerUrl(NewTicker.BannerImageUrl, NewsDb.ImageUrl);
            return ResultObject(Mapper.Map<NewsTicker, NewsTickerDTO>(NewTicker));
        }
        #endregion
        #region ChangeNewsTickerNews
        public virtual BaseResponseDTO ChangeNewsTickerNews(long NewsIdOld, long NewsIdNew)
        {
            PermissionsProvider.RequestAdminRole();

            NewsTicker TickerDb = DbContext.NewsTickers.FirstOrDefault(itm => itm.NewsId == NewsIdOld);
            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsIdNew);

            if (TickerDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no existing news ticker for the old news id " + NewsIdOld.ToString());
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "This news (" + NewsIdNew.ToString() + ") does not exist in the database!");

            if (DbContext.NewsTickers.FirstOrDefault(itm => itm.NewsId == NewsIdNew) != null)
                return Error(ReturnCodes.RecordAlreadyExists, "There is already another news ticker for this news article!");

            TickerDb.NewsId = NewsIdNew;
            DbContext.SaveChanges();

            TickerDb.BannerImageUrl = GetBannerUrl(TickerDb.BannerImageUrl, NewsDb.ImageUrl);
            return ResultObject(Mapper.Map<NewsTicker, NewsTickerDTO>(TickerDb));
        }
        #endregion
        #region ChangeBannerOrder
        public virtual BaseResponseDTO ChangeBannerOrder(long NewsId, int NewBannerOrder)
        {
            PermissionsProvider.RequestAdminRole();

            List<NewsTicker> TickerDbList = DbContext.NewsTickers.Where(itm => itm.Status == NewsTickerStatus.Online).OrderBy(itm => itm.BannerOrder).ToList();
            NewsTicker TickerToChangeDb = TickerDbList.FirstOrDefault(itm => itm.NewsId == NewsId);
            if (TickerToChangeDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news ticker with the news id " + NewsId.ToString() + " in the database!");

            int iOrder = 1;
            foreach (NewsTicker TickerDb in TickerDbList)
            {
                if (iOrder == NewBannerOrder)
                    iOrder++;

                if (TickerDb.NewsId != NewsId)
                {
                    TickerDb.BannerOrder = iOrder;
                    iOrder++;
                }
            }

            TickerToChangeDb.BannerOrder = NewBannerOrder;

            // to make sure the orders are correct check them again
            int iNr = 1;
            foreach (NewsTicker TickerDb in TickerDbList.OrderBy(itm => itm.BannerOrder))
            {
                if (TickerDb.BannerOrder != iNr)
                    return Error(ReturnCodes.InternalServerError, "Error on reordering the banners - the order mechanism failed - the order numbering is broken!");
                iNr++;
            }

            DbContext.SaveChanges();
            DbContext.SP_UpdateNewsStatus();

            return GetNewsTicker(true);
        }
        #endregion
        #region EnableDisableBanner
        public virtual BaseResponseDTO EnableDisableBanner(long NewsId, bool Enabled)
        {
            PermissionsProvider.RequestAdminRole();

            NewsTicker TickerDb = DbContext.NewsTickers.FirstOrDefault(itm => itm.NewsId == NewsId);
            List<NewsTicker> TickerListDb = DbContext.NewsTickers.OrderBy(itm => itm.BannerOrder).ToList();

            if (TickerDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news ticker with the news id " + NewsId.ToString());

            if (((TickerDb.Status == NewsTickerStatus.Online) && (Enabled)) ||
                ((TickerDb.Status == NewsTickerStatus.Offline) && (!Enabled)))
                return GetNewsTicker(true); // nothing changes..

            if (Enabled)
            {
                // enable a before disabled news
                int iNewBannerOrder = (TickerListDb == null ? 1 : TickerListDb.Where(itm => itm.Status == NewsTickerStatus.Online).OrderBy(itm => itm.BannerOrder).Last().BannerOrder + 1);

                TickerDb.Status = NewsTickerStatus.Online;
                TickerDb.BannerOrder = iNewBannerOrder;
                DbContext.SaveChanges();
            }
            else
            {
                TickerDb.Status = NewsTickerStatus.Offline;
                TickerDb.BannerOrder = 0;

                DbContext.SaveChanges();
            }

            DbContext.SP_UpdateNewsStatus();

            return GetNewsTicker(true);
        }
        #endregion
        #region DeleteBanner
        public virtual BaseResponseDTO DeleteBanner(long NewsId)
        {
            PermissionsProvider.RequestAdminRole();

            NewsTicker TickerDb = DbContext.NewsTickers.FirstOrDefault(itm => itm.NewsId == NewsId);
            if (TickerDb != null)
            {
                DbContext.NewsTickers.Remove(TickerDb);

                // reorder banner list
                List<NewsTicker> TickerListDb = DbContext.NewsTickers.OrderBy(itm => itm.BannerOrder).ToList();

                int iOrderNr = 1;
                foreach (NewsTicker TickerInListDb in TickerListDb.Where(itm => itm.Status == NewsTickerStatus.Online))
                {
                    TickerInListDb.BannerOrder = iOrderNr;
                    iOrderNr++;
                }

                DbContext.SaveChanges();
            }

            return GetNewsTicker(true);
        }
        #endregion

        #region GetBannerUrl
        private string GetBannerUrl(string BannerUrl, string NewsImageUrl)
        {
            string Url = BannerUrl;

            if (string.IsNullOrWhiteSpace(BannerUrl))
                Url = NewsImageUrl;
            if ((!string.IsNullOrWhiteSpace(Url)) && (!Url.ToLower().StartsWith("http://")) && (!Url.ToLower().StartsWith("https://")))
                Url = Kooco.Framework.Shared.Utils.APIServerUrl(Url);

            if (string.IsNullOrWhiteSpace(Url))
                Url = Kooco.Framework.Shared.Utils.APIServerUrl("images/noimage.png");

            return Url;
        }
        #endregion

        // News Messages
        // -------------

        #region AddMessage
        /// <summary>
        ///     Adds a new messsage/comment to the given news article
        /// </summary>
        public virtual BaseResponseDTO AddMessage(long NewsId, long UserId, NewsMessageDTO MessageData)
        {
            bool ContentsCut = false;
            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsId);
            NewsMessage ParentMessage = null;
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "No news with the id " + NewsId.ToString() + " can be found!");

            if (MessageData.ParentId != null)
            {
                ParentMessage = DbContext.NewsMessages.Find(MessageData.ParentId);
                if (ParentMessage == null)
                    return Error(ReturnCodes.RecordNotFound, "No message found for the parent message id " + MessageData.ParentId.ToString() + "!");
            }

            NewsMessage Message = new NewsMessage();
            Message.Message = VmaBase.Shared.Utils.MaxLengthString(MessageData.Message, NewsMessage.MessageMaxLength);
            Message.ParentMessageId = MessageData.ParentId;
            Message.ParentMessage = ParentMessage;
            Message.NewsId = NewsDb.Id;
            Message.NewsHeader = NewsDb;
            Message.LikeCount = 0;
            Message.ShareCount = 0;
            Message.Status = NewsStatus.Online;
            Message.UID = UserId;
            NewsDb.Messages.Add(Message);

            if ((MessageData.Message != null) && (Message.Message.Length > NewsMessage.MessageMaxLength))
                ContentsCut = true;

            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<NewsMessage, NewsMessageDTO>(Message), ContentsCut: ContentsCut);
        }
        #endregion
        #region SaveMessage
        public virtual BaseResponseDTO SaveMessage(SaveNewsMessageParamsDTO Params)
        {
            BaseResponseDTO Result = Params.CheckAppAuthorization(this);
            if (Result.ReturnCode != ReturnCodes.Success)
                return Result;
            Kooco.Framework.Models.DataStorage.Tables.UserBase User = (Kooco.Framework.Models.DataStorage.Tables.UserBase)Result.Data;

            switch (Params.UpdateType)
            {
                case NewsUpdateTypes.Add:
                    return AddMessage(Params.NewsId, User.UID, Params.Message);
                case NewsUpdateTypes.Modify:
                    return SaveMessage(User.UID, Params.Message, false);
                case NewsUpdateTypes.Delete:
                    return Error(ReturnCodes.NewsUpdateTypeNotSupported);
                default:
                    return Error(ReturnCodes.NewsUnknownUpdateType);
            }
        }

        public virtual BaseResponseDTO SaveMessage(long UID, NewsMessageDTO MessageData, bool bAllowCountChanges)
        {
            bool ContentsCut = false;
            NewsMessage MessageDb = DbContext.NewsMessages.Find(MessageData.Id);
            if (MessageDb == null)
                return Error(ReturnCodes.RecordNotFound);

            // only the user who created the message can modify it
            if (UID != MessageDb.UID)
                return Error(ReturnCodes.WritePermissionDenied);

            MessageDb.Message = (MessageData.Message != null ? VmaBase.Shared.Utils.MaxLengthString(MessageData.Message, NewsMessage.MessageMaxLength) : MessageDb.Message);
            if (bAllowCountChanges)
            {
                MessageDb.LikeCount = (MessageData.LikeCount != null ? MessageData.LikeCount.Value : MessageDb.LikeCount);
                MessageDb.ShareCount = (MessageData.ShareCount != null ? MessageData.ShareCount.Value : MessageDb.ShareCount);
            }

            if ((MessageData.Message != null) && (MessageData.Message.Length > NewsMessage.MessageMaxLength))
                ContentsCut = true;

            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<NewsMessage, NewsMessageDTO>(MessageDb), ContentsCut: ContentsCut);
        }
        #endregion

        #region GetMessages
        /// <summary>
        ///     Returns the messages of the given news entry sorted by creation date, the newest messages first.
        ///     If there are answers to messages, then all the answers to the given messages will be in order after the answered message.
        ///     (If recursion switched on)
        /// </summary>
        public virtual BaseResponseDTO GetMessages(GetNewsMessagesParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.NewsId == null)
                return Error(ReturnCodes.MissingArgument, "newsId parameter missing!");
            if (Params.AppToken != VmaBase.Shared.GlobalConst.ApiKey)
                return Error(ReturnCodes.InvalidAPIToken);

            if ((!string.IsNullOrWhiteSpace(Params.SessionToken)) && (Params.AuthenticatedUser == null))
            {
                BaseResponseDTO Response = Params.CheckAppAuthorization(this);
                if (Response.ReturnCode != ReturnCodes.Success)
                    return Response;
            }

            List<NewsMessageDTO> Messages = DbContext.SP_GetNewsMessages(Params.NewsId.Value, (string.IsNullOrWhiteSpace(Params.SessionToken) ? (long?)null : Params.AuthenticatedUser.UID), Params.ParentId, Params.Recursive);
            List<NewsMessageDTO> lstMessages = null;

            if (Params.Recursive)
            {
                lstMessages = new List<NewsMessageDTO>();

                foreach (NewsMessageDTO Message in Messages.Where(msg => msg.ParentId == null))
                    AddMessageToDTOListWithAnswers(lstMessages, Messages, Message);

                return ResultObject(lstMessages, RecordsCount: lstMessages.Count);
            }
            else
                return ResultObject(Messages, RecordsCount: Messages.Count);
        }

        private void AddMessageToDTOListWithAnswers(List<NewsMessageDTO> lstMessages, List<NewsMessageDTO> Source, NewsMessageDTO MessageToAdd)
        {
            lstMessages.Add(MessageToAdd);

            foreach (NewsMessageDTO AnswerMessage in Source.Where(msg => msg.ParentId == MessageToAdd.Id))
                AddMessageToDTOListWithAnswers(lstMessages, Source, AnswerMessage);
        }
        #endregion

        // Likes and Shares Management :)
        // ------------------------------

        #region AddLikeOrShare
        public virtual BaseResponseDTO AddLikeOrShare(bool AddLike, bool bAdd, NewsLikeOrShareParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "Please add (form/json) parameters to the POST request!");

            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            switch (Params.Type)
            {
                case NewsArticleTypes.NewsArticle:
                    if (AddLike)
                        return AddNewsLike(Params, (!bAdd));
                    else
                        return AddNewsShare(Params);

                case NewsArticleTypes.NewsMessage:
                    if (AddLike)
                        return AddMessageLike(Params, (!bAdd));
                    else
                        return AddMessageShare(Params);

                default:
                    return Error(ReturnCodes.NewsUnknownArticleType);
            }
        }
        #endregion

        #region AddNewsLike
        public virtual BaseResponseDTO AddNewsLike(NewsLikeOrShareParamsDTO Params, bool bMinus = false)
        {
            NewsArticle NewsDb = DbContext.NewsArticles.FirstOrDefault(news => news.Id == Params.NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound);

            UserLike LikeDb = DbContext.UserLikes.FirstOrDefault(itm => itm.UID == Params.AuthenticatedUser.UID && itm.NewsId == Params.NewsId);
            if ((LikeDb != null) && (LikeDb.Status == GeneralStatusEnum.Active) && (!bMinus))
                return Error(ReturnCodes.RecordAlreadyExists, "This user already added a like for this news id");
            if ((LikeDb == null) && (bMinus))
                return Error(ReturnCodes.RecordNotFound, "This user did not like this news id so far!");
            if ((LikeDb != null) && (LikeDb.Status == GeneralStatusEnum.Inactive) && (bMinus))
                return Error(ReturnCodes.RecordAlreadyExists, "This user already disliked this news article!");

            lock (LockLikeUpdateObject)
            {
                DbContext.Entry<NewsArticle>(NewsDb).Reload(); // force reload from database, don't use a cached version

                if (bMinus)
                    NewsDb.LikeCount--;
                else
                    NewsDb.LikeCount++;

                if (LikeDb == null)
                {
                    LikeDb = new UserLike();
                    LikeDb.UID = Params.AuthenticatedUser.UID;
                    LikeDb.NewsId = Params.NewsId;
                    DbContext.UserLikes.Add(LikeDb);
                }

                LikeDb.Status = (bMinus ? GeneralStatusEnum.Inactive : GeneralStatusEnum.Active);

                DbContext.SaveChanges();
            } // lock process until the update is done

            return ResultObject(NewsDb.LikeCount);
        }
        #endregion
        #region AddMessageLike
        public virtual BaseResponseDTO AddMessageLike(NewsLikeOrShareParamsDTO Params, bool bMinus = false)
        {
            NewsMessage MessageDb = DbContext.NewsMessages.FirstOrDefault(msg => msg.MessageId == Params.MessageId);
            if (MessageDb == null)
                return Error(ReturnCodes.RecordNotFound);

            UserLike LikeDb = DbContext.UserLikes.FirstOrDefault(itm => itm.UID == Params.AuthenticatedUser.UID && itm.MessageId == Params.MessageId);
            if ((LikeDb != null) && (LikeDb.Status == GeneralStatusEnum.Active) && (!bMinus))
                return Error(ReturnCodes.RecordAlreadyExists, "This user already added a like for this news message id");
            if ((LikeDb == null) && (bMinus))
                return Error(ReturnCodes.RecordNotFound, "This user did not like this news message id so far!");
            if ((LikeDb != null) && (LikeDb.Status == GeneralStatusEnum.Inactive) && (bMinus))
                return Error(ReturnCodes.RecordAlreadyExists, "This user already disliked this news message!");

            lock (LockLikeUpdateObject)
            {
                DbContext.Entry<NewsMessage>(MessageDb).Reload(); // force reload from database, don't use a cached version

                if (bMinus)
                    MessageDb.LikeCount--;
                else
                    MessageDb.LikeCount++;

                if (LikeDb == null)
                {
                    LikeDb = new UserLike();
                    LikeDb.UID = Params.AuthenticatedUser.UID;
                    LikeDb.MessageId = Params.MessageId;
                    DbContext.UserLikes.Add(LikeDb);
                }

                LikeDb.Status = (bMinus ? GeneralStatusEnum.Inactive : GeneralStatusEnum.Active);

                DbContext.SaveChanges();
            } // lock process until the update is done

            return ResultObject(MessageDb.LikeCount);
        }
        #endregion
        #region AddNewsShare
        public virtual BaseResponseDTO AddNewsShare(NewsLikeOrShareParamsDTO Params)
        {
            NewsArticle NewsDb = DbContext.NewsArticles.FirstOrDefault(news => news.Id == Params.NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound);

            lock (LockShareUpdateObject)
            {
                DbContext.Entry<NewsArticle>(NewsDb).Reload(); // force reload from database, don't use a cached version

                NewsDb.ShareCount++;
                DbContext.SaveChanges();
            } // lock process until the update is done

            return ResultObject(NewsDb.ShareCount);
        }
        #endregion
        #region AddMessageShare
        public virtual BaseResponseDTO AddMessageShare(NewsLikeOrShareParamsDTO Params)
        {
            NewsMessage MessageDb = DbContext.NewsMessages.FirstOrDefault(msg => msg.MessageId == Params.MessageId);
            if (MessageDb == null)
                return Error(ReturnCodes.RecordNotFound);

            lock (LockShareUpdateObject)
            {
                DbContext.Entry<NewsMessage>(MessageDb).Reload(); // force reload from database, don't use a cached version

                MessageDb.ShareCount++;
                DbContext.SaveChanges();
            } // lock process until the update is done

            return ResultObject(MessageDb.ShareCount);
        }
        #endregion
        #region UpdateNewsStatus
        public void UpdateNewsStatus()
        {
            DbContext.SP_UpdateNewsStatus();
        }
        #endregion

        #region RegisterUpdateNewsStatus (private)
        private static object NewsRegLock { get; set; } = "lockme";
        private void RegisterUpdateNewsStatus()
        {
            lock (NewsRegLock)
            {
                if (HttpContext.Current.Cache["_do_updatenewsstatus"] == null)
                    HttpContext.Current.Cache.Insert("_do_updatenewsstatus", "active", null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Normal, DoUpdateNews);
            }
        }

        public void DoUpdateNews(string key, object value, System.Web.Caching.CacheItemRemovedReason reason)
        {
            try
            {
                ApplicationDbContext CallbackDbContext = new ApplicationDbContext();
                CallbackDbContext.SP_UpdateNewsStatus();
            }
            catch (Exception exc)
            {
                VmaBase.Shared.Utils.LogError(exc, "DoUpdateNews task");
            }
        }
        #endregion
    }
}
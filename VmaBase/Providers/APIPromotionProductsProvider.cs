﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class APIPromotionProductsProvider : BaseProvider
    {
        public static string ProductStockLock = "productstocks";

        #region GetProductList
        public BaseResponseDTO GetProductList(GetPromoProductListParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            PromotionProductListDTO DTO = new PromotionProductListDTO();
            List<PromotionProduct> ProductListDb = null;
            if (Params.AlsoNotAvailable == true)
                ProductListDb = DbContext.PromotionProducts.Where(itm => itm.Status != Kooco.Framework.Models.Enum.GeneralStatusEnum.Deleted).ToList();
            else
                ProductListDb = DbContext.PromotionProducts.Where(itm => itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active).ToList();

            DTO.ProductsCount = ProductListDb.Count;
            DTO.BaseImageUrl = (ProductListDb.Count > 0 ? ProductListDb[0].ImageUrl.Substring(0, ProductListDb[0].ImageUrl.LastIndexOf('/') + 1) : string.Empty);
            DTO.Products = ProductListDb.Select(Mapper.Map<PromotionProduct, PromotionProductDTO>).ToList();

            foreach (PromotionProductDTO Product in DTO.Products)
                Product.PickupLocation = ConfigManager.DefaultPromotionProductsPickupLocation;

            return ResultObject(DTO);
        }
        #endregion
        #region OrderProduct
        public BaseResponseDTO OrderProduct(PromoOrderParamsDTO Params)
        {
            string OrderNr = string.Empty;
            bool bCommit = false;
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            if (Params.ProductId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'productId' is missing or is 0!");

            try
            {
                DbContext.Database.BeginTransaction();

                lock (ProductStockLock)
                {
                    PromotionProduct ProductDb = DbContext.PromotionProducts.Find(Params.ProductId);
                    if ((ProductDb == null) || (ProductDb.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Deleted))
                        return Error(ReturnCodes.RecordNotFound, "There is no promotion product with the id " + Params.ProductId.ToString() + "!");

                    if (ProductDb.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Inactive)
                        return Error(ReturnCodes.ProductNotAvailable);
                    if (ProductDb.InStock <= 0)
                        return Error(ReturnCodes.ProductNotEnoughInStock);

                    DbContext.SP_CheckExpiredPoints(Params.AuthenticatedUser.UID);
                    UserData UserDb = DbContext.UserDatas.Find(Params.AuthenticatedUser.UID);
                    if (UserDb == null)
                        return Error(ReturnCodes.UserNotFound);

                    if (UserDb.PointsTotal < ProductDb.Points)
                        return Error(ReturnCodes.NotEnoughPoints);

                    // reduce stock amount
                    ProductDb.InStock--;

                    // create a unique order number
                    bool bOk = false;
                    int iMainIterationNr = 1;                    

                    while (!bOk)
                    {
                        Random rnd = new Random();
                        PromotionOrder FoundOrder = null;
                        string OrderNrBase = DateTime.Now.Millisecond.ToString("000") + (DateTime.Now.Day * rnd.Next(1, 3)).ToString("00");
                        OrderNr = OrderNrBase + rnd.Next(0, 999).ToString("000");
                        int iIterationNr = 1;

                        iMainIterationNr++;

                        while ((FoundOrder = DbContext.PromotionOrders.FirstOrDefault(itm => itm.OrderNumber == OrderNr)) != null)
                        {
                            OrderNr = OrderNrBase + rnd.Next(0, 999).ToString("000");

                            iIterationNr++;
                            if (iIterationNr >= 50)
                                break; // maximum tries with the same base order number: 50 times
                        }

                        if (FoundOrder == null)
                            bOk = true;
                        if ((!bOk) && (iMainIterationNr >= 5)) // 250 tries in sum is the absolute maximum.. if that doesn't work, then the logic has to be changed..
                            return Error(ReturnCodes.InternalServerError, "Could not create a unique order number for the product exchange! Please contact the server development team.");
                    }

                    // create order
                    PromotionOrder OrderDb = new PromotionOrder();
                    OrderDb.OrderNumber = OrderNr;
                    OrderDb.PointsExchanged = ProductDb.Points;
                    OrderDb.Status = Models.Enum.PromotionOrderStatus.PointsExchanged;
                    OrderDb.UID = Params.AuthenticatedUser.UID;
                    OrderDb.ProductId = ProductDb.Id;
                    OrderDb.OrderNotes = string.Empty;
                    DbContext.PromotionOrders.Add(OrderDb);

                    DbContext.SaveChanges(); // save inside the transaction, so we get an id for the order

                    // create point log
                    int iRetCode = DbContext.SP_UseUserPoints(Params.AuthenticatedUser.UID, ProductDb.Points, Models.Enum.PointsTransactionType.PromotionProductExchange, null, null, null, OrderDb.Id); // this order id we need for the points log
                    if (iRetCode != 1)
                        throw new Exception("Error when trying to deduct the points from the user account! ReturnCode: " + iRetCode.ToString());

                    DbContext.Database.CurrentTransaction.Commit();
                    bCommit = true;
                }

                return ResultObject(OrderNr);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in API-Provider-method 'OrderProduct'");
                return Error(ReturnCodes.InternalServerError, "Error in API-Provider-Method 'OrderProduct': " + exc.Message);
            }
            finally
            {
                if ((!bCommit) && (DbContext.Database.CurrentTransaction != null))
                    DbContext.Database.CurrentTransaction.Rollback();
            }
        }
        #endregion
        #region GetExchangedProducts
        public BaseResponseDTO GetExchangedProducts(GetExchangedProductsParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != Shared.ReturnCodes.Success)
                return Response;

            DateTime? MinOrderDate = null;
            if (Params.MaxAgeDays != null)
                MinOrderDate = DateTime.UtcNow.AddDays(-(Params.MaxAgeDays.Value));

            PromotionOrderListDTO OrderListDTO = new PromotionOrderListDTO();
            List<PromotionOrder> OrderListDb = DbContext.PromotionOrders.Include(itm => itm.Product).Where(itm => itm.UID == Params.AuthenticatedUser.UID &&
                                                                                      (itm.Status == Models.Enum.PromotionOrderStatus.PointsExchanged || Params.IncludePickedUp != false) &&
                                                                                      (itm.PickupTime == null || MinOrderDate == null || itm.PickupTime >= MinOrderDate) &&
                                                                                      (itm.RefundTime == null || MinOrderDate == null || itm.RefundTime >= MinOrderDate))
                                                                                      .OrderByDescending(itm => itm.ExchangeTime).ToList();

            OrderListDTO.Orders = OrderListDb.Select(Mapper.Map<PromotionOrder, PromotionOrderDTO>).ToList();
            OrderListDTO.OrdersCount = OrderListDTO.Orders.Count;
            OrderListDTO.BaseImageUrl = (OrderListDb.Count > 0 ? OrderListDb[0].Product.ImageUrl.Substring(0, OrderListDb[0].Product.ImageUrl.LastIndexOf('/') + 1) : string.Empty);

            return ResultObject(OrderListDTO);
        }
        #endregion
    }
}

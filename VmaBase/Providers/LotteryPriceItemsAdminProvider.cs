﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.AdminResponse;


namespace VmaBase.Providers
{
    /// <summary>
    ///     LotteryPriceItemsAdminProvider
    ///     ------------------------------
    ///     
    ///     Provider class for reporting the availability of the prices
    ///     for: Admin Interface
    /// </summary>
    public class LotteryPriceItemsAdminProvider : BaseListDataProvider<LotteryPriceAvailabilityItem, LotteryPriceItemAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<LotteryPriceItemAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<LotteryPriceAvailabilityItem> Query = DbContext.LotteryPriceAvailabilityItems.Include(itm => itm.Price).Include(itm => itm.WonByUser).Include(itm => itm.WonByUser.UserBase)
                                                                .Where(itm => itm.Status != Kooco.Framework.Models.Enum.GeneralStatusEnum.Deleted);

            // customized search criterias
            StandardSearchFilterCriteria CriteriaToRemove = null;
            foreach (StandardSearchFilterCriteria Criteria in Params.FilterCriterias)
            {
                if (Criteria.DTOPropertyName == "IsWon")
                {
                    if (Convert.ToBoolean(Criteria.Value) == true)
                        Query = Query.Where(itm => itm.WonByUID != null);
                    else
                        Query = Query.Where(itm => itm.WonByUID == null);

                    CriteriaToRemove = Criteria;
                }
            }
            if (CriteriaToRemove != null)
                Params.FilterCriterias.Remove(CriteriaToRemove);

            // pass to standard GetPagedList
            return GetPagedList(Params, Query);
        }
    }
}

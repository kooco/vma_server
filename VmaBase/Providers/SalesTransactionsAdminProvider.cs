﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects.AdminResponse;

namespace VmaBase.Providers
{
    public class SalesTransactionsAdminProvider : BaseListDataProvider<SalesTransaction, SalesTransactionAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<SalesTransactionAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<SalesTransaction> Query = DbContext.SalesTransactions
                                                    .Include(itm => itm.User).Include(itm => itm.User.UserBase)
                                                    .Include(itm => itm.VendingMachine)
                                                    .Include(itm => itm.Product)
                                                    .Where(itm => itm.Status != Models.Enum.SalesTransactionStatus.Cancelled);

            return GetPagedList(Params, Query);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;

using AutoMapper;
using Newtonsoft.Json;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects.MOCCP;
using VmaBase.Models.Enum;

namespace VmaBase.Providers
{
    public class MOCCPImportProvider : BaseProvider
    {
        public void StartVendingImport(bool UpdateImages = false)
        {
            ImportProducts(UpdateImages);
            ImportVendingMachines();
        }

        #region ImportProducts
        private void ImportProducts(bool UpdateImages = false)
        {
            ImportLogProvider Log = new ImportLogProvider();

            try
            {
                Log.StartImport(Models.Enum.ImportLogType.VendingProductsImport);
                Console.WriteLine("Retrieving Product List.. (api/buybuy/GetMerchandiseList)");

                string sProductsResult = GetData("api/buybuy/GetMerchandiseList");
                Console.WriteLine("List retrieved.");

                List<VendingProduct> VendingProductsDb = DbContext.VendingProducts.Where(itm => itm.Status != VendingProductStatus.Deleted).ToList();
                List<BuyBuyGetMerchandiseListEntry> MerchandiseList = JsonConvert.DeserializeObject<List<BuyBuyGetMerchandiseListEntry>>(sProductsResult);
                Console.WriteLine("List extracted. " + MerchandiseList.Count.ToString() + " Products retrieved.");

                int iAdded = 0;
                int iModified = 0;
                int iProducts = MerchandiseList.Count;
                int iActive = MerchandiseList.Where(itm => itm.IsActive).Count();
                bool bUpdateImages = false;
                int iNr = 0;

                if (((Log.ImportTicket.RunNumber % 500) == 0) || (UpdateImages))
                {
                    bUpdateImages = true;
                    Log.WriteLog("Updating images" + (UpdateImages ? " (explicitelly activated)" : " (activated every 500 runs)"));
                    Console.WriteLine("(also updating images)");
                }

                List<VendingProduct> ProductsDb = DbContext.VendingProducts.ToList();

                foreach (BuyBuyGetMerchandiseListEntry ImportProduct in MerchandiseList)
                {
                    iNr++;
                    if (((iNr % 5) == 0) || (iNr == 1))
                        Console.WriteLine("processing product " + iNr.ToString() + " / " + MerchandiseList.Count.ToString());

                    VendingProduct ProductDb = ProductsDb.FirstOrDefault(itm => itm.MOCCPGoodsId == ImportProduct.MerchandiseID);
                    if (ProductDb == null)
                    {
                        ProductDb = new VendingProduct();
                        ProductDb.MOCCPGoodsId = ImportProduct.MerchandiseID;
                        ProductDb.MOCCPOrganization = ImportProduct.Organization;
                        ProductDb.MOCCPInternalId = ImportProduct.InternalId;
                        ProductDb.Name = ImportProduct.MerchandiseName;
                        ProductDb.NameEN = ImportProduct.MerchandiseName;
                        ProductDb.ProductTypeId = GlobalConst.ProductTypeId_Others;
                        ProductDb.ProductBrandId = GlobalConst.ProductBrandId_Others;
                        ProductDb.Volume = ImportProduct.Spec;
                        ProductDb.ImageUrl = StoreImage(Log, ImportProduct.MerchandiseID, ImportProduct.PictureUrl, "vendingproduct_" + ImportProduct.MerchandiseID + ".png");
                        ProductDb.Status = (ImportProduct.IsActive ? VendingProductStatus.Available : VendingProductStatus.NotAvailable);
                        DbContext.VendingProducts.Add(ProductDb);

                        iAdded++;
                    }
                    else
                    {
                        if (ProductDb.Status != VendingProductStatus.Deleted)
                        {
                            if ((ProductDb.Name != ImportProduct.MerchandiseName) ||
                                (ProductDb.Volume != ImportProduct.Spec) ||
                                (ProductDb.ImageUrl != ImportProduct.PictureUrl) ||
                                ((ProductDb.Status == VendingProductStatus.Available) != ImportProduct.IsActive) ||
                                (ProductDb.MOCCPInternalId != ImportProduct.InternalId) ||
                                (ProductDb.MOCCPOrganization != ImportProduct.Organization) ||
                                (bUpdateImages))
                            {
                                ProductDb.Name = ImportProduct.MerchandiseName;
                                ProductDb.NameEN = ImportProduct.MerchandiseName;
                                ProductDb.MOCCPOrganization = ImportProduct.Organization;
                                ProductDb.MOCCPInternalId = ImportProduct.InternalId;
                                ProductDb.Volume = ImportProduct.Spec;
                                ProductDb.Status = (ImportProduct.IsActive ? VendingProductStatus.Available : VendingProductStatus.NotAvailable);
                                ProductDb.ModifyTime = DateTime.UtcNow;

                                if (bUpdateImages)
                                    ProductDb.ImageUrl = StoreImage(Log, ImportProduct.MerchandiseID, ImportProduct.PictureUrl, "vendingproduct_" + ImportProduct.MerchandiseID + ".png");

                                iModified++;
                            }
                        }
                    }
                }

                DbContext.SaveChanges();
                Log.WriteLog(iAdded.ToString() + " new products added. " + iModified.ToString() + " existing products modified.");
                Console.WriteLine(iAdded.ToString() + " new products added. " + iModified.ToString() + " existing products modified.");

                // "delete" products which are not in the list (anymore)
                Console.WriteLine("deleting not retrieved products..");
                int iDeleted = 0;
                foreach (VendingProduct ProductDb in ProductsDb)
                {
                    if (MerchandiseList.FirstOrDefault(itm => itm.MerchandiseID == ProductDb.MOCCPGoodsId) == null)
                    {
                        ProductDb.Status = VendingProductStatus.Deleted;
                        iDeleted++;
                    }
                }

                DbContext.SaveChanges();
                if (iDeleted > 0)
                {
                    Log.WriteLog(iDeleted.ToString() + " products deleted");
                    Console.WriteLine(iDeleted.ToString() + " products deleted");
                }

                Log.FinishImport();
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in ImportProducts!");
                Log.StopImport();
            }
        }
        #endregion
        #region ImportVendingMachines
        private void ImportVendingMachines()
        { 
            ImportLogProvider Log = new ImportLogProvider();

            try
            {
                Log.StartImport(Models.Enum.ImportLogType.VendingMachinesImport);

                string sMachinesResult = GetData("api/buybuy/GetVendingList");

                List<VendingMachine> VendingMachinesDb = DbContext.VendingMachines.ToList();
                List<BuyBuyGetVendingListEntry> MachinesList = JsonConvert.DeserializeObject<List<BuyBuyGetVendingListEntry>>(sMachinesResult);

                int iAdded = 0;
                int iModified = 0;
                int iProducts = MachinesList.Count;
                int iActive = MachinesList.Where(itm => itm.IsActive).Count();

                foreach (BuyBuyGetVendingListEntry ImportMachine in MachinesList)
                {
                    VendingMachine MachineDb = VendingMachinesDb.FirstOrDefault(itm => itm.MOCCPAssetNo == ImportMachine.AssetNo);
                    if (MachineDb == null)
                    {
                        MachineDb = new VendingMachine();
                        MachineDb.MOCCPAssetNo = ImportMachine.AssetNo;
                        MachineDb.Name = (string.IsNullOrWhiteSpace(MachineDb.Name) ? ImportMachine.AssetNo : MachineDb.Name);
                        MachineDb.QRToken = ImportMachine.AssetNo;
                        MachineDb.Location = ImportMachine.Location;
                        MachineDb.MOCCPOrganization = ImportMachine.Organization;
                        MachineDb.RegistrationId = ImportMachine.RegistrationID;
                        if ((ImportMachine.Latitude == 0) && (ImportMachine.Longitude == 0))
                            // unfortunatelly the coca cola's test data does not contain a valid location -> we will just use a fixed one for that
                            MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation((decimal?)113.540867, (decimal?)22.186984);
                        else
                            MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation((decimal?) ImportMachine.Longitude, (decimal?) ImportMachine.Latitude);
                        MachineDb.Status = (ImportMachine.IsActive ? VendingMachineStatus.Active : VendingMachineStatus.NotReady);
                        MachineDb.City = "澳門";
                        DbContext.VendingMachines.Add(MachineDb);

                        iAdded++;
                    }
                    else
                    {
                        if ((MachineDb.Location != ImportMachine.Location) ||
                            (MachineDb.GeoLocation.Latitude != ImportMachine.Latitude) ||
                            (MachineDb.GeoLocation.Longitude != ImportMachine.Longitude) ||
                            (MachineDb.MOCCPOrganization != ImportMachine.Organization) ||
                            (MachineDb.RegistrationId != ImportMachine.RegistrationID) ||
                            ((MachineDb.Status == VendingMachineStatus.Active) != ImportMachine.IsActive) ||
                            ((MachineDb.Status == VendingMachineStatus.Broken) != (ImportMachine.LastResponseTime < DateTime.Now.AddMinutes(-1))))
                        {
                            MachineDb.MOCCPOrganization = ImportMachine.Organization;
                            MachineDb.Location = ImportMachine.Location;
                            MachineDb.RegistrationId = ImportMachine.RegistrationID;
                            if ((ImportMachine.Latitude == 0) && (ImportMachine.Longitude == 0))
                                // unfortunatelly the coca cola's test data does not contain a valid location -> we will just use a fixed one for that
                                MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation((decimal?)113.540867, (decimal?)22.186984);
                            else
                                MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation((decimal?)ImportMachine.Longitude, (decimal?)ImportMachine.Latitude);
                            MachineDb.Status = (ImportMachine.IsActive ? VendingMachineStatus.Active : VendingMachineStatus.NotReady);
                            if (ImportMachine.LastResponseTime < DateTime.Now.AddMinutes(-1))
                                MachineDb.Status = VendingMachineStatus.Broken;
                            MachineDb.ModifyTime = DateTime.UtcNow;

                            iModified++;
                        }
                    }
                }

                DbContext.SaveChanges();
                Log.WriteLog(iAdded.ToString() + " new vending machines added. " + iModified.ToString() + " existing vending machines modified.");

                // "delete" vending machines which are not in the list (anymore)
                int iDeleted = 0;
                foreach (VendingMachine MachineDb in VendingMachinesDb)
                {
                    if (MachinesList.FirstOrDefault(itm => itm.AssetNo == MachineDb.MOCCPAssetNo) == null)
                    {
                        MachineDb.Status = VendingMachineStatus.Removed;
                        iDeleted++;
                    }
                }

                DbContext.SaveChanges();
                if (iDeleted > 0)
                    Log.WriteLog(iDeleted.ToString() + " vending machines deleted");

                Log.FinishImport();
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in ImportVendingMachines!");
                Log.StopImport();
            }
        }
        #endregion

        #region GetMerchandiseAvailability
        public string GetMerchandiseAvailability(string Organization, string AssetNo)
        {
            //return "091N:10|5.4;099N:18;1361:5;1366:0;377:16;467:8;468:22;469:2;550:9;1678:11;1635:25;161706:12;"; // DEBUG - test data while the BuyBuy service is not available
            string sAvailabilityResult = string.Empty;

            try
            {
                sAvailabilityResult = GetData("api/buybuy/GetMerchandiseRemainQuantity?Organization=" + HttpUtility.UrlEncode(Organization) + "&AssetNo=" + HttpUtility.UrlEncode(AssetNo));                
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "while getting data from the BuyBuy API service");
                return "ERR:can not connect to or download from the BuyBuy API service!";
            }

            List<BuyBuyRemainQuantityListEntry> QuantityList = null;
            if (string.IsNullOrWhiteSpace(sAvailabilityResult))
            {
                if (AssetNo.StartsWith("TEST"))
                    return "091N:10|5;099N:18|3;1361:5|3.5;1366:0|3;377:16|4;467:8|10;468:22|8;469:2|5;550:9|6.5;1678:11|3;1635:25|2;161706:12|4;";
                else
                    QuantityList = new List<BuyBuyRemainQuantityListEntry>();
            }
            else
            {                
                try
                {
                    QuantityList = JsonConvert.DeserializeObject<List<BuyBuyRemainQuantityListEntry>>(sAvailabilityResult);
                }
                catch (Exception exc)
                {
                    Utils.LogError(exc, "while parsing the BuyBuy /GetMerchandiseRemainQuantity API result");
                    return "ERR:unable to parse the BuyBuy API service result for this machine's sales list";
                }
            }

            StringBuilder sbProducts = new StringBuilder();
            foreach(BuyBuyRemainQuantityListEntry Entry in QuantityList)
            {
                sbProducts.Append(Entry.MerchandiseID + ":" + Entry.RemainQuantity.ToString() + "|" + Entry.Price.ToString(GlobalConst.CultureEN) + ";");
            }

            return sbProducts.ToString();
        }
        #endregion

        #region StoreImage
        /// <summary>
        ///     Downloads the given image
        ///     and stores it into our AliYun storage and creates a standardized url, which will be returned then
        /// </summary>
        private string StoreImage(ImportLogProvider Log, string MerchandiseId, string SourceImageUrl, string Filename)
        {
            WebClient client = new WebClient();

            try
            {
                Log.WriteLog("downloading image " + SourceImageUrl);
                int iRetCode = 0;
                string sMessage = string.Empty;
                
                string sImageData = GetData(SourceImageUrl, false, out iRetCode, out sMessage);
                byte[] imageData = null;
                if (string.IsNullOrWhiteSpace(sImageData))
                {
                    Log.WriteLog("Can not download image for merchandise product id " + MerchandiseId + ": The BuyBuy API returned the ErrorCode " + iRetCode.ToString() + ". Message: " + sMessage);

                    imageData = client.DownloadData("http://vma-admin-jp.azurewebsites.net/images/noimage.png");
                    return Kooco.Framework.Shared.AliyunUtils.StoreImage(Filename, imageData, false);
                }
                else
                {
                    imageData = Convert.FromBase64String(sImageData.Substring(1, sImageData.Length - 2));
                    return Kooco.Framework.Shared.AliyunUtils.StoreImage(Filename, imageData, false);
                }
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error while trying to save the imported image " + SourceImageUrl + " (MOCCPImportProvider)");

                byte[] imageData = client.DownloadData("http://vma-admin-jp.azurewebsites.net/images/noimage.png");
                return Kooco.Framework.Shared.AliyunUtils.StoreImage(Filename, imageData, false);
            }
            finally
            {
                if (client != null)
                    client.Dispose();
            }
        }
        #endregion
        #region GetData
        private string GetData(string apiPath)
        {
            int iRetCode = 0;
            string sMessage = string.Empty;
            return GetData(apiPath, false, out iRetCode, out sMessage);
        }

        private string GetData(string apiPath, bool bExceptionOnErrors, out int iRetCode, out string sMessage)
        {
            string sAPIUrl = ConfigManager.BuyBuyApiUrl.Trim();
            if (!sAPIUrl.EndsWith("/"))
                sAPIUrl += "/";
            string sUrl = sAPIUrl + apiPath;
            if (apiPath.StartsWith("http:") || apiPath.StartsWith("https:"))
                sUrl = apiPath;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string sData = client.DownloadString(sUrl);

                MOCCPVendingApiResult Result = JsonConvert.DeserializeObject<MOCCPVendingApiResult>(sData);
                if (Result.ReturnCode != 0)
                {
                    if (bExceptionOnErrors)
                        throw new Exception("The BuyBuy API '" + apiPath + "' returned an error! ReturnCode: " + Result.ReturnCode.ToString() + ". Message: " + Result.Message);
                    else
                    {
                        iRetCode = Result.ReturnCode;
                        sMessage = Result.Message;
                        return string.Empty;
                    }
                }

                iRetCode = 0;
                sMessage = string.Empty;

                string sDecrypted = Utils.MOCCPVendingApi_DESDecrypt(Result.EncryptedData);
                return sDecrypted;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using AutoMapper;

using Kooco.Framework.Providers;

using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models;
using VmaBase.Models.DataTransferObjects.MOCCP;


namespace VmaBase.Providers
{    
    public class MOCCPMachineQueryProvider : BaseProvider
    {
        private const int NewMachineTokenMinutes = 5;
        private static string LockMachineCache = "VendingMachinesQRCache";

        #region MachineQRTokensCache
        private static Dictionary<string, MachineQRInfo> _MachineQRTokensCache = null;
        private Dictionary<string, MachineQRInfo> MachineQRTokensCache
        {
            get
            {
                if (_MachineQRTokensCache != null)
                    return _MachineQRTokensCache;

                _MachineQRTokensCache = (Dictionary<string, MachineQRInfo>) HttpContext.Current.Cache["vma_machineqrtokencache"];
                if (_MachineQRTokensCache == null)
                {
                    lock (LockMachineCache)
                    {
                        _MachineQRTokensCache = new Dictionary<string, MachineQRInfo>();
                        HttpContext.Current.Cache.Add("vma_machineqrtokencache", _MachineQRTokensCache, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0), System.Web.Caching.CacheItemPriority.High, null);
                    }
                }

                return _MachineQRTokensCache;
            }
        }
        #endregion
        #region SetToken
        private void SetToken(string AssetNo, string NewToken)
        {
            lock (LockMachineCache)
            {
                if (MachineQRTokensCache.ContainsKey(AssetNo))
                {
                    MachineQRTokensCache[AssetNo].CurrentToken = NewToken;
                    MachineQRTokensCache[AssetNo].GenerateNewTokenAt = DateTime.Now.AddMinutes(NewMachineTokenMinutes);
                }
                else
                {
                    MachineQRTokensCache.Add(AssetNo, new MachineQRInfo() { AssetNo = AssetNo, CurrentToken = NewToken, GenerateNewTokenAt = DateTime.Now.AddMinutes(NewMachineTokenMinutes) });
                }

                HttpContext.Current.Cache["vma_machineqrtokencache"] = _MachineQRTokensCache;
            }
        }
        #endregion

        #region GetMachineQR
        public BaseResponseDTO GetMachineQR(BuyBuyMachineParamsDTO Params)
        {
            MachineQRInfo QRInfo = null;

            BaseResponseDTO Response = Params.CheckValidity();
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (MachineQRTokensCache.ContainsKey(Params.AssetNo))
                QRInfo = MachineQRTokensCache[Params.AssetNo];
            if ((QRInfo != null) && (DateTime.Now <= QRInfo.GenerateNewTokenAt))
            {
                BuyBuyGetMachineQRResultDTO ResultDTO = new BuyBuyGetMachineQRResultDTO();
                ResultDTO.QRToken = QRInfo.CurrentToken;

                return ResultObject(ResultDTO);
            }

            // generate new token
            VendingMachine MachineDb = DbContext.VendingMachines.FirstOrDefault(itm => itm.MOCCPOrganization == Params.Organization && itm.MOCCPAssetNo == Params.AssetNo);
            if (MachineDb != null)
            {
                string sNewToken = Utils.GenerateQR(QRActionType.VendingMachineQR, MachineDb.Id.ToString());                

                MachineDb.QRTokenOld = MachineDb.QRToken;
                MachineDb.QRToken = sNewToken;
                DbContext.SaveChanges();
                SetToken(Params.AssetNo, sNewToken);

                BuyBuyGetMachineQRResultDTO ResultDTO = new BuyBuyGetMachineQRResultDTO();
                ResultDTO.QRToken = sNewToken;

                return ResultObject(ResultDTO);
            }
            else
                return Error(ReturnCodes.RecordNotFound, "VMA can not find any vending machine record for the assetNo '" + Params.AssetNo + "' in the database!");
        }
        #endregion
        #region QueryMerchandiseSelection
        public BaseResponseDTO QueryMerchandiseSelection(BuyBuyMachineParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckValidity();
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            return DbContext.SP_GetMachineMsgProductSelection(Params.Organization, Params.AssetNo);
        }
        #endregion
        #region QueryRedeemCoupon
        public BaseResponseDTO QueryRedeemCoupon(BuyBuyMachineParamsDTO Params)
        {
            if (Params.ApiKey != GlobalConst.BuyBuyApiKey)
                return Error(ReturnCodes.InvalidAPIToken);

            return DbContext.SP_GetMachineMsgRedeemCoupon(Params.Organization, Params.AssetNo);
        }
        #endregion
        #region CompleteOrder
        public BaseResponseDTO CompleteOrder(BuyBuyCompleteOrderParamsDTO Params)
        {
            long? UID = 0;
            long? ProductId = 0;

            BaseResponseDTO Response = Params.CheckValidity();
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (Params.Cancel != 0 && Params.Cancel != 1)
                return Error(ReturnCodes.InvalidArguments, "The parameter 'cancel' is invalid! It can be only 1 or 0!");

            Response = DbContext.SP_CompleteMachineOrder(Params.Organization, Params.AssetNo, Params.InternalId, Params.VmaTransactionId, Params.BuyBuyTransactionNr, Params.Cancel == 1, out UID, out ProductId);
            if ((Response.ReturnCode == ReturnCodes.Success) && (UID != null) && (UID > 0))
            {
                PushNotificationProvider.Current.SendTextNotification(UID.Value, "訂單完成", string.Empty,
                    PushAction.Create(PushActionCode.OrderCompleted, (ProductId != null ? ProductId.Value.ToString() : "0")));
            }
            if ((Response.ReturnCode == ReturnCodes.CancelledSuccesfully) && (UID != null) && (UID > 0))
            {
                PushNotificationProvider.Current.SendTextNotification(UID.Value, "訂單取消", string.Empty,
                    PushAction.Create(PushActionCode.OrderCancelled, (ProductId != null ? ProductId.Value.ToString() : "0")));
            }

            BuyBuyCompleteOrderResultDTO ResultDTO = new BuyBuyCompleteOrderResultDTO();
            ResultDTO.QRToken = (Response.Data == null ? string.Empty : Response.Data.ToString());
            Response.Data = ResultDTO;

            return Response;
        }
        #endregion
        #region ValidateCoupon
        private class CheckMachineForCouponResult
        {
            public long ActivityId { get; set; }
            public bool AllVendingMachines { get; set; }
            public bool IsForVendingMachines { get; set; }
            public int? MOCCPInternalProductId { get; set; }
        }

        public class PriceCouponInfo
        {
            public long Id { get; set; }
            public LotteryPriceCouponStatus Status { get; set; }
        }

        public BaseResponseDTO ValidateCoupon(BuyBuyValidateCouponParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckValidity();
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (string.IsNullOrWhiteSpace(Params.CouponToken))
                return Error(ReturnCodes.MissingArgument, "The parameter 'CouponToken' is missing!");
            if (string.IsNullOrWhiteSpace(Params.AssetNo))
                return Error(ReturnCodes.MissingArgument, "The parameter 'assetNo' is missing!");
            if (string.IsNullOrWhiteSpace(Params.Organization))
                return Error(ReturnCodes.MissingArgument, "The parameter 'organization' is missing!");

            PriceCouponInfo LotteryPriceCouponInfo = DbContext.LotteryPriceCoupons.Where(itm => itm.PickupNumber == Params.CouponToken).Select(itm => new PriceCouponInfo { Id = itm.Id, Status = itm.Status }).FirstOrDefault();
            if ((LotteryPriceCouponInfo == null) || (LotteryPriceCouponInfo.Id <= 0) || (LotteryPriceCouponInfo.Status == LotteryPriceCouponStatus.Cancelled) || (LotteryPriceCouponInfo.Status == LotteryPriceCouponStatus.Expired))
                return Error(ReturnCodes.LotteryCouponTokenInvalid);
            if (LotteryPriceCouponInfo.Status == LotteryPriceCouponStatus.PickedUp)
                return Error(ReturnCodes.LotteryCouponTokenAlreadyUsed);

            List<CheckMachineForCouponResult> ResultList = DbContext.Database.SqlQuery<CheckMachineForCouponResult>(
                                         "select act.[Id] [ActivityId], cast(act.[AllVendingMachines] as bit) [AllVendingMachines], price.[UseActivityCoupon] [IsForVendingMachines], prod.[MOCCPInternalId] [MOCCPInternalProductId] " +
                                          " from [dbo].[LotteryPriceCoupons] coupon " +
                                    " inner join [dbo].[LotteryPrices] price on price.[Id] = coupon.[LotteryPriceId] " +
                                    " inner join [dbo].[LotteryGames] game on game.[Id] = price.[LotteryGameId]" +
                                    " inner join [dbo].[CouponActivities] act on act.[Id] = game.[ConnectedActivityId]" +
                               " left outer join [dbo].[VendingProducts] prod on prod.[Id] = price.[ActivityCouponProductId] " +
                                    "      where coupon.[Id] = @PriceCouponId ", new SqlParameter("@PriceCouponId", LotteryPriceCouponInfo.Id)).ToList();
            if (ResultList.Count > 0)
            {
                CheckMachineForCouponResult Result = ResultList[0];

                if (!Result.IsForVendingMachines)
                    return Error(ReturnCodes.LotteryPriceNotForVendingMachines);

                if (!Result.AllVendingMachines)
                {
                    if (DbContext.ActivityParticipatingMachines.Include(itm => itm.VendingMachine)
                        .Where(itm => itm.VendingMachine.MOCCPAssetNo == Params.AssetNo && itm.VendingMachine.MOCCPOrganization == Params.Organization && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active)
                        .Count() <= 0)
                        return Error(ReturnCodes.LotteryPriceRedeemNotAtThisMachine);
                }

                LotteryPriceCoupon CouponDb = DbContext.LotteryPriceCoupons.Find(LotteryPriceCouponInfo.Id);
                CouponDb.Status = LotteryPriceCouponStatus.PickedUp;
                CouponDb.TimeRedeemed = DateTime.UtcNow;
                DbContext.SaveChanges();

                BuyBuyValidateCouponResultDTO ResultDTO = new BuyBuyValidateCouponResultDTO();
                ResultDTO.InternalId = Result.MOCCPInternalProductId;

                return ResultObject(ResultDTO);
            }
            else
                return Error(ReturnCodes.LotteryPriceRedeemNotAtThisMachine);
        }
        #endregion
    }

    public class MachineQRInfo
    {
        public string AssetNo { get; set; }
        public string CurrentToken { get; set; }
        public DateTime GenerateNewTokenAt { get; set; }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using AutoMapper;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Providers
{
    public class NewsProvider : MaximaNewsModule.NewsProviderBase
    {
        private long CurrentNewsId = 0;

        #region AddNews
        public override BaseResponseDTO AddNews(NewsDetailsDTO NewsEntry)
        {
            BaseResponseDTO Response = base.AddNews(NewsEntry);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                NewsDetailsDTO NewsDetails = (NewsDetailsDTO)Response.Data;
                NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsDetails.Id);
                if (NewsDb.ActivityId != null)
                {
                    CouponActivity ActivityDb = DbContext.CouponActivities.Find(NewsDb.ActivityId);
                    ActivityDb.LimitStartTime = (NewsDb.LimitStartTime != null ? NewsDb.LimitStartTime.Value : NewsDb.CreateTime);
                    ActivityDb.LimitEndTime = (NewsDb.LimitEndTime != null ? NewsDb.LimitEndTime.Value : DateTime.MaxValue);
                    ActivityDb.Status = (NewsDetails.Category == GlobalConst.NewsCategory_Activities ? NewsEntry.ActivityStatus : ActivityStatus.Deleted);
                }

                if (NewsDb.LimitStartTime != null)
                    NewsDb.CreateTime = NewsDb.LimitStartTime.Value; // TODO later add a "NewsDate" instead of the "CreateTime"

                DbContext.SaveChanges();

                Response.Data = Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsDb);  // TODO move to maxima news module
                return Response;
            }
            else
                return Response;
        }
        #endregion
        #region ModifyNews
        public override BaseResponseDTO ModifyNews(NewsDetailsDTO NewsEntry)
        {
            BaseResponseDTO Response = base.ModifyNews(NewsEntry);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                NewsDetailsDTO NewsDetails = (NewsDetailsDTO)Response.Data;
                NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsDetails.Id);
                if (NewsDb.ActivityId != null)
                {
                    CouponActivity ActivityDb = DbContext.CouponActivities.Find(NewsDb.ActivityId);
                    ActivityDb.LimitStartTime = (NewsDb.LimitStartTime != null ? NewsDb.LimitStartTime.Value : NewsDb.CreateTime);
                    ActivityDb.LimitEndTime = (NewsDb.LimitEndTime != null ? NewsDb.LimitEndTime.Value : DateTime.MaxValue);
                    ActivityDb.Status = (NewsDetails.Category == GlobalConst.NewsCategory_Activities ? NewsEntry.ActivityStatus : ActivityStatus.Deleted);
                }

                if (NewsDb.LimitStartTime != null)
                    NewsDb.CreateTime = NewsDb.LimitStartTime.Value; // TODO later add a "NewsDate" instead of the "CreateTime" + NewsDate update in admin page

                if (NewsDb.ActivityId != null)
                {
                    LotteryGame ConnectedLotteryGameDb = DbContext.LotteryGames.Where(itm => itm.ConnectedActivityId == NewsDb.ActivityId).FirstOrDefault();
                    if (ConnectedLotteryGameDb != null)
                    {
                        ConnectedLotteryGameDb.StartTime = (NewsDb.LimitStartTime != null ? NewsDb.LimitStartTime.Value : ConnectedLotteryGameDb.StartTime);
                        ConnectedLotteryGameDb.EndTime = (NewsDb.LimitEndTime != null ? NewsDb.LimitEndTime.Value : ConnectedLotteryGameDb.EndTime);
                    }
                }

                Response.Data = Mapper.Map<NewsArticle, NewsDetailsDTO>(NewsDb); // TODO move to maxima news module
                DbContext.SaveChanges();

                return Response;
            }
            else
                return Response;
        }
        #endregion

        #region GetNewsActivity
        /// <summary>
        ///     For admin site:
        ///     returns the activity part of a news article.
        ///     If no activity exists, an empty template will be returned.
        /// </summary>
        /// <returns></returns>
        public BaseResponseDTO GetNewsActivity(long NewsId)
        {
            PermissionsProvider.RequestAdminRole();

            NewsArticle NewsDb = DbContext.NewsArticles.Find(NewsId);
            if ((NewsDb == null) && (NewsId > 0))
                return Error(ReturnCodes.RecordNotFound, "There is no news article with the id " + NewsId.ToString() + "!");

            CouponActivity ActivityDb = null;
            if ((NewsDb != null) && (NewsDb.ActivityId != null))
                ActivityDb = DbContext.CouponActivities.Find(NewsDb.ActivityId);

            CouponActivityAdminDTO ActivityDTO = null;
            if (ActivityDb != null)
            {
                ActivityDTO = Mapper.Map<CouponActivity, CouponActivityAdminDTO>(ActivityDb);
                ActivityDTO.StampsCount = DbContext.CouponStampDefinitions.Count(itm => itm.ActivityId == ActivityDb.Id);
                ActivityDTO.HasUserCoupons = (DbContext.UserCoupons.FirstOrDefault(itm => itm.ActivityId == ActivityDb.Id && itm.Status != Models.Enum.UserCouponStatus.Disabled) != null);
                ActivityDTO.ActivityId = ActivityDb.Id;
                ActivityDTO.AllVendingMachines = ActivityDb.AllVendingMachines;
                ActivityDTO.ActivityText = ActivityDb.ActivityText;
                ActivityDTO.ActivityListImageUrl = ActivityDb.ActivityListImageUrl;
                ActivityDTO.RedeemEndTime = ActivityDb.RedeemEndTime;
            }
            else
            {
                ActivityDTO = new CouponActivityAdminDTO();
                ActivityDTO.ActivityId = -1;
                ActivityDTO.CouponExpiryDays = 60;
                ActivityDTO.AllVendingMachines = false;
                ActivityDTO.StampsCount = 6;
                ActivityDTO.HasUserCoupons = false;
                ActivityDTO.Title = string.Empty;
                ActivityDTO.NoStampsActivity = false;
                ActivityDTO.SendingCouponsAllowed = true;
                ActivityDTO.ActivityListImageUrl = (NewsDb != null ? NewsDb.ListImageUrl : string.Empty);
                ActivityDTO.RedeemEndTime = (NewsDb.LimitEndTime != null ? NewsDb.LimitEndTime.Value : DateTime.UtcNow.AddDays(30));
            }

            if (ActivityDTO.StampsCount <= 2)
                ActivityDTO.StampsCount = 6;

            return ResultObject(ActivityDTO);
        }
        #endregion
        #region GetNewsActivityParticipatingProducts        
        /// <summary>
        ///     This will return a list of participating products for this activity.
        ///     It will provide a temporary "cache" for adding/removing rows, without already saving them.
        ///     
        ///     "LoadFromDatabase" defines if the data should be returned from the database or from the "session" cache
        ///     The first time "LoadFromDatabase"="true" should be used. The other times before save "false" should be used.
        /// </summary>
        /// <param name="ActivityId"></param>
        /// <param name="LoadFromDatabase"></param>
        /// <returns></returns>
        public BaseResponseDTO GetNewsActivityParticipatingProducts(long ActivityId)
        {
            PermissionsProvider.RequestAdminRole();

            string sSQL = @"
                			 select pp.[Id] as [Id],
			                        prod.[Id] [ProductId],
                                    prod.[MOCCPInternalId] [InternalId],
                                    prod.[MOCCPGoodsId] [MerchandiseId],
                                    prod.[Volume] [Volume],
				                    prod.[Name] [ProductName],
                                    prod.[Name] + ' (' + prod.[MOCCPGoodsId] + ')' [ProductDisplayName],
				                    prod.[ImageUrl] [ProductImageUrl],
				                    prod.[DefaultStampTypeId] [StampTypeId1],
                                    prod.[MOCCPOrganization] [Organization],
				                    stamp.[Name] [StampType1Name],
				                    stamp.[ImageUrl] [StampType1ImageUrl],
                                    pp.[Redeemable] [Redeemable],
                                    pp.[Status] [Status],
                                    (SELECT Replace(Replace((SELECT distinct CAST(',' + cast([VendingMachineId] as varchar(20)) AS VARCHAR(MAX)) [Id] FROM [SalesLists] sl where sl.[ProductId] = prod.[Id] and sl.[Status] = 1 FOR XML PATH ('')), '<Id>', ''), '</Id>', '')) [VendingMachineIdsAvailable]
                               from [dbo].[ActivityParticipatingProducts] pp
                         inner join [dbo].[VendingProducts] prod on prod.[Id] = pp.[ProductId]
                    left outer join [dbo].[StampTypes] stamp on stamp.[Id] = prod.[DefaultStampTypeId]
                              where pp.[ActivityId] = @ActivityId";

            List<ActivityParticipatingProductAdminDTO> ParticipatingProductsList = DbContext.Database.SqlQuery<ActivityParticipatingProductAdminDTO>(sSQL, new SqlParameter("@ActivityId", ActivityId)).ToList();
            return ResultObject(ParticipatingProductsList);
        }
        #endregion
        #region GetNewsActivityParticipatingMachines
        /// <summary>
        ///     This will return a list of participating vending machines for this activity.
        ///     It will provide a temporary "cache" for adding/removing rows, without already saving them.
        ///     
        ///     "LoadFromDatabase" defines if the data should be returned from the database or from the "session" cache
        ///     The first time "LoadFromDatabase"="true" should be used. The other times before save "false" should be used.
        /// </summary>
        /// <param name="ActivityId"></param>
        /// <param name="LoadFromDatabase"></param>
        /// <returns></returns>
        public BaseResponseDTO GetNewsActivityParticipatingMachines(long ActivityId)
        {
            PermissionsProvider.RequestAdminRole();
                                    
            return ResultObject(DbContext.ActivityParticipatingMachines.Include(itm => itm.VendingMachine)
                    .Where(itm => itm.ActivityId == ActivityId)
                    .Select(Mapper.Map<ActivityParticipatingMachine, ActivityParticipatingMachineAdminDTO>).ToList());
        }
        #endregion
        #region GetProductChoiceList
        public BaseResponseDTO GetProductChoiceList(string VendingMachineIdsFilter, bool ForAutoCoupons = false, bool AllMachines = false)
        {
            string sFilter = VendingMachineIdsFilter;
            if (string.IsNullOrWhiteSpace(sFilter))
                sFilter = "0";
            else
            {
                if (sFilter.EndsWith(","))
                    sFilter = sFilter.Substring(0, sFilter.Length - 1);
            }

            string sSQL = @"
                			 select prod.[Id] as [Id],
			                        prod.[Id] [ProductId],
                                    prod.[MOCCPInternalId] [InternalId],
                                    prod.[MOCCPGoodsId] [MerchandiseId],
				                    prod.[Name] [ProductName],
                                    prod.[Volume] [Volume],
                                    prod.[Name] + ' (' + prod.[MOCCPGoodsId] + ')' [ProductDisplayName],
				                    prod.[ImageUrl] [ProductImageUrl],
				                    prod.[DefaultStampTypeId] [StampTypeId1],
                                    prod.[MOCCPOrganization] [Organization],
				                    stamp.[Name] [StampType1Name],
				                    stamp.[ImageUrl] [StampType1ImageUrl],
				                    cast((case when prod.[Status] = 1 then 1 else 9 end) as int) [Status],
                                    (SELECT Replace(Replace((SELECT distinct CAST(',' + cast([VendingMachineId] as varchar(20)) AS VARCHAR(MAX)) [Id] FROM [SalesLists] sl where sl.[ProductId] = prod.[Id] and sl.[Status] = 1 FOR XML PATH ('')), '<Id>', ''), '</Id>', '')) [VendingMachineIdsAvailable]
			                    from [dbo].[VendingProducts] prod " +
		                (ForAutoCoupons ? "left outer join" : "inner join") + @" [dbo].[StampTypes] stamp on stamp.[Id] = prod.[DefaultStampTypeId]
		                        where " + (AllMachines ? "1 = 1" : "[MOCCPOrganization] in (select [MOCCPOrganization] from [dbo].[VendingMachines] vm where vm.[Id] in (" + sFilter + ")) ") +
			                    (AllMachines ? "" : " and Exists(select 1 from [dbo].[SalesLists] sl where sl.[MOCCPMerchandiseId] = prod.[MOCCPGoodsId] and sl.[VendingMachineId] in (" + sFilter + ")) ") + @"
			                    and prod.[Status] <> -1";

            List<ActivityParticipatingProductAdminDTO> ProductsList = DbContext.Database.SqlQuery<ActivityParticipatingProductAdminDTO>(sSQL).ToList();
            return ResultObject(ProductsList);
        }
        #endregion
        #region GetMachineChoiceList
        public BaseResponseDTO GetMachineChoiceList()
        {
            return ResultObject(DbContext.VendingMachines
                .Where(itm => itm.Status != Models.Enum.VendingMachineStatus.Removed)
                .Select(Mapper.Map<VendingMachine, ActivityParticipatingMachineAdminDTO>).ToList()
                );
        }
        #endregion
        #region SaveActivity
        public BaseResponseDTO SaveActivity(NewsActivitySaveDTO ActivityData)
        {
            PermissionsProvider.RequestAdminRole();

            if (ActivityData.Products == null)
                ActivityData.Products = new List<ActivityParticipatingProductAdminDTO>();
            if (ActivityData.Machines == null)
                ActivityData.Machines = new List<ActivityParticipatingMachineAdminDTO>();

            int iOldStampsCount = 0;
            NewsArticle NewsDb = DbContext.NewsArticles.Find(ActivityData.NewsId);
            if (NewsDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no news with the NewsId " + ActivityData.NewsId.ToString());

            CouponActivity ActivityDb = null;
            if (ActivityData.ActivityId <= 0)
            {
                ActivityDb = new CouponActivity();
                ActivityDb.CreateTime = DateTime.UtcNow;
            }
            else
            {
                ActivityDb = DbContext.CouponActivities.Find(ActivityData.ActivityId);
                if (ActivityDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no activity with the ActivityId " + ActivityData.ActivityId.ToString());

                iOldStampsCount = DbContext.CouponStampDefinitions.Count(itm => itm.ActivityId == ActivityData.ActivityId);
                if ((iOldStampsCount != ActivityData.StampsCount) && (DbContext.UserCoupons.FirstOrDefault(itm => itm.ActivityId == ActivityData.ActivityId && itm.Status != Models.Enum.UserCouponStatus.Disabled) != null))
                    Error(ReturnCodes.InvalidArguments, "The 'StampsCount' can not be changed, because this activity already has been started and coupons already have been created!");

                ActivityDb.ModifyTime = DateTime.UtcNow;
            }
            ActivityDb.LimitStartTime = (NewsDb.LimitStartTime != null ? NewsDb.LimitStartTime.Value : NewsDb.CreateTime);
            ActivityDb.LimitEndTime = (NewsDb.LimitEndTime != null ? NewsDb.LimitEndTime.Value : DateTime.MaxValue);
            ActivityDb.ActivityText = ActivityData.ActivityText;
            ActivityDb.CouponExpiryDays = ActivityData.CouponExpiryDays;
            ActivityDb.AllVendingMachines = false; //ActivityData.AllVendingMachines;
            ActivityDb.Title = (string.IsNullOrWhiteSpace(ActivityData.Title) ? NewsDb.Title : ActivityData.Title);
            ActivityDb.SendingCouponsAllowed = ActivityData.SendingCouponsAllowed;
            ActivityDb.NoStampsActivity = ActivityData.NoStampsActivity;
            ActivityDb.ShowOnPromotionMachinesList = ActivityData.ShowOnPromotionMachinesList;
            ActivityDb.RedeemEndTime = ActivityData.RedeemEndTime;
            ActivityDb.Status = ActivityData.Status;

            // add/remove stamp definitions
            if (ActivityData.StampsCount > iOldStampsCount)
            {
                for(int iNr = 1; iNr <= (ActivityData.StampsCount - iOldStampsCount); iNr++)
                {
                    CouponStampDefinition StampDefDb = new CouponStampDefinition();
                    StampDefDb.Activity = ActivityDb;
                    StampDefDb.StampTypeId = null;
                    DbContext.CouponStampDefinitions.Add(StampDefDb);
                }
            }
            else if (ActivityData.StampsCount < iOldStampsCount)
            {
                List<CouponStampDefinition> StampDefList = DbContext.CouponStampDefinitions.Where(itm => itm.ActivityId == ActivityData.ActivityId).ToList();

                for (int iNr = 1; iNr <= (iOldStampsCount - ActivityData.StampsCount); iNr++)
                {
                    CouponStampDefinition StampDefDb = StampDefList.FirstOrDefault();
                    if (StampDefDb != null)
                    {
                        DbContext.CouponStampDefinitions.Remove(StampDefDb);
                        StampDefList.Remove(StampDefDb);
                    }
                }
            }

            // add participating machines       
            List<long> ParticipatingVendingMachineIdList = new List<long>();

            foreach (ActivityParticipatingMachineAdminDTO MachineDTO in ActivityData.Machines)
            {
                ActivityParticipatingMachine PartMachineDb = (ActivityData.ActivityId > 0 ? DbContext.ActivityParticipatingMachines.FirstOrDefault(itm => itm.ActivityId == ActivityData.ActivityId && itm.VendingMachineId == MachineDTO.VendingMachineId) : null);
                ParticipatingVendingMachineIdList.Add(MachineDTO.VendingMachineId);

                if (PartMachineDb == null)
                {
                    PartMachineDb = new ActivityParticipatingMachine();
                    PartMachineDb.Activity = ActivityDb;
                    PartMachineDb.VendingMachineId = MachineDTO.VendingMachineId;
                    PartMachineDb.CreateTime = DateTime.UtcNow;
                    PartMachineDb.Status = MachineDTO.Status;
                    DbContext.ActivityParticipatingMachines.Add(PartMachineDb);
                }
                else
                {
                    if (PartMachineDb.Status != MachineDTO.Status)
                    {
                        PartMachineDb.Status = MachineDTO.Status;
                        PartMachineDb.ModifyTime = DateTime.UtcNow;
                    }
                }
            }

            // remove not used machines
            if (ActivityData.ActivityId > 0)
            {
                List<ActivityParticipatingMachine> ParticipatingMachineListDb = DbContext.ActivityParticipatingMachines.Where(itm => itm.ActivityId == ActivityData.ActivityId).ToList();
                foreach (ActivityParticipatingMachine MachineDb in ParticipatingMachineListDb)
                {
                    if (ActivityData.Machines.FirstOrDefault(itm => itm.VendingMachineId == MachineDb.VendingMachineId) == null)
                        DbContext.ActivityParticipatingMachines.Remove(MachineDb);
                }
            }

            // create string list of participating vending machines
            string CurrentVendingMachineIds = string.Empty;
            foreach (long VendingMachineId in ParticipatingVendingMachineIdList)
                CurrentVendingMachineIds += VendingMachineId.ToString() + ",";
            if (CurrentVendingMachineIds.EndsWith(","))
                CurrentVendingMachineIds = CurrentVendingMachineIds.Substring(0, CurrentVendingMachineIds.Length - 1);
            if (string.IsNullOrWhiteSpace(CurrentVendingMachineIds))
                CurrentVendingMachineIds = "0";

            // create SQL statement for checking if the product(s) can be added to this activity
            string sSQLCheckProductValidity = "SELECT Cast(IsNull(Count(pp.[Id]), 0) as int) " +
                                                          " FROM [dbo].[CouponActivities] act " +
                                                    " INNER JOIN [dbo].[ActivityParticipatingProducts] pp on pp.[ActivityId] = act.[Id] " +
                                                    " INNER JOIN [dbo].[NewsArticles] news ON news.[ActivityId] = act.[Id] " +
                                                         " WHERE act.[Id] <> @CurrentActivityId " +
                                                           " AND Exists(SELECT 1 FROM [dbo].[ActivityParticipatingMachines] pm " +
                                                                              " WHERE pm.[ActivityId] = act.[Id] " +
                                                                                " AND pm.[VendingMachineId] IN (" + CurrentVendingMachineIds + "))" +
                                                           " AND act.[Status] = 1 " +
                                                           " AND news.[Status] = 1 " +
                                                           " AND getutcdate() >= news.[LimitStartTime] AND getutcdate() <= news.[LimitEndTime] " +
                                                           " AND pp.[ProductId] = @ProductId" +
                                                           " AND act.[NoStampsActivity] = 0";

            // add participating products
            foreach (ActivityParticipatingProductAdminDTO ProductDTO in ActivityData.Products)
            {
                ActivityParticipatingProduct PartProductDb = (ActivityData.ActivityId > 0 ? DbContext.ActivityParticipatingProducts.FirstOrDefault(itm => itm.ActivityId == ActivityData.ActivityId && itm.ProductId == ProductDTO.ProductId) : null);

                if (PartProductDb == null)
                {
                    List<int> QueryResult = DbContext.Database.SqlQuery<int>(sSQLCheckProductValidity, new SqlParameter("@CurrentActivityId", ActivityDb.Id), new SqlParameter("@ProductId", ProductDTO.ProductId)).ToList();
                    if ((!ActivityData.NoStampsActivity) && ((QueryResult.Count > 0) && (QueryResult[0] > 0)))
                        return Error(ReturnCodes.ProductNotValidForActivity, "The product '" + ProductDTO.ProductName + "' is already part of another activity (which also has one of these machines added) for this activity and can not be added anymore to this activity!");

                    PartProductDb = new ActivityParticipatingProduct();
                    PartProductDb.Activity = ActivityDb;
                    PartProductDb.ProductId = ProductDTO.ProductId;
                    PartProductDb.StampTypeId1 = ProductDTO.StampTypeId1.Value;
                    PartProductDb.CreateTime = DateTime.UtcNow;
                    PartProductDb.Redeemable = ProductDTO.Redeemable;
                    PartProductDb.Status = ProductDTO.Status;
                    DbContext.ActivityParticipatingProducts.Add(PartProductDb);
                }
                else
                {
                    List<int> QueryResult = DbContext.Database.SqlQuery<int>(sSQLCheckProductValidity, new SqlParameter("@CurrentActivityId", ActivityDb.Id), new SqlParameter("@ProductId", ProductDTO.ProductId)).ToList();
                    if ((!ActivityData.NoStampsActivity) && ((QueryResult.Count > 0) && (QueryResult[0] > 0)))
                        return Error(ReturnCodes.ProductNotValidForActivity, "The product '" + ProductDTO.ProductName + "' is already part of another activity (which also has one of these machines added) for this activity and therefore can not be used anymore in this activity!");

                    if ((PartProductDb.StampTypeId1 != ProductDTO.StampTypeId1) ||
                        (PartProductDb.Redeemable != ProductDTO.Redeemable) ||
                        (PartProductDb.Status != ProductDTO.Status))
                    {
                        PartProductDb.StampTypeId1 = ProductDTO.StampTypeId1.Value;
                        PartProductDb.ModifyTime = DateTime.UtcNow;
                        PartProductDb.Redeemable = ProductDTO.Redeemable;
                        PartProductDb.Status = ProductDTO.Status;
                    }
                }
            }

            // remove not used products
            if (ActivityData.ActivityId > 0)
            {
                List<ActivityParticipatingProduct> ParticipatingProductsListDb = DbContext.ActivityParticipatingProducts.Where(itm => itm.ActivityId == ActivityData.ActivityId).ToList();
                foreach (ActivityParticipatingProduct ProductDb in ParticipatingProductsListDb)
                {
                    if (ActivityData.Products.FirstOrDefault(itm => itm.ProductId == ProductDb.ProductId) == null)
                        DbContext.ActivityParticipatingProducts.Remove(ProductDb);
                }
            }

            NewsDb.Activity = ActivityDb;
            DbContext.SaveChanges();

            return Ok();
        }
        #endregion

        #region GetNews
        public override BaseResponseDTO GetNews(SearchNewsParamsDTO Params)
        {
            return GetNews(Params, false);
        }

        public BaseResponseDTO GetNews(SearchNewsParamsDTO Params, bool bForAdmin)
        {
            BaseResponseDTO Response = base.GetNews(Params);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                if (!bForAdmin)
                {
                    List<GetNewsListEntryDTO> List = ((IEnumerable<GetNewsListEntryDTO>)Response.Data).ToList();
                    foreach (GetNewsListEntryDTO EntryDTO in List)
                        EntryDTO.DetailImageUrl = null; // in the API don't return the detail image url in the news list
                    Response.Data = List;
                }
            }

            return Response;
        }
        #endregion

        #region GetNewsDetails
        public override BaseResponseDTO GetNewsDetails(GetNewsDetailsParamsDTO Params)
        {
            BaseResponseDTO Response = base.GetNewsDetails(Params);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                NewsDetailsDTO DetailsDTO = (NewsDetailsDTO)Response.Data;
                DetailsDTO.ListImageUrl = null; // in the API don't return the list image url for the detail news article
            }

            return Response;
        }

        public override BaseResponseDTO GetNewsDetails(long NewsId, long? UID)
        {
            BaseResponseDTO Response = base.GetNewsDetails(NewsId, UID);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                NewsDetailsDTO DetailsDTO = (NewsDetailsDTO)Response.Data;
                DetailsDTO.ListImageUrl = (!string.IsNullOrWhiteSpace(DetailsDTO.ListImageUrl) ? DetailsDTO.ListImageUrl : DetailsDTO.ImageUrl);

                if (DetailsDTO.ActivityId != null)
                {
                    CouponActivity ActivityDb = DbContext.CouponActivities.Where(itm => itm.Id == DetailsDTO.ActivityId.Value).FirstOrDefault();
                    if (ActivityDb != null)
                        DetailsDTO.ActivityStatus = ActivityDb.Status;
                }

                Response.Data = DetailsDTO;
            }

            return Response;
        }
        #endregion

        #region SaveNewsListImage (Admin)
        /// <summary>
        ///     Changes or adds the list image of the given news
        /// </summary>
        /// <returns></returns>
        public virtual BaseResponseDTO SaveNewsListImage(long NewsId, string ContentType, string FileExtension, byte[] Data)
        {
            string ImageFilename = "max_news_list_image_" + NewsId.ToString("0000000");
            string ImageUrl = string.Empty;

            PermissionsProvider.RequestAdminRole();

            NewsArticle NewsDb = DbContext.NewsArticles.FirstOrDefault(itm => itm.Id == NewsId);

            if (NewsDb != null)
            {
                NewsDb.ListImageUrl = Kooco.Framework.Shared.AliyunUtils.StoreSizedImage(ImageFilename, Data);

                if (!string.IsNullOrWhiteSpace(NewsDb.ListImageUrl))
                {
                    DbContext.SaveChanges();
                    return ResultObject(NewsDb.ListImageUrl);
                }
                else
                    return Error(ReturnCodes.InternalServerError, "Error while uploading the list image to AliYun");
            }
            else
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId.ToString() + "!");
        }
        #endregion
        #region SaveActivityListImage (Admin)
        /// <summary>
        ///     Changes or adds the activity list image of the given activity bound to a news article
        /// </summary>
        /// <returns></returns>
        public virtual BaseResponseDTO SaveActivityListImage(long NewsId, string ContentType, string FileExtension, byte[] Data)
        {
            string ImageFilename = string.Empty;
            string ImageUrl = string.Empty;

            PermissionsProvider.RequestAdminRole();

            long? ActivityId = DbContext.NewsArticles.Where(itm => itm.Id == NewsId).Select(itm => itm.ActivityId).FirstOrDefault();
            if ((ActivityId == null) || (ActivityId <= 0))
                return Error(ReturnCodes.RecordNotFound, "There is no news with the id " + NewsId.ToString() + " in the db OR this news does not have an activity assigned yet!");

            CouponActivity ActivityDb = DbContext.CouponActivities.FirstOrDefault(itm => itm.Id == ActivityId);
            ImageFilename = "max_activity_list_image_" + ActivityId.Value.ToString("0000000");

            if (ActivityDb != null)
            {
                ActivityDb.ActivityListImageUrl = Kooco.Framework.Shared.AliyunUtils.StoreSizedImage(ImageFilename, Data);

                if (!string.IsNullOrWhiteSpace(ActivityDb.ActivityListImageUrl))
                {
                    DbContext.SaveChanges();
                    return ResultObject(ActivityDb.ActivityListImageUrl);
                }
                else
                    return Error(ReturnCodes.InternalServerError, "Error while uploading the activity list image to AliYun");
            }
            else
                return Error(ReturnCodes.RecordNotFound, "There is no activity with the id " + ActivityId.ToString() + "!");
        }
        #endregion

        #region GetActivityList (Admin)
        public BaseResponseDTO GetActivityList()
        {
            DateTime CurrentDate = DateTime.Now;

            List<CouponActivityAdminDTO> ActivitiesList = DbContext.CouponActivities.Where(itm => itm.Status != ActivityStatus.Deleted &&                                                                                           
                                                                                          (itm.LimitEndTime == null || itm.LimitEndTime > CurrentDate))
                                                                                   .Select(Mapper.Map<CouponActivity, CouponActivityAdminDTO>).ToList();
            List<CouponActivityAdminDTO> ReturnList = new List<CouponActivityAdminDTO>();

            foreach(CouponActivityAdminDTO ActivityDb in ActivitiesList)
            {
                if (DbContext.NewsArticles.Where(itm => itm.ActivityId == ActivityDb.ActivityId && itm.Status != NewsStatus.Deleted).Count() > 0)
                    ReturnList.Add(ActivityDb);
            }


            return ResultObject(ReturnList);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using VmaBase.Shared;

namespace VmaBase.Providers.Server
{
    /// <summary>
    ///     This request and response handler makes it possible to "catch" the original requests
    ///     to log them into the API requests log, to be able to see the original request sent.
    ///     This handler have to be added to the WebApiConfig.
    ///     
    ///     The handler will save the original response in a property which is used then by the Utils.LogApiRequest() method
    /// </summary>
    public class ApiRequestAndResponseHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            string requestBody = await request.Content.ReadAsStringAsync();
            request.Properties.Add(new KeyValuePair<string, object>(GlobalConst.RequestProperty_OriginalRequest, requestBody));

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            return result;
        }
    }
}

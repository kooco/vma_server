﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;

using AutoMapper;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models;

namespace VmaBase.Providers
{
    public class QRProvider : BaseProvider
    {
        #region GetUserQRCode
        public BaseResponseDTO GetUserQRCode(BaseAPIAuthParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            return ResultObject("USER_" + Params.AuthenticatedUser.Account);
        }
        #endregion
        #region ExecuteQRAction
        public BaseResponseDTO ExecuteQRAction(QRActionParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            ReturnCodes retCode = ReturnCodes.Success;

            QRActionInfoDTO ActionInfo = new QRActionInfoDTO();

            string sToken = Params.QRToken.Trim();
            if (sToken.ToLower().StartsWith("user_"))
            {
                string sAccount = sToken.Substring(5);                
                UserData FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).First(itm => itm.UserBase.Account == sAccount);

                ActionInfo.ActionType = Models.Enum.QRActionType.AddFriend;
                ActionInfo.UserAccount = FriendUserDb.UserBase.Account;
                ActionInfo.UserName = FriendUserDb.UserBase.Name;
                ActionInfo.UserNickname = FriendUserDb.UserBase.Nickname;
                ActionInfo.UserImageUrl = FriendUserDb.ImageUrl;
            }
            else if (sToken.ToLower().StartsWith("aq"))
            {
                // Watersports Event
                return Error(ReturnCodes.InvalidQRcode, "The watersports event is not active anymore!");

                /*
                SingleUseQRToken TokenDb = DbContext.SingleUseQRTokens.Where(itm => itm.Type == Models.Enum.SingleUseQRType.WatersportsLotteryCoin && itm.Token == Params.QRToken && itm.Status != Models.Enum.SingleUseQRStatus.Deleted).FirstOrDefault();
                if (TokenDb == null)
                    return Error(ReturnCodes.InvalidQRcode, "This QR code does not exist!");

                if (TokenDb.Status == Models.Enum.SingleUseQRStatus.Used)
                    return Error(ReturnCodes.QRAlreadyUsed);

                TokenDb.Status = Models.Enum.SingleUseQRStatus.Used;
                TokenDb.UseTime = DateTime.UtcNow;

                LotteryCoinLog CoinLog = new LotteryCoinLog();
                CoinLog.Coins = 1;
                CoinLog.TransactionTime = DateTime.UtcNow;
                CoinLog.Type = Models.Enum.LotteryCoinLogType.SingleUseQRReceive;
                CoinLog.UID = Params.AuthenticatedUser.UID;
                CoinLog.QRTokenUsed = TokenDb.Token;
                DbContext.LotteryCoinLogs.Add(CoinLog);

                UserLotteryCoin CoinDb = new UserLotteryCoin();
                CoinDb.Amount = 1;
                CoinDb.DateAquired = DateTime.UtcNow;
                CoinDb.ExpiryDate = DbContext.CouponActivities.Where(itm => itm.Id == 100).Select(itm => itm.LimitEndTime).FirstOrDefault();
                CoinDb.LotteryGameId = GlobalConst.LotteryGameId_Watersports;
                CoinDb.OriginalAmount = 1;
                CoinDb.Status = Models.Enum.LotteryCoinStatus.Available;
                CoinDb.Type = Models.Enum.LotteryCoinType.Earned;
                CoinDb.UID = Params.AuthenticatedUser.UID;
                DbContext.UserLotteryCoins.Add(CoinDb);

                DbContext.SaveChanges();

                ActionInfo.ActionType = Models.Enum.QRActionType.WatersportsQR;
                ActionInfo.lotteryCoinsAdded = 1;
                ActionInfo.lotteryCoinsTotal = DbContext.UserLotteryCoins.Where(itm => itm.UID == Params.AuthenticatedUser.UID && itm.Status == Models.Enum.LotteryCoinStatus.Available && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Sum(itm => itm.Amount);

                PushNotificationProvider.Current.SendTextNotification(Params.AuthenticatedUser.UID, "水動樂活動通知", "恭喜你獲得一次參加水動樂活動的機會", PushAction.Create(Models.Enum.PushActionCode.WatersportsCoinReceived, string.Empty));
                */
            }
            else
            {
                QRActionInfo QRInfo = Utils.GetQRContent(sToken);
                if (QRInfo.IsValid)
                {
                    switch(QRInfo.ActionType)
                    {
                        case Models.Enum.QRActionType.VendingMachineQR:
                            long MachineId = 0;
                            if (!long.TryParse(QRInfo.Id, out MachineId))
                                return Error(ReturnCodes.InvalidQRcode, "The given QR token is invalid! It's recognized as a vending machine QR but the vending machine id included is not a valid number!");

                            VendingMachine MachineDb = DbContext.VendingMachines.FirstOrDefault(itm => itm.Id == MachineId);
                            if (MachineDb == null)
                                return Error(ReturnCodes.RecordNotFound, "The vending machine id " + QRInfo.Id + " stored in the QR code can not be found!");
                            if ((MachineDb.QRToken != sToken) && (MachineDb.QRTokenOld != sToken))
                                return Error(ReturnCodes.InvalidQRcode, "The QR code is invalid or expired!");

                            ActionInfo.ActionType = Models.Enum.QRActionType.VendingMachineQR;
                            ActionInfo.VendingMachineStatus = MachineDb.Status;
                            ActionInfo.VendingMachineId = MachineDb.Id;

                            if ((Params.ActivityId != null) && (Params.ActivityId.Value > 0))
                            {
                                CouponActivity ActivityDb = DbContext.CouponActivities.Find(Params.ActivityId.Value);
                                if (ActivityDb.AllVendingMachines)
                                    ActionInfo.MachineMatchesActivity = true;
                                else
                                    ActionInfo.MachineMatchesActivity = DbContext.ActivityParticipatingMachines.FirstOrDefault(itm => itm.ActivityId == Params.ActivityId.Value && itm.VendingMachineId == MachineDb.Id && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active) != null;
                            }

                            // check if this machine is part of an activity and the user has a coupon for it
                            string sSQL = "select (case when Exists(SELECT 1 FROM [dbo].[ActivityParticipatingMachines] pm " +
                                                                     " inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] " +
                                                                     " inner join [dbo].[NewsArticles] news on news.[ActivityId] = act.[Id] " +
                                                                     " inner join [dbo].[NewsCategories] cat ON cat.[Id] = news.[CategoryId] " +
                                                                     " inner join [dbo].[UserCoupons] coupon on coupon.[ActivityId] = act.[Id] and coupon.[UID] = @UserId and coupon.[Status] = 2 " +
                                                                          " where pm.[VendingMachineId] = @VendingMachineId " +
                                                                          "   and news.[Status] = 1 " +
                                                                          "   and act.[Status] = 1 " +
                                                                          "   and cat.[Code] = 'ACT'" +
                                                                          "   and getutcdate() >= act.[LimitStartTime] " +
                                                                          "   and getutcdate() <= act.[LimitEndTime])" +
                                                    " then cast(1 as bit) else cast(0 as bit) end)";
                            List<bool> HasActivityCoupon = DbContext.Database.SqlQuery<bool>(sSQL, new SqlParameter("@UserId", Params.AuthenticatedUser.UID),
                                                                                                   new SqlParameter("@VendingMachineId", MachineDb.Id)).ToList();
                            ActionInfo.ActivityCouponAvailable = (HasActivityCoupon.Count > 0 && (HasActivityCoupon[0]));
                            break;

                        case Models.Enum.QRActionType.ProductStampsQR:
                            BaseResponseDTO AddStampsResponse = DbContext.SP_AcquireOrderStamps(Params.AuthenticatedUser, Params.QRToken, Params.ActivityId);
                            if (AddStampsResponse.ReturnCode != ReturnCodes.Success)
                                return AddStampsResponse;

                            AddStampsApiResultDTO ResultDTO = (AddStampsApiResultDTO)AddStampsResponse.Data;

                            ActionInfo.ActionType = Models.Enum.QRActionType.ProductStampsQR;
                            ActionInfo.Activity = ResultDTO.Activity;
                            ActionInfo.pointsAmountAdded = ResultDTO.PointsAdded;
                            ActionInfo.stampsAmountAdded = ResultDTO.StampsAdded;
                            ActionInfo.lotteryCoinsAdded = ResultDTO.WatersportsCoinsAdded;

                            if (ResultDTO.WatersportsCoinsAdded > 0)
                                PushNotificationProvider.Current.SendTextNotification(Params.AuthenticatedUser.UID, "水動樂活動通知", "恭喜你獲得一次參加水動樂活動的機會", PushAction.Create(Models.Enum.PushActionCode.WatersportsCoinReceived, string.Empty));
                            break;

                        default:
                            return Error(ReturnCodes.InvalidQRcode, "The QR action type " + QRInfo.ActionType.ToString() + " is currently not implemented or invalid!");
                    }
                }
                else
                    return Error(ReturnCodes.InvalidQRcode, "The given QR token is invalid!");
            }

            UserData UserDb = DbContext.UserDatas.Find(Params.AuthenticatedUser.UID);
            ActionInfo.pointsTotal = (long)UserDb.PointsTotal;

            Response = ResultObject(ActionInfo);
            Response.ReturnCode = retCode;
            return Response;
        }
        #endregion
    }
}

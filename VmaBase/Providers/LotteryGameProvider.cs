﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using AutoMapper;

using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataStorage.Resultsets;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models;
using VmaBase.Models.DataStorage;

namespace VmaBase.Providers
{
    public class LotteryGameProvider : BaseProvider
    {
        #region GetLotteryGame
        public BaseResponseDTO GetLotteryGame(GetLotteryGameParamsDTO Params)
        {
            BaseResponseDTO Response = CheckLotteryParams(Params, true);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            LotteryGame GameDb = (LotteryGame) Response.Data;
            UserLotteryCoinsInfo CoinsInfo = new UserLotteryCoinsInfo();
            CoinsInfo.DailyCoins = DbContext.UserLotteryCoins.Where(itm => itm.ExpiryDate >= DateTime.UtcNow && itm.Status == LotteryCoinStatus.Available && itm.UID == Params.AuthenticatedUser.UID && itm.LotteryGameId == GameDb.Id && itm.Type == LotteryCoinType.Daily).Select(itm => itm.Amount).DefaultIfEmpty(0).Sum(itm => itm);
            CoinsInfo.EarnedCoins = DbContext.UserLotteryCoins.Where(itm => itm.ExpiryDate >= DateTime.UtcNow && itm.Status == LotteryCoinStatus.Available && itm.UID == Params.AuthenticatedUser.UID && itm.LotteryGameId == GameDb.Id && itm.Type == LotteryCoinType.Earned).Select(itm => itm.Amount).DefaultIfEmpty(0).Sum(itm => itm);

            LotteryGameInfoDTO GameInfoDTO = new LotteryGameInfoDTO();
            GameInfoDTO.Title = GameDb.Title;
            GameInfoDTO.LotteryGameId = GameDb.Id;
            GameInfoDTO.LotteryEndDate = GameDb.EndTime;
            GameInfoDTO.Coins = CoinsInfo.DailyCoins + CoinsInfo.EarnedCoins;

            return ResultObject(GameInfoDTO);
        }
        #endregion
        #region GetPriceList
        /// <summary>
        ///     Returns a list of prices for the given lottery game
        /// </summary>
        public BaseResponseDTO GetPriceList(BaseLotteryParamsDTO Params)
        {
            BaseResponseDTO Response = CheckLotteryParams(Params, false);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            long LotteryGameId = (long)Response.Data;
            List<LotteryPriceItemDTO> PriceList = DbContext.LotteryPrices.Where(itm => itm.LotteryGameId == LotteryGameId && itm.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active).Select(Mapper.Map<LotteryPrice, LotteryPriceItemDTO>).ToList();
            return ResultObject(PriceList);
        }
        #endregion
        #region DrawGame
        public BaseResponseDTO DrawGame(BaseLotteryParamsDTO Params)
        {
            BaseResponseDTO Response = CheckLotteryParams(Params, false);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            long LotteryGameId = (long)Response.Data;
            Response = DbContext.SP_LotteryDrawGame(Params.AuthenticatedUser.UID, LotteryGameId);

            // because of the transactions the stored procedure can not return the updated coins
            // so we have to query it here again
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                ((LotteryDrawGameResultDTO)Response.Data).CoinsLeft =
                    DbContext.UserLotteryCoins.Where(itm => itm.LotteryGameId == LotteryGameId &&
                                                            itm.Status == LotteryCoinStatus.Available &&
                                                            itm.UID == Params.AuthenticatedUser.UID &&
                                                            itm.ExpiryDate >= DateTime.UtcNow)
                                              .Select(itm => itm.Amount).DefaultIfEmpty(0).Sum();
            }

            return Response; // now return the response
        }
        #endregion
        #region GetCouponsWon
        public BaseResponseDTO GetCouponsWon(GetLotteryCouponListParamsDTO Params)
        {
            BaseResponseDTO Response = CheckLotteryParams(Params, false);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            long LotteryGameId = (long)Response.Data;
            List<LotteryPriceCouponDTO> ListCoupons = DbContext.SP_GetLotteryCouponList(LotteryGameId, Params.AuthenticatedUser.UID, (Params.OnlyUsable == null ? true : Params.OnlyUsable.Value));
            return ResultObject(ListCoupons, RecordsCount: ListCoupons.Count);
        }
        #endregion
        #region RedeemCoupon
        public BaseResponseDTO RedeemCoupon(RedeemLotteryCouponParamsDTO Params)
        {
            BaseResponseDTO Response = CheckLotteryParams(Params, false);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            VendingMachine MachineDb = null;
            if (Params.VendingMachineId > 0)
            {
                MachineDb = DbContext.VendingMachines.Find(Params.VendingMachineId);
                if (MachineDb == null)
                    return Error(ReturnCodes.RecordNotFound, "Can not find a vending machine with the id " + Params.VendingMachineId.ToString() + " in the database!");
            }

            if ((Params.LotteryPriceId <= 0) && (Params.ActivityId == null))
                return Error(ReturnCodes.MissingArgument, "At least one of the parameters 'PriceId' or 'ActivityId' must be given!");

            long LotteryGameId = (long)Response.Data;
            Response = DbContext.SP_LotteryRedeemMachinePrice(Params.AuthenticatedUser.UID, Params.LotteryPriceId, Params.VendingMachineId, Params.ProductId, Params.ActivityId, Params.Token);
            if (Response.ReturnCode == ReturnCodes.Success)
            {
                if ((MachineDb != null) && (!string.IsNullOrWhiteSpace(MachineDb.RegistrationId)))
                {
                    APIVendingProvider VendingProvider = new APIVendingProvider();
                    VendingProvider.SendBuyBuyJPush(MachineDb.Id, MachineDb.RegistrationId, GlobalConst.BuyBuyJPushType_CouponRedeem, Params.LotteryPriceId.ToString());
                }
            }

            return Response;
        }
        #endregion

        #region CheckLotteryGameState
        private ReturnCodes CheckLotteryGameState(LotteryGameInfo GameDb)
        {
            if ((GameDb.Status == LotteryGameStatus.Inactive) || (GameDb.Status == LotteryGameStatus.Stopped))
                return ReturnCodes.LotteryGameInactive;
            if (GameDb.Status == LotteryGameStatus.Finished)
                return ReturnCodes.LotteryGameAlreadyFinished;
            if (GameDb.TestMode == LotteryTestMode.Off)
            {
                if (DateTime.UtcNow < GameDb.StartTime)
                    return ReturnCodes.LotteryGameInactive;
            }
            else
            {
                if ((GameDb.TestStartTime != null) && (DateTime.UtcNow < GameDb.TestStartTime))
                    return ReturnCodes.LotteryGameInactive;
                else if ((GameDb.TestStartTime == null) && (DateTime.UtcNow < GameDb.StartTime))
                    return ReturnCodes.LotteryGameInactive;
            }

            if (DateTime.UtcNow > GameDb.EndTime)
                return ReturnCodes.LotteryGameAlreadyFinished;

            return ReturnCodes.Success;
        }
        #endregion
        #region CheckLotteryParams
        private BaseResponseDTO CheckLotteryParams(BaseLotteryParamsDTO Params, bool RetrieveLotteryGameRecord)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            LotteryGame GameDb = null;
            LotteryGameInfo GameInfo = null;
            ReturnCodes Code = ReturnCodes.Success;

            if (((Params.LotteryGameId != null) && (Params.LotteryGameId.Value > 0)) || ((Params.ActivityId != null) && (Params.ActivityId.Value > 0)))
            {
                // get the lottery game given in the parameters
                if (RetrieveLotteryGameRecord)
                {
                    // retrieve the whole game record, because it's needed
                    GameDb = DbContext.LotteryGames.FirstOrDefault(itm => (itm.Id == Params.LotteryGameId || Params.LotteryGameId == null || Params.LotteryGameId == 0) &&
                                                                          (itm.ConnectedActivityId == Params.ActivityId || Params.ActivityId == null || Params.ActivityId == 0));
                    if (GameDb != null)
                    {
                        GameInfo = new LotteryGameInfo();
                        GameInfo.StartTime = GameDb.StartTime;
                        GameInfo.EndTime = GameDb.EndTime;
                        GameInfo.TestStartTime = GameDb.TestStartTime;
                        GameInfo.TestMode = GameDb.TestMode;
                        GameInfo.Status = GameDb.Status;
                    }
                }
                else
                {
                    // only retrieve some fields, if not the whole game record is needed (the operation will be more quick then)
                    GameInfo = DbContext.LotteryGames.Where(itm => (itm.Id == Params.LotteryGameId || Params.LotteryGameId == null || Params.LotteryGameId == 0) &&
                                                                   (itm.ConnectedActivityId == Params.ActivityId || Params.ActivityId == null || Params.ActivityId == 0))
                                                     .Select(itm => new LotteryGameInfo() { Id = itm.Id, StartTime = itm.StartTime, EndTime = itm.EndTime, TestStartTime = itm.TestStartTime, TestMode = itm.TestMode, Status = itm.Status }).FirstOrDefault();
                }

                if (GameInfo == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no lottery game with the " + (Params.LotteryGameId != null && Params.LotteryGameId.Value > 0 ? " id " + Params.LotteryGameId.Value.ToString() : " activity id " + Params.ActivityId.Value.ToString() + "!"));

                if (GameInfo.Id != 2)
                {
                    Code = CheckLotteryGameState(GameInfo);
                    if (Code != ReturnCodes.Success)
                        return new BaseResponseDTO(Code);
                }
            }
            else
            {
                if (RetrieveLotteryGameRecord)
                {
                    // get the (hopefully) only currently active lottery game
                    List<LotteryGame> GamesFoundDb = DbContext.LotteryGames.Where(itm => (itm.Status == LotteryGameStatus.Ready || itm.Status == LotteryGameStatus.Started) &&
                                                                          ((DateTime.UtcNow >= itm.StartTime && DateTime.UtcNow <= itm.EndTime) ||
                                                                           (itm.TestMode != LotteryTestMode.Off && DateTime.UtcNow >= itm.TestStartTime && DateTime.UtcNow <= itm.EndTime))).ToList();
                    if (GamesFoundDb.Count > 1)
                        return Error(ReturnCodes.MoreActiveLotteryGames);
                    if (GamesFoundDb.Count <= 0)
                        return Error(ReturnCodes.NoActiveLotteryGame);
                    GameDb = GamesFoundDb[0];
                }
                else
                {
                    // using only a few columns (LotteryGameInfo) should be more quick, if we don't need the whole LotteryGame record
                    List<LotteryGameInfo> GamesFoundList = DbContext.LotteryGames.Where(itm => (itm.Status == LotteryGameStatus.Ready || itm.Status == LotteryGameStatus.Started) &&
                                                                          ((DateTime.UtcNow >= itm.StartTime && DateTime.UtcNow <= itm.EndTime) ||
                                                                           (itm.TestMode != LotteryTestMode.Off && DateTime.UtcNow >= itm.TestStartTime && DateTime.UtcNow <= itm.EndTime)))
                                                                           .Select(itm => new LotteryGameInfo() { Id = itm.Id, StartTime = itm.StartTime, EndTime = itm.EndTime, TestStartTime = itm.TestStartTime, TestMode = itm.TestMode, Status = itm.Status }).ToList();
                    if (GamesFoundList.Count > 1)
                        return Error(ReturnCodes.MoreActiveLotteryGames);
                    if (GamesFoundList.Count <= 0)
                        return Error(ReturnCodes.NoActiveLotteryGame);
                    GameInfo = GamesFoundList[0];
                }
            }

            if (RetrieveLotteryGameRecord)
                return ResultObject(GameDb);
            else
                return ResultObject(GameInfo.Id);
        }
        #endregion

        #region TestAddLotteryCoins
        public BaseResponseDTO TestAddLotteryCoins(TestAddLotteryCoinsParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            if (Params.LotteryGameId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'lotteryGameId' is missing!");

            LotteryGame GameDb = DbContext.LotteryGames.Find(Params.LotteryGameId);
            if (GameDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no lottery game with the id " + Params.LotteryGameId.ToString() + " in the database!");

            UserData UserDb = DbContext.UserDatas.Find(Params.UID);
            if (UserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the UID " + Params.UID.ToString() + "!");

            LotteryCoinLog CoinLogDb = new LotteryCoinLog();
            CoinLogDb.Coins = Params.Coins;
            CoinLogDb.UID = Params.UID;
            CoinLogDb.TransactionTime = DateTime.UtcNow;
            CoinLogDb.Type = LotteryCoinLogType.TestCoinsAdded;
            DbContext.LotteryCoinLogs.Add(CoinLogDb);

            UserLotteryCoin CoinDb = new UserLotteryCoin();
            CoinDb.UID = Params.UID;
            CoinDb.LotteryGameId = Params.LotteryGameId;
            CoinDb.OriginalAmount = Params.Coins;
            CoinDb.Amount = Params.Coins;
            CoinDb.Type = LotteryCoinType.Earned;
            CoinDb.DateAquired = DateTime.UtcNow;
            CoinDb.ExpiryDate = GameDb.CouponsExpiryTime;
            DbContext.UserLotteryCoins.Add(CoinDb);

            DbContext.SaveChanges();

            return Ok();
        }
        #endregion
        #region TestDeleteLotteryCoupons
        public BaseResponseDTO TestDeleteLotteryCoupons(TestResetLotteryCouponsParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            LotteryGame GameDb = null;
            if (Params.LotteryGameId <= 0)
                GameDb = DbContext.LotteryGames.FirstOrDefault(itm => (itm.Status == LotteryGameStatus.Ready || itm.Status == LotteryGameStatus.Started));
            else
                GameDb = DbContext.LotteryGames.Find(Params.LotteryGameId);

            if (GameDb == null)
                return Error(ReturnCodes.RecordNotFound, "Lottery game not available!");

            UserData UserDb = DbContext.UserDatas.Find(Params.UID);
            if (UserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the UID " + Params.UID.ToString() + "!");

            DbContext.Database.ExecuteSqlCommand("update [dbo].[LotteryPriceCoupons] set [Status] = @Status where [UID] = @UID and [LotteryGameId] = @LotteryGameId", new SqlParameter("@Status", LotteryPriceCouponStatus.Cancelled), new SqlParameter("@UID", Params.UID), new SqlParameter("@LotteryGameId", GameDb.Id));

            return Ok();
        }
        #endregion
        #region TestResetLotteryCoupons
        public BaseResponseDTO TestResetLotteryCoupons(TestResetLotteryCouponsParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            UserData UserDb = DbContext.UserDatas.Find(Params.UID);
            if (UserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the UID " + Params.UID.ToString() + "!");

            LotteryGame GameDb = null;
            if (Params.LotteryGameId <= 0)
                GameDb = DbContext.LotteryGames.FirstOrDefault(itm => (itm.Status == LotteryGameStatus.Ready || itm.Status == LotteryGameStatus.Started));
            else
                GameDb = DbContext.LotteryGames.Find(Params.LotteryGameId);

            if (GameDb == null)
                return Error(ReturnCodes.RecordNotFound, "Lottery game not available!");

            DbContext.Database.ExecuteSqlCommand("update [dbo].[LotteryPriceCoupons] set [Status] = @Status, [TimeRedeemed] = null where [UID] = @UID and @LotteryGameId = @LotteryGameId", new SqlParameter("@Status", LotteryPriceCouponStatus.Won), new SqlParameter("@UID", Params.UID), new SqlParameter("@LotteryGameId", GameDb.Id));

            return Ok();
        }
        #endregion
        #region TestRespreadLotteryPrices
        public BaseResponseDTO TestRespreadLotteryPrices(TestRespreadLotteryPricesParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            if (Params.AlsoResetPriceAvailability)
                DbContext.Database.ExecuteSqlCommand("update [dbo].[LotteryPrices] set [AmountWon] = 0");

            LotteryGame GameDb = null;
            if (Params.LotteryGameId <= 0)
                GameDb = DbContext.LotteryGames.FirstOrDefault(itm => (itm.Status == LotteryGameStatus.Ready || itm.Status == LotteryGameStatus.Started));
            else
                GameDb = DbContext.LotteryGames.Find(Params.LotteryGameId);

            if (GameDb == null)
                return Error(ReturnCodes.RecordNotFound, "Lottery game not available!");

            DbContext.Database.ExecuteSqlCommand("exec [dbo].[sp_LotterySpreadPrices] @LotteryGameId", new SqlParameter("@LotteryGameId", GameDb.Id));

            return Ok();
        }
        #endregion

        #region AddDailyLotteryCoins
        public void AddDailyLotteryCoins()
        {
            DbContext.SP_AddDailyLotteryCoins();
        }
        #endregion

        #region IsGameActive
        private static Dictionary<long, DateTime> _LotteryEndDates = null;

        public static bool IsGameActive(long LotteryGameId)
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Cache["lotteryenddates"] != null)
                    _LotteryEndDates = (Dictionary<long, DateTime>)HttpContext.Current.Cache["lotteryenddates"];
                else
                {
                    using (ApplicationDbContext StaticDbContext = new ApplicationDbContext())
                    {
                        _LotteryEndDates = Utils.LinqKeyValueListToDictionary<long, DateTime>(StaticDbContext.LotteryGames.Select(itm => new LinqKeyValuePair<long, DateTime>() { Key = itm.Id, Value = itm.EndTime }).ToList());
                    }

                    HttpContext.Current.Cache.Insert("lotteryenddates", _LotteryEndDates, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 30, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                }
            }
            else
            {
                if (_LotteryEndDates == null)
                {
                    using (ApplicationDbContext StaticDbContext = new ApplicationDbContext())
                    {
                        _LotteryEndDates = Utils.LinqKeyValueListToDictionary<long, DateTime>(StaticDbContext.LotteryGames.Select(itm => new LinqKeyValuePair<long, DateTime>() { Key = itm.Id, Value = itm.EndTime }).ToList());
                    }
                }
            }

            if (!_LotteryEndDates.ContainsKey(LotteryGameId))
                throw new Exception("There is no lottery game with the id " + LotteryGameId.ToString() + " in the database!");

            return (_LotteryEndDates[LotteryGameId] >= DateTime.UtcNow);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models;
using VmaBase.Models.Enum;
using VmaBase.Models.DataStorage.Tables;

namespace VmaBase.Providers
{
    public class ImportLogProvider : BaseProvider
    {
        public ImportTicket ImportTicket { get; set; }
        public ImportLog CurrentLog { get; set; }

        #region StartImport
        public ImportTicket StartImport(ImportLogType ImportType, bool bStartedAutomatically = true)
        {
            ImportTicket = new ImportTicket();
            ImportTicket.DateStarted = DateTime.UtcNow;

            DbContext.SP_CleanImportLog();

            ImportLog LastLogDb = DbContext.ImportLogs.OrderByDescending(itm => itm.RunNumber).FirstOrDefault(itm => itm.ImportType == ImportType);
            long RunNumber = (LastLogDb == null ? 1 : LastLogDb.RunNumber + 1);

            ImportLog LogDb = new ImportLog();
            LogDb.ImportType = ImportType;
            LogDb.StartTime = ImportTicket.DateStarted;
            LogDb.RunNumber = RunNumber;
            LogDb.Status = ImportStatus.Running;
            DbContext.ImportLogs.Add(LogDb);
            DbContext.SaveChanges();

            CurrentLog = LogDb;

            ImportTicket.Id = LogDb.Id;
            ImportTicket.RunNumber = RunNumber;

            WriteLog("Import started");

            return ImportTicket;
        }
        #endregion
        #region FinishImport
        public void FinishImport()
        {
            if (ImportTicket == null)
                throw new Exception("ImportLogProvider.FinishImport() can not be called, because the import has not yet been started with StartImport()! (No ImportTicket existing)");
            if (CurrentLog == null)
                CurrentLog = DbContext.ImportLogs.Find(ImportTicket.Id);

            WriteLog("Import finished successfully");

            CurrentLog.Status = ImportStatus.Completed;
            CurrentLog.CompletionTime = DateTime.UtcNow;
            CurrentLog.ExecutionTime = (int) (CurrentLog.CompletionTime.Value - CurrentLog.StartTime).TotalSeconds;
            DbContext.SaveChanges();            
        }
        #endregion
        #region StopImport
        public void StopImport(bool bFailure = true)
        {
            if (ImportTicket == null)
                throw new Exception("ImportLogProvider.StopImport() can not be called, because the import has not yet been started with StartImport()! (No ImportTicket existing)");
            if (CurrentLog == null)
                CurrentLog = DbContext.ImportLogs.Find(ImportTicket.Id);

            WriteLog(bFailure ? "Import failed!" : "Import cancelled");

            CurrentLog.Status = (bFailure ? ImportStatus.Failed : ImportStatus.Cancelled);
            CurrentLog.CompletionTime = DateTime.UtcNow;
            CurrentLog.ExecutionTime = (int)(CurrentLog.CompletionTime.Value - CurrentLog.StartTime).TotalSeconds;
            DbContext.SaveChanges();
        }
        #endregion

        #region WriteLog
        public void WriteLog(string LogMessage)
        {
            if (ImportTicket == null)
                throw new Exception("ImportLogProvider.WriteLog() can not be called, because the import has not yet been started with StartImport()! (No ImportTicket existing)");
            if (CurrentLog == null)
                CurrentLog = DbContext.ImportLogs.Find(ImportTicket.Id);

            ImportLogMessage Message = new ImportLogMessage();
            Message.ImportLogId = CurrentLog.Id;
            Message.LogTime = DateTime.UtcNow;
            Message.Message = LogMessage;
            DbContext.ImportLogMessages.Add(Message);
            DbContext.SaveChanges();
        }
        #endregion

        #region SetImportTicket
        public void SetImportTicket(ImportTicket Ticket)
        {
            ImportTicket = Ticket;
        }
        #endregion
    }
}

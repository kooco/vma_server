﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using AutoMapper;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

using OfficeOpenXml;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Reporting;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;


namespace VmaBase.Providers.Reporting
{
    public class UserCouponReportsProvider : BaseProvider
    {
        #region AutoCouponRedeemsQuerySql
        private string AutoCouponRedeemsQuerySql(DateTime DateFrom, DateTime DateTo, long? ProductId, long? ProductTypeId, long? ProductBrandId, long? MachineId, long? CriteriaId, long? ActivityId, string AccountFilter, string UsernameFilter, string sQueryFilter, string sOrderBy)
        {
            string sDateFrom = "'" + DateFrom.Year.ToString("0000") + "-" + DateFrom.Month.ToString("00") + "-" + DateFrom.Day.ToString("00") + " " + DateFrom.Hour.ToString("00") + ":" + DateFrom.Minute.ToString("00") + ":" + DateFrom.Second.ToString("00") + "'";
            string sDateTo = "'" + DateTo.Year.ToString("0000") + "-" + DateTo.Month.ToString("00") + "-" + DateTo.Day.ToString("00") + " " + DateTo.Hour.ToString("00") + ":" + DateTo.Minute.ToString("00") + ":" + DateTo.Second.ToString("00") + "'";

            return @"
                select [MachineId], [MachineAssetNo], [MachineName], [MachineLocation], [ProductId], [ProductName], [RedeemTime], [UID], [UserAccountId], [UserName], [CriteriaId], [CriteriaName], [ActivityId], [ActivityTitle], [CriteriaCouponsTitle], [AssignmentId]
                  from (
	                select msg.VendingMachineId [MachineId], vm.MOCCPAssetNo [MachineAssetNo], vm.[Name] [MachineName], vm.[Location] [MachineLocation], msg.ProductId [ProductId], prd.[Name] [ProductName], cp.RedeemTime, cp.[UID], usr.Account [UserAccountId], usr.[Name] [UserName], crit.[Id] [CriteriaId], crit.[Name] [CriteriaName], act.[Id] [ActivityId], act.[Title] [ActivityTitle], crit.CouponsTitle [CriteriaCouponsTitle], cp.[Id] [AssignmentId]
	                  from dbo.UserCoupons cp
                inner join dbo.VendingMachineMessages msg on msg.CouponId = cp.Id
           left outer join dbo.VendingMachines vm on vm.Id = msg.VendingMachineId
           left outer join dbo.VendingProducts prd on prd.Id = msg.ProductId
                inner join dbo.UserBases usr on usr.[UID] = cp.[UID]
                inner join dbo.UserDatas usrd on usrd.[UID] = cp.[UID]
           left outer join dbo.AutoCouponAssignments assgn on assgn.UserCouponId = cp.Id
           left outer join dbo.CouponActivities act on act.Id = cp.ActivityId
           left outer join dbo.AutoCouponCriterias crit on crit.Id = assgn.CriteriaId
	                 where not msg.CouponId is null
	                   and not RedeemTime is null
                       and RedeemTime >= " + sDateFrom + @"
                       and RedeemTime <= " + sDateTo + 
                       (ProductId != null && ProductId > 0 ? " and msg.ProductId = " + ProductId.Value.ToString() + " " : "") +
                       (MachineId != null && MachineId > 0 ? " and msg.VendingMachineId = " + MachineId.Value.ToString() + " " : "") +
                       (ProductTypeId != null && ProductTypeId > 0 ? " and prd.ProductTypeId = " + ProductTypeId.Value.ToString() + " " : "") +
                       (ProductBrandId != null && ProductBrandId > 0 ? " and prd.ProductBrandId = " + ProductBrandId.Value.ToString() + " " : "") +
                       (CriteriaId != null && CriteriaId > 0 ? " and crit.[Id] = " + CriteriaId.Value.ToString() + " " : "") +
                       (ActivityId != null && ActivityId > 0 ? " and act.[Id] = " + ActivityId.Value.ToString() + " " : "") +
                       (!string.IsNullOrWhiteSpace(AccountFilter) ? " and usr.[Account] like N'" + AccountFilter.Replace("'", "''").Trim() + "%' " : "") +
                       (!string.IsNullOrWhiteSpace(UsernameFilter) ? " and usr.[Name] like N'" + UsernameFilter.Replace("'", "''").Trim() + "%' " : "") + @"
                ) AutoCouponRedeems
            " + 
            (string.IsNullOrWhiteSpace(sQueryFilter) ? "" : " WHERE 1=1 AND " + sQueryFilter) +
            (string.IsNullOrWhiteSpace(sOrderBy) ? "" : " ORDER BY " + sOrderBy);
        }
        #endregion
        #region GetKendoAutoCouponRedeems
        public DataSourceResult GetKendoAutoCouponRedeems(DataSourceRequest request, DateTime DateFrom, DateTime DateTo, long? ProductId, long? ProductTypeId, long? ProductBrandId, long? MachineId, long? CriteriaId, long? ActivityId, string AccountFilter, string UsernameFilter)
        {
            string sFilterQuery = Utils.GetSQLWhereFromKendoFilter(request.Filters);
            string sOrderBy = Utils.GetSQLOrderByFromKendoSorts(request.Sorts);

            AuthenticationManager.SetUserSessionVariable("AutoRedeems_DateFrom", DateFrom);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_DateTo", DateTo);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_ProductId", ProductId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_ProductTypeId", ProductTypeId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_ProductBrandId", ProductBrandId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_MachineId", MachineId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_CriteriaId", CriteriaId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_ActivityId", ActivityId);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_Account", AccountFilter);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_Username", UsernameFilter);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_QueryFilter", sFilterQuery);
            AuthenticationManager.SetUserSessionVariable("AutoRedeems_SortOrder", sOrderBy);

            string sQuerySQL = AutoCouponRedeemsQuerySql(DateFrom.AddHours(-8), DateTo.AddHours(16), ProductId, ProductTypeId, ProductBrandId, MachineId, CriteriaId, ActivityId, AccountFilter, UsernameFilter, sFilterQuery, sOrderBy);
            return DbContext.Database.SqlQuery<AutoCouponRedeemReportEntryDTO>(sQuerySQL).ToDataSourceResult(request);
        }
        #endregion
        #region GetExcelAutoCouponRedeems
        public System.Web.Mvc.ActionResult GetExcelAutoCouponRedeems()
        {
            string sFilterQuery = AuthenticationManager.GetUserSessionVariableString("AutoRedeems_QueryFilter");
            string sOrderBy = AuthenticationManager.GetUserSessionVariableString("AutoRedeems_SortOrder");
            DateTime DateFrom = ((DateTime)AuthenticationManager.GetUserSessionVariable("AutoRedeems_DateFrom")).AddHours(-8);
            DateTime DateTo = ((DateTime)AuthenticationManager.GetUserSessionVariable("AutoRedeems_DateTo")).AddHours(16);
            long? ProductId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_ProductId"));
            long? ProductTypeId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_ProductTypeId"));
            long? ProductBrandId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_ProductBrandId"));
            long? MachineId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_MachineId"));
            long? CriteriaId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_CriteriaId"));
            long? ActivityId = ((long?)AuthenticationManager.GetUserSessionVariable("AutoRedeems_ActivityId"));
            string AccountId = AuthenticationManager.GetUserSessionVariableString("AutoRedeems_Account");
            string Username = AuthenticationManager.GetUserSessionVariableString("AutoRedeems_Username");
            string sSQL = AutoCouponRedeemsQuerySql(DateFrom.AddHours(-8), DateTo.AddHours(16), ProductId, ProductTypeId, ProductBrandId, MachineId, CriteriaId, ActivityId, AccountId, Username, sFilterQuery, sOrderBy);

            // create excel sheet
            try
            {
                // create excel object
                MemoryStream ExcelStream = new MemoryStream();
                ExcelPackage Excel = new ExcelPackage(ExcelStream);
                ExcelWorksheet Worksheet = Excel.Workbook.Worksheets.Add("資料");

                // generate header row
                Worksheet.Cells[1, 1].Value = "序列號";
                Worksheet.Cells[1, 2].Value = "兌換日期";
                Worksheet.Cells[1, 3].Value = "販賣機AssetNo";
                Worksheet.Cells[1, 4].Value = "販賣機地點";
                Worksheet.Cells[1, 5].Value = "產品";
                Worksheet.Cells[1, 6].Value = "會員帳號";
                Worksheet.Cells[1, 7].Value = "會員名字";
                Worksheet.Cells[1, 8].Value = "規則名稱";
                Worksheet.Cells[1, 9].Value = "活動";

                // format header
                Worksheet.Row(1).Style.Font.Bold = true;
                Worksheet.Row(1).Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                Worksheet.Row(1).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                // add rows
                string sDateTimeFormat = GlobalConst.ExcelExportDateTimeFormat;
                int iRow = 2;
                foreach (AutoCouponRedeemReportEntryDTO DTO in DbContext.Database.SqlQuery<AutoCouponRedeemReportEntryDTO>(sSQL))
                {
                    Worksheet.Cells[iRow, 1].Value = DTO.AssignmentId.ToString("00000000");

                    Worksheet.Cells[iRow, 2].Value = Convert.ToDateTime(DTO.RedeemTime);
                    Worksheet.Cells[iRow, 2].Style.Numberformat.Format = sDateTimeFormat;

                    Worksheet.Cells[iRow, 3].Value = DTO.MachineAssetNo;

                    Worksheet.Cells[iRow, 4].Value = DTO.MachineName;

                    Worksheet.Cells[iRow, 5].Value = DTO.ProductName;

                    Worksheet.Cells[iRow, 6].Value = DTO.UserAccountId;

                    Worksheet.Cells[iRow, 7].Value = DTO.UserName;

                    Worksheet.Cells[iRow, 8].Value = DTO.CriteriaName;

                    Worksheet.Cells[iRow, 9].Value = DTO.ActivityTitle;

                    iRow++;
                }

                for (int iInCol = 1; iInCol <= 6; iInCol++)
                    Worksheet.Column(iInCol).AutoFit();

                byte[] exceldata = Excel.GetAsByteArray();
                Excel.Dispose();
                ExcelStream.Dispose();

                FileContentResult Result = new FileContentResult(exceldata, "application/excel");
                Result.FileDownloadName = "AutoCouponRedeems.xlsx";
                return Result;
            }
            catch (Exception exc)
            {
                string sParamsJSON = string.Empty;
                Utils.LogError(exc, "Error while generating the excel file for the AutoCouponRedeems Kendo Grid Report");

                ContentResult Result = new ContentResult();
                Result.Content = "Sorry! An unexpected error occured while exporting the data to an excel file!";
                return Result;
            }
        }
        #endregion

        #region GetProductsList
        public BaseResponseDTO GetProductsList()
        {
            BaseResponseDTO Response = new BaseResponseDTO();

            List<long> UsedProductIdsList = DbContext.Database.SqlQuery<long>("select distinct ProductId " +
                                                                              "  from dbo.VendingMachineMessages msg " +
                                                                         " inner join dbo.UserCoupons cp on cp.Id = msg.CouponId " +
                                                                         " inner join dbo.AutoCouponAssignments assgn on assgn.UserCouponId = cp.Id").ToList();
            Response.Data = DbContext.VendingProducts.Where(itm => UsedProductIdsList.Contains(itm.Id)).Select(Mapper.Map<VendingProduct, VendingProductAdminDTO>).ToList();
            return Response;
        }
        #endregion
    }
}

﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AutoMapper;
using Newtonsoft.Json;
using OfficeOpenXml;

using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;

namespace VmaBase.Providers
{
    /// <summary>
    ///     LotteryPriceCouponsAdminProvider
    ///     --------------------------------
    ///     
    ///     Provider class for reporting the already won lottery price coupons
    ///     for: Admin Interface
    /// </summary>
    public class AutoCouponCriteriaAdminProvider : BaseListDataProvider<AutoCouponCriteria, AutoCouponCriteriaAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<AutoCouponCriteriaAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<AutoCouponCriteria> Query = DbContext.AutoCouponCriterias.Include(itm => itm.Module).Include(itm => itm.UserBase).Where(itm => itm.Status != AutoCouponCriteriaStatus.Deleted);
            return GetPagedList(Params, Query, DataPrepared, HandleSearchCriterias);
        }

        private void HandleSearchCriterias(ref IQueryable<AutoCouponCriteria> Query, ref StandardSearchParamsDTO<AutoCouponCriteriaAdminDTO> Params)
        {
            // customized search criterias
        }

        public Kooco.Framework.Shared.FrameworkReturnCodes DataPrepared(ref List<AutoCouponCriteriaAdminDTO> CriteriaList)
        {
            foreach(AutoCouponCriteriaAdminDTO CriteriaDTO in CriteriaList)
            {
                CriteriaDTO.CouponsIssued = DbContext.AutoCouponAssignments.Where(itm => itm.CriteriaId == CriteriaDTO.Id).Count();
            }

            return Kooco.Framework.Shared.FrameworkReturnCodes.Success;
        }

        #region GetExcel
        public ActionResult GetExcel()
        {
            IQueryable<AutoCouponCriteria> Query = DbContext.AutoCouponCriterias.Include(itm => itm.Module).Include(itm => itm.UserBase).Where(itm => itm.Status != AutoCouponCriteriaStatus.Deleted);
            return GetExcel(Query, null, HandleSearchCriterias);
        }
        #endregion

        #region CreateNew
        public BaseResponseDTO CreateNew(long? UID = null)
        {
            AutoCouponCriteria CriteriaDb = new AutoCouponCriteria();
            CriteriaDb.CouponsTitle = "<請輸入活動的辯題>";
            CriteriaDb.CouponsSubTitle = "";
            CriteriaDb.Name = "<請輸入名稱>";
            CriteriaDb.Status = AutoCouponCriteriaStatus.Inactive;
            CriteriaDb.UID = UID;
            DbContext.AutoCouponCriterias.Add(CriteriaDb);
            DbContext.SaveChanges();

            return Ok();
        }
        #endregion

        #region GetCriteriaDetails
        public BaseResponseDTO GetCriteriaDetails(long CriteriaId)
        {
            if (CriteriaId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'CriteriaId' is missing!");

            AutoCouponCriteria CriteriaDb = DbContext.AutoCouponCriterias.Include(itm => itm.Module).Include(itm => itm.UserBase).Where(itm => itm.Id == CriteriaId).FirstOrDefault();
            if (CriteriaDb == null)
                return Error(ReturnCodes.RecordNotFound);

            AutoCouponCriteriaAdminDTO CriteriaDTO = Mapper.Map<AutoCouponCriteria, AutoCouponCriteriaAdminDTO>(CriteriaDb);
            CriteriaDTO.CSharpCode = (CriteriaDb.Module != null ? CriteriaDb.Module.CsharpCode : string.Empty);
            CriteriaDTO.Products = DbContext.AutoCouponCriteriaProducts.Include(itm => itm.Product).Include(itm => itm.Product.DefaultStampType)
                .Where(itm => itm.CriteriaId == CriteriaId).Select(Mapper.Map<AutoCouponCriteriaProduct, CouponCriteriaProductAdminDTO>).ToList();
            CriteriaDTO.Machines = DbContext.AutoCouponCriteriaMachines.Include(itm => itm.Machine)
                .Where(itm => itm.CriteriaId == CriteriaId).Select(Mapper.Map<AutoCouponCriteriaMachine, CouponCriteriaMachineAdminDTO>).ToList();
            return ResultObject(CriteriaDTO);
        }
        #endregion
        #region CheckCSharpModule
        public BaseResponseDTO CheckCSharpModule(CheckCSharpModuleParamsDTO Params)
        {
            bool MeetsCriteria = false;

            if (string.IsNullOrWhiteSpace(Params.ModuleName))
                return Error(ReturnCodes.AdminInputError, "名稱不能為空！");
            if (string.IsNullOrWhiteSpace(Params.ModuleCode))
                return Error(ReturnCodes.AdminInputError, "C# Code 不能為空！");
            if ((!Params.SaveOnSuccess) && (Params.TestuserUID <= 0))
                return Error(ReturnCodes.AdminInputError, "測試用的會員不能為空！");
            if (Params.CriteriaId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'CriteriaId' is missing!");

            CheckAutoCouponModuleResultDTO Result = AutoCouponsProvider.CheckModuleCode(Params.CriteriaId, Params.ModuleCode, out MeetsCriteria, Params.TestuserUID);
            if ((Result.Ok) && (Params.SaveOnSuccess))
            {
                // load criteria
                AutoCouponCriteria CriteriaDb = DbContext.AutoCouponCriterias.FirstOrDefault(itm => itm.Id == Params.CriteriaId && itm.Status != AutoCouponCriteriaStatus.Deleted);
                if (CriteriaDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no criteria with the id " + Params.CriteriaId.ToString() + " in the database!");

                // load or create module
                AutoCouponModule ModuleDb = null;
                bool bAddNewModule = false;

                if ((Params.CreateNewModule) || (CriteriaDb.ModuleId == null))
                {
                    ModuleDb = new AutoCouponModule();
                    ModuleDb.Description = "Module Template";
                    ModuleDb.CreateUserUID = AuthenticationManager.CurrentUser.UID;
                    ModuleDb.Status = CSharpModuleStatus.Active;
                    CriteriaDb.Module = ModuleDb;
                    bAddNewModule = true;
                }
                else
                {
                    if (Params.ModuleId > 0)
                        CriteriaDb.ModuleId = Params.ModuleId;

                    ModuleDb = DbContext.AutoCouponModules.FirstOrDefault(itm => itm.Id == CriteriaDb.ModuleId);
                    if (ModuleDb == null)
                        return Error(ReturnCodes.RecordNotFound, "Could not find the module bound to the criteria with the module id " + CriteriaDb.ModuleId.ToString() + "!");
                }

                ModuleDb.Name = Params.ModuleName;
                ModuleDb.CsharpCode = Params.ModuleCode;
                ModuleDb.ModifyTime = DateTime.UtcNow;
                ModuleDb.ModifyUserUID = AuthenticationManager.CurrentUser.UID;

                if (bAddNewModule)
                    DbContext.AutoCouponModules.Add(ModuleDb);
                DbContext.SaveChanges();

                Result.ModuleId = ModuleDb.Id;
                Result.ModuleName = ModuleDb.Name;
            }

            return ResultObject(Result);
        }
        #endregion
        #region SaveCSharpModule
        public BaseResponseDTO SaveCSharpModule(SaveCSharpModuleAdminParamsDTO Params)
        {
            CheckAutoCouponModuleResultDTO Result = null;

            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.CriteriaId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'CriteriaId' is missing!");
            if ((!Params.NewModule) && ((Params.ModuleId == null) || (Params.ModuleId <= 0)))
                return Error(ReturnCodes.MissingArgument, "The parameter 'ModuleId' is missing. This parameter must be set, if 'NewModule' is set to false!");
            if (string.IsNullOrWhiteSpace(Params.CSharpCode))
                return Error(ReturnCodes.MissingArgument, "The parameter 'CSharpCode' is missing or empty!");

            // get criteria
            AutoCouponCriteria CriteriaDb = DbContext.AutoCouponCriterias.FirstOrDefault(itm => itm.Id == Params.CriteriaId && itm.Status != AutoCouponCriteriaStatus.Deleted);
            if (CriteriaDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no critria with the id " + Params.CriteriaId.ToString() + " in the database!");

            // get/create code module
            AutoCouponModule ModuleDb = null;
            if (Params.NewModule)
            {
                ModuleDb = DbContext.AutoCouponModules.FirstOrDefault(itm => itm.Id == Params.ModuleId && itm.Status != CSharpModuleStatus.Deleted);
                if (ModuleDb == null)
                    return Error(ReturnCodes.RecordNotFound, "There is no auto coupon module with the id " + Params.ModuleId.ToString() + " in the database!");
            }
            else
            {
                ModuleDb = new AutoCouponModule();
                ModuleDb.Description = "Module Description";
                ModuleDb.CreateUserUID = AuthenticationManager.CurrentUser.UID;
                ModuleDb.Status = CSharpModuleStatus.Active;
            }

            // test the C# code
            bool MeetsCriteria = false;
            Result = AutoCouponsProvider.CheckModuleCode(Params.CriteriaId, Params.CSharpCode, out MeetsCriteria, ConfigManager.CSharpModulesCompileCheckUserId);
            if (!Result.Ok)
                return ResultObject(Result);

            // update module code
            ModuleDb.CsharpCode = Params.CSharpCode;
            ModuleDb.Name = Params.ModuleName;

            if (Params.NewModule)
                DbContext.AutoCouponModules.Add(ModuleDb);

            // update criteria
            CriteriaDb.Status = (Params.Activate ? AutoCouponCriteriaStatus.Active : AutoCouponCriteriaStatus.Inactive);

            // if an invalid module deactivated other criterias -> reactivate them now
            DbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[AutoCouponCriterias] SET [Status] = " + ((int)(AutoCouponCriteriaStatus.Active)).ToString() + " WHERE [ModuleId] = " + ModuleDb.Id.ToString() + " AND [Status] = " + ((int)(AutoCouponCriteriaStatus.ModuleInvalid)).ToString());

            // save changes
            DbContext.SaveChanges();

            return ResultObject(Result);
        }
        #endregion
        #region SaveCriteriaDetails
        public BaseResponseDTO SaveCriteriaDetails(SaveCriteriaDetailsAdminParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No parameters given!");
            if (Params.CriteriaId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'CriteriaId' is missing!");

            // get criteria
            AutoCouponCriteria CriteriaDb = DbContext.AutoCouponCriterias.FirstOrDefault(itm => itm.Id == Params.CriteriaId && itm.Status != AutoCouponCriteriaStatus.Deleted);
            if (CriteriaDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no critria with the id " + Params.CriteriaId.ToString() + " in the database!");

            // get current product and machine assignments
            List<AutoCouponCriteriaProduct> CurrentAssignedProducts = DbContext.AutoCouponCriteriaProducts.Where(itm => itm.CriteriaId == Params.CriteriaId).ToList();
            List<AutoCouponCriteriaMachine> CurrentAssignedMachines = DbContext.AutoCouponCriteriaMachines.Where(itm => itm.CriteriaId == Params.CriteriaId).ToList();

            CriteriaDb.CouponsForAllProducts = Params.CouponsForAllProducts;
            CriteriaDb.CouponsForAllMachines = Params.CouponsForAllMachines;
            CriteriaDb.CouponsExpiryDate = Params.CouponsExpiryDate;
            CriteriaDb.Status = Params.Status;
            CriteriaDb.ModifyTime = DateTime.UtcNow;

            // when activating the criteria => also check the C# module again before
            if (Params.Status == AutoCouponCriteriaStatus.Active)
            {
                if (CriteriaDb.ModuleId == null)
                    return Error(ReturnCodes.CSharpModuleMissing, "The criteria can not be activated, because no C# module has been assigned yet!");

                string CSharpCode = DbContext.AutoCouponModules.First(itm => itm.Id == CriteriaDb.ModuleId.Value).CsharpCode;
                bool MeetsCriteria = false;
                CheckAutoCouponModuleResultDTO Result = AutoCouponsProvider.CheckModuleCode(Params.CriteriaId, CSharpCode, out MeetsCriteria, ConfigManager.CSharpModulesCompileCheckUserId);
                if (!Result.Ok)
                    return Error(ReturnCodes.CriteriaInvalidModule);
            }

            // add/remove assigned products/machines
            List<AutoCouponCriteriaProduct> ProductsToRemove = new List<AutoCouponCriteriaProduct>();
            List<AutoCouponCriteriaMachine> MachinesToRemove = new List<AutoCouponCriteriaMachine>();

            // add new product assignments
            if (Params.Products == null)
                Params.Products = new List<CouponCriteriaProductAdminDTO>();

            foreach(CouponCriteriaProductAdminDTO ProductDTO in Params.Products)
            {
                if (CurrentAssignedProducts.Where(itm => itm.ProductId == ProductDTO.ProductId).Count() <= 0)
                {
                    AutoCouponCriteriaProduct NewProductAssignmentDb = new AutoCouponCriteriaProduct();
                    NewProductAssignmentDb.CriteriaId = Params.CriteriaId;
                    NewProductAssignmentDb.ProductId = ProductDTO.ProductId;
                    DbContext.AutoCouponCriteriaProducts.Add(NewProductAssignmentDb);
                }
            }

            // add new machine assignments
            if (Params.Machines == null)
                Params.Machines = new List<CouponCriteriaMachineAdminDTO>();

            foreach (CouponCriteriaMachineAdminDTO MachineDTO in Params.Machines)
            {
                if (CurrentAssignedMachines.Where(itm => itm.MachineId == MachineDTO.VendingMachineId).Count() <= 0)
                {
                    AutoCouponCriteriaMachine NewMachineAssignmentDb = new AutoCouponCriteriaMachine();
                    NewMachineAssignmentDb.CriteriaId = Params.CriteriaId;
                    NewMachineAssignmentDb.MachineId = MachineDTO.VendingMachineId;
                    DbContext.AutoCouponCriteriaMachines.Add(NewMachineAssignmentDb);
                }
            }

            // remove removed product assignments
            foreach(AutoCouponCriteriaProduct Product in CurrentAssignedProducts)
            {
                if (Params.Products.Where(itm => itm.ProductId == Product.ProductId).Count() <= 0)
                    ProductsToRemove.Add(Product);
            }
            foreach (AutoCouponCriteriaProduct ProductToRemove in ProductsToRemove)
                DbContext.AutoCouponCriteriaProducts.Remove(ProductToRemove);

            // remove removed machine assignments
            foreach (AutoCouponCriteriaMachine Machine in CurrentAssignedMachines)
            {
                if (Params.Machines.Where(itm => itm.VendingMachineId == Machine.MachineId).Count() <= 0)
                    MachinesToRemove.Add(Machine);
            }
            foreach (AutoCouponCriteriaMachine MachineToRemove in MachinesToRemove)
                DbContext.AutoCouponCriteriaMachines.Remove(MachineToRemove);

            DbContext.SaveChanges();

            return Ok();
        }
        #endregion
        #region GetModuleCode
        public BaseResponseDTO GetModuleCode(long ModuleId)
        {
            if (ModuleId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'ModuleId' is missing!");

            AutoCouponModule ModuleDb = DbContext.AutoCouponModules.FirstOrDefault(itm => itm.Id == ModuleId && itm.Status != CSharpModuleStatus.Deleted);
            if (ModuleDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no module with the id " + ModuleId.ToString() + " in the database!");

            return ResultObject(ModuleDb.CsharpCode);
        }
        #endregion

        #region GetModulesDropDownList
        public BaseResponseDTO GetModulesDropDownList()
        {
            List<AutoCouponModuleAdminListDTO> ModulesList = DbContext.AutoCouponModules.Where(itm => itm.Status != CSharpModuleStatus.Deleted).Select(Mapper.Map<AutoCouponModule, AutoCouponModuleAdminListDTO>).ToList();
            return ResultObject(ModulesList);
        }
        #endregion

        #region ResetUserStates
        public BaseResponseDTO ResetUserStates(ResetUserStatesParamsDTO Params)
        {
            if (Params == null)
                return Error(ReturnCodes.MissingArgument, "No arguments given!");
            if (Params.CriteriaId <= 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'CriteriaId' is missing!");
            if (string.IsNullOrWhiteSpace(Params.Account))
                return Error(ReturnCodes.MissingArgument, "The parameter 'Account' is missing!");

            long UserId = DbContext.UserBases.Where(itm => itm.Account == Params.Account).Select(itm => itm.UID).FirstOrDefault();
            if (UserId <= 0)
                return Error(ReturnCodes.UserNotFound, "There is no user with the id '" + Params.Account + "'!");

            DbContext.Database.ExecuteSqlCommand("DELETE FROM [dbo].[AutoCouponUserStates] WHERE [CriteriaId] = " + Params.CriteriaId.ToString() + " AND [UID] = " + UserId.ToString());
            return Ok();
        }
        #endregion
    }
}

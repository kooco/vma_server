﻿using Kooco.Framework.Models.DataStorage.Tables;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;

namespace VmaBase.Providers
{
    public class TestProvider : BaseProvider
    {
        #region CreateTestUsers
        public void CreateTestUsers(int iCount)
        {
            Random rnd = new Random();
            UserBase UserBaseDb = null;
            UserData UserDataDb = null;
            bool bError;

            ApplicationDbContext Context = new ApplicationDbContext();

            for(int iNr = 1; iNr <= iCount; iNr++)
            {
                bError = true;

                while (bError)
                {
                    try
                    {
                        UserBaseDb = new UserBase();
                        UserBaseDb.Account = ((char)64 + rnd.Next(1, 26)).ToString() + Guid.NewGuid().ToString().Substring(1, 6).ToLower() + ((char)64 + rnd.Next(1, 26)).ToString() + DateTime.Now.Millisecond.ToString("000");
                        UserBaseDb.Phone = "+886" + rnd.Next(100000000, 999999999);
                        UserBaseDb.Status = Kooco.Framework.Models.Enum.UserStatus.Normal;

                        UserDataDb = new UserData();
                        UserDataDb.Location = "unknown";
                        UserDataDb.UserBase = UserBaseDb;

                        Context.UserDatas.Add(UserDataDb);
                        Context.SaveChanges();

                        if (iNr % 20 == 0)
                        {
                            Debug.WriteLine(iNr.ToString() + " / " + iCount.ToString());
                        }

                        bError = false;
                    }
                    catch (Exception exc)
                    {
                        Context.Dispose();
                        Context = new ApplicationDbContext();
                        Debug.WriteLine(exc.Message);
                    }
                }
            }

            DbContext.SaveChanges();
        }
        #endregion
        #region GetRandomPhoneSearch7List
        public List<string> GetRandomPhoneSearch7List()
        {
            List<string> PhoneNumerList = new List<string>();
            Random rnd = new Random();
            int iRecord = 0;

            // get at least 10 real phone numbers
            for (int iNr = 1; iNr <= 10; iNr++)
            {
                iRecord = rnd.Next(1, 10000);
                PhoneNumerList.Add(DbContext.UserDatas.OrderBy(itm => itm.UID).Skip(iRecord).Take(1).ToList()[0].PhoneNrSearch7);
            }

            // now generate random phone numbers
            for (int iNr = 1; iNr <= 250; iNr++)
            {
                string sNr = rnd.Next(1000000, 9999999).ToString();
                if (!PhoneNumerList.Contains(sNr))
                    PhoneNumerList.Add(sNr);
            }

            return PhoneNumerList;
        }
        #endregion
    }
}

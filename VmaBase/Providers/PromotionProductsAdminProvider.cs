﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaBase.Providers
{
    public class PromotionProductsAdminProvider : BaseListDataProvider<PromotionProduct, PromotionProductAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<PromotionProductAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<PromotionProduct> Query = DbContext.PromotionProducts.Where(itm => itm.Status != GeneralStatusEnum.Deleted);

            return GetPagedList(Params, Query);
        }

        public BaseResponseDTO CreateNewProduct()
        {
            int iNumber = 1;

            if (DbContext.PromotionProducts.Count() > 0)
                iNumber = Convert.ToInt32(DbContext.PromotionProducts.Where(itm => itm.Status != GeneralStatusEnum.Deleted).OrderBy(itm => itm.ProductNumber).Max(itm => itm.ProductNumber)) + 1;

            PromotionProduct Product = new PromotionProduct();
            Product.CreateTime = DateTime.Now;
            Product.ProductNumber = iNumber;
            Product.Name = "<名字>";
            Product.ImageUrl = "/images/noimage.png";
            Product.Points = 0;
            Product.Status = GeneralStatusEnum.Inactive;
            DbContext.PromotionProducts.Add(Product);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<PromotionProduct, PromotionProductAdminDTO>(Product));
        }
    }
}

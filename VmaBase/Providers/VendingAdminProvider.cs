﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;

namespace VmaBase.Providers
{
    /// <summary>
    ///     VendingAdminProvider
    ///     --------------------
    ///     
    ///     Provider class for managing vending machines, products and sales list
    ///     for: Admin Interface / Coca Cola Warehouse Interface
    /// </summary>
    public class VendingAdminProvider : BaseProvider
    {
        #region AddVendingMachine
        public BaseResponseDTO AddVendingMachine(VendingMachineDTO MachineDTO)
        {
            PermissionsProvider.RequestAdminRole();

            if (MachineDTO.Id > 0)
                return Error(ReturnCodes.InvalidArguments, "The Id can not be greater than 0, if a new vending machine should be created!");

            VendingMachine MachineDb = Mapper.Map<VendingMachineDTO, VendingMachine>(MachineDTO);
            MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation(MachineDTO.Longitude, MachineDTO.Latitude);
            if (MachineDb.GeoLocation == null)
                return Error(ReturnCodes.MissingArgument, "Latitude and/or Longitude are missing!");

            MachineDb.ImageUrl = "notimplemented";
            MachineDb.QRToken = "notimplemented";

            DbContext.VendingMachines.Add(MachineDb);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<VendingMachine, VendingMachineDTO>(MachineDb));
        }
        #endregion
        #region ChangeVendingMachineStatus
        public BaseResponseDTO ChangeVendingMachineStatus(long VendingMachineId, VendingMachineStatus NewStatus)
        {
            PermissionsProvider.RequestAdminRole();

            VendingMachine MachineDb = DbContext.VendingMachines.Find(VendingMachineId);
            if (MachineDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no vending machine with the id " + VendingMachineId);

            MachineDb.Status = NewStatus;
            MachineDb.ModifyTime = DateTime.UtcNow;
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<VendingMachine, VendingMachineDTO>(MachineDb));
        }
        #endregion
        #region UpdateVendingMachine
        public BaseResponseDTO UpdateVendingMachine(VendingMachineDTO MachineDTO)
        {
            PermissionsProvider.RequestAdminRole();
            VendingMachine MachineDb = DbContext.VendingMachines.Find(MachineDTO.Id);
            if (MachineDb == null)
                return Error(ReturnCodes.RecordNotFound, "There is no vending machine with the id " + MachineDTO.Id.ToString());

            if (string.IsNullOrWhiteSpace(MachineDTO.Name))
                return Error(ReturnCodes.MissingArgument, "The parameter 'Name' is required!");
            if (string.IsNullOrWhiteSpace(MachineDTO.Location))
                return Error(ReturnCodes.MissingArgument, "The parameter 'Location' is required!");
            if (MachineDTO.Latitude == 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'Latitude' is required!");
            if (MachineDTO.Longitude == 0)
                return Error(ReturnCodes.MissingArgument, "The parameter 'Longitude' is required!");

            MachineDb.City = MachineDTO.City;
            MachineDb.Location = MachineDTO.Location;
            MachineDb.Name = MachineDTO.Name;
            MachineDb.Status = MachineDTO.Status;
            MachineDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation(MachineDTO.Longitude, MachineDTO.Latitude);
            DbContext.SaveChanges();

            return ResultObject(Mapper.Map<VendingMachine, VendingMachineDTO>(MachineDb));
        }
        #endregion
    }
}

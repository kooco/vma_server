﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Models.DataTransferObjects.Input;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class ErrorReportProvider : BaseProvider
    {
        public BaseResponseDTO ReportError(ReportErrorParamsDTO Params)
        {
            try
            {
                if (Params == null)
                    return Error(Shared.ReturnCodes.MissingArgument, "No parameters given!");

                string sErrorInfo = "SMARTPHONE ERROR REPORT. The smartphone reported an error or a problem.\nApp version: "
                        + (string.IsNullOrWhiteSpace(Params.AppVersion) ? "n/a" : Params.AppVersion) + "\n"
                        + "User: " + (string.IsNullOrWhiteSpace(Params.UserInfo) ? "n/a" : Params.UserInfo) + "\n"
                        + "Description: " + (string.IsNullOrWhiteSpace(Params.Description) ? "n/a" : Params.Description) + "\n"
                        + "App Log:\n" + Params.ReportData + "\n"
                        + "-- end of smartphone error report --";

                Utils.LogError(sErrorInfo, System: "CLIENT");
                return Ok();
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in ReportError() API!");
                return Error(ReturnCodes.InternalServerError);
            }            
        }
    }
}

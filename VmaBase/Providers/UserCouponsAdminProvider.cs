﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AutoMapper;
using Newtonsoft.Json;
using OfficeOpenXml;

using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Models.Enum;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;

namespace VmaBase.Providers
{
    /// <summary>
    ///     UserCouponsAdminProvider
    ///     ------------------------
    ///     
    ///     Provider class for reporting the user coupons the users have
    ///     for: Admin Interface
    /// </summary>
    public class UserCouponsAdminProvider : BaseListDataProvider<UserCoupon, UserCouponAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<UserCouponAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<UserCoupon> Query = DbContext.UserCoupons
                .Include(itm => itm.Activity)
                .Include(itm => itm.AutoCouponCriteria)
                .Include(itm => itm.UserData)
                .Include(itm => itm.UserData.UserBase)
                .Include(itm => itm.SenderUserData)
                .Include(itm => itm.SenderUserData.UserBase)
                .Where(itm => itm.Status != UserCouponStatus.Disabled);
            return GetPagedList(Params, Query, null, HandleSearchCriterias);
        }

        private void HandleSearchCriterias(ref IQueryable<UserCoupon> Query, ref StandardSearchParamsDTO<UserCouponAdminDTO> Params)
        {
            // customized search criterias
        }

        #region GetExcel
        public ActionResult GetExcel()
        {
            IQueryable<UserCoupon> Query = DbContext.UserCoupons
                .Include(itm => itm.Activity)
                .Include(itm => itm.AutoCouponCriteria)
                .Include(itm => itm.UserData)
                .Include(itm => itm.UserData.UserBase)
                .Include(itm => itm.SenderUserData)
                .Include(itm => itm.SenderUserData.UserBase)
                .Where(itm => itm.Status != UserCouponStatus.Disabled);

            return GetExcel(Query, null, HandleSearchCriterias);
        }
        #endregion

        #region GetActivitiesDropDownList
        public BaseResponseDTO GetActivitiesDropDownList()
        {
            List<CouponActivityListEntryDTO> DDList = 
               DbContext.CouponActivities
                .Where(itm => (itm.Status == ActivityStatus.Active || itm.Status == ActivityStatus.Closed))
                .Select(Mapper.Map<CouponActivity, CouponActivityListEntryDTO>)
                .ToList();

            return ResultObject(DDList);
        }
        #endregion
        #region GetCriteriasDropDownList
        public BaseResponseDTO GetCriteriasDropDownList()
        {
            List<AutoCouponCriteriaListEntry> DDList =
                DbContext.AutoCouponCriterias
                    .Where(itm => itm.Status != AutoCouponCriteriaStatus.Deleted &&
                            ((itm.CouponsExpiryDate != null && DateTime.UtcNow <= itm.CouponsExpiryDate) ||
                             (itm.CouponsExpiryDate == null && DateTime.UtcNow <= itm.LimitEndTime)))
                    .Select(Mapper.Map<AutoCouponCriteria, AutoCouponCriteriaListEntry>)
                    .ToList();

            return ResultObject(DDList);
        }
        #endregion
    }
}

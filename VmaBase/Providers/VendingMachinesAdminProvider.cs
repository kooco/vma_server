﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects.AdminResponse;

namespace VmaBase.Providers
{
    public class VendingMachinesAdminProvider : BaseListDataProvider<VendingMachine, VendingMachineAdminDTO>
    {
        public BaseResponseDTO GetList(StandardSearchParamsDTO<VendingMachineAdminDTO> Params)
        {
            PermissionsProvider.RequestAdminRole();

            IQueryable<VendingMachine> Query = DbContext.VendingMachines.Where(itm => itm.Status != Models.Enum.VendingMachineStatus.Removed);

            return GetPagedList(Params, Query);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Kooco.Framework.Models;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Providers;
using Kooco.Framework.Models.Interfaces;
using Kooco.Framework.Models.DataStorage;
using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Models.Enum;

using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.Enum;
using VmaBase.Shared;


namespace VmaBase.Providers
{
    public class StatisticsProvider : StatisticsBaseProvider<VmaBase.Models.DataStorage.ApplicationDbContext>
    {
        #region DbContext
        public new VmaBase.Models.DataStorage.ApplicationDbContext DbContext
        {
            get
            {
                return (VmaBase.Models.DataStorage.ApplicationDbContext) base.DbContext;
            }
        }
        #endregion

        #region GetStatistics
        public override FrameworkResponseDTO GetStatistics(GetStatisticsBaseParamsDTO Params, bool IncludeNullInGroupValues = false)
        {
            FrameworkResponseDTO Response = base.GetStatistics(Params, IncludeNullInGroupValues);
            if (Response.ReturnCode == Kooco.Framework.Shared.FrameworkReturnCodes.Success)
            {
                GetStatisticsResultDTO ResultDTO = (GetStatisticsResultDTO)Response.Data;

                switch(Params.ItemId)
                {
                    case (long)StatisticsItems.Lottery_WonPricesCount:
                    case (long)StatisticsItems.Lottery_RedeemedPricesCount:
                        if (ResultDTO.Group1Values.Count > 0)
                        {
                            List<StatisticsGroupValueEntryDTO> GroupValues =
                                DbContext.LotteryPrices.Select(itm => new StatisticsGroupValueEntryDTO() { Value = itm.Id.ToString(), DisplayName = itm.Title }).ToList();
                            MapGroupValueDisplayNames(ResultDTO.Group1Values, GroupValues);
                        }
                        break;

                    case (long)StatisticsItems.Sales_SoldItemsCount:
                    case (long)StatisticsItems.Sales_SoldItemsCountUsingApp:
                        if (ResultDTO.Group1Values.Count > 0)
                        {
                            List<StatisticsGroupValueEntryDTO> GroupValues =
                                DbContext.VendingMachines.Select(itm => new StatisticsGroupValueEntryDTO() { Value = itm.Id.ToString(), DisplayName = itm.MOCCPAssetNo }).ToList();
                            MapGroupValueDisplayNames(ResultDTO.Group1Values, GroupValues);
                        }
                        break;
                }
            }

            return Response;
        }
        #endregion

        #region GetCustomStatistics
        public BaseResponseDTO GetCustomStatistics(GetCustomStatisticsBaseParamsDTO Params)
        {
            switch (Params.CustomItemId)
            {
                case (int)CustomStatisticItems.Users_RetentionRates:
                    return GetCustomUserRolesCountStatistics(Params);

                default:
                    throw new Exception("The CustomItemID " + Params.CustomItemId.ToString() + " is invalid or not supported!");
            }
        }
        #endregion
        #region GetUserRetentionRateStatistics
        private BaseResponseDTO GetCustomUserRolesCountStatistics(GetCustomStatisticsBaseParamsDTO Params)
        {
            /*
            return GetCustomStatistics(
                new CustomStatisticsSQLParams("SELECT RoleCode [Group1Value], Cast(IsNull(Count([UID]), 0) As float) [Value]" +
                                                     "  FROM [dbo].[UserBases] " +
                                                     (Params.Group1ValueList != null && Params.Group1ValueList.Count > 0 ? " WHERE [RoleCode] IN (" + RolesINClause + ")" : "") +
                                                     " GROUP BY [RoleCode]"),
                new CustomStatisticsSQLParams("SELECT [Code] [Value], [Description] [DisplayName] FROM [dbo].[ApplicationRoles] WHERE [Code] IN (" + RolesINClause + ")"),
                Group1Label: "Role");*/
            return null;
        }
        #endregion

        // -------------------------
        // Filter DropDown Providers
        // -------------------------

        #region GetVendingMachinesList
        public BaseResponseDTO GetVendingMachinesList()
        {
            List<StatisticsGroupValueEntryDTO> MachinesList =
                DbContext.VendingMachines.Where(itm => itm.Status != VendingMachineStatus.Removed).Select(itm => new StatisticsGroupValueEntryDTO() { Value = itm.Id.ToString(), DisplayName = itm.MOCCPAssetNo }).ToList();

            return new BaseResponseDTO(MachinesList);
        }
        #endregion

        // ---------------
        // Update Routines
        // ---------------

        #region UpdateUserStatistics
        public void UpdateUserStatistics(int iDays)
        {
            for (int iDay = 0; iDay < iDays; iDay++)
            {
                DateTime DateFrom = DateTime.Now.Date.AddDays(-(iDay)).AddHours(-(GlobalConst.LocalTimeOffset)); // local time day begin (db times are UTC)
                DateTime DateTo = DateTime.Now.Date.AddDays(-(iDay)).AddHours(24-GlobalConst.LocalTimeOffset).AddMilliseconds(-003);   // local time day end (db times are UTC) - 23:59:59.997 (SQL Server stores only .003 millisecond steps)
                DateTime OriginalDateTo = DateTo;
                DateTime StatisticsDate = DateTime.Now.AddDays(-(iDay)).Date;

                // Registrations Count
                long lCount = DbContext.UserLoginLogs.Where(itm => itm.Time >= DateFrom && itm.Time <= DateTo && itm.Type == UserLoginLogType.RegistrationSuccessful).Select(itm => itm.UID).Distinct().Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Users_NewRegistrations, null, null, null, StatisticsDate, lCount);

                // User Login Count
                lCount = DbContext.UserLoginLogs.Where(itm => itm.Time >= DateFrom && itm.Time <= DateTo && itm.Type == UserLoginLogType.LoginSuccessful).Select(itm => itm.UID).Distinct().Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Users_LoginCount, null, null, null, StatisticsDate, lCount);

                // User Count
                lCount = DbContext.UserBases.Count(itm => itm.CreateTime <= DateTo && itm.Status != Kooco.Framework.Models.Enum.UserStatus.Pending);                
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsDate, lCount, false);

                #region for the user count also update current and last week / current and last month / current and last year
                // week
                DateTo = Utils.GetLastDayOfWeek(DateTime.UtcNow.AddDays(-7)).AddHours(24-GlobalConst.LocalTimeOffset).AddMilliseconds(-003);
                long lCountLastWeek = DbContext.UserBases.Count(itm => itm.CreateTime <= DateTo && itm.Status != Kooco.Framework.Models.Enum.UserStatus.Pending);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Week, DateTime.UtcNow, lCount);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Week, DateTo.AddDays(-1), lCountLastWeek);

                // month
                DateTo = Utils.GetLastDayOfMonth(DateTime.UtcNow.AddMonths(-1)).AddHours(24-GlobalConst.LocalTimeOffset).AddMilliseconds(-003);
                long lCountLastMonth = DbContext.UserBases.Count(itm => itm.CreateTime <= DateTo && itm.Status != Kooco.Framework.Models.Enum.UserStatus.Pending);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Month, DateTime.UtcNow, lCount);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Month, DateTo.AddDays(-1), lCountLastMonth);

                // year
                DateTo = Utils.GetLastDayOfYear(DateTime.UtcNow.AddYears(-1)).AddHours(24-GlobalConst.LocalTimeOffset).AddMilliseconds(-003);
                long lCountLastYear = DbContext.UserBases.Count(itm => itm.CreateTime <= DateTo && itm.Status != Kooco.Framework.Models.Enum.UserStatus.Pending);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Year, DateTime.UtcNow, lCount);
                DbContext.SP_SetStatisticsValue((long)StatisticsItems.Users_UserCount, null, null, null, StatisticsPeriodType.Year, DateTo.AddDays(-1), lCountLastYear);

                // retention rate
                DateTime RetentionCanCalculateFromRangeDate = (new DateTime(2017, 10, 20)).Date.AddHours(-(GlobalConst.LocalTimeOffset)); // we don't have a login log before the 2017/11/21, which is needed to calculate the retention rate
                DateTo = OriginalDateTo;

                List<int> RetentionRanges = new List<int>() { 1, 3, 7, 15, 30 };
                foreach (int RetentionRange in RetentionRanges)
                {
                    DateTime RangeFrom = DateFrom.AddDays(-(RetentionRange));    // the retention rate range goes from x days - beginning of the day (db times are UTC - "DateFrom" is already the beginning of the local time day)
                    DateTime RangeTo = DateFrom.AddMilliseconds(-003);           // till the beginning of the date to measure

                    if (RangeFrom >= RetentionCanCalculateFromRangeDate)
                    {
                        long lRegCount = DbContext.UserLoginLogs.Where(itm => itm.Type == UserLoginLogType.RegistrationSuccessful && itm.Time >= RangeFrom && itm.Time <= RangeTo).Count();

                        // because LINQ/EF is incapable of generating good SQL statements when using an IN clause
                        // (it will generate a list of all values in the list and sends this list to server then.. in this case can thousands of values....)
                        // we have to use raw SQL to generate the report here..
                        var lRegUsersLoginCount =
                            DbContext.Database.SqlQuery<long>(@"
                                select Cast(IsNull(Count(1), 0) as bigint)
                                  from [dbo].[UserLoginLogs]
                                 where [Type] = @TypeLogin
                                   and [Time] >= @DateFrom and [Time] <= @DateTo
                                   and [UID] in (
                                        select [UID]
                                          from [dbo].[UserLoginLogs]
                                         where [Type] = @TypeRegistration
                                           and [Time] >= @RangeFrom and [Time] <= @RangeTo)",
                                 new SqlParameter("@TypeLogin", UserLoginLogType.LoginSuccessful),
                                 new SqlParameter("@TypeRegistration", UserLoginLogType.RegistrationSuccessful),
                                 new SqlParameter("@RangeFrom", RangeFrom),
                                 new SqlParameter("@RangeTo", RangeTo),
                                 new SqlParameter("@DateFrom", DateFrom),
                                 new SqlParameter("@DateTo", DateTo)).First();

                        double RetentionRate = 0;
                        if (lRegCount > 0)
                            RetentionRate = Math.Round((double) ((((double)lRegUsersLoginCount) / ((double)lRegCount)) * (double) 100), 1);

                        // TODO -- for 11/21 in prod should be 347 regs and 86 logins inside these regs. but for some reason the RetentionRate value is always 0

                        DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Users_RetentionRate, RetentionRange, null, null, StatisticsDate, RetentionRate, true);
                    }
                }
                #endregion
            }
        }
        #endregion
        #region UpdateLotteryStatistics
        public void UpdateLotteryStatistics(int iDays)
        {
            for (int iDay = 0; iDay < iDays; iDay++)
            {
                DateTime DateFrom = DateTime.Now.Date.AddDays(-(iDay)).AddHours(-(GlobalConst.LocalTimeOffset)); // local time day begin (db times are UTC)
                DateTime DateTo = DateTime.Now.Date.AddDays(-(iDay)).AddHours(24-GlobalConst.LocalTimeOffset).AddMilliseconds(-003);   // local time day end (db times are UTC) - 23:59:59.997 (SQL Server stores only .003 millisecond steps)
                DateTime StatisticsDate = DateTime.Now.AddDays(-(iDay)).Date;

                // Draws Count
                long lCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DateFrom && itm.DrawTime <= DateTo && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_DrawedGames, null, null, null, StatisticsDate, lCount);

                // Players Count
                lCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DateFrom && itm.DrawTime <= DateTo && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery).GroupBy(itm => itm.UID).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_PlayersCount, null, null, null, StatisticsDate, lCount);

                // Won Prices
                List<GroupedStatisticsValue> WonPricesCountList =
                    DbContext.LotteryPriceCoupons
                        .Where(itm => itm.TimeWon >= DateFrom && itm.TimeWon <= DateTo && itm.Status != LotteryPriceCouponStatus.Cancelled && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery)                        
                        .GroupBy(itm => itm.LotteryPriceId)
                        .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                        .ToList();

                foreach(GroupedStatisticsValue StatsValue in WonPricesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_WonPricesCount, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_WonPricesCount, null, null, null, StatisticsDate, WonPricesCountList.Sum(itm => itm.Value));

                // Redeemed Prices
                List<GroupedStatisticsValue> RedeemedPricesCountList =
                    DbContext.LotteryPriceCoupons
                        .Where(itm => itm.TimeRedeemed >= DateFrom && itm.TimeRedeemed <= DateTo && itm.Status == LotteryPriceCouponStatus.PickedUp && itm.LotteryGameId == GlobalConst.LotteryGameId_StandardLottery)
                        .GroupBy(itm => itm.LotteryPriceId)
                        .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                        .ToList();

                foreach (GroupedStatisticsValue StatsValue in RedeemedPricesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_RedeemedPricesCount, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Lottery_RedeemedPricesCount, null, null, null, StatisticsDate, RedeemedPricesCountList.Sum(itm => itm.Value));
            }
        }
        #endregion
        #region UpdateWatersportsStatistics
        public void UpdateWatersportsStatistics(int iDays)
        {
            for (int iDay = 0; iDay < iDays; iDay++)
            {
                DateTime DateFrom = DateTime.Now.Date.AddDays(-(iDay)).AddHours(-(GlobalConst.LocalTimeOffset)); // local time day begin (db times are UTC)
                DateTime DateTo = DateTime.Now.Date.AddDays(-(iDay)).AddHours(24 - GlobalConst.LocalTimeOffset).AddMilliseconds(-003);   // local time day end (db times are UTC) - 23:59:59.997 (SQL Server stores only .003 millisecond steps)
                DateTime StatisticsDate = DateTime.Now.AddDays(-(iDay)).Date;

                // Draws Count
                long lCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DateFrom && itm.DrawTime <= DateTo && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_DrawedGames, null, null, null, StatisticsDate, lCount);

                // Players Count
                lCount = DbContext.LotteryDrawLogs.Where(itm => itm.DrawTime >= DateFrom && itm.DrawTime <= DateTo && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports).GroupBy(itm => itm.UID).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_PlayersCount, null, null, null, StatisticsDate, lCount);

                // Won Prices
                List<GroupedStatisticsValue> WonPricesCountList =
                    DbContext.LotteryPriceCoupons
                        .Where(itm => itm.TimeWon >= DateFrom && itm.TimeWon <= DateTo && itm.Status != LotteryPriceCouponStatus.Cancelled && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports)
                        .GroupBy(itm => itm.LotteryPriceId)
                        .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                        .ToList();

                foreach (GroupedStatisticsValue StatsValue in WonPricesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_WonPricesCount, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_WonPricesCount, null, null, null, StatisticsDate, WonPricesCountList.Sum(itm => itm.Value));

                // Redeemed Prices
                List<GroupedStatisticsValue> RedeemedPricesCountList =
                    DbContext.LotteryPriceCoupons
                        .Where(itm => itm.TimeRedeemed >= DateFrom && itm.TimeRedeemed <= DateTo && itm.Status == LotteryPriceCouponStatus.PickedUp && itm.LotteryGameId == GlobalConst.LotteryGameId_Watersports)
                        .GroupBy(itm => itm.LotteryPriceId)
                        .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                        .ToList();

                foreach (GroupedStatisticsValue StatsValue in RedeemedPricesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_RedeemedPricesCount, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Watersports_RedeemedPricesCount, null, null, null, StatisticsDate, RedeemedPricesCountList.Sum(itm => itm.Value));
            }
        }
        #endregion
        #region UpdateSalesStatistics
        public void UpdateSalesStatistics(int iDays)
        {
            for (int iDay = 0; iDay < iDays; iDay++)
            {
                DateTime DateFrom = DateTime.Now.Date.AddDays(-(iDay)).AddHours(-(GlobalConst.LocalTimeOffset)); // local time day begin (db times are UTC)
                DateTime DateTo = DateTime.Now.Date.AddDays(-(iDay)).AddHours(24 - GlobalConst.LocalTimeOffset).AddMilliseconds(-003);   // local time day end (db times are UTC) - 23:59:59.997 (SQL Server stores only .003 millisecond steps)
                DateTime StatisticsDate = DateTime.Now.AddDays(-(iDay)).Date;

                // Total Sales / Machine
                List<GroupedStatisticsValue> SalesCountList =
                    DbContext.SalesTransactions
                    .Where(itm => itm.CreateTime >= DateFrom && itm.CreateTime <= DateTo && (itm.Status == SalesTransactionStatus.Completed || itm.Status == SalesTransactionStatus.StampsCollected))
                    .GroupBy(itm => itm.VendingMachineId)
                    .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                    .ToList();

                foreach(GroupedStatisticsValue StatsValue in SalesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Sales_SoldItemsCount, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Sales_SoldItemsCount, null, null, null, StatisticsDate, SalesCountList.Sum(itm => itm.Value));

                // App Sales / Machine
                List<GroupedStatisticsValue> AppSalesCountList =
                    DbContext.SalesTransactions
                    .Where(itm => itm.CreateTime >= DateFrom && itm.CreateTime <= DateTo && itm.PurchaseType == PurchaseType.SmartphoneApp && (itm.Status == SalesTransactionStatus.Completed || itm.Status == SalesTransactionStatus.StampsCollected))
                    .GroupBy(itm => itm.VendingMachineId)
                    .Select(itm => new GroupedStatisticsValue() { GroupValue = itm.Key.ToString(), Value = itm.Count() })
                    .ToList();

                foreach (GroupedStatisticsValue StatsValue in AppSalesCountList)
                {
                    DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Sales_SoldItemsCountUsingApp, StatsValue.GroupValue, null, null, StatisticsDate, StatsValue.Value);
                }

                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Sales_SoldItemsCountUsingApp, null, null, null, StatisticsDate, AppSalesCountList.Sum(itm => itm.Value));
            }
        }
        #endregion
        #region UpdateActivityStatistics
        public void UpdateActivityStatistics(int iDays)
        {
            for (int iDay = 0; iDay < iDays; iDay++)
            {
                DateTime DateFrom = DateTime.Now.Date.AddDays(-(iDay)).AddHours(-(GlobalConst.LocalTimeOffset)); // local time day begin (db times are UTC)
                DateTime DateTo = DateTime.Now.Date.AddDays(-(iDay)).AddHours(24 - GlobalConst.LocalTimeOffset).AddMilliseconds(-003);   // local time day end (db times are UTC) - 23:59:59.997 (SQL Server stores only .003 millisecond steps)
                DateTime StatisticsDate = DateTime.Now.AddDays(-(iDay)).Date;

                // Collected Stamps Count
                long lCount = DbContext.UserCouponStamps.Where(itm => itm.Status != UserStampStatus.SentToFriend && itm.DateCollected >= DateFrom && itm.DateCollected <= DateTo).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Activity_CollectedStampsCount, null, null, null, StatisticsDate, lCount);

                // Completed Coupons Count
                lCount = DbContext.UserCoupons.Where(itm => (itm.Status == UserCouponStatus.Completed || itm.Status == UserCouponStatus.Redeemed) && itm.CompletionTime >= DateFrom && itm.CompletionTime <= DateTo).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Activity_CompletedCouponsCount, null, null, null, StatisticsDate, lCount);

                // Redeemed Coupons Count
                lCount = DbContext.UserCoupons.Where(itm => itm.Status == UserCouponStatus.Redeemed && itm.RedeemTime >= DateFrom && itm.RedeemTime <= DateTo).Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Activity_RedeemedCouponsCount, null, null, null, StatisticsDate, lCount);

                // User Amount collecting stamps
                lCount = DbContext.UserCouponStamps.Include(itm => itm.UserCoupon).Where(itm => itm.DateCollected >= DateFrom && itm.DateCollected <= DateTo).Select(itm => itm.UserCoupon.UID).Distinct().Count();
                DbContext.SP_SetStatisticsDayValue((long)StatisticsItems.Activity_UsersCountCollectingStamps, null, null, null, StatisticsDate, lCount);
            }
        }
        #endregion

        #region class: GroupedStatisticsValue
        public class GroupedStatisticsValue
        {
            public string GroupValue { get; set; }

            public DateTime? Date { get; set; }

            public float Value { get; set; }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects.Response;

using VmaBase.Models;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.Enum;
using VmaBase.Shared;

namespace VmaBase.Providers
{
    public class AutoCouponsProvider
    {
        public static DateTime CacheExpiryTime = DateTime.UtcNow;
        public static DynamicFunctionHelper DynamicHelper = null;

        public static DynamicFunctionHelper LastCheckHelper = null;

        private const int LineNumberModifier = -35;

        #region ClassBody
        private const string ClassBody = @"
                using System;
                using System.Net;
                using System.Linq;
                using System.Data.Entity;
                using System.Collections;
                using System.Collections.Generic;

                using VmaBase.Shared;
                using VmaBase.Models;
                using VmaBase.Models.Enum;                
                using VmaBase.Models.DataStorage;
                using VmaBase.Models.DataStorage.Tables;
                using VmaBase.Providers;

                namespace VmaBase.AutoCouponModules
                {
                    public class AutoCouponChecker
                    {
                        public static List<MatchingCriteriaInfo> MeetsCriterias(ApplicationDbContext DbContext, UserData UserDb)
                        {
                            List<MatchingCriteriaInfo> MatchingCriterias = new List<MatchingCriteriaInfo>();
                            List<AutoCouponCriteria> ActiveCriterias = DbContext.AutoCouponCriterias.Where(itm => itm.Status == AutoCouponCriteriaStatus.Active && (itm.UID == null || itm.UID.Value == UserDb.UID) && itm.Id != 1).ToList();
                            long CriteriaId = 0;
                            AutoCouponCriteria Criteria = null;
                            bool AssignCoupon = false;
                            int AssignCouponsCount = 1;

                            DynamicModulesAutoCouponHelper AutoCouponHelper = new DynamicModulesAutoCouponHelper(DbContext, UserDb);

                            ##moduleshere##

                            return MatchingCriterias;
                        }

                        private static void HandleModuleError(long CriteriaId, ApplicationDbContext DbContext, Exception Exc)
                        {
                            Utils.LogError(Exc, ""Error while executing custom code for criteria id "" + CriteriaId.ToString());
                            DbContext.Database.ExecuteSqlCommand(""UPDATE [dbo].[AutoCouponCriterias] SET [Status] = "" + ((int)AutoCouponCriteriaStatus.ModuleInvalid).ToString());
                        }
                    }
                }
            ";
        #endregion

        #region CheckCodeCache
        private static void CheckCodeCache()
        {
            if ((DynamicHelper == null) || (CacheExpiryTime <= DateTime.UtcNow))
            {
                try
                {
                    BuildCheckerClass();
                }
                catch (Exception)
                {
                    // if an error happens -> check all modules for validity and try again
                    CheckModulesValidity();
                    BuildCheckerClass();
                }
            }
        }
        #endregion
        #region BuildCheckerClass
        /// <summary>
        ///     (Re)Builds and compiles the event bonus checker C# code
        ///     
        ///     Returns the name of the messages log filename
        /// </summary>
        private static string BuildCheckerClass()
        {
            string MessagesFilename = GetMessagesFilename();
            DateTime DateNow = DateTime.UtcNow;

            DynamicHelper = new DynamicFunctionHelper();
            DynamicHelper.LineNumberModifier = LineNumberModifier;

            ApplicationDbContext DbContext = new ApplicationDbContext();
            List<AutoCouponCriteria> ActiveCriterias = DbContext.AutoCouponCriterias.Include(itm => itm.Module).Where(itm => itm.Status == Models.Enum.AutoCouponCriteriaStatus.Active && DateNow >= itm.LimitStartTime && DateNow <= itm.LimitEndTime && itm.ModuleId != null && itm.Id != 1).ToList();
            StringBuilder ModuleCodes = new StringBuilder();

            ModuleCodes.AppendLine("AutoCouponHelper.LogfileName = @\"" + MessagesFilename + "\";");
            ModuleCodes.AppendLine();

            foreach (AutoCouponCriteria Criteria in ActiveCriterias)
            {
                if (Criteria.UID != null)
                {
                    ModuleCodes.AppendLine("if (UserDb.UID == " + Criteria.UID.ToString() + ")");
                    ModuleCodes.AppendLine("{");
                }

                ModuleCodes.AppendLine("// Criteria " + Criteria.Id.ToString() + " -- " + Criteria.Name);
                ModuleCodes.AppendLine("CriteriaId = " + Criteria.Id.ToString() + ";");
                ModuleCodes.AppendLine("Criteria = ActiveCriterias.FirstOrDefault(itm => itm.Id == CriteriaId);");
                ModuleCodes.AppendLine("AssignCoupon = false;");
                ModuleCodes.AppendLine("AssignCouponsCount = 1;");
                ModuleCodes.AppendLine("AutoCouponHelper.InitCriteria(CriteriaId);");
                ModuleCodes.AppendLine();
                ModuleCodes.AppendLine("if (Criteria != null)");
                ModuleCodes.AppendLine("{");
                ModuleCodes.AppendLine("  try");
                ModuleCodes.AppendLine("  {");
                ModuleCodes.AppendLine(Criteria.Module.CsharpCode);
                ModuleCodes.AppendLine("    if (AssignCoupon)");
                ModuleCodes.AppendLine("      MatchingCriterias.Add(new MatchingCriteriaInfo(Criteria, AssignCouponsCount));");
                ModuleCodes.AppendLine("    AutoCouponHelper.ApplyChanges();");
                ModuleCodes.AppendLine("  }");
                ModuleCodes.AppendLine("  catch (Exception Exc)");
                ModuleCodes.AppendLine("  {");
                ModuleCodes.AppendLine("    HandleModuleError(CriteriaId, DbContext, Exc);");
                ModuleCodes.AppendLine("  }");
                ModuleCodes.AppendLine("}");
                ModuleCodes.AppendLine("else");
                ModuleCodes.AppendLine("  Console.WriteLine(\"WARNING! The criteria id \" + CriteriaId.ToString() + \" is not active anymore!\");");
                ModuleCodes.AppendLine();

                if (Criteria.UID != null)
                    ModuleCodes.AppendLine("}");
            }

            string CSharpCode = ClassBody.Replace("##moduleshere##", ModuleCodes.ToString());
            if (!DynamicHelper.Compile(CSharpCode, new List<string>() { typeof(System.Net.ServicePoint).Assembly.Location,  // System
                                                                        typeof(System.Data.DataRow).Assembly.Location,                       // System.Data 
                                                                        typeof(System.Net.IPEndPointCollection).Assembly.Location,           // System.Net
                                                                        typeof(System.Dynamic.DynamicObject).Assembly.Location,              // System.Core
                                                                        typeof(AutoCouponsProvider).Assembly.Location,                       // VmaBase
                                                                        typeof(FrameworkResponseDTO).Assembly.Location,                      // Kooco.Framework
                                                                        typeof(System.Data.Entity.DbContext).Assembly.Location,              // EntityFramework
                                                                        typeof(System.Data.Entity.SqlServer.SqlFunctions).Assembly.Location  // EntityFramework.SqlServer
                                                                       },
                                       "VmaBase.AutoCouponModules.AutoCouponChecker"))
                throw new Exception("Error while trying to compile the bonus event modules. The last error was: " + DynamicHelper.LastErrorMessage + ".");

            CacheExpiryTime = DateTime.UtcNow.AddMinutes(ConfigManager.CSharpModulesCacheExpiryTime);

            return MessagesFilename;
        }
        #endregion
        #region CheckModulesValidity
        /// <summary>
        ///     If there is a problem with the loaded modules
        ///     this code will check every single module for problems
        ///     and deactivates invalid modules.
        /// </summary>
        public static void CheckModulesValidity()
        {
            try
            {
                using (ApplicationDbContext DbContext = new ApplicationDbContext())
                {
                    List<AutoCouponModule> ModulesList = DbContext.AutoCouponModules.Where(itm => itm.Status == Models.Enum.CSharpModuleStatus.Active).ToList();
                    bool MeetsCriteria = false;

                    foreach(AutoCouponModule ModuleDb in ModulesList)
                    {
                        if (!CheckModuleCode(1, ModuleDb.CsharpCode, out MeetsCriteria, ConfigManager.CSharpModulesCompileCheckUserId).Ok)
                        {
                            ModuleDb.Status = Models.Enum.CSharpModuleStatus.CompileError;
                            DbContext.SaveChanges();

                            DbContext.Database.ExecuteSqlCommand("UPDATE [dbo].[AutoCouponCriterias] SET [Status] = " + ((int)(AutoCouponCriteriaStatus.ModuleInvalid)).ToString() + " WHERE [ModuleId] = " + ModuleDb.Id.ToString() + " AND [Status] = " + ((int)(AutoCouponCriteriaStatus.Active)).ToString());
                        }
                    }
                }
            }
            catch(Exception Exc)
            {
                Utils.LogError(Exc, "Error in CheckModulesValidity()");
            }
        }
        #endregion

        #region CheckModuleCode
        /// <summary>
        ///     Checks the validity of the given module.
        ///     Returns an empty string if everything is fine.
        /// </summary>
        public static CheckAutoCouponModuleResultDTO CheckModuleCode(long CriteriaId, string ModuleCode, out bool TestuserMeetsCriteria, long TestUserId = 0)
        {
            TestuserMeetsCriteria = false;
            string MessagesFilename = GetMessagesFilename();

            if (CriteriaId <= 0)
                throw new Exception("No CriteriaId given!");


            ApplicationDbContext DbContext = new ApplicationDbContext();
            if (DbContext.AutoCouponCriterias.Where(itm => itm.Id == CriteriaId).Count() <= 0)
                return new CheckAutoCouponModuleResultDTO(false, "Auto coupon criteria does not exist or not saved yet!");

            if (string.IsNullOrWhiteSpace(ModuleCode))
                return new CheckAutoCouponModuleResultDTO(false, "Module code is empty!");

            StringBuilder ModuleCodeBuilder = new StringBuilder();
            ModuleCodeBuilder.AppendLine("AutoCouponHelper.LogfileName = @\"" + MessagesFilename + "\";");
            ModuleCodeBuilder.AppendLine();
            ModuleCodeBuilder.AppendLine("// Auto Coupon Module " + CriteriaId.ToString() + " -- Test Execution");
            ModuleCodeBuilder.AppendLine("CriteriaId = " + CriteriaId.ToString() + ";");
            ModuleCodeBuilder.AppendLine("Criteria = DbContext.AutoCouponCriterias.FirstOrDefault(itm => itm.Id == CriteriaId);");
            ModuleCodeBuilder.AppendLine("AssignCoupon = false;");
            ModuleCodeBuilder.AppendLine("AssignCouponsCount = 1;");
            ModuleCodeBuilder.AppendLine("AutoCouponHelper.InitCriteria(CriteriaId);");
            ModuleCodeBuilder.AppendLine();
            ModuleCodeBuilder.AppendLine(ModuleCode);
            ModuleCodeBuilder.AppendLine();
            ModuleCodeBuilder.AppendLine("AutoCouponHelper.ApplyChanges();");
            ModuleCodeBuilder.AppendLine("if (AssignCoupon)");
            ModuleCodeBuilder.AppendLine("  MatchingCriterias.Add(new MatchingCriteriaInfo(Criteria, AssignCouponsCount));");

            string CSharpCode = ClassBody.Replace("##moduleshere##", ModuleCodeBuilder.ToString());
            object ReturnValue = null;

            UserData UserDb = null;
            if (TestUserId > 0)
                UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).Where(itm => itm.UID == TestUserId).FirstOrDefault();
            else
                UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).Where(itm => itm.UID == ConfigManager.CSharpModulesCompileCheckUserId).FirstOrDefault();

            if (UserDb == null)
                return new CheckAutoCouponModuleResultDTO(false, "Test-User not found!");

            try
            {
                DynamicFunctionHelper TestHelper = new DynamicFunctionHelper();
                TestHelper.LineNumberModifier = LineNumberModifier - 4;
                LastCheckHelper = TestHelper;

                if (!TestHelper.Execute(CSharpCode, new List<string>() { typeof(System.Net.ServicePoint).Assembly.Location,                   // System
                                                                         typeof(System.Data.DataRow).Assembly.Location,                       // System.Data 
                                                                         typeof(System.Net.IPEndPointCollection).Assembly.Location,           // System.Net
                                                                         typeof(System.Dynamic.DynamicObject).Assembly.Location,              // System.Core
                                                                         typeof(AutoCouponsProvider).Assembly.Location,                       // VmaBase
                                                                         typeof(FrameworkResponseDTO).Assembly.Location,                      // Kooco.Framework
                                                                         typeof(System.Data.Entity.DbContext).Assembly.Location,              // EntityFramework
                                                                         typeof(System.Data.Entity.SqlServer.SqlFunctions).Assembly.Location  // EntityFramework.SqlServer
                                                                        },
                                   "VmaBase.AutoCouponModules.AutoCouponChecker", "MeetsCriterias", out ReturnValue, DbContext, UserDb))
                    return new CheckAutoCouponModuleResultDTO(false, "Compile error: " + TestHelper.LastErrorMessage, TestHelper, false, 0);

                if (ReturnValue == null)
                    return new CheckAutoCouponModuleResultDTO(false, "Unexpected result 'null' returned by 'MeetsCriterias'!", TestHelper, false, 0);
                else
                {
                    MatchingCriteriaInfo MatchingCriteria = ((List<MatchingCriteriaInfo>)ReturnValue).FirstOrDefault(itm => itm.Criteria.Id == CriteriaId);
                    TestuserMeetsCriteria = MatchingCriteria != null;

                    string Messages = string.Empty;
                    if (File.Exists(MessagesFilename))
                    {
                        Messages = String.Join("<br/>", File.ReadAllLines(MessagesFilename));
                        File.Delete(MessagesFilename);
                    }

                    CheckAutoCouponModuleResultDTO Result = new CheckAutoCouponModuleResultDTO(true, "", TestHelper, TestuserMeetsCriteria, (MatchingCriteria == null ? 0 : MatchingCriteria.CouponsCount));
                    Result.Messages = Messages;
                    return Result;
                }
            }
            catch (Exception Exc)
            {
                if (File.Exists(MessagesFilename))
                    File.Delete(MessagesFilename);

                return new CheckAutoCouponModuleResultDTO(false, "Error while trying to compile/execute the module code: " + Exc.Message);
            }
        }
        #endregion
        #region CreateApplicableAutoCoupons
        public static int CreateApplicableAutoCoupons(ApplicationDbContext DbContext, UserData UserDb)
        {
            int CouponsReceived = 0;

            CheckCodeCache();

            try
            {
                List<MatchingCriteriaInfo> MatchingCriterias = (List<MatchingCriteriaInfo>)DynamicHelper.ExecuteCompiledMethod("MeetsCriterias", DbContext, UserDb);
                foreach (MatchingCriteriaInfo MatchingCriteria in MatchingCriterias)
                {
                    if (MatchingCriteria.CouponsCount <= 0)
                        MatchingCriteria.CouponsCount = 1;

                    if ((MatchingCriteria.Criteria.CouponsLimit > 0) &&
                        (DbContext.AutoCouponAssignments.Where(itm => itm.CriteriaId == MatchingCriteria.Criteria.Id).Count() >= MatchingCriteria.Criteria.CouponsLimit))
                    {
                        AutoCouponCriteria CriteriaDb = DbContext.AutoCouponCriterias.FirstOrDefault(itm => itm.Id == MatchingCriteria.Criteria.Id);
                        MatchingCriteria.Criteria = null;

                        CriteriaDb.Status = AutoCouponCriteriaStatus.CouponsLimitExceeded;
                        DbContext.SaveChanges();
                        return 0;
                    }
                    else
                    {
                        for (int iNr = 1; iNr <= (MatchingCriteria.CouponsCount); iNr++)
                        {
                            Random rnd = new Random();

                            UserCoupon CouponDb = new UserCoupon();
                            CouponDb.AutoCouponCriteriaId = MatchingCriteria.Criteria.Id;
                            CouponDb.ActivityId = null;
                            CouponDb.CompletedBy = UserDb.UID;
                            CouponDb.CompletionTime = DateTime.UtcNow;
                            CouponDb.ExpiryTime = (MatchingCriteria.Criteria.CouponsExpiryDate != null ? MatchingCriteria.Criteria.CouponsExpiryDate.Value : new DateTime(2099, 12, 31));
                            CouponDb.RedeemTime = null;
                            CouponDb.StartTime = DateTime.UtcNow;
                            CouponDb.Status = UserCouponStatus.Completed;
                            CouponDb.Type = UserCouponType.AutoCoupon;
                            CouponDb.UID = UserDb.UID;
                            DbContext.UserCoupons.Add(CouponDb);

                            AutoCouponAssignment AssignmentDb = new AutoCouponAssignment();
                            AssignmentDb.AssignmentTime = CouponDb.CompletionTime.Value;
                            AssignmentDb.CriteriaId = MatchingCriteria.Criteria.Id;
                            AssignmentDb.UID = UserDb.UID;
                            AssignmentDb.UserCoupon = CouponDb;
                            DbContext.AutoCouponAssignments.Add(AssignmentDb);
                        }

                        DbContext.SaveChanges();

                        CouponsReceived += MatchingCriteria.CouponsCount;
                    }
                }
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error during executing the C# modules for creating auto coupons");
            }

            return CouponsReceived;
        }
        #endregion
        #region CreateAllUsersMissingAutoCoupons
        /// <summary>
        ///     To be run in a batch job (VMAWebJob)
        ///     
        ///     Loops through all users and creates all auto-coupons which not have been created yet.
        /// </summary>
        /// <returns></returns>
        public static int CreateAllUsersMissingAutoCoupons()
        {
            int CouponsCreated = 0;

            using (ApplicationDbContext DbContext = new ApplicationDbContext())
            {
                List<long> UserIdList = DbContext.UserBases.Where(itm => itm.Status == Kooco.Framework.Models.Enum.UserStatus.Normal).Select(itm => itm.UID).ToList();
                int iNr = 0;                

                foreach(long UID in UserIdList)
                {
                    iNr++;
                    if (iNr % 50 == 0)
                        DbContext.Database.ExecuteSqlCommand("SELECT getutcdate()"); // keep DbContext alive

                    UserData UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).Where(itm => itm.UID == UID).FirstOrDefault();
                    if (UserDb != null)
                        CouponsCreated += CreateApplicableAutoCoupons(DbContext, UserDb);
                }
            }

            return CouponsCreated;
        }
        #endregion

        #region GetMessagesFilename
        private static string GetMessagesFilename()
        {
            string Path = ConfigManager.TempDir.Replace("\\\\", "\\");
            if (!Directory.Exists(Path))
                throw new Exception("The temp directory '" + Path + "' does not exist!");

            Random rnd = new Random(545264935);
            int RndNumber = rnd.Next(1000, 9999);
            string Filename = "accdbgmsg_" + DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00") + DateTime.Now.Millisecond.ToString("000") + RndNumber.ToString() + ".log";
            return System.IO.Path.Combine(Path, Filename);
        }
        #endregion
    }
}

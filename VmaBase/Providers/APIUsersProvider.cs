﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;
using Kooco.Framework.Models.Enum;

using AutoMapper;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models;


namespace VmaBase.Providers
{
    public class APIUsersProvider : UsersAPIProviderBase<UserData, UserProfileDTO, ApplicationDbContext>
    {
		private ReturnCodes? HandlerReturnCode = null;
		private string HandlerErrorMessage = string.Empty;
        private APILoginType LoginType = APILoginType.UserPwd;

        public BaseResponseDTO LoginUser(GetLoginAPIParamsDTO LoginParams)
        {
            if (LoginParams == null)
                return Error(ReturnCodes.MissingArgument, "No arguments given or JSON invalid!");

            LoginType = LoginParams.LoginType;

            FrameworkResponseDTO Response = base.LoginUser(LoginParams, BeforeSuccessfulLogin);
            return new BaseResponseDTO(Response);
        }

        private bool BeforeSuccessfulLogin(UserBase UserDb)
        {
            DbContext.SP_CheckExpiredPoints(UserDb.UID);

            UserData UserDataDb = DbContext.UserDatas.Include(itm => itm.UserBase).Where(itm => itm.UID == UserDb.UID).FirstOrDefault();
            if (UserDataDb != null)
            {
                UserDataDb.LastLoginType = LoginType;
                DbContext.SaveChanges();

                AutoCouponsProvider.CreateApplicableAutoCoupons(DbContext, UserDataDb);
            }            

            return true;
        }
		
		public BaseResponseDTO RegisterUser(UserProfileDTO UserProfile)	
		{
            HandlerReturnCode = null;
            HandlerErrorMessage = string.Empty;

			FrameworkResponseDTO MaximaResponse = base.RegisterUser(UserProfile, BeforeRegistrationSave);
			BaseResponseDTO Response = new BaseResponseDTO(MaximaResponse);
            if (HandlerReturnCode != null)
                Response.SetError(HandlerReturnCode.Value, HandlerErrorMessage);

            if (Response.ReturnCode == ReturnCodes.Success)
            {
                Kooco.Framework.Models.RegistrationInfo RegInfo = (Kooco.Framework.Models.RegistrationInfo)MaximaResponse.AdditionalData;                
                if ((RegInfo != null) && (RegInfo.UserData != null))
                {
                    if (RegInfo.IsNewRegistration)
                    {
                        UserData UserDb = (UserData)RegInfo.UserData;
                        DbContext.SP_AddUserPoints(UserDb.UID, 100, Models.Enum.PointsTransactionType.RegistrationReward, null, null, null);
                        AutoCouponsProvider.CreateApplicableAutoCoupons(DbContext, UserDb);
                    }
                    else
                    {
                        UserData UserDb = (UserData)RegInfo.UserData;
                        AutoCouponsProvider.CreateApplicableAutoCoupons(DbContext, UserDb);
                    }
                }
                else
                    Utils.LogError("Could not add or check for adding 100 points to registered user, because no user record or additional data has been returned by the Maxima RegisterUser API! (could be auto-login registration)");
            }

			return Response;
		}

		private bool BeforeRegistrationSave(ref UserData UserDataDb, UserProfileDTO UserProfile)		
		{
            UserDataDb.PhoneNrSearch7 = ((!string.IsNullOrWhiteSpace(UserProfile.Phone)) ? (UserProfile.Phone.Trim().Length > 7 ? UserProfile.Phone.Trim().Substring(UserProfile.Phone.Length - 7, 7) : UserProfile.Phone.Trim()) : string.Empty);
            UserDataDb.Birthday = UserProfile.Birthday;
            UserDataDb.Location = Utils.MaxLengthString(UserProfile.Location, UserData.LocationMaxLength);
            UserDataDb.City = Utils.MaxLengthString(UserProfile.City, UserData.CityMaxLength);
            UserDataDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation(UserProfile.Longitude, UserProfile.Latitude);
            UserDataDb.PushToken = UserProfile.PushToken;
            UserDataDb.Status = Models.Enum.UserDataStatus.Normal;
            UserDataDb.MacauPassId = UserProfile.MacauPassId;
            UserDataDb.ImageUrl = Utils.MaxLengthString(UserProfile.ImageUrl, UserData.ImageUrlMaxLength);
            UserDataDb.Gender = (UserProfile.Gender == null ? Models.Enum.GenderTypes._None : UserProfile.Gender.Value);

            if (LotteryGameProvider.IsGameActive(GlobalConst.LotteryGameId_StandardLottery))
            {
                if (DateTime.UtcNow >= DbContext.LotteryGames.FirstOrDefault(itm => itm.Id == GlobalConst.LotteryGameId_StandardLottery).StartTime)
                {
                    UserLotteryCoin InitialCoins = new UserLotteryCoin();
                    InitialCoins.DateAquired = DateTime.UtcNow;
                    InitialCoins.Amount = 3;
                    InitialCoins.ExpiryDate = new DateTime(2099, 12, 31);
                    InitialCoins.LotteryGameId = GlobalConst.LotteryGameId_StandardLottery;
                    InitialCoins.UserData = UserDataDb;
                    InitialCoins.OriginalAmount = 3;
                    InitialCoins.Status = Models.Enum.LotteryCoinStatus.Available;
                    InitialCoins.Type = Models.Enum.LotteryCoinType.Daily;
                    DbContext.UserLotteryCoins.Add(InitialCoins);
                }
            }

            if ((UserDataDb.Birthday != null) && (UserDataDb.Birthday < new DateTime(1850, 1, 1)))
            {
                HandlerErrorMessage = "The birthday '" + UserDataDb.Birthday.Value.ToShortDateString() + "' is invalid. The birthday can not be a value before 1850/1/1!";
                HandlerReturnCode = ReturnCodes.InvalidArguments;
                return false;
            }

            if (!string.IsNullOrWhiteSpace(UserProfile.AccountIdRecommendedBy))
                return CheckRecommendedUser(UserProfile.AccountIdRecommendedBy, UserDataDb);

            return true; // true if the user should be saved, false if save should be cancelled (RegisterUser will return FrameworkReturnCodes.RegistrationNotPossible then)
        }

        public class FriendUserInfo
        {
            public long UID { get; set; }
            public string Nickname { get; set; }
        }

        private bool CheckRecommendedUser(string accountIdRecommendedBy, UserData UserDataDb)
        {
            // TODO maybe change to a stored procedure with locking the user record

            FriendUserInfo FriendInfo = DbContext.UserBases.Where(itm => itm.Account == accountIdRecommendedBy && itm.Status != UserStatus.Blocked).Select(itm => new FriendUserInfo() { UID = itm.UID, Nickname = itm.Nickname }).FirstOrDefault();
            if ((FriendInfo == null) || (FriendInfo.UID <= 0))
            {
                HandlerReturnCode = ReturnCodes.UserNotFound;
                HandlerErrorMessage = "The given recommended account id does not exist!";
                return false;
            }

            if ((UserDataDb.RecommendedByUID != null) && (UserDataDb.RecommendedByUID > 0))
            {
                HandlerReturnCode = ReturnCodes.AlreadyRecommended;
                return false;
            }

            if (FriendInfo.UID == UserDataDb.UID)
            {
                HandlerReturnCode = ReturnCodes.CannotRecommendYourself;
                return false;
            }

            UserData UserDataDbFriend = DbContext.UserDatas.Find(FriendInfo.UID);

            if (LotteryGameProvider.IsGameActive(GlobalConst.LotteryGameId_StandardLottery))
            {
                if (DateTime.UtcNow >= DbContext.LotteryGames.FirstOrDefault(itm => itm.Id == GlobalConst.LotteryGameId_StandardLottery).StartTime)
                {
                    UserLotteryCoin Coin = new UserLotteryCoin();
                    Coin.Amount = 1;
                    Coin.DateAquired = DateTime.UtcNow;
                    Coin.ExpiryDate = new DateTime(2099, 12, 31);
                    Coin.LotteryGameId = GlobalConst.LotteryGameId_StandardLottery;
                    Coin.UID = UserDataDbFriend.UID;
                    Coin.OriginalAmount = 1;
                    Coin.Type = Models.Enum.LotteryCoinType.Earned;
                    DbContext.UserLotteryCoins.Add(Coin);


                    LotteryCoinLog CoinLogDb = new LotteryCoinLog();
                    CoinLogDb.TransactionTime = DateTime.UtcNow;
                    CoinLogDb.Type = Models.Enum.LotteryCoinLogType.RecommendedFriend;
                    CoinLogDb.UID = UserDataDbFriend.UID;
                    CoinLogDb.FriendUserData = UserDataDb;
                    CoinLogDb.Coins = 1;
                    DbContext.LotteryCoinLogs.Add(CoinLogDb);
                }
            }

            UserDataDb.RecommendedByUID = UserDataDbFriend.UID;

            PushNotificationProvider.Current.SendTextNotification(UserDataDbFriend.UID, "你的好友完成註冊了", "你的好友 " + FriendInfo.Nickname + " 完成註冊了。你收到了一個硬幣。快去試試你的好手氣！", PushAction.Create(Models.Enum.PushActionCode.RecommendedFriendRegistered, UserDataDbFriend.UID.ToString()));

            return true;
        }

        /// <summary>
        ///    This will activate a user currently in pending state,
        ///    if SMS registration is activated. The verficiation code has to be sent to this function in Params.VerifyToken
        ///    (like for SMS login)
        /// </summary>
        public BaseResponseDTO ActivatePendingUser(ActivateUserParamsDTO Params)
		{
            FrameworkResponseDTO Response = base.ActivatePendingUser(Params);
            if (Response.ReturnCode == Kooco.Framework.Shared.FrameworkReturnCodes.Success)
            {
                UserBase UserDb = (UserBase)Response.AdditionalData;
                if (UserDb != null)
                    DbContext.SP_AddUserPoints(UserDb.UID, 100, Models.Enum.PointsTransactionType.RegistrationReward, null, null, null);

                if (Params.AutoLogin)
                    AutoCouponsProvider.CreateApplicableAutoCoupons(DbContext, DbContext.UserDatas.Find(UserDb.UID));
            }
			return new BaseResponseDTO(Response);		
		}

		public BaseResponseDTO UpdateUser(UserProfileDTO UserProfile)
		{
            HandlerReturnCode = null;
            HandlerErrorMessage = string.Empty;

			FrameworkResponseDTO MaximaResponse = base.UpdateUser(UserProfile, BeforeUserUpdateSave);
			BaseResponseDTO Response = new BaseResponseDTO(MaximaResponse);
            if (HandlerReturnCode != null)
                Response.SetError(HandlerReturnCode.Value, HandlerErrorMessage);

			return Response;		
		}

		private bool BeforeUserUpdateSave(ref UserData UserDataDb, UserProfileDTO UserProfile)		
		{
            long UID = UserDataDb.UID;

            if (UserProfile.Phone != null && UserProfile.Phone.Trim() == string.Empty)
                return HandlerError(ReturnCodes.InvalidArguments, "The parameter 'phone' can not be an empty string!");

            UserDataDb.MacauPassId = (UserProfile.MacauPassId == null ? UserDataDb.MacauPassId : UserProfile.MacauPassId);
            UserDataDb.PhoneNrSearch7 = (UserProfile.Phone == null ? UserDataDb.PhoneNrSearch7 : (UserProfile.Phone.Trim().Length > 7 ? UserProfile.Phone.Trim().Substring(UserProfile.Phone.Length - 7, 7) : UserProfile.Phone.Trim()));
            UserDataDb.Birthday = (UserProfile.Birthday == null ? UserDataDb.Birthday : UserProfile.Birthday);
            UserDataDb.City = (UserProfile.City == null ? UserDataDb.City : Utils.MaxLengthString(UserProfile.City, 50));
            UserDataDb.Location = (UserProfile.Location == null ? UserDataDb.Location : Utils.MaxLengthString(UserProfile.Location, UserData.LocationMaxLength));
            UserDataDb.PushToken = (UserProfile.PushToken == null ? UserDataDb.PushToken : UserProfile.PushToken);
            UserDataDb.GeoLocation = (UserProfile.Longitude == null || UserProfile.Latitude == null ? UserDataDb.GeoLocation : Utils.LongitudeLatitudeToDbLocation(UserProfile.Longitude, UserProfile.Latitude));
            UserDataDb.ImageUrl = (UserProfile.ImageUrl == null ? UserDataDb.ImageUrl : Utils.MaxLengthString(UserProfile.ImageUrl, UserData.ImageUrlMaxLength));
            UserDataDb.Gender = (UserProfile.Gender == null ? UserDataDb.Gender : UserProfile.Gender.Value);
            UserDataDb.ModifyTime = DateTime.UtcNow;

            if (!string.IsNullOrWhiteSpace(UserProfile.AccountIdRecommendedBy))
                CheckRecommendedUser(UserProfile.AccountIdRecommendedBy, UserDataDb);

            return true; 
		}

        public BaseResponseDTO GetUser(GetUserParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (((Params.UID == null) || (Params.UID <= 0)) &&
                (string.IsNullOrWhiteSpace(Params.Account)) &&
                (string.IsNullOrWhiteSpace(Params.UserToken)) &&
                (string.IsNullOrWhiteSpace(Params.Phone)))
                return new BaseResponseDTO(Error(Kooco.Framework.Shared.FrameworkReturnCodes.MissingArgument, "One of the parameters 'userId', 'account', 'userToken' or 'phone' must be set!"));

            UserData UserDb = null;
            if ((Params.UID != null) && (Params.UID > 0))
                UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UID == Params.UID.Value);
            else if (! string.IsNullOrWhiteSpace(Params.Account))
                UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.Account == Params.Account);
            else if (! string.IsNullOrWhiteSpace(Params.Phone))
                UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.Phone == Params.Phone);
            else
            {
                switch(Params.SocialLoginType)
                {
                    case Kooco.Framework.Models.Enum.SocialLoginTypes.WeChat:
                        UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.WechatAccount == Params.UserToken);
                        break;
                    case Kooco.Framework.Models.Enum.SocialLoginTypes.Facebook:
                        UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.FacebookAccount == Params.UserToken);
                        break;
                    case Kooco.Framework.Models.Enum.SocialLoginTypes.Google:
                        UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.GoogleAccount == Params.UserToken);
                        break;
                    case Kooco.Framework.Models.Enum.SocialLoginTypes.Twitter:
                        UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.TwitterAccount == Params.UserToken);
                        break;
                    case Kooco.Framework.Models.Enum.SocialLoginTypes.QQ:
                        UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(usr => usr.UserBase.QQAccount == Params.UserToken);
                        break;
                    default:
                        return new BaseResponseDTO(Error(Kooco.Framework.Shared.FrameworkReturnCodes.InvalidSocialLoginType));
                }
            }

            if (UserDb == null)
                return new BaseResponseDTO(Error(Kooco.Framework.Shared.FrameworkReturnCodes.UserNotFound));

            SentToFriendInfoDTO Info = null;
            if (Params.AuthenticatedUser.UID == UserDb.UID)
                Info = new SentToFriendInfoDTO();
            else
                Info = new SentToFriendInfoDTO(DbContext, Params.AuthenticatedUser.UID, UserDb.UID);

            UserProfileDTO UserDTO = Mapper.Map<UserData, UserProfileDTO>(UserDb);
            UserDTO.LoginType = UserDb.LastLoginType;
            UserDTO.SentToPoints = Info.Points;
            UserDTO.SentToStamps = Info.Stamps;
            return ResultObject(UserDTO);
        }

        public BaseResponseDTO SearchUsersByPhoneNr(SearchUsersByPhoneParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            List<string> lstPhoneNrSearch7 = Params.PhoneNumbers.Select(itm => itm.Length > 7 ? itm.Substring(itm.Length - 7, 7) : itm).ToList();
            List<UserBase> UserListDb = DbContext.SP_GetUsersByPhoneNumbersSearch7(Params.AuthenticatedUser.UID, lstPhoneNrSearch7);
            return ResultObject(UserListDb.Select(Mapper.Map<UserBase, UserSearchResultDTO>).ToList());
        }

        public BaseResponseDTO AddSocialLogin(AddRemoveSocialLoginAPIParamsDTO<ApplicationDbContext> Params)
        {
            return new BaseResponseDTO(base.AddSocialLogin(Params));
        }

        /// <summary>
        ///     Updates only the location of a user
        /// </summary>
        public BaseResponseDTO UpdateLocation(UpdateLocationParamsDTO UserProfile)
        {
            BaseResponseDTO Response = UserProfile.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if ((UserProfile.Latitude == null) || (UserProfile.Longitude == null) || (UserProfile.Latitude.Value == 0) || (UserProfile.Longitude.Value == 0))
                return new BaseResponseDTO(Error(Kooco.Framework.Shared.FrameworkReturnCodes.MissingArgument, "For setting the location please provide both parameters 'Latitude' and 'Longitude'!"));

            UserData UserDb = DbContext.UserDatas.Find(UserProfile.AuthenticatedUser.UID);
            if (UserDb == null)
                return new BaseResponseDTO(Error(Kooco.Framework.Shared.FrameworkReturnCodes.RecordNotFound, "This user is not an API user!"));
            UserDb.GeoLocation = Utils.LongitudeLatitudeToDbLocation(UserProfile.Longitude, UserProfile.Latitude);
            DbContext.SaveChanges();

            return Ok();
        }

        public BaseResponseDTO GetPointsList(BaseAPIAuthParamsDTO Params)
        {
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            DbContext.SP_CheckExpiredPoints(Params.AuthenticatedUser.UID);

            UserData UserDb = DbContext.UserDatas.Find(Params.AuthenticatedUser.UID);
            if (UserDb == null)
                return Error(ReturnCodes.UserNotFound);

            PointListDTO PointList = new PointListDTO();
            PointList.UID = Params.AuthenticatedUser.UID;
            PointList.UserAccount = Params.AuthenticatedUser.Account;
            PointList.UserName = Params.AuthenticatedUser.Name;
            PointList.PointsTotal = (long) UserDb.PointsTotal;
            PointList.PointsList = (from userpoints in DbContext.UserPoints
                                    where userpoints.UID == Params.AuthenticatedUser.UID &&
                                          userpoints.Status == Models.Enum.UserPointStatus.Available
                                    group userpoints by new { userpoints.DateAquired.Year, userpoints.DateAquired.Month } into upgroup
                                    orderby upgroup.Key.Year descending, upgroup.Key.Month descending
                                    select new PointMonthInfoDTO
                                    {
                                        Month = upgroup.Key.Month,
                                        Year = upgroup.Key.Year,
                                        ExpiryDate = upgroup.OrderBy(itm => itm.ExpiryDate).FirstOrDefault().ExpiryDate,                                        
                                        Points = upgroup.Sum(itm => itm.Amount)
                                    }).ToList();

            foreach (PointMonthInfoDTO MonthInfo in PointList.PointsList)
                MonthInfo.MonthTerm = GlobalConst.CultureEN.DateTimeFormat.GetMonthName(MonthInfo.Month).Substring(0, 3).ToUpper();

            return ResultObject(PointList);
        }

        public BaseResponseDTO SendPointsToFriend(SendPointsToFriendParamsDTO Params)
        {
            bool bCommit = false;
            BaseResponseDTO Response = Params.CheckAppAuthorization(this);
            if (Response.ReturnCode != ReturnCodes.Success)
                return Response;

            if (((Params.FriendUID == null) || (Params.FriendUID <= 0)) &&
                (string.IsNullOrWhiteSpace(Params.FriendAccount)))
                return Error(ReturnCodes.MissingArgument, "One of the parameters 'friendUserId' or 'friendUserAccount' must be set!");

            if (Params.Points <= 0)
                return Error(ReturnCodes.InvalidArguments, "The parameter 'points' can not be less than or equals 0!");

            UserData FriendUserDb = null;
            if ((Params.FriendUID != null) && (Params.FriendUID > 0))
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UID == Params.FriendUID && itm.UserBase.Status != Kooco.Framework.Models.Enum.UserStatus.Blocked);
            else
                FriendUserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UserBase.Account == Params.FriendAccount && itm.UserBase.Status != Kooco.Framework.Models.Enum.UserStatus.Blocked);
            if (FriendUserDb == null)
                return Error(ReturnCodes.UserNotFound, "The friend user can not be found!");

            if (FriendUserDb.UserBase.Status == Kooco.Framework.Models.Enum.UserStatus.Banned)
                return Error(ReturnCodes.UserBlocked, "This friend user has been banned!");

            int iReturnCode = DbContext.SP_SendPoints(Params.AuthenticatedUser.UID, FriendUserDb.UID, Params.Points);
            if (iReturnCode != 1)
                return Error((ReturnCodes) iReturnCode);

            long CurrentPoints = DbContext.SP_CheckExpiredPoints(Params.AuthenticatedUser.UID);

            PushNotificationProvider.Current.SendTextNotification(FriendUserDb.UID, "你的朋友送給你積分了", "你的朋友 " + Params.AuthenticatedUser.Nickname + " 剛剛送給你" + Params.Points.ToString() + "點積分.",
                PushAction.Create(Models.Enum.PushActionCode.PointsReceived, FriendUserDb.UID.ToString()));

            return ResultObject(new SentToFriendInfoDTO(DbContext, Params.AuthenticatedUser.UID, FriendUserDb.UID));
        }

        public BaseResponseDTO TestAddPoints(TestAddPointsParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            UserData UserDb = DbContext.UserDatas.Find(Params.UID);
            if (UserDb == null)
                return Error(ReturnCodes.UserNotFound, "There is no user with the UID " + Params.UID.ToString() + "!");

            PointsTransaction TransactionDb = new PointsTransaction();
            TransactionDb.Amount = Params.Points;
            TransactionDb.TransactionType = Models.Enum.PointsTransactionType.TestPointsAdded;
            TransactionDb.UID = Params.UID;
            DbContext.PointsTransactions.Add(TransactionDb);

            UserDb.PointsTotal += Params.Points;
            DbContext.SaveChanges();

            return Ok();
        }

        public new BaseResponseDTO GetSessionInfo(BaseAPIAuthParamsDTO Params)
        {
            FrameworkResponseDTO Response = base.GetSessionInfo(Params);
            return new BaseResponseDTO(Response);
        }

        public BaseResponseDTO RefreshSessionToken(BaseAPIAuthParamsDTO Params)
        {
            FrameworkResponseDTO Response = base.RefreshSessionToken(Params);
            return new BaseResponseDTO(Response);
        }

        #region SendTestPushNotification
        public BaseResponseDTO SendTestPushNotification(SendTestPushNotificationParamsDTO Params)
        {
            if (!Params.IsAppTokenValid())
                return Error(ReturnCodes.InvalidAPIToken);

            if ((Params.UID <= 0) && (Params.UID != -100) && (string.IsNullOrWhiteSpace(Params.Account)))
                return Error(ReturnCodes.MissingArgument, "At least one of the parameters 'UID' or 'Account' must be given!");

            UserData UserDb = null;
            if (Params.UID >= 0)
                UserDb = DbContext.UserDatas.Find(Params.UID);
            else
            {
                if (!string.IsNullOrWhiteSpace(Params.Account))
                    UserDb = DbContext.UserDatas.Include(itm => itm.UserBase).FirstOrDefault(itm => itm.UserBase.Account == Params.Account);
            }

            if ((UserDb == null) && (Params.UID != -100))
                return Error(ReturnCodes.UserNotFound);

            if (Params.UID == -100)
                Kooco.Framework.Providers.PushNotificationProvider.Current.BroadcastTextNotificationToAllUsers(Params.Title, Params.Message);
            else
                Kooco.Framework.Providers.PushNotificationProvider.Current.SendTextNotification(UserDb.UID, Params.Title, Params.Message);

            return ResultObject(Kooco.Framework.Providers.PushNotificationProvider.Current.LastResponseText);
        }
        #endregion           

        #region Error
        public BaseResponseDTO Error(ReturnCodes ErrorCode, string ErrorMessage)
        {
            return new BaseResponseDTO(ErrorCode, ErrorMessage);
        }
        public BaseResponseDTO Error(ReturnCodes ErrorCode)
        {
            return new BaseResponseDTO(ErrorCode);
        }
        #endregion
        #region Ok
        public new BaseResponseDTO Ok()
        {
            return new BaseResponseDTO(ReturnCodes.Success);
        }
        #endregion
        #region ResultObject
        public BaseResponseDTO ResultObject(object resultObject, int? RecordsCount = null, int? TotalPages = null, bool ContentsCut = false, string DebugMessage = null)
        {
            BaseResponseDTO Response = new BaseResponseDTO(resultObject);
            if (RecordsCount != null)
                Response.Count = RecordsCount;
            if (TotalPages != null)
                Response.TotalPages = TotalPages;
            if (ContentsCut)
                Response.ReturnCode = ReturnCodes.ContentsCut;
            if (!string.IsNullOrWhiteSpace(DebugMessage))
                Response.ErrorMessage = DebugMessage;
            return Response;
        }
        #endregion

        #region HandlerError
        /// <summary>
        ///     Helping method for handler functions to return an error
        /// </summary>
        private bool HandlerError(ReturnCodes ReturnCode, string ErrorMessage)
        {
            HandlerReturnCode = ReturnCode;
            HandlerErrorMessage = ErrorMessage;
            return false;
        }

        private bool HandlerError(ReturnCodes ReturnCode)
        {
            return HandlerError(ReturnCode, string.Empty);
        }
        #endregion
    }
}

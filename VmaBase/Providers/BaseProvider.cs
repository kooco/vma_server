﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VmaBase.Shared;
using VmaBase.Models.DataStorage;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaBase.Providers
{
    public class BaseProvider : Kooco.Framework.Providers.KoocoBaseProvider<ApplicationDbContext>
    {
		public new ApplicationDbContext DbContext
		{
			get
			{
				return (ApplicationDbContext)base.DbContext;
			}
		}

        // Response Helpers

        #region Ok
        public new BaseResponseDTO Ok()
        {
            return new BaseResponseDTO(ReturnCodes.Success);
        }
        #endregion
        #region Error
        public BaseResponseDTO Error(ReturnCodes ErrorCode)
        {
            return new BaseResponseDTO(ErrorCode);
        }
        public BaseResponseDTO Error(ReturnCodes ErrorCode, string sErrorMessage)
        {
            return new BaseResponseDTO(ErrorCode, sErrorMessage);
        }
        #endregion
        #region OperationPending
        public new BaseResponseDTO OperationPending()
        {
            return new BaseResponseDTO(ReturnCodes.OperationPending);
        }
        public new BaseResponseDTO OperationPending(string Message)
        {
            BaseResponseDTO Response = new Models.DataTransferObjects.Response.BaseResponseDTO(ReturnCodes.OperationPending);
            Response.ErrorMessage = Message;
            return Response;
        }
        #endregion
        #region ResultObject
        public new BaseResponseDTO ResultObject(object objResult)
        {
            return new BaseResponseDTO(objResult);
        }
        public BaseResponseDTO ResultObject(object objResult, string DebugMessage)
        {
            BaseResponseDTO Response = new BaseResponseDTO(objResult);
            Response.ErrorMessage = DebugMessage;
            return Response;
        }
        #endregion
        #region ResultObject
        public new BaseResponseDTO ResultObject(object objResult, int? TotalPages)
        {
            BaseResponseDTO Response = new BaseResponseDTO(objResult);
            Response.TotalPages = TotalPages;
            return Response;
        }
        public BaseResponseDTO ResultObject(object objResult, int? TotalPages, string DebugMessage)
        {
            BaseResponseDTO Response = new BaseResponseDTO(objResult);
            Response.TotalPages = TotalPages;
            Response.ErrorMessage = DebugMessage;
            return Response;
        }
        public BaseResponseDTO ResultObject(object objResult, int? TotalPages = null, int? RecordsCount = null, bool ContentsCut = false, string DebugMessage = "")
        {
            BaseResponseDTO Response = new BaseResponseDTO(objResult);
            Response.TotalPages = TotalPages;
            Response.Count = RecordsCount;
            Response.ErrorMessage = DebugMessage;
            if (ContentsCut)
                Response.ReturnCode = ReturnCodes.ContentsCut;
            return Response;
        }
        #endregion
    }
}

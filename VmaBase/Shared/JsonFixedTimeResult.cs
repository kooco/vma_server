﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace VmaBase.Shared
{
    public class JsonFixedTimeResult : Kooco.Framework.Shared.JsonFixedTimeResult
    {
        #region Constructor
        public JsonFixedTimeResult() : base()
        {
        }

        public JsonFixedTimeResult(object data) : base(data)
        {
        }

        public JsonFixedTimeResult(object data, JsonRequestBehavior requestBehavior) : base(data, requestBehavior)
        {
        }
        #endregion
    }
}

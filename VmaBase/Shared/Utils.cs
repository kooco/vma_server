using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Spatial;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;

using Newtonsoft.Json;

using VmaBase.Models.Enum;
using VmaBase.Models;


namespace VmaBase.Shared
{
    public class Utils : Kooco.Framework.Shared.Utils
    {
        private static Random random = null;

        #region LongitudeLatitudeToDbLocation
        public static DbGeography LongitudeLatitudeToDbLocation(decimal? Longitude, decimal? Latitude)
        {
            if ((Latitude != null) && (Latitude.Value != 0) && (Longitude != null) && (Longitude.Value != 0))
            {
                string sLocationString = string.Format(GlobalConst.CultureEN, "POINT({0} {1})", Longitude, Latitude);
                return DbGeography.PointFromText(sLocationString, GlobalConst.CoordinateSystemId);
            }
            else
                return null;
        }
        #endregion
        #region GetPointsExpiryDate
        public static DateTime GetPointsExpiryDate(DateTime date)
        {
            DateTime ExpiryDate = date;

            switch (ConfigManager.UserPointsExpiryPeriod)
            {
                case GlobalConst.PointsExpiryPeriod_EndOfDay:
                    date = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
                    break;
                case GlobalConst.PointsExpiryPeriod_EndOfWeek:
                    while (date.DayOfWeek != DayOfWeek.Sunday)
                        date = date.AddDays(1);
                    date = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
                    break;
                case GlobalConst.PointsExpiryPeriod_EndOfMonth:
                    date = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.DaysInMonth(DateTime.UtcNow.Year, DateTime.UtcNow.Month));
                    break;
                default:
                    throw new Exception("Invalid application setting value '" + ConfigManager.UserPointsExpiryPeriod + "' for the AppSetting 'UserPointsExpiryPeriod'!");
            }

            return ExpiryDate.AddMonths(ConfigManager.UserPointsExpiryInMonths);
        }
        #endregion
        #region GetWatersportsCoinsExpiryDate
        public static DateTime GetWatersportsCoinsExpiryDate(DateTime date)
        {
            return date.AddDays(120);
        }
        #endregion
        #region MOCCPVendingApi_DESDecrypt
        public static string MOCCPVendingApi_DESDecrypt(string DecryptStr)
        {
            DESCryptoServiceProvider DESProvider = new DESCryptoServiceProvider();
            byte[] key = Encoding.ASCII.GetBytes(GlobalConst.MOCCPVendingApi_DESKey);
            byte[] iv = Encoding.ASCII.GetBytes(GlobalConst.MOCCPVendingApi_DESIV);
            DESProvider.Key = key;
            DESProvider.IV = iv;

            byte[] output = Convert.FromBase64String(DecryptStr);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, DESProvider.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(output, 0, output.Length);
                    cs.FlushFinalBlock();
                    return Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }
        #endregion
        #region BuyBuyAESEncrypt
        public static string BuyBuyAESEncrypt(string raw)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                ICryptoTransform e = GetAESCryptoTransform(csp, true);
                byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
                byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);

                string encrypted = Convert.ToBase64String(output);

                return encrypted;
            }
        }

        public static string BuyBuyAESDecrypt(string encrypted)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                var d = GetAESCryptoTransform(csp, false);
                byte[] output = Convert.FromBase64String(encrypted);
                byte[] decryptedOutput = d.TransformFinalBlock(output, 0, output.Length);

                string decypted = Encoding.UTF8.GetString(decryptedOutput);
                return decypted;
            }
        }

        private static ICryptoTransform GetAESCryptoTransform(AesCryptoServiceProvider csp, bool encrypting)
        {
            csp.Mode = CipherMode.CBC;
            csp.Padding = PaddingMode.PKCS7;
            var passWord = GlobalConst.BuyBuyAESKey;
            var salt = "S@1tS@1t";

            //a random Init. Vector. just for testing
            String iv = GlobalConst.BuyBuyAESIV;

            var spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(passWord), Encoding.UTF8.GetBytes(salt), 1000); // 65536
            byte[] key = spec.GetBytes(16);


            csp.IV = Encoding.UTF8.GetBytes(iv);
            csp.Key = key;
            if (encrypting)
            {
                return csp.CreateEncryptor();
            }
            return csp.CreateDecryptor();
        }
        #endregion
        #region DecryptParams
        public static T DecryptParams<T>(string ParamsString)
            where T : class
        {
            T Object = null;
            string sDecrypted = Utils.BuyBuyAESDecrypt(ParamsString);
            Object = JsonConvert.DeserializeObject<T>(sDecrypted);

            return Object;
        }
        #endregion

        #region GenerateQR
        /// <summary>
        ///     Generates a unique "secret" QR which contains the given
        ///     action type and record id
        /// </summary>
        public static string GenerateQR(QRActionType ActionType, string RecordId)
        {
            string QRToken = "-" + ((int)ActionType).ToString("0") + RecordId;

            if (random == null)
                random = new Random();

            QRToken = DateTime.Now.Millisecond.ToString("000") +
                      random.Next(100000, 999999).ToString() +
                      RecordId.Length.ToString("00") + QRToken;

            return QRToken;
        }
        #endregion
        #region GetQRContent
        public static QRActionInfo GetQRContent(string QRToken)
        {
            QRActionInfo actionInfo = new QRActionInfo();
            actionInfo.IsValid = false;

            if (QRToken.Length >= 14)
            {
                if (QRToken.Substring(11, 1) == "-")
                {
                    int iLength = 0;
                    int iQRType = 0;
                    QRActionType actionType = QRActionType._None;

                    if ((int.TryParse(QRToken.Substring(9, 2), out iLength)) && ((int.TryParse(QRToken.Substring(12, 1), out iQRType))))
                    {
                        if (Enum.TryParse<QRActionType>(iQRType.ToString(), out actionType))
                        {
                            actionInfo.ActionType = actionType;
                            actionInfo.Id = QRToken.Substring(13);
                            if (actionInfo.Id.Length == iLength)
                                actionInfo.IsValid = true;
                        }
                    }
                }
            }

            return actionInfo;
        }
        #endregion
        #region GetTestQRImage
        public static byte[] GetTestQRImage(string QRToken)
        {
            MessagingToolkit.QRCode.Codec.QRCodeEncoder encoder = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            Bitmap QRBitmap = encoder.Encode(QRToken);

            using (MemoryStream stream = new MemoryStream())
            {
                QRBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
        #endregion

        #region DebugShowTimeDifference
        public static void DebugShowTimeDifference(ref DateTime startDate, string Message)
        {
            Debug.WriteLine(Message + " - " + (DateTime.Now - startDate).TotalSeconds.ToString() + " seconds");
            Console.WriteLine(Message + " - " + (DateTime.Now - startDate).TotalSeconds.ToString() + " seconds");
            startDate = DateTime.Now;
        }
        #endregion

        public static Dictionary<TKey, TValue> LinqKeyValueListToDictionary<TKey, TValue>(List<LinqKeyValuePair<TKey, TValue>> KeyValueList)
        {
            Dictionary<TKey, TValue> Dict = new Dictionary<TKey, TValue>();
            foreach (LinqKeyValuePair<TKey, TValue> Pair in KeyValueList)
                Dict.Add(Pair.Key, Pair.Value);

            return Dict;
        }

        #region GetParameterListFromString
        /// <summary>
        ///     Converts a standardized parameter string into a key-value dictionary.
        ///     
        ///     The string must have the following format:
        ///         name=value;name2=value2;...
        ///         
        ///     The value can contain escaped ";" characters by using ;;
        /// </summary>
        /// <param name="Parameters"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetParameterListFromString(string Parameters, bool AllKeysLowerCase = false)
        {
            string sParameters = Parameters.Replace(";;", "\001");
            string[] sParamsList = sParameters.Split(';');
            Dictionary<string, string> KeyValuePairs = new Dictionary<string, string>();
            int iIndex = 0;
            string sKey = string.Empty;
            string sValue = string.Empty;

            foreach (string sParam in sParamsList)
            {
                iIndex = sParam.IndexOf("=");
                if (iIndex > 0)
                {
                    sKey = sParam.Substring(0, iIndex).Trim();
                    if (AllKeysLowerCase)
                        sKey = sKey.ToLower();

                    if (sParam.Length > iIndex + 1)
                        sValue = sParam.Substring(iIndex + 1).Replace("\001", ";");
                    else
                        sValue = string.Empty;

                    KeyValuePairs.Add(sKey, sValue);
                }
            }

            return KeyValuePairs;
        }
        #endregion
    }
}

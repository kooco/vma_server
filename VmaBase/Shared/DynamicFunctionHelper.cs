﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CSharp;

namespace VmaBase.Shared
{
    public class DynamicFunctionHelper
    {
        public string LastErrorMessage { get; set; }
        public List<string> LastErrorMessages { get; set; }
        public bool LastExecutionOk { get; set; }

        public string CompiledNamespace { get; set; }
        public Assembly CompiledAssembly { get; set; }
        public MethodInfo CompiledMethod { get; set; }

        public int LineNumberModifier { get; set; }

        #region Execute
        public bool Execute(string CSharpCode, List<string> AssemblyReferencesToAdd, string Namespace, string FunctionName, out object ReturnParameter, params object[] Parameters)
        {
            return Execute(CSharpCode, AssemblyReferencesToAdd, Namespace, FunctionName, out ReturnParameter, false, Parameters);
        }

        public bool Compile(string CSharpCode, List<string> AssemblyReferencesToAdd, string Namespace)
        {
            object dummy = null;
            return Execute(CSharpCode, AssemblyReferencesToAdd, Namespace, string.Empty, out dummy, true);
        }

        private bool Execute(string CSharpCode, List<string> AssemblyReferencesToAdd, string Namespace, string FunctionName, out object ReturnParameter, bool CompileOnly, params object[] Parameters)
        {
            CSharpCodeProvider CSharpProvider = new CSharpCodeProvider();
            CompilerParameters CompileParameters = new CompilerParameters();

            if (AssemblyReferencesToAdd != null)
            {
                foreach (string AssemblyReference in AssemblyReferencesToAdd)
                    CompileParameters.ReferencedAssemblies.Add(AssemblyReference);
            }

            CompileParameters.GenerateExecutable = false;
            CompileParameters.GenerateInMemory = true;
            ReturnParameter = null;

            LastErrorMessages = new List<string>();
            LastErrorMessage = string.Empty;
            LastExecutionOk = false;

            CompilerResults Results = CSharpProvider.CompileAssemblyFromSource(CompileParameters, CSharpCode);
            if (Results.Errors.Count > 0)
            {
                LastErrorMessage = "in line #" + (Results.Errors[Results.Errors.Count - 1].Line + LineNumberModifier).ToString() + ": " + Results.Errors[Results.Errors.Count - 1].ErrorText;
                foreach (CompilerError Error in Results.Errors)
                    LastErrorMessages.Add("in line #" + (Results.Errors[Results.Errors.Count - 1].Line + LineNumberModifier).ToString() + ": " + Results.Errors[Results.Errors.Count - 1].ErrorText);

                return false;
            }    

            if (Results.CompiledAssembly != null)
            {
                Type DynamicNamespace = Results.CompiledAssembly.GetType(Namespace);
                if (DynamicNamespace != null)
                {
                    CompiledNamespace = Namespace;

                    if (CompileOnly)
                    {
                        CompiledAssembly = Results.CompiledAssembly;
                        return true;
                    }
                    else
                    {
                        MethodInfo Method = DynamicNamespace.GetMethod(FunctionName);
                        if (Method != null)
                        {
                            try
                            {
                                ReturnParameter = Method.Invoke(null, Parameters);
                                CompiledAssembly = Results.CompiledAssembly;
                                CompiledMethod = Method;
                                LastExecutionOk = true;
                            }
                            catch(Exception Exc)
                            {
                                string ErrorMessage = "Runtime error:\n" + Exc.Message + "\n" + Exc.StackTrace;
                                Exception InnerExc = Exc.InnerException;
                                while (InnerExc != null)
                                {
                                    ErrorMessage += "\n\nINNER EXCEPTION:\n" + InnerExc.Message + "\n" + InnerExc.StackTrace;
                                    InnerExc = InnerExc.InnerException;
                                }

                                LastErrorMessage = ErrorMessage;
                                LastErrorMessages.Add(LastErrorMessage);

                                return false;
                            }

                            return true;
                        }
                        else
                        {
                            LastErrorMessage = "Can not find the method '" + FunctionName + "' in the dynamic C# code in the namespace '" + Namespace + "'!";
                            LastErrorMessages.Add(LastErrorMessage);
                            return false;
                        }
                    }
                }
                else
                {
                    LastErrorMessage = "Can not find the namespace '" + Namespace + "' in the dynamic C# code!";
                    LastErrorMessages.Add(LastErrorMessage);
                    return false;
                }
            }
            else
            {
                LastErrorMessage = "Unknown Error! Code has not been compiled for unknown reasons! (maybe the code block is empty?)";
                LastErrorMessages.Add(LastErrorMessage);
                return false;
            }
        }
        #endregion
        #region ExecuteCompiledMethod
        public object ExecuteCompiledMethod(string MethodName, params object[] Parameters)
        {
            if (CompiledAssembly == null)
                throw new Exception("No dynamic C# code has been compiled so far!");

            Type DynamicNamespace = CompiledAssembly.GetType(CompiledNamespace);
            if (DynamicNamespace != null)
            {
                MethodInfo Method = DynamicNamespace.GetMethod(MethodName);
                if (Method != null)
                    return Method.Invoke(null, Parameters);
                else
                    throw new Exception("Can not find the method '" + MethodName + "' in the compiled C# code in the namespace '" + CompiledNamespace + "'!");
            }
            else
                throw new Exception("Can not find the namespace '" + CompiledNamespace + "' in the compiled C# code!");
        }
        #endregion

        public DynamicFunctionHelper()
        {
            LastExecutionOk = false;
            LastErrorMessage = string.Empty;
            LastErrorMessages = new List<string>();

            CompiledAssembly = null;
            CompiledMethod = null;
        }
    }
}

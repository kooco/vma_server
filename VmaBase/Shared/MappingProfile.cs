﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Kooco.Framework.Models.DataStorage.Tables;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataStorage.Tables;
using VmaBase.Models.DataStorage.Resultsets;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.Enum;

namespace VmaBase.Shared
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserData, UserProfileDTO>()
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.UserBase.Account))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UserBase.Email))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserBase.Name))
                .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.UserBase.Nickname))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.UserBase.Phone))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.GeoLocation != null ? src.GeoLocation.Latitude : null))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.GeoLocation != null ? src.GeoLocation.Longitude : null));

            CreateMap<UserData, UserProfileAdminDTO>()
                .ForMember(dest => dest.ButtonsId, opt => opt.MapFrom(src => src.UID))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Account : string.Empty))
                .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Name : string.Empty))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Email : string.Empty))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Phone : string.Empty))
                .ForMember(dest => dest.LastLoginTime, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.LastLoginTime : (DateTime?)null))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Status : Kooco.Framework.Models.Enum.UserStatus._None));

            CreateMap<UserBase, UserSearchResultDTO>();
            CreateMap<UserFriend, UserProfileDTO>()
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.FriendUser.UserBase.Account))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.FriendUser.UserBase.Email))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FriendUser.UserBase.Name))
                .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.FriendUser.UserBase.Nickname))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.FriendUser.UserBase.Phone));
            CreateMap<UserFriend, UserSearchResultDTO>()
                .ForMember(dest => dest.UID, opt => opt.MapFrom(src => src.FriendUser.UID))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.FriendUser.UserBase.Account))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.FriendUser.UserBase.Email))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FriendUser.UserBase.Name))
                .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.FriendUser.UserBase.Nickname))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.FriendUser.UserBase.Phone))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.FriendUser.ImageUrl));

            // Coupons, Stamps, Activities
            CreateMap<StampType, StampTypeAdminDTO>();
            CreateMap<StampType, StampTypeDTO>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.Number.ToString("00")));

            CreateMap<CouponStampDefinition, CouponStampDTO>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Number.ToString("00") : "00")))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Name : "")));

            CreateMap<CouponStampDefinition, StampTypeDTO>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Number.ToString("00") : "00")))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Name : "")));

            CreateMap<UserCouponStamp, StampTypeDTO>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Number.ToString("00") : "00")))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => (src.StampType != null ? src.StampType.Name : "")));

            CreateMap<CouponActivity, CouponDTO>()
                .ForMember(dest => dest.ProductUniqueNumber, opt => opt.MapFrom(src => (src.Product != null ? src.Product.MOCCPGoodsId : "")))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => (src.Product != null ? src.Product.Name : "")))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => (src.Product != null ? src.Product.ImageUrl : "")));
            CreateMap<CouponActivity, CouponActivityDTO>()
                .ForMember(dest => dest.ActivityId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ProductUniqueNumber, opt => opt.MapFrom(src => (src.Product != null ? src.Product.MOCCPGoodsId : "")))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => (src.Product != null ? src.Product.Name : "")))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => (src.Product != null ? src.Product.ImageUrl : "")));
            CreateMap<CouponActivity, CouponActivityListEntryDTO>();

            CreateMap<UserCoupon, CouponDTO>()
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(src => src.ExpiryTime))
                .ForMember(dest => dest.ActivityText, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.ActivityText : (src.AutoCouponCriteria != null ? src.AutoCouponCriteria.CouponsSubTitle : string.Empty)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.Title : (src.AutoCouponCriteria != null ? src.AutoCouponCriteria.CouponsTitle : string.Empty)))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.ProductId : null))
                .ForMember(dest => dest.ProductUniqueNumber, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.MOCCPGoodsId : string.Empty))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.Name : string.Empty))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.ImageUrl : string.Empty));

            CreateMap<UserCoupon, CompletedCouponDTO>()
                .ForMember(dest => dest.UID, opt => opt.MapFrom(src => src.UID))
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(src => src.ExpiryTime))
                .ForMember(dest => dest.ActivityText, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.ActivityText : (src.AutoCouponCriteria != null ? src.AutoCouponCriteria.CouponsSubTitle : string.Empty)))
                .ForMember(dest => dest.CriteriaId, opt => opt.MapFrom(src => src.AutoCouponCriteriaId))
                .ForMember(dest => dest.SendAllowed, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.SendingCouponsAllowed : true))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.Title : (src.AutoCouponCriteria != null ? src.AutoCouponCriteria.CouponsTitle : string.Empty)))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.ProductId : null))
                .ForMember(dest => dest.ProductUniqueNumber, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.MOCCPGoodsId : null))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.Name : null))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.Activity != null && src.Activity.Product != null ? src.Activity.Product.ImageUrl : null))
                .ForMember(dest => dest.ReceivedByFriend, opt => opt.MapFrom(src => src.SenderUID != null))
                .ForMember(dest => dest.ReceivedByUID, opt => opt.MapFrom(src => src.SenderUID != null ? (long?) src.SenderUserData.UID : null))
                .ForMember(dest => dest.ReceivedByNickname, opt => opt.MapFrom(src => src.SenderUID != null && src.SenderUserData != null && src.SenderUserData.UserBase != null ? src.SenderUserData.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.ReceivedByAccount, opt => opt.MapFrom(src => src.SenderUID != null && src.SenderUserData != null && src.SenderUserData.UserBase != null ? src.SenderUserData.UserBase.Account : string.Empty));

            CreateMap<UserCoupon, UserCouponAdminDTO>()
                .ForMember(dest => dest.CriteriaId, opt => opt.MapFrom(src => src.AutoCouponCriteriaId))
                .ForMember(dest => dest.ActivityName, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.Title : (src.AutoCouponCriteria != null ? src.AutoCouponCriteria.Name : string.Empty)))
                .ForMember(dest => dest.ExpiryTime, opt => opt.MapFrom(src => src.Activity != null ? src.Activity.RedeemEndTime : (src.AutoCouponCriteria != null ? (src.AutoCouponCriteria.CouponsExpiryDate != null ? src.AutoCouponCriteria.CouponsExpiryDate.Value : src.AutoCouponCriteria.LimitEndTime) : new DateTime(2099, 12, 31))))
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Account : string.Empty))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Name : string.Empty))
                .ForMember(dest => dest.SenderUserAccount, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Account : string.Empty))
                .ForMember(dest => dest.SenderUserName, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Name : string.Empty))
                .ForMember(dest => dest.SenderUserNickname, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Nickname : string.Empty));

            CreateMap<AutoCouponCriteria, AutoCouponCriteriaAdminDTO>()
                .ForMember(dest => dest.ModuleName, opt => opt.MapFrom(src => src.Module != null ? src.Module.Name : string.Empty))
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Account : string.Empty))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Nickname : string.Empty));

            CreateMap<AutoCouponCriteria, AutoCouponCriteriaListEntry>();

            CreateMap<AutoCouponCriteriaProduct, CouponCriteriaProductAdminDTO>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty))
                .ForMember(dest => dest.MOCCPOrganization, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPOrganization : string.Empty))
                .ForMember(dest => dest.Volume, opt => opt.MapFrom(src => src.Product != null ? src.Product.Volume : null))
                .ForMember(dest => dest.InternalId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPInternalId : null))
                .ForMember(dest => dest.MerchandiseId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPGoodsId : string.Empty))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.Product != null ? src.Product.ImageUrl : "/images/noimage.png"))
                .ForMember(dest => dest.StampTypeId1, opt => opt.MapFrom(src => src.Product != null ? src.Product.DefaultStampTypeId : null))
                .ForMember(dest => dest.StampType1ImageUrl, opt => opt.MapFrom(src => src.Product != null && src.Product.DefaultStampType != null ? src.Product.DefaultStampType.ImageUrl : "/images/noimage.png"))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Product != null ? src.Product.Status : VendingProductStatus.NotAvailable));

            CreateMap<AutoCouponCriteriaMachine, CouponCriteriaMachineAdminDTO>()
                .ForMember(dest => dest.VendingMachineId, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.Id : 0))
                .ForMember(dest => dest.VendingMachineName, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.Name : string.Empty))
                .ForMember(dest => dest.MOCCPOrganization, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.MOCCPOrganization : string.Empty))
                .ForMember(dest => dest.MOCCPAssetNo, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.MOCCPAssetNo : null))
                .ForMember(dest => dest.VendingMachineLocation, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.Location : null))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Machine != null ? src.Machine.Status : VendingMachineStatus.NotReady));

            CreateMap<AutoCouponModule, AutoCouponModuleAdminListDTO>();

            CreateMap<AcquireOrderStampsResultDTO, CouponSelectionEntryDTO>();

            CreateMap<CouponActivity, CouponActivityAdminDTO>()
                .ForMember(dest => dest.ActivityId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ActivityParticipatingProduct, ActivityParticipatingProductAdminDTO>()
                .ForMember(dest => dest.Organization, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPOrganization : string.Empty))
                .ForMember(dest => dest.MerchandiseId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPGoodsId : string.Empty))
                .ForMember(dest => dest.InternalId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPInternalId : 0))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.Product != null ? src.Product.ImageUrl : string.Empty))
                .ForMember(dest => dest.StampType1Name, opt => opt.MapFrom(src => src.StampType1 != null ? src.StampType1.Name : string.Empty))
                .ForMember(dest => dest.StampType1ImageUrl, opt => opt.MapFrom(src => src.StampType1 != null ? src.StampType1.ImageUrl : string.Empty));

            CreateMap<VendingProduct, ActivityParticipatingProductAdminDTO>()
                .ForMember(dest => dest.InternalId, opt => opt.MapFrom(src => src.MOCCPInternalId))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.MerchandiseId, opt => opt.MapFrom(src => src.MOCCPGoodsId))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status == Models.Enum.VendingProductStatus.Available ? Kooco.Framework.Models.Enum.GeneralStatusEnum.Active : Kooco.Framework.Models.Enum.GeneralStatusEnum.Inactive))
                .ForMember(dest => dest.StampTypeId1, opt => opt.MapFrom(src => src.DefaultStampType != null ? src.DefaultStampType.Id : -1))
                .ForMember(dest => dest.StampType1Name, opt => opt.MapFrom(src => src.DefaultStampType != null ? src.DefaultStampType.Name : string.Empty))
                .ForMember(dest => dest.StampType1ImageUrl, opt => opt.MapFrom(src => src.DefaultStampType != null ? src.DefaultStampType.ImageUrl : string.Empty));

            CreateMap<VendingProductType, VendingProductTypeAdminDTO>();
            CreateMap<VendingProductBrand, VendingProductBrandAdminDTO>();

            CreateMap<VendingProductAdminDTO, VendingProductActivityAdminDTO>();
            CreateMap<VendingProductActivityAdminDTO, VendingProductAdminDTO>();                

            CreateMap<ActivityParticipatingMachine, ActivityParticipatingMachineAdminDTO>()
                .ForMember(dest => dest.AppStatus, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.AppStatus : VendingMachineAppStatus.Show))
                .ForMember(dest => dest.VendingMachineName, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.Name : string.Empty))
                .ForMember(dest => dest.VendingMachineLocation, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.Location : string.Empty))
                .ForMember(dest => dest.MOCCPAssetNo, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.MOCCPAssetNo : string.Empty));

            CreateMap<VendingMachine, ActivityParticipatingMachineAdminDTO>()
                .ForMember(dest => dest.VendingMachineId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VendingMachineName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.VendingMachineLocation, opt => opt.MapFrom(src => src.Location))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status == Models.Enum.VendingMachineStatus.Active ? Kooco.Framework.Models.Enum.GeneralStatusEnum.Active : Kooco.Framework.Models.Enum.GeneralStatusEnum.Inactive));

            // Points and Promotions
            CreateMap<PromotionProduct, PromotionProductAdminDTO>();

            CreateMap<PromotionProduct, PromotionProductDTO>()
                .ForMember(dest => dest.Available, opt => opt.MapFrom(src => src.Status == Kooco.Framework.Models.Enum.GeneralStatusEnum.Active))
                .ForMember(dest => dest.ProductNumber, opt => opt.MapFrom(src => src.ProductNumber.ToString("00")));

            CreateMap<PromotionOrder, PromotionOrderDTO>()
                .ForMember(dest => dest.ProductNumber, opt => opt.MapFrom(src => src.Product != null ? src.Product.ProductNumber.ToString("00") : string.Empty))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => src.Product != null ? src.Product.ImageUrl : string.Empty))
                .ForMember(dest => dest.PayAdditional, opt => opt.MapFrom(src => src.Product != null ? src.Product.PayAdditional : -1));

            CreateMap<PromotionOrder, PromotionOrderAdminDTO>()
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Account : string.Empty))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Name : string.Empty))
                .ForMember(dest => dest.UserAddress, opt => opt.MapFrom(src => src.User != null ? src.User.Location : string.Empty))
                .ForMember(dest => dest.UserPhone, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Phone : string.Empty))
                .ForMember(dest => dest.PromotionProductId, opt => opt.MapFrom(src => src.ProductId))
                .ForMember(dest => dest.PromotionProductName, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty))
                .ForMember(dest => dest.PromotionProductImageUrl, opt => opt.MapFrom(src => src.Product != null ? src.Product.ImageUrl : string.Empty))
                .ForMember(dest => dest.PayAdditional, opt => opt.MapFrom(src => src.Product != null ? src.Product.PayAdditional : -1));

            // Vending Products and Machines
            CreateMap<VendingMachine, VendingMachineDTO>();
            CreateMap<VendingMachineDTO, VendingMachine>();
            CreateMap<VendingMachine, VendingMachineAdminDTO>()
                .ForMember(dest => dest.Organization, opt => opt.MapFrom(src => src.MOCCPOrganization))
                .ForMember(dest => dest.AssetNo, opt => opt.MapFrom(src => src.MOCCPAssetNo))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.GeoLocation != null ? src.GeoLocation.Latitude : 0))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.GeoLocation != null ? src.GeoLocation.Longitude : 0));

            CreateMap<VendingProduct, VendingProductDTO>()
                .ForMember(dest => dest.Available, opt => opt.MapFrom(src => src.Status == Models.Enum.VendingProductStatus.Available))
                .ForMember(dest => dest.UniqueNumber, opt => opt.MapFrom(src => src.MOCCPGoodsId));
            CreateMap<VendingProduct, VendingProductAdminDTO>()
                .ForMember(dest => dest.DefaultStampTypeImageUrl, opt => opt.MapFrom(src => src.DefaultStampType != null ? src.DefaultStampType.ImageUrl : "/images/noimage.png"));
            CreateMap<VendingProductDTO, VendingProduct>();

            CreateMap<VendingProductActivitySetting, VendingProductActivityAdminDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AveragePrice, opt => opt.MapFrom(src => src.Product != null ? src.Product.AveragePrice : 0))
                .ForMember(dest => dest.DefaultStampTypeId, opt => opt.MapFrom(src => src.Product != null ? src.Product.DefaultStampTypeId : null))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Product != null ? src.Product.ImageUrl : "images/noimage.png"))
                .ForMember(dest => dest.MOCCPGoodsId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPGoodsId : null))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty))
                .ForMember(dest => dest.NameEN, opt => opt.MapFrom(src => src.Product != null ? src.Product.NameEN : string.Empty))
                .ForMember(dest => dest.PointsExtra, opt => opt.MapFrom(src => src.Product != null ? src.Product.PointsExtra : 0))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Product != null ? src.Product.Status : VendingProductStatus.NotAvailable))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Product != null ? src.Product.ProductTypeId : 0))
                .ForMember(dest => dest.Volume, opt => opt.MapFrom(src => src.Product != null ? src.Product.Volume : string.Empty))
                .ForMember(dest => dest.DefaultStampTypeImageUrl, opt => opt.MapFrom(src => src.Product != null && src.Product.DefaultStampType != null ? src.Product.DefaultStampType.ImageUrl : "/images/noimage.png"));

            CreateMap<SalesList, SalesListEntryDTO>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => (src.Product != null ? src.Product.Name : "")))
                .ForMember(dest => dest.ProductImageUrl, opt => opt.MapFrom(src => (src.Product != null ? src.Product.ImageUrl : "")));

            CreateMap<PointsTransaction, PointsLogAdminDTO>()
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Account : string.Empty))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.TransactionTime, opt => opt.MapFrom(src => src.CreateTime))
                .ForMember(dest => dest.TransactionTime, opt => opt.MapFrom(src => PointsLogAdminDTO.GetPointsTransactionText(src)));

            CreateMap<SalesTransaction, SalesTransactionAdminDTO>()
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Account : "沒用手機"))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Name : ""))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.User != null && src.User.UserBase != null ? src.User.UserBase.Account : ""))
                .ForMember(dest => dest.VendingMachineName, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.Name : string.Empty))
                .ForMember(dest => dest.VendingMachineAssetNo, opt => opt.MapFrom(src => src.VendingMachine != null ? src.VendingMachine.MOCCPAssetNo : string.Empty))
                .ForMember(dest => dest.ProductMerchandiseId, opt => opt.MapFrom(src => src.Product != null ? src.Product.MOCCPGoodsId : string.Empty))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product != null ? src.Product.Name : string.Empty));

            // Lottery
            CreateMap<LotteryGame, LotteryGameAdminDTO>();

            CreateMap<LotteryPrice, LotteryPriceItemDTO>()
                .ForMember(dest => dest.PriceId, opt => opt.MapFrom(src => src.Id));

            CreateMap<LotteryPriceCoupon, LotteryPriceCouponAdminDTO>()
                .ForMember(dest => dest.UserAccount, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Account : string.Empty))
                .ForMember(dest => dest.UserNickname, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Nickname : string.Empty))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Name : string.Empty))
                .ForMember(dest => dest.UserPhone, opt => opt.MapFrom(src => src.UserData != null && src.UserData.UserBase != null ? src.UserData.UserBase.Phone : string.Empty))
                .ForMember(dest => dest.UserAddress, opt => opt.MapFrom(src => src.UserData != null ? src.UserData.Location : string.Empty))
                .ForMember(dest => dest.PriceTitle, opt => opt.MapFrom(src => src.Price != null ? src.Price.Title : string.Empty))
                .ForMember(dest => dest.PriceImageUrl, opt => opt.MapFrom(src => src.Price != null ? src.Price.ImageUrl : string.Empty))
                .ForMember(dest => dest.PriceExchangeType, opt => opt.MapFrom(src => src.Price != null ? (src.Price.UseActivityCoupon ? LotteryPriceExchangeType.VendingMachineProduct : LotteryPriceExchangeType.PickupProduct) : LotteryPriceExchangeType._None));

            CreateMap<LotteryPriceAvailabilityItem, LotteryPriceItemAdminDTO>()
                .ForMember(dest => dest.PriceTitle, opt => opt.MapFrom(src => src.Price != null ? src.Price.Title : string.Empty))
                .ForMember(dest => dest.PriceImageUrl, opt => opt.MapFrom(src => src.Price != null ? src.Price.ImageUrl : string.Empty))
                .ForMember(dest => dest.IsWon, opt => opt.MapFrom(src => src.WonByUID != null ? true : false))
                .ForMember(dest => dest.WonByAccount, opt => opt.MapFrom(src => src.WonByUser != null && src.WonByUser.UserBase != null ? src.WonByUser.UserBase.Account : string.Empty))
                .ForMember(dest => dest.WonByNickname, opt => opt.MapFrom(src => src.WonByUser != null && src.WonByUser.UserBase != null ? src.WonByUser.UserBase.Nickname : string.Empty));

// ##section[NEWSMODULE] begin - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
			VmaBase.Shared.MaximaNewsModule.NewsModuleAutoMapper.InitializeNewsModuleMapper(this); // auto-generated. please do NOT modify or remove!
// ##section[NEWSMODULE] end - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace VmaBase.Shared
{
    /// <summary>
    ///     contains all standard error codes used in the Kooco.Framework
	///		you may add other error codes to this class
	///		But please take care if updating the EFTemplate package!
    /// </summary>
    public enum ReturnCodes
    {
        NoData = 0,
        Success = 1,
        Redirect = 2,
        OperationPending = 3,
        [DefaultErrorMessage("Operation successfully cancelled")]
        CancelledSuccesfully = 5,
        [DefaultErrorMessage("Operation successul, but contents have been cut because it has exceeded the maximum allowed length")]
        ContentsCut = 10,
        // Friend List (VMA)
        [DefaultErrorMessage("There is already a pending friend request for this user!")]
        PendingFriendRequest = -100,
        [DefaultErrorMessage("A request for adding this user as a friend already have been denied!")]
        FriendRequestDeclined = -101,
        [DefaultErrorMessage("The QR token given can not be recognized. No action can be executed, because the action is unknown.")]
        InvalidQRcode = -102,
        [DefaultErrorMessage("The user can not recommend him/herself!")]
        CannotRecommendYourself = -103,
        [DefaultErrorMessage("This user already has a recommendedBy user set! It can not be set again!")]
        AlreadyRecommended = -104,
        [DefaultErrorMessage("This QR code already has been used!")]
        QRAlreadyUsed = -105,
        // Stamps (VMA)
        [DefaultErrorMessage("Stickers of this coupon can not be sent to the friend user, because this coupon is already completed or redeemed!")]
        CouponAlreadyCompleted = -201,
        [DefaultErrorMessage("This coupon can not be used (anymore), because it has been disabled!")]
        CouponDisabled = -202,
        [DefaultErrorMessage("This coupon can not be used anymore, because it's already expired!")]
        CouponExpired = -203,
        [DefaultErrorMessage("The stickers can not be sent to the friend user, because the friend's coupon already is complete or even redeemed!")]
        FriendCouponAlreadyCompleted = -204,
        [DefaultErrorMessage("No stickers can be sent to the friend user, because the friend's coupon has been disabled!")]
        FriendCouponDisabled = -205,
        [DefaultErrorMessage("This user didn't collect any stamps yet for this coupon!")]
        CouponNoStampsYet = -206,
        [DefaultErrorMessage("This user does not have enough stamps to send to a friend!")]
        CouponNotEnoughStamps = -207,
        [DefaultErrorMessage("Can not send stamps to the user him/herself!")]
        CanNotSendStampToSelf = -208,
        [DefaultErrorMessage("This coupon has not yet been completed and can not be used to send to a friend or to redeem it!")]
        CouponNotCompleted = -209,
        [DefaultErrorMessage("This coupon already has been redeemed and can not be used anymore!")]
        CouponAlreadyRedeemed = -210,
        [DefaultErrorMessage("There is no coupon for this user for this activity with the status 'Collecting'! Either the activity is disabled or collecting has not yet been started for this activity!")]
        NoStartedCoupon = -211,
        [DefaultErrorMessage("There is no completed coupon for this user and this activity which could be used for redeem!")]
        NoCompletedCoupon = -212,
        [DefaultErrorMessage("Sending coupons to friends has been disabled for this activity and therefore is not allowed!")]
        SendingCouponsNotAllowed = -213,
        [DefaultErrorMessage("It is not possible to collect or send stamps for this activity, because collecting stamps has been disabled for this activity!")]
        StampsDisabled = -214,
        [DefaultErrorMessage("This friend already has been added to the friend list")]
        FriendAlreadyAdded = -215,
        [DefaultErrorMessage("This activity is currently edited by another user! Please wait until the other user has sved or cancelled his changes.")]        
        ActivityProcessingByOtherUser = -299,
        // vending, points, exchange products
        [DefaultErrorMessage("The user does not have enough points to complete this transaction!")]
        NotEnoughPoints = -300,
        [DefaultErrorMessage("This product is not available!")]
        ProductNotAvailable = -301,
        [DefaultErrorMessage("There are not enough items of this product in stock! Product temporarily not available!")]
        ProductNotEnoughInStock = -302,
        [DefaultErrorMessage("This sales transaction already has been cancelled because of a timeout!")]
        SalesTransactionCancelled = -303,
        [DefaultErrorMessage("This sales transaction already have been completed!")]
        SalesTransactionAlreadyCompleted = -304,
        [DefaultErrorMessage("Points could be added to the user account")]
        PointsCouldNotBeAdded = -305,
        [DefaultErrorMessage("The last order message still has not been processed by this machine (machine did not receive the last order so far)")]
        VendingMachineHasOpenOrderMessage = -306,
        [DefaultErrorMessage("This vending machine still is processing another order which has net yet been completed")]
        VendingMachineAlreadyHasOrder = -307,
        [DefaultErrorMessage("This QR code is invalid or already has been used for collecting stamps!")]
        StampsQRInvalid = -308,
        [DefaultErrorMessage("This QR code does not match the activity given or there is no stamp available for this machine/product combination!")]
        StampsQRNotMatchingActivity = -309,
        [DefaultErrorMessage("This vending machine is currently not available or does not respond!")]
        VendingMachineNotAvailable = -310,
        [DefaultErrorMessage("This vending machine is not valid for this activity! (It's not part of this activity)")]
        VendingMachineNotValidForActivity = -311,
        [DefaultErrorMessage("This product is already part of another activity (which also has one of these machines added) for this activity and can not be added anymore this this activity!")]
        ProductNotValidForActivity = -312,
        [DefaultErrorMessage("This order already has been picked up!")]
        OrderAlreadyPickedUp = -313,
        [DefaultErrorMessage("This order already is expired and can not be marked as 'picked up' anymore!")]
        OrderExpired = -314,
        [DefaultErrorMessage("This order already has been cancelled/refund. The user already got the points back for this order. Therefore this order can not be marked as 'picked up'!")]
        OrderAlreadyRefund = -315,
        [DefaultErrorMessage("No matching product can be found at this vending machine (no product that can be redeemed for this prize at this machine)!")]
        NoMatchingProduct = -316,
        [DefaultErrorMessage("The selected product can not be redeemed with this price!")]
        ProductSelectionInvalid = -317,
        // lottery gaming
        [DefaultErrorMessage("There is more than one lottery game currently active. Please use the 'lotteryGameId' or 'activityId' to choose the lottery game you want to have the game info for.")]
        MoreActiveLotteryGames = -400,
        [DefaultErrorMessage("This lottery game has been disabled and can currently not be used!")]
        LotteryGameInactive = -401,
        [DefaultErrorMessage("This lottery game already has been finished (end date already reached). No draws can be done anymore and no coupons of this lottery game can be used anymore!")]
        LotteryGameAlreadyFinished = -402,
        [DefaultErrorMessage("Currently there is no active lottery game existing!")]
        NoActiveLotteryGame = -403,
        [DefaultErrorMessage("This user does not have enough coins anymore to draw a game!")]
        LotteryNotEnoughCoins = -404,
        [DefaultErrorMessage("This lottery price is not for use on vending machines and therefore can not be used to be redeemed at a vending machine! This item needs to be picked up!")]
        LotteryPriceNotForVendingMachines = -405,
        [DefaultErrorMessage("The lottery price can not be redeemed at this vending machine, because this vending machine is not part of the lottery activity!")]
        LotteryPriceRedeemNotAtThisMachine = -406,
        [DefaultErrorMessage("The user does not have enough won coupons for this price anymore.")]
        LotteryNotEnoughCoupons = -407,
        [DefaultErrorMessage("The given coupon token is invalid")]
        LotteryCouponTokenInvalid = -408,
        [DefaultErrorMessage("The given coupon token already has been used")]
        LotteryCouponTokenAlreadyUsed = -409,
        [DefaultErrorMessage("For this lottery game the parameter 'token' is required!")]
        LotteryPickupTokenMissing = -410,
        [DefaultErrorMessage("The given pickup token is invalid!")]
        LotteryPickupTokenInvalid = -411,
        [DefaultErrorMessage("No product selection has been given for redeem! That price has no fixed product defined, so the parameter 'productId' need to be set!")]
        LotteryRedeemMissingProductSelection = -412,
        // auto coupons/modules
        [DefaultErrorMessage("The criteria can not be saved, because the C# module used is invalid!")]
        CriteriaInvalidModule = -500,
        [DefaultErrorMessage("The criteria has no C# module assigned!")]
        CSharpModuleMissing = -501,
        // common VMA error codes
        [DefaultErrorMessage("The connection to the BuyBuy API failed or the BuyBuy API returned an error")]        
        BuyBuyApiConnectionError = -1000,
        [DefaultErrorMessage("Failed to decrypt the message correctly")]
        BuyBuyDecryptionError = -1001,
        [DefaultErrorMessage("All records available already have been used. There is no record available anymore")]
        AlreadyUsedAllRecords = -1002,
        [DefaultErrorMessage("An internal db error occured.")]
        ExceptionInStoredProcedure = -1003,
        // common error codes
        [DefaultErrorMessage("Record not found")]
        RecordNotFound = -10000,
        [DefaultErrorMessage("User does not exist")]
        UserNotFound = -10001,
        [DefaultErrorMessage("Invalid password")]
        InvalidPassword = -10002,
        [DefaultErrorMessage("Permission denied")]
        PermissionDenied = -10003,
        [DefaultErrorMessage("Unknown or invalid social login type")]
        InvalidSocialLoginType = -10004,
        [DefaultErrorMessage("Invalid email address")]
        InvalidEmailAddress = -10005,
        [DefaultErrorMessage("Invalid phone number")]
        InvalidPhoneNumber = -10006,
        [DefaultErrorMessage("At least one parameter is missing!")]
        MissingArgument = -10007,
        [DefaultErrorMessage("One or more arguments given are invalid!")]
        InvalidArguments = -10008,
        [DefaultErrorMessage("This record already exists and can not be added/created again!")]
        RecordAlreadyExists = -10009,
        //[DefaultErrorMessage("A required parameter is missing!")]
        //-10010 still can be used
        [DefaultErrorMessage("The entered username is too short. Please choose a longer one!")]
        UsernameTooShort = -10011,
        [DefaultErrorMessage("The entered password is too short. Please choose a longer one!")]
        PasswordTooShort = -10012,
        FileFormatNotSupported = -10013,
        [DefaultErrorMessage("Internal server error")]
        InternalServerError = -10900,
        [DefaultErrorMessage("Write permission denied!")]
        WritePermissionDenied = -10014,
        [DefaultErrorMessage("Wrong input")]
        AdminInputError = -10015,
        // registration
        [DefaultErrorMessage("This user already has been registered!")]
        UserAlreadyExists = -10100,
        [DefaultErrorMessage("The phone number given is already in use by another user account!")]
        PhoneAlreadyInUse = -10101,
        [DefaultErrorMessage("The email address given is already in use by another user account!")]
        EmailAlreadyInUse = -10102,
        [DefaultErrorMessage("The given password is too short! It needs to have at least 6 characters!")]
        PasswordTooSimple = -10103,
        [DefaultErrorMessage("This passport number already have been used by another user account!")]
        PassportNoAlreadyUsed = -10104,
        [DefaultErrorMessage("This user token is already in use by another user account!")]
        UserTokenAlreadyInUse = -10105,
        [DefaultErrorMessage("This user already have been registered with this social login type and therefore this login type can not be added again to this users registration")]
        SocialLoginAlreadyUsed = -10106,
        // API access
        [DefaultErrorMessage("The given API token is not valid!")]
        InvalidAPIToken = -10200,
        [DefaultErrorMessage("This given user token is invalid!")]
        UserTokenInvalid = -10201,
        //[DefaultErrorMessage("Unknow or not implemented login type!")]
        //-10202 still can be used
        [DefaultErrorMessage("Invalid or expired session token")]
        InvalidSessionToken = -10203,
        [DefaultErrorMessage("Unknown or not implemented login type!")]
        UnknownLoginType = -10204,
        //[DefaultErrorMessage("This user token is already in use by another user account!")]
        //-10205 still can be used
        [DefaultErrorMessage("The verification token given is invalid!")]
        InvalidVerificationToken = -10206,
        [DefaultErrorMessage("The login process can not be completed. This is due to an internal reason. If you see this error messsage then maybe something went wrong in the API software.")]
        LoginNotPossible = -10207,
        [DefaultErrorMessage("The registration process can not be completed. This is due to an internal reason. If you see this error messsage then maybe something went wrong in the API software.")]
        RegistrationNotPossible = -10208,
        [DefaultErrorMessage("This user have been banned or deactivated!")]
        UserBlocked = -10209,
        [DefaultErrorMessage("This user's registration is still in Pending state! The verification code need to be sent first!")]
        UserStatusPending = -10210,
        // MODULECODES HERE - please do not remove or change this commment!!
// ##section[NEWSMODULE] begin - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
        [DefaultErrorMessage("The given news category id does not exist!")]
        NewsCategoryDoesNotExist = -40001,
        [DefaultErrorMessage("Could not find a matching news entry")]
        NewsEntryNotFound = -40002,
        [DefaultErrorMessage("Unknown update type! Valid update types are: 1 = Add, 2 = Modify, 3 = Delete.")]
        NewsUnknownUpdateType = -40003,
        [DefaultErrorMessage("This update type is not yet supported or allowed for this API call!")]
        NewsUpdateTypeNotSupported = -40004,
        [DefaultErrorMessage("The type parameter (news article type) given is unknown!")]
        NewsUnknownArticleType = -40005,
// ##section[NEWSMODULE] end - auto-generated. Please do NOT remove and do NOT change anything here or inside this section! (changes will be overwritten)
    }
}

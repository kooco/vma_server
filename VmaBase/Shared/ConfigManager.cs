using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VmaBase.Models.Enum;

namespace VmaBase.Shared
{
    /// <summary>
    ///     Class for managing web.config settings
    /// </summary>
    public class ConfigManager : Kooco.Framework.Shared.ConfigManager
    {
        #region UserPointsExpiryInMonths
        /// <summary>
        ///     Returns the amount of months after which new points will expire
        /// </summary>
        public static int UserPointsExpiryInMonths
        {
            get
            {
                return GetIntFromDb("UserPointsExpiryInMonths");
            }
        }
        #endregion
        #region UserPointsExpiryInMonths
        /// <summary>
        ///     Returns from which date the points expiry date should be calculated from.
        ///     Can be one of the following values:
        ///         GlobalConst.PointsExpiryPeriod_EndOfDay
        ///         GlobalConst.PointsExpiryPeriod_EndOfWeek
        ///         GlobalConst.PointsExpiryPeriod_EndOfMonth
        /// </summary>
        public static string UserPointsExpiryPeriod
        {
            get
            {
                return GetStringFromDb("UserPointsExpiryPeriod");
            }
        }
        #endregion
        #region PromotionOrderExpiryInMonths
        /// <summary>
        ///     Returns the amount of months an exchanged product order will expire after
        /// </summary>
        public static int PromotionOrderExpiryInMonths
        {
            get
            {
                return GetIntFromDb("PromotionOrderExpiryInMonths");
            }
        }
        #endregion
        #region ImportLogMaxAgeInDays
        /// <summary>
        ///     Returns the maximum amount of days the import log entries should be kept
        /// </summary>
        public static int ImportLogMaxAgeInDays
        {
            get
            {
                return GetIntFromDb("ImportLogMaxAgeDays");
            }
        }
        #endregion
        #region BuyBuyApiUrl
        /// <summary>
        ///     Returns the url for the "BuyBuy" (Coca Cola) vending API
        /// </summary>
        public static string BuyBuyApiUrl
        {
            get
            {
                return GetString("BuyBuyApiUrl");
            }
        }
        #endregion
        #region DefaultPromotionProductsPickupLocation
        public static string DefaultPromotionProductsPickupLocation
        {
            get
            {
                return GetStringFromDb("PromoProdStdPickupLoc");
            }
        }
        #endregion
        #region SalesListAvailabilityUpdateInterval
        public static int SalesListAvailabilityUpdateInterval
        {
            get
            {
                return GetIntFromDb("SalesListAvailUpdateSecs");
            }
        }
        #endregion
        #region IsReviewEnvironment
        public static bool IsReviewEnvironment
        {
            get
            {
                return GetBoolFromDb("IsReviewEnv");
            }
        }
        #endregion
        #region Environment
        public static HostEnvironment Environment
        {
            get
            {
                string sValue = GetStringFromDb("Environment");
                switch(sValue)
                {
                    case GlobalConst.Environment_Production:
                        return HostEnvironment.Production;
                    case GlobalConst.Environment_Test:
                        return HostEnvironment.Test;
                    default:
                        return HostEnvironment.Development;
                }
            }
        }
        #endregion
        #region EnableVendingMachineQueryAPISuccessLog
        public static bool EnableVendingMachineQueryAPISuccessLog
        {
            get
            {
                return GetBoolFromDb("EnableVMQApiSuccessLog");
            }
        }
        #endregion
        #region MinimumAppVersionAndroid
        public static string MinimumAppVersionAndroid
        {
            get
            {
                string sVersion = GetStringFromDb("MinimumAppVersionAndroid");
                return sVersion;
            }
        }
        #endregion
        #region MinimumAppVersionIOS
        public static string MinimumAppVersionIOS
        {
            get
            {
                string sVersion = GetStringFromDb("MinimumAppVersionIOS");
                return sVersion;
            }
        }
        #endregion

        #region TempDir
        public static string TempDir
        {
            get
            {
                return GetString("TempDir");
            }
        }
        #endregion

        #region CSharpModulesCompileCheckUserId
        public static long CSharpModulesCompileCheckUserId
        {
            get
            {
                return GetIntFromDb("CSharpModulesCompileCheckUserId");
            }
        }
        #endregion
        #region CSharpModulesCacheExpiryTime
        public static long CSharpModulesCacheExpiryTime
        {
            get
            {
                return GetIntFromDb("CSharpModulesCacheExpiryTime");
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace VmaBase.Shared
{
    public class GlobalConst : Kooco.Framework.Shared.GlobalConst
    {
        public const string ApplicationName = "VMAServer"; // don't change! also used for AliYun!
        public static AppVersion ApplicationVersion = new AppVersion(1, 2, 2); 
        public const string ApiKey = "SKUZ72W901CLM00A";
        public const string BuyBuyApiKey = "KXA2Y3A29";

        public const string ExcelExportDateTimeFormat = "yyyy/mm/dd hh:mm";

        public const int LocalTimeOffset = 8; // Offset to UTC time for the local time

        public const int CoordinateSystemId = 4326;
        public const int SalesTransactionsExpiryTime = 10; // expiry time in minutes for sales transaction which are started (product sent to machine) but not yet completed so far (machine sends "transaction completed") after which the order will be considered as cancelled
        public const int SalesTransactionMessageExpiryTimeSeconds = 60; // expiry time in seconds after which a sales transaction's product selection message will be cancelled if not retrieved by the vending machine

        public const string PointsExpiryPeriod_EndOfDay = "EndOfDay";
        public const string PointsExpiryPeriod_EndOfWeek = "EndOfWeek";
        public const string PointsExpiryPeriod_EndOfMonth = "EndOfMonth";

        public const string MOCCPVendingApi_DESKey = "B1uYKe1y";
        public const string MOCCPVendingApi_DESIV = "buY@yBi5";

        public const string BuyBuyAESKey = "B67uYKe2yL";
        public const string BuyBuyAESIV = "buYAS+yAe6c-T17x";

        public const string NewsCategory_Activities = "ACT";

        public const string Environment_Development = "Development";
        public const string Environment_Test = "Test";
        public const string Environment_Production = "Production";

        public const string BuyBuyJpushKey = "d0ebe61579f6817ed2891b0a";
        public const string BuyBuyJpushSec = "88ad7000603aa8d1ccf93ba6";

        public const string BuyBuyJPushType_ProductChosen = "2";
        public const string BuyBuyJPushType_CouponRedeem = "3";

        public const int LotteryGameId_StandardLottery = 1;
        public const int LotteryGameId_Watersports = 2;

        public const int ProductTypeId_Others = 9;
        public const int ProductBrandId_Others = 19;
    }
}

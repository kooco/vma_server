﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

	using Kooco.Framework;
    using Kooco.Framework.Providers.EntityFramework;
	using Kooco.Framework.DataMigrations;

	using VmaBase.Models.DataStorage;

    public sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"DataMigrations";

            SetSqlGenerator("System.Data.SqlClient", new CustomSqlServerMigrationSqlGenerator());
			CodeGenerator = new FrameworkMigrationCodeGenerator();
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
        }
    }
}

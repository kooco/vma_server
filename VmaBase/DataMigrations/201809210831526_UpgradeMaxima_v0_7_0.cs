namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima_v0_7_0 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JiraConnectedAttachments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IssueId = c.Long(nullable: false),
                        JiraIssueAttachmentId = c.Long(nullable: false),
                        MimeType = c.String(maxLength: 128),
                        FileName = c.String(maxLength: 255),
                        FileSize = c.Int(),
                        AttachmentCreateTime = c.DateTime(nullable: false),
                        PreviewImageData = c.Binary(),
                        FirstImportTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastUpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JiraConnectedIssues", t => t.IssueId)
                .Index(t => t.IssueId);
            
            CreateTable(
                "dbo.JiraConnectedIssues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 32),
                        IssueCreateTime = c.DateTime(nullable: false),
                        Summary = c.String(maxLength: 255),
                        Description = c.String(storeType: "ntext"),
                        IssueType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        Priority = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "3")
                                },
                            }),
                        ReporterUsername = c.String(maxLength: 128),
                        AsigneeUsername = c.String(maxLength: 128),
                        ReporterUID = c.Long(),
                        UpdaterUID = c.Long(),
                        IssueStatus = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        LastCompletedTime = c.DateTime(),
                        IssueUpdatedTime = c.DateTime(nullable: false),
                        FirstImportTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastUpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.ReporterUID)
                .ForeignKey("dbo.UserBases", t => t.UpdaterUID)
                .Index(t => t.Key, unique: true, name: "UIX_Key")
                .Index(t => t.ReporterUID)
                .Index(t => t.UpdaterUID)
                .Index(t => t.LastCompletedTime);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_7_0(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbchangesV0_7_0(this);

            DropForeignKey("dbo.JiraConnectedAttachments", "IssueId", "dbo.JiraConnectedIssues");
            DropForeignKey("dbo.JiraConnectedIssues", "UpdaterUID", "dbo.UserBases");
            DropForeignKey("dbo.JiraConnectedIssues", "ReporterUID", "dbo.UserBases");
            DropIndex("dbo.JiraConnectedIssues", new[] { "LastCompletedTime" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "UpdaterUID" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "ReporterUID" });
            DropIndex("dbo.JiraConnectedIssues", "UIX_Key");
            DropIndex("dbo.JiraConnectedAttachments", new[] { "IssueId" });
            DropTable("dbo.JiraConnectedIssues",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "FirstImportTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "IssueStatus",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "IssueType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "LastUpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Priority",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "3" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedAttachments",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "FirstImportTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LastUpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

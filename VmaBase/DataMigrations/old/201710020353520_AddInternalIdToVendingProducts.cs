namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddInternalIdToVendingProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingProducts", "MOCCPInternalId", c => c.Int());
            AddColumn("dbo.SalesLists", "MOCCPInternalId", c => c.Int());

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v4.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgProductSelection_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgRedeemCoupon_v3.sql"));            
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgProductSelection_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgRedeemCoupon_v2.sql"));

            DropColumn("dbo.SalesLists", "MOCCPInternalId");
            DropColumn("dbo.VendingProducts", "MOCCPInternalId");
        }
    }
}

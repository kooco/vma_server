namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    public partial class InitialModel : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationActions",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 12, unicode: false),
                        ControllerName = c.String(nullable: false, maxLength: 128),
                        ActionName = c.String(maxLength: 128),
                        PermissionLevel = c.Int(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code)
                .Index(t => new { t.ControllerName, t.ActionName }, unique: true, name: "UIX_ApplicationAction_ControllerAction");
            
            CreateTable(
                "dbo.ApplicationMenuItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentId = c.Long(),
                        ActionCode = c.String(maxLength: 12, unicode: false),
                        Url = c.String(maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 32),
                        Icon = c.String(maxLength: 32),
                        Sort = c.Int(),
                        Status = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationActions", t => t.ActionCode)
                .ForeignKey("dbo.ApplicationMenuItems", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.ActionCode);
            
            CreateTable(
                "dbo.ApplicationPermissionSets",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 12, unicode: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.ApplicationRoles",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 12, unicode: false),
                        ParentRoleCode = c.String(maxLength: 12, unicode: false),
                        Description = c.String(maxLength: 256),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.ApplicationRoles", t => t.ParentRoleCode)
                .Index(t => t.ParentRoleCode);
            
            CreateTable(
                "dbo.UserBases",
                c => new
                    {
                        UID = c.Long(nullable: false, identity: true),
                        Account = c.String(nullable: false, maxLength: 128),
                        Password = c.String(maxLength: 256),
                        FacebookAccount = c.String(maxLength: 50),
                        QQAccount = c.String(maxLength: 50),
                        WechatAccount = c.String(maxLength: 50),
                        TwitterAccount = c.String(maxLength: 50),
                        GoogleAccount = c.String(maxLength: 50),
                        Nickname = c.String(maxLength: 50),
                        Name = c.String(maxLength: 128),
                        Email = c.String(maxLength: 128),
                        Phone = c.String(maxLength: 128),
                        RoleCode = c.String(nullable: false, maxLength: 12, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "USER")
                                },
                            }),
                        Status = c.Short(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastLoginTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.UID)
                .ForeignKey("dbo.ApplicationRoles", t => t.RoleCode)
                .Index(t => t.RoleCode);
            
            CreateTable(
                "dbo.UserDatas",
                c => new
                    {
                        UID = c.Long(nullable: false),
                        ImageUrl = c.String(maxLength: 128),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.UID)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.UserRolePermissions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleCode = c.String(maxLength: 12, unicode: false),
                        UID = c.Long(),
                        ActionCode = c.String(maxLength: 12, unicode: false),
                        PermissionSetCode = c.String(maxLength: 12, unicode: false),
                        PermissionType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationActions", t => t.ActionCode)
                .ForeignKey("dbo.ApplicationPermissionSets", t => t.PermissionSetCode)
                .ForeignKey("dbo.ApplicationRoles", t => t.RoleCode)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.RoleCode, name: "IX_UserRolePermission_Roles")
                .Index(t => t.UID, name: "IX_UserRolePermission_Users")
                .Index(t => t.ActionCode)
                .Index(t => t.PermissionSetCode);
            
            CreateTable(
                "dbo.UserSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Token = c.String(nullable: false, maxLength: 50),
                        UID = c.Long(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateExpires = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.Token, unique: true, name: "UIX_UserSession_Token")
                .Index(t => t.UID);

            FrameworkMigrationHelper.ApplyAdditionalDbChanges(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_0_34(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_0_60(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_0_60(this);
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_0_34(this);
            FrameworkMigrationHelper.RevertAdditionalDbChanges(this);

            DropForeignKey("dbo.UserSessions", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserRolePermissions", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserRolePermissions", "RoleCode", "dbo.ApplicationRoles");
            DropForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets");
            DropForeignKey("dbo.UserRolePermissions", "ActionCode", "dbo.ApplicationActions");
            DropForeignKey("dbo.UserDatas", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserBases", "RoleCode", "dbo.ApplicationRoles");
            DropForeignKey("dbo.ApplicationRoles", "ParentRoleCode", "dbo.ApplicationRoles");
            DropForeignKey("dbo.ApplicationMenuItems", "ParentId", "dbo.ApplicationMenuItems");
            DropForeignKey("dbo.ApplicationMenuItems", "ActionCode", "dbo.ApplicationActions");
            DropIndex("dbo.UserSessions", new[] { "UID" });
            DropIndex("dbo.UserSessions", "UIX_UserSession_Token");
            DropIndex("dbo.UserRolePermissions", new[] { "PermissionSetCode" });
            DropIndex("dbo.UserRolePermissions", new[] { "ActionCode" });
            DropIndex("dbo.UserRolePermissions", "IX_UserRolePermission_Users");
            DropIndex("dbo.UserRolePermissions", "IX_UserRolePermission_Roles");
            DropIndex("dbo.UserDatas", new[] { "UID" });
            DropIndex("dbo.UserBases", new[] { "RoleCode" });
            DropIndex("dbo.ApplicationRoles", new[] { "ParentRoleCode" });
            DropIndex("dbo.ApplicationMenuItems", new[] { "ActionCode" });
            DropIndex("dbo.ApplicationMenuItems", new[] { "ParentId" });
            DropIndex("dbo.ApplicationActions", "UIX_ApplicationAction_ControllerAction");
            DropTable("dbo.UserSessions");
            DropTable("dbo.UserRolePermissions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "PermissionType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.UserDatas",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-1" },
                        }
                    },
                });
            DropTable("dbo.UserBases",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LastLoginTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "RoleCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "USER" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-1" },
                        }
                    },
                });
            DropTable("dbo.ApplicationRoles",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.ApplicationPermissionSets",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.ApplicationMenuItems",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.ApplicationActions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

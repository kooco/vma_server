﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddAppUsersMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("APPUSR", "AppUsers", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);

            AddMenuItem(Id: 44000, ParentId: null, ActionCode: "APPUSR", Name: "會員", Sort: 200);
            AddMenuItem(Id: 44001, ParentId: 44000, ActionCode: "APPUSR", Name: "會員管理", Sort: 201);
        }
        
        public override void Down()
        {
            RemoveMenuItem(44001);
            RemoveMenuItem(44000);

            RemoveApplicationAction("APPUSR");
        }
    }
}

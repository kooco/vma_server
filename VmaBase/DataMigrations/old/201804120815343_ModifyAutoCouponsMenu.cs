﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class ModifyAutoCouponsMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            UpdateMenuItem(50000, Name: "飲料卷自動系統");
            UpdateMenuItem(50001, Name: "飲料卷自動系統(規則)");
        }
        
        public override void Down()
        {

        }
    }
}

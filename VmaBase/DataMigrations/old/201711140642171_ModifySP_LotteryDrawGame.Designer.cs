// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModifySP_LotteryDrawGame : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModifySP_LotteryDrawGame));
        
        string IMigrationMetadata.Id
        {
            get { return "201711140642171_ModifySP_LotteryDrawGame"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

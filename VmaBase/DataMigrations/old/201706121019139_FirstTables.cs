namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class FirstTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDatas", "PassportIDNumber", c => c.String(nullable: false, maxLength: 8));
            AddColumn("dbo.UserDatas", "Birthday", c => c.DateTime());
            AddColumn("dbo.UserDatas", "City", c => c.String(maxLength: 50));
            AddColumn("dbo.UserDatas", "Location", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.UserDatas", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserDatas", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserDatas", "PointsTotal", c => c.Double(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.UserDatas", "StampsTotal", c => c.Double(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.UserDatas", "PushToken", c => c.String(maxLength: 64));
            CreateIndex("dbo.UserDatas", "PassportIDNumber", unique: true, name: "UK_PassportNo");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserDatas", "UK_PassportNo");
            DropColumn("dbo.UserDatas", "PushToken");
            DropColumn("dbo.UserDatas", "StampsTotal",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.UserDatas", "PointsTotal",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.UserDatas", "Latitude");
            DropColumn("dbo.UserDatas", "Longitude");
            DropColumn("dbo.UserDatas", "Location");
            DropColumn("dbo.UserDatas", "City");
            DropColumn("dbo.UserDatas", "Birthday");
            DropColumn("dbo.UserDatas", "PassportIDNumber");
        }
    }
}

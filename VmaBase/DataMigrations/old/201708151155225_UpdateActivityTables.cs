namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class UpdateActivityTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StampTransactions", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.StampTransactions", "UID", "dbo.UserDatas");
            DropIndex("dbo.StampTransactions", "IX_User_Type");
            CreateTable(
                "dbo.UserStampLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserStampId = c.Long(nullable: false),
                        StampTypeId = c.Long(nullable: false),
                        LogType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        UserReferenceId = c.Long(),
                        LogText = c.String(nullable: false, maxLength: 128),
                        LogTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId)
                .ForeignKey("dbo.UserBases", t => t.UserReferenceId)
                .ForeignKey("dbo.UserCouponStamps", t => t.UserStampId)
                .Index(t => t.UserStampId, name: "IX_CouponId")
                .Index(t => t.StampTypeId)
                .Index(t => t.UserReferenceId);
            
            AddColumn("dbo.NewsArticles", "ActivityId", c => c.Long());
            AddColumn("dbo.UserCouponStamps", "Status", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            AddColumn("dbo.UserCouponStamps", "ReceivedByUserId", c => c.Long());
            CreateIndex("dbo.NewsArticles", "ActivityId");
            CreateIndex("dbo.UserCouponStamps", "ReceivedByUserId");
            AddForeignKey("dbo.NewsArticles", "ActivityId", "dbo.CouponActivities", "Id");
            AddForeignKey("dbo.UserCouponStamps", "ReceivedByUserId", "dbo.UserBases", "UID");
            DropTable("dbo.StampTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "EventId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "TransactionId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v2vma.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v2.sql"));

            CreateTable(
                "dbo.StampTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        StampTypeId = c.Long(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        TransactionId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        EventId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Amount = c.Double(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.UserStampLogs", "UserStampId", "dbo.UserCouponStamps");
            DropForeignKey("dbo.UserStampLogs", "UserReferenceId", "dbo.UserBases");
            DropForeignKey("dbo.UserStampLogs", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.UserCouponStamps", "ReceivedByUserId", "dbo.UserBases");
            DropForeignKey("dbo.NewsArticles", "ActivityId", "dbo.CouponActivities");
            DropIndex("dbo.UserStampLogs", new[] { "UserReferenceId" });
            DropIndex("dbo.UserStampLogs", new[] { "StampTypeId" });
            DropIndex("dbo.UserStampLogs", "IX_CouponId");
            DropIndex("dbo.UserCouponStamps", new[] { "ReceivedByUserId" });
            DropIndex("dbo.NewsArticles", new[] { "ActivityId" });
            DropColumn("dbo.UserCouponStamps", "ReceivedByUserId");
            DropColumn("dbo.UserCouponStamps", "Status",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.NewsArticles", "ActivityId");
            DropTable("dbo.UserStampLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "LogTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LogType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            CreateIndex("dbo.StampTransactions", new[] { "UID", "StampTypeId" }, name: "IX_User_Type");
            AddForeignKey("dbo.StampTransactions", "UID", "dbo.UserDatas", "UID");
            AddForeignKey("dbo.StampTransactions", "StampTypeId", "dbo.StampTypes", "Id");
        }
    }
}

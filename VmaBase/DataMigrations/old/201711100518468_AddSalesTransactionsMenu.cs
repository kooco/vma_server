﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddSalesTransactionsMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("SALES", "SalesTransactions", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, "銷售清單");

            AddMenuItem(48000, ParentId: null, ActionCode: "SALES", Name: "銷售清單", Sort: 780, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(48001, ParentId: 48000, ActionCode: "SALES", Name: "銷售清單", Sort: 780, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(48001);
            RemoveMenuItem(48000);

            RemoveApplicationAction("SALES");
        }
    }
}

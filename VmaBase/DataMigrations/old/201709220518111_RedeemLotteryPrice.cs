namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class RedeemLotteryPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingMachineMessages", "LotteryPriceCouponId", c => c.Long());
            CreateIndex("dbo.VendingMachineMessages", "LotteryPriceCouponId");
            AddForeignKey("dbo.VendingMachineMessages", "LotteryPriceCouponId", "dbo.LotteryPriceCoupons", "Id");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgRedeemCoupon_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryRedeemMachinePrice_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_LotteryRedeemMachinePrice]");
            Sql("DROP PROCEDURE [dbo].[sp_GetMachineMsgRedeemCoupon]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgRedeemCoupon_v1.sql"));

            DropForeignKey("dbo.VendingMachineMessages", "LotteryPriceCouponId", "dbo.LotteryPriceCoupons");
            DropIndex("dbo.VendingMachineMessages", new[] { "LotteryPriceCouponId" });
            DropColumn("dbo.VendingMachineMessages", "LotteryPriceCouponId");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AlterNewsArticle_AddListImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NewsArticles", "ListImageUrl", c => c.String(maxLength: 255));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v4vma.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v3vma.sql"));

            DropColumn("dbo.NewsArticles", "ListImageUrl");
        }
    }
}

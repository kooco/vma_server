namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UpgradeMaxima_v0_4_57 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserLoginLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UID = c.Long(),
                        Type = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-2")
                                },
                            }),
                        UserToken = c.String(maxLength: 50),
                        Account = c.String(maxLength: 128),
                        Phone = c.String(maxLength: 128),
                        MobileDeviceType = c.Int(nullable: false),
                        LoginType = c.Int(nullable: false),
                        ReturnCode = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => new { t.Time, t.Type })
                .Index(t => t.UID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserLoginLogs", "UID", "dbo.UserBases");
            DropIndex("dbo.UserLoginLogs", new[] { "UID" });
            DropIndex("dbo.UserLoginLogs", new[] { "Time", "Type" });
            DropTable("dbo.UserLoginLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ReturnCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Time",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Type",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-2" },
                        }
                    },
                });
        }
    }
}

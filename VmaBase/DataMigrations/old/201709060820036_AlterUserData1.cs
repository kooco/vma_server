namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserData1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserDatas", "Location", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserDatas", "Location", c => c.String(nullable: false, maxLength: 128));
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserPhone : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserDatas", "UK_PhoneNrSearch7");
            AlterColumn("dbo.UserDatas", "PhoneNrSearch7", c => c.String(maxLength: 7));
            CreateIndex("dbo.UserDatas", "PhoneNrSearch7", name: "UK_PhoneNrSearch7");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserDatas", "UK_PhoneNrSearch7");
            AlterColumn("dbo.UserDatas", "PhoneNrSearch7", c => c.String(nullable: false, maxLength: 7));
            CreateIndex("dbo.UserDatas", "PhoneNrSearch7", name: "UK_PhoneNrSearch7");
        }
    }
}

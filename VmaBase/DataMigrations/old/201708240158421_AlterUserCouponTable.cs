namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserCouponTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserCoupons", "UIX_ActivityUser");
            CreateIndex("dbo.UserCoupons", new[] { "ActivityId", "UID" }, clustered: true, name: "IX_ActivityUser");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserCoupons", "IX_ActivityUser");
            CreateIndex("dbo.UserCoupons", new[] { "ActivityId", "UID" }, unique: true, clustered: true, name: "UIX_ActivityUser");
        }
    }
}

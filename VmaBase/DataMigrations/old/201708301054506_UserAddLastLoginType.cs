namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UserAddLastLoginType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDatas", "LastLoginType", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDatas", "LastLoginType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
        }
    }
}

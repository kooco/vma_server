﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class InsertWatersportsActivity : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT [dbo].[CouponActivities] ON\n" +
                "INSERT INTO [dbo].[CouponActivities] ([Id], [Title], [ActivityText], [AllVendingMachines], [LimitStartTime], [LimitEndTime], [CouponExpiryDays], [NoStampsActivity], [SendingCouponsAllowed], [ShowOnPromotionMachinesList], [RedeemEndTime])\n" +
                                             " Values (100, N'水動樂活動', N'水動樂活動', 1, '2018-08-31 16:00:00', '2018-10-31 15:59:59.997', 180, 1, 0, 1, '2018-11-30 15:59:59.997')\n" +
                "SET IDENTITY_INSERT [dbo].[CouponActivities] OFF");

            Sql("SET IDENTITY_INSERT [dbo].[NewsArticles] ON\n" + 
                "INSERT INTO [dbo].[NewsArticles] ([Id], [Title],      [CategoryId], [Status], [ActivityId], [LimitStartTime], [LimitEndTime], [Content]) " +
                                         " Values (100,  N'水動樂活動', (SELECT [Id] FROM [dbo].[NewsCategories] WHERE [Code] = 'ACT'), 1, 100, '2018-08-31 16:00:00', '2018-10-31 15:59:59.997', N'水動樂活動')\n" +
                "SET IDENTITY_INSERT [dbo].[NewsArticles] OFF");

            Sql("INSERT INTO [dbo].[ActivityParticipatingProducts] ([ActivityId], [ProductId], [StampTypeId1], [Status]) " +
                    "SELECT 100, [Id], [DefaultStampTypeId], 1 FROM [dbo].[VendingProducts] WHERE [Name] like N'%水動樂%' AND NOT [DefaultStampTypeId] IS NULL");

            Sql("UPDATE [dbo].[LotteryGames] SET [ConnectedActivityId] = 100 WHERE [Id] = " + ((int)GlobalConst.LotteryGameId_Watersports).ToString());
            Sql("UPDATE [dbo].[LotteryPrices] SET [UseActivityCoupon] = 1 WHERE [LotteryGameId] = " + ((int)GlobalConst.LotteryGameId_Watersports).ToString() + " AND [Title] LIKE N'%系列飲品%'");

            Sql("UPDATE [dbo].[VendingProducts] SET [IsWatersports] = 1 WHERE [Name] like N'%水動樂%'");
        }
        
        public override void Down()
        {
            Sql("UPDATE [dbo].[VendingProducts] SET [IsWatersports] = 0 WHERE [Name] like N'%水動樂%'");

            Sql("UPDATE[dbo].[LotteryGames] SET [ConnectedActivityId] = null WHERE[Id] = " + ((int)GlobalConst.LotteryGameId_Watersports).ToString());
            Sql("UPDATE [dbo].[LotteryPrices] SET [UseActivityCoupon] = 0 WHERE [LotteryGameId] = " + ((int)GlobalConst.LotteryGameId_Watersports).ToString() + " AND [Title] LIKE N'%系列飲品%'");

            Sql("DELETE FROM [dbo].[UserCoupons] WHERE [ActivityId] = 100");
            Sql("DELETE FROM [dbo].[ActivityParticipatingMachines] WHERE [ActivityId] = 100");
            Sql("DELETE FROM [dbo].[ActivityParticipatingProducts] WHERE [ActivityId] = 100");
            Sql("DELETE FROM [dbo].[NewsArticles] WHERE [ActivityId] = 100");
            Sql("DELETE FROM [dbo].[CouponActivities] WHERE [Id] = 100");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    using Kooco.Framework.DataMigrations;

    public partial class AddSystemSetting_Environment : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationSetting("Environment", Kooco.Framework.Models.Enum.AppSettingType.List, "Environment", ListValues: new string[] { GlobalConst.Environment_Development, GlobalConst.Environment_Test, GlobalConst.Environment_Production }, InitialValue: GlobalConst.Environment_Development);
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("Environment");
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddAutoCouponsMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("ACCRIT", "AutoCoupons", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "自動發送的飲料卷");
            AddMenuItem(50000, ParentId: null, ActionCode: "ACCRIT", Name: "自動發送的飲料卷", Sort: 540);
            AddMenuItem(50001, ParentId: 50001, ActionCode: "ACCRIT", Name: "自動發送的飲料卷(規則)");
        }
        
        public override void Down()
        {
            RemoveMenuItem(50001);
            RemoveMenuItem(50000);
            RemoveApplicationAction("ACCRIT");
        }
    }
}

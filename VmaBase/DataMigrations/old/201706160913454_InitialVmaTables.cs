namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class InitialVmaTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PointsTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        TransactionId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        EventId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Amount = c.Double(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.UID, name: "IX_User");
            
            CreateTable(
                "dbo.SalesEvents",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        VendingMachineId = c.Long(),
                        ProductId = c.Long(),
                        PointsReceivable = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        StampsReceivable = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        StampTypeId = c.Long(),
                        StartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "Cast('1900-1-1' as datetime)")
                                },
                            }),
                        EndTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "Cast('2999-12-31' as datetime)")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.VendingMachineId)
                .Index(t => t.ProductId)
                .Index(t => t.StampTypeId);
            
            CreateTable(
                "dbo.VendingProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ExternalProductId = c.Long(nullable: false),
                        Name = c.String(maxLength: 64),
                        ImageUrl = c.String(maxLength: 128),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ExternalProductId, unique: true, name: "UK_ExternalProductId");
            
            CreateTable(
                "dbo.StampTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VendingMachines",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        City = c.String(maxLength: 50),
                        Location = c.String(nullable: false, maxLength: 128),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QRToken = c.String(nullable: false, maxLength: 32),
                        ImageUrl = c.String(nullable: false, maxLength: 128),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SalesLists",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        VendingMachineId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        RetrievablePoints = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        RetrievableStamps = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.VendingMachineId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.SalesTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReceiptNumber = c.String(maxLength: 64),
                        UID = c.Long(nullable: false),
                        VendingMachineId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Price = c.Double(nullable: false),
                        PaymentType = c.Int(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.ReceiptNumber)
                .Index(t => t.UID)
                .Index(t => t.VendingMachineId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.StampsPointsTransferTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UIDSent = c.Long(nullable: false),
                        UIDReceived = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Memo = c.String(nullable: false, maxLength: 1024),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserDatas", t => t.UIDReceived)
                .ForeignKey("dbo.UserDatas", t => t.UIDSent)
                .Index(t => t.UIDSent)
                .Index(t => t.UIDReceived);
            
            CreateTable(
                "dbo.StampTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        StampTypeId = c.Long(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        TransactionId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        EventId = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Amount = c.Double(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.UID, t.StampTypeId }, name: "IX_User_Type");
            
            CreateTable(
                "dbo.UserFriends",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        UIDFriend = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserDatas", t => t.UIDFriend)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.UIDFriend);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserFriends", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.UserFriends", "UIDFriend", "dbo.UserDatas");
            DropForeignKey("dbo.StampTransactions", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.StampTransactions", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.StampsPointsTransferTransactions", "UIDSent", "dbo.UserDatas");
            DropForeignKey("dbo.StampsPointsTransferTransactions", "UIDReceived", "dbo.UserDatas");
            DropForeignKey("dbo.SalesTransactions", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.SalesTransactions", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.SalesTransactions", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.SalesLists", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.SalesLists", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.SalesEvents", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.SalesEvents", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.SalesEvents", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.PointsTransactions", "UID", "dbo.UserDatas");
            DropIndex("dbo.UserFriends", new[] { "UIDFriend" });
            DropIndex("dbo.UserFriends", new[] { "UID" });
            DropIndex("dbo.StampTransactions", "IX_User_Type");
            DropIndex("dbo.StampsPointsTransferTransactions", new[] { "UIDReceived" });
            DropIndex("dbo.StampsPointsTransferTransactions", new[] { "UIDSent" });
            DropIndex("dbo.SalesTransactions", new[] { "ProductId" });
            DropIndex("dbo.SalesTransactions", new[] { "VendingMachineId" });
            DropIndex("dbo.SalesTransactions", new[] { "UID" });
            DropIndex("dbo.SalesTransactions", new[] { "ReceiptNumber" });
            DropIndex("dbo.SalesLists", new[] { "ProductId" });
            DropIndex("dbo.SalesLists", new[] { "VendingMachineId" });
            DropIndex("dbo.VendingProducts", "UK_ExternalProductId");
            DropIndex("dbo.SalesEvents", new[] { "StampTypeId" });
            DropIndex("dbo.SalesEvents", new[] { "ProductId" });
            DropIndex("dbo.SalesEvents", new[] { "VendingMachineId" });
            DropIndex("dbo.PointsTransactions", "IX_User");
            DropTable("dbo.UserFriends",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.StampTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "EventId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "TransactionId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.StampsPointsTransferTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.SalesTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.SalesLists",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "RetrievablePoints",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "RetrievableStamps",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.VendingMachines",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.StampTypes");
            DropTable("dbo.VendingProducts",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.SalesEvents",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "EndTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "Cast('2999-12-31' as datetime)" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "PointsReceivable",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "StampsReceivable",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "StartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "Cast('1900-1-1' as datetime)" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.PointsTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "EventId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "TransactionId",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
        }
    }
}

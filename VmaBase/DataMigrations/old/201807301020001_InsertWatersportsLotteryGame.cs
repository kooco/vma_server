﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Models.Enum;
    using VmaBase.Shared;

    public partial class InsertWatersportsLotteryGame : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("set IDENTITY_INSERT dbo.LotteryGames ON\n" +
                "INSERT INTO [dbo].[LotteryGames] ([Id], [Title], [StartTime], [EndTime], [CouponsExpiryTime], [TestStartTime], [TestMode], [DrawType], [Status]) " +
                                         " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", N'水動樂活動', '2018-08-31 16:00:00', '2018-10-31 15:59:59.997', '2018-11-30 15:59:59.997', '2018-07-30 00:00:00', " + ((int)LotteryTestMode.RegurarelyWinTestMode).ToString() + ", 1, " + ((int)LotteryGameStatus.Started).ToString() + ")\n " +
                "set IDENTITY_INSERT dbo.LotteryGames OFF");
            Sql("update dbo.VendingProducts set [WatersportsCoins] = 1 where [Name] like N'%水動樂%'");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM [dbo].[LotteryGameUseStatistics] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());                        
            Sql("DELETE FROM [dbo].[UserLotteryCoins] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("DELETE FROM [dbo].[LotteryCoinLogs] WHERE [LotteryDrawLogId] IN (SELECT [Id] FROM [dbo].[LotteryDrawLogs] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString() + ")");
            Sql("DELETE FROM [dbo].[LotteryDrawLogs] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("DELETE FROM [dbo].[LotteryGames] WHERE [Id] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("update dbo.VendingProducts set [WatersportsCoins] = 0 where [Name] like N'%水動樂%'");
        }
    }
}

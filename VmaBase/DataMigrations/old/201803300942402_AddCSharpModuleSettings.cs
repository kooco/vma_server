namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddCSharpModuleSettings : FrameworkDbMigration
    {
        public override void Up()
        {            
            AddApplicationSetting("CSharpModulesCompileCheckUserId", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "C# Modules Compile User-Id", "The user id to be used for compiling and checking (test-executing) the auto coupon module codes before they are ready to use. The C# modules always will be executed with that user id to verify it's correct behavior.", InitialValue: "97");
            AddApplicationSetting("CSharpModulesCacheExpiryTime", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "C# Modules Cache Expiry", "Expiry time in minutes, after which the C# modules cache will expire and being refreshed", NumRangeFrom: 1, NumRangeTo: 600, InitialValue: "10");
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("CSharpModulesCacheExpiryTime");
            RemoveApplicationSetting("CSharpModulesCompileCheckUserId");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using Kooco.Framework.DataMigrations;
    using System;
    using System.Data.Entity.Migrations;

    public partial class UpdateMaxima_v092 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationSettings",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 32),
                        DisplayName = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 255),
                        UnitName = c.String(maxLength: 14),
                        NumRangeFrom = c.Long(),
                        NumRangeTo = c.Long(),
                        MinLength = c.Long(),
                        MaxLength = c.Long(),
                        ListValues = c.String(maxLength: 255),
                        Type = c.Int(nullable: false),
                        SettingValue = c.String(nullable: false, maxLength: 255),
                        LastModifiedByUID = c.Long(),
                        LastModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.LastModifiedByUID)
                .Index(t => t.Key, unique: true)
                .Index(t => t.LastModifiedByUID);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_0_92(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_0_92(this);

            DropForeignKey("dbo.ApplicationSettings", "LastModifiedByUID", "dbo.UserBases");
            DropIndex("dbo.ApplicationSettings", new[] { "LastModifiedByUID" });
            DropIndex("dbo.ApplicationSettings", new[] { "Key" });
            DropTable("dbo.ApplicationSettings");
        }
    }
}

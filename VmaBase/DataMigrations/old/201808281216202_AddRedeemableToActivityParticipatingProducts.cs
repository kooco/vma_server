namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddRedeemableToActivityParticipatingProducts : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActivityParticipatingProducts", "Redeemable", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ActivityParticipatingProducts", "Redeemable",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
        }
    }
}

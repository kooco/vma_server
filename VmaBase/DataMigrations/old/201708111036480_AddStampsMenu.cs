﻿namespace VmaBase.DataMigrations
{    
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;

    public partial class AddStampsMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("STMPIDX", "Stamps", "Index");

            AddMenuItem(41000, ParentId: null, ActionCode: "STMPIDX", Name: "印花", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(41001, ParentId: 41000, ActionCode: "STMPIDX", Name: "印花管理", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(41001);
            RemoveMenuItem(41000);

            RemoveApplicationAction("STMPIDX");
        }
    }
}

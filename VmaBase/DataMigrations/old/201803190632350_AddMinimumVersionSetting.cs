namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddMinimumVersionSetting : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationSetting("MinimumAppVersionAndroid", Kooco.Framework.Models.Enum.AppSettingType.Text, "Minimum App Version (Android)", "Minimum required version for the Android client app. Please provide the version number in the format #.# (like 1.2 or 1.5 for example)", InitialValue: "1.2");
            AddApplicationSetting("MinimumAppVersionIOS", Kooco.Framework.Models.Enum.AppSettingType.Text, "Minimum App Version (IOS)", "Minimum required version for the IOS client app. Please provide the version number in the format #.# (like 1.2 or 1.5 for example)", InitialValue: "1.2");
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("MinimumAppVersionAndroid");
            RemoveApplicationSetting("MinimumAppVersionIOS");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeNewsModule_v0_3_28 : FrameworkDbMigration
    {
        public override void Up()
        {
            NewsModuleMigrationHelper.ApplyAdditionalNewsModuleDbChanges_0_3_28(this);
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v6vma.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v5vma.sql"));
            NewsModuleMigrationHelper.RevertAdditionalNewsModuleDbChanges_0_3_28(this);
        }
    }
}

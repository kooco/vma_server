﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class UpdateActionNamesAndMasterUser : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [dbo].[UserBases] SET [IsMasterAdmin] = 1 where [Account] = 'admin'");

            UpdateApplicationAction("APPUSR", FriendlyName: "會員管理");
            UpdateApplicationAction("BUGTRK", FriendlyName: "錯誤跟踪");
            UpdateApplicationAction("LOTITM", FriendlyName: "獎品狀態");
            UpdateApplicationAction("LOTTERY", FriendlyName: "贏獎品");
            UpdateApplicationAction("MACHINE", FriendlyName: "販賣機管理");
            UpdateApplicationAction("NEWSBAN", FriendlyName: "新聞輪播");
            UpdateApplicationAction("NEWSMNG", FriendlyName: "新聞管理");
            UpdateApplicationAction("PRDIDX", FriendlyName: "產品管理");
            UpdateApplicationAction("PRORDR", FriendlyName: "兌換列表");
            UpdateApplicationAction("PRPROD", FriendlyName: "禮品管理");
            UpdateApplicationAction("STMPIDX", FriendlyName: "印花管理");
        }
        
        public override void Down()
        {
        }
    }
}

// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModifySP_AcquireOrderStamps_FixReturnCode : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModifySP_AcquireOrderStamps_FixReturnCode));
        
        string IMigrationMetadata.Id
        {
            get { return "201808310652583_ModifySP_AcquireOrderStamps_FixReturnCode"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

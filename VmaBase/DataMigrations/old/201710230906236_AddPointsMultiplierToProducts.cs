namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointsMultiplierToProducts : DbMigration
    {
        public override void Up()
        {
            Sql(@"DECLARE @con nvarchar(128)
                  SELECT @con = name
                    FROM sys.default_constraints
                   WHERE parent_object_id = object_id('dbo.SalesLists')
                     AND col_name(parent_object_id, parent_column_id) = 'RetrievablePoints';

                  IF @con IS NOT NULL
                    EXECUTE('ALTER TABLE [dbo].[SalesLists] DROP CONSTRAINT ' + @con)");

            Sql(@"DECLARE @con nvarchar(128)
                  SELECT @con = name
                    FROM sys.default_constraints
                   WHERE parent_object_id = object_id('dbo.SalesLists')
                     AND col_name(parent_object_id, parent_column_id) = 'RetrievableStamps';

                  IF @con IS NOT NULL
                    EXECUTE('ALTER TABLE [dbo].[SalesLists] DROP CONSTRAINT ' + @con)");

            AddColumn("dbo.VendingProducts", "PointsExtra", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.VendingProducts", "PointsMultiplier", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            AddColumn("dbo.VendingProducts", "AveragePrice", c => c.Double(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.SalesLists", "Price", c => c.Double(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AlterColumn("dbo.SalesLists", "RetrievablePoints", c => c.Int(nullable: false));
            AlterColumn("dbo.SalesLists", "RetrievableStamps", c => c.Int(nullable: false));
            DropColumn("dbo.VendingProducts", "Points",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.VendingProducts", "Price",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
        
        public override void Down()
        {
            AddColumn("dbo.VendingProducts", "Price", c => c.Double(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.VendingProducts", "Points", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AlterColumn("dbo.SalesLists", "RetrievableStamps", c => c.Double(nullable: false));
            AlterColumn("dbo.SalesLists", "RetrievablePoints", c => c.Double(nullable: false));
            DropColumn("dbo.SalesLists", "Price",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.VendingProducts", "AveragePrice",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.VendingProducts", "PointsMultiplier",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.VendingProducts", "PointsExtra",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserCoupon : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserCoupons", "CompletionTime", c => c.DateTime());
            AlterColumn("dbo.UserCoupons", "RedeemTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserCoupons", "RedeemTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.UserCoupons", "CompletionTime", c => c.DateTime(nullable: false));
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AlterCouponActivityTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponActivities", "NoStampsActivity", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.CouponActivities", "SendingCouponsAllowed", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v4.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v5.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v4.sql"));            
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v4.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v3.sql"));

            DropColumn("dbo.CouponActivities", "SendingCouponsAllowed",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.CouponActivities", "NoStampsActivity",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima_v0_4_14 : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationRoles", "IsSystemRole", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_12(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_12(this);

            DropColumn("dbo.ApplicationRoles", "IsSystemRole",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
    }
}

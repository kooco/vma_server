namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddAppSettingReviewEnvironment : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationSetting("IsReviewEnv", Kooco.Framework.Models.Enum.AppSettingType.List, "Review Environment", "Flag, if this environment is used as the review (fake) environment or not", ListValues: new string[] { "Yes", "No" }, InitialValue: "No");
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("IsReviewEnv");
        }
    }
}

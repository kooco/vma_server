﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertLotteryGame_FoodFestival : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[NewsArticles]([Title], [ImageUrl], [CategoryId], [Content], [LikeCount], [ShareCount], [ReadOnly], [Status], [LimitStartTime], [LimitEndTime]) " +
                                         " Values(N'可口可樂幸運汽水機', null, (select [Id] from [dbo].[NewsCategories] where [Code] = 'ACT'), N'', 0, 0, 0, 1, '2017-09-20 00:00:00', '2017-11-26 16:00:00')"); // UTC 16:00 is midnight in macau!

            Sql("INSERT INTO [dbo].[CouponActivities]([Title], [ActivityText], [ProductId], [AllVendingMachines], [LimitStartTime], [LimitEndTime], [CouponExpiryDays], [NoStampsActivity], [SendingCouponsAllowed], [Status])" +
                                         " Values(N'可口可樂幸運汽水機', N'',    (select [Id] from [dbo].[VendingProducts] where [MOCCPGoodsId] = '467'), 1, '2017-09-20 00:00:00', '2017-11-26 16:00:00', 0, 1, 0, 1)");

            Sql("UPDATE [dbo].[NewsArticles] SET [ActivityId] = (SELECT [Id] from [dbo].[CouponActivities] where [Title] = N'可口可樂幸運汽水機') where [Title] = N'可口可樂幸運汽水機'");

            Sql("INSERT INTO [dbo].[LotteryGames]([Title], [StartTime], [EndTime], [CouponsExpiryTime], [TestStartTime], [TestMode], [DrawType], [ConnectedActivityId], [Status])" +
                                         " Values(N'可口可樂幸運汽水機', '2017-11-10 16:00:00', '2017-11-26 16:00:00', '2017-11-26 16:00:00', '2017-09-20 00:00:00', 0, 1, (select [Id] from [dbo].[CouponActivities] where [Title] = N'可口可樂幸運汽水機'), 1)");
        }
        
        public override void Down()
        {
        }
    }
}

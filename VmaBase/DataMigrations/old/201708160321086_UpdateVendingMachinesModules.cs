namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Shared;

    public partial class UpdateVendingMachinesModules : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE SPATIAL INDEX IX_VendingMachines_GeoLocation ON dbo.VendingMachines(GeoLocation) USING GEOGRAPHY_GRID");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineListCount_v1.sql"));
        }

        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_GetVendingMachineListCount]");
            Sql("DROP PROCEDURE [dbo].[sp_GetVendingMachineList]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v1.sql"));

            Sql("DROP SPATIAL INDEX IX_VendingMachines_GeoLocation ON dbo.VendingMachines(GeoLocation)");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrganizationToProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingProducts", "MOCCPOrganization", c => c.String(maxLength: 15));
            CreateIndex("dbo.VendingProducts", new[] { "MOCCPOrganization", "MOCCPInternalId" }, name: "IX_InternalId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.VendingProducts", "IX_InternalId");
            DropColumn("dbo.VendingProducts", "MOCCPOrganization");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;

    public partial class AddUserPoints : FrameworkDbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StampsPointsTransferTransactions", "UIDReceived", "dbo.UserDatas");
            DropForeignKey("dbo.StampsPointsTransferTransactions", "UIDSent", "dbo.UserDatas");
            DropIndex("dbo.StampsPointsTransferTransactions", new[] { "UIDSent" });
            DropIndex("dbo.StampsPointsTransferTransactions", new[] { "UIDReceived" });
            CreateTable(
                "dbo.UserPoints",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        Amount = c.Int(nullable: false),
                        OriginalAmount = c.Int(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateAquired = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ExpiryDate = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.UID, t.Status }, name: "UserPoint_UIDStatus");
            
            AddColumn("dbo.PointsTransactions", "ReferenceUserId", c => c.Long());
            AddColumn("dbo.PointsTransactions", "SalesTransactionId", c => c.Long());
            AddColumn("dbo.PointsTransactions", "ActivityId", c => c.Long());
            CreateIndex("dbo.PointsTransactions", "ReferenceUserId");
            CreateIndex("dbo.PointsTransactions", "SalesTransactionId");
            CreateIndex("dbo.PointsTransactions", "ActivityId");
            AddForeignKey("dbo.PointsTransactions", "ActivityId", "dbo.CouponActivities", "Id");
            AddForeignKey("dbo.PointsTransactions", "ReferenceUserId", "dbo.UserDatas", "UID");
            AddForeignKey("dbo.PointsTransactions", "SalesTransactionId", "dbo.SalesTransactions", "Id");
            DropColumn("dbo.PointsTransactions", "TransactionId",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.PointsTransactions", "EventId",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropTable("dbo.StampsPointsTransferTransactions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });

            AddApplicationSetting("UserPointsExpiryInMonths", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "User-Points Expiry in months", "The amount of months after which points the user have will expire - Note: changing this value will have effect only for new points, the expiry date of existing points will not be changed!", NumRangeFrom: 1, NumRangeTo: 240, InitialValue: "12");
            AddApplicationSetting("UserPointsExpiryPeriod", Kooco.Framework.Models.Enum.AppSettingType.List, "Calculation of the expiry date from", "From which date the expiry date should be calculated from? - Note: changing this value will only affect new points, NO points the users already have!", ListValues: new string[] { GlobalConst.PointsExpiryPeriod_EndOfDay, GlobalConst.PointsExpiryPeriod_EndOfWeek, GlobalConst.PointsExpiryPeriod_EndOfMonth }, InitialValue: GlobalConst.PointsExpiryPeriod_EndOfMonth);

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CheckExpiredPoints_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserPoints_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UseUserPoints_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_UseUserPoints]");
            Sql("DROP PROCEDURE [dbo].[sp_AddUserPoints]");
            Sql("DROP PROCEDURE [dbo].[sp_CheckExpiredPoints]");

            RemoveApplicationSetting("UserPointsExpiryPeriod");
            RemoveApplicationSetting("UserPointsExpiryInMonths");

            CreateTable(
                "dbo.StampsPointsTransferTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UIDSent = c.Long(nullable: false),
                        UIDReceived = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Memo = c.String(nullable: false, maxLength: 1024),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PointsTransactions", "EventId", c => c.Long(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.PointsTransactions", "TransactionId", c => c.Long(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            DropForeignKey("dbo.UserPoints", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.PointsTransactions", "SalesTransactionId", "dbo.SalesTransactions");
            DropForeignKey("dbo.PointsTransactions", "ReferenceUserId", "dbo.UserDatas");
            DropForeignKey("dbo.PointsTransactions", "ActivityId", "dbo.CouponActivities");
            DropIndex("dbo.UserPoints", "UserPoint_UIDStatus");
            DropIndex("dbo.PointsTransactions", new[] { "ActivityId" });
            DropIndex("dbo.PointsTransactions", new[] { "SalesTransactionId" });
            DropIndex("dbo.PointsTransactions", new[] { "ReferenceUserId" });
            DropColumn("dbo.PointsTransactions", "ActivityId");
            DropColumn("dbo.PointsTransactions", "SalesTransactionId");
            DropColumn("dbo.PointsTransactions", "ReferenceUserId");
            DropTable("dbo.UserPoints",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateAquired",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ExpiryDate",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            CreateIndex("dbo.StampsPointsTransferTransactions", "UIDReceived");
            CreateIndex("dbo.StampsPointsTransferTransactions", "UIDSent");
            AddForeignKey("dbo.StampsPointsTransferTransactions", "UIDSent", "dbo.UserDatas", "UID");
            AddForeignKey("dbo.StampsPointsTransferTransactions", "UIDReceived", "dbo.UserDatas", "UID");
        }
    }
}

namespace VmaBase.DataMigrations
{    
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    public partial class UpgradeMaxima_0_3_58 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserValueChangeRequests",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        FieldSetName = c.String(nullable: false, maxLength: 64),
                        NewValue = c.String(maxLength: 255),
                        ValidationToken = c.String(maxLength: 32),
                        ExpiryTime = c.DateTime(),
                        AutoApproveTime = c.DateTime(),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => new { t.UID, t.FieldSetName, t.Status }, name: "UIX_UID_FieldSet_Status")
                .Index(t => new { t.ValidationToken, t.Status }, unique: true, name: "UIX_ValidationToken_Status");

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_3_58(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_3_58(this);

            DropForeignKey("dbo.UserValueChangeRequests", "UID", "dbo.UserBases");
            DropIndex("dbo.UserValueChangeRequests", "UIX_ValidationToken_Status");
            DropIndex("dbo.UserValueChangeRequests", "UIX_UID_FieldSet_Status");
            DropTable("dbo.UserValueChangeRequests",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

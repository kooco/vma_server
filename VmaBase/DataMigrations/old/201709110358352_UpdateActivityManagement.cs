namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class UpdateActivityManagement : DbMigration
    {
        public override void Up()
        {
            // change "ReceiverUID" to "SenderUID" for a better data model, to make queries more simple and fast
            RenameColumn(table: "dbo.UserCoupons", name: "ReceiverUID", newName: "SenderUID");
            RenameIndex(table: "dbo.UserCoupons", name: "IX_ActivityReceiverUser", newName: "IX_ActivitySenderUser");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_GetCouponActivityList]");
            Sql("DROP PROCEDURE [dbo].[sp_AddUserStamp]");
            
            RenameIndex(table: "dbo.UserCoupons", name: "IX_ActivitySenderUser", newName: "IX_ActivityReceiverUser");
            RenameColumn(table: "dbo.UserCoupons", name: "SenderUID", newName: "ReceiverUID");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v1.sql"));
        }
    }
}

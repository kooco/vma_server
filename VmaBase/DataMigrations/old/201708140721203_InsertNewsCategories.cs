﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class InsertNewsCategories : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[NewsCategories]([Code], [Name]) Values('ACT', N'最新推廣')");
            Sql("INSERT INTO [dbo].[NewsCategories]([Code], [Name]) Values('DSC', N'最新打折')");
            Sql("UPDATE [dbo].[NewsArticles] SET [CategoryId] = (SELECT [Id] FROM [dbo].[NewsCategories] WHERE [Code] = 'DSC')");
        }

        public override void Down()
        {
            Sql("DELETE FROM [dbo].[NewsCategories] WHERE [Code] IN ('ACT', 'DSC')");
        }
    }
}

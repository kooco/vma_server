namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class RedeemCouponModule : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.StartOrderProduct_v5.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgRedeemCoupon_v1.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.StartOrderProduct_v4.sql"));
            Sql("DROP PROCEDURE [dbo].[sp_GetMachineMsgRedeemCoupon]");
        }
    }
}

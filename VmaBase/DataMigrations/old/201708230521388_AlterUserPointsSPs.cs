namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AlterUserPointsSPs : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UseUserPoints_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserPoints_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.SendPoints_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_SendPoints]");
            Sql("DROP PROCEDURE [dbo].[sp_AddUserPoints]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UseUserPoints_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserPoints_v1.sql"));
        }
    }
}

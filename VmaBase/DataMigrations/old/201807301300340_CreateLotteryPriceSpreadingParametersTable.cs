namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class CreateLotteryPriceSpreadingParametersTable : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LotteryPriceSpreadingParameters",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        LotteryPriceId = c.Long(nullable: false),
                        PeriodFrom = c.DateTime(nullable: false),
                        PeriodTo = c.DateTime(nullable: false),
                        SpreadCount = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.LotteryPrices", t => t.LotteryPriceId)
                .Index(t => t.LotteryGameId)
                .Index(t => t.LotteryPriceId);
            
            AddColumn("dbo.LotteryPrices", "UseSpreadingParameters", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LotteryPriceSpreadingParameters", "LotteryPriceId", "dbo.LotteryPrices");
            DropForeignKey("dbo.LotteryPriceSpreadingParameters", "LotteryGameId", "dbo.LotteryGames");
            DropIndex("dbo.LotteryPriceSpreadingParameters", new[] { "LotteryPriceId" });
            DropIndex("dbo.LotteryPriceSpreadingParameters", new[] { "LotteryGameId" });
            DropColumn("dbo.LotteryPrices", "UseSpreadingParameters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropTable("dbo.LotteryPriceSpreadingParameters",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "SpreadCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
        }
    }
}

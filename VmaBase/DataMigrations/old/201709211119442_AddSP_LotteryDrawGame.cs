namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddSP_LotteryDrawGame : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryDrawGame_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_LotteryDrawGame]");
        }
    }
}

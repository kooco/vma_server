namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpgradMaxima_0_3_61 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserValueChangeRequests", "UIX_ValidationToken_Status");
            CreateIndex("dbo.UserValueChangeRequests", new[] { "ValidationToken", "Status" });
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserValueChangeRequests", new[] { "ValidationToken", "Status" });
            CreateIndex("dbo.UserValueChangeRequests", new[] { "ValidationToken", "Status" }, unique: true, name: "UIX_ValidationToken_Status");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Kooco.Framework.Models.Enum;

    using Models.Enum;

    public partial class UpgradeMaxima_v0_4_56 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_55(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_56(this);

            Sql("UPDATE [dbo].[StatisticsItems] SET [AggregateType] = " + ((int)StatisticsAggregateType.Max).ToString() + " WHERE [Id] IN (" +
                ((int)StatisticsItems.Lottery_PlayersCount).ToString() + "," +
                ((int)StatisticsItems.Users_UserCount).ToString() + "," +
                ((int)StatisticsItems.Activity_UsersCountCollectingStamps).ToString() + ")");
        }
        
        public override void Down()
        {
            Sql("UPDATE [dbo].[StatisticsItems] SET [AggregateType] = " + ((int)StatisticsAggregateType.Count).ToString() + " WHERE [Id] IN (" +
                ((int)StatisticsItems.Lottery_PlayersCount).ToString() + "," +
                ((int)StatisticsItems.Users_UserCount).ToString() + "," +
                ((int)StatisticsItems.Activity_UsersCountCollectingStamps).ToString() + ")");

            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_55(this);
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_56(this);
        }
    }
}

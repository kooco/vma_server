namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActivityCouponToPriceCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LotteryPriceCoupons", "ActivityCouponId", c => c.Long());
            CreateIndex("dbo.LotteryPriceCoupons", "ActivityCouponId");
            AddForeignKey("dbo.LotteryPriceCoupons", "ActivityCouponId", "dbo.UserCoupons", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LotteryPriceCoupons", "ActivityCouponId", "dbo.UserCoupons");
            DropIndex("dbo.LotteryPriceCoupons", new[] { "ActivityCouponId" });
            DropColumn("dbo.LotteryPriceCoupons", "ActivityCouponId");
        }
    }
}

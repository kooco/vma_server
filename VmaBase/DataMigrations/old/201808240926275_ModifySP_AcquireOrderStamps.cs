namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class ModifySP_AcquireOrderStamps : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v10.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v9.sql"));
        }
    }
}

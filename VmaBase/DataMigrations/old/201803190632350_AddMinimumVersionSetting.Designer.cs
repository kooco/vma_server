// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddMinimumVersionSetting : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddMinimumVersionSetting));
        
        string IMigrationMetadata.Id
        {
            get { return "201803190632350_AddMinimumVersionSetting"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddLotteryMenuItems : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("LOTTERY", "Lottery", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddApplicationAction("LOTITM", "Lottery", "PriceItems", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);

            AddMenuItem(Id: 45000, ParentId: null, ActionCode: "LOTTERY", Name: "幸運汽水機", Sort: 800, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(Id: 45001, ParentId: 45000, ActionCode: "LOTTERY", Name: "贏獎品", Sort: 801, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(Id: 45002, ParentId: 45000, ActionCode: "LOTITM", Name: "獎品狀態", Sort: 802, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(45002);
            RemoveMenuItem(45001);
            RemoveMenuItem(45000);

            RemoveApplicationAction("LOTITM");
            RemoveApplicationAction("LOTTERY");
        }
    }
}

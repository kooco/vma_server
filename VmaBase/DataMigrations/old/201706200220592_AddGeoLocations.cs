namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Spatial;
    
    public partial class AddGeoLocations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDatas", "GeoLocation", c => c.Geography());
            AddColumn("dbo.VendingMachines", "GeoLocation", c => c.Geography(nullable: false));
            DropColumn("dbo.UserDatas", "Longitude");
            DropColumn("dbo.UserDatas", "Latitude");
            DropColumn("dbo.VendingMachines", "Longitude");
            DropColumn("dbo.VendingMachines", "Latitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VendingMachines", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.VendingMachines", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserDatas", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserDatas", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.VendingMachines", "GeoLocation");
            DropColumn("dbo.UserDatas", "GeoLocation");
        }
    }
}

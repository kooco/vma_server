namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima4_0_0 : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserBases", "IsMasterAdmin", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.ApplicationActions", "FriendlyName", c => c.String(maxLength: 128));

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_0(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_0(this);

            DropColumn("dbo.ApplicationActions", "FriendlyName");
            DropColumn("dbo.UserBases", "IsMasterAdmin",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
    }
}

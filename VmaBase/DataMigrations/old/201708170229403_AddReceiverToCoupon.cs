namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceiverToCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserCoupons", "ReceiverUID", c => c.Long());
            CreateIndex("dbo.UserCoupons", "ReceiverUID", name: "IX_ActivityReceiverUser");
            AddForeignKey("dbo.UserCoupons", "ReceiverUID", "dbo.UserDatas", "UID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCoupons", "ReceiverUID", "dbo.UserDatas");
            DropIndex("dbo.UserCoupons", "IX_ActivityReceiverUser");
            DropColumn("dbo.UserCoupons", "ReceiverUID");
        }
    }
}

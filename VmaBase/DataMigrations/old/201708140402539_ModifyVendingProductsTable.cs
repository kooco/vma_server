namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyVendingProductsTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.VendingProducts", "UK_ExternalProductId");
            AddColumn("dbo.VendingProducts", "MOCCPGoodsId", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.VendingProducts", "NameEN", c => c.String(maxLength: 64));
            AddColumn("dbo.VendingProducts", "Volume", c => c.String(maxLength: 32));
            AddColumn("dbo.VendingProducts", "Type", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "2")
                    },
                }));
            AlterColumn("dbo.VendingProducts", "Name", c => c.String(nullable: false, maxLength: 64));
            CreateIndex("dbo.VendingProducts", "MOCCPGoodsId", unique: true, name: "UK_ExternalProductId");
            DropColumn("dbo.VendingProducts", "ExternalProductId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VendingProducts", "ExternalProductId", c => c.Long(nullable: false));
            DropIndex("dbo.VendingProducts", "UK_ExternalProductId");
            AlterColumn("dbo.VendingProducts", "Name", c => c.String(maxLength: 64));
            DropColumn("dbo.VendingProducts", "Type",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "2" },
                });
            DropColumn("dbo.VendingProducts", "Volume");
            DropColumn("dbo.VendingProducts", "NameEN");
            DropColumn("dbo.VendingProducts", "MOCCPGoodsId");
            CreateIndex("dbo.VendingProducts", "ExternalProductId", unique: true, name: "UK_ExternalProductId");
        }
    }
}

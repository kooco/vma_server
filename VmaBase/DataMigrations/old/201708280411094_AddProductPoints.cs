namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddProductPoints : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SalesTransactions", new[] { "VendingMachineId" });
            AddColumn("dbo.VendingProducts", "Points", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            CreateIndex("dbo.SalesTransactions", new[] { "Status", "VendingMachineId" }, name: "IX_Status_Machine");
            CreateIndex("dbo.SalesTransactions", new[] { "Status", "CreateTime" });

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.StartOrderProduct_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgProductSelection_v2.sql"));
        }
        
        public override void Down()
        {
            DropIndex("dbo.SalesTransactions", new[] { "Status", "CreateTime" });
            DropIndex("dbo.SalesTransactions", "IX_Status_Machine");
            DropColumn("dbo.VendingProducts", "Points",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            CreateIndex("dbo.SalesTransactions", "VendingMachineId");

            Sql("DROP PROCEDURE [dbo].[sp_GetMachineMsgProductSelection]");
            Sql("DROP PROCEDURE [dbo].[sp_StartOrderProduct]");
            Sql("DROP PROCEDURE [dbo].[sp_UpdateSalesList]");
            Sql("DROP PROCEDURE [dbo].[sp_CompleteMachineOrder]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgProductSelection_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.StartOrderProduct_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v1.sql"));
        }
    }
}

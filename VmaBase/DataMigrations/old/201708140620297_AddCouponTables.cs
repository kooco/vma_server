namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddCouponTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityParticipatingMachines",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ActivityId = c.Long(nullable: false),
                        VendingMachineId = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponActivities", t => t.ActivityId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.ActivityId, name: "IX_Activity")
                .Index(t => new { t.ActivityId, t.VendingMachineId }, unique: true, name: "UIX_Activity_Machine");
            
            CreateTable(
                "dbo.CouponActivities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 64),
                        ActivityText = c.String(maxLength: 128),
                        ProductId = c.Long(),
                        AllVendingMachines = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        LimitStartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LimitEndTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "dateadd(month, 1, getutcdate())")
                                },
                            }),
                        CouponExpiryDays = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "90")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ActivityParticipatingProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ActivityId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        StampTypeId1 = c.Long(nullable: false),
                        StampTypeId2 = c.Long(),
                        StampTypeId3 = c.Long(),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponActivities", t => t.ActivityId)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId1)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId2)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId3)
                .Index(t => t.ActivityId, name: "IX_Activity")
                .Index(t => new { t.ActivityId, t.ProductId }, unique: true, name: "UIX_Activity_Product")
                .Index(t => t.StampTypeId1)
                .Index(t => t.StampTypeId2)
                .Index(t => t.StampTypeId3);
            
            CreateTable(
                "dbo.CouponStampDefinitions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ActivityId = c.Long(nullable: false),
                        StampTypeId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponActivities", t => t.ActivityId)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId)
                .Index(t => t.ActivityId, name: "IX_Activity")
                .Index(t => t.StampTypeId);
            
            CreateTable(
                "dbo.UserCoupons",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ActivityId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        ExpiryTime = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        CompletionTime = c.DateTime(nullable: false),
                        RedeemTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id, clustered: false)
                .ForeignKey("dbo.CouponActivities", t => t.ActivityId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.ActivityId, t.UID }, unique: true, clustered: true, name: "UIX_ActivityUser");
            
            CreateTable(
                "dbo.UserCouponStamps",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserCouponId = c.Long(nullable: false),
                        StampTypeId = c.Long(nullable: false),
                        DateCollected = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StampTypes", t => t.StampTypeId)
                .ForeignKey("dbo.UserCoupons", t => t.UserCouponId)
                .Index(t => t.UserCouponId, name: "IX_CouponId")
                .Index(t => t.StampTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCouponStamps", "UserCouponId", "dbo.UserCoupons");
            DropForeignKey("dbo.UserCouponStamps", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.UserCoupons", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.UserCoupons", "ActivityId", "dbo.CouponActivities");
            DropForeignKey("dbo.CouponStampDefinitions", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.CouponStampDefinitions", "ActivityId", "dbo.CouponActivities");
            DropForeignKey("dbo.ActivityParticipatingProducts", "StampTypeId3", "dbo.StampTypes");
            DropForeignKey("dbo.ActivityParticipatingProducts", "StampTypeId2", "dbo.StampTypes");
            DropForeignKey("dbo.ActivityParticipatingProducts", "StampTypeId1", "dbo.StampTypes");
            DropForeignKey("dbo.ActivityParticipatingProducts", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.ActivityParticipatingProducts", "ActivityId", "dbo.CouponActivities");
            DropForeignKey("dbo.ActivityParticipatingMachines", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.ActivityParticipatingMachines", "ActivityId", "dbo.CouponActivities");
            DropForeignKey("dbo.CouponActivities", "ProductId", "dbo.VendingProducts");
            DropIndex("dbo.UserCouponStamps", new[] { "StampTypeId" });
            DropIndex("dbo.UserCouponStamps", "IX_CouponId");
            DropIndex("dbo.UserCoupons", "UIX_ActivityUser");
            DropIndex("dbo.CouponStampDefinitions", new[] { "StampTypeId" });
            DropIndex("dbo.CouponStampDefinitions", "IX_Activity");
            DropIndex("dbo.ActivityParticipatingProducts", new[] { "StampTypeId3" });
            DropIndex("dbo.ActivityParticipatingProducts", new[] { "StampTypeId2" });
            DropIndex("dbo.ActivityParticipatingProducts", new[] { "StampTypeId1" });
            DropIndex("dbo.ActivityParticipatingProducts", "UIX_Activity_Product");
            DropIndex("dbo.ActivityParticipatingProducts", "IX_Activity");
            DropIndex("dbo.CouponActivities", new[] { "ProductId" });
            DropIndex("dbo.ActivityParticipatingMachines", "UIX_Activity_Machine");
            DropIndex("dbo.ActivityParticipatingMachines", "IX_Activity");
            DropTable("dbo.UserCouponStamps",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCollected",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.UserCoupons",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "StartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.CouponStampDefinitions");
            DropTable("dbo.ActivityParticipatingProducts",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.CouponActivities",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "AllVendingMachines",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "CouponExpiryDays",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "90" },
                        }
                    },
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LimitEndTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "dateadd(month, 1, getutcdate())" },
                        }
                    },
                    {
                        "LimitStartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.ActivityParticipatingMachines",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

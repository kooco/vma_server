﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;
    
    public partial class AddProductsMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("PRDIDX", "VendingProducts", "Index");

            AddMenuItem(42000, ParentId: null, ActionCode: "PRDIDX", Name: "產品", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(42001, ParentId: 42000, ActionCode: "PRDIDX", Name: "產品管理", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(42001);
            RemoveMenuItem(42002);

            RemoveApplicationAction("PRDIDX");
        }
    }
}

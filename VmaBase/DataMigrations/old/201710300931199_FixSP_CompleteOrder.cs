namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class FixSP_CompleteOrder : DbMigration
    {
        public override void Up()
        {
            Sql("DROP INDEX [UIX_SalesTransaction_BuyBuyTransactionNr] ON [dbo].[SalesTransactions]");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v9.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v8.sql"));
        }
    }
}

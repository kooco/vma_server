namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddSP_CheckMachineMessageExpiry : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.VendingMachineMessages", new[] { "Status", "CreateTime" }, name: "IX_StatusCreateTime");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CheckMachineMessagesExpiry_v1.sql"));
        }
        
        public override void Down()
        {
            DropIndex("dbo.VendingMachineMessages", "IX_StatusCreateTime");

            Sql("DROP PROCEDURE [dbo].[sp_CheckMachineMessagesExpiry]");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpdateOldActivities : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("update dbo.CouponActivities set [Status] = 9 where Id in (select [ActivityId] from dbo.NewsArticles where not [ActivityId] is null and [Status] = 9)");
        }
        
        public override void Down()
        {
        }
    }
}

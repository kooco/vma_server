namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    
    public partial class AddCompletedByToUserCoupons : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserCoupons", "CompletedBy", c => c.Long());
            CreateIndex("dbo.UserCoupons", "CompletedBy");
            AddForeignKey("dbo.UserCoupons", "CompletedBy", "dbo.UserDatas", "UID");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v3.sql"));            
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v2.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v2.sql"));

            DropForeignKey("dbo.UserCoupons", "CompletedBy", "dbo.UserDatas");
            DropIndex("dbo.UserCoupons", new[] { "CompletedBy" });
            DropColumn("dbo.UserCoupons", "CompletedBy");
        }
    }
}

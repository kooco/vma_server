namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRecommendedByToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDatas", "RecommendedByUID", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDatas", "RecommendedByUID");
        }
    }
}

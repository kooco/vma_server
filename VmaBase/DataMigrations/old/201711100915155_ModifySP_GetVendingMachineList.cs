namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class ModifySP_GetVendingMachineList : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v8.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineListCount_v5.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v7.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineListCount_v4.sql"));
        }
    }
}

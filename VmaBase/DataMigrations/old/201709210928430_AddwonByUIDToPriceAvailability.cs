namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddwonByUIDToPriceAvailability : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LotteryPriceAvailabilityItems", "WonByUID", c => c.Long());
            Sql("CREATE SEQUENCE [dbo].[seq_LotteryPickupNumber] AS [int] START WITH 1 INCREMENT BY 1 MINVALUE - 1 MAXVALUE 9999 CACHE CYCLE");
        }
        
        public override void Down()
        {
            Sql("DROP SEQUENCE [dbo].[seq_LotteryPickupNumber]");
            DropColumn("dbo.LotteryPriceAvailabilityItems", "WonByUID");
        }
    }
}

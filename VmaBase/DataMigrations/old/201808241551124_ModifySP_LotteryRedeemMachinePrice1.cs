namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class ModifySP_LotteryRedeemMachinePrice1 : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryRedeemMachinePrice_v4.sql"));            
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryRedeemMachinePrice_v3.sql"));
        }
    }
}

namespace VmaBase.DataMigrations
{
    using Shared;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddUserPhoneNrSearch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDatas", "PhoneNrSearch7", c => c.String(maxLength: 7));            

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetUsersByPhoneNumbers_v1.sql"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDatas", "PhoneNrSearch7");

            Sql("DROP PROCEDURE [dbo].[sp_GetUsersByPhoneNumbers]");
        }
    }
}

namespace VmaBase.DataMigrations
{    
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    public partial class UpdateMaximav80 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.APIUserSessions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Provider = c.Int(nullable: false),
                        UID = c.Long(nullable: false),
                        Token = c.String(nullable: false, maxLength: 50),
                        ExpiryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID);
            
            AddColumn("dbo.UserBases", "VerificationToken", c => c.String(maxLength: 64));

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_0_80(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_0_80(this);

            DropForeignKey("dbo.APIUserSessions", "UID", "dbo.UserBases");
            DropIndex("dbo.APIUserSessions", new[] { "UID" });
            DropColumn("dbo.UserBases", "VerificationToken");
            DropTable("dbo.APIUserSessions");
        }
    }
}

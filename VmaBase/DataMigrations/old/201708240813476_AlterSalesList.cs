namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterSalesList : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SalesLists", new[] { "VendingMachineId" });
            DropIndex("dbo.SalesLists", new[] { "ProductId" });
            AddColumn("dbo.VendingMachines", "MOCCPOrganization", c => c.String(maxLength: 15));
            AddColumn("dbo.SalesLists", "MOCCPMerchandiseId", c => c.String(nullable: false, maxLength: 32));
            CreateIndex("dbo.SalesLists", new[] { "VendingMachineId", "MOCCPMerchandiseId" }, unique: true, name: "UIX_MOCCPMerchandiseId");
            CreateIndex("dbo.SalesLists", new[] { "VendingMachineId", "ProductId" }, unique: true, name: "UIX_MOCCPProductId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.SalesLists", "UIX_MOCCPProductId");
            DropIndex("dbo.SalesLists", "UIX_MOCCPMerchandiseId");
            DropColumn("dbo.SalesLists", "MOCCPMerchandiseId");
            DropColumn("dbo.VendingMachines", "MOCCPOrganization");
            CreateIndex("dbo.SalesLists", "ProductId");
            CreateIndex("dbo.SalesLists", "VendingMachineId");
        }
    }
}

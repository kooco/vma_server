namespace VmaBase.DataMigrations
{
    using Shared;
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddSP_GetVendingMachineList : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_GetVendingMachineList]");
        }
    }
}

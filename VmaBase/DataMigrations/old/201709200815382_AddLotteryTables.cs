namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddLotteryTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LotteryCoinLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        TransactionTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        Coins = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        LotteryDrawLogId = c.Long(),
                        FriendUID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryDrawLogs", t => t.LotteryDrawLogId)
                .ForeignKey("dbo.UserDatas", t => t.FriendUID)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.LotteryDrawLogId)
                .Index(t => t.FriendUID);
            
            CreateTable(
                "dbo.LotteryDrawLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        DrawDate = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "cast(getutcdate() as date)")
                                },
                            }),
                        DrawTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        Won = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        LotteryPriceIdWon = c.Long(),
                        LotteryPriceCouponId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.LotteryPriceCoupons", t => t.LotteryPriceCouponId)
                .ForeignKey("dbo.LotteryPrices", t => t.LotteryPriceIdWon)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.LotteryGameId)
                .Index(t => t.UID)
                .Index(t => t.DrawDate, name: "IX_Game_DrawDate")
                .Index(t => t.LotteryPriceIdWon)
                .Index(t => t.LotteryPriceCouponId);
            
            CreateTable(
                "dbo.LotteryGames",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 64),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        CouponsExpiryTime = c.DateTime(nullable: false),
                        TestStartTime = c.DateTime(),
                        TestMode = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        DrawType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        ConnectedActivityId = c.Long(),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponActivities", t => t.ConnectedActivityId)
                .Index(t => t.ConnectedActivityId);
            
            CreateTable(
                "dbo.LotteryPriceCoupons",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        LotteryPriceId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        PickupNumber = c.String(nullable: false, maxLength: 12),
                        TimeWon = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        TimeRedeemed = c.DateTime(),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.LotteryPrices", t => t.LotteryPriceId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.LotteryGameId, t.PickupNumber }, unique: true, name: "UIX_PickupNumber_Game")
                .Index(t => t.LotteryPriceId)
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.LotteryPrices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        Title = c.String(nullable: false, maxLength: 64),
                        ImageUrl = c.String(nullable: false, maxLength: 128),
                        Unit = c.String(nullable: false, maxLength: 4,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "N'個'")
                                },
                            }),
                        PickupAddress = c.String(nullable: false, maxLength: 128),
                        PickupPhone = c.String(nullable: false, maxLength: 32),
                        UseActivityCoupon = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        ActivityCouponProductId = c.Long(),
                        AmountAvailable = c.Int(nullable: false),
                        AmountWon = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        AmountPickedUp = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VendingProducts", t => t.ActivityCouponProductId)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .Index(t => t.LotteryGameId)
                .Index(t => t.ActivityCouponProductId);
            
            CreateTable(
                "dbo.LotteryGameUseStatistics",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        DrawDate = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "cast(getutcdate() as date)")
                                },
                            }),
                        DrawCount = c.Int(nullable: false),
                        WinCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .Index(t => t.LotteryGameId);
            
            AddColumn("dbo.UserDatas", "LotteryDailyCoins", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.UserDatas", "LotteryEarnedCoins", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LotteryGameUseStatistics", "LotteryGameId", "dbo.LotteryGames");
            DropForeignKey("dbo.LotteryCoinLogs", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.LotteryCoinLogs", "FriendUID", "dbo.UserDatas");
            DropForeignKey("dbo.LotteryCoinLogs", "LotteryDrawLogId", "dbo.LotteryDrawLogs");
            DropForeignKey("dbo.LotteryDrawLogs", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.LotteryDrawLogs", "LotteryPriceIdWon", "dbo.LotteryPrices");
            DropForeignKey("dbo.LotteryDrawLogs", "LotteryPriceCouponId", "dbo.LotteryPriceCoupons");
            DropForeignKey("dbo.LotteryPriceCoupons", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.LotteryPriceCoupons", "LotteryPriceId", "dbo.LotteryPrices");
            DropForeignKey("dbo.LotteryPrices", "LotteryGameId", "dbo.LotteryGames");
            DropForeignKey("dbo.LotteryPrices", "ActivityCouponProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.LotteryPriceCoupons", "LotteryGameId", "dbo.LotteryGames");
            DropForeignKey("dbo.LotteryDrawLogs", "LotteryGameId", "dbo.LotteryGames");
            DropForeignKey("dbo.LotteryGames", "ConnectedActivityId", "dbo.CouponActivities");
            DropIndex("dbo.LotteryGameUseStatistics", new[] { "LotteryGameId" });
            DropIndex("dbo.LotteryPrices", new[] { "ActivityCouponProductId" });
            DropIndex("dbo.LotteryPrices", new[] { "LotteryGameId" });
            DropIndex("dbo.LotteryPriceCoupons", new[] { "UID" });
            DropIndex("dbo.LotteryPriceCoupons", new[] { "LotteryPriceId" });
            DropIndex("dbo.LotteryPriceCoupons", "UIX_PickupNumber_Game");
            DropIndex("dbo.LotteryGames", new[] { "ConnectedActivityId" });
            DropIndex("dbo.LotteryDrawLogs", new[] { "LotteryPriceCouponId" });
            DropIndex("dbo.LotteryDrawLogs", new[] { "LotteryPriceIdWon" });
            DropIndex("dbo.LotteryDrawLogs", "IX_Game_DrawDate");
            DropIndex("dbo.LotteryDrawLogs", new[] { "UID" });
            DropIndex("dbo.LotteryDrawLogs", new[] { "LotteryGameId" });
            DropIndex("dbo.LotteryCoinLogs", new[] { "FriendUID" });
            DropIndex("dbo.LotteryCoinLogs", new[] { "LotteryDrawLogId" });
            DropIndex("dbo.LotteryCoinLogs", new[] { "UID" });
            DropColumn("dbo.UserDatas", "LotteryEarnedCoins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.UserDatas", "LotteryDailyCoins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropTable("dbo.LotteryGameUseStatistics",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DrawDate",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "cast(getutcdate() as date)" },
                        }
                    },
                });
            DropTable("dbo.LotteryPrices",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "AmountPickedUp",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "AmountWon",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "Unit",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "N'個'" },
                        }
                    },
                    {
                        "UseActivityCoupon",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.LotteryPriceCoupons",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "TimeWon",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.LotteryGames",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DrawType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "TestMode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.LotteryDrawLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DrawDate",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "cast(getutcdate() as date)" },
                        }
                    },
                    {
                        "DrawTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Won",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.LotteryCoinLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "Coins",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "TransactionTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddQRTokenToCoinLog : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LotteryCoinLogs", "QRTokenUsed", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LotteryCoinLogs", "QRTokenUsed");
        }
    }
}

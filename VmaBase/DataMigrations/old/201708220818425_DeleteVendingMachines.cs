namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteVendingMachines : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingMachines", "MOCCPAssetNo", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.VendingMachines", "ImageUrl", c => c.String(maxLength: 128));
            CreateIndex("dbo.VendingMachines", "MOCCPAssetNo", unique: true, name: "UIX_MOCCPAssetNo");
        }
        
        public override void Down()
        {
            DropIndex("dbo.VendingMachines", "UIX_MOCCPAssetNo");
            AlterColumn("dbo.VendingMachines", "ImageUrl", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.VendingMachines", "MOCCPAssetNo");
        }
    }
}

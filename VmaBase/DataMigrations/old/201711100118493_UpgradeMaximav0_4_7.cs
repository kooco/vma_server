namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UpgradeMaximav0_4_7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserBases", "ModifyTime", c => c.DateTime(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                    },
                }));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserBases", "ModifyTime",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "getutcdate()" },
                });
        }
    }
}

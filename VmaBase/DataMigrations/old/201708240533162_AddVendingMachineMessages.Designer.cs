// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddVendingMachineMessages : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddVendingMachineMessages));
        
        string IMigrationMetadata.Id
        {
            get { return "201708240533162_AddVendingMachineMessages"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

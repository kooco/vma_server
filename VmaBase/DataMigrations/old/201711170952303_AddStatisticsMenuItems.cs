﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddStatisticsMenuItems : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("USRSTATS", "Statistics", "Users", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "會員統計");
            AddApplicationAction("LOTSTATS", "Statistics", "Lottery", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "幸運汽水機統計");
            AddApplicationAction("SALESTATS", "Statistics", "Sales", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "銷售統計");
            AddApplicationAction("ACTSTATS", "Statistics", "Activity", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "活動統計");

            AddMenuItem(49000, ParentId: null, ActionCode: "USRSTATS", Name: "統計表", Sort: 990, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(49001, ParentId: 49000, ActionCode: "USRSTATS", Name: "會員統計", Sort: 991, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(49002, ParentId: 49000, ActionCode: "LOTSTATS", Name: "幸運汽水機統計", Sort: 992, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(49003, ParentId: 49000, ActionCode: "SALESTATS", Name: "銷售統計", Sort: 993, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(49004, ParentId: 49000, ActionCode: "ACTSTATS", Name: "活動統計", Sort: 994, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(49004);
            RemoveMenuItem(49002);
            RemoveMenuItem(49003);
            RemoveMenuItem(49001);
            RemoveMenuItem(49000);

            RemoveApplicationAction("USRSTATS");
            RemoveApplicationAction("LOTSTATS");
            RemoveApplicationAction("SALESTATS");
        }
    }
}

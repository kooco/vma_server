namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddSP_CompleteMachineOrder : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SalesTransactions", new[] { "UID" });
            AlterColumn("dbo.SalesTransactions", "UID", c => c.Long());
            CreateIndex("dbo.SalesTransactions", "UID");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_CompleteMachineOrder]");

            DropIndex("dbo.SalesTransactions", new[] { "UID" });
            AlterColumn("dbo.SalesTransactions", "UID", c => c.Long(nullable: false));
            CreateIndex("dbo.SalesTransactions", "UID");
        }
    }
}

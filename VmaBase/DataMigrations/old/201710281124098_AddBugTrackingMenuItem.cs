﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddBugTrackingMenuItem : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("BUGTRK", "BugTracking", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);

            AddMenuItem(Id: 46000, ParentId: null, ActionCode: "BUGTRK", Name: "錯誤跟踪", Sort: 3000, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(Id: 46001, ParentId: 46000, ActionCode: "BUGTRK", Name: "錯誤跟踪", Sort: 3001, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(46001);
            RemoveMenuItem(46000);

            RemoveApplicationAction("BUGTRK");
        }
    }
}

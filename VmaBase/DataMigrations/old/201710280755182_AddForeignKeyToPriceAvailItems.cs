namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddForeignKeyToPriceAvailItems : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.LotteryPriceAvailabilityItems", "WonByUID");
            AddForeignKey("dbo.LotteryPriceAvailabilityItems", "WonByUID", "dbo.UserDatas", "UID");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryDrawGame_v5.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryDrawGame_v4.sql"));

            DropForeignKey("dbo.LotteryPriceAvailabilityItems", "WonByUID", "dbo.UserDatas");
            DropIndex("dbo.LotteryPriceAvailabilityItems", new[] { "WonByUID" });
        }
    }
}

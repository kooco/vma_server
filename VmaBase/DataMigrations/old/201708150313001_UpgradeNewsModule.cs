namespace VmaBase.DataMigrations
{    
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;

    public partial class UpgradeNewsModule : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewsSubscriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        NewsId = c.Long(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewsArticles", t => t.NewsId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.UID, t.NewsId }, unique: true, name: "IX_UserNewsSubscriptions");
            
            CreateTable(
                "dbo.UserLikes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        NewsId = c.Long(),
                        MessageId = c.Long(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewsMessages", t => t.MessageId)
                .ForeignKey("dbo.NewsArticles", t => t.NewsId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.NewsId)
                .Index(t => t.MessageId);
            
            AlterColumn("dbo.NewsMessages", "Message", c => c.String(nullable: false, maxLength: 200));

            NewsModuleMigrationHelper.ApplyAdditionalNewsModuleDbChanges_0_2_90(this);
        }
        
        public override void Down()
        {
            NewsModuleMigrationHelper.RevertAdditionalNewsModuleDbChanges_0_2_90(this);

            DropForeignKey("dbo.UserLikes", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.UserLikes", "NewsId", "dbo.NewsArticles");
            DropForeignKey("dbo.UserLikes", "MessageId", "dbo.NewsMessages");
            DropForeignKey("dbo.NewsSubscriptions", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.NewsSubscriptions", "NewsId", "dbo.NewsArticles");
            DropIndex("dbo.UserLikes", new[] { "MessageId" });
            DropIndex("dbo.UserLikes", new[] { "NewsId" });
            DropIndex("dbo.UserLikes", new[] { "UID" });
            DropIndex("dbo.NewsSubscriptions", "IX_UserNewsSubscriptions");
            AlterColumn("dbo.NewsMessages", "Message", c => c.String(nullable: false, maxLength: 50));
            DropTable("dbo.UserLikes");
            DropTable("dbo.NewsSubscriptions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

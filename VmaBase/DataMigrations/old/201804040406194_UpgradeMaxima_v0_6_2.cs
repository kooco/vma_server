namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima_v0_6_2 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogSources",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Provider = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        Name = c.String(nullable: false, maxLength: 100),
                        Parameters = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_6_0(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbchangesV0_6_0(this);

            DropTable("dbo.LogSources",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "Provider",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

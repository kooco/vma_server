﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameLotteryPricesMenuItem : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [dbo].[ApplicationMenuItems] SET [Name] = N'兌獎紀錄' WHERE [Id] = 45001");
        }
        
        public override void Down()
        {
            Sql("UPDATE [dbo].[ApplicationMenuItems] SET [Name] = N'贏獎品' WHERE [Id] = 45001");
        }
    }
}

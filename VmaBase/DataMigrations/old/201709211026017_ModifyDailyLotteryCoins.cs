namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class ModifyDailyLotteryCoins : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LotteryCoinLogs", "Type", c => c.Int(nullable: false));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddDailyLotteryCoins_v2.sql"));
        }
        
        public override void Down()
        {            
            Sql("DROP PROCEDURE [dbo].[sp_AddDailyLotteryCoins]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddDailyLotteryCoins_v1.sql"));

            DropColumn("dbo.LotteryCoinLogs", "Type");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddStatisticsTables : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StatisticsItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StatisticsSetId = c.Long(nullable: false),
                        Name = c.String(maxLength: 80),
                        AggregateType = c.Int(nullable: false),
                        Group1Name = c.String(maxLength: 32),
                        Group2Name = c.String(maxLength: 32),
                        Group3Name = c.String(maxLength: 32),
                        LastUpdateTime = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StatisticsSets", t => t.StatisticsSetId)
                .Index(t => new { t.StatisticsSetId, t.AggregateType, t.Group1Name, t.Group2Name, t.Group3Name }, name: "IX_Set_Groups_AggregateType");
            
            CreateTable(
                "dbo.StatisticsSets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 64),
                        LastUpdateTime = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StatisticsValues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StatisticsItemId = c.Long(nullable: false),
                        Group1Value = c.String(maxLength: 32),
                        Group2Value = c.String(maxLength: 32),
                        Group3Value = c.String(maxLength: 32),
                        Year = c.Int(nullable: false),
                        PeriodType = c.Int(nullable: false),
                        PeriodValue = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StatisticsItems", t => t.StatisticsItemId)
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Year, t.PeriodType, t.PeriodValue }, name: "IX_Item_GroupValue1_Period")
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Group2Value, t.Group3Value, t.PeriodType, t.PeriodValue }, name: "IX_Item_GroupValues_Day")
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Group2Value, t.Group3Value, t.Year, t.PeriodType, t.PeriodValue }, unique: true, name: "UIX_Item_GroupValues_Period");

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_5(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_5(this);

            DropForeignKey("dbo.StatisticsValues", "StatisticsItemId", "dbo.StatisticsItems");
            DropForeignKey("dbo.StatisticsItems", "StatisticsSetId", "dbo.StatisticsSets");
            DropIndex("dbo.StatisticsValues", "UIX_Item_GroupValues_Period");
            DropIndex("dbo.StatisticsValues", "IX_Item_GroupValues_Day");
            DropIndex("dbo.StatisticsValues", "IX_Item_GroupValue1_Period");
            DropIndex("dbo.StatisticsItems", "IX_Set_Groups_AggregateType");
            DropTable("dbo.StatisticsValues",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.StatisticsSets",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.StatisticsItems",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

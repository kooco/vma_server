namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddUIDToUserStampsLog : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM [dbo].[UserStampLogs]");

            AddColumn("dbo.UserStampLogs", "UID", c => c.Long(nullable: false));
            CreateIndex("dbo.UserStampLogs", "UID");
            AddForeignKey("dbo.UserStampLogs", "UID", "dbo.UserDatas", "UID");
            
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v5.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v6.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v5.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v4.sql"));

            DropForeignKey("dbo.UserStampLogs", "UID", "dbo.UserDatas");
            DropIndex("dbo.UserStampLogs", new[] { "UID" });
            DropColumn("dbo.UserStampLogs", "UID");            
        }
    }
}

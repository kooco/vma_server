// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UpgradeMaxima0_4_59 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpgradeMaxima0_4_59));
        
        string IMigrationMetadata.Id
        {
            get { return "201711200538503_UpgradeMaxima0_4_59"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

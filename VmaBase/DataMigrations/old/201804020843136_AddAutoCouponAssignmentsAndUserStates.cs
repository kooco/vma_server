namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddAutoCouponAssignmentsAndUserStates : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AutoCouponAssignments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CriteriaId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        AssignmentTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UserCouponId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AutoCouponCriterias", t => t.CriteriaId)
                .ForeignKey("dbo.UserCoupons", t => t.UserCouponId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.CriteriaId, t.UID }, unique: true, name: "UIX_Criteria_User")
                .Index(t => t.UserCouponId);
            
            CreateTable(
                "dbo.AutoCouponUserStates",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CriteriaId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        Counter = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        DateValue = c.DateTime(),
                        Settings = c.String(maxLength: 2048),
                        Blob = c.String(storeType: "ntext"),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AutoCouponCriterias", t => t.CriteriaId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.CriteriaId, t.UID }, unique: true, name: "UIX_Criteria_User");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AutoCouponUserStates", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.AutoCouponUserStates", "CriteriaId", "dbo.AutoCouponCriterias");
            DropForeignKey("dbo.AutoCouponAssignments", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.AutoCouponAssignments", "UserCouponId", "dbo.UserCoupons");
            DropForeignKey("dbo.AutoCouponAssignments", "CriteriaId", "dbo.AutoCouponCriterias");
            DropIndex("dbo.AutoCouponUserStates", "UIX_Criteria_User");
            DropIndex("dbo.AutoCouponAssignments", new[] { "UserCouponId" });
            DropIndex("dbo.AutoCouponAssignments", "UIX_Criteria_User");
            DropTable("dbo.AutoCouponUserStates",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "Counter",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.AutoCouponAssignments",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "AssignmentTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

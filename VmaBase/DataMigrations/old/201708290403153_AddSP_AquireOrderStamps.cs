namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddSP_AquireOrderStamps : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AddUserStamp_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.FunctionScripts.fnGenerateQRToken_v2.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP FUNCTION [dbo].[fnGenerateQRToken]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.FunctionScripts.fnGenerateQRToken_v1.sql"));

            Sql("DROP PROCEDURE [dbo].[sp_AddUserStamp]");
            Sql("DROP PROCEDURE [dbo].[sp_AcquireOrderStamps]");
        }
    }
}

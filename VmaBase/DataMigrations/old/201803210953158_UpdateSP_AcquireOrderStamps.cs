namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using Kooco.Framework.Shared;

    public partial class UpdateSP_AcquireOrderStamps : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v7.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.AcquireOrderStamps_v6.sql"));
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Models.Enum;

    public partial class AddWatersportsStatistics : FrameworkDbMigration
    {
        public override void Up()
        {
            AddStatisticsItem((long)StatisticsItems.Watersports_DrawedGames, (long)StatisticSets.Lottery, "Game Draws", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Watersports_PlayersCount, (long)StatisticSets.Lottery, "Players Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Watersports_WonPricesCount, (long)StatisticSets.Lottery, "Won Prices Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "獎品", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Watersports_RedeemedPricesCount, (long)StatisticSets.Lottery, "Redeemed Prices Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "獎品", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
        }
        
        public override void Down()
        {
            RemoveStatisticsItem((long)StatisticsItems.Watersports_DrawedGames);
            RemoveStatisticsItem((long)StatisticsItems.Watersports_PlayersCount);
            RemoveStatisticsItem((long)StatisticsItems.Watersports_WonPricesCount);
            RemoveStatisticsItem((long)StatisticsItems.Watersports_RedeemedPricesCount);            
        }
    }
}

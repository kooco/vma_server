namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddJPushMessageLogsTable : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JPushMessageLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        VendingMachineId = c.Long(nullable: false),
                        RegistrationId = c.String(nullable: false, maxLength: 128),
                        SentTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        Message = c.String(nullable: false, maxLength: 512),
                        MessageId = c.Long(nullable: false),
                        SendNo = c.Long(nullable: false),
                        Response = c.String(maxLength: 512),
                        RetryCount = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SentTime, name: "IX_PushMessageDate")
                .Index(t => t.Status, name: "IX_PushMessageStatus");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.JPushMessageLogs", "IX_PushMessageStatus");
            DropIndex("dbo.JPushMessageLogs", "IX_PushMessageDate");
            DropTable("dbo.JPushMessageLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "RetryCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "SentTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

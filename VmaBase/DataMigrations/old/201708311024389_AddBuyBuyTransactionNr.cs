namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddBuyBuyTransactionNr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SalesTransactions", "BuyBuyTransactionNr", c => c.String(maxLength: 32));
            Sql("CREATE UNIQUE INDEX [UIX_SalesTransaction_BuyBuyTransactionNr] ON [dbo].[SalesTransactions]([BuyBuyTransactionNr]) WHERE [BuyBuyTransactionNr] IS NOT NULL");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v4.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v3.sql"));

            Sql("DROP INDEX [UIX_SalesTransaction_BuyBuyTransactionNr] ON [dbo].[SalesTransactions]");
            DropColumn("dbo.SalesTransactions", "BuyBuyTransactionNr");
        }
    }
}

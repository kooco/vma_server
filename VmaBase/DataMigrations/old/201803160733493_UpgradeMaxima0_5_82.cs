namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima0_5_82 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.RemoveUserRolePermissionIndexes_0_5_40(this);

            DropForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets");
            DropIndex("dbo.UserRolePermissions", new[] { "PermissionSetCode" });
            DropPrimaryKey("dbo.ApplicationPermissionSets");
            CreateTable(
                "dbo.ApplicationModules",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 8),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Code)
                .Index(t => t.Name, unique: true);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_10(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_17(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_38(this);

            CreateTable(
                "dbo.UserPushNotificationLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(),
                        TimeSent = c.DateTime(nullable: false),
                        DeviceType = c.Int(nullable: false),
                        Title = c.String(maxLength: 128),
                        Message = c.String(nullable: false, maxLength: 512),
                        Contents = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.TimeSent);
            
            AddColumn("dbo.ApplicationActions", "ModuleCode", c => c.String(nullable: false, maxLength: 8,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "'A'")
                    },
                }));
            AddColumn("dbo.ApplicationPermissionSets", "ModuleCode", c => c.String(nullable: false, maxLength: 8,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "'A'")
                    },
                }));

            AlterColumn("dbo.ApplicationPermissionSets", "Code", c => c.String(nullable: false, maxLength: 32, unicode: false));
            AlterColumn("dbo.ApplicationSettings", "SettingValue", c => c.String(nullable: false, maxLength: 2048));
            AlterColumn("dbo.UserRolePermissions", "PermissionSetCode", c => c.String(maxLength: 32, unicode: false));
            AddPrimaryKey("dbo.ApplicationPermissionSets", "Code");
            CreateIndex("dbo.ApplicationActions", "ModuleCode");
            CreateIndex("dbo.ApplicationPermissionSets", "ModuleCode");
            CreateIndex("dbo.UserRolePermissions", "PermissionSetCode");
            AddForeignKey("dbo.ApplicationActions", "ModuleCode", "dbo.ApplicationModules", "Code");
            AddForeignKey("dbo.ApplicationPermissionSets", "ModuleCode", "dbo.ApplicationModules", "Code");
            AddForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets", "Code");

            FrameworkMigrationHelper.AddUserRolePermissionIndex_0_5_40(this);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_43(this);
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_45(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_5_10(this);
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_5_17(this);

            FrameworkMigrationHelper.RemoveUserRolePermissionIndexes_0_5_40(this);

            DropForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets");
            DropForeignKey("dbo.UserPushNotificationLogs", "UID", "dbo.UserBases");
            DropForeignKey("dbo.ApplicationPermissionSets", "ModuleCode", "dbo.ApplicationModules");
            DropForeignKey("dbo.ApplicationActions", "ModuleCode", "dbo.ApplicationModules");
            DropIndex("dbo.UserRolePermissions", new[] { "PermissionSetCode" });
            DropIndex("dbo.UserPushNotificationLogs", new[] { "TimeSent" });
            DropIndex("dbo.UserPushNotificationLogs", new[] { "UID" });
            DropIndex("dbo.ApplicationPermissionSets", new[] { "ModuleCode" });
            DropIndex("dbo.ApplicationModules", new[] { "Name" });
            DropIndex("dbo.ApplicationActions", new[] { "ModuleCode" });
            DropPrimaryKey("dbo.ApplicationPermissionSets");
            AlterColumn("dbo.UserRolePermissions", "PermissionSetCode", c => c.String(maxLength: 12, unicode: false));
            AlterColumn("dbo.ApplicationSettings", "SettingValue", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.ApplicationPermissionSets", "Code", c => c.String(nullable: false, maxLength: 12, unicode: false));
            DropColumn("dbo.ApplicationPermissionSets", "ModuleCode",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "'A'" },
                });
            DropColumn("dbo.ApplicationActions", "ModuleCode",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "'A'" },
                });

            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_5_38(this);
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_5_43(this);
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_5_45(this);

            DropTable("dbo.UserPushNotificationLogs");
            DropTable("dbo.ApplicationModules");
            AddPrimaryKey("dbo.ApplicationPermissionSets", "Code");
            CreateIndex("dbo.UserRolePermissions", "PermissionSetCode");
            AddForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets", "Code");

            FrameworkMigrationHelper.AddUserRolePermissionIndex_0_5_40(this);
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePromotionProductUIX : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PromotionProducts", "UIX_PromotionProductNumber");
            Sql("CREATE UNIQUE INDEX [UIX_PromotionProductNumber] ON [dbo].[PromotionProducts]([ProductNumber]) WHERE [Status] <> -1");
        }
        
        public override void Down()
        {
            Sql("DROP INDEX [UIX_PromotionProductNumber] ON [dbo].[PromotionProducts]");
            CreateIndex("dbo.PromotionProducts", "ProductNumber", unique: true, name: "UIX_PromotionProductNumber");
        }
    }
}

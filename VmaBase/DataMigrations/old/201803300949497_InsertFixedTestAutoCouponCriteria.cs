namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class InsertFixedTestAutoCouponCriteria : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("set IDENTITY_INSERT [dbo].[AutoCouponCriterias] ON " + 
                "INSERT INTO [dbo].[AutoCouponCriterias] ([Id], [Name],    [CouponsTitle],               [CouponsSubTitle], [ModuleId], [UID], [CouponsForAllProducts], [CouponsForAllMachines], [LimitStartTime], [LimitEndTime], [Status])" +
                                                " Values (1,    N'System', N'Module Tester and Creator', N'',               null,       null,  0,                       0,                       getutcdate(),     '2099-12-31',   1) " +
                "set IDENTITY_INSERT [dbo].[AutoCouponCriterias] OFF");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM [dbo].[AutoCouponCriterias] WHERE [Id] = 1");
        }
    }
}

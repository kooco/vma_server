﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using Kooco.Framework.Models.Enum;
    using VmaBase.Shared;

    public partial class AddWatersportsPrices : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[LotteryPrices] ([LotteryGameId], [Title], [ImageUrl], [Unit], [PickupAddress], [PickupPhone], [UseActivityCoupon], [UseSpreadingParameters], [AmountAvailable], [Status]) " +
                                          " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", N'運動用品現金券$5,000', 'http://vma-admin-jp.azurewebsites.net/images/noimage.png', N'份', '-', '-', 0, 1, 2, " + ((int)GeneralStatusEnum.Active).ToString() + ")");

            Sql("INSERT INTO [dbo].[LotteryPrices] ([LotteryGameId], [Title], [ImageUrl], [Unit], [PickupAddress], [PickupPhone], [UseActivityCoupon], [UseSpreadingParameters], [AmountAvailable], [Status]) " +
                                          " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", N'\"GARMIN\"運動手錶', 'http://vma-admin-jp.azurewebsites.net/images/noimage.png', N'份', '-', '-', 0, 1, 6, " + ((int)GeneralStatusEnum.Active).ToString() + ")");

            Sql("INSERT INTO [dbo].[LotteryPrices] ([LotteryGameId], [Title], [ImageUrl], [Unit], [PickupAddress], [PickupPhone], [UseActivityCoupon], [UseSpreadingParameters], [AmountAvailable], [Status]) " +
                                          " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", N'「水動樂」系列飲品1支', 'http://vma-admin-jp.azurewebsites.net/images/noimage.png', N'份', '-', '-', 0, 1, 3300, " + ((int)GeneralStatusEnum.Active).ToString() + ")");

            // spreading parameters for the first prize
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%運動用品現金%'), '2018-09-01 16:00:00', '2018-09-08 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%運動用品現金%'), '2018-09-09 16:00:00', '2018-09-15 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%運動用品現金%'), '2018-09-30 16:00:00', '2018-10-06 15:59:59.997', 1)");

            // spreading parameters for the second prize
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-09-01 16:00:00', '2018-09-08 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-09-09 16:00:00', '2018-09-15 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-09-16 16:00:00', '2018-09-22 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-09-23 16:00:00', '2018-09-29 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-09-30 16:00:00', '2018-10-06 15:59:59.997', 1)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%GARMIN%'), '2018-10-07 16:00:00', '2018-10-13 15:59:59.997', 1)");

            // spreading parameters for the aqua prize
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-09-01 16:00:00', '2018-09-08 15:59:59.997', 500)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-09-09 16:00:00', '2018-09-15 15:59:59.997', 500)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-09-16 16:00:00', '2018-09-22 15:59:59.997', 500)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-09-23 16:00:00', '2018-09-29 15:59:59.997', 350)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-09-30 16:00:00', '2018-10-06 15:59:59.997', 350)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-10-07 16:00:00', '2018-10-13 15:59:59.997', 350)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-10-14 16:00:00', '2018-10-20 15:59:59.997', 350)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-10-21 16:00:00', '2018-10-27 15:59:59.997', 200)");
            Sql("INSERT INTO [dbo].[LotteryPriceSpreadingParameters] ([LotteryGameId], [LotteryPriceId], [PeriodFrom], [PeriodTo], [SpreadCount]) " +
                                                " Values (" + GlobalConst.LotteryGameId_Watersports.ToString() + ", (select [Id] from [dbo].[LotteryPrices] where [Title] like N'%飲品%'), '2018-10-28 16:00:00', '2018-10-31 15:59:59.997', 200)");
        }

        public override void Down()
        {
            Sql("DELETE FROM [dbo].[LotteryPriceSpreadingParameters] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("DELETE FROM [dbo].[LotteryPriceCoupons] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("DELETE FROM [dbo].[LotteryPriceAvailabilityItems] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
            Sql("DELETE FROM [dbo].[LotteryPrices] WHERE [LotteryGameId] = " + GlobalConst.LotteryGameId_Watersports.ToString());
        }
    }
}

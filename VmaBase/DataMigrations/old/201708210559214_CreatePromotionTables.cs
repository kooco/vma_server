﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;

    public partial class CreatePromotionTables : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromotionOrders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        OrderNumber = c.String(nullable: false, maxLength: 12),
                        PointsExchanged = c.Int(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        OrderNotes = c.String(maxLength: 255),
                        RefundReason = c.Int(nullable: false),
                        ExchangeTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ExpiryTime = c.DateTime(nullable: false),
                        PickupTime = c.DateTime(),
                        RefundTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PromotionProducts", t => t.ProductId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.ProductId)
                .Index(t => t.OrderNumber, unique: true, name: "UIX_OrderNumber");
            
            CreateTable(
                "dbo.PromotionProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductNumber = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        ImageUrl = c.String(maxLength: 128),
                        Points = c.Int(nullable: false),
                        InStock = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProductNumber, unique: true, name: "UIX_PromotionProductNumber");
            
            AddColumn("dbo.PointsTransactions", "PromotionOrderId", c => c.Long());
            CreateIndex("dbo.PointsTransactions", "PromotionOrderId");
            AddForeignKey("dbo.PointsTransactions", "PromotionOrderId", "dbo.PromotionOrders", "Id");

            AddApplicationSetting("PromotionOrderExpiryInMonths", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "Expiry in month for promotion products (禮品)", "Amount of months after which promotion product exchanges will expire", NumRangeFrom: 1, NumRangeTo: 24, InitialValue: "3");

            AddApplicationAction("PRPROD", "PromotionProducts", "Index");
            AddApplicationAction("PRORDR", "PromotionProducts", "Orders");
            AddMenuItem(43000, ParentId: null, ActionCode: "PRPROD", Name: "禮品", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(43001, ParentId: 43000, ActionCode: "PRPROD", Name: "禮品管理", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(43002, ParentId: 43000, ActionCode: "PRORDR", Name: "兌換列表", Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Inactive);

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UseUserPoints_v2.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_UseUserPoints]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UseUserPoints_v1.sql"));

            RemoveMenuItem(43002);
            RemoveMenuItem(43001);
            RemoveMenuItem(43000);
            RemoveApplicationAction("PRORDR");
            RemoveApplicationAction("PRPROD");

            RemoveApplicationSetting(SettingKey: "PromotionOrderExpiryInMonths");

            DropForeignKey("dbo.PointsTransactions", "PromotionOrderId", "dbo.PromotionOrders");
            DropForeignKey("dbo.PromotionOrders", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.PromotionOrders", "ProductId", "dbo.PromotionProducts");
            DropIndex("dbo.PromotionProducts", "UIX_PromotionProductNumber");
            DropIndex("dbo.PromotionOrders", "UIX_OrderNumber");
            DropIndex("dbo.PromotionOrders", new[] { "ProductId" });
            DropIndex("dbo.PromotionOrders", new[] { "UID" });
            DropIndex("dbo.PointsTransactions", new[] { "PromotionOrderId" });
            DropColumn("dbo.PointsTransactions", "PromotionOrderId");
            DropTable("dbo.PromotionProducts",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "InStock",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.PromotionOrders",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ExchangeTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class FixActivityLists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponActivities", "Status", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v2.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_GetCouponActivityList]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v1.sql"));

            DropColumn("dbo.CouponActivities", "Status",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddRegistrationIdToVendingMachine : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingMachines", "RegistrationId", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VendingMachines", "RegistrationId");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddPromotionListFlagToActivities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponActivities", "ShowOnPromotionMachinesList", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CheckMachinesRecommended_v3.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v8.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v7.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CheckMachinesRecommended_v2.sql"));

            DropColumn("dbo.CouponActivities", "ShowOnPromotionMachinesList",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
        }
    }
}

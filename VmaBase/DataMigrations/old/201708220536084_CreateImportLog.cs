namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;

    public partial class CreateImportLog : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportLogMessages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ImportLogId = c.Long(nullable: false),
                        Message = c.String(nullable: false, maxLength: 200),
                        LogTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ImportLogs", t => t.ImportLogId)
                .Index(t => t.ImportLogId);
            
            CreateTable(
                "dbo.ImportLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ImportType = c.Int(nullable: false),
                        RunNumber = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        StartType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        StartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        CompletionTime = c.DateTime(),
                        ExecutionTime = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ImportType, t.RunNumber }, unique: true, name: "UIX_TypeRunNumber")
                .Index(t => t.StartTime);

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CleanImportLog_v1.sql"));

            AddApplicationSetting("ImportLogMaxAgeDays", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "Import Logs - Maximum age in days", "How long to keep the import logs (days) before they will be deleted automatically", NumRangeFrom: 7, NumRangeTo: 1065, InitialValue: "30");
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("ImportLogMaxAgeDays");

            Sql("DROP PROCEDURE [dbo].[sp_CleanImportLog]");

            DropForeignKey("dbo.ImportLogMessages", "ImportLogId", "dbo.ImportLogs");
            DropIndex("dbo.ImportLogs", new[] { "StartTime" });
            DropIndex("dbo.ImportLogs", "UIX_TypeRunNumber");
            DropIndex("dbo.ImportLogMessages", new[] { "ImportLogId" });
            DropTable("dbo.ImportLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ExecutionTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "StartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "StartType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            DropTable("dbo.ImportLogMessages",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "LogTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

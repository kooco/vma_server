namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Kooco.Framework.Models.Enum;

    public partial class UpgradeMaxima0_4_59 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_59(this);

            Sql("INSERT INTO [dbo].[UserLoginLogs] ([Time], [UID], [Type], [UserToken], [Account], [Phone], [MobileDeviceType], [LoginType], [ReturnCode])" +
                                          " SELECT [CreateTime], [UID], " + ((int)UserLoginLogType.RegistrationSuccessful).ToString() + ", null, [Account], [Phone], 0, 0, 1 FROM [dbo].[UserBases] WHERE [Status] <> " + ((int)UserStatus.Pending).ToString());
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_59(this);
        }
    }
}

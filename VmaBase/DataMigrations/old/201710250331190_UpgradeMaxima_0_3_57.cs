namespace VmaBase.DataMigrations
{    
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    public partial class UpgradeMaxima_0_3_57 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_3_57(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_3_57(this);
        }
    }
}

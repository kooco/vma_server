// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlterSP_LotteryDrawGame : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterSP_LotteryDrawGame));
        
        string IMigrationMetadata.Id
        {
            get { return "201711080356578_AlterSP_LotteryDrawGame"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActivityImageToActivities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponActivities", "ActivityListImageUrl", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CouponActivities", "ActivityListImageUrl");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    using Models.Enum;

    public partial class AddRetentionRateStatistic : FrameworkDbMigration
    {
        public override void Up()
        {
            AddStatisticsItem((long)StatisticsItems.Users_RetentionRate, (long)StatisticSets.Users, AggregateType: Kooco.Framework.Models.Enum.StatisticsAggregateType.Average, Group1Name: "Day-Range", Name: "User Retention Rate");
        }
        
        public override void Down()
        {
            RemoveStatisticsItem((long)StatisticsItems.Users_RetentionRate);
        }
    }
}

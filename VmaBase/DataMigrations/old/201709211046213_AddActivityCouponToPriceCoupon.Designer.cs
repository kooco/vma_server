// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddActivityCouponToPriceCoupon : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddActivityCouponToPriceCoupon));
        
        string IMigrationMetadata.Id
        {
            get { return "201709211046213_AddActivityCouponToPriceCoupon"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserRenamePassportNo : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserDatas", "UK_PassportNo");
            AddColumn("dbo.UserDatas", "MacauPassId", c => c.String(maxLength: 8));
            DropColumn("dbo.UserDatas", "PassportIDNumber");

            Sql("CREATE UNIQUE INDEX [UIX_User_MacauPassId] ON [dbo].[UserDatas]([MacauPassId]) WHERE [MacauPassId] IS NOT NULL");
        }
        
        public override void Down()
        {
            Sql("DROP INDEX [UIX_User_MacauPassId] ON [dbo].[UserDatas]");

            AddColumn("dbo.UserDatas", "PassportIDNumber", c => c.String(nullable: false, maxLength: 8));
            DropColumn("dbo.UserDatas", "MacauPassId");
            CreateIndex("dbo.UserDatas", "PassportIDNumber", unique: true, name: "UK_PassportNo");
        }
    }
}

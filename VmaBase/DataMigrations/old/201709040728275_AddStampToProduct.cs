namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStampToProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingProducts", "DefaultStampTypeId", c => c.Long());
            CreateIndex("dbo.VendingProducts", "DefaultStampTypeId");
            AddForeignKey("dbo.VendingProducts", "DefaultStampTypeId", "dbo.StampTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendingProducts", "DefaultStampTypeId", "dbo.StampTypes");
            DropIndex("dbo.VendingProducts", new[] { "DefaultStampTypeId" });
            DropColumn("dbo.VendingProducts", "DefaultStampTypeId");
        }
    }
}

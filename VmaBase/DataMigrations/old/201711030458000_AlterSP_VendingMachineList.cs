namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AlterSP_VendingMachineList : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ActivityParticipatingMachines", "VendingMachineId", name: "IX_VendingMachines");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v6.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineListCount_v3.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineList_v5.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetVendingMachineListCount_v2.sql"));

            DropIndex("dbo.ActivityParticipatingMachines", "IX_VendingMachines");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddAutoCouponTables : FrameworkDbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserCoupons", "IX_ActivityUser");
            CreateTable(
                "dbo.AutoCouponCriteriaMachines",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CriteriaId = c.Long(nullable: false),
                        MachineId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AutoCouponCriterias", t => t.CriteriaId)
                .ForeignKey("dbo.VendingMachines", t => t.MachineId)
                .Index(t => t.CriteriaId, unique: true, name: "UIX_Criteria_Machine")
                .Index(t => t.MachineId);
            
            CreateTable(
                "dbo.AutoCouponCriterias",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        CouponsTitle = c.String(nullable: false, maxLength: 64),
                        CouponsSubTitle = c.String(maxLength: 100),
                        ModuleId = c.Long(),
                        UID = c.Long(),
                        CouponsExpiryDate = c.DateTime(),
                        CouponsForAllProducts = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CouponsForAllMachines = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        LimitStartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "dateadd(day, 7, getutcdate())")
                                },
                            }),
                        LimitEndTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "dateadd(day, 28, getutcdate())")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "9")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AutoCouponModules", t => t.ModuleId)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.ModuleId)
                .Index(t => new { t.UID, t.Status });
            
            CreateTable(
                "dbo.AutoCouponModules",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(nullable: false, maxLength: 200),
                        CsharpCode = c.String(nullable: false, storeType: "ntext"),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "9")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        CreateUserUID = c.Long(),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyUserUID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Status);
            
            CreateTable(
                "dbo.AutoCouponCriteriaProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CriteriaId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AutoCouponCriterias", t => t.CriteriaId)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .Index(t => t.CriteriaId, unique: true, name: "UIX_Criteria_Product")
                .Index(t => t.ProductId);
            
            AddColumn("dbo.UserCoupons", "Type", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            AddColumn("dbo.UserCoupons", "AutoCouponCriteriaId", c => c.Long());
            AlterColumn("dbo.UserCoupons", "ActivityId", c => c.Long());
            CreateIndex("dbo.UserCoupons", new[] { "ActivityId", "UID" }, clustered: true, name: "IX_ActivityUser");
            CreateIndex("dbo.UserCoupons", "AutoCouponCriteriaId");
            AddForeignKey("dbo.UserCoupons", "AutoCouponCriteriaId", "dbo.AutoCouponCriterias", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCoupons", "AutoCouponCriteriaId", "dbo.AutoCouponCriterias");
            DropForeignKey("dbo.AutoCouponCriteriaProducts", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.AutoCouponCriteriaProducts", "CriteriaId", "dbo.AutoCouponCriterias");
            DropForeignKey("dbo.AutoCouponCriteriaMachines", "MachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.AutoCouponCriteriaMachines", "CriteriaId", "dbo.AutoCouponCriterias");
            DropForeignKey("dbo.AutoCouponCriterias", "UID", "dbo.UserBases");
            DropForeignKey("dbo.AutoCouponCriterias", "ModuleId", "dbo.AutoCouponModules");
            DropIndex("dbo.UserCoupons", new[] { "AutoCouponCriteriaId" });
            DropIndex("dbo.UserCoupons", "IX_ActivityUser");
            DropIndex("dbo.AutoCouponCriteriaProducts", new[] { "ProductId" });
            DropIndex("dbo.AutoCouponCriteriaProducts", "UIX_Criteria_Product");
            DropIndex("dbo.AutoCouponModules", new[] { "Status" });
            DropIndex("dbo.AutoCouponCriterias", new[] { "UID", "Status" });
            DropIndex("dbo.AutoCouponCriterias", new[] { "ModuleId" });
            DropIndex("dbo.AutoCouponCriteriaMachines", new[] { "MachineId" });
            DropIndex("dbo.AutoCouponCriteriaMachines", "UIX_Criteria_Machine");
            AlterColumn("dbo.UserCoupons", "ActivityId", c => c.Long(nullable: false));
            DropColumn("dbo.UserCoupons", "AutoCouponCriteriaId");
            DropColumn("dbo.UserCoupons", "Type",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropTable("dbo.AutoCouponCriteriaProducts");
            DropTable("dbo.AutoCouponModules",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "9" },
                        }
                    },
                });
            DropTable("dbo.AutoCouponCriterias",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CouponsForAllMachines",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "CouponsForAllProducts",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LimitEndTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "dateadd(day, 28, getutcdate())" },
                        }
                    },
                    {
                        "LimitStartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "dateadd(day, 7, getutcdate())" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "9" },
                        }
                    },
                });
            DropTable("dbo.AutoCouponCriteriaMachines");
            CreateIndex("dbo.UserCoupons", new[] { "ActivityId", "UID" }, clustered: true, name: "IX_ActivityUser");
        }
    }
}

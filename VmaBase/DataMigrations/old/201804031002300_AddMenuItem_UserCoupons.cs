﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddMenuItem_UserCoupons : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("USRCP", "UserCoupons", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "發送的飲料卷");

            AddMenuItem(40010, ParentId: 40000, ActionCode: "USRCP", Name: "發送的飲料卷", Sort: 520, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(40010);
            RemoveApplicationAction("USRCP");
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    using Shared;    
    using Models.Enum;

    public partial class AddStatisticItems : FrameworkDbMigration
    {
        public override void Up()
        {
            AddStatisticsSet((long)StatisticSets.Users, "會員");
            AddStatisticsSet((long)StatisticSets.Lottery, "幸運汽水機");
            AddStatisticsSet((long)StatisticSets.Sales, "銷售");
            AddStatisticsSet((long)StatisticSets.Activity, "銷售");

            // User Statistics Items
            AddStatisticsItem((long)StatisticsItems.Users_LoginCount, (long)StatisticSets.Users, "Login Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Users_NewRegistrations, (long)StatisticSets.Users, "New Registrations", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Users_UserCount, (long)StatisticSets.Users, "Current User Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);

            // Lottery Statistics Items
            AddStatisticsItem((long)StatisticsItems.Lottery_DrawedGames, (long)StatisticSets.Lottery, "Game Draws", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Lottery_PlayersCount, (long)StatisticSets.Lottery, "Players Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Lottery_WonPricesCount, (long)StatisticSets.Lottery, "Won Prices Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "獎品", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Lottery_RedeemedPricesCount, (long)StatisticSets.Lottery, "Redeemed Prices Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "獎品", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);

            // Sales Statistics Items
            AddStatisticsItem((long)StatisticsItems.Sales_SoldItemsCount, (long)StatisticSets.Sales, "Sold Products Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "販賣器", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Sales_SoldItemsCountUsingApp, (long)StatisticSets.Sales, "Sold Products Using App", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "販賣器", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);

            // Activity Statistics Items
            AddStatisticsItem((long)StatisticsItems.Activity_CollectedStampsCount, (long)StatisticSets.Activity, "Collected Stamps Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "會員", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Activity_CompletedCouponsCount, (long)StatisticSets.Activity, "Completed Coupons Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "會員", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Activity_RedeemedCouponsCount, (long)StatisticSets.Activity, "Redeemed Coupons Count", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Group1Name: "會員", Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
            AddStatisticsItem((long)StatisticsItems.Activity_UsersCountCollectingStamps, (long)StatisticSets.Activity, "Users Collecting Stamps (Count)", Kooco.Framework.Models.Enum.StatisticsAggregateType.Count, Status: Kooco.Framework.Models.Enum.StatisticsItemStatus.Active);
        }
        
        public override void Down()
        {
            RemoveStatisticsItem((long)StatisticsItems.Users_LoginCount);
            RemoveStatisticsItem((long)StatisticsItems.Users_NewRegistrations);
            RemoveStatisticsItem((long)StatisticsItems.Users_UserCount);

            RemoveStatisticsItem((long)StatisticsItems.Lottery_DrawedGames);
            RemoveStatisticsItem((long)StatisticsItems.Lottery_PlayersCount);
            RemoveStatisticsItem((long)StatisticsItems.Lottery_WonPricesCount);
            RemoveStatisticsItem((long)StatisticsItems.Lottery_RedeemedPricesCount);

            RemoveStatisticsItem((long)StatisticsItems.Sales_SoldItemsCount);
            RemoveStatisticsItem((long)StatisticsItems.Sales_SoldItemsCountUsingApp);

            RemoveStatisticsItem((long)StatisticsItems.Activity_CollectedStampsCount);
            RemoveStatisticsItem((long)StatisticsItems.Activity_CompletedCouponsCount);
            RemoveStatisticsItem((long)StatisticsItems.Activity_RedeemedCouponsCount);
            RemoveStatisticsItem((long)StatisticsItems.Activity_UsersCountCollectingStamps);

            RemoveStatisticsSet((long)StatisticSets.Users);
            RemoveStatisticsSet((long)StatisticSets.Lottery);
            RemoveStatisticsSet((long)StatisticSets.Sales);
            RemoveStatisticsSet((long)StatisticSets.Activity);
        }
    }
}

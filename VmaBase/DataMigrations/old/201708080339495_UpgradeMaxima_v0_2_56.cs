namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    using Shared;    

    public partial class UpgradeMaxima_v0_2_56 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserPushTokens",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        DeviceType = c.Int(nullable: false),
                        PushToken = c.String(nullable: false, maxLength: 100),
                        ExpiryDate = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID)
                .Index(t => new { t.UID, t.PushToken }, unique: true, name: "UIX_UID_PushToken");

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_2_2(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_2_2(this);

            DropForeignKey("dbo.UserPushTokens", "UID", "dbo.UserBases");
            DropIndex("dbo.UserPushTokens", "UIX_UID_PushToken");
            DropIndex("dbo.UserPushTokens", new[] { "UID" });
            DropTable("dbo.UserPushTokens",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

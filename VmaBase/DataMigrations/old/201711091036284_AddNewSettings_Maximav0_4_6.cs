namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddNewSettings_Maximav0_4_6 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_6(this);

            AddApplicationSetting("EnableVMQApiSuccessLog", Kooco.Framework.Models.Enum.AppSettingType.List, "API - Enable Success Logs for VendingMachineQuery API", "Flag, if logging for successful API calls of the VendingMachineQueryAPI should be logged or not (can be very much)", ListValues: new string[] { "Yes", "No" }, InitialValue: "No");
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_6(this);

            RemoveApplicationSetting("EnableVMQApiSuccessLog");
        }
    }
}

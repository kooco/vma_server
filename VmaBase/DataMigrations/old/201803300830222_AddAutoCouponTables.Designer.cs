// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddAutoCouponTables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAutoCouponTables));
        
        string IMigrationMetadata.Id
        {
            get { return "201803300830222_AddAutoCouponTables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

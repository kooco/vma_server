namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima_v0_4_52 : FrameworkDbMigration
    {
        public override void Up()
        {
            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_4_52(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_4_52(this);
        }
    }
}

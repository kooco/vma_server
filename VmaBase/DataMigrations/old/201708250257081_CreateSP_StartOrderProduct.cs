namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class CreateSP_StartOrderProduct : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE SEQUENCE [dbo].[seq_SalesTransactionReceiptNr] AS [bigint] START WITH 1 INCREMENT BY 1 MINVALUE - 9223372036854775808 MAXVALUE 9223372036854775807 CACHE ");
            Sql("create view [dbo].[vGetNewID] as select newid() as new_id");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.FunctionScripts.fnGenerateQRToken_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.StartOrderProduct_v1.sql"));
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetMachineMsgProductSelection_v1.sql")); 
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_GetMachineMsgProductSelection]");
            Sql("DROP PROCEDURE [dbo].[sp_StartOrderProduct]");
            Sql("DROP FUNCTION [dbo].[fnGenerateQRToken]");

            Sql("DROP VIEW [dbo].[vGetNewID]");
            Sql("DROP SEQUENCE [dbo].[seq_SalesTransactionReceiptNr]");
        }
    }
}

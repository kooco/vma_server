namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;    
    
    public partial class AddAvailUpdateSetting : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationSetting("SalesListAvailUpdateSecs", Kooco.Framework.Models.Enum.AppSettingType.Numeric, "Sales List - Product Availability Update Interval (Seconds)", "Internal in seconds for updating the sales list of a vending machine. After that interval, when calling the sales_list API, the product availability will be updated via the BuyBuy API.", NumRangeFrom: 1, NumRangeTo: 60, InitialValue: "3");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v10.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateSalesList_v9.sql"));
            RemoveApplicationSetting("SalesListAvailUpdateSecs");
        }
    }
}

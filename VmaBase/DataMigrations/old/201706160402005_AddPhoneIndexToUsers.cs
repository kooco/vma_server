namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using VmaBase.Shared;

    public partial class AddPhoneIndexToUsers : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.UserBases", "Phone", name: "IX_PhoneNr");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserBases", "IX_PhoneNr");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserData : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserDatas", "ImageUrl", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserDatas", "ImageUrl", c => c.String(maxLength: 128));
        }
    }
}

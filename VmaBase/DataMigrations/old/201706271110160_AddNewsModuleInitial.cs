namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddNewsModuleInitial : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewsArticles",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        ImageUrl = c.String(maxLength: 100),
                        CategoryId = c.Long(),
                        Content = c.String(nullable: false, storeType: "ntext"),
                        UIDAuthor = c.Long(),
                        UIDEditor = c.Long(),
                        LikeCount = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        ShareCount = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        ReadOnly = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Short(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "9")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UIDAuthor)
                .ForeignKey("dbo.NewsCategories", t => t.CategoryId)
                .ForeignKey("dbo.UserBases", t => t.UIDEditor)
                .Index(t => t.CategoryId, name: "IX_News_CategoryId")
                .Index(t => t.UIDAuthor)
                .Index(t => t.UIDEditor);
            
            CreateTable(
                "dbo.NewsCategories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NewsMessages",
                c => new
                    {
                        MessageId = c.Long(nullable: false, identity: true),
                        ParentMessageId = c.Long(),
                        NewsId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        Message = c.String(nullable: false, maxLength: 50),
                        LikeCount = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        ShareCount = c.Long(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Short(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        NewsHeader_Id = c.Long(),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.NewsArticles", t => t.NewsHeader_Id)
                .ForeignKey("dbo.NewsMessages", t => t.ParentMessageId)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => t.ParentMessageId)
                .Index(t => t.UID)
                .Index(t => t.NewsHeader_Id);
            
            CreateTable(
                "dbo.NewsTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NewsTickers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BannerOrder = c.Int(nullable: false),
                        BannerImageUrl = c.String(maxLength: 128),
                        NewsId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        UpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewsArticles", t => t.NewsId)
                .Index(t => t.NewsId);
            
            CreateTable(
                "dbo.NewsTagNewsArticles",
                c => new
                    {
                        NewsTag_Id = c.Int(nullable: false),
                        NewsArticle_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.NewsTag_Id, t.NewsArticle_Id })
                .ForeignKey("dbo.NewsTags", t => t.NewsTag_Id)
                .ForeignKey("dbo.NewsArticles", t => t.NewsArticle_Id)
                .Index(t => t.NewsTag_Id)
                .Index(t => t.NewsArticle_Id);
            
            AddColumn("dbo.VendingProducts", "Status", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));

            NewsModuleMigrationHelper.ApplyAdditionalNewsModuleDbChanges_Initial(this);
        }
        
        public override void Down()
        {
            NewsModuleMigrationHelper.RevertAdditionalNewsModuleDbChanges_Initial(this);

            DropForeignKey("dbo.NewsTickers", "NewsId", "dbo.NewsArticles");
            DropForeignKey("dbo.NewsTagNewsArticles", "NewsArticle_Id", "dbo.NewsArticles");
            DropForeignKey("dbo.NewsTagNewsArticles", "NewsTag_Id", "dbo.NewsTags");
            DropForeignKey("dbo.NewsMessages", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.NewsMessages", "UID", "dbo.UserBases");
            DropForeignKey("dbo.NewsMessages", "ParentMessageId", "dbo.NewsMessages");
            DropForeignKey("dbo.NewsMessages", "NewsHeader_Id", "dbo.NewsArticles");
            DropForeignKey("dbo.NewsArticles", "UIDEditor", "dbo.UserBases");
            DropForeignKey("dbo.NewsArticles", "CategoryId", "dbo.NewsCategories");
            DropForeignKey("dbo.NewsArticles", "UIDAuthor", "dbo.UserBases");
            DropIndex("dbo.NewsTagNewsArticles", new[] { "NewsArticle_Id" });
            DropIndex("dbo.NewsTagNewsArticles", new[] { "NewsTag_Id" });
            DropIndex("dbo.NewsTickers", new[] { "NewsId" });
            DropIndex("dbo.NewsMessages", new[] { "NewsHeader_Id" });
            DropIndex("dbo.NewsMessages", new[] { "UID" });
            DropIndex("dbo.NewsMessages", new[] { "ParentMessageId" });
            DropIndex("dbo.NewsArticles", new[] { "UIDEditor" });
            DropIndex("dbo.NewsArticles", new[] { "UIDAuthor" });
            DropIndex("dbo.NewsArticles", "IX_News_CategoryId");
            DropColumn("dbo.VendingProducts", "Status",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropTable("dbo.NewsTagNewsArticles");
            DropTable("dbo.NewsTickers",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "UpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.NewsTags");
            DropTable("dbo.NewsMessages",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LikeCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ShareCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-1" },
                        }
                    },
                });
            DropTable("dbo.NewsCategories");
            DropTable("dbo.NewsArticles",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LikeCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ReadOnly",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "ShareCount",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "9" },
                        }
                    },
                });
        }
    }
}

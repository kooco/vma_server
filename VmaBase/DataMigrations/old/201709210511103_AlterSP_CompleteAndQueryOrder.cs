namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AlterSP_CompleteAndQueryOrder : DbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v6.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.CompleteMachineOrder_v5.sql"));
        }
    }
}

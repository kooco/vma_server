namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserStampsLogSalesTransactionRef : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserStampLogs", "SalesTransactionId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserStampLogs", "SalesTransactionId");
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;
    
    public partial class AddVendingMachinesMenu : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("MACHINE", "VendingMachines", "Index", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);

            AddMenuItem(47000, ParentId: null, ActionCode: "MACHINE", Name: "販賣機", Sort: 750, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(47001, ParentId: 47000, ActionCode: "MACHINE", Name: "販賣機管理", Sort: 751, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }
        
        public override void Down()
        {
            RemoveMenuItem(47001);
            RemoveMenuItem(47000);

            RemoveApplicationAction("MACHINE");
        }
    }
}

﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;
    using Shared;
    
    public partial class AddSettingPickupLocation : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationSetting("PromoProdStdPickupLoc", Kooco.Framework.Models.Enum.AppSettingType.Text, "Pickup location for 禮品", "The default pickup location for promotion products", InitialValue: "可口可樂公司總部");
        }
        
        public override void Down()
        {
            RemoveApplicationSetting("PromoProdStdPickupLoc");
        }
    }
}

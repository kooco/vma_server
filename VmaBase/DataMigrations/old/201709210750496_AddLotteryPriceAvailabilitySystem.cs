namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Shared;
    
    public partial class AddLotteryPriceAvailabilitySystem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LotteryPriceAvailabilityItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        LotteryPriceId = c.Long(nullable: false),
                        WinFromTime = c.DateTime(nullable: false),
                        WonTime = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.LotteryPrices", t => t.LotteryPriceId)
                .Index(t => t.LotteryGameId)
                .Index(t => t.LotteryPriceId)
                .Index(t => t.WinFromTime, name: "IX_Game_WinPriceDate");

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotterySpreadPrices_v1.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_LotterySpreadPrices]");

            DropForeignKey("dbo.LotteryPriceAvailabilityItems", "LotteryPriceId", "dbo.LotteryPrices");
            DropForeignKey("dbo.LotteryPriceAvailabilityItems", "LotteryGameId", "dbo.LotteryGames");
            DropIndex("dbo.LotteryPriceAvailabilityItems", "IX_Game_WinPriceDate");
            DropIndex("dbo.LotteryPriceAvailabilityItems", new[] { "LotteryPriceId" });
            DropIndex("dbo.LotteryPriceAvailabilityItems", new[] { "LotteryGameId" });
            DropTable("dbo.LotteryPriceAvailabilityItems",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

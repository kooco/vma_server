namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyStampTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StampTypes", "Number", c => c.Int(nullable: false));
            AddColumn("dbo.StampTypes", "ImageUrl", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.StampTypes", "Status", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "9")
                    },
                }));
            AddColumn("dbo.StampTypes", "DateCreated", c => c.DateTime(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                    },
                }));
            AddColumn("dbo.StampTypes", "DateModified", c => c.DateTime(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                    },
                }));
            AlterColumn("dbo.StampTypes", "Name", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.StampTypes", "Description", c => c.String(maxLength: 128));

            Sql("CREATE UNIQUE NONCLUSTERED INDEX [UIX_Number] ON [dbo].[StampTypes]([Number]) WHERE [Status] <> -1");
        }
        
        public override void Down()
        {
            Sql("DROP INDEX [UIX_Number] ON [dbo].[StampTypes]");

            AlterColumn("dbo.StampTypes", "Description", c => c.String());
            AlterColumn("dbo.StampTypes", "Name", c => c.String());
            DropColumn("dbo.StampTypes", "DateModified",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "getutcdate()" },
                });
            DropColumn("dbo.StampTypes", "DateCreated",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "getutcdate()" },
                });
            DropColumn("dbo.StampTypes", "Status",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "9" },
                });
            DropColumn("dbo.StampTypes", "ImageUrl");
            DropColumn("dbo.StampTypes", "Number");
        }
    }
}

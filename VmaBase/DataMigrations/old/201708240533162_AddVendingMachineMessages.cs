namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddVendingMachineMessages : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SalesEvents", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.SalesEvents", "StampTypeId", "dbo.StampTypes");
            DropForeignKey("dbo.SalesEvents", "VendingMachineId", "dbo.VendingMachines");
            DropIndex("dbo.SalesTransactions", new[] { "ReceiptNumber" });
            DropIndex("dbo.SalesEvents", new[] { "VendingMachineId" });
            DropIndex("dbo.SalesEvents", new[] { "ProductId" });
            DropIndex("dbo.SalesEvents", new[] { "StampTypeId" });
            CreateTable(
                "dbo.VendingMachineMessages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Direction = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        AssetNo = c.String(nullable: false, maxLength: 32),
                        VendingMachineId = c.Long(nullable: false),
                        ProductId = c.Long(),
                        SalesTransactionId = c.Long(),
                        CouponToken = c.String(maxLength: 32),
                        CouponId = c.Long(),
                        BuyBuyTransactionNo = c.String(maxLength: 32),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        QueryTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserCoupons", t => t.CouponId)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .ForeignKey("dbo.SalesTransactions", t => t.SalesTransactionId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => new { t.Status, t.Direction, t.Type, t.AssetNo }, name: "IX_StatusDirectionTypeAssetNo")
                .Index(t => t.VendingMachineId)
                .Index(t => t.ProductId)
                .Index(t => t.SalesTransactionId)
                .Index(t => t.CouponId);
            
            AddColumn("dbo.VendingMachines", "QRTokenOld", c => c.String(maxLength: 32));
            AddColumn("dbo.SalesTransactions", "QRToken", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.UserCoupons", "CouponToken", c => c.String(maxLength: 32));
            CreateIndex("dbo.VendingMachines", "QRToken", unique: true, name: "UIX_QRToken");
            CreateIndex("dbo.SalesTransactions", "ReceiptNumber", unique: true, name: "UIX_ReceiptNumber");
            CreateIndex("dbo.SalesTransactions", "QRToken", unique: true, name: "UIX_QRToken");
            DropColumn("dbo.SalesTransactions", "PaymentType");
            DropTable("dbo.SalesEvents",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "EndTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "Cast('2999-12-31' as datetime)" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "PointsReceivable",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "StampsReceivable",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "StartTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "Cast('1900-1-1' as datetime)" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });

            Sql("CREATE UNIQUE INDEX [UIX_VendingMachine_QRTokenOld] ON [dbo].[VendingMachines]([QRTokenOld]) WHERE [QRTokenOld] IS NOT NULL");
            Sql("CREATE UNIQUE INDEX [UIX_UserCoupon_CouponToken] ON [dbo].[UserCoupons]([CouponToken]) WHERE [CouponToken] IS NOT NULL");
        }
        
        public override void Down()
        {
            Sql("DROP INDEX [UIX_VendingMachine_QRTokenOld] ON [dbo].[VendingMachines]");
            Sql("DROP INDEX [UIX_UserCoupon_CouponToken] ON [dbo].[UserCoupons]");

            CreateTable(
                "dbo.SalesEvents",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        VendingMachineId = c.Long(),
                        ProductId = c.Long(),
                        PointsReceivable = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        StampsReceivable = c.Double(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        StampTypeId = c.Long(),
                        StartTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "Cast('1900-1-1' as datetime)")
                                },
                            }),
                        EndTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "Cast('2999-12-31' as datetime)")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.SalesTransactions", "PaymentType", c => c.Int(nullable: false));
            DropForeignKey("dbo.VendingMachineMessages", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.VendingMachineMessages", "SalesTransactionId", "dbo.SalesTransactions");
            DropForeignKey("dbo.VendingMachineMessages", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.VendingMachineMessages", "CouponId", "dbo.UserCoupons");
            DropIndex("dbo.VendingMachineMessages", new[] { "CouponId" });
            DropIndex("dbo.VendingMachineMessages", new[] { "SalesTransactionId" });
            DropIndex("dbo.VendingMachineMessages", new[] { "ProductId" });
            DropIndex("dbo.VendingMachineMessages", new[] { "VendingMachineId" });
            DropIndex("dbo.VendingMachineMessages", "IX_StatusDirectionTypeAssetNo");
            DropIndex("dbo.SalesTransactions", "UIX_QRToken");
            DropIndex("dbo.SalesTransactions", "UIX_ReceiptNumber");
            DropIndex("dbo.VendingMachines", "UIX_QRToken");
            DropColumn("dbo.UserCoupons", "CouponToken");
            DropColumn("dbo.SalesTransactions", "QRToken");
            DropColumn("dbo.VendingMachines", "QRTokenOld");
            DropTable("dbo.VendingMachineMessages",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
            CreateIndex("dbo.SalesEvents", "StampTypeId");
            CreateIndex("dbo.SalesEvents", "ProductId");
            CreateIndex("dbo.SalesEvents", "VendingMachineId");
            CreateIndex("dbo.SalesTransactions", "ReceiptNumber");
            AddForeignKey("dbo.SalesEvents", "VendingMachineId", "dbo.VendingMachines", "Id");
            AddForeignKey("dbo.SalesEvents", "StampTypeId", "dbo.StampTypes", "Id");
            AddForeignKey("dbo.SalesEvents", "ProductId", "dbo.VendingProducts", "Id");
        }
    }
}

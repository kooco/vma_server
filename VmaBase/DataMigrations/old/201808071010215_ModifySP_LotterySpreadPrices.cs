namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class ModifySP_LotterySpreadPrices : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotterySpreadPrices_v2.sql"));
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[sp_LotterySpreadPrices]");
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotterySpreadPrices_v1.sql"));
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    using Kooco.Framework.DataMigrations;

    using VmaBase.Shared;
    using VmaBase.Models.Enum;


    public partial class AddUserLotteryCoinsTable : FrameworkDbMigration
    {
        public override void Up()
        {            
            CreateTable(
                "dbo.UserLotteryCoins",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        UID = c.Long(nullable: false),
                        Type = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Amount = c.Int(nullable: false),
                        OriginalAmount = c.Int(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateAquired = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ExpiryDate = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.UserDatas", t => t.UID)
                .Index(t => new { t.LotteryGameId, t.UID, t.Status }, name: "UserLotteryCoin_GameUIDStatus");

            Sql("INSERT INTO [dbo].[UserLotteryCoins] ([UID], [LotteryGameId], [Type], [OriginalAmount], [Amount], [DateAquired], [ExpiryDate], [Status]) " +
                                  " SELECT [UID], " + GlobalConst.LotteryGameId_StandardLottery.ToString() + ", " +
                                   ((int)LotteryCoinType.Daily).ToString() + ", [LotteryDailyCoins], [LotteryDailyCoins], getutcdate(), '2099-12-31 00:00:00', " +
                                   ((int)LotteryCoinStatus.Available).ToString() +
                                    " FROM [dbo].[UserDatas] WHERE [LotteryDailyCoins] > 0");

            Sql("INSERT INTO [dbo].[UserLotteryCoins] ([UID], [LotteryGameId], [Type], [OriginalAmount], [Amount], [DateAquired], [ExpiryDate], [Status]) " +
                                  " SELECT [UID], " + GlobalConst.LotteryGameId_StandardLottery.ToString() + ", " +
                                   ((int)LotteryCoinType.Earned).ToString() + ", [LotteryEarnedCoins], [LotteryEarnedCoins], getutcdate(), '2099-12-31 00:00:00', " +
                                   ((int)LotteryCoinStatus.Available).ToString() + 
                                    " FROM [dbo].[UserDatas] WHERE [LotteryEarnedCoins] > 0");

            AddColumn("dbo.VendingProducts", "IsWatersports", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.VendingProducts", "WatersportsCoins", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            AddColumn("dbo.LotteryGames", "CoinsCanExpire", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.LotteryGames", "CoinsExpiryDate", c => c.DateTime());
            AddColumn("dbo.LotteryGames", "CoinsExpiryType", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "2")
                    },
                }));
            AddColumn("dbo.LotteryGames", "CoinsExpiryPeriod", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            DropColumn("dbo.UserDatas", "LotteryDailyCoins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.UserDatas", "LotteryEarnedCoins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserDatas", "LotteryEarnedCoins", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.UserDatas", "LotteryDailyCoins", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));

            Sql("UPDATE [dbo].[UserDatas] SET [LotteryDailyCoins] = IsNull((SELECT Sum([Amount]) FROM [dbo].[UserLotteryCoins] coins WHERE coins.[UID] = [dbo].[UserDatas].[UID] AND [LotteryGameId] = " + GlobalConst.LotteryGameId_StandardLottery.ToString() + " AND [Type] = " + ((int)LotteryCoinType.Daily).ToString() + "), 0)");
            Sql("UPDATE [dbo].[UserDatas] SET [LotteryEarnedCoins] = IsNull((SELECT Sum([Amount]) FROM [dbo].[UserLotteryCoins] coins WHERE coins.[UID] = [dbo].[UserDatas].[UID] AND [LotteryGameId] = " + GlobalConst.LotteryGameId_StandardLottery.ToString() + " AND [Type] = " + ((int)LotteryCoinType.Earned).ToString() + "), 0)");

            DropForeignKey("dbo.UserLotteryCoins", "UID", "dbo.UserDatas");
            DropForeignKey("dbo.UserLotteryCoins", "LotteryGameId", "dbo.LotteryGames");
            DropIndex("dbo.UserLotteryCoins", "UserLotteryCoin_GameUIDStatus");
            DropColumn("dbo.LotteryGames", "CoinsExpiryPeriod",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.LotteryGames", "CoinsExpiryType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "2" },
                });
            DropColumn("dbo.LotteryGames", "CoinsExpiryDate");
            DropColumn("dbo.LotteryGames", "CoinsCanExpire",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropColumn("dbo.VendingProducts", "WatersportsCoins",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.VendingProducts", "IsWatersports",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropTable("dbo.UserLotteryCoins",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateAquired",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ExpiryDate",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "Type",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                });
        }
    }
}

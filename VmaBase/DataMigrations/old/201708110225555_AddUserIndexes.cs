namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserIndexes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.UserBases", "Account", unique: true, name: "UIX_Account");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserBases", "UIX_Account");
        }
    }
}

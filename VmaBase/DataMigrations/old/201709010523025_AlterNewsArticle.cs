namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Shared;

    using Kooco.Framework.DataMigrations;

    public partial class AlterNewsArticle : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NewsArticles", "LimitStartTime", c => c.DateTime());
            AddColumn("dbo.NewsArticles", "LimitEndTime", c => c.DateTime());

            NewsModuleMigrationHelper.ApplyAdditionalNewsModuleDbChanges_0_3_1(this);
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v3vma.sql"));
        }
        
        public override void Down()
        {
            NewsModuleMigrationHelper.RevertAdditionalNewsModuleDbChanges_0_3_1(this);
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v2vma.sql"));

            DropColumn("dbo.NewsArticles", "LimitEndTime");
            DropColumn("dbo.NewsArticles", "LimitStartTime");
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class AlterActivityAddRedeemEndTime : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponActivities", "RedeemEndTime", c => c.DateTime(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "dateadd(month, 1, getutcdate())")
                    },
                }));

            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v9.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetCouponActivityList_v8.sql"));

            DropColumn("dbo.CouponActivities", "RedeemEndTime",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "dateadd(month, 1, getutcdate())" },
                });            
        }
    }
}

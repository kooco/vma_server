namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUserPhoneNrSearch : DbMigration
    {
        public override void Up()
        {
            Sql("update [dbo].[UserDatas] set PhoneNrSearch7 = right(ub.Phone, 7) from [UserDatas] ud inner join [UserBases] ub on ub.[UID] = ud.[UID]", true); // with transaction it will become a deadlock .. for what reason ever...
        }
        
        public override void Down()
        {
            Sql("update [dbo].[UserDatas] set PhoneNrSearch7 = null");
        }
    }
}

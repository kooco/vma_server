namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Models.Enum;
    using VmaBase.Shared;

    public partial class ModifyLotteryGameTable_PickupToken : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LotteryGames", "PickupTokenMode", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.LotteryGames", "PickupToken", c => c.String(maxLength: 12));

            Sql("UPDATE [dbo].[LotteryGames] SET [PickupTokenMode] = " + ((int)LotteryPickupTokenMode.PriceCouponToken).ToString() + " WHERE [Id] = " + GlobalConst.LotteryGameId_StandardLottery.ToString());
            Sql("UPDATE [dbo].[LotteryGames] SET [PickupTokenMode] = " + ((int)LotteryPickupTokenMode.GameToken).ToString() + ", [PickupToken] = N'874569' WHERE [Id] = " + GlobalConst.LotteryGameId_StandardLottery.ToString());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LotteryGames", "PickupToken");
            DropColumn("dbo.LotteryGames", "PickupTokenMode",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
        }
    }
}

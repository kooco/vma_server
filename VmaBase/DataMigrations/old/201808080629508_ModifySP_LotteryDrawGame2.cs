namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    using VmaBase.Shared;

    public partial class ModifySP_LotteryDrawGame2 : FrameworkDbMigration
    {
        public override void Up()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryDrawGame_v9.sql"));
        }
        
        public override void Down()
        {
            Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.LotteryDrawGame_v8.sql"));
        }
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class FixEFBug_UIX_CriteriaMachine : FrameworkDbMigration
    {
        public override void Up()
        {
            // because of Microsoft's inability to generate the correct code for a normal unique index,
            // we have to fix it here now manually.. (EntityFramework just forget the second field "MachineId" for the unique index for BOTH tables!! (AutoCouponCriteriaMachines and AutoCouponCriteriaProducts)..)

            Sql("DROP INDEX [UIX_Criteria_Machine] ON [dbo].[AutoCouponCriteriaMachines]");
            Sql("CREATE UNIQUE NONCLUSTERED INDEX [UIX_Criteria_Machine] ON [dbo].[AutoCouponCriteriaMachines]([CriteriaId] ASC, [MachineId] ASC)");

            Sql("DROP INDEX [UIX_Criteria_Product] ON [dbo].[AutoCouponCriteriaProducts]");
            Sql("CREATE UNIQUE NONCLUSTERED INDEX [UIX_Criteria_Product] ON [dbo].[AutoCouponCriteriaProducts]([CriteriaId] ASC, [ProductId] ASC)");
        }
        
        public override void Down()
        {
        }
    }
}

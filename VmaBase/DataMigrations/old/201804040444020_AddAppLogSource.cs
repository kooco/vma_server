namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;

    using VmaBase.Shared;

    public partial class AddAppLogSource : FrameworkDbMigration
    {
        public override void Up()
        {
            AddLogSource("Main", Kooco.Framework.Models.Enum.LogProviderType.SQLite, "dbfilename=" + ConfigManager.SQLLiteLogFilename);
        }
        
        public override void Down()
        {
            RemoveLogSource("Main");
        }
    }
}

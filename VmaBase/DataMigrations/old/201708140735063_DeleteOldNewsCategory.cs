﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteOldNewsCategory : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM [dbo].[NewsCategories] WHERE [Code] = 'GENERAL'");
        }
        
        public override void Down()
        {
            Sql("INSERT INTO [dbo].[NewsCategories]([Code], [Name]) Values('GENERAL', N'一般')");
            Sql("UPDATE [dbo].[NewsArticles] SET [CategoryId] = (SELECT [Id] FROM [dbo].[NewsCategories] WHERE [Code] = 'GENERAL')");
        }
    }
}

// <auto-generated />
namespace VmaBase.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FixSP_LotterySpreadPrices : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FixSP_LotterySpreadPrices));
        
        string IMigrationMetadata.Id
        {
            get { return "201810231304297_FixSP_LotterySpreadPrices"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

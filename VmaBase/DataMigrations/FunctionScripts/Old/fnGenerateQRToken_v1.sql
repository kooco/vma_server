﻿/*
 * fnGenerateQRToken
 * =================
 * generates a new, valid QRToken for the given record id with the given action
 */
CREATE FUNCTION [dbo].[fnGenerateQRToken]
(
	@ActionCode int,
	@RecordId bigint
)
RETURNS VARCHAR(32)
AS
BEGIN
	DECLARE @QRToken Varchar(32)
	DECLARE @RecordIdStr varchar(20)
	DECLARE @NewId varchar(40)

	SELECT @NewId = Cast([new_id] as varchar(40)) FROM [dbo].[vGetNewID]

	SET @RecordIdStr = Cast(@RecordId As Varchar(20))

	SET @QRToken = format(datepart(ms, getdate()), '000')
	SET @QRToken = @QRToken + substring(@NewId, 3, 6)
	SET @QRToken = @QRToken + format(len(@RecordIdStr), '00')
	SET @QRToken = @QRToken + '#' + Cast(@ActionCode as varchar(2)) + @RecordIdStr

	return @QRToken
END
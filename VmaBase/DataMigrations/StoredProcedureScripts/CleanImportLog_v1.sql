﻿CREATE PROCEDURE [dbo].[sp_CleanImportLog]
	@MaxAgeDays int
AS
BEGIN
	declare @DateMin datetime
	SET @DateMin = dateadd(day, -(@MaxAgeDays), getutcdate())

	DELETE FROM [dbo].[ImportLogMessages]
	      WHERE [ImportLogId] IN (SELECT [Id] FROM [dbo].[ImportLogs] WHERE [StartTime] < @DateMin)
    
	DELETE FROM [dbo].[ImportLogs]
	      WHERE [StartTime] < @DateMin								
END
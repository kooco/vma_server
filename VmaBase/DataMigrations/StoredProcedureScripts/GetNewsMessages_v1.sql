﻿CREATE PROCEDURE [dbo].[sp_GetNewsMessages]
	@NewsId bigint,
	@UID bigint = null,
	@ParentId bigint = null,
	@Recursive bit = 0
AS
BEGIN
		SELECT msg.[MessageId] As [Id],
			   msg.[NewsId] As [NewsId],
			   msg.[ParentMessageId] As [ParentId],
			   msg.[UID] As [UID],
			   usr.[Name] As [UserName],
			   usr.[Nickname] As [UserNickname],
			   usd.[ImageUrl] As [UserImageUrl],
			   msg.[Message] As [Message],
			   Cast((case when msg.[LikeCount] is null then 0 else msg.[LikeCount] end) as int) as LikeCount,
			   Cast((case when msg.[ShareCount] is null then 0 else msg.[ShareCount] end) as int) as ShareCount,
			   (Cast((case when IsNull(likes.[Status], 9) = 1 then 1 else 0 end) as bit)) As [UserLike],
			   msg.[CreateTime]
		  FROM [dbo].[NewsMessages] msg
	INNER JOIN [dbo].[UserBases] usr ON usr.[UID] = msg.[UID]
	INNER JOIN [dbo].[UserDatas] usd ON usd.[UID] = msg.[UID]
LEFT OUTER JOIN [dbo].[UserLikes] likes ON likes.[UID] = IsNull(@UID, -1) AND likes.[MessageId] = msg.[MessageId]
		 WHERE msg.[NewsId] = @NewsID
		   AND msg.[Status] = 1
		   AND (IsNull(@Recursive, 0) = 1 Or msg.[ParentMessageId] is null)
		   AND (IsNull(@ParentId, 0) = 0 Or msg.[ParentMessageId] = @ParentId) 
	  ORDER BY msg.[CreateTime] desc
END

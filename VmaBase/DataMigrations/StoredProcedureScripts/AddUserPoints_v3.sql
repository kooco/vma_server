﻿ALTER PROCEDURE [dbo].[sp_AddUserPoints]
	@UID bigint,
	@Amount int,
	@ExpiryDate datetime,
	@TransactionType int,
	@ReferenceUID bigint,
	@SalesTransactionId bigint,
	@ActivityId bigint,
	@PointsList varchar(MAX)
AS
BEGIN
	BEGIN TRY
		begin transaction 

		-- we have to make a "savepoint" for the case of nested transaction, because there are no real nested transactions in SQL server..
		-- https://social.msdn.microsoft.com/Forums/en-US/3eac605e-0e0f-41ab-9bff-f032e349d2fb/error-message-on-mismatch-in-transaction-count-when-nesting-stored-procedures?forum=transactsql
		-- what did microsoft smoke, when implementing transactions?...
		save transaction AddPointsTran

		if @TransactionType = 1 AND IsNull(@ActivityId, 0) = 0 begin
			rollback transaction AddPointsTran
			return -10007
		end
		if @TransactionType IN (2, 11) AND IsNull(@SalesTransactionId, 0) = 0 begin
			rollback transaction AddPointsTran
			return -10007
		end
		if @TransactionType IN (5, 15) AND IsNull(@ReferenceUID, 0) = 0 begin
			rollback transaction AddPointsTran
			return -10007
		end

		INSERT INTO [dbo].[PointsTransactions] ([UID], [TransactionType], [Amount], [ReferenceUserId], [SalesTransactionId], [ActivityId], [CreateTime], [ModifyTime])
									    Values (@UID,  @TransactionType,  @Amount,  @ReferenceUID,     @SalesTransactionId,  @ActivityId,  getutcdate(), getutcdate())

		IF IsNull(@PointsList, '') = '' begin
			INSERT INTO [dbo].[UserPoints] ([UID], [Amount], [OriginalAmount], [Status], [DateAquired], [ExpiryDate])
									Values (@UID,  @Amount,  @Amount,          1,        getutcdate(),  @ExpiryDate)
		END ELSE BEGIN
			DECLARE @PointsAdded bigint
			DECLARE @PointsEntry varchar(50)
			DECLARE @PointsId bigint
			DECLARE @PartAmount int
			DECLARE @ExpiryDateStr varchar(12)
			DECLARE @PartExpiryDate datetime

			DECLARE cPoints CURSOR
			    FOR SELECT ltrim(rtrim([Item])) from [dbo].[fn_SplitString](@PointsList, ';')

			open cPoints
			fetch next from cPoints into @PointsEntry
			while @@FETCH_STATUS = 0
			begin
				if IsNull(@PointsEntry, '') <> '' begin
					select @PointsId = Cast([Item] as bigint) FROM (select row_number() over (order by (select 1)) as [ItemIndex], [Item] from [dbo].[fn_SplitString](@PointsEntry, ',')) ItemArray WHERE [ItemIndex] = 1
					select @PartAmount = Cast([Item] as int) FROM (select row_number() over (order by (select 1)) as [ItemIndex], [Item] from [dbo].[fn_SplitString](@PointsEntry, ',')) ItemArray WHERE [ItemIndex] = 2
					select @ExpiryDateStr = [Item] FROM (select row_number() over (order by (select 1)) as [ItemIndex], [Item] from [dbo].[fn_SplitString](@PointsEntry, ',')) ItemArray WHERE [ItemIndex] = 3

					set @PartExpiryDate = datetimefromparts(cast(substring(@ExpiryDateStr, 1, 4) as int), cast(substring(@ExpiryDateStr, 5, 2) as int), cast(substring(@ExpiryDateStr, 7, 2) as int), cast(substring(@ExpiryDateStr, 9, 2) as int), cast(substring(@ExpiryDateStr, 11, 2) as int), 0, 0)

					INSERT INTO [dbo].[UserPoints] ([UID], [Amount], [OriginalAmount], [Status], [DateAquired], [ExpiryDate])
						                    Values (@UID,  @PartAmount,  @PartAmount,  1,        getutcdate(),  @PartExpiryDate)

					set @PointsAdded = @PointsAdded + @PartAmount
				end

				fetch next from cPoints into @PointsEntry
			end

			close cPoints
			deallocate cPoints

			if @PointsAdded <> @Amount begin
				declare @ErrorMsg varchar(200)
				set @ErrorMsg = 'The points added by the @PointsList (' + cast(@PointsAdded as varchar(20)) + ' points in sum) does not match the @Amount parameter given (' + Cast(@Amount as varchar(20)) + ' points)!'
				raiserror(@ErrorMsg, 18, 1)
			end
		END

	    UPDATE [dbo].[UserDatas] SET [PointsTotal] = (SELECT IsNull(Sum([Amount]), 0)
														FROM [dbo].[UserPoints]
													   WHERE [UID] = @UID
													     AND [Status] = 1
														 AND [ExpiryDate] >= getutcdate())
											WHERE [UID] = @UID

		commit transaction

		return 1
	END TRY
	BEGIN CATCH
		rollback transaction AddPointsTran;
		commit transaction;
		throw;
		return -1003
	END CATCH
END

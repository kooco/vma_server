﻿CREATE PROCEDURE [dbo].[sp_UseUserPoints]
	@UID bigint,
	@Amount int,
	@TransactionType int,
	@ReferenceUID bigint,
	@SalesTransactionId bigint,
	@ActivityId bigint
AS
BEGIN
	begin transaction

	declare @DeductAmount int

	if @Amount < 0 BEGIN
		SET @DeductAmount = -(@Amount)
	END ELSE BEGIN
		SET @DeductAmount = @Amount
	END

	BEGIN TRY
		if @TransactionType = 1 AND IsNull(@ActivityId, 0) = 0 begin
			rollback transaction
			return -10007
		end
		if @TransactionType IN (2, 11) AND IsNull(@SalesTransactionId, 0) = 0 begin
			rollback transaction
			return -10007
		end
		if @TransactionType IN (5, 15) AND IsNull(@ReferenceUID, 0) = 0 begin
			raiserror('sp_UseUserPoints: the parameter "@ReferenceUID" is missing, which is required for TransactionType 5 or 15!', 18, 1) with seterror
			return -10007
		end

		exec [dbo].[sp_CheckExpiredPoints] @UID
		if ((Select [PointsTotal] FROM [dbo].[UserDatas] WHERE [UID] = @UID) < @DeductAmount) BEGIN
			rollback transaction
			return -300
		END

		INSERT INTO [dbo].[PointsTransactions] ([UID], [TransactionType],  [Amount],		[ReferenceUserId], [SalesTransactionId], [ActivityId], [CreateTime], [ModifyTime])
									    Values (@UID,  @TransactionType,  -(@DeductAmount),  @ReferenceUID,     @SalesTransactionId,  @ActivityId,  getutcdate(), getutcdate())

		DECLARE cPoints CURSOR FOR
			SELECT [Id], [Amount]
			  FROM [dbo].[UserPoints]
			 WHERE [UID] = @UID
			   AND [Status] = 1
		  ORDER BY [ExpiryDate] asc

		DECLARE @PointsId bigint
		DECLARE @AvailableAmount int

		open cPoints
		fetch next from cPoints into @PointsId, @AvailableAmount

		while ((@@FETCH_STATUS = 0) AND (@DeductAmount > 0))
		begin
			if @AvailableAmount > @DeductAmount begin
				-- this points entry is higher than the amount we need
				UPDATE [dbo].[UserPoints]
				   SET [Amount] = [Amount] - @DeductAmount
				 WHERE [Id] = @PointsId

				   SET @DeductAmount = 0
			end else begin
				-- this points entry is less than the amount we need
				UPDATE [dbo].[UserPoints]
				   SET [Amount] = 0,
				       [Status] = 0
				 WHERE [Id] = @PointsId

				   SET @DeductAmount = @DeductAmount - @AvailableAmount
			end

			fetch next from cPoints into @PointsId, @AvailableAmount
		end

		close cPoints
		deallocate cPoints

		UPDATE [dbo].[UserDatas]
		   SET [PointsTotal] = (Select Sum([Amount]) FROM [dbo].[UserPoints] WHERE [Status] = 1)
		 WHERE [UID] = @UID

		commit transaction

		return 1
	END TRY
	BEGIN CATCH
		rollback transaction;
		throw;
	END CATCH
END
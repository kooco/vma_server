﻿CREATE PROCEDURE [dbo].[sp_GetVendingMachineList]
	@GeoLocation geography = null,
	@RadiusMeters bigint = 1000,
	@OnlyActive bit = 1
AS
BEGIN
	if (@GeoLocation is null) begin
		SELECT [Id],
			   [Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   [GeoLocation]
		  FROM dbo.[VendingMachines]
		 WHERE ([Status] = 1 Or @OnlyActive = 0)
	 end else begin
		SELECT [Id],
			   [Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   [GeoLocation]
		  FROM dbo.[VendingMachines]
		 WHERE ([Status] = 1 Or @OnlyActive = 0)
		   AND @GeoLocation.STDistance([GeoLocation]) <= @RadiusMeters
	 end
END
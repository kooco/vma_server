﻿
CREATE PROCEDURE [dbo].[sp_GetUsersByPhoneNumbers]
	@UIDAuthUser bigint,
	@PhoneListSearch7 text
AS
BEGIN
	SELECT ub.[UID],
		   Account,
		   cast('' AS nvarchar(256)) As [Password],
		   FacebookAccount,
		   QQAccount,
		   WeChatAccount,
		   TwitterAccount,
		   GoogleAccount,
		   Nickname,
		   [Name],
		   Email,
		   Phone,
		   RoleCode,
		   ub.[Status],
		   ub.CreateTime,
		   LastLoginTime,
		   VerificationToken
	  FROM [dbo].[UserBases] ub
INNER JOIN [dbo].[UserDatas] ud
LEFT OUTER JOIN [dbo].[UserFriend] uf
			 ON uf.[UID] = @UIDAuthUser
			AND uf.[UIDFriend] = ub.[UID]
	    ON ud.[UID] = ub.[UID]
	 WHERE [PhoneNrSearch7] in (Select Cast(Item As nvarchar(255)) from [dbo].[fn_SplitString](@PhoneListSearch7, ','))
	   AND uf.[UID] is null -- only if not already in the friend list
END
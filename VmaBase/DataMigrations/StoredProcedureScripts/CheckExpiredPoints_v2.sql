﻿ALTER PROCEDURE [dbo].[sp_CheckExpiredPoints]
	@UID bigint
AS
BEGIN
	DECLARE @CurrentPoints float

	UPDATE [dbo].[UserPoints]
	   SET [Status] = -1
	 WHERE [UID] = @UID
	   AND [ExpiryDate] <= getutcdate()

	SET @CurrentPoints = IsNull((SELECT Sum([Amount]) FROM [dbo].[UserPoints] WHERE [UID] = @UID AND [Status] = 1), 0)

	UPDATE [dbo].[UserDatas]
	   SET [PointsTotal] = @CurrentPoints
	 WHERE [UID] = @UID

	return Cast(@CurrentPoints as int)
END
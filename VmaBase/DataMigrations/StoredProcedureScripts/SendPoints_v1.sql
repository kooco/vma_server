﻿CREATE PROCEDURE [dbo].[sp_SendPoints]
	@UID bigint,
	@FriendUID bigint,
	@Amount int
AS
BEGIN
	declare @ReturnCode int
	declare @PointsList varchar(max)

	begin transaction
	save transaction SendPointsTran

	print 'calling useuserpoints'
	exec @ReturnCode = [dbo].[sp_UseUserPoints] @UID, @Amount, 15, @FriendUID, null, null, null, @PointsList output
	if @ReturnCode <> 1 begin	
		rollback transaction SendPointsTran -- rollback savepoint
		commit transaction -- commit outer transaction
		-- what the hell microsoft was thinking about not to support normal nested transactions...
		return @ReturnCode
	end

	print 'calling adduserpoints'
	exec @ReturnCode = [dbo].[sp_AddUserPoints] @FriendUID, @Amount, null, 5, @UID, null, null, @PointsList
	if @ReturnCode <> 1 begin	
		rollback transaction SendPointsTran
		commit transaction
		return @ReturnCode
	end

	commit transaction

	return 1
END

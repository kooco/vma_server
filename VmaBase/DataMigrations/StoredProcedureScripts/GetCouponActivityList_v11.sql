﻿/*
	sp_GetCouponActivityList
	========================

	Returns a list of activities with infos about completed coupons
*/
ALTER PROCEDURE [dbo].[sp_GetCouponActivityList]
	@UID bigint,
	@OnlyCompleted bit, -- Flag, if only activities should be returned, which have at least one completed, not already redeemed coupon
						-- Because if @OnlyCompleted is set to true it means, that usually the App wants to have a list of coupons that can be redeemed
						-- the [RedeemEndTime] will be used instead of the [LimitEndTime] here! (in this way it was not necessary to change the API and client app)
	@ForPromotionMachinesList bit, -- Flag, if the list returned is used for the promoted machines list
	@VendingMachineId bigint = null -- Id of the Vending Machine to filter the activity list for. Only activities which are having this vending machine as part of it will be returned then
AS
BEGIN
	SELECT act.[Id] ActivityId,
		   Cast(null as bigint) as [CriteriaId],
		   Cast(1 as int) as [CouponsType], -- 1 = Activity Coupon
	       news.[Title] Title,
		   act.[ActivityText] ActivityText,
		   act.[LimitStartTime],
		   act.[LimitEndTime],
		   act.[LimitEndTime] ExpiryTime,
		   news.[ListImageUrl] ImageUrl,
		   act.[ActivityListImageUrl] ActivityListImageUrl,
		   Cast(Count(usrcp.[Id]) as int) AmountTotal,
		   (case when act.[SendingCouponsAllowed] = 1 then Cast(Count(usrcp.[Id]) as int) else Cast(0 as int) end) As AmountSendable,
		   act.[SendingCouponsAllowed] [SendAllowed],
		   act.[NoStampsActivity] IsNoStampsActivity
	  FROM [dbo].[CouponActivities] act
INNER JOIN [dbo].[NewsArticles] news ON news.[ActivityId] = act.[Id]
INNER JOIN [dbo].[NewsCategories] cat ON cat.[Id] = news.[CategoryId]
LEFT OUTER JOIN [dbo].[UserCoupons] usrcp ON usrcp.[ActivityId] = act.[Id] AND usrcp.[Status] = 2 AND usrcp.[UID] = @UID
     WHERE (news.[Status] = 1 or
	        (@OnlyCompleted = 1 and act.[Status] = 8))
	   AND (act.[Status] = 1 or (@OnlyCompleted = 1 and act.[Status] = 8))
	   AND cat.[Code] = 'ACT'
	   AND getutcdate() >= act.[LimitStartTime]
	   AND getutcdate() <= (case when @OnlyCompleted = 1 then act.[RedeemEndTime] else act.[LimitEndTime] end)
	   AND (IsNull(@VendingMachineId, 0) = 0 Or
	        Exists(SELECT 1 from [dbo].[ActivityParticipatingMachines] pm
						   where pm.ActivityId = act.[Id]
						     and pm.VendingMachineId = @VendingMachineId))
	   AND (act.[ShowOnPromotionMachinesList] = 1 Or IsNull(@ForPromotionMachinesList, 0) = 0)
  GROUP BY act.[Id], act.[SendingCouponsAllowed], act.[NoStampsActivity], news.[Title], act.[ActivityText], act.[LimitStartTime], act.[LimitEndTime], news.[ListImageUrl], act.[ActivityListImageUrl]
  HAVING (Cast(Count(usrcp.[Id]) as int) > 0 OR IsNull(@OnlyCompleted, 0) = 0)
  UNION
    SELECT Cast(null as bigint) as [ActivityId],
	       crit.[Id] [CriteriaId],
		   Cast(2 as int) as [CouponsType], -- 2 = auto-coupon
		   crit.[CouponsTitle] Title,
		   crit.[CouponsSubTitle] ActivityText,
		   crit.[LimitStartTime],
		   crit.[LimitEndTime],
		   crit.[CouponsExpiryDate] ExpiryTime,
		   null [ImageUrl],
		   null [ActivityListImageUrl],
		   Cast(Count(usrcp.[Id]) as int) AmountTotal,
		   Cast(Count(usrcp.[Id]) as int) AmountSendable,
		   Cast(1 as bit) SendAllowed,
		   Cast(1 as bit) IsNoStampsActivity
	  FROM [dbo].[AutoCouponCriterias] crit
LEFT OUTER JOIN [dbo].[UserCoupons] usrcp ON usrcp.[AutoCouponCriteriaId] = crit.[Id] AND usrcp.[Status] = 2 AND usrcp.[UID] = @UID
     WHERE 1 = 0 or IsNull(@OnlyCompleted, 0) = 1  -- only return auto-coupons, if @OnlyCompleted is set to 1 (if 0 usually it's for "real" activity lists)  
	   AND usrcp.[UID] = @UID
	   AND (IsNull(@VendingMachineId, 0) = 0 Or
	        crit.CouponsForAllMachines = 1 Or
			exists(select 1 from dbo.[AutoCouponCriteriaMachines] critmach where critmach.CriteriaId = crit.[Id] and critmach.[MachineId] = @VendingMachineId))
	   AND crit.[Status] <> -1
	   AND (getutcdate() <= IsNull(crit.[CouponsExpiryDate], crit.[LimitEndTime]))
  GROUP BY crit.[Id], crit.[CouponsTitle], crit.[CouponsSubTitle], crit.[LimitStartTime], crit.[LimitEndTime], crit.[CouponsExpiryDate]
  ORDER BY [CouponsType], [LimitStartTime] desc

	return 1
END

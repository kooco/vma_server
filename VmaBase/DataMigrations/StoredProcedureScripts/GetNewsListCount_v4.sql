﻿ALTER PROCEDURE [dbo].[sp_GetNewsListCount]
	@CategoryCode varchar(MAX),
	@Categories varchar(MAX),
	@TagIds varchar(MAX),
	@TagNames varchar(MAX),
	@IncludeTickerNews bit,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@OnlySubscribed bit = 0,
	@OnlyAvailable bit = 0,
	@UID bigint = null
AS
BEGIN
	if IsNull(@TagNames, '') = '' begin
		SELECT Cast(Count(news.[Id]) as bigint) NewsCount
		  FROM [dbo].[NewsArticles] news
	INNER JOIN [dbo].[NewsCategories] category ON category.Id = news.CategoryId
LEFT OUTER JOIN [dbo].[NewsTickers] tickers ON tickers.[NewsId] = news.[Id]
LEFT OUTER JOIN [dbo].[UserBases] usrauth on usrauth.[UID] = news.[UIDAuthor]
LEFT OUTER JOIN [dbo].[NewsSubscriptions] subscr ON subscr.[UID] = IsNull(@UID, -1) AND subscr.[NewsId] = news.[Id]
LEFT OUTER JOIN [dbo].[UserLikes] likes ON likes.[UID] = IsNull(@UID, -1) AND likes.[NewsId] = news.[Id]
		 WHERE ((category.code = @CategoryCode) Or IsNull(@CategoryCode, '') = '')
		   AND ((IsNull(@Categories, '') = '') Or category.Code in (Select lower(Cast(Item As varchar(50))) from [dbo].[fn_SplitString](@Categories, ',')))
		   AND news.[Status] <> -1
		   AND ((IsNull(@TagIds, '') = '') Or
				Exists(SELECT 1 from [dbo].[NewsTagNewsArticles] tagnews
							   where [NewsArticle_Id] = news.Id
								 and [NewsTag_id] in (Select Cast(Item As bigint) from [dbo].[fn_SplitString](@TagIds, ','))
					   )
				)
		   AND ((@IncludeTickerNews = 1) Or (tickers.[Id] is null))
		   AND ((subscr.[Id] is not null) Or (@OnlySubscribed = 0))
		   AND (IsNull(@OnlyAvailable, 0) = 0 Or (news.[Status] = 1))
		   AND (IsNull(@OnlyAvailable, 0) = 0 Or ((getutcdate() >= [LimitStartTime] Or [LimitStartTime] Is Null) AND (getutcdate() <= [LimitEndTime] Or [LimitEndTime] Is Null)))
		   AND ((@DateFrom is null and @DateTo is null) or ([LimitStartTime] is null or [LimitEndTime] is null) or 
															((@DateFrom >= [LimitStartTime] AND @DateFrom <= [LimitEndTime]) or
															 (@DateTo >= [LimitStartTime] AND @DateTo <= [LimitEndTime]) or
															 ([LimitStartTime] >= @DateFrom and [LimitStartTime] <= @DateTo) or
															 ([LimitEndTime] >= @DateTo and [LimitEndTime] <= @DateTo)))
		   AND ((@DateFrom is null and @DateTo is null) or ([LimitStartTime] is not null or [LimitEndTime] is not null) or
														  (news.[CreateTime] >= @DateFrom and news.[CreateTime] <= @DateTo))
	end else begin
		SELECT Cast(Count(news.[Id]) as bigint) NewsCount
		  FROM [dbo].[NewsArticles] news
	INNER JOIN [dbo].[NewsCategories] category ON category.Id = news.CategoryId
LEFT OUTER JOIN [dbo].[NewsTickers] tickers ON tickers.[NewsId] = news.[Id]
LEFT OUTER JOIN [dbo].[UserBases] usrauth on usrauth.[UID] = news.[UIDAuthor]
LEFT OUTER JOIN [dbo].[NewsSubscriptions] subscr ON subscr.[UID] = IsNull(@UID, -1) AND subscr.[NewsId] = news.[Id]
LEFT OUTER JOIN [dbo].[UserLikes] likes ON likes.[UID] = IsNull(@UID, -1) AND likes.[NewsId] = news.[Id]
		 WHERE ((category.code = @CategoryCode) Or IsNull(@CategoryCode, '') = '')
		   AND ((IsNull(@Categories, '') = '') Or category.Code in (Select lower(Cast(Item As varchar(50))) from [dbo].[fn_SplitString](@Categories, ',')))
		   AND news.[Status] <> -1
		   AND ((IsNull(@TagNames, '') = '') Or
				Exists(SELECT 1 from [dbo].[NewsTagNewsArticles] tagnews
						  inner join [dbo].[NewsTags] tags
							      on tags.Id = tagnews.NewsTag_Id								  
							   where [NewsArticle_Id] = news.Id
								 and lower(tags.[Name]) in (Select lower(Cast(Item As varchar(50))) from [dbo].[fn_SplitString](@TagNames, ','))
					   )
				)
		   AND ((@IncludeTickerNews = 1) Or (tickers.[Id] is null))
		   AND ((subscr.[Id] is not null) Or (@OnlySubscribed = 0))
		   AND (IsNull(@OnlyAvailable, 0) = 0 Or (news.[Status] = 1))
		   AND (IsNull(@OnlyAvailable, 0) = 0 Or ((getutcdate() >= [LimitStartTime] Or [LimitStartTime] Is Null) AND (getutcdate() <= [LimitEndTime] Or [LimitEndTime] Is Null)))
		   AND ((@DateFrom is null and @DateTo is null) or ([LimitStartTime] is null or [LimitEndTime] is null) or 
															((@DateFrom >= [LimitStartTime] AND @DateFrom <= [LimitEndTime]) or
															 (@DateTo >= [LimitStartTime] AND @DateTo <= [LimitEndTime]) or
															 ([LimitStartTime] >= @DateFrom and [LimitStartTime] <= @DateTo) or
															 ([LimitEndTime] >= @DateTo and [LimitEndTime] <= @DateTo)))
		   AND ((@DateFrom is null and @DateTo is null) or ([LimitStartTime] is not null or [LimitEndTime] is not null) or
														  (news.[CreateTime] >= @DateFrom and news.[CreateTime] <= @DateTo))
	end	
END

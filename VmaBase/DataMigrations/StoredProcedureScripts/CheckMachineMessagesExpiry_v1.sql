﻿/*
 * sp_CheckMachineMessagesExpiry
 * -----------------------------
 *
 * This procedure, called every minute, will make sure that
 * all machine messages will expire after about one minute.
 * And in case of redeemed coupons to give the coupon back to the user,
 * if the machine didn't pick up the message.
 */
CREATE PROCEDURE [dbo].[sp_CheckMachineMessagesExpiry]
	@VendingMachineMsgExpirySeconds int
AS
BEGIN
	declare @ExpiryTime datetime
	set @ExpiryTime = dateadd(second, -(@VendingMachineMsgExpirySeconds), getutcdate())

	if exists(select 1 from [dbo].[VendingMachineMessages]
					  where [Direction] = 0
					    and [Type] in (10, 11)
						and [Status] = 0
						and [CreateTime] <= @ExpiryTime)
	begin
		-- there are expired messages		
		-- now loop through them one by one

		declare cExpiredMessages cursor for
			select [Id], [CouponId], [LotteryPriceCouponId], [Type], [Status]
			  from [dbo].[VendingMachineMessages]
			 where [Direction] = 0
			   and [Type] in (10, 11)
			   and [Status] = 0
			   and [CreateTime] <= @ExpiryTime

		declare @MessageId bigint
		declare @CouponId bigint
		declare @LotteryPriceCouponId bigint
		declare @Status int
		declare @MessageType int

		open cExpiredMessages
		fetch next from cExpiredMessages into @MessageId, @CouponId, @LotteryPriceCouponId, @MessageType, @Status
		while @@fetch_status = 0
		begin
			if @Status = 0 begin
				-- lock the record
				update [dbo].[VendingMachineMessages] set [Status] = [Status] where [Id] = @MessageId

				-- make sure the status did not change
				select @Status = [Status] from [dbo].[VendingMachineMessages] where [Id] = @MessageId

				if @Status = 0 begin
					-- change the message status
					update [dbo].[VendingMachineMessages] set [Status] = -1 where [Id] = @MessageId
				end
			end

			fetch next from cExpiredMessages into @MessageId, @CouponId, @LotteryPriceCouponId, @MessageType, @Status
		end

		close cExpiredMessages
		deallocate cExpiredMessages
	end

	-- and also delete old cancelled messages	
	delete from [dbo].[VendingMachineMessages] where [Status] = -1 and [CreateTime] <= dateadd(day, -3, getutcdate())

		 delete [dbo].[SalesTransactions]
		   from [dbo].[SalesTransactions] sales
left outer join [dbo].[VendingMachineMessages] msg on msg.[SalesTransactionId] = sales.[Id]
		where sales.[Status] = -1 
		  and sales.[CreateTime] <= dateadd(day, -3, getutcdate())
		  and IsNull(msg.[Status], -1) = -1
END

﻿/*
 * Fills up the user profiles with the daily lottery coins
 */
ALTER PROCEDURE [dbo].[sp_AddDailyLotteryCoins]
AS
BEGIN
	declare @LotteryGameId bigint
	declare @DailyCoinsToAdd int
	declare @UID bigint
	declare @PossessedDailyCoins int
	declare @CoinsExpiryTime datetime

	declare @CoinsCanExpire bit
	declare @CoinsExpiryType int
	declare @CoinsExpiryAbsDate datetime
	declare @CoinsExpiryPeriod int

	declare cLotteryGames cursor for select [Id], [DailyCoins], [CoinsCanExpire], [CoinsExpiryType], [CoinsExpiryDate], [CoinsExpiryPeriod] from [dbo].[LotteryGames] 
									  where [Status] in (1, 2, 9) 
										and (getutcdate() >= [StartTime] and getutcdate() <= [EndTime])
									    and [DailyCoins] > 0
	open cLotteryGames
	fetch next from cLotteryGames into @LotteryGameId, @DailyCoinsToAdd, @CoinsCanExpire, @CoinsExpiryType, @CoinsExpiryAbsDate, @CoinsExpiryPeriod

	while @@fetch_status = 0
	begin
		set @CoinsExpiryTime = '2099-12-31 23:59:59.997'
		if @CoinsCanExpire = 1 begin
			if not @CoinsExpiryAbsDate is null begin
				set @CoinsExpiryTime = @CoinsExpiryAbsDate
			end else begin
				set @CoinsExpiryTime = case when @CoinsExpiryType = 0 then -- end of day
													dateadd(hour, -8, format(year(getdate()), '0000') + '-' + format(month(getdate()), '00') + '-' + format(day(getdate()), '00') + ' 23:59:59.997') 
											when @CoinsExpiryType = 1 then -- end of week
													DATEADD(hour, -8, CONVERT(varchar, DATEADD(dd, DATEDIFF(dd, 0, DATEADD(dd, 7-((DATEPART(dw, getdate()) + 5) % 7 + 1), getdate())), 0), 23) + ' 23:59:59.997')
											when @CoinsExpiryType = 2 then -- end of month
													DATEADD(hour, -8, format(year(EOMONTH(getdate())), '0000') + '-' + format(month(EOMONTH(getdate())), '00') + '-' + format(day(EOMONTH(getdate())), '00') + ' 23:59:59.997')
											when @CoinsExpiryType = 10 then -- days after issue date
													DATEADD(hour, -8, convert(varchar, dateadd(day, @CoinsExpiryPeriod, convert(date, getdate())), 23) + ' 23:59:59.997')
											when @CoinsExpiryType = 11 then -- weeks after issue date
													DATEADD(hour, -8, convert(varchar, dateadd(week, @CoinsExpiryPeriod, convert(date, getdate())), 23) + ' 23:59:59.997')
											when @CoinsExpiryType = 12 then -- months after issue date
													DATEADD(hour, -8, convert(varchar, dateadd(month, @CoinsExpiryPeriod, convert(date, getdate())), 23) + ' 23:59:59.997')
											else -- default: end of month
												DATEADD(hour, -8, format(year(EOMONTH(getdate())), '0000') + '-' + format(month(EOMONTH(getdate())), '00') + '-' + format(day(EOMONTH(getdate())), '00') + ' 23:59:59.997')
											end
			end
		end

		declare cUsers cursor for select [UID] from [dbo].[UserDatas] where [Status] = 1
		
		open cUsers
		fetch next from cUsers into @UID
		while @@fetch_status = 0
		begin
			set @PossessedDailyCoins = 0
			select @PossessedDailyCoins = IsNull(Sum([Amount]), 0) 
			  from [dbo].[UserLotteryCoins] 
			 where [LotteryGameId] = @LotteryGameId
			   and [Type] = 1 -- 1 = Daily, 0 = Earned

			if @PossessedDailyCoins < 3 begin
				insert into [dbo].[LotteryCoinLogs] ([UID], [Type], [TransactionTime], [Coins])
											 Values (@UID,  1,      getutcdate(),      3 - @PossessedDailyCoins)
				insert into [dbo].[UserLotteryCoins] ([LotteryGameId], [UID], [Type], [Amount],                 [OriginalAmount],		  [Status], [DateAquired], [ExpiryDate])
											 Values (@LotteryGameId,   @UID,  1,      3 - @PossessedDailyCoins, 3 - @PossessedDailyCoins, 1,        getutcdate(),  @CoinsExpiryTime)
			end

			fetch next from cUsers into @UID
		end

		close cUsers
		deallocate cUsers

		fetch next from cLotteryGames into @LotteryGameId, @DailyCoinsToAdd, @CoinsCanExpire, @CoinsExpiryType, @CoinsExpiryAbsDate, @CoinsExpiryPeriod
	end

	close cLotteryGames
	deallocate cLotteryGames
END

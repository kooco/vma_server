﻿ALTER PROCEDURE [dbo].[sp_CompleteMachineOrder]
	@Organization varchar(20),
	@AssetNo varchar(32),
	@InternalId int,
	@VmaTransactionId bigint,
	@BuyBuyTransactionNr varchar(32),
	@Cancel bit,
	@PointsExpiryDate datetime,
	@SalesTransExpiryMinutes int
AS
BEGIN
	DECLARE @SalesTransactionId bigint
	DECLARE @VendingMachineId bigint
	DECLARE @StampsQRToken varchar(32)
	DECLARE @SalesStatus int
	DECLARE @UID bigint
	DECLARE @ProductId bigint
	DECLARE @Message varchar(200)
	DECLARE @ReturnValue int
	DECLARE @SalesCreateTime datetime

	SET @SalesTransactionId = 0
	SET @SalesStatus = 0
	SET @UID = null

	if (IsNull(@VmaTransactionId, 0) > 0) begin
		-- we use an existing VMA transaction
		SELECT @SalesTransactionId = trans.[Id], @StampsQRToken = trans.[QRToken], @SalesStatus = trans.[Status], @ProductId = [ProductId], @VendingMachineId = trans.[VendingMachineId], @UID = trans.[UID], @SalesCreateTime = trans.[CreateTime]
		  FROM [dbo].[SalesTransactions] trans
		 WHERE trans.[Id] = @VmaTransactionId

		IF IsNull(@SalesTransactionId, 0) = 0 begin
			select 'There is no VMA transaction with the id ' + Cast(@VmaTransactionId as varchar(20)) + '!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		end
		if @SalesStatus = -1 or getutcdate() > dateadd(minute, @SalesTransExpiryMinutes, @SalesCreateTime) begin
			if @SalesStatus <> -1 begin
				update [dbo].[SalesTransactions] SET [Status] = -1 where [Id] = @SalesTransactionId
			end
			if @Cancel = 0 begin
				select 'This VMA transaction already has been cancelled because of a timeout!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
				return -303
			end	else begin
				select 'Transaction already cancelled' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
				return 5
			end
		end
		if @SalesStatus <> 0 begin
			SELECT 'This VMA transaction already has been completed and can not be completed again!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -304
		end

		if @Cancel = 1 begin
			if IsNull(@VmaTransactionId, 0) <> 0 begin
				UPDATE [dbo].[SalesTransactions]
				   SET [Status] = -1,
					   [ModifyTime] = getutcdate()
				 WHERE [Id] = @VmaTransactionId
		     end

			 select 'Transaction successfully cancelled' As [Message], '' As [QRToken], @UID As [UID], @ProductId As [ProductId]
			 return 5
		end else begin
			UPDATE [dbo].[SalesTransactions]
			   SET [Status] = 1,
				   [ModifyTime] = getutcdate()
			 WHERE [Id] = @VmaTransactionId
	    end
	end else begin
		-- we should use the BuyBuyTransactionNr (the user bought the product without the app)

		DECLARE @SeqNr bigint
		DECLARE @TransactionReceiptNr as varchar(64)

		   SET @VendingMachineId = 0
		SELECT @VendingMachineId = [Id]
		  FROM [dbo].[VendingMachines] WHERE [MOCCPOrganization] = @Organization AND [MOCCPAssetNo] = @AssetNo
       
		IF IsNull(@VendingMachineId, 0) = 0 BEGIN
			SELECT 'There is no vending machine record in the VMA database for the assetNo ' + @AssetNo + '!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		END
		IF IsNull(@InternalId, 0) = 0 begin
			SELECT 'The argument "internalId" is missing. This parameter is needed if the BuyBuyTransactionNr is used instead of the VmaTransactionId! (transaction without the use of the smartphone app)' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10007
		end
		if IsNull(@BuyBuyTransactionNr, '') = '' begin
			SELECT 'The argument "buybuyTransactionNr" is missing. This parameter is needed if no VmaTransactionId is used! (transaction without the use of the smartphone app)' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10007
		end

		   SET @ProductId = 0
		SELECT @ProductId = [Id]
		  FROM [dbo].[VendingProducts] WHERE [MOCCPInternalId] = @InternalId AND [MOCCPOrganization] = @Organization

		if IsNull(@ProductId, 0) = 0 begin
			SELECT 'There is no product with the internalId ' + cast(@InternalId as varchar(20)) + ' in the VMA database!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		end

		SELECT @SeqNr = NEXT VALUE FOR [dbo].[seq_SalesTransactionReceiptNr]
		SET @TransactionReceiptNr = format(datepart(ms, getdate()), '000') + Format(Cast((rand() * 1000) as int), '000') + Format(@SeqNr, '00000000')

		INSERT INTO [dbo].[SalesTransactions] ([ReceiptNumber],       [UID], [PurchaseType], [VendingMachineId], [ProductId], [Price], [Status], [QRToken],   [CreateTime], [ModifyTime], [BuyBuyTransactionNr])
									   Values (@TransactionReceiptNr, null,  2,              @VendingMachineId,  @ProductId,  0,       1,        'temptoken', getutcdate(), getutcdate(), @BuyBuyTransactionNr)

		SET @SalesTransactionId = scope_identity()
		SET @StampsQRToken = [dbo].[fnGenerateQRToken](3, @SalesTransactionId)

		UPDATE [dbo].[SalesTransactions] SET [QRToken] = @StampsQRToken WHERE [Id] = @SalesTransactionId
	end

	SET @ReturnValue = 1

	SELECT @Message As [Message], @StampsQRToken AS [QRToken], @UID As [UID], @ProductId As [ProductId] -- StampsQRToken will be returned only if there are stamps to be collected or if the transaction is done without the app (for collecting points)
	RETURN @ReturnValue
END
﻿/*
 * sp_LotteryDrawGame
 * ==================
 *
 * Draws the lottery game for the given user with the chance to win a price.
 * the LotteryGameId is assumed to exist, because it's already checked by the LotteryGameProvider.
 *
 * Note: AmountUsable always will be 0 or 1 (if won something) for the draw result! AmountRedeemed always 0!
 *
 */
ALTER PROCEDURE [dbo].[sp_LotteryDrawGame]
	@UID bigint,
	@LotteryGameId bigint
AS
BEGIN
	declare @WonPriceId bigint
	declare @UserDailyCoins int
	declare @UserEarnedCoins int
	declare @LotteryTestMode int
	declare @FestivalTime bit
	declare @CanWin bit
	declare @MacauTime datetime
	declare @GameLogId bigint
	declare @ItemId bigint
	declare @SeqNr int
	declare @PickupNumber nvarchar(12)
	declare @CouponId bigint

	declare @PriceUseActivityCoupon bit
	declare @PriceActivityId bigint
	declare @IsVendingMachinePrice bit
	declare @LotteryCoinLogId bigint
	declare @LotteryExpiryTime datetime
	declare @ActivityCouponId bigint

	set @IsVendingMachinePrice = 0

	set @UserDailyCoins = IsNull((select Sum([Amount]) from [dbo].[UserLotteryCoins] WHERE [UID] = @UID AND [LotteryGameId] = @LotteryGameId AND [Type] = 1 AND [ExpiryDate] > getutcdate() AND [Status] = 1), 0) -- Type 1 = Daily Coins
	set @UserEarnedCoins = IsNull((select Sum([Amount]) from [dbo].[UserLotteryCoins] WHERE [UID] = @UID AND [LotteryGameId] = @LotteryGameId AND [Type] = 0 AND [ExpiryDate] > getutcdate() AND [Status] = 1), 0) -- Type 0 = Earned Coins

	if ((@UserDailyCoins + @UserEarnedCoins) <= 0) begin
		select '' as [Message], Cast(0 as bit) as [Won], (@UserDailyCoins + @UserEarnedCoins) as [CoinsLeft], Cast(0 as bigint) as [PriceId], '' as [PriceTitle], '' as [PriceImageUrl], '' as [PriceUnit], '' as [PricePickupNumber], '' as [PricePickupAddress], '' as [PricePickupPhone], @IsVendingMachinePrice as [IsVendingMachinePrice], 0 as [PriceAmountUsable], 0 as [PriceAmountRedeemed]
		return -404 -- not enough coins
	end	

	-- reduce the lottery coins
	insert into [dbo].[LotteryCoinLogs] ([UID], [Type], [TransactionTime], [Coins], [LotteryDrawLogId])
								 Values (@UID,  10,     getutcdate(),      -1,      null)
	set @LotteryCoinLogId = scope_identity()

	exec dbo.sp_UseLotteryCoins @LotteryGameId, @UID, 1

	-- win chance
	set @CanWin = (case when cast(substring(cast(CHECKSUM(NEWID()) as varchar(14)), 3, 1) as int) + cast((rand() * 10) as int) >= 12 then 1 else 0 end)
	if exists(select 1 from dbo.LotteryPriceCoupons where UID = @UID and TimeWon >= dateadd(minute, -5, getutcdate())) begin
		-- this user already won within the last 5 minutes - lower the chance to win again 
		-- (otherwise the chance is too high to win many items in a row because of the poor random generator)
		set @CanWin = (case when cast(substring(cast(CHECKSUM(NEWID()) as varchar(14)), 3, 1) as int) + cast((rand() * 10) as int) <= 6 then 1 else 0 end)
	end

	if @CanWin = 0 begin
		-- insert game log
		insert into [dbo].[LotteryDrawLogs]([LotteryGameId], [UID], [DrawDate],					[DrawTime],   [Won], [LotteryPriceIdWon], [LotteryPriceCouponId])
									Values (@LotteryGameId,  @UID,  cast(getutcdate() as date), getutcdate(), 0,     null,                null)
		update [dbo].[LotteryCoinLogs] set [LotteryDrawLogId] = scope_identity() where [Id] = @LotteryCoinLogId

		-- return "lost"
		select '' as [Message], Cast(0 as bit) as [Won], (@UserDailyCoins + @UserEarnedCoins) as [CoinsLeft], Cast(0 as bigint) as [PriceId], '' as [PriceTitle], '' as [PriceImageUrl], '' as [PriceUnit], '' as [PricePickupNumber], '' as [PricePickupAddress], '' as [PricePickupPhone], @IsVendingMachinePrice as [IsVendingMachinePrice], 0 as [PriceAmountUsable], 0 as [PriceAmountRedeemed]
		return 1
	end

	set @LotteryTestMode = (select [TestMode] from [dbo].[LotteryGames] where [Id] = @LotteryGameId)

	declare @TestWinGoodItem bit
	if @LotteryTestMode = 2 begin		
		set @TestWinGoodItem = (case when cast((rand() * 10) as int) >= 8 then 1 else 0 end)
	end

	-- check the next price that can be won
	set @ItemId = 0
	select top 1 @ItemId = [Id]
	  from [dbo].[LotteryPriceAvailabilityItems]
	 where [LotteryGameId] = @LotteryGameId
	   and (getutcdate() >= [WinFromTime] or @LotteryTestMode > 0)
	   and [Status] = 1
	   and (not [LotteryPriceId] in (  -- make sure, that the same price for more valuable prices is not won by the same user two times
				select price.[Id]
				  from [dbo].[LotteryPriceCoupons] cp
		    inner join [dbo].[LotteryPrices] price on price.[Id] = cp.[LotteryPriceId]
				 where cp.[UID] = @UID
				   and cp.[Status] in (1, 2)
				   and price.[LotteryGameId] = @LotteryGameId
				   and price.[AmountAvailable] <= 500)
		    or @LotteryTestMode > 0)
	   and (@LotteryTestMode <> 2 or @TestWinGoodItem = 0 or   -- in test mode 2 use a higher chance to win a good item
	        [LotteryPriceId] in (
				select [Id]
				  from [dbo].[LotteryPrices]
				 where [AmountAvailable] <= 100
				   and [LotteryGameId] = @LotteryGameId))
	 order by newid()
	
	if IsNull(@ItemId, 0) <> 0 begin
		-- lock that item
		update [dbo].[LotteryPriceAvailabilityItems]
		   set [Status] = [Status]
		 where [Id] = @ItemId

		-- item is still available?
		if ((select [Status] from [dbo].[LotteryPriceAvailabilityItems] where [Id] = @ItemId) = 1) begin
			-- yes -> update it's status and "WonById"
			update [dbo].[LotteryPriceAvailabilityItems]
			   set [WonByUID] = @UID, [WonTime] = getutcdate(), [Status] = 9
			 where [Id] = @ItemId

			waitfor delay '00:00:00:200'

			-- double check - is the user who won the price still the same?
			if ((select [WonByUID] from [dbo].[LotteryPriceAvailabilityItems] where [Id] = @ItemId) = @UID) begin
				-- yes. okay -- now we can consider this price as won

				declare @PriceTitle nvarchar(64)
				declare @PriceImageUrl nvarchar(128)
				declare @PriceUnit nvarchar(4)
				declare @PricePickupAddress nvarchar(128)
				declare @PricePickupPhone nvarchar(32)
				
				-- get id of the price won and other data
				select @WonPriceId = avail.[LotteryPriceId], @PriceUseActivityCoupon = price.[UseActivityCoupon], @PriceTitle = [Title], @PriceImageUrl = [ImageUrl], @PriceUnit = [Unit], @PricePickupAddress = [PickupAddress], @PricePickupPhone = [PickupPhone]
				  from [dbo].[LotteryPriceAvailabilityItems] avail
			inner join [dbo].[LotteryPrices] price on avail.[LotteryPriceId] = price.[Id]
				 where avail.[Id] = @ItemId

				set @IsVendingMachinePrice = @PriceUseActivityCoupon

				-- before we insert something, check if the price won is an activity coupon
				if @PriceUseActivityCoupon = 1 begin
					set @PriceActivityId = 0
					select @PriceActivityId = [ConnectedActivityId], @LotteryExpiryTime = [CouponsExpiryTime]
					  from [dbo].[LotteryGames]
					 where [Id] = @LotteryGameId

				   if IsNull(@PriceActivityId, 0) = 0 begin
					  select 'There are activity coupon prices in this lottery game, but the lottery game is not connected with an activity!' as [Message], Cast(0 as bit) as [Won], (@UserDailyCoins + @UserEarnedCoins) as [CoinsLeft], Cast(0 as bigint) as [PriceId], '' as [PriceTitle], '' as [PriceImageUrl], '' as [PriceUnit], '' as [PricePickupNumber], '' as [PricePickupAddress], '' as [PricePickupPhone], @IsVendingMachinePrice as [IsVendingMachinePrice], 0 as [PriceAmountUsable], 0 as [PriceAmountRedeemed]
					  return -10900 -- internal server error (we loose the coin then.. but well, it should actually never happen...)
				   end
				end

				-- generate pickup number
				SELECT @SeqNr = NEXT VALUE FOR [dbo].[seq_LotteryPickupNumber]
				SET @PickupNumber = substring(format(datepart(ms, getdate()), '00'), 2, 2) + format(datepart(day, getdate()), '00') + Format(Cast((rand() * 10000) as int), '0000') + Format(@SeqNr, '0000')
				while exists(select 1 from [dbo].[LotteryPriceCoupons] where [LotteryGameId] = @LotteryGameId and [PickupNumber] = @PickupNumber) begin
					SET @PickupNumber = substring(Format(Cast((rand() * 100) as int), '00'), 2, 2) + format(datepart(day, getdate()), '00') + Format(Cast((rand() * 10000) as int), '0000') + Format(@SeqNr, '0000')
				end
												 
				-- if the price is a activity coupon price, then also add the activity coupon
				set @ActivityCouponId = null
				-- (price will be redeemed directly without a UserCoupon record, by just adding a redeem message for the machine)
				--if @PriceUseActivityCoupon = 1 begin
				--	insert into [dbo].[UserCoupons] ([ActivityId],	   [UID], [Status], [ExpiryTime],		[StartTime],  [CompletionTime], [CouponToken], [CompletedBy])
				--							 Values (@PriceActivityId, @UID,  2,        @LotteryExpiryTime, getutcdate(), getutcdate(),     @PickupNumber, @UID)
				--	set @ActivityCouponId = scope_identity()
				--end

				-- insert winning coupon
				insert into [dbo].[LotteryPriceCoupons] ([LotteryGameId], [LotteryPriceId], [UID], [PickupNumber], [TimeWon],    [ActivityCouponId], [Status])
												 Values (@LotteryGameId,  @WonPriceId,      @UID,  @PickupNumber,  getutcdate(), @ActivityCouponid,  1)
				set @CouponId = scope_identity()

				-- insert game log	
				insert into [dbo].[LotteryDrawLogs]([LotteryGameId], [UID], [DrawDate],					[DrawTime],   [Won], [LotteryPriceIdWon], [LotteryPriceCouponId])
									        Values (@LotteryGameId,  @UID,  cast(getutcdate() as date), getutcdate(), 1,     @WonPriceId,         @CouponId)
				update [dbo].[LotteryCoinLogs] set [LotteryDrawLogId] = scope_identity() where [Id] = @LotteryCoinLogId

				select '' as [Message], Cast(1 as bit) as [Won], (@UserDailyCoins + @UserEarnedCoins) as [CoinsLeft], @WonPriceId as [PriceId], @PriceTitle as [PriceTitle], @PriceImageUrl as [PriceImageUrl], @PriceUnit as [PriceUnit], @PickupNumber as [PricePickupNumber], @PricePickupAddress as [PricePickupAddress], @PricePickupPhone as [PricePickupPhone], @IsVendingMachinePrice as [IsVendingMachinePrice], 1 as [PriceAmountUsable], 0 as [PriceAmountRedeemed]
				return 1
			end
		end
	end

	set @UserDailyCoins = IsNull((select Sum([Amount]) from [dbo].[UserLotteryCoins] WHERE [UID] = @UID AND [LotteryGameId] = @LotteryGameId AND [Type] = 1 AND [ExpiryDate] > getutcdate() AND [Status] = 1), 0) -- Type 1 = Daily Coins
	set @UserEarnedCoins = IsNull((select Sum([Amount]) from [dbo].[UserLotteryCoins] WHERE [UID] = @UID AND [LotteryGameId] = @LotteryGameId AND [Type] = 0 AND [ExpiryDate] > getutcdate() AND [Status] = 1), 0) -- Type 0 = Earned Coins

	select '' as [Message], Cast(0 as bit) as [Won], (@UserDailyCoins + @UserEarnedCoins) as [CoinsLeft], Cast(0 as bigint) as [PriceId], '' as [PriceTitle], '' as [PriceImageUrl], '' as [PriceUnit], '' as [PricePickupNumber], '' as [PricePickupAddress], '' as [PricePickupPhone], @IsVendingMachinePrice as [IsVendingMachinePrice], 0 as [PriceAmountUsable], 0 as [PriceAmountRedeemed]
	return 1
END
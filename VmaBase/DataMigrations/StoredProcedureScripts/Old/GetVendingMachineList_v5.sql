﻿ALTER PROCEDURE [dbo].[sp_GetVendingMachineList]
	@Latitude float = null,
	@Longitude float = null,
	@RadiusMeters bigint = 1000,
	@OnlyActive bit = 1,
	@OnlyRecommended bit = 0,
	@PageNr int,
	@PageSize int
AS
BEGIN
	declare @GeoLocation geography
	set @GeoLocation = null

	if (IsNull(@Latitude, 0) <> 0) AND (IsNull(@Longitude, 0) <> 0) begin
		set @GeoLocation = geography::Point(@Latitude, @Longitude, 4326)
	end

	if (@GeoLocation is null) begin
		SELECT [Id],
		       [MOCCPAssetNo],
			   [Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   Cast([GeoLocation].Lat As decimal(15, 8)) [Latitude],
			   Cast([GeoLocation].Long As decimal(15, 8)) [Longitude],
			   Cast(0 as bigint) [DistanceMeters],
			   [Status],
			   [IsRecommended],
			   (case when [IsRecommended] = 1 then (select top 1 act.[Id] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else null end) [ActivityId],
			   (case when [IsRecommended] = 1 then (select top 1 act.[Title] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else N'' end) [ActivityTitle]
		  FROM dbo.[VendingMachines] vm
		 WHERE ([Status] = 1 Or @OnlyActive = 0)
		   AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
	  ORDER BY [Name]
	  OFFSET ((@PageNr - 1) * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY -- SQL Server 2012 syntax
	 end else begin
		SELECT [Id],
			   [MOCCPAssetNo],
			   [Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   [Latitude],
			   [Longitude],
			   [DistanceMeters],
			   [Status],
			   [IsRecommended],
			   [ActivityId],
			   [ActivityTitle]
		  FROM (
			SELECT [Id],
				   [MOCCPAssetNo],
				   [Name],
				   [City],
				   [Location],
				   [QRToken],
				   [ImageUrl],
				   Cast([GeoLocation].Lat as decimal(15, 8)) [Latitude],
				   Cast([GeoLocation].Long as decimal(15, 8)) [Longitude],
				   Cast([GeoLocation].STDistance(@GeoLocation) As bigint) [DistanceMeters],
				   [Status],
				   [IsRecommended],
			       (case when [IsRecommended] = 1 then (select top 1 act.[Id] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else null end) [ActivityId],
			       (case when [IsRecommended] = 1 then (select top 1 act.[Title] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else N'' end) [ActivityTitle]
			  FROM dbo.[VendingMachines] vm
			 WHERE ([Status] = 1 Or @OnlyActive = 0)
			   AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
			   AND @GeoLocation.STDistance([GeoLocation]) <= @RadiusMeters
			) VendingMachinesWithDistance
		ORDER BY [DistanceMeters]
		OFFSET ((@PageNr - 1) * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY -- SQL Server 2012 syntax
	 end
END
﻿/*
	sp_GetCouponActivityList
	========================

	Returns a list of activities with infos about completed coupons
*/
ALTER PROCEDURE [dbo].[sp_GetCouponActivityList]
	@UID bigint,
	@OnlyCompleted bit -- Flag, if only activities should be returned, which have at least one completed, not already redeemed coupon
AS
BEGIN
	SELECT act.[Id] ActivityId,
	       news.[Title] Title,
		   act.[ActivityText] ActivityText,
		   act.[LimitStartTime],
		   act.[LimitEndTime],
		   news.[ListImageUrl] ImageUrl,
		   Count(act.[Id]) AmountTotal,
		   Count(act.[Id]) AmountSendable
	  FROM [dbo].[CouponActivities] act
INNER JOIN [dbo].[NewsArticles] news ON news.[ActivityId] = act.[Id]
INNER JOIN [dbo].[NewsCategories] cat ON cat.[Id] = news.[CategoryId]
LEFT OUTER JOIN [dbo].[UserCoupons] usrcp ON usrcp.[ActivityId] = act.[Id]    
     WHERE news.[Status] = 1
	   AND act.[Status] = 1
	   AND cat.[Code] = 'ACT'
	   AND getutcdate() >= act.[LimitStartTime]
	   AND getutcdate() <= act.[LimitEndTime]
	   AND usrcp.[Status] = 2
	   AND usrcp.[UID] = @UID
  GROUP BY act.[Id], news.[Title], act.[ActivityText], act.[LimitStartTime], act.[LimitEndTime], news.[ListImageUrl]
    HAVING Count(act.[Id]) > 0 OR IsNull(@OnlyCompleted, 0) = 0
  ORDER BY [LimitStartTime] desc

	return 1
END

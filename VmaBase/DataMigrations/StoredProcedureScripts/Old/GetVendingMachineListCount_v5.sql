﻿ALTER PROCEDURE [dbo].[sp_GetVendingMachineListCount]
	@Latitude float = null,
	@Longitude float = null,
	@RadiusMeters bigint = 1000,
	@OnlyActive bit = 1,
	@ActivityId bigint = null,
	@OnlyRecommended bit = 0,
	@OnlyAppActivated bit = 1,
	@IsTestUser bit = 0
AS
BEGIN
	declare @GeoLocation geography
	set @GeoLocation = null

	if (IsNull(@Latitude, 0) <> 0) AND (IsNull(@Longitude, 0) <> 0) begin
		set @GeoLocation = geography::Point(@Latitude, @Longitude, 4326)
	end

	if (@GeoLocation is null) begin
			SELECT Cast(Count(vm.[Id]) as bigint) MachinesCount
			  FROM dbo.[VendingMachines] vm
   LEFT OUTER JOIN dbo.[ActivityParticipatingMachines] pm on pm.[VendingMachineId] = vm.[Id] and pm.[ActivityId] = IsNull(@ActivityId, 0) and pm.[Status] = 1
			 WHERE (vm.[Status] = 1 Or @OnlyActive = 0)
			   AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
			   AND (pm.[ActivityId] = @ActivityId or IsNull(@ActivityId, 0) = 0)
			   AND (vm.[AppStatus] = 1 Or (IsNull(@OnlyAppActivated, 1) = 0 and [AppStatus] = 0) Or (IsNull(@IsTestUser, 0) = 1 And [AppStatus] = 2))
	 end else begin
		SELECT Cast(Count(vm.[Id]) as bigint) MachinesCount
			FROM dbo.[VendingMachines] vm
 LEFT OUTER JOIN dbo.[ActivityParticipatingMachines] pm on pm.[VendingMachineId] = vm.[Id] and pm.[ActivityId] = IsNull(@ActivityId, 0) and pm.[Status] = 1
			WHERE (vm.[Status] = 1 Or @OnlyActive = 0)
			AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
			AND @GeoLocation.STDistance([GeoLocation]) <= @RadiusMeters
			AND (pm.[ActivityId] = @ActivityId or IsNull(@ActivityId, 0) = 0)
			AND (vm.[AppStatus] = 1 Or (IsNull(@OnlyAppActivated, 1) = 0 and [AppStatus] = 0) Or (IsNull(@IsTestUser, 0) = 1 And [AppStatus] = 2))
	 end
END
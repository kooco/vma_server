﻿CREATE PROCEDURE [dbo].[sp_GetVendingMachineListCount]
	@Latitude float = null,
	@Longitude float = null,
	@RadiusMeters bigint = 1000,
	@OnlyActive bit = 1
AS
BEGIN
	declare @GeoLocation geography
	set @GeoLocation = null

	if (IsNull(@Latitude, 0) <> 0) AND (IsNull(@Longitude, 0) <> 0) begin
		set @GeoLocation = geography::Point(@Latitude, @Longitude, 4326)
	end

	if (@GeoLocation is null) begin
		SELECT Cast(Count([Id]) as bigint) MachinesCount
		  FROM dbo.[VendingMachines]
		 WHERE ([Status] = 1 Or @OnlyActive = 0)
	 end else begin
		SELECT Cast(Count([Id]) as bigint) MachinesCount
			FROM dbo.[VendingMachines]
			WHERE ([Status] = 1 Or @OnlyActive = 0)
			AND @GeoLocation.STDistance([GeoLocation]) <= @RadiusMeters
	 end
END
﻿CREATE PROCEDURE [dbo].[sp_GetMachineMsgProductSelection]
	@Organization varchar(20),
	@AssetNo varchar(32)
AS
BEGIN
	DECLARE @MessageId bigint
	DECLARE @SalesTransactionId bigint
	DECLARE @ProductId bigint

	SET @MessageId = 0
	SELECT TOP 1 @MessageId = [Id], @SalesTransactionId = [SalesTransactionId], @ProductId = [ProductId]
	  FROM [dbo].[VendingMachineMessages]
	 WHERE [Status] = 0 -- 0 = not queried yet, 1 = queried
	   AND [Direction] = 0 -- 0 = Outgoing, 1 = Incoming
	   AND [Type] = 10 -- ProductChosen = 10, RedeemCoupon = 11, TransactionFinished = 20
	   AND [AssetNo] = @AssetNo

	IF IsNull(@MessageId, 0) = 0 BEGIN
		SELECT '' As [MerchandiseID], Cast(-1 as bigint) As [TransactionId]
	END ELSE BEGIN
		SELECT [MOCCPGoodsId] As [MerchandiseID],
			   @SalesTransactionId As [TransactionId]
		  FROM [dbo].[VendingProducts]
		 WHERE [Id] = @ProductId
	END

	IF IsNull(@MessageId, 0) <> 0 BEGIN
		UPDATE [dbo].[VendingMachineMessages]
		   SET [Status] = 1
	     WHERE [Id] = @MessageId
	END
END
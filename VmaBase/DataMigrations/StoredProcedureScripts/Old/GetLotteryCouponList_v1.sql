﻿CREATE PROCEDURE [dbo].[sp_GetLotteryCouponList]
	@LotteryGameId bigint,
	@UID bigint,
	@OnlyUsable bit = 0
AS
BEGIN

	select [PriceId],
		   [Title],
		   [ImageUrl],
		   [Unit],
		   [PickupNumber],
		   [PickupAddress],
		   [PickupPhone],
		   [IsVendingMachinePrice],
		   Count(case when [Status] = 1 then 1 else 0 end) [AmountUsable],
		   Count(case when [Status] = 2 then 1 else 0 end) [AmountRedeemed]
	  from
	   (
			select coupon.[PriceId],
				   price.[Title],
				   price.[ImageUrl],
				   price.[Unit],
				   (case when price.[UseActivityCoupon] = 1 then Cast(null as nvarchar(12)) else coupon.[PickupNumber] end) As [PickupNumber],
				   price.[PickupAddress],
				   price.[PickupPhone],
				   coupon.[Status],
				   price.[UseActivityCoupon] [IsVendingMachinePrice]
			  from [dbo].[LotteryPriceCoupons] coupon
		inner join [dbo].[LotteryPrice] price on price.[Id] = [coupon].[LotteryPriceId]
			 where coupon.[LotteryGameId] = @LotteryGameId
			   and coupon.[UID] = @UID
			   and coupon.[Status] in (1, 2)
			   and (IsNull(@OnlyUsable, 0) = 0 or coupon.[Status] = 1)
	    ) usercoupon
END

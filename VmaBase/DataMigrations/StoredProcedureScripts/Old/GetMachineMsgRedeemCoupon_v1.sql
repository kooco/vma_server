﻿CREATE PROCEDURE [dbo].[sp_GetMachineMsgRedeemCoupon]
	@Organization varchar(20),
	@AssetNo varchar(32)
AS
BEGIN
	DECLARE @MessageId bigint
	DECLARE @CouponId bigint
	DECLARE @ProductId bigint

	SET @MessageId = 0
	SELECT TOP 1 @MessageId = [Id], @CouponId = [CouponId], @ProductId = [ProductId]
	  FROM [dbo].[VendingMachineMessages]
	 WHERE [Status] = 0 -- 0 = not queried yet, 1 = queried
	   AND [Direction] = 0 -- 0 = Outgoing, 1 = Incoming
	   AND [Type] = 11 -- ProductChosen = 10, RedeemCoupon = 11, TransactionFinished = 20
	   AND [AssetNo] = @AssetNo

	IF IsNull(@MessageId, 0) = 0 BEGIN
		SELECT '' As [MerchandiseID], Cast(-1 as bigint) As [CouponId]
	END ELSE BEGIN
		-- just to make sure - "close" all other vending machine messages with the same coupon
		UPDATE [dbo].[VendingMachineMessages]
		   SET [Status] = -1
		 WHERE [Status] = 0
		   AND [Direction] = 0
		   AND [Type] = 11
		   AND [AssetNo] <> @AssetNo
		   AND [CouponId] = @CouponId

		-- update coupon
		UPDATE [dbo].[UserCoupons]
		   SET [Status] = 3 -- 3 = redeemed
		 WHERE [Id] = @CouponId

		-- now select the product
		SELECT [MOCCPGoodsId] As [MerchandiseID],
			   @CouponId As [CouponId]
		  FROM [dbo].[VendingProducts]
		 WHERE [Id] = @ProductId
	END

	IF IsNull(@MessageId, 0) <> 0 BEGIN
		UPDATE [dbo].[VendingMachineMessages]
		   SET [Status] = 1,
		       [QueryTime] = getutcdate()
	     WHERE [Id] = @MessageId
	END
END
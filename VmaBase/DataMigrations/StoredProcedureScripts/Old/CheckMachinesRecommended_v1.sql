﻿/*
 * sp_CheckMachinesRecommended
 * ---------------------------
 *
 * Refreshes the "IsRecommended" state of the machines
 * by checking which machines are explicitelly part of a currently active activity
 */
CREATE PROCEDURE [dbo].[sp_CheckMachinesRecommended]
AS
BEGIN
	UPDATE [dbo].[VendingMachines]
	   SET [IsRecommended] = 0
	 WHERE [Id] NOT IN (SELECT [VendingMachineId] 
						  from [dbo].[ActivityParticipatingMachines] pm 
					inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId]
					inner join [dbo].[NewsArticles] art on art.[ActivityId] = act.[Id]
					     where act.[Status] = 1
						   and getutcdate() >= act.[LimitStartTime] and getutcdate() <= act.[LimitEndTime]
						   and art.[Status] = 1
						   and pm.[Status] = 1)

	UPDATE [dbo].[VendingMachines]
	   SET [IsRecommended] = 1
	 WHERE [Id] IN (SELECT [VendingMachineId] 
					from [dbo].[ActivityParticipatingMachines] pm 
			inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId]
			inner join [dbo].[NewsArticles] art on art.[ActivityId] = act.[Id]
					where act.[Status] = 1
					and getutcdate() >= act.[LimitStartTime] and getutcdate() <= act.[LimitEndTime]
					and art.[Status] = 1
					and pm.[Status] = 1)
END
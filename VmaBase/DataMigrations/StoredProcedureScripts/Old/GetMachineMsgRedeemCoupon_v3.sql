﻿ALTER PROCEDURE [dbo].[sp_GetMachineMsgRedeemCoupon]
	@Organization varchar(20),
	@AssetNo varchar(32)
AS
BEGIN
	DECLARE @MessageId bigint
	DECLARE @CouponId bigint
	DECLARE @LotteryPriceCouponId bigint
	DECLARE @ProductId bigint

	SET @MessageId = 0
	SET @CouponId = 0
	SET @LotteryPriceCouponId = 0

	SELECT TOP 1 @MessageId = [Id], @CouponId = [CouponId], @LotteryPriceCouponId = [LotteryPriceCouponId], @ProductId = [ProductId]
	  FROM [dbo].[VendingMachineMessages]
	 WHERE [Status] = 0 -- 0 = not queried yet, 1 = queried
	   AND [Direction] = 0 -- 0 = Outgoing, 1 = Incoming
	   AND [Type] = 11 -- ProductChosen = 10, RedeemCoupon = 11, TransactionFinished = 20
	   AND [AssetNo] = @AssetNo

	IF IsNull(@MessageId, 0) = 0 BEGIN
		SELECT '' As [MerchandiseID], Cast(0 as bigint) As [CouponId]
	END ELSE BEGIN
		if IsNull(@CouponId, 0) <> 0 begin
			-- redeem activity coupon

			-- just to make sure - "close" all other vending machine messages with the same coupon
			UPDATE [dbo].[VendingMachineMessages]
			   SET [Status] = -1
			 WHERE [Status] = 0
			   AND [Direction] = 0
			   AND [Type] = 11
			   AND [Id] <> @MessageId
			   AND [CouponId] = @CouponId

			-- update coupon
			UPDATE [dbo].[UserCoupons]
			   SET [Status] = 3 -- 3 = redeemed
			 WHERE [Id] = @CouponId
	    end else begin
			-- redeem lottery price

			-- just to make sure - "close" all other vending machine messages with the same price coupon
			UPDATE [dbo].[VendingMachineMessages]
			   SET [Status] = -1
			 WHERE [Status] = 0
			   AND [Direction] = 0
			   AND [Type] = 11
			   AND [Id] <> @MessageId
			   AND [LotteryPriceCouponId] = @LotteryPriceCouponId

			-- update coupon
			UPDATE [dbo].[LotteryPriceCoupons]
			   SET [Status] = 2 -- 2 = "picked up"
			 WHERE [Id] = @LotteryPriceCouponId
		end

		-- now select the product
		SELECT [MOCCPGoodsId] As [MerchandiseID],
			   [MOCCPInternalId] As [InternalId],
			   (case when (IsNull(@CouponId, 0) = 0) then -(@LotteryPriceCouponId) else @CouponId end) As [CouponId]
		  FROM [dbo].[VendingProducts]
		 WHERE [Id] = @ProductId
	END

	IF IsNull(@MessageId, 0) <> 0 BEGIN
		UPDATE [dbo].[VendingMachineMessages]
		   SET [Status] = 1,
		       [QueryTime] = getutcdate()
	     WHERE [Id] = @MessageId
	END
END
﻿/*
 * sp_LotterySpreadPrices
 * ======================
 *
 * This stored procedure will initiate the price winning system for the given lottery game.
 * It will create "LotteryPriceAvailabilityItems", with a date+time after which these items will be won.
 *
 * The "LotteryPriceAvailabilityItems" table is used for spreading the price items randomly over the lottery time range.
 * Every time the 'WinDate' is reached this item is considered as 'free to win'.
 * The next user will win this item then.
 * With that logic a random price winning is ensured and it's also ensured that
 * every price is given away until the end of the lottery game.
 * Also with this method it's possible to already see when every price will be given away before the lottery game starts
 * by the administrator.
 *
 * Respread
 * ========
 * To re-spread the prices it's just needed to call this procedure again.
 * Old availability items will be deleted before spreading the prices.
 * Already won prices WILL be considered! 
 * If you want the prices start from scratch, you have to set the [AmountWon] property of the prices to 0!
 *
 * This procedure has it's own transaction!
 */
ALTER PROCEDURE [dbo].[sp_LotterySpreadPrices]
	@LotteryGameId bigint
AS
BEGIN
	declare @StartTime datetime
	declare @EndTime datetime
	declare @TotalSeconds bigint
	declare @Blocks bigint
	declare @BlockSeconds bigint
	declare @RndSeconds bigint
	declare @BlockSecondsStart bigint
	declare @BlockNr bigint
	declare @WinDate datetime

	begin try
		begin transaction

		select @StartTime = (case when [TestMode] >= 1 then getutcdate() else [StartTime] end),
			   @EndTime = [EndTime]
		  from [dbo].[LotteryGames]
		 where [Id] = @LotteryGameId

		set @TotalSeconds = datediff(second, @StartTime, @EndTime)

		delete from [dbo].[LotteryPriceAvailabilityItems] where [LotteryGameId] = @LotteryGameId

		declare cPrices cursor for
			select [Id], ([AmountAvailable] - [AmountWon]), [UseSpreadingParameters]
			  from [dbo].[LotteryPrices]
			 where [LotteryGameId] = @LotteryGameId
		declare @PriceId bigint
		declare @AmountAvailable int
		declare @UseSpreadingParameters bit

		open cPrices
		fetch next from cPrices into @PriceId, @AmountAvailable, @UseSpreadingParameters
		while @@fetch_status = 0
		begin
			if @UseSpreadingParameters = 0 begin
				-- "Normal" Spreading Logic (spread the price average to the lottery's game time)
				-- ==============================================================================

				-- spread prices into "blocks" of seconds. Within every seconds block the price will be placed at a random second
				set @Blocks = @AmountAvailable
				set @BlockSeconds = @TotalSeconds / @Blocks
				set @BlockSecondsStart = 0
				set @BlockNr = 0

				while @BlockNr < @Blocks
				begin
					set @BlockNr = @BlockNr + 1				

					set @RndSeconds = Cast((rand() * @BlockSeconds) as bigint)
					set @WinDate = dateadd(second, @RndSeconds, dateadd(second, @BlockSecondsStart, @StartTime))

					insert into [dbo].[LotteryPriceAvailabilityItems] ([LotteryGameId], [LotteryPriceId], [WinFromTime], [WonTime], [Status])
															   Values (@LotteryGameId,  @PriceId,         @WinDate,      null,      1)

					-- the next block starts @BlockSeconds after
					set @BlockSecondsStart = @BlockSecondsStart + @BlockSeconds
				end
			end else begin
				-- Parameterized Spreading Logic (spread the prices with the given amounts to the given time blocks set in the price parameters table)
				-- ===================================================================================================================================

				declare cPriceParameters cursor for
					select [PeriodFrom], [PeriodTo], [SpreadCount]
					  from dbo.[LotteryPriceSpreadingParameters]
					 where [LotteryGameId] = @LotteryGameId
					   and [LotteryPriceId] = @PriceId

				declare @PeriodFromDate datetime
				declare @PeriodToDate datetime
				declare @SpreadCount int
				declare @TotalParamSeconds bigint

				open cPriceParameters
				fetch next from cPriceParameters into @PeriodFromDate, @PeriodToDate, @SpreadCount

				while @@fetch_status = 0
				begin

					-- spread prices into "blocks" of seconds within the given time range in the parameter. 
					-- Within every seconds block the price will be placed at a random second
					set @TotalParamSeconds = datediff(second, @PeriodFromDate, @PeriodToDate)
					set @Blocks = @SpreadCount
					set @BlockSeconds = @TotalParamSeconds / @Blocks
					set @BlockSecondsStart = 0
					set @BlockNr = 0

					while @BlockNr < @Blocks
					begin
						set @BlockNr = @BlockNr + 1				

						set @RndSeconds = Cast((rand() * @BlockSeconds) as bigint)
						set @WinDate = dateadd(second, @RndSeconds, dateadd(second, @BlockSecondsStart, @PeriodFromDate))

						insert into [dbo].[LotteryPriceAvailabilityItems] ([LotteryGameId], [LotteryPriceId], [WinFromTime], [WonTime], [Status])
																   Values (@LotteryGameId,  @PriceId,         @WinDate,      null,      1)

						-- the next block starts @BlockSeconds after
						set @BlockSecondsStart = @BlockSecondsStart + @BlockSeconds
					end					

					fetch next from cPriceParameters into @PeriodFromDate, @PeriodToDate, @SpreadCount
				end

				close cPriceParameters
				deallocate cPriceParameters
			end

			fetch next from cPrices into @PriceId, @AmountAvailable, @UseSpreadingParameters
		end

		close cPrices
		deallocate cPrices

		commit transaction
	end try
	begin catch
		rollback transaction;
		throw;
	end catch
END

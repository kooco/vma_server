﻿/*
 * Fills up the user profiles with the daily lottery coins
 */
CREATE PROCEDURE [dbo].[sp_AddDailyLotteryCoins]
AS
BEGIN
	if exists(select 1 from [dbo].[LotteryGames] 
					  where [Status] in (1, 2, 9) 
						and ((getutcdate() >= [StartTime] and getutcdate() <= [EndTime]) or
						     ([TestMode] > 0 AND getutcdate() >= [TestStartTime] and getutcdate() <= [EndTime])))
	begin
		-- a lottery game is running - set the daily coins now
		update [dbo].[UserDatas]
		   set [LotteryDailyCoins] = 3
		 where [Status] = 1
	end
END

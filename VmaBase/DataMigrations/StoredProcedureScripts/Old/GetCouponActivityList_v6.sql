﻿/*
	sp_GetCouponActivityList
	========================

	Returns a list of activities with infos about completed coupons
*/
ALTER PROCEDURE [dbo].[sp_GetCouponActivityList]
	@UID bigint,
	@OnlyCompleted bit, -- Flag, if only activities should be returned, which have at least one completed, not already redeemed coupon
	@VendingMachineId bigint = null -- Id of the Vending Machine to filter the activity list for. Only activities which are having this vending machine as part of it will be returned then
AS
BEGIN
	SELECT act.[Id] ActivityId,
	       news.[Title] Title,
		   act.[ActivityText] ActivityText,
		   act.[LimitStartTime],
		   act.[LimitEndTime],
		   news.[ListImageUrl] ImageUrl,
		   Cast(Count(usrcp.[Id]) as int) AmountTotal,
		   (case when act.[SendingCouponsAllowed] = 1 then Cast(Count(usrcp.[Id]) as int) else Cast(0 as int) end) As AmountSendable,
		   act.[SendingCouponsAllowed] [SendAllowed],
		   act.[NoStampsActivity] IsNoStampsActivity
	  FROM [dbo].[CouponActivities] act
INNER JOIN [dbo].[NewsArticles] news ON news.[ActivityId] = act.[Id]
INNER JOIN [dbo].[NewsCategories] cat ON cat.[Id] = news.[CategoryId]
LEFT OUTER JOIN [dbo].[UserCoupons] usrcp ON usrcp.[ActivityId] = act.[Id] AND usrcp.[Status] = 2 AND usrcp.[UID] = @UID
     WHERE news.[Status] = 1
	   AND act.[Status] = 1
	   AND cat.[Code] = 'ACT'
	   AND getutcdate() >= act.[LimitStartTime]
	   AND getutcdate() <= act.[LimitEndTime]
	   AND (IsNull(@VendingMachineId, 0) = 0 Or
	        Exists(SELECT 1 from [dbo].[ActivityParticipatingMachines] pm
						   where pm.ActivityId = act.[Id]
						     and pm.VendingMachineId = @VendingMachineId))
  GROUP BY act.[Id], act.[SendingCouponsAllowed], act.[NoStampsActivity], news.[Title], act.[ActivityText], act.[LimitStartTime], act.[LimitEndTime], news.[ListImageUrl]
    HAVING (Cast(Count(usrcp.[Id]) as int) > 0 OR IsNull(@OnlyCompleted, 0) = 0)
  ORDER BY [LimitStartTime] desc

	return 1
END

﻿ALTER PROCEDURE [dbo].[sp_CompleteMachineOrder]
	@Organization varchar(20),
	@AssetNo varchar(32),
	@MerchandiseId varchar(32),
	@VmaTransactionId bigint,
	@BuyBuyTransactionNr varchar(32),
	@PointsExpiryDate datetime,
	@SalesTransExpiryMinutes int
AS
BEGIN
	DECLARE @SalesTransactionId bigint
	DECLARE @VendingMachineId bigint
	DECLARE @StampsQRToken varchar(32)
	DECLARE @SalesStatus int
	DECLARE @UID bigint
	DECLARE @ProductId bigint
	DECLARE @Points int
	DECLARE @Message varchar(200)
	DECLARE @ReturnValue int
	DECLARE @SalesCreateTime datetime

	SET @SalesTransactionId = 0
	SET @SalesStatus = 0
	SET @UID = null

	if (IsNull(@VmaTransactionId, 0) > 0) begin
		-- we use an existing VMA transaction
		SELECT @SalesTransactionId = [Id], @StampsQRToken = [QRToken], @SalesStatus = [Status], @ProductId = [ProductId], @VendingMachineId = [VendingMachineId], @UID = [UID], @SalesCreateTime = [CreateTime]
		  FROM [dbo].[SalesTransactions]
		 WHERE [Id] = @VmaTransactionId

		IF IsNull(@SalesTransactionId, 0) = 0 begin
			select 'There is no VMA transaction with the id ' + Cast(@VmaTransactionId as varchar(20)) + '!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		end
		if @SalesStatus = -1 or getutcdate() > dateadd(minute, @SalesTransExpiryMinutes, @SalesCreateTime) begin
			if @SalesStatus <> -1 begin
				update [dbo].[SalesTransactions] SET [Status] = -1 where [Id] = @SalesTransactionId
			end
			select 'This VMA transaction already has been cancelled because of a timeout!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -303
		end
		if @SalesStatus <> 0 begin
			SELECT 'This VMA transaction already has been completed and can not be completed again!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -304
		end

		UPDATE [dbo].[SalesTransactions]
		   SET [Status] = 1,
		       [ModifyTime] = getutcdate()
		 WHERE [Id] = @VmaTransactionId

		SELECT @Points = [Points]
		  FROM [dbo].[VendingProducts] WHERE [Id] = @ProductId

		if (Select Count([Id]) from (
					select distinct act.[Id], act.[Title], act.[LimitStartTime], act.[LimitEndTime], pp.[StampTypeId1], pp.[StampTypeId2], pp.[StampTypeId3], act.[CouponExpiryDays]
							from [dbo].[CouponActivities] act
						inner join [dbo].[ActivityParticipatingProducts] pp
								on pp.[ActivityId] = act.[Id]
							   and pp.[Status] = 1
				left outer join [dbo].[ActivityParticipatingMachines] pm
								on pm.[ActivityId] = act.[Id]
							   and pm.[Status] = 1
					    inner join [dbo].[NewsArticles] news
						        on news.[ActivityId] = act.[Id]
						  where getutcdate() >= act.[LimitStartTime] and getutcdate() <= act.[LimitEndTime]
							and pp.[ProductId] = @ProductId
							and (act.[AllVendingMachines] = 1 or (pm.[VendingMachineId] = @VendingMachineId))) StampActivities) <= 0 begin

			set @StampsQRToken = null
		end
	end else begin
		-- we should use the BuyBuyTransactionNr (the user bought the product without the app)

		DECLARE @SeqNr bigint
		DECLARE @TransactionReceiptNr as varchar(64)

		   SET @VendingMachineId = 0
		SELECT @VendingMachineId = [Id]
		  FROM [dbo].[VendingMachines] WHERE [MOCCPOrganization] = @Organization AND [MOCCPAssetNo] = @AssetNo

		IF IsNull(@VendingMachineId, 0) = 0 BEGIN
			SELECT 'There is no vending machine record in the VMA database for the assetNo ' + @AssetNo + '!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		END
		IF IsNull(@MerchandiseId, 0) = 0 begin
			SELECT 'The argument "merchandiseID" is missing. This parameter is needed if the BuyBuyTransactionNr is used instead of the VmaTransactionId! (transaction without the use of the smartphone app)' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10007
		end

		   SET @ProductId = 0
		SELECT @ProductId = [Id], @Points = [Points]
		  FROM [dbo].[VendingProducts] WHERE [MOCCPGoodsId] = @MerchandiseId

		if IsNull(@ProductId, 0) = 0 begin
			SELECT 'There is no product with the merchandiseID ' + @MerchandiseId + ' in the VMA database!' As [Message], '' As [QRToken], null As [UID], null As [ProductId]
			return -10000
		end

		SELECT @SeqNr = NEXT VALUE FOR [dbo].[seq_SalesTransactionReceiptNr]
		SET @TransactionReceiptNr = format(datepart(ms, getdate()), '000') + Format(Cast((rand() * 1000) as int), '000') + Format(@SeqNr, '00000000')

		INSERT INTO [dbo].[SalesTransactions] ([ReceiptNumber],       [UID], [PurchaseType], [VendingMachineId], [ProductId], [Price], [Status], [QRToken],   [CreateTime], [ModifyTime])
									   Values (@TransactionReceiptNr, null,  2,              @VendingMachineId,  @ProductId,  0,       1,        'temptoken', getutcdate(), getutcdate())

		SET @SalesTransactionId = scope_identity()
		SET @StampsQRToken = [dbo].[fnGenerateQRToken](3, @SalesTransactionId)

		UPDATE [dbo].[SalesTransactions] SET [QRToken] = @StampsQRToken WHERE [Id] = @SalesTransactionId
	end

	-- add points to the user account
	SET @ReturnValue = 1
	SET @Message = ''

	if IsNull(@UID, 0) <> 0 begin -- if user bought without app we can not add points here (they will be added with the QR code later then)		
		begin try
			declare @Ret int
			exec @Ret = [dbo].[sp_AddUserPoints] @UID, @Points, @PointsExpiryDate, 2, null, @SalesTransactionId, null, null
			if @Ret <> 1 begin
				set @ReturnValue = -305 -- only as warning
				set @Message = 'Could not add ' + Cast(@Points as varchar(20)) + ' to the user account ' + Cast(@UID as varchar(20)) + ' for the SalesTransaction Id ' + Cast(@SalesTransactionId as varchar(20)) + '. sp_AddUserPoints returned ' + Cast(@Ret as varchar(20))
			end
		end try
		begin catch
			-- if adding points failed for some reason we will just return an error for logging it and anyway complete the transaciton
			set @ReturnValue = -305 -- only as warning
			set @Message = 'Could not add ' + Cast(@Points as varchar(20)) + ' to the user account ' + Cast(@UID as varchar(20)) + ' for the SalesTransaction Id ' + Cast(@SalesTransactionId as varchar(20)) + ' because sp_AddUserPoints yielded an exception! The error was: ' + error_message()
		end catch
	end

	SELECT @Message As [Message], @StampsQRToken AS [QRToken], @UID As [UID], @ProductId As [ProductId] -- StampsQRToken will be returned only if there are stamps to be collected or if the transaction is done without the app (for collecting points)
	RETURN @ReturnValue
END
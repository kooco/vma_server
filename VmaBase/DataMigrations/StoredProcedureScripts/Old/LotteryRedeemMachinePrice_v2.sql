﻿/*
 * sp_LotteryRedeemMachinePrice
 * ============================
 *
 * Redeems the given vending machine lottery price.
 * Sends as message to the given vending machine, that it should drop a free drink.
 *
 */
ALTER PROCEDURE [dbo].[sp_LotteryRedeemMachinePrice]
	@UID bigint,
	@PriceId bigint,
	@VendingMachineId bigint
AS
BEGIN
	declare @UseActivityCoupon bit
	declare @ActivityId bigint
	declare @CouponsExpiryTime datetime
	declare @ProductId bigint
	declare @AllVendingMachines bit
	declare @PriceCouponId bigint
	declare @LotteryGameId bigint
	declare @MachineAssetNo nvarchar(32)
	declare @CouponToken nvarchar(32)
	declare @MachineStatus int

	set @UseActivityCoupon = 0
	set @ProductId = 0
	set @ActivityId = 0

	select @UseActivityCoupon = [UseActivityCoupon], @ActivityId = [ConnectedActivityId], @CouponsExpiryTime = game.[EndTime], @ProductId = [ActivityCouponProductId], @LotteryGameId = [LotteryGameId]
	  from [dbo].[LotteryPrices] price
inner join [dbo].[LotteryGames] game on game.[Id] = price.[LotteryGameId]

	if IsNull(@UseActivityCoupon, 0) = 0 begin
		return -405 -- price not for vending machines
	end

	if getutcdate() >= @CouponsExpiryTime begin
		return -402 -- game already finished
	end

	if IsNull(@ProductId, 0) = 0 or IsNull(@ActivityId, 0) = 0 begin
		return -10900 -- internal server error, beause there is no product id defined or no connected activity has been defined
	end

	select @AllVendingMachines = [AllVendingMachines]
	  from [dbo].[CouponActivities]
	 where [Id] = @ActivityId

   if IsNull(@AllVendingMachines, 0) <> 1 begin
      if not exists(select 1 from [dbo].[ActivityParticipatingMachines] where [ActivityId] = @ActivityId and [VendingMachineId] = @VendingMachineId and [Status] = 1) begin
		  return -406 -- this machine is not part of the activity
	  end
   end

   -- get (and check) the first coupon the user has for this price
   set @PriceCouponId = 0

   select top 1 @PriceCouponId = [Id], @CouponToken = [PickupNumber]
     from [dbo].[LotteryPriceCoupons]
	where [LotteryGameId] = @LotteryGameId
	  and [LotteryPriceId] = @PriceId
	  and [UID] = @UID
	  and [Status] = 1 -- 1 won, but not yet "picked up"

   if IsNull(@PriceCouponId, 0) = 0 begin
     return -407
   end

   -- get the assetno for the machine
   set @MachineAssetNo = ''
   select @MachineAssetNo = [MOCCPAssetNo], @MachineStatus = [Status] from [dbo].[VendingMachines] where [Id] = @VendingMachineId
   if isnull(@MachineAssetNo, '') = '' begin
	  return -10000 -- record not found (vending machine not found)
   end
   if @MachineStatus <> 1 begin
	  return -310 -- vending machine not available
   end

   -- now create the redeem message
   insert into [dbo].[VendingMachineMessages] ([Direction], [Type], [AssetNo],		 [VendingMachineId], [ProductId], [LotteryPriceCouponId], [CouponToken], [Status], [CreateTime])
									   Values (0,           11,     @MachineAssetNo, @VendingMachineId,  @ProductId,  @PriceCouponId,         @CouponToken,  0,        getutcdate())

   return 1
END
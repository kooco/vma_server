﻿CREATE PROCEDURE [dbo].[sp_AddUserPoints]
	@UID bigint,
	@Amount int,
	@ExpiryDate datetime,
	@TransactionType int,
	@ReferenceUID bigint,
	@SalesTransactionId bigint,
	@ActivityId bigint
AS
BEGIN
	BEGIN TRY
		begin transaction

		if @TransactionType = 1 AND IsNull(@ActivityId, 0) = 0 begin
			rollback transaction
			return -10007
		end
		if @TransactionType IN (2, 11) AND IsNull(@SalesTransactionId, 0) = 0 begin
			rollback transaction
			return -10007
		end
		if @TransactionType IN (5, 15) AND IsNull(@ReferenceUID, 0) = 0 begin
			rollback transaction
			return -10007
		end

		INSERT INTO [dbo].[PointsTransactions] ([UID], [TransactionType], [Amount], [ReferenceUserId], [SalesTransactionId], [ActivityId], [CreateTime], [ModifyTime])
									    Values (@UID,  @TransactionType,  @Amount,  @ReferenceUID,     @SalesTransactionId,  @ActivityId,  getutcdate(), getutcdate())

		INSERT INTO [dbo].[UserPoints] ([UID], [Amount], [OriginalAmount], [Status], [DateAquired], [ExpiryDate])
		                        Values (@UID,  @Amount,  @Amount,          1,        getutcdate(),  @ExpiryDate)

	    UPDATE [dbo].[UserDatas] SET [PointsTotal] = (SELECT Sum([Amount])
														FROM [dbo].[UserPoints]
													   WHERE [UID] = @UID
													     AND [Status] = 1
														 AND [ExpiryDate] >= getutcdate())
											WHERE [UID] = @UID

		commit transaction

		return 1
	END TRY
	BEGIN CATCH
		rollback transaction;
		return 0;
	END CATCH
END

﻿ALTER PROCEDURE [dbo].[sp_StartOrderProduct]
	@UID bigint,
	@VendingMachineId bigint,
	@ProductId bigint,
	@SalesTransExpiryMinutes int,
	@VendingMachineMsgExpirySeconds int
AS
BEGIN
	DECLARE @MachineAssetNo varchar(32)
	DECLARE @SalesListId bigint
	DECLARE @ProductMerchId varchar(32)
	DECLARE @SalesTransactionId bigint
	DECLARE @TransactionReceiptNr varchar(64)
	DECLARE @TransactionQRToken varchar(32)
	DECLARE @SeqNr bigint
	DECLARE @SalesListStatus int
	DECLARE @SalesExpiryDate datetime
	DECLARE @MsgCreateTime datetime
	DECLARE @ExistingMsgId bigint
	DECLARE @HasStamps bit

	SET @SalesListId = 0
	SET @ProductMerchId = null
	SET @SalesExpiryDate = dateadd(minute, -(@SalesTransExpiryMinutes), getutcdate())

	-- update status of expired orders (if that causes performance problems maybe could be removed and just leave the check in CompleteMachineOrder)
	UPDATE [dbo].[SalesTransactions]
	   SET [Status] = -1
	 WHERE [Status] = 0
	   AND [CreateTime] < @SalesExpiryDate

	-- get sales list entry
	SELECT @ProductMerchId = [MOCCPMerchandiseId], @SalesListId = [Id], @SalesListStatus = [Status]
	  FROM [dbo].[SalesLists]
	 WHERE [VendingMachineId] = @VendingMachineId
	   AND [ProductId] = @ProductId

	IF IsNull(@SalesListId, 0) = 0 begin
		select 'There is no sales list entry with the given VendingMachineId/ProductId combination!' as [Message], Cast(0 as bit) as [HasStamps]
		return -10000 -- record not found
	end
	if @SalesListStatus <> 1 begin
		select 'this product is currently not available or sold out!' as [Message], Cast(0 as bit) as [HasStamps]
		return -301
	end

	-- get machine asset no
	SET @MachineAssetNo = null
	SELECT @MachineAssetNo = [MOCCPAssetNo] FROM [dbo].[VendingMachines] WHERE [Id] = @VendingMachineId

	IF IsNull(@MachineAssetNo, '') = '' begin
		select 'Could not find a vending machine with the id ' + Cast(@VendingMachineId as varchar(20)) as [Message], Cast(0 as bit) as [HasStamps]
		return -10000
	end

	-- is there still an open message for this vending machine?
	SET @MsgCreateTime = null
	SET @ExistingMsgId = null

	SELECT @ExistingMsgId = [Id], @MsgCreateTime = [CreateTime] FROM [dbo].[VendingMachineMessages] WHERE [Status] = 0 AND [Direction] = 0 AND [Type] = 10 AND [AssetNo] = @MachineAssetNo
	if IsNull(@ExistingMsgId, 0) <> 0 begin
		if @MsgCreateTime < dateadd(second, -(@VendingMachineMsgExpirySeconds), getutcdate()) begin
			-- last message already expired - set to "Timeout"
			UPDATE [dbo].[VendingMachineMessages] SET [Status] = -1 WHERE [Id] = @ExistingMsgId
		end else begin
			select 'There is already another started order not yet processed by this vending machine!' as [Messages], Cast(0 as bit) as [HasStamps]
			return -306
		end
	end

	-- is there still some open sales transaction for this vending machine?
	if exists(select 1 from [dbo].[SalesTransactions] where [Status] = 0 AND [VendingMachineId] = @VendingMachineId) begin
		select 'This vending machine still is processing another order which has net yet been completed!' as [Message], Cast(0 as bit) as [HasStamps]
		return -307
	end

	BEGIN TRY
		begin transaction

		-- create sales transaction
		SELECT @SeqNr = NEXT VALUE FOR [dbo].[seq_SalesTransactionReceiptNr]
		SET @TransactionReceiptNr = format(datepart(ms, getdate()), '000') + Format(Cast((rand() * 1000) as int), '000') + Format(@SeqNr, '00000000')

		INSERT INTO [dbo].[SalesTransactions] ([ReceiptNumber],       [UID], [VendingMachineId], [ProductId], [Price], [Status], [QRToken], [CreateTime], [ModifyTime])
									   Values (@TransactionReceiptNr, @UID,  @VendingMachineId,  @ProductId,  0,       0,        'init',    getutcdate(), getutcdate())

		SET @SalesTransactionId = scope_identity()
		SET @TransactionQRToken = [dbo].[fnGenerateQRToken](3, @SalesTransactionId)

		UPDATE [dbo].[SalesTransactions]
		   SET [QRToken] = @TransactionQRToken
		 WHERE [Id] = @SalesTransactionId

		-- now create "show product" message for the vending machine
		INSERT INTO [dbo].[VendingMachineMessages] ([Direction], [Type], [AssetNo],	      [VendingMachineId], [ProductId], [SalesTransactionId], [Status], [CreateTime])
										    Values (0,           10,     @MachineAssetNo, @VendingMachineId,  @ProductId,  @SalesTransactionId,  0,        getutcdate())

		commit transaction

		-- check if there are stamps available for this product/machine
		SET @HasStamps = 0
		if exists(select 1 from [dbo].[ActivityParticipatingProducts] pp
					 inner join [dbo].[CouponActivities] act
					         on act.[Id] = pp.[ActivityId]
				left outer join [dbo].[ActivityParticipatingMachines] pm
						     on pm.[ActivityId] = act.[Id]
					      where getutcdate() >= [LimitStartTime]
							and getutcdate() <= [LimitEndTime]
							and ([AllVendingMachines] = 1 Or pm.[Id] is not null))
		begin
			set @HasStamps = 1
		end

		select '' as [Message], @HasStamps as [HasStamps]
		return 1
	END TRY
	BEGIN CATCH
		rollback transaction;
		throw;
		return 0;
	END CATCH
END
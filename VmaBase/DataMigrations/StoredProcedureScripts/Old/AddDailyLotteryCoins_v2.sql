﻿/*
 * Fills up the user profiles with the daily lottery coins
 */
ALTER PROCEDURE [dbo].[sp_AddDailyLotteryCoins]
AS
BEGIN
	if exists(select 1 from [dbo].[LotteryGames] 
					  where [Status] in (1, 2, 9) 
						and ((getutcdate() >= [StartTime] and getutcdate() <= [EndTime]) or
						     ([TestMode] > 0 AND getutcdate() >= [TestStartTime] and getutcdate() <= [EndTime])))
	begin
		declare cUsers cursor for select [UID], [LotteryDailyCoins] from [dbo].[UserDatas] where [Status] = 1
		declare @UID bigint
		declare @LotteryDailyCoins int
		
		open cUsers
		fetch next from cUsers into @UID, @LotteryDailyCoins
		while @@fetch_status = 0
		begin
			if @LotteryDailyCoins < 3 begin
				insert into [dbo].[LotteryCoinLogs] ([UID], [Type], [TransactionTime], [Coins])
											 Values (@UID,  1,      getutcdate(),      3 - @LotteryDailyCoins)

				update [dbo].[UserDatas] set [LotteryDailyCoins] = 3 where [UID] = @UID
			end

			fetch next from cUsers into @UID, @LotteryDailyCoins
		end

		close cUsers
		deallocate cUsers
	end
END

﻿CREATE PROCEDURE [dbo].[sp_CheckExpiredPoints]
	@UID bigint
AS
BEGIN
	UPDATE [dbo].[UserPoints]
	   SET [Status] = -1
	 WHERE [UID] = @UID
	   AND [ExpiryDate] <= getutcdate()

	UPDATE [dbo].[UserDatas]
	   SET [PointsTotal] = IsNull((SELECT Sum([Amount]) FROM [dbo].[UserPoints] WHERE [UID] = @UID AND [Status] = 1), 0)
	 WHERE [UID] = @UID
END
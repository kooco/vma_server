﻿CREATE PROCEDURE [dbo].[sp_UpdateSalesList]
	@VendingMachineId bigint,
	@AvailabilityList varchar(max),
	@ReturnOnlyAvailable bit
AS
BEGIN
	DECLARE cProductsAvailability CURSOR FOR
		SELECT [Item] FROM [dbo].[fn_SplitString](@AvailabilityList, ';')
	DECLARE @Item varchar(40)
	DECLARE @MOCCPMerchId varchar(32)
	DECLARE @AvailAmountStr varchar(10)
	DECLARE @AvailAmount int
	DECLARE @SeparatorPos int
	DECLARE @ProductId bigint

	open cProductsAvailability
	fetch next from cProductsAvailability into @Item
	while @@FETCH_STATUS = 0
	begin
		if IsNull(@Item, '') <> '' begin
			SET @SeparatorPos = CHARINDEX(':', @Item)
			IF @SeparatorPos > 0 BEGIN
				SET @MOCCPMerchId = SUBSTRING(@Item, 1, @SeparatorPos - 1)
				SET @AvailAmountStr = SUBSTRING(@Item, @SeparatorPos + 1, 20)
				SET @AvailAmount = cast(@AvailAmountStr as int)

				IF EXISTS(SELECT 1 FROM [dbo].[SalesLIsts] WHERE [VendingMachineId] = @VendingMachineId AND [MOCCPMerchandiseId] = @MOCCPMerchId) BEGIN
					UPDATE [dbo].[SalesLists]
					   SET [Status] = case when (@AvailAmount > 0) then 1 else 0 end
					 WHERE [VendingMachineId] = @VendingMachineId AND [MOCCPMerchandiseId] = @MOCCPMerchId
				END ELSE BEGIN
					SELECT @ProductId = [Id] FROM [dbo].[VendingProducts] WHERE [MOCCPGoodsId] = @MOCCPMerchId
					INSERT INTO [dbo].[SalesLists] ([VendingMachineId], [ProductId], [MOCCPMerchandiseId], [RetrievablePoints], [RetrievableStamps], [Status])
										    Values (@VendingMachineId,  @ProductId,  @MOCCPMerchId,        0,                   0,                   case when (@AvailAmount > 0) then 1 else 0 end)
				END
			END
		end

		fetch next from cProductsAvailability into @Item
	end

	close cProductsAvailability
	deallocate cProductsAvailability

	update [dbo].[SalesLists]
	   SET [Status] = -1
	 WHERE [VendingMachineId] = @VendingMachineId
	   AND [MOCCPMerchandiseId] NOT IN (SELECT (case when IsNull([Item], '') = '' then '' else SUBSTRING([Item], 1, CHARINDEX(':', [Item]) - 1) end) FROM [dbo].[fn_SplitString](@AvailabilityList, ';'))

	-- now select this sales list
	SELECT sales.[Id] As [Id],
		   sales.[VendingMachineId] As [VendingMachineId],
		   prod.[Id] As [ProductId],
		   prod.[MOCCPGoodsId] As [UniqueNumber],
		   prod.[Name] As [ProductName],
		   prod.[ImageUrl] As [ProductImageUrl],
		   sales.[Status] As [Status]
	  FROM [dbo].[SalesLists] sales
INNER JOIN [dbo].[VendingProducts] prod ON prod.[Id] = sales.[ProductId]
	 WHERE [VendingMachineId] = @VendingMachineId
	   AND (sales.[Status] = 1 OR (sales.[Status] = 0 AND @ReturnOnlyAvailable = 0))
END
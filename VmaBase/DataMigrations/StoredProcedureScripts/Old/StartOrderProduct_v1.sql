﻿CREATE PROCEDURE [dbo].[sp_StartOrderProduct]
	@UID bigint,
	@VendingMachineId bigint,
	@ProductId bigint
AS
BEGIN
	DECLARE @MachineAssetNo varchar(32)
	DECLARE @SalesListId bigint
	DECLARE @ProductMerchId varchar(32)
	DECLARE @SalesTransactionId bigint
	DECLARE @TransactionReceiptNr varchar(64)
	DECLARE @TransactionQRToken varchar(32)
	DECLARE @SeqNr bigint

	SET @SalesListId = 0
	SET @ProductMerchId = null

	-- get sales list entry
	SELECT @ProductMerchId = [MOCCPMerchandiseId], @SalesListId = [Id]
	  FROM [dbo].[SalesLists]
	 WHERE [VendingMachineId] = @VendingMachineId
	   AND [ProductId] = @ProductId

	IF IsNull(@SalesListId, 0) = 0 begin
		select 'There is no sales list entry with the given VendingMachineId/ProductId combination!' as [Message], Cast(0 as bit) as [HasStamps]
		return -10000 -- record not found
	end

	-- get machine asset no
	SET @MachineAssetNo = null
	SELECT @MachineAssetNo = [MOCCPAssetNo] FROM [dbo].[VendingMachines] WHERE [Id] = @VendingMachineId

	IF IsNull(@MachineAssetNo, '') = '' begin
		select 'Could not find a vending machine with the id ' + Cast(@VendingMachineId as varchar(20)) as [Message], Cast(0 as bit) as [HasStamps]
		return -10000
	end

	BEGIN TRY
		begin transaction

		-- create sales transaction
		SELECT @SeqNr = NEXT VALUE FOR [dbo].[seq_SalesTransactionReceiptNr]
		SET @TransactionReceiptNr = format(datepart(ms, getdate()), '000') + Format(Cast((rand() * 1000) as int), '000') + Format(@SeqNr, '00000000')

		INSERT INTO [dbo].[SalesTransactions] ([ReceiptNumber],       [UID], [VendingMachineId], [ProductId], [Price], [Status], [QRToken], [CreateTime], [ModifyTime])
									   Values (@TransactionReceiptNr, @UID,  @VendingMachineId,  @ProductId,  0,       0,        'init',    getutcdate(), getutcdate())

		SET @SalesTransactionId = scope_identity()
		SET @TransactionQRToken = [dbo].[fnGenerateQRToken](3, @SalesTransactionId)

		UPDATE [dbo].[SalesTransactions]
		   SET [QRToken] = @TransactionQRToken
		 WHERE [Id] = @SalesTransactionId

		-- now create "show product" message for the vending machine
		INSERT INTO [dbo].[VendingMachineMessages] ([Direction], [Type], [AssetNo],	      [VendingMachineId], [ProductId], [SalesTransactionId], [Status], [CreateTime])
										    Values (0,           10,     @MachineAssetNo, @VendingMachineId,  @ProductId,  @SalesTransactionId,  0,        getutcdate())

		commit transaction

		select '' as [Message], Cast(0 as bit) as [HasStamps]
		return 1
	END TRY
	BEGIN CATCH
		rollback transaction;
		throw;
		return 0;
	END CATCH
END
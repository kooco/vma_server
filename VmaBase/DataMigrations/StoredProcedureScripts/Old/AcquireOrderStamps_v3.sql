﻿ALTER PROCEDURE [dbo].[sp_AcquireOrderStamps]
	@UID bigint,
	@QRToken varchar(32),
	@PointsExpiryDate datetime,
	@ChosenActivityId bigint = null
AS
BEGIN
	DECLARE @SalesTransactionId bigint
	DECLARE @VendingMachineId bigint
	DECLARE @ProductId bigint
	DECLARE @TransactionUID bigint
	DECLARE @Status int
	DECLARE @ActivityId bigint
	DECLARE @StampType1 bigint
	DECLARE @StampType2 bigint
	DECLARE @StampType3 bigint
	DECLARE @UserCouponId bigint
	DECLARE @CouponExpiryDays int
	DECLARE @PurchaseType int
	DECLARE @PointsAdded int

	SET @PurchaseType = 0
	SET @SalesTransactionId = null
	SET @VendingMachineId = null
	SET @ProductId = null
	SET @StampType1 = null
	SET @StampType2 = null
	SET @StampType3 = null

	SELECT @SalesTransactionId = [Id], @VendingMachineId = [VendingMachineId], @ProductId = [ProductId], @TransactionUID = [UID], @PurchaseType = [PurchaseType]
	  FROM [dbo].[SalesTransactions]
	 WHERE [QRToken] = @QRToken AND [Status] = 1

	if IsNull(@SalesTransactionId, 0) = 0 begin
		select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], 'QR code is invalid! This token does not exist or already has been used!' as [Message]
		return -308
	end
	if IsNull(@TransactionUID, 0) <> 0 AND @TransactionUID <> @UID begin
		select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], 'The UID given does not match the UID who started this order!' as [Message]
		return -10008
	end

	DECLARE @Activities TABLE ([ActivityId] bigint, [Title] nvarchar(64), [LimitStartTime] datetime, [LimitEndTime] datetime, [StampType1] bigint, [StampType2] bigint, [StampType3] bigint, [CouponExpiryDays] int)

	insert into @Activities ([ActivityId], [Title], [LimitStartTime], [LimitEndTime], [StampType1], [StampType2], [StampType3], [CouponExpiryDays])
				select distinct act.[Id], act.[Title], act.[LimitStartTime], act.[LimitEndTime], pp.[StampTypeId1], pp.[StampTypeId2], pp.[StampTypeId3], act.[CouponExpiryDays]
						from [dbo].[CouponActivities] act
					inner join [dbo].[ActivityParticipatingProducts] pp
					        on pp.[ActivityId] = act.[Id]
						   and pp.[Status] = 1
			left outer join [dbo].[ActivityParticipatingMachines] pm
				            on pm.[ActivityId] = act.[Id]
						   and pm.[Status] = 1
				      where getutcdate() >= act.[LimitStartTime] and getutcdate() <= act.[LimitEndTime]
						and pp.[ProductId] = @ProductId
						and (act.[AllVendingMachines] = 1 or (pm.[VendingMachineId] = @VendingMachineId))
						and (act.[Id] = @ChosenActivityId or IsNull(@ChosenActivityId, 0) = 0) -- if activity chosen filter the chosen activity
						and (act.[NoStampsActivity] = 0)

	-- make sure to select only one activity
	SET @ActivityId = @ChosenActivityId
	if IsNull(@ActivityId, 0) = 0 begin
		if (select count([ActivityId]) from @Activities) > 1 begin
			-- there is more than one activity to choose from -> return them with a pending state
				SELECT act.[ActivityId], act.[Title], news.[ImageUrl], act.[LimitStartTime], act.[LimitEndTime], Cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], '' as [Message]
				  FROM @Activities act
			INNER JOIN [dbo].[NewsArticles] news on news.[ActivityId] = act.[ActivityId]

			RETURN 3
		end else begin
			if (select Count([ActivityId]) from @Activities) = 0 begin
				select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], 'This QR code does not match the activity given or there is no stamp available for this machine/product combination!' as [Message]
				RETURN -309
			end else begin
				select @ActivityId = [ActivityId], @StampType1 = [StampType1], @StampType2 = [StampType2], @StampType3 = [StampType3], @CouponExpiryDays = [CouponExpiryDays] from @Activities
			end
		end
	end else begin
		-- already activity chosen - just check if the activity matches the qr code
		if (select count([ActivityId]) from @Activities) <= 0 begin
			select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], 'This QR code does not match the activity given or there is no stamp available for this machine/product combination!' as [Message]
			RETURN -309
		end

		SELECT @StampType1 = [StampType1], @StampType2 = [StampType2], @StampType3 = [StampType3], @CouponExpiryDays = [CouponExpiryDays] FROM @Activities
	end

	-- now add the stamps for that activity
	declare @RetCode int
	if IsNull(@StampType1, 0) <> 0 begin
		exec @RetCode = [dbo].[sp_AddUserStamp] @UID, @ActivityId, @StampType1, @ProductId, @SalesTransactionId, @CouponExpiryDays, @UserCouponId output
		if @RetCode <> 1 begin
			select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], null as [Message]
			return @RetCode
		end
	end
	if IsNull(@StampType2, 0) <> 0 begin
		exec @RetCode = [dbo].[sp_AddUserStamp] @UID, @ActivityId, @StampType2, @ProductId, @SalesTransactionId, @CouponExpiryDays, @UserCouponId output
		if @RetCode <> 1 begin
			select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], null as [Message]
			return @RetCode
		end
	end
	if IsNull(@StampType3, 0) <> 0 begin
		exec @RetCode = [dbo].[sp_AddUserStamp] @UID, @ActivityId, @StampType3, @ProductId, @SalesTransactionId, @CouponExpiryDays, @UserCouponId output
		if @RetCode <> 1 begin
			select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], cast(null as bigint) as [UserCouponId], cast(0 as int) as [PointsAdded], null as [Message]
			return @RetCode
		end
	end	

	-- add points for transactions done without the app
	SET @PointsAdded = 0
	if @PurchaseType = 2 begin
		declare @Points int
		select @Points = [Points] FROM [dbo].[VendingProducts] WHERE [Id] = @ProductId

		exec [dbo].[sp_AddUserPoints] @UID, @Points, @PointsExpiryDate, 3, null, @SalesTransactionId, @ActivityId, null
		SET @PointsAdded = @Points
	end

	update [dbo].[SalesTransactions]
	   set [Status] = 2
	 where [Id] = @SalesTransactionId

	select Cast(null as bigint) As [ActivityId], null As [Title], null As [ImageUrl], cast(null as datetime) As [LimitStartTime], cast(null as datetime) as [LimitEndTime], @UserCouponId As [UserCouponId], @PointsAdded as [PointsAdded], '' as [Message]
	RETURN 1
END

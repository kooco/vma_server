﻿/*
 * sp_LotteryRedeemMachinePrice
 * ============================
 *
 * Redeems the given (vending machine) lottery price.
 * Sends as message to the given vending machine, that it should drop a free drink.
 *
 */
ALTER PROCEDURE [dbo].[sp_LotteryRedeemMachinePrice]
	@UID bigint,
	@PriceId bigint,
	@VendingMachineId bigint,
	@SelectedProductId bigint,
	@ActivityIdFilter bigint,
	@PickupToken nvarchar(12)
AS
BEGIN
	declare @UseActivityCoupon bit
	declare @ActivityId bigint
	declare @CouponsExpiryTime datetime
	declare @ProductId bigint
	declare @AllVendingMachines bit
	declare @PriceCouponId bigint
	declare @LotteryGameId bigint
	declare @MachineAssetNo nvarchar(32)
	declare @CouponToken nvarchar(32)
	declare @MachineStatus int
	declare @PickupGameToken nvarchar(12)
	declare @PickupTokenMode int
	declare @SalesListId bigint
	declare @SalesListStatus int
	declare @HasProductAssignment bit
	declare @AutoChooseFirstAvailProduct bit

	set @UseActivityCoupon = 0
	set @ProductId = 0
	set @ActivityId = 0
	set @AutoChooseFirstAvailProduct = 0

	if IsNull(@PriceId, 0) = 0 AND IsNull(@ActivityIdFilter, 0) = 0 begin
		return -10007 -- parameter is missing
	end

	select @UseActivityCoupon = [UseActivityCoupon], @ActivityId = [ConnectedActivityId], @CouponsExpiryTime = game.[EndTime], @ProductId = [ActivityCouponProductId], @AutoChooseFirstAvailProduct = [AutoChooseFirstAvailProduct], @LotteryGameId = [LotteryGameId], @PickupTokenMode = [PickupTokenMode], @PickupGameToken = [PickupToken], @PriceId = IsNull(case when @PriceId = 0 then null else @PriceId end, price.[Id])
	  from [dbo].[LotteryPrices] price
inner join [dbo].[LotteryGames] game on game.[Id] = price.[LotteryGameId]
     where ((price.[Id] = @PriceId and not IsNull(@PriceId, 0) = 0) or
	       ((IsNull(@PriceId, 0) = 0) and [UseActivityCoupon] = 1)) and
		   (IsNull(@ActivityIdFilter, 0) = 0 or [ConnectedActivityId] = @ActivityIdFilter)

	-- check if any product has been assigned to the price
	if IsNull(@ProductId, 0) = 0 begin
		if exists(select 1 from [dbo].[LotteryPriceRedeemProducts] where [LotteryPriceId] = @PriceId and [Status] = 1) begin
			set @HasProductAssignment = 1

			-- if a product selection has been given -> check if this product selection is valid
			if IsNull(@SelectedProductId, 0) <> 0 begin
				if not exists(select 1 from [dbo].[LotteryPriceRedeemProducts] where [LotteryPriceId] = @PriceId and [ProductId] = @SelectedProductId and [Status] = 1) begin
					return -317 -- product can not be redeemed with this prize
				end
				set @ProductId = @SelectedProductId
			end 
		end else begin
			set @HasProductAssignment = 0
		end
	end else begin
		set @HasProductAssignment = 1

		if IsNull(@SelectedProductId, 0) <> 0 begin
			if @SelectedProductId <> @ProductId begin
				return -317 -- product can not be redeemed with this prize
			end
		end
	end

	if @HasProductAssignment = 0 and @UseActivityCoupon = 1 begin
		-- if no product is defined for that price and also no product selection has been given, then return an error
		if IsNull(@SelectedProductId, 0) = 0 begin
			return -412 -- product selection missing
		end

		set @ProductId = @SelectedProductId
		if not exists(select [Id] from [dbo].[VendingProducts] WHERE [Id] = @ProductId AND [Status] = 1) begin
			return -301 -- product not available
		end
	end

	-- if there is any product assignment and no product has been chosen
	-- return an error, or automatically choose a matching product
	if @HasProductAssignment = 1 and @UseActivityCoupon = 1 and IsNull(@ProductId, 0) = 0 begin
		if @AutoChooseFirstAvailProduct = 1 begin
			select top 1 @ProductId = sl.[ProductId]
			  from [dbo].[SalesLists] sl
		inner join [dbo].[LotteryPriceRedeemProducts] prp on prp.[ProductId] = sl.[ProductId]
			 where sl.[VendingMachineId] = @VendingMachineId
			   and prp.[LotteryPriceId] = @PriceId
			   and prp.[Status] = 1
			   and sl.[Status] = 1
		
			if IsNull(@ProductId, 0) = 0 begin
				return -316 -- no matching product found
			end
		end else begin
			return -412 -- product selection missing
		end
	end

	-- check product availability at this machine
	if IsNull(@ProductId, 0) <> 0 and @UseActivityCoupon = 1 begin
		SELECT @SalesListId = [Id], @SalesListStatus = [Status]
		  FROM [dbo].[SalesLists]
		 WHERE [VendingMachineId] = @VendingMachineId
		   AND [ProductId] = @ProductId

		IF IsNull(@SalesListId, 0) = 0 begin
			select 'There is no sales list entry at the given VendingMachineId for the product to be redeemed!' as [Message], Cast(0 as bit) as [HasStamps]
			return -10000 -- record not found
		end
		if @SalesListStatus <> 1 begin
			select 'this product is currently not available or sold out!' as [Message], Cast(0 as bit) as [HasStamps]
			return -301
		end
	end

	if getutcdate() >= @CouponsExpiryTime begin
		return -402 -- game already finished
	end

	if @UseActivityCoupon = 1 and (IsNull(@ProductId, 0) = 0 or IsNull(@ActivityId, 0) = 0) begin
		return -10900 -- internal server error, beause there is no product id defined or no connected activity has been defined
	end

	if @UseActivityCoupon = 1 begin
		select @AllVendingMachines = [AllVendingMachines]
		  from [dbo].[CouponActivities]
		 where [Id] = @ActivityId

	   if IsNull(@AllVendingMachines, 0) <> 1 begin
		  if not exists(select 1 from [dbo].[ActivityParticipatingMachines] where [ActivityId] = @ActivityId and [VendingMachineId] = @VendingMachineId and [Status] = 1) begin
			  return -406 -- this machine is not part of the activity
		  end
	   end
   end

   -- get (and check) the first coupon the user has for this price
   set @PriceCouponId = 0

   select top 1 @PriceCouponId = [Id], @CouponToken = [PickupNumber]
     from [dbo].[LotteryPriceCoupons]
	where [LotteryGameId] = @LotteryGameId
	  and [LotteryPriceId] = @PriceId
	  and [UID] = @UID
	  and [Status] = 1 -- 1 won, but not yet "picked up"

   if IsNull(@PriceCouponId, 0) = 0 begin
     return -407
   end

   -- check the pickup token
   if @PickupTokenMode > 0 and @UseActivityCoupon = 0 begin
		if @PickupTokenMode = 1 begin -- TokenMode 1 = GameToken
			if IsNull(@PickupToken, '') = '' begin
				return -410 -- the parameter "token" is required
			end

			if @PickupToken <> @PickupGameToken begin
				return -411 -- the pickup token is invalid
			end
		end

		if @PickupTokenMode = 2 begin -- TokenMode 2 = price coupon token (every price coupon has it's own token)
			if IsNull(@PickupToken, '') <> '' begin -- giving the pickup token in this mode is optional
				if @PickupToken <> @CouponToken begin
					return -411 -- the pickup token is invalid
				end
			end
		end
   end

   if @UseActivityCoupon = 1 begin
	   -- get the assetno for the machine
	   set @MachineAssetNo = ''
	   select @MachineAssetNo = [MOCCPAssetNo], @MachineStatus = [Status] from [dbo].[VendingMachines] where [Id] = @VendingMachineId
	   if isnull(@MachineAssetNo, '') = '' begin
		  return -10000 -- record not found (vending machine not found)
	   end
	   if @MachineStatus <> 1 begin
		  return -310 -- vending machine not available
	   end

	   -- now create the redeem message
	   insert into [dbo].[VendingMachineMessages] ([Direction], [Type], [AssetNo],		 [VendingMachineId], [ProductId], [LotteryPriceCouponId], [CouponToken], [Status], [CreateTime])
										   Values (0,           11,     @MachineAssetNo, @VendingMachineId,  @ProductId,  @PriceCouponId,         @CouponToken,  0,        getutcdate())

	   return 1
	end else begin
		-- not a vending machine product -> just mark the price as "picked up"
		update [dbo].[LotteryPriceCoupons] 
		   SET [TimeRedeemed] = getutcdate(), [Status] = 2 -- 2 = Picked up
		 WHERE [Id] = @PriceCouponId	

		return 1
	end
END
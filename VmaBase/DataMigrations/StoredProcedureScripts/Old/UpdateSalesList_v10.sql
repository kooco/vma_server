﻿/*
 * sp_UpdateSalesList
 * ------------------
 * Updates the sales list for the given vending machine
 * AND returns the list of products for it
 *
 * If ActivityIdFilter is given, then the returned list is filtered by products
 * which are part of this activity. The UPDATE will still be done for ALL products of the sales list!
 */
ALTER PROCEDURE [dbo].[sp_UpdateSalesList]
	@VendingMachineId bigint,
	@ActivityIdFilter bigint,
	@AvailabilityList varchar(max),
	@UpdateAvailList bit,
	@ReturnOnlyAvailable bit,
	@SalesTransExpiryMinutes int,
	@VendingMachineMsgExpirySeconds int,
	@UID bigint
AS
BEGIN
	DECLARE cProductsAvailability CURSOR FOR
		SELECT [Item] FROM [dbo].[fn_SplitString](@AvailabilityList, ';')
	DECLARE @Item varchar(40)
	DECLARE @MOCCPMerchId varchar(32)
	DECLARE @MOCCPInternalId int
	DECLARE @AvailAmountStr varchar(10)
	DECLARE @AvailAmount int
	DECLARE @SeparatorPos int
	DECLARE @ProductId bigint
	DECLARE @MsgCreateTime datetime
	DECLARE @ExistingMsgId bigint
	DECLARE @AssetNo varchar(32)
	DECLARE @SalesExpiryDate datetime
	DECLARE @PriceStr varchar(15)
	DECLARE @Price float
	DECLARE @PointsExtra int
	DECLARE @PointsMultiplier int

	SET @SalesExpiryDate = dateadd(minute, -(@SalesTransExpiryMinutes), getutcdate())

	if IsNull(@UpdateAvailList, 1) = 1 begin
		open cProductsAvailability
		fetch next from cProductsAvailability into @Item
		while @@FETCH_STATUS = 0
		begin
			if IsNull(@Item, '') <> '' begin
				SET @SeparatorPos = CHARINDEX(':', @Item)
				IF @SeparatorPos > 0 BEGIN
					SET @MOCCPMerchId = SUBSTRING(@Item, 1, @SeparatorPos - 1)
					SET @AvailAmountStr = SUBSTRING(@Item, @SeparatorPos + 1, 20)
					SET @SeparatorPos = CHARINDEX('|', @AvailAmountStr)
					IF @SeparatorPos > 0 BEGIN
						SET @PriceStr = SUBSTRING(@AvailAmountStr, @SeparatorPos + 1, 20)
						SET @AvailAmountStr = SUBSTRING(@AvailAmountStr, 1, @SeparatorPos - 1)

						SET @AvailAmount = cast(@AvailAmountStr as int)
						SET @Price = Cast(@PriceStr as float)

						SELECT @ProductId = [Id], @MOCCPInternalId = [MOCCPInternalId], @PointsExtra = [PointsExtra], @PointsMultiplier = [PointsMultiplier] FROM [dbo].[VendingProducts] WHERE [MOCCPGoodsId] = @MOCCPMerchId

						IF EXISTS(SELECT 1 FROM [dbo].[SalesLIsts] WHERE [VendingMachineId] = @VendingMachineId AND [MOCCPMerchandiseId] = @MOCCPMerchId) BEGIN
							UPDATE [dbo].[SalesLists]
							   SET [Status] = case when (@AvailAmount > 0) then 1 else 0 end, 
								   [Price] = @Price,
								   [RetrievablePoints] = (Cast(Round(@Price, 0) as int) * @PointsMultiplier) + @PointsExtra
							 WHERE [VendingMachineId] = @VendingMachineId AND [MOCCPMerchandiseId] = @MOCCPMerchId
						END ELSE BEGIN						
							INSERT INTO [dbo].[SalesLists] ([VendingMachineId], [ProductId], [Price], [MOCCPMerchandiseId], [MOCCPInternalId], [RetrievablePoints], [RetrievableStamps], [Status])
													Values (@VendingMachineId,  @ProductId,  @Price,  @MOCCPMerchId,        @MOCCPInternalId,  0,                   0,                   case when (@AvailAmount > 0) then 1 else 0 end)
						END
					END
				END
			end

			fetch next from cProductsAvailability into @Item
		end

		close cProductsAvailability
		deallocate cProductsAvailability

		update [dbo].[SalesLists]
		   SET [Status] = -1
		 WHERE [VendingMachineId] = @VendingMachineId
		   AND [MOCCPMerchandiseId] NOT IN (SELECT (case when IsNull([Item], '') = '' then '' else SUBSTRING([Item], 1, CHARINDEX(':', [Item]) - 1) end) FROM [dbo].[fn_SplitString](@AvailabilityList, ';'))
	end

	-- now select this sales list
			SELECT sales.[Id] As [Id],
				   sales.[VendingMachineId] As [VendingMachineId],
				   prod.[Id] As [ProductId],
				   prod.[MOCCPGoodsId] As [UniqueNumber],
				   prod.[MOCCPInternalId] As [InternalId],
				   prod.[Name] As [ProductName],
				   prod.[ImageUrl] As [ProductImageUrl],
				   sales.[Status] As [Status],
				   sales.[Price] As [Price],
				   sales.[RetrievablePoints] As [RetrievablePoints]
			  FROM [dbo].[SalesLists] sales
		INNER JOIN [dbo].[VendingProducts] prod ON prod.[Id] = sales.[ProductId]
   LEFT OUTER JOIN [dbo].[ActivityParticipatingProducts] pp ON pp.[ActivityId] = @ActivityIdFilter AND pp.[ProductId] = prod.[Id]
   LEFT OUTER JOIN [dbo].[ActivityParticipatingMachines] pm on pm.[ActivityId] = @ActivityIdFilter AND pm.[VendingMachineId] = sales.[VendingMachineId]
			 WHERE sales.[VendingMachineId] = @VendingMachineId
			   AND (sales.[Status] = 1 OR (sales.[Status] = 0 AND @ReturnOnlyAvailable = 0))
			   AND (prod.[Status] = 1)
			   AND ((IsNull(@ActivityIdFilter, 0) = 0) Or (IsNull(pp.[Id], 0) <> 0))
			   AND ((IsNull(@ActivityIdFilter, 0) = 0) Or (IsNull(pm.[Id], 0) <> 0))

	-- is there still an open message for this vending machine?
	SET @MsgCreateTime = null
	SET @ExistingMsgId = null

	SELECT @AssetNo = [MOCCPAssetNo] FROM [dbo].[VendingMachines] WHERE [Id] = @VendingMachineId
	SELECT @ExistingMsgId = [Id], @MsgCreateTime = [CreateTime] FROM [dbo].[VendingMachineMessages] WHERE [Status] = 0 AND [Direction] = 0 AND [Type] = 10 AND [AssetNo] = @AssetNo
	if IsNull(@ExistingMsgId, 0) <> 0 begin
		if @MsgCreateTime < dateadd(second, -(@VendingMachineMsgExpirySeconds), getutcdate()) begin
			-- last message already expired - set to "Timeout"
			UPDATE [dbo].[VendingMachineMessages] SET [Status] = -1 WHERE [Id] = @ExistingMsgId
		end else begin
			return -306
		end
	end

	-- check if there is another sales transaction currently processed and not yet completed by this machine
	if exists(select 1 from [dbo].[SalesTransactions] where [Status] = 0 AND [VendingMachineId] = @VendingMachineId AND [CreateTime] > @SalesExpiryDate AND [UID] <> @UID) begin
		return -307 -- we will just return this code but anyway return the sales list, so that the app can know about the machine status (if it's the same user, then this code will not be returned)
	end

	RETURN 1
END
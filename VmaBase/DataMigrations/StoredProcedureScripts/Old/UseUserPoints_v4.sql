﻿ALTER PROCEDURE [dbo].[sp_UseUserPoints]
	@UID bigint,
	@Amount int,
	@TransactionType int,
	@ReferenceUID bigint,
	@SalesTransactionId bigint,
	@ActivityId bigint,
	@PromoOrderId bigint,
	@PointsList varchar(max) output -- will contain a list of all point entries used with id, amount of points and expiry date in this format: Id,Amount,YYYYMMDDHHMM;Id,Amount,YYYYMMDDHHMM;...
AS
BEGIN
	begin transaction

	print 'before save trans UsePointsTran'

	-- we have to make a "savepoint" for the case of nested transaction, because there are no real nested transactions in SQL server..
	-- https://social.msdn.microsoft.com/Forums/en-US/3eac605e-0e0f-41ab-9bff-f032e349d2fb/error-message-on-mismatch-in-transaction-count-when-nesting-stored-procedures?forum=transactsql
	-- what did microsoft smoke, when implementing transactions?...
	save transaction UsePointsTran 

	print 'after save trans UsePointsTran'

	declare @DeductAmount int
	set @PointsList = ''

	if @Amount < 0 BEGIN
		SET @DeductAmount = -(@Amount)
	END ELSE BEGIN
		SET @DeductAmount = @Amount
	END

	BEGIN TRY
		print 'deduct amount: ' + cast(@DeductAmount as varchar(20))

		if @TransactionType = 1 AND IsNull(@ActivityId, 0) = 0 begin			
			rollback transaction UsePointsTran
			commit transaction
			return -10007
		end
		if @TransactionType IN (2, 11) AND IsNull(@SalesTransactionId, 0) = 0 begin
			rollback transaction UsePointsTran
			commit transaction
			return -10007
		end
		if @TransactionType IN (5, 15) AND IsNull(@ReferenceUID, 0) = 0 begin
			rollback transaction UsePointsTran
			commit transaction
			return -10007
		end
		if @TransactionType IN (10, 12) AND IsNull(@PromoOrderId, 0) = 0 begin
			rollback transaction UsePointsTran
			commit transaction
			return -10007
		end

		print 'CheckExpiredPoints called ' + Cast(@UID as varchar(20))
		exec [dbo].[sp_CheckExpiredPoints] @UID
		print 'after CheckExpiredPoints'
		if ((Select [PointsTotal] FROM [dbo].[UserDatas] WHERE [UID] = @UID) < @DeductAmount) BEGIN
			rollback transaction UsePointsTran
			commit transaction
			return -300
		END

		print 'before insert log'

		INSERT INTO [dbo].[PointsTransactions] ([UID], [TransactionType],  [Amount],		[ReferenceUserId], [SalesTransactionId], [ActivityId], [PromotionOrderId], [CreateTime], [ModifyTime])
									    Values (@UID,  @TransactionType,  -(@DeductAmount),  @ReferenceUID,     @SalesTransactionId,  @ActivityId, @PromoOrderId,      getutcdate(), getutcdate())

		DECLARE cPoints CURSOR FOR
			SELECT [Id], [Amount], [ExpiryDate]
			  FROM [dbo].[UserPoints]
			 WHERE [UID] = @UID
			   AND [Status] = 1
		  ORDER BY [ExpiryDate] asc

		DECLARE @PointsId bigint
		DECLARE @AvailableAmount int
		DECLARE @ExpiryDate datetime

		open cPoints
		fetch next from cPoints into @PointsId, @AvailableAmount, @ExpiryDate

		while ((@@FETCH_STATUS = 0) AND (@DeductAmount > 0))
		begin
			print @PointsId
			if @AvailableAmount > @DeductAmount begin
				-- this points entry is higher than the amount we need
				UPDATE [dbo].[UserPoints]
				   SET [Amount] = [Amount] - @DeductAmount
				 WHERE [Id] = @PointsId

				 SET @PointsList = @PointsList + ';' + Cast(@PointsId as varchar(20)) + ',' + Cast(@DeductAmount as varchar(20)) + ',' + Cast(year(@ExpiryDate) as varchar(4)) + Format(month(@ExpiryDate), '00') + Format(day(@ExpiryDate), '00') + Format(datepart(hour, @ExpiryDate), '00') + Format(datepart(minute, @ExpiryDate), '00')
				 SET @DeductAmount = 0				   
			end else begin
				-- this points entry is less than the amount we need
				UPDATE [dbo].[UserPoints]
				   SET [Amount] = 0,
				       [Status] = 0
				 WHERE [Id] = @PointsId

				 SET @PointsList = @PointsList + ';' + Cast(@PointsId as varchar(20)) + ',' + Cast(@AvailableAmount as varchar(20)) + ',' + Cast(year(@ExpiryDate) as varchar(4)) + Format(month(@ExpiryDate), '00') + Format(day(@ExpiryDate), '00') + Format(datepart(hour, @ExpiryDate), '00') + Format(datepart(minute, @ExpiryDate), '00')
				 SET @DeductAmount = @DeductAmount - @AvailableAmount
			end

			fetch next from cPoints into @PointsId, @AvailableAmount, @ExpiryDate
		end

		close cPoints
		deallocate cPoints

		if IsNull(@PointsList, '') <> '' begin
			SET @PointsList = substring(@PointsList, 2, 10000) -- remove the first ;
		end

		UPDATE [dbo].[UserDatas]
		   SET [PointsTotal] = (Select Sum([Amount]) FROM [dbo].[UserPoints] WHERE [UID] = @UID AND [Status] = 1)
		 WHERE [UID] = @UID

		commit transaction

		return 1
	END TRY
	BEGIN CATCH
		rollback transaction UsePointsTran;
		commit transaction
		throw;
	END CATCH
END
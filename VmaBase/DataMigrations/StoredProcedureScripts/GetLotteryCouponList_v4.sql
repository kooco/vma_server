﻿ALTER PROCEDURE [dbo].[sp_GetLotteryCouponList]
	@LotteryGameId bigint,
	@UID bigint,
	@OnlyUsable bit = 0
AS
BEGIN
			select coupon.[LotteryPriceId] [PriceId],
				   price.[Title],
				   price.[ImageUrl],
				   price.[Unit],
				   coupon.[PickupNumber],
				   price.[PickupAddress],
				   price.[PickupPhone],
				   Cast((case when coupon.[Status] = 1 then 1 else 0 end) as bit) [IsUsable],
				   price.[UseActivityCoupon] [IsVendingMachinePrice]
			  from [dbo].[LotteryPriceCoupons] coupon
		inner join [dbo].[LotteryPrices] price on price.[Id] = [coupon].[LotteryPriceId]
			 where coupon.[LotteryGameId] = @LotteryGameId
			   and coupon.[UID] = @UID
			   and coupon.[Status] in (1, 2)
			   and (IsNull(@OnlyUsable, 0) = 0 or coupon.[Status] = 1)
			   and price.[Status] not in (9, -1)
	      order by coupon.[Status], coupon.[CreateTime]
END

﻿ALTER PROCEDURE [dbo].[sp_AddUserStamp]
	@UID bigint,
	@ActivityId bigint,
	@StampTypeId bigint,
	@ProductId bigint,
	@SalesTransactionId bigint,
	@CouponExpiryDays int,
	@UserCouponId bigint output
AS
BEGIN
	DECLARE @CountNeeded int
	DECLARE @CountCollected int
	DECLARE @StampId bigint
	DECLARE @CouponExpiryTime datetime
	DECLARE @NoStampsActivity bit

	SET @UserCouponId = null
	SELECT @UserCouponId = [Id]
	  FROM [dbo].[UserCoupons]
	 WHERE [ActivityId] = @ActivityId
	   AND [UID] = @UID
	   AND [Status] = 1

	SELECT @CouponExpiryTime = [LimitEndTime], @NoStampsActivity = [NoStampsActivity]
	  FROM [dbo].[CouponActivities]
	 WHERE [Id] = @ActivityId

	if IsNull(@NoStampsActivity, 0) = 1 begin
		return -214 -- stamps disabled
	end

	if IsNull(@UserCouponId, 0) = 0 begin
		insert into [dbo].[UserCoupons] ([ActivityId], [UID], [ExpiryTime],      [StartTime], [Status])
								 Values (@ActivityId,  @UID,  @CouponExpiryTime, getutcdate(), 1)
		SET @UserCouponId = scope_identity()
		--return -211
	end

	-- count needed and already collected stamps of this type
	SELECT @CountNeeded = Count([Id])
	  FROM [dbo].[CouponStampDefinitions]
	 WHERE [ActivityId] = @ActivityId
	   AND [StampTypeId] = @StampTypeId

	IF @CountNeeded = 0 BEGIN
		-- count already collected stamps for the "any" stamp type
		SELECT @CountNeeded = Count([Id])
		  FROM [dbo].[CouponStampDefinitions]
		 WHERE [ActivityId] = @ActivityId

		SELECT @CountCollected = Count([Id])
		  FROM [dbo].[UserCouponStamps]
		 WHERE [UserCouponId] = @UserCouponId
		   AND [Status] IN (1, 3) -- self collected (1) or received by friend (3)
	END ELSE BEGIN
		-- count already collected stamps for the given stamp type
		SELECT @CountCollected = Count([Id])
		  FROM [dbo].[UserCouponStamps]
		 WHERE [UserCouponId] = @UserCouponId
		   AND [StampTypeId] = @StampTypeId
		   AND [Status] IN (1, 3) -- self collected (1) or received by friend (3)

		if @CountCollected >= @CountNeeded begin
			-- already collected all stamps of that type - now check if for the "any" type still stamps are needed
			SELECT @CountNeeded = Count([Id])
			  FROM [dbo].[CouponStampDefinitions]
			 WHERE [ActivityId] = @ActivityId
			   AND [StampTypeId] is null

			SELECT @CountCollected = Count([Id])
			  FROM [dbo].[UserCouponStamps]
			 WHERE [UserCouponId] = @UserCouponId
			   AND [StampTypeId] not in (select [StampTypeId] from [dbo].[CouponStampDefinitions] where [ActivityId] = @ActivityId and [StampTypeId] is not null)
			   AND [Status] IN (1, 3) -- self collected (1) or received by friend (3)
		end
	END

	-- add the stamp to the coupon
	if @CountNeeded > @CountCollected begin
		insert into [dbo].[UserCouponStamps] ([UserCouponId], [StampTypeId], [DateCollected], [Status], [ReceivedByUserId])
									  Values (@UserCouponId,  @StampTypeId,  getutcdate(),    1,        null)
		SET @StampId = scope_identity();

		insert into [dbo].[UserStampLogs] ([UID], [UserStampId], [StampTypeId], [LogType], [UserReferenceId], [SalesTransactionId], [LogText], [LogTime])
								   Values (@UID,  @StampId,      @StampTypeId,  1,         null,              @SalesTransactionId,  '買產品ID ' + Cast(@ProductId as varchar(20)) + ' 收到 StampType ' + Cast(@StampTypeId as varchar(20)) + ' SalesTransaction ' + Cast(@SalesTransactionId as varchar(20)), getutcdate()) 

	    -- is the coupon already completed?
		if (select Count([Id]) from [dbo].[UserCouponStamps] where [UserCouponId] = @UserCouponId and [Status] in (1, 3)) >=
		   (select Count([Id]) from [dbo].[CouponStampDefinitions] where [ActivityId] = @ActivityId)
		begin
			-- set status to "completed"
			update [dbo].[UserCoupons] set [Status] = 2, [CompletionTime] = getutcdate(), [CompletedBy] = @UID where [Id] = @UsercouponId

			-- create new coupon
			insert into [dbo].[UserCoupons] ([ActivityId], [UID], [ExpiryTime], [StartTime], [Status])
								     Values (@ActivityId,  @UID,  dateadd(day, @CouponExpiryDays, getutcdate()), getutcdate(), 1)
		end
	end

	return 1
END

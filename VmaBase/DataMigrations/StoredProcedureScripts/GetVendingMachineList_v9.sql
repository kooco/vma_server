﻿ALTER PROCEDURE [dbo].[sp_GetVendingMachineList]
	@Latitude float = null,
	@Longitude float = null,
	@RadiusMeters bigint = 1000,
	@OnlyActive bit = 1,
	@ActivityId bigint = null,
	@CriteriaId bigint = null,
	@OnlyRecommended bit = 0,
	@OnlyAppActivated bit = 1,
	@IsTestUser bit = 0,
	@PageNr int,
	@PageSize int
AS
BEGIN
	declare @GeoLocation geography
	set @GeoLocation = null

	if (IsNull(@Latitude, 0) <> 0) AND (IsNull(@Longitude, 0) <> 0) begin
		set @GeoLocation = geography::Point(@Latitude, @Longitude, 4326)
	end

	if (@GeoLocation is null) begin
		SELECT vm.[Id],
		       [MOCCPAssetNo],
			   vm.[Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   Cast([GeoLocation].Lat As decimal(15, 8)) [Latitude],
			   Cast([GeoLocation].Long As decimal(15, 8)) [Longitude],
			   Cast(0 as bigint) [DistanceMeters],
			   vm.[Status],
			   [IsRecommended],
			   (case when [IsRecommended] = 1 then (select top 1 act.[Id] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else null end) [ActivityId],
			   (case when [IsRecommended] = 1 then (select top 1 act.[Title] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else N'' end) [ActivityTitle]
		  FROM dbo.[VendingMachines] vm
LEFT OUTER JOIN dbo.[ActivityParticipatingMachines] pm on pm.[VendingMachineId] = vm.[Id] and pm.[ActivityId] = IsNull(@ActivityId, 0) and pm.[Status] = 1
LEFT OUTER JOIN dbo.[AutoCouponCriteriaMachines] cm on cm.[MachineId] = vm.[Id] and cm.[CriteriaId] = IsNull(@CriteriaId, 0)
LEFT OUTER JOIN dbo.[AutoCouponCriterias] crit on crit.[Id] = IsNull(@CriteriaId, 0)
		 WHERE (vm.[Status] = 1 Or @OnlyActive = 0)
		   AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
		   AND (pm.[ActivityId] = @ActivityId or IsNull(@ActivityId, 0) = 0)
		   AND (IsNull(crit.[CouponsForAllMachines], 0) = 1 or IsNull(@CriteriaId, 0) = 0 or cm.[CriteriaId] = @CriteriaId)
		   AND (vm.[AppStatus] = 1 Or (IsNull(@OnlyAppActivated, 1) = 0 and [AppStatus] = 0) Or (IsNull(@IsTestUser, 0) = 1 And [AppStatus] = 2))
	  ORDER BY [Name]
	  OFFSET ((@PageNr - 1) * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY -- SQL Server 2012 syntax
	 end else begin
		SELECT [Id],
			   [MOCCPAssetNo],
			   [Name],
			   [City],
			   [Location],
			   [QRToken],
			   [ImageUrl],
			   [Latitude],
			   [Longitude],
			   [DistanceMeters],
			   [Status],
			   [IsRecommended],
			   [ActivityId],
			   [ActivityTitle]
		  FROM (
			SELECT vm.[Id],
				   [MOCCPAssetNo],
				   vm.[Name],
				   [City],
				   [Location],
				   [QRToken],
				   [ImageUrl],
				   Cast([GeoLocation].Lat as decimal(15, 8)) [Latitude],
				   Cast([GeoLocation].Long as decimal(15, 8)) [Longitude],
				   Cast([GeoLocation].STDistance(@GeoLocation) As bigint) [DistanceMeters],
				   vm.[Status],
				   [IsRecommended],
			       (case when [IsRecommended] = 1 then (select top 1 act.[Id] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else null end) [ActivityId],
			       (case when [IsRecommended] = 1 then (select top 1 act.[Title] from [dbo].[ActivityParticipatingMachines] pm inner join [dbo].[CouponActivities] act on act.[Id] = pm.[ActivityId] where pm.[VendingMachineId] = vm.[Id] order by act.[CreateTime] desc) else N'' end) [ActivityTitle]
			  FROM dbo.[VendingMachines] vm
   LEFT OUTER JOIN dbo.[ActivityParticipatingMachines] pm on pm.[VendingMachineId] = vm.[Id] and pm.[ActivityId] = IsNull(@ActivityId, 0) and pm.[Status] = 1
   LEFT OUTER JOIN dbo.[AutoCouponCriteriaMachines] cm on cm.[MachineId] = vm.[Id] and cm.[CriteriaId] = IsNull(@CriteriaId, 0)
   LEFT OUTER JOIN dbo.[AutoCouponCriterias] crit on crit.[Id] = IsNull(@CriteriaId, 0)
			 WHERE (vm.[Status] = 1 Or @OnlyActive = 0)
			   AND ([IsRecommended] = 1 Or IsNull(@OnlyRecommended, 0) = 0)
			   AND @GeoLocation.STDistance([GeoLocation]) <= @RadiusMeters
			   AND (pm.[ActivityId] = @ActivityId or IsNull(@ActivityId, 0) = 0)
			   AND (IsNull(crit.[CouponsForAllMachines], 0) = 1 or IsNull(@CriteriaId, 0) = 0 or cm.[CriteriaId] = @CriteriaId)
			   AND (vm.[AppStatus] = 1 Or (IsNull(@OnlyAppActivated, 1) = 0 and [AppStatus] = 0) Or (IsNull(@IsTestUser, 0) = 1 And [AppStatus] = 2))
			) VendingMachinesWithDistance
		ORDER BY [DistanceMeters]
		OFFSET ((@PageNr - 1) * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY -- SQL Server 2012 syntax
	 end
END
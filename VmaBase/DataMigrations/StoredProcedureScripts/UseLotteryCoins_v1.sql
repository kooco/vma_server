﻿CREATE PROCEDURE [dbo].[sp_UseLotteryCoins]
    @LotteryGameId bigint,
	@UID bigint,
	@Amount int
AS
BEGIN
	BEGIN TRY
		begin transaction 

		-- we have to make a "savepoint" for the case of nested transaction, because there are no real nested transactions in SQL server..
		-- https://social.msdn.microsoft.com/Forums/en-US/3eac605e-0e0f-41ab-9bff-f032e349d2fb/error-message-on-mismatch-in-transaction-count-when-nesting-stored-procedures?forum=transactsql
		-- what did microsoft smoke, when implementing transactions?...
		save transaction UseLotteryCoinsTran

		declare @AvailableCoins int
		declare @CoinsId bigint
		declare @CurrentCoinAmount int
		declare @NeededCoins int
		declare @RestAmount int
		declare @NewCoinStatus int

		set @NeededCoins = @Amount
		set @AvailableCoins = (select Sum([Amount]) FROM [dbo].[UserLotteryCoins] 
												   WHERE [LotteryGameId] = IsNull(@LotteryGameId, -1) 
												     AND [UID] = IsNull(@UID, -1) 
													 AND [ExpiryDate] >= getutcdate()
													 AND [Status] = 1)

		if @AvailableCoins < @Amount begin
			rollback transaction UseLotteryCoinsTran;
			commit transaction;
			return -404
		end

		declare cCoins cursor for
			select [Id], [Amount]
			  from [dbo].[UserLotteryCoins]
			 where [LotteryGameId] = IsNull(@LotteryGameId, -1)
			   and [UID] = IsNull(@UID, -1)
			   and [ExpiryDate] >= getutcdate()
			   and [Amount] > 0
			   and [Status] = 1
		  order by [Type] desc, [ExpiryDate]
		open cCoins

		fetch next from cCoins into @CoinsId, @CurrentCoinAmount
		while ((@@fetch_status = 0) and (@NeededCoins > 0))
		begin
			set @RestAmount = 0
			set @NewCoinStatus = 0
			
			if @NeededCoins > @CurrentCoinAmount begin
				set @NeededCoins = @NeededCoins - @CurrentCoinAmount
				set @RestAmount = 0
				set @NewCoinStatus = 0 -- 0 = Used up
			end else begin
				set @RestAmount = @CurrentCoinAmount - @NeededCoins
				set @NewCoinStatus = (case when @NeededCoins = @CurrentCoinAmount then 0 else 1 end) -- 1 = still available, 0 = Used up
				set @NeededCoins = 0
			end

			update [dbo].[UserLotteryCoins] SET [Amount] = @RestAmount, [Status] = @NewCoinStatus WHERE [Id] = @CoinsId

			fetch next from cCoins into @CoinsId, @CurrentCoinAmount
		end

		close cCoins
		deallocate cCoins

		commit transaction

		return 1
	END TRY
	BEGIN CATCH
		rollback transaction UseLotteryCoinsTran;
		commit transaction;
		throw;
		return -1003
	END CATCH
END

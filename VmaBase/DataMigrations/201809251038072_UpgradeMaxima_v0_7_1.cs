namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class UpgradeMaxima_v0_7_1 : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JiraConnectedIssueStatus",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(maxLength: 64),
                        IconUrl = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        IsInProgress = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        IsCompleted = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        IsClosed = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JiraConnectedIssueTypes",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(maxLength: 64),
                        IconUrl = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.JiraConnectedIssues", "IssueTypeId", c => c.Long(nullable: false));
            AddColumn("dbo.JiraConnectedIssues", "IssueStatusId", c => c.Long(nullable: false));
            CreateIndex("dbo.JiraConnectedIssues", "IssueTypeId");
            CreateIndex("dbo.JiraConnectedIssues", "IssueStatusId");
            AddForeignKey("dbo.JiraConnectedIssues", "IssueStatusId", "dbo.JiraConnectedIssueStatus", "Id");
            AddForeignKey("dbo.JiraConnectedIssues", "IssueTypeId", "dbo.JiraConnectedIssueTypes", "Id");
            DropColumn("dbo.JiraConnectedIssues", "IssueType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "1" },
                });
            DropColumn("dbo.JiraConnectedIssues", "IssueStatus",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_7_1(this);
        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertAdditionalDbChangesV0_7_1(this);

            AddColumn("dbo.JiraConnectedIssues", "IssueStatus", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
            AddColumn("dbo.JiraConnectedIssues", "IssueType", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "1")
                    },
                }));
            DropForeignKey("dbo.JiraConnectedIssues", "IssueTypeId", "dbo.JiraConnectedIssueTypes");
            DropForeignKey("dbo.JiraConnectedIssues", "IssueStatusId", "dbo.JiraConnectedIssueStatus");
            DropIndex("dbo.JiraConnectedIssues", new[] { "IssueStatusId" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "IssueTypeId" });
            DropColumn("dbo.JiraConnectedIssues", "IssueStatusId");
            DropColumn("dbo.JiraConnectedIssues", "IssueTypeId");
            DropTable("dbo.JiraConnectedIssueTypes",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "UpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedIssueStatus",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "IsClosed",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "IsCompleted",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "IsInProgress",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "UpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
        }
    }
}

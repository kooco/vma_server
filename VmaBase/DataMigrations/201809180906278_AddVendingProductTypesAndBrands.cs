﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddVendingProductTypesAndBrands : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VendingProductBrands",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "UIX_Name");
            
            CreateTable(
                "dbo.VendingProductTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "UIX_Name");

            Sql(@"
                SET IDENTITY_INSERT [dbo].[VendingProductTypes] ON
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (1, N'汽水類', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (2, N'甜茶類', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (3, N'無糖茶', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (4, N'水', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (5, N'果汁', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (6, N'運動飲品', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (7, N'咖啡', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (8, N'荳奶', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (9, N'其他(飲品)', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (10, N'朱古力', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (11, N'糖果', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (12, N'杯面', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (13, N'餅干', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (14, N'用品', 1)
                INSERT INTO [dbo].[VendingProductTypes]([Id], [Name], [Status]) Values (15, N'其他(零食機)', 1)
                SET IDENTITY_INSERT [dbo].[VendingProductTypes] OFF
            ");

            Sql(@"
                SET IDENTITY_INSERT [dbo].[VendingProductBrands] ON
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (1, N'可口可樂', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (2, N'雪碧', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (3, N'芬達', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (4, N'玉泉', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (5, N'陽光', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (6, N'飛想', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (7, N'淳茶舍', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (8, N'飛雪', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (9, N'美粒果', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (10, N'美粒果酷兒', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (11, N'水動樂', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (12, N'健康工房', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (13, N'喬亞咖啡', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (14, N'維他命水', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (15, N'魔爪', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (16, N'雀巢茶品', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (17, N'雀巢咖啡', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (18, N'雀巢', 1)
                INSERT INTO [dbo].[VendingProductBrands]([Id], [Name], [Status]) Values (19, N'其他', 1)
                SET IDENTITY_INSERT [dbo].[VendingProductBrands] OFF
            ");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM [dbo].[VendingProductBrands]");
            Sql("DELETE FROM [dbo].[VendingProductTypes]");

            DropIndex("dbo.VendingProductTypes", "UIX_Name");
            DropIndex("dbo.VendingProductBrands", "UIX_Name");
            DropTable("dbo.VendingProductTypes",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.VendingProductBrands",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

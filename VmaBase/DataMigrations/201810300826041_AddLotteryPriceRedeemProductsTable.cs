namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddLotteryPriceRedeemProductsTable : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LotteryPriceRedeemProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LotteryGameId = c.Long(nullable: false),
                        LotteryPriceId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LotteryGames", t => t.LotteryGameId)
                .ForeignKey("dbo.LotteryPrices", t => t.LotteryPriceId)
                .ForeignKey("dbo.VendingProducts", t => t.ProductId)
                .Index(t => t.LotteryGameId)
                .Index(t => new { t.LotteryPriceId, t.ProductId }, unique: true, name: "UIX_LotteryPrice_Product");
            
            AddColumn("dbo.LotteryPrices", "AutoChooseFirstAvailProduct", c => c.Boolean(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "0")
                    },
                }));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LotteryPriceRedeemProducts", "ProductId", "dbo.VendingProducts");
            DropForeignKey("dbo.LotteryPriceRedeemProducts", "LotteryPriceId", "dbo.LotteryPrices");
            DropForeignKey("dbo.LotteryPriceRedeemProducts", "LotteryGameId", "dbo.LotteryGames");
            DropIndex("dbo.LotteryPriceRedeemProducts", "UIX_LotteryPrice_Product");
            DropIndex("dbo.LotteryPriceRedeemProducts", new[] { "LotteryGameId" });
            DropColumn("dbo.LotteryPrices", "AutoChooseFirstAvailProduct",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "0" },
                });
            DropTable("dbo.LotteryPriceRedeemProducts",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
        }
    }
}

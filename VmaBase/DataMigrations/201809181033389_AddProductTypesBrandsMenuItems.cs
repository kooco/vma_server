﻿namespace VmaBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddProductTypesBrandsMenuItems : FrameworkDbMigration
    {
        public override void Up()
        {
            AddApplicationAction("PRDTYP", "VendingProducts", "ProductTypes", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "產品類型");
            AddApplicationAction("PRDBRN", "VendingProducts", "ProductBrands", Kooco.Framework.Models.Enum.ActionPermissionLevels.Admin, Kooco.Framework.Models.Enum.GeneralStatusEnum.Active, FriendlyName: "品牌");

            AddMenuItem(42002, ParentId: 42000, ActionCode: "PRDTYP", Name: "產品類型", Sort: 310, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
            AddMenuItem(42003, ParentId: 42000, ActionCode: "PRDBRN", Name: "品牌", Sort: 310, Status: Kooco.Framework.Models.Enum.GeneralStatusEnum.Active);
        }

        public override void Down()
        {
            RemoveMenuItem(42003);
            RemoveMenuItem(42002);

            RemoveApplicationAction("PRDBRN");
            RemoveApplicationAction("PRDTYP");
        }
    }
}

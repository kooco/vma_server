﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.DataMigrations;

using VmaBase.Shared;

namespace VmaBase.DataMigrations
{
    /// <summary>
    ///     NewsModuleMigrationHelper class
    ///     -------------------------------
    ///     
    ///     Class for generating all the neccessary stored procedures / functions / table records
    ///     for the Maxima.NewsModuleBase NuGet package.
    ///     
    ///     Auto-Generated by Maxima.NewsModuleBase.
    ///     Please do NOT modify!
    ///     Every change will be overwritten with the next update!
    /// </summary>
    public class NewsModuleMigrationHelper
    {
        #region ApplyAdditionalNewsModuleDbChanges_Initial
        public static void ApplyAdditionalNewsModuleDbChanges_Initial(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.FunctionScripts.fnGetNewsListBase_v1.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v1.sql"));

            Migration.AddApplicationAction("NEWSBAN", "News", "Index");
            Migration.AddApplicationAction("NEWSMNG", "News", "NewsManagement");

            Migration.AddMenuItem(Id: 40000, ActionCode: "NEWSBAN", Name: "新聞管理", Sort: 40000);
            Migration.AddMenuItem(Id: 40001, ParentId: 40000, ActionCode: "NEWSBAN", Name: "新聞輪播", Sort: 40001);
            Migration.AddMenuItem(Id: 40002, ParentId: 40000, ActionCode: "NEWSMNG", Name: "新聞", Sort: 40001);

            Migration.Sql("SET IDENTITY_INSERT [dbo].[NewsCategories] ON " +
                          "INSERT INTO [dbo].[NewsCategories] ([Id], [Code], [Name]) Values (1, 'GENERAL', '一般') " +
                          "SET IDENTITY_INSERT [dbo].[NewsCategories] OFF");
        }
        #endregion
		#region ApplyAdditionalNewsModuleDbChanges_0_2_90
		public static void ApplyAdditionalNewsModuleDbChanges_0_2_90(FrameworkDbMigration Migration)
		{
			Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v2.sql"));
			Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v1.sql"));
			Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsMessages_v1.sql"));
		}
		#endregion
        #region ApplyAdditionalNewsModuleDbChanges_0_3_1
        public static void ApplyAdditionalNewsModuleDbChanges_0_3_1(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v3.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v2.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateNewsStatus_v1.sql"));
        }
        #endregion
        #region ApplyAdditionalNewsModuleDbChanges_0_3_28
        public static void ApplyAdditionalNewsModuleDbChanges_0_3_28(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v4.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v3.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateNewsStatus_v2.sql"));
        }
        #endregion
        #region RevertAdditionalNewsModuleDbChanges_Initial
        public static void RevertAdditionalNewsModuleDbChanges_Initial(FrameworkDbMigration Migration)
        {
            Migration.Sql("DELETE FROM [dbo].[NewsCategories] WHERE [Code] = 'GENERAL'");

            Migration.RemoveMenuItem(40002);
            Migration.RemoveMenuItem(40001);
            Migration.RemoveMenuItem(40000);

            Migration.RemoveApplicationAction("NEWSMNG");
            Migration.RemoveApplicationAction("NEWSBAN");

            Migration.Sql("DROP FUNCTION [dbo].[fnGetNewsListBase]");
            Migration.Sql("DROP PROCEDURE [dbo].[sp_GetNewsList]");
        }
        #endregion
		#region RevertAdditionalNewsModuleDbChanges_0_2_90
		public static void RevertAdditionalNewsModuleDbChanges_0_2_90(FrameworkDbMigration Migration)
		{
			Migration.Sql("DROP PROCEDURE [dbo].[sp_GetNewsList]");
			Migration.Sql("DROP PROCEDURE [dbo].[sp_GetNewsListCount]");
			Migration.Sql("DROP PROCEDURE [dbo].[sp_GetNewsMessages]");

			Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v1.sql"));
		}
		#endregion
        #region RevertAdditionalNewsModuleDbChanges_0_3_1
        public static void RevertAdditionalNewsModuleDbChanges_0_3_1(FrameworkDbMigration Migration)
        {
            Migration.Sql("DROP PROCEDURE [dbo].[sp_UpdateNewsStatus]");
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v2.sql"));
            Migration.Sql("DROP PROCEDURE [dbo].[sp_GetNewsListCount]");
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v1.sql"));
        }
        #endregion
        #region RevertAdditionalNewsModuleDbChanges_0_3_28
        public static void RevertAdditionalNewsModuleDbChanges_0_3_28(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v3.sql"));            
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v2.sql"));
			Migration.Sql("DROP PROCEDURE [dbo].[sp_UpdateNewsStatus]");
			Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.UpdateNewsStatus_v1.sql"));
        }
        #endregion
        #region ApplyAdditionalNewsModuleDbChanges_0_5_56
        public static void ApplyAdditionalNewsModuleDbChanges_0_5_56(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v5.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v4.sql"));
        }
        #endregion
        #region RevertAdditionalNewsModuleDbChanges_0_5_56
        public static void RevertAdditionalNewsModuleDbChanges_0_5_56(FrameworkDbMigration Migration)
        {
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsList_v4.sql"));
            Migration.Sql(Utils.ReadResourceContents("VmaBase.DataMigrations.StoredProcedureScripts.GetNewsListCount_v3.sql"));
        }
        #endregion
    }
}

namespace VmaBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddTypeAndBrandToVendingProducts : FrameworkDbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingProducts", "ProductTypeId", c => c.Long(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "9")
                    },
                }));
            AddColumn("dbo.VendingProducts", "ProductBrandId", c => c.Long(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "19")
                    },
                }));
            CreateIndex("dbo.VendingProducts", "ProductTypeId");
            CreateIndex("dbo.VendingProducts", "ProductBrandId");
            AddForeignKey("dbo.VendingProducts", "ProductBrandId", "dbo.VendingProductBrands", "Id");
            AddForeignKey("dbo.VendingProducts", "ProductTypeId", "dbo.VendingProductTypes", "Id");
            DropColumn("dbo.VendingProducts", "Type",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "2" },
                });
        }
        
        public override void Down()
        {
            AddColumn("dbo.VendingProducts", "Type", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "2")
                    },
                }));
            DropForeignKey("dbo.VendingProducts", "ProductTypeId", "dbo.VendingProductTypes");
            DropForeignKey("dbo.VendingProducts", "ProductBrandId", "dbo.VendingProductBrands");
            DropIndex("dbo.VendingProducts", new[] { "ProductBrandId" });
            DropIndex("dbo.VendingProducts", new[] { "ProductTypeId" });
            DropColumn("dbo.VendingProducts", "ProductBrandId",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "19" },
                });
            DropColumn("dbo.VendingProducts", "ProductTypeId",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "9" },
                });
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using VmaAdmin.Shared;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(VmaAdmin.Maxima.AppStart), "Start")]
namespace VmaAdmin.Maxima
{
    /// <summary>
    ///     Maxima Package project configuration.
    ///     
    ///     AUTOMATICALLY GENERATED FILE
    ///     PLEASE DO NOT CHANGE!
    /// </summary>    
    public static class AppStart
    {
        public static void Start()
        {			
			System.Web.Hosting.HostingEnvironment.RegisterVirtualPathProvider(new MaximaWeb.Shared.MaximaVirtualPathProvider());
		}
    }
}
﻿When using the MaximaWeb package, please make sure to add the following changes to your _Layout page!!!

*) Add the following code to the end (best quite before </body>) of your _Layout page:
	@Html.Partial("_MaximaBase")

   Please make sure that this code is as defined AFTER @RenderBody()!

*) If you want to use the jQuery $(document).ready() function PLEASE USE THE KOOCOHTMLHELPER method!
	@KoocoHtmlHelper.DocumentReady(@<text>
		your javascript code here
	</text>)

	Because the Maxima Framework is already using the document ready function you can NOT define it yourself again.
	Instead you have to add javascript code to the existing document ready function with the KoocoHtmlHelper.DocumentReady() function!
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;

using VmaBase.Providers;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminInput;

namespace VmaAdmin.Controllers
{
    public class NewsController : KoocoBaseController
    {
        // GET: News
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewsManagement()
        {
            return View();
        }

        #region AjaxGetCategories
        [PrincipalPermission(PermissionType = PermissionTypes.AllUsers)]
        public ActionResult AjaxGetCategories()
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetCategories(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetCategories");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError));
            }
        }
        #endregion

        #region AjaxGetNewsList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetNewsList(SearchNewsParamsDTO Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                NP.UpdateNewsStatus();
                Params.OnlyActive = false;
                return Json(NP.GetNews(Params, true), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetnewsList");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxGetNewsDetails
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetNewsDetails(long NewsId)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetNewsDetails(NewsId, null), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetNewsDetails");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxAddNews

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxAddNews(NewsDetailsDTO NewsArticle)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.AddNews(NewsArticle));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxModifyNews");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxModifyNews

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxModifyNews(NewsDetailsDTO NewsArticle)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.ModifyNews(NewsArticle));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxModifyNews");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxChangeNewsStatus
        public class AjaxChangeNewsStatusParams
        {
            public long NewsId { get; set; }
            public bool Online { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxChangeNewsStatus(AjaxChangeNewsStatusParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.ChangeNewsStatus(Params.NewsId, Params.Online));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxChangeNewsStatus");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxDeleteNews
        public class AjaxDeleteNewsParams
        {
            public long NewsId { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxDeleteNews(AjaxDeleteNewsParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.DeleteNews(Params.NewsId));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxDeleteNews");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region News Image Upload
        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxUploadNewsImage(long newsId)
        {
            try
            {
                return Json(Kooco.Framework.Shared.UploadManager.HandleFileUpload("NewsImage", newsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxUploadNewsImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxUploadNewsListImage(long newsId)
        {
            try
            {
                return Json(Kooco.Framework.Shared.UploadManager.HandleFileUpload("NewsListImage", newsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxUploadNewsListImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxUploadActivityListImage(long newsId)
        {
            try
            {
                return Json(Kooco.Framework.Shared.UploadManager.HandleFileUpload("ActivityListImage", newsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxUploadActivityListImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        public class AjaxSaveNewsImageParams
        {
            public long NewsId { get; set; }
            public bool NewArticle { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxSaveNewsImage(AjaxSaveNewsImageParams Params)
        {
            try
            {
                bool bImageSaved = false;
                string ContentType = string.Empty;
                string FileName = string.Empty;

                BaseResponseDTO SaveImageResponse = null;
                SaveImageAdminResponseDTO ResponseDTO = new SaveImageAdminResponseDTO();

                // save news detail image
                byte[] filedata = Kooco.Framework.Shared.UploadManager.GetUploadedFileData("NewsImage", (Params.NewArticle ? 0 : Params.NewsId), ref ContentType, ref FileName);
                if (filedata != null)
                {
                    NewsProvider NP = new NewsProvider();
                    SaveImageResponse = NP.SaveNewsImage(Params.NewsId, ContentType, Path.GetExtension(FileName), filedata);
                    Kooco.Framework.Shared.UploadManager.ClearUploadedFileData("NewsImage", (Params.NewArticle ? 0 : Params.NewsId));
                    bImageSaved = true;

                    if (SaveImageResponse.ReturnCode == ReturnCodes.Success)
                        ResponseDTO.ImageUrl = (string)SaveImageResponse.Data;
                    else
                        return Json(SaveImageResponse, JsonRequestBehavior.AllowGet);

                    filedata = null;
                }

                // save news list image
                filedata = Kooco.Framework.Shared.UploadManager.GetUploadedFileData("NewsListImage", (Params.NewArticle ? 0 : Params.NewsId), ref ContentType, ref FileName);
                if (filedata != null)
                {
                    NewsProvider NP = new NewsProvider();
                    SaveImageResponse = NP.SaveNewsListImage(Params.NewsId, ContentType, Path.GetExtension(FileName), filedata);
                    Kooco.Framework.Shared.UploadManager.ClearUploadedFileData("NewsListImage", (Params.NewArticle ? 0 : Params.NewsId));
                    bImageSaved = true;

                    if (SaveImageResponse.ReturnCode == ReturnCodes.Success)
                        ResponseDTO.ListImageUrl = (string)SaveImageResponse.Data;
                    else
                        return Json(SaveImageResponse, JsonRequestBehavior.AllowGet);
                }

                // save activity list image
                filedata = Kooco.Framework.Shared.UploadManager.GetUploadedFileData("ActivityListImage", (Params.NewArticle ? 0 : Params.NewsId), ref ContentType, ref FileName);
                if (filedata != null)
                {
                    NewsProvider NP = new NewsProvider();
                    SaveImageResponse = NP.SaveActivityListImage(Params.NewsId, ContentType, Path.GetExtension(FileName), filedata);
                    Kooco.Framework.Shared.UploadManager.ClearUploadedFileData("ActivityListImage", (Params.NewArticle ? 0 : Params.NewsId));
                    bImageSaved = true;

                    if (SaveImageResponse.ReturnCode == ReturnCodes.Success)
                        ResponseDTO.ActivityListImageUrl = (string)SaveImageResponse.Data;
                    else
                        return Json(SaveImageResponse, JsonRequestBehavior.AllowGet);
                }

                if (!bImageSaved)
                    return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound, "The image uploaded already expired or is not stored on the server anymore. Please upload the image again!"), JsonRequestBehavior.AllowGet);

                return Json(new BaseResponseDTO(ResponseDTO), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxSaveNewsImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetUploadedNewsImage(long newsId)
        {
            try
            {
                return Kooco.Framework.Shared.UploadManager.GetUploadedFileMVC("NewsImage", newsId);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetUploadedNewsImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetUploadedNewsListImage(long newsId)
        {
            try
            {
                return Kooco.Framework.Shared.UploadManager.GetUploadedFileMVC("NewsListImage", newsId);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetUploadedNewsListImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetUploadedActivityListImage(long newsId)
        {
            try
            {
                return Kooco.Framework.Shared.UploadManager.GetUploadedFileMVC("ActivityListImage", newsId);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetUploadedActivityListImage");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        // Activity Management

        #region AjaxGetNewsActivity
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetNewsActivity(long NewsId)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetNewsActivity(NewsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetNewsActivity");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxGetNewsActivityParticipatingProducts
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetNewsActivityParticipatingProducts(long ActivityId)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetNewsActivityParticipatingProducts(ActivityId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetNewsActivityParticipatingProducts");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxGetNewsActivityParticipatingMachines
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetNewsActivityParticipatingMachines(long ActivityId)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetNewsActivityParticipatingMachines(ActivityId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetNewsActivityParticipatingMachines");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxGetProductChoiceList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetProductChoiceList(GetProductChoiceListParamsDTO Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetProductChoiceList(Params.VendingMachineIdsFilter, Params.ForAutoCoupons, Params.AllMachines), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetProductChoiceList");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxGetMachineChoiceList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetMachineChoiceList()
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetMachineChoiceList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetMachineChoiceList");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxSaveActivity
        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxSaveActivity(NewsActivitySaveDTO ActivityData)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.SaveActivity(ActivityData), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxSaveActivity");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AjaxGetActivitiesList
        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetActivityList()
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetActivityList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetActivityList");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        // News Ticker Management

        #region AjaxGetTickerList
        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetTickerList(bool GetOfflineToo = true)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.GetNewsTicker(GetOfflineToo), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetTickerList");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxChangeNewsTickerNews
        public class ChangeTickerPostParams
        {
            public long NewsIdOld { get; set; }
            public long NewsIdNew { get; set; }
        }
        
        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxChangeNewsTickerNews(ChangeTickerPostParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.ChangeNewsTickerNews(Params.NewsIdOld, Params.NewsIdNew), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxChangeNewsTickerNews");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxAddNewsTicker
        public class AddTickerPostParams
        {
            public long NewsId { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxAddNewsTicker(AddTickerPostParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.AddNewsTicker(Params.NewsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxAddNewsTicker");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxEnableDisableNewsTicker
        public class EnableDisableBannerPostParams
        {
            public long NewsId { get; set; }
            public bool Enabled { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxEnableDisableNewsTicker(EnableDisableBannerPostParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.EnableDisableBanner(Params.NewsId, Params.Enabled), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxEnableDisableNewsTicker");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxDeleteNewsTicker
        public class DeleteBannerPostParams
        {
            public long NewsId { get; set; }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxDeleteNewsTicker(DeleteBannerPostParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.DeleteBanner(Params.NewsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxDeleteNewsTicker");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region AjaxNewsTickerChangeOrder
        public class ChangeBannerOrderPostParams
        {
            public long NewsId { get; set; }
            public int NewBannerOrder { get; set; }
        }        
        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxNewsTickerChangeOrder(ChangeBannerOrderPostParams Params)
        {
            try
            {
                NewsProvider NP = new NewsProvider();
                return Json(NP.ChangeBannerOrder(Params.NewsId, Params.NewBannerOrder), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxNewsTickerChangeOrder");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Ticker Image Upload

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxUploadTickerBanner(long newsId)
        {
            try
            {                
                return Json(Kooco.Framework.Shared.UploadManager.HandleFileUpload("TickerBanner", newsId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxUploadTickerBanner");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxSaveTickerBanner(long newsId)
        {
            try
            {
                string ContentType = string.Empty;
                string FileName = string.Empty;
                byte[] filedata = Kooco.Framework.Shared.UploadManager.GetUploadedFileData("TickerBanner", newsId, ref ContentType, ref FileName);
                if (filedata == null)
                    return Json(new BaseResponseDTO(ReturnCodes.RecordNotFound, "The image uploaded already expired or is not stored on the server anymore. Please upload the image again!"), JsonRequestBehavior.AllowGet);

                NewsProvider NP = new NewsProvider();
                return Json(NP.SaveTickerBanner(newsId, ContentType, Path.GetExtension(FileName), filedata), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxSaveTickerBanner");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetUploadedBanner(long newsId)
        {
            try
            {
                return Kooco.Framework.Shared.UploadManager.GetUploadedFileMVC("TickerBanner", newsId);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in AjaxGetUploadedBanner");
                return Json(new BaseResponseDTO(ReturnCodes.InternalServerError), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion           
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Shared;

namespace VmaAdmin.Controllers
{
    public class PromotionProductsController : KoocoBaseController
    {
        // GET: PromotionProducts
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Orders()
        {
            return View();
        }

        #region AjaxGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<PromotionProductAdminDTO> Params)
        {
            try
            {
                PromotionProductsAdminProvider AP = new PromotionProductsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                PromotionProductsAdminProvider AP = new PromotionProductsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                PromotionProductsAdminProvider AP = new PromotionProductsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxCreateProduct
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxCreateProduct()
        {
            try
            {
                PromotionProductsAdminProvider AP = new PromotionProductsAdminProvider();
                return Json(AP.CreateNewProduct(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxOrdersGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxOrdersGetList(StandardSearchParamsDTO<PromotionOrderAdminDTO> Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxOrdersGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxOrdersGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxOrdersSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxOrdersSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxOrdersSetExcelExportParams
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxOrdersSetExcelExportParams(ExcelExportParamsDTO Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.SetExcelExportParams(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxOrdersGetExcel
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxOrdersGetExcel(StandardSearchParamsDTO<LotteryPriceCouponAdminDTO> Params)
        {
            PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
            return AP.GetExcel();
        }
        #endregion

        #region AjaxOrdersMarkPickedUp
        public class OrderParamsDTO
        {
            public long PromotionOrderId { get; set; }
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxOrdersMarkPickedUp(OrderParamsDTO Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.MarkPickedUp(Params.PromotionOrderId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxOrdersRefund
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxOrdersRefund(PromotionOrderRefundParamsDTO Params)
        {
            try
            {
                PromotionOrdersAdminProvider AP = new PromotionOrdersAdminProvider();
                return Json(AP.Refund(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
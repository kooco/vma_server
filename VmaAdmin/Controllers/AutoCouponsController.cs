﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Providers.Reporting;

namespace VmaAdmin.Controllers
{
    public class AutoCouponsController : KoocoBaseController
    {
        // GET: Stamps
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AutoCouponRedeemsReport()
        {
            return View();
        }

        #region AjaxGetList
        [ActionPermission(Action: "Index")]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<AutoCouponCriteriaAdminDTO> Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxCreateNewCriteria
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxCreateNewCriteria()
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.CreateNew(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion        
        #region AjaxGetCriteriaDetails
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetCriteriaDetails(long CriteriaId)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.GetCriteriaDetails(CriteriaId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxCheckCSharpModule
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxCheckCSharpModule(CheckCSharpModuleParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.CheckCSharpModule(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetModuleCode
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetModuleCode(long ModuleId)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.GetModuleCode(ModuleId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveCSharpModule
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxSaveCSharpModule(SaveCSharpModuleAdminParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.SaveCSharpModule(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion        
        #region AjaxSaveCriteriaDetails
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxSaveCriteriaDetails(SaveCriteriaDetailsAdminParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.SaveCriteriaDetails(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetModulesDropDownList
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetModulesDropDownList()
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.GetModulesDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxResetUserStates
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxResetUserStates(ResetUserStatesParamsDTO Params)
        {
            try
            {
                AutoCouponCriteriaAdminProvider AP = new AutoCouponCriteriaAdminProvider();
                return Json(AP.ResetUserStates(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region GetKendoAutoCouponRedeems
        [ActionPermission(Action: "AutoCouponRedeemsReport")]
        public ActionResult GetKendoAutoCouponRedeems([DataSourceRequest]DataSourceRequest request, DateTime? DateFrom, DateTime? DateTo, long? ProductId, long? ProductTypeId, long? ProductBrandId, long? MachineId, long? CriteriaId, long? ActivityId, string UserAccount, string UserName)
        {
            try
            {
                if ((DateFrom == null) || (DateTo == null))
                    throw new Exception("DateFrom and DateTo must be set!");

                UserCouponReportsProvider RP = new UserCouponReportsProvider();
                return new JsonFixedTimeResult(RP.GetKendoAutoCouponRedeems(request, DateFrom.Value, DateTo.Value, ProductId, ProductTypeId, ProductBrandId, MachineId, CriteriaId, ActivityId, UserAccount, UserName));
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "Error in GetKendoAutoCouponRedeems");
                throw new Exception("Internal error", exc);
            }
        }
        #endregion
        #region GetKendoAutoCouponRedeemsExcel
        [ActionPermission(Action: "AutoCouponRedeemsReport")]
        public ActionResult GetKendoAutoCouponRedeemsExcel()
        {
            UserCouponReportsProvider GP = new UserCouponReportsProvider();
            return GP.GetExcelAutoCouponRedeems();
        }
        #endregion
        #region AjaxGetProductsList
        [ActionPermission(Action: "BetTotals")]
        public ActionResult AjaxGetProductsList()
        {
            try
            {
                UserCouponReportsProvider RP = new UserCouponReportsProvider();
                return Json(RP.GetProductsList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
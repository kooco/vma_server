﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Models.DataTransferObjects;

namespace VmaAdmin.Controllers
{
    public class StatisticsController : KoocoBaseController
    {
        public ActionResult Users()
        {
            StatisticsProvider SP = new StatisticsProvider();
            SP.UpdateUserStatistics(1);

            return View();
        }

        public ActionResult Lottery()
        {
            StatisticsProvider SP = new StatisticsProvider();
            SP.UpdateLotteryStatistics(1);
            SP.UpdateWatersportsStatistics(1);

            return View();
        }

        public ActionResult Sales()
        {
            StatisticsProvider SP = new StatisticsProvider();
            SP.UpdateSalesStatistics(1);

            return View();
        }

        public ActionResult Activity()
        {
            StatisticsProvider SP = new StatisticsProvider();
            SP.UpdateActivityStatistics(1);

            return View();
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult GetStatistics(GetStatisticsBaseParamsDTO Params)
        {
            try
            {
                StatisticsProvider SP = new StatisticsProvider();
                return Json(SP.GetStatistics(Params, true), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "GetStatistics");
                throw new Exception("Internal server error", exc);
            }
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult GetCustomStatistics(GetCustomStatisticsBaseParamsDTO Params)
        {
            try
            {
                StatisticsProvider SP = new StatisticsProvider();
                return Json(SP.GetCustomStatistics(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "GetCustomStatistics");
                throw new Exception("Internal server error", exc);
            }
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult GetVendingMachinesList()
        {
            try
            {
                StatisticsProvider SP = new StatisticsProvider();
                return Json(SP.GetVendingMachinesList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                Utils.LogError(exc, "GetVendingMachinesList");
                throw new Exception("Internal server error", exc);
            }
        }
    }
}
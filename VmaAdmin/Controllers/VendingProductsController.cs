﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Shared;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;

namespace VmaAdmin.Controllers
{
    public class VendingProductsController : KoocoBaseController
    {
        // GET: VendingProducts
        public ActionResult Index()
        {
            AuthenticationManager.SetUserSessionVariable("ProductListUsingActivity", null);

            return View();
        }

        public ActionResult ProductTypes()
        {
            return View();
        }

        public ActionResult ProductBrands()
        {
            return View();
        }

        #region AjaxGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<VendingProductAdminDTO> Params)
        {
            try
            {
                // check if there is an filter for the ActivityId
                long? ActivityIdFilter = null;
                if (Params.HasFilterCriteria("ActivityId"))
                {
                    StandardSearchFilterCriteria Criteria = Params.GetFilterCriteria("ActivityId");
                    if ((Criteria.Value != null) && (Criteria.Value.ToString() != string.Empty) && (Criteria.Value.ToString() != "0"))
                        ActivityIdFilter = Convert.ToInt64(Criteria.Value);

                    if (ActivityIdFilter == null)
                        Params.FilterCriterias.Remove(Criteria);
                }

                if (ActivityIdFilter != null)
                {
                    AuthenticationManager.SetUserSessionVariable("ProductListUsingActivity", true);
                    VendingActivityProductAdminProvider AP = new VendingActivityProductAdminProvider();
                    return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    AuthenticationManager.SetUserSessionVariable("ProductListUsingActivity", false);
                    VendingProductsAdminProvider AP = new VendingProductsAdminProvider();
                    return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                VendingProductsAdminProvider AP = new VendingProductsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                bool UsesActivity = (bool)AuthenticationManager.GetUserSessionVariable("ProductListUsingActivity");

                if (UsesActivity)
                {
                    // we are currently using the activity-based product settings (the user selected an activity)

                    bool bIsProductProperty = true;

                    switch(Params.Name)
                    {
                        case "PointsMultiplier":
                        case "StampsMultiplier":
                        //case "PointsExtra":
                            bIsProductProperty = false;
                            break;
                    }

                    VendingActivityProductAdminProvider AP = new VendingActivityProductAdminProvider();

                    if (bIsProductProperty)
                    {
                        // if a product property should be changed -> use the base vending products admin provider with the product id instead of the activity-product-settings id
                        long ProductId = AP.GetProductIdByActivityProductSettingId(Convert.ToInt64(Params.Id));

                        VendingProductsAdminProvider VAP = new VendingProductsAdminProvider();
                        Params.Id = ProductId.ToString();
                        return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    // saving normal products (no activity selected)
                    VendingProductsAdminProvider AP = new VendingProductsAdminProvider();
                    return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxCreateProduct
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxCreateProduct()
        {
            try
            {
                VendingProductsAdminProvider AP = new VendingProductsAdminProvider();
                return Json(AP.CreateNewProduct(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetStampsSelectionList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetStampsSelectionList()
        {
            try
            {
                VendingProductsAdminProvider AP = new VendingProductsAdminProvider();
                return Json(AP.GetStampsSelectionList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxProductTypesGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxProductTypesGetList(StandardSearchParamsDTO<VendingProductTypeAdminDTO> Params)
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductTypesGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxProductTypesGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductTypesSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxProductTypesSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductTypesCreateNew
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxProductTypesCreateNew()
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.CreateNewProductType(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductTypesRemove
        [HttpPost]
        [ActionPermission("Index")]
        public ActionResult AjaxProductTypesRemove(RemoveRecordParamsDTO Params)
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.RemoveProductType(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProductTypesDDList
        [HttpGet]
        [ActionPermission("Index")]
        public ActionResult AjaxGetProductTypesDDList()
        {
            try
            {
                VendingProductTypesAdminProvider AP = new VendingProductTypesAdminProvider();
                return Json(AP.GetDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxProductBrandsGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxProductBrandsGetList(StandardSearchParamsDTO<VendingProductBrandAdminDTO> Params)
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductBrandsGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxProductBrandsGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductBrandsSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxProductBrandsSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductBrandsCreateNew
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxProductBrandsCreateNew()
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.CreateNewProductBrand(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxProductBrandsRemove
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxProductBrandsRemove(RemoveRecordParamsDTO Params)
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.RemoveProductBrand(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProductBrandsDDList
        [HttpGet]
        [ActionPermission("Index")]
        public ActionResult AjaxGetProductBrandsDDList()
        {
            try
            {
                VendingProductBrandsAdminProvider AP = new VendingProductBrandsAdminProvider();
                return Json(AP.GetDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
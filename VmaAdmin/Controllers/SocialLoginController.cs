﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;

using VmaBase.Providers;


namespace VmaAdmin.Controllers
{
    public class SocialLoginController : KoocoBaseController
    {
        // Facebook Login
        // ==============

        #region FacebookLogin
        [NoAuthentication]
        public ActionResult FacebookLogin()
        {
            SocialLoginProvider SP = new SocialLoginProvider();
            return Redirect(SP.Facebook_Login());
        }
        #endregion
        #region FacebookCallback
        [NoAuthentication]
        public ActionResult FacebookCallback(string code)
        {
            SocialLoginProvider SP = new SocialLoginProvider();
            return Redirect(SP.Facebook_ProcessCallback(code));
        }
        #endregion
        #region Logout
        [NoAuthentication]
        public ActionResult Logout()
        {
            SocialLoginProvider SP = new SocialLoginProvider();
            SP.Logout();
            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;

namespace VmaAdmin.Controllers
{
    public class SalesTransactionsController : KoocoBaseController
    {
        // GET: VendingProducts
        public ActionResult Index()
        {
            return View();
        }

        #region AjaxGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<SalesTransactionAdminDTO> Params)
        {
            try
            {
                SalesTransactionsAdminProvider AP = new SalesTransactionsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                SalesTransactionsAdminProvider AP = new SalesTransactionsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                SalesTransactionsAdminProvider AP = new SalesTransactionsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
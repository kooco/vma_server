﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;

using VmaBase.Models.DataTransferObjects.Response;
using VmaBase.Shared;
using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects.MOCCP;
using VmaBase.Models;

namespace VmaAdmin.Controllers
{
    public class HomeController : KoocoBaseController
    {
        public ActionResult Index()
        {
            //Kooco.Framework.Providers.PushNotificationProvider PP = new Kooco.Framework.Providers.PushNotificationProvider();
            //PP.SendTextNotification(152, "Test message with data", "Test message with data", PushAction.Create(VmaBase.Models.Enum.PushActionCode.FriendAdded, "150"));

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxDecryptAESResponse(string sString)
        {
            return Json(new BaseResponseDTO(Utils.BuyBuyAESDecrypt(sString)), JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Providers;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Models.DataTransferObjects.Response;

namespace VmaAdmin.Controllers
{
    public class AppUsersController : KoocoBaseController
    {
        // GET: Stamps
        public ActionResult Index()
        {
            return View();
        }

        #region AjaxGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<UserProfileAdminDTO> Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSetExcelExportParams
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxSetExcelExportParams(ExcelExportParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.SetExcelExportParams(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetExcel
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetExcel()
        {
            AppUsersAdminProvider AP = new AppUsersAdminProvider();
            return AP.GetExcel();
        }
        #endregion

        #region AjaxGetActvityDropDownList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetActvityDropDownList()
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetActivityDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetUserActivityInfo
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxGetUserActivityInfo(GetUserActivityInfoParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetUserActivityInfo(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxAddPoints
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxAddPoints(AppUserAddPointsAdminParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.AddPoints(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxAddLotteryCoins
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxAddLotteryCoins(AppUserAddCoinsAdminParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.AddLotteryCoins(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxAddWsCoins
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxAddWsCoins(AppUserAddCoinsAdminParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.AddWatersportsCoins(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxAddStamp
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxAddStamp(AddStampToUserAdminParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.AddStampToUser(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetUserId
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetUserId(string Account)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetUserId(Account), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetUserDetails
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxGetUserDetails(GetUserDetailsParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.GetUserDetails(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxSendNotification
        [HttpPost]
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxSendNotification(AdminSendPushParamsDTO Params)
        {
            try
            {
                PushNotificationProvider UP = new PushNotificationProvider();
                bool bOk = false;

                if ((Params.UID != null) && (Params.UID.Value > 0))
                    bOk = UP.SendTextNotification(Params.UID.Value, "您收到一條消息來自管理員", Params.Message);
                else
                    bOk = UP.BroadcastTextNotificationToAllUsers("您收到一條消息來自管理員", Params.Message);

                if (bOk)
                    return Json(new BaseResponseDTO(VmaBase.Shared.ReturnCodes.Success), JsonRequestBehavior.AllowGet);
                else
                    return Json(new BaseResponseDTO(VmaBase.Shared.ReturnCodes.InternalServerError, "Error when sending the push notification message: " + UP.LastError), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxRemoveUser
        public class RemoveUserParams
        {
            public long UID { get; set; }
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxRemoveUser(RemoveUserParams Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.RemoveUser(Params.UID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
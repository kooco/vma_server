﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Shared;

namespace VmaAdmin.Controllers
{
    public class LotteryController : KoocoBaseController
    {
        // GET: Stamps
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PriceItems()
        {
            return View();
        }

        public ActionResult Prices() // currently not used
        {
            return View();
        }

        #region AjaxLotteryCouponsGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryCouponsGetList(StandardSearchParamsDTO<LotteryPriceCouponAdminDTO> Params)
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryCouponsGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxLotteryCouponsGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryCouponsSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxLotteryCouponsSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryCouponsUserPickup
        public class LotterPricePickupParamsDTO
        {
            public long LotteryPriceCouponId { get; set; }
        }

        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxLotteryCouponsUserPickup(LotterPricePickupParamsDTO Params)
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.PriceUserPickup(Params.LotteryPriceCouponId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryCouponsSetExcelExportParams
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxLotteryCouponsSetExcelExportParams(ExcelExportParamsDTO Params)
        {
            try
            {
                AppUsersAdminProvider AP = new AppUsersAdminProvider();
                return Json(AP.SetExcelExportParams(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryCouponsGetExcel
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxLotteryCouponsGetExcel(StandardSearchParamsDTO<LotteryPriceCouponAdminDTO> Params)
        {
            LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
            return AP.GetExcel();
        }
        #endregion

        #region AjaxLotteryGetPricesDropDownList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryGetPricesDropDownList()
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.GetPricesDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxLotteryPriceItemsGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryPriceItemsGetList(StandardSearchParamsDTO<LotteryPriceItemAdminDTO> Params)
        {
            try
            {
                LotteryPriceItemsAdminProvider AP = new LotteryPriceItemsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryPriceItemsGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxLotteryPriceItemsGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                LotteryPriceItemsAdminProvider AP = new LotteryPriceItemsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryPriceItemsSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxLotteryPriceItemsSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                LotteryPriceItemsAdminProvider AP = new LotteryPriceItemsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxLotteryPricesGetList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryPricesGetList(StandardSearchParamsDTO<LotteryPriceItemDTO> Params)
        {
            try
            {
                LotteryPricesAdminProvider AP = new LotteryPricesAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryPricesGetProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpGet]
        public ActionResult AjaxLotteryPricesGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                LotteryPricesAdminProvider AP = new LotteryPricesAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxLotteryPricesSaveProperty
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        [HttpPost]
        public ActionResult AjaxLotteryItemSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                LotteryPricesAdminProvider AP = new LotteryPricesAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxLotteryGetGamesDropDownList
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryGetGamesDropDownList()
        {
            try
            {
                LotteryPriceCouponsAdminProvider AP = new LotteryPriceCouponsAdminProvider();
                return Json(AP.GetGamesDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion

        #region AjaxLotteryGetBaseStatistics
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxLotteryGetBaseStatistics()
        {
            try
            {
                LotteryPricesAdminProvider AP = new LotteryPricesAdminProvider();
                return Json(AP.GetLotteryBaseStatistics(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxWatersportsGetBaseStatistics
        [PrincipalPermission(PermissionType = PermissionTypes.AdminRequired)]
        public ActionResult AjaxWatersportsGetBaseStatistics()
        {
            try
            {
                LotteryPricesAdminProvider AP = new LotteryPricesAdminProvider();
                return Json(AP.GetWatersportsBaseStatistics(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kooco.Framework.Controllers;
using Kooco.Framework.Authentication;
using Kooco.Framework.Models.DataTransferObjects.Input;

using VmaBase.Providers;
using VmaBase.Models.DataTransferObjects;
using VmaBase.Models.DataTransferObjects.AdminResponse;
using VmaBase.Models.DataTransferObjects.AdminInput;
using VmaBase.Shared;

namespace VmaAdmin.Controllers
{
    public class UserCouponsController : KoocoBaseController
    {
        // GET: PromotionProducts
        public ActionResult Index()
        {
            return View();
        }

        #region AjaxGetList
        [ActionPermission(Action: "Index")]
        public ActionResult AjaxGetList(StandardSearchParamsDTO<UserCouponAdminDTO> Params)
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.GetList(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetProperty
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetProperty(GetPropertyParamsDTO Params)
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.GetProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSaveProperty
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxSaveProperty(SavePropertyParamsDTO Params)
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.SaveProperty(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxSetExcelExportParams
        [ActionPermission(Action: "Index")]
        [HttpPost]
        public ActionResult AjaxSetExcelExportParams(ExcelExportParamsDTO Params)
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.SetExcelExportParams(Params), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetExcel
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetExcel(StandardSearchParamsDTO<UserCouponAdminDTO> Params)
        {
            UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
            return AP.GetExcel();
        }
        #endregion

        #region AjaxGetActivitiesDropDownList
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetActivitiesDropDownList()
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.GetActivitiesDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
        #region AjaxGetCriteriasDropDownList
        [ActionPermission(Action: "Index")]
        [HttpGet]
        public ActionResult AjaxGetCriteriasDropDownList()
        {
            try
            {
                UserCouponsAdminProvider AP = new UserCouponsAdminProvider();
                return Json(AP.GetCriteriasDropDownList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                return HandleException(exc);
            }
        }
        #endregion
    }
}
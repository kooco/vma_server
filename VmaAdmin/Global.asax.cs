﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using System.Data.Entity.SqlServer;

using AutoMapper;

using VmaBase.Models.DataStorage;
using VmaAdmin.Shared;


// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace VmaAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SqlProviderServices.SqlServerTypesAssemblyName = "Microsoft.SqlServer.Types, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91";

            GlobalConfiguration.Configure(MaximaWeb.WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

			Kooco.Framework.Shared.FrameworkManager.Initialize(GlobalConst.ApplicationName, GlobalConst.ApplicationVersion, GetMappingProfile(), Kooco.Framework.Shared.FrameworkLanguages.ChineseTW, "/", GlobalConst.ApiKey);

			ApplicationDbContext DbContext = new ApplicationDbContext();
			DbContext.Database.Initialize(false);
        }

        public static Profile GetMappingProfile()
        {
            return new VmaBase.Shared.MappingProfile();
        }
    }
}

$( window ).load( function () {

  'use strict';


  /**
   * Color definitions
   */

  var color01 = '#2196f3',
    color02 = '#f44336',
    color03 = '#3f51b5',
    color04 = '#ffc107',
    color05 = '#212121',
    color06 = '#4caf50';


  /**
   * Chart elements
   */

  var barChart = $( '#bar-chart' ),
    areaChart = $( '#area-chart' ),
    lineChart = $( '#line-chart' ),
    stackChart = $( '#stack-chart' ),
    realTimeUpdateChart = $( "#realTimeUpdate-chart" ),
    donutChart = $( "#donut-chart" );



  // -----------------------------------------------
  // @Bar Chart
  // -----------------------------------------------

  /**
   * Bar Chart Data
   */

  var dataBar = [ {
    data: [
      [ 1, 20 ]
    ],
    color: color01
  }, {
    data: [
      [ 2, 40 ]
    ],
    color: color02
  }, {
    data: [
      [ 3, 60 ]
    ],
    color: color03
  }, {
    data: [
      [ 4, 60 ]
    ],
    color: color04
  }, {
    data: [
      [ 5, 40 ]
    ],
    color: color05
  }, {
    data: [
      [ 6, 20 ]
    ],
    color: color06
  } ];


  /**
   * Bar Chart Options
   */

  var optionsBar = {
    series: {
      lines: {
        fill: false
      },

      points: {
        show: false
      },

      bars: {
        show: true,
        barWidth: 0.5,
        lineWidth: 1,
        fill: 1,
        align: 'center'
      }
    },

    xaxis: {
      tickLength: 0,

      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      position: 'bottom',

      ticks: [
        [ 1, 'Direct' ],
        [ 2, 'Links' ],
        [ 3, 'Search' ],
        [ 4, 'Social' ],
        [ 5, 'Mail' ],
        [ 6, 'Display' ]
      ]
    },

    yaxis: {
      tickColor: 'rgba(232, 235, 238, 0.5)',

      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      }
    },

    grid: {
      borderWidth: {
        top: 1,
        right: 1,
        bottom: 1,
        left: 1
      },

      borderColor: 'rgba(232, 235, 238, 0.5)',
      margin: 0,
      minBorderMargin: 0,
      labelMargin: 14,
      hoverable: true,
      clickable: true,
      mouseActiveRadius: 6
    },

    tooltip: true,

    tooltipOpts: {
      content: '%y',
      defaultTheme: false,
      shifts: {
        x: 0,
        y: 20
      }
    }
  };


  /**
   * Init Bar Chart
   */

  if ( barChart.length > 0 ) {
    var plotBars = $.plot( barChart, dataBar, optionsBar );

    $( window ).resize( function () {
      plotBars.resize();
      plotBars.setupGrid();
      plotBars.draw();
    } );
  }




  // -----------------------------------------------
  // @Area Chart
  // -----------------------------------------------


  /**
   * Area Chart Data
   */

  var area1 = {
    data: [
      [ 0, 10 ],
      [ 1, 8 ],
      [ 2, 16 ],
      [ 3, 8 ],
      [ 4, 10 ],
      [ 5, 6 ],
      [ 6, 12 ],
      [ 7, 4 ],
      [ 8, 8 ],
      [ 9, 10 ]
    ],

    label: 'Chrome',

    points: {
      show: true,
      radius: 1
    },

    splines: {
      show: true,
      tension: 0.45,
      lineWidth: 1,
      fill: 0.8
    }
  };


  var area2 = {
    data: [
      [ 0, 6 ],
      [ 1, 4 ],
      [ 2, 12 ],
      [ 3, 4 ],
      [ 4, 6 ],
      [ 5, 2 ],
      [ 6, 8 ],
      [ 7, 1 ],
      [ 8, 4 ],
      [ 9, 6 ]
    ],

    label: 'Firefox',

    points: {
      show: true,
      radius: 1
    },

    splines: {
      show: true,
      tension: 0.45,
      lineWidth: 1,
      fill: 0.8
    }
  };


  var dataArea = [ area1, area2 ];


  /**
   * Area Chart Options
   */

  var optionsArea = {
    colors: [ color01, color03 ],


    legend: {
      backgroundOpacity: 0,
      position: 'ne',
      noColumns: 2
    },

    xaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      ticks: [
        [ 0, 'January' ],
        [ 1, 'February' ],
        [ 2, 'March' ],
        [ 3, 'April' ],
        [ 4, 'May' ],
        [ 5, 'June' ],
        [ 6, 'July' ],
        [ 7, 'August' ],
        [ 8, 'September' ],
        [ 9, 'October' ],
        [ 10, 'November' ],
        [ 11, 'December' ]
      ],

      rotateTicks: 135,
      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    yaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    grid: {
      borderWidth: {
        top: 1,
        right: 1,
        bottom: 1,
        left: 1
      },

      borderColor: 'rgba(232, 235, 238, 0.5)',
      margin: 0,
      minBorderMargin: 0,
      labelMargin: 14,
      hoverable: true,
      clickable: true,
      mouseActiveRadius: 6
    },

    tooltip: true,
    tooltipOpts: {
      content: 'Usage is %y%',
      defaultTheme: false,
      shifts: {
        x: 0,
        y: 20
      }
    }
  };


  /**
   * Area Chart Options
   */

  if ( areaChart.length > 0 ) {

    var plotAreas = $.plot( areaChart, dataArea, optionsArea );

    $( window ).resize( function () {
      plotAreas.resize();
      plotAreas.setupGrid();
      plotAreas.draw();
    } )

  };



  // -----------------------------------------------
  // @Line Chart
  // -----------------------------------------------


  /**
   * Line Chart Data
   */

  var line1 = {
    data: [
      [ 0, 10 ],
      [ 1, 8 ],
      [ 2, 16 ],
      [ 3, 8 ],
      [ 4, 10 ],
      [ 5, 6 ],
      [ 6, 12 ],
      [ 7, 4 ],
      [ 8, 8 ],
      [ 9, 10 ]
    ],

    label: 'Angular',

    points: {
      show: true,
      radius: 1
    },

    splines: {
      show: true,
      tension: 0.45,
      lineWidth: 1,
      fill: 0
    }
  };


  var line2 = {
    data: [
      [ 0, 12 ],
      [ 1, 16 ],
      [ 2, 12 ],
      [ 3, 4 ],
      [ 4, 10 ],
      [ 5, 2 ],
      [ 6, 8 ],
      [ 7, 6 ],
      [ 8, 4 ],
      [ 9, 10 ]
    ],

    label: 'React',

    points: {
      show: true,
      radius: 1
    },

    splines: {
      show: true,
      tension: 0.45,
      lineWidth: 1,
      fill: 0
    }
  };


  var dataLine = [ line1, line2 ];



  /**
   * Line Chart Options
   */

  var optionsLine = {
    colors: [ color02, color04 ],
    series: {
      shadowSize: null
    },

    legend: {
      backgroundOpacity: 0,
      position: 'ne',
      noColumns: 2
    },

    xaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)',
      position: 'bottom',
      ticks: [
        [ 0, 'Jan' ],
        [ 1, 'Feb' ],
        [ 2, 'Mar' ],
        [ 3, 'Apr' ],
        [ 4, 'May' ],
        [ 5, 'Jun' ],
        [ 6, 'Jul' ],
        [ 7, 'Aug' ],
        [ 8, 'Sep' ],
        [ 9, 'Oct' ],
        [ 10, 'Nov' ],
        [ 11, 'Dec' ]
      ]
    },

    yaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    grid: {
      borderWidth: {
        top: 1,
        right: 1,
        bottom: 1,
        left: 1
      },

      borderColor: 'rgba(232, 235, 238, 0.5)',
      margin: 0,
      minBorderMargin: 0,
      labelMargin: 14,
      hoverable: true,
      clickable: true,
      mouseActiveRadius: 6
    },

    tooltip: true,
    tooltipOpts: {
      content: '%x.1 is %y.4',
      defaultTheme: false,
      shifts: {
        x: 0,
        y: 20
      }
    }
  };


  /**
   * Line Chart Init
   */

  if ( lineChart.length > 0 ) {
    var plotLines = $.plot( lineChart, dataLine, optionsLine );

    $( window ).resize( function () {
      plotLines.resize();
      plotLines.setupGrid();
      plotLines.draw();
    } )
  };



  // -----------------------------------------------
  // @Stack Chart
  // -----------------------------------------------

  /**
   * Stack Chart Data
   */

  var dataStack = [ {
    label: 'Pizza',
    data: [
      [ 10, 120 ],
      [ 20, 70 ],
      [ 30, 70 ],
      [ 40, 60 ]
    ]
  }, {
    label: 'Ham',
    data: [
      [ 10, 50 ],
      [ 20, 60 ],
      [ 30, 90 ],
      [ 40, 35 ]
    ]
  }, {
    label: 'Cheeze',
    data: [
      [ 10, 80 ],
      [ 20, 40 ],
      [ 30, 30 ],
      [ 40, 20 ]
    ]
  } ];


  /**
   * Stack Chart Options
   */

  var dataOptions = {
    legend: {
      backgroundOpacity: 0,
      position: 'ne',
      noColumns: 3
    },

    bars: {
      show: true,
      fill: true,
      lineWidth: 0,
      barWidth: 2,
      order: 1
    },

    colors: [ color03, color04, color05 ],

    series: {
      shadowSize: 1
    },

    xaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    yaxis: {
      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    grid: {
      borderWidth: {
        top: 1,
        right: 1,
        bottom: 1,
        left: 1
      },

      borderColor: 'rgba(232, 235, 238, 0.5)',
      margin: 0,
      minBorderMargin: 0,
      labelMargin: 14,
      hoverable: true,
      clickable: true,
      mouseActiveRadius: 6
    },

    tooltip: true
  };


  /**
   * Stack Chart Init
   */

  if ( stackChart.length > 0 ) {
    var plotStack = $.plot( stackChart, dataStack, dataOptions );

    $( window ).resize( function () {
      plotStack.resize();
      plotStack.setupGrid();
      plotStack.draw();
    } );
  };




  // -----------------------------------------------
  // @Real-time Update Chart
  // -----------------------------------------------

  /**
   * Real-time Chart Options
   */

  var optionsRealtime = {
    colors: [ color02 ],
    series: {
      shadowSize: 0
    },

    yaxis: {
      min: 0,
      max: 100,

      font: {
        color: 'rgba(54, 76, 99, 0.92)'
      },

      tickColor: 'rgba(232, 235, 238, 0.5)'
    },

    xaxis: {
      show: false
    },

    grid: {
      borderWidth: {
        top: 1,
        right: 1,
        bottom: 1,
        left: 1
      },

      borderColor: 'rgba(232, 235, 238, 0.5)',
      margin: 0,
      minBorderMargin: 0,
      labelMargin: 14
    },
  };


  /**
   * Real-time Chart Core
   */

  if ( realTimeUpdateChart.length > 0 ) {

    var data = [],
      totalPoints = 300;

    function getRandomData() {

      if ( data.length > 0 )
        data = data.slice( 1 );

      while ( data.length < totalPoints ) {
        var prev = data.length > 0 ? data[ data.length - 1 ] : 50,
          y = prev + Math.random() * 10 - 5;

        if ( y < 0 ) {
          y = 0;
        } else if ( y > 100 ) {
          y = 100;
        }

        data.push( y );
      }

      var res = [];
      for ( var i = 0; i < data.length; ++i ) {
        res.push( [ i, data[ i ] ] )
      }

      return res;
    };


    /**
     * Real-time Chart Init
     */

    var plot = $.plot( realTimeUpdateChart, [ getRandomData() ], optionsRealtime );

    function update() {
      plot.setData( [ getRandomData() ] );
      plot.draw();
      setTimeout( update, 30 );
    }

    update();
  };


  // -----------------------------------------------
  // @Donut Chart
  // -----------------------------------------------


  /**
   * Donut Chart Data
   */

  var dataDonut = [ {
    label: "Data A",
    data: 90
  }, {
    label: "Data B",
    data: 50
  }, {
    label: "Data C",
    data: 90
  }, {
    label: "Data D",
    data: 70
  }, {
    label: "Data E",
    data: 80
  } ];


  /**
   * Donut Chart Options
   */

  var optionsDonut = {
    series: {
      pie: {
        show: true,
        innerRadius: 0.7,
        stroke: {
          width: 0
        },

        label: {
          show: false,
          threshold: 0.05
        }
      }
    },

    colors: [ '#7266ba', '#2196f3', '#27c24c', '#fad733', '#f05050' ],

    grid: {
      hoverable: true,
      clickable: true,
      borderWidth: 0,
      color: 'rgba(54, 76, 99, 0.92)'
    },

    tooltip: true,
    tooltipOpts: {
      content: '%p.0%'
    }
  };


  /**
   * Donut Chart Init
   */

  if ( donutChart.length > 0 ) {
    var plotDonut = $.plot( donutChart, dataDonut, optionsDonut );
  };

} );
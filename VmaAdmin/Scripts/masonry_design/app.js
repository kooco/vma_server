jQuery( document ).ready( function ( $ ) {

    'use strict';

    // ---------------------------------------------------------
    // #Screen Size
    // ---------------------------------------------------------

    var win = $(window),
      winWidth = win.width(),
      winHeight = win.height();


    win.on('resize', function () {
        winWidth = win.width();
        winHeight = win.height();
    });




    // ---------------------------------------------------------
    // #Date Picker
    // ---------------------------------------------------------

    var tightContent = $('.c-content--tight'),
      mainContainer = $('.c-main'),
      pageHeader = $('.c-pageHeader');

    function adjustTightContentHeight() {
        if (tightContent.length > 0) {
            if (winWidth > 1023) {
                var tightContentHeight = tightContent.outerHeight(),
                  pageHeaderHeight = pageHeader.outerHeight(),
                  mainContainerHeight = tightContentHeight - pageHeaderHeight;

                mainContainer.height(mainContainerHeight)
            } else {
                mainContainer.height('auto')
            }
        }

    }

    adjustTightContentHeight();

    win.on('resize', function () {
        adjustTightContentHeight();
    });



    // ---------------------------------------------------------
    // #Date Picker
    // ---------------------------------------------------------

    $('.datepicker').datepicker({
        prevText: '<i class="c-icon c-icon--main">keyboard_arrow_left</i>',
        nextText: '<i class="c-icon c-icon--main">keyboard_arrow_right</i>'
    });



    // ---------------------------------------------------------
    // #Masonry
    // ---------------------------------------------------------

    win.on('load', function () {
        /*
        var result = $('.c-Masonry').masonry({
            itemSelector: '.o-grid__cell',
            columnWidth: '.c-Masonry__sizer',
            percentPosition: true
        });

        $('.c-Masonry').masonry('layout');*/
    });


    // ---------------------------------------------------------
    // #Sortable
    // ---------------------------------------------------------


    $(".has-portlets").sortable({
        connectWith: ".has-portlets",
        handle: ".c-portlet__handle"
    });


    // ---------------------------------------------------------
    // #Datatables
    // ---------------------------------------------------------

    $('table.dataTable').dataTable();



    // ---------------------------------------------------------
    // #Forms Page
    // ---------------------------------------------------------

    var inputs = $('input, textarea, select'),
      controls = $('.c-control');

    inputs.on({
        'focus': function () {
            controls.removeClass('is-focused')
            var _this = $(this)
            var parentControl = _this.closest('.c-control')
            parentControl.addClass('is-focused')
        },

        'change': function () {
            var _this = $(this)
            var parentControl = _this.closest('.c-control')
            parentControl.addClass('is-dirty')
        },

        'blur': function () {
            controls.removeClass('is-focused')
        }
    });


    // ---------------------------------------------------------
    // #Textarea
    // ---------------------------------------------------------

    autosize(document.querySelectorAll('textarea.autoresizable'));



    // ---------------------------------------------------------
    // #Email
    // ---------------------------------------------------------

    function handleMain() {
        if (winWidth < 1367) {

            if (!$('.c-emailMsgWrapper').hasClass('is-active')) {
                $('.c-emailMsgWrapper').hide();
            }

            $('.c-mailbox__briefTitle').on('click', function (event) {
                event.preventDefault();

                $('.c-mailboxMessages').hide();
                $('.c-emailMsgWrapper').show();
                $('.c-emailMsgWrapper').addClass('is-active');
            });


            $('.fn-messagesRevealer').on('click', function (event) {
                event.preventDefault();

                $('.c-mailboxMessages').show();
                $('.c-emailMsgWrapper').hide();
                $('.c-emailMsgWrapper').removeClass('is-active');
            });
        } else {
            $('.c-mailboxMessages').show();
            $('.c-emailMsgWrapper').show();
        }
    };

    handleMain()
    win.on('resize', function () {
        handleMain()
    });




    // ---------------------------------------------------------
    // #Hover to Click
    // ---------------------------------------------------------

    var event = ('ontouchstart' in window) ? 'click' : 'mouseenter mouseleave';

    $('[data-touch-alt]').on(event, function () {
        $(this).toggleClass('is-tapped');
    });




    // ---------------------------------------------------------
    // #Menu
    // ---------------------------------------------------------

    applyTriggers();


    // ---------------------------------------------------------
    // #Right Sidebar
    // ---------------------------------------------------------

    $('.fn-sidebarTrigger').on('click', function (event) {
        event.preventDefault();
        $('body').toggleClass('js-sidebar-open')
    });



    // ---------------------------------------------------------
    // #Offcanvas
    // ---------------------------------------------------------

    /**
     * Handling offcanvas collapsing functionality
     * by setting the appropriate classes.
     */

    $('.fn-offcanvasCollapse').on('click', function (event) {
        event.preventDefault();

        $('body').toggleClass('js-offcanvas--collapsed')
        $('.c-menu .c-menu').toggleClass('c-menu--pinned c-menu--floated')
        $('.c-offcanvas .is-tapped').removeClass('is-tapped')
        $('.c-offcanvas .is-visible').removeClass('is-visible')
    });



    /**
     * Reset offcanvas classes for small screen sizes
     * and trigger tapping functionality for touch devices.
     */

    function offcanvasTypeSetter() {
        if (window.matchMedia('(max-width: 799px)').matches) {
            $('body').removeClass('js-offcanvas--collapsed');
            $('.c-menu .c-menu--floated').addClass('c-menu--pinned')
            $('.c-menu .c-menu').removeClass('c-menu--floated')
            $('body').addClass('js-offcanvas--out');
        } else {
            $('body').removeClass('js-offcanvas--out');
            $('body').removeClass('js-offcanvas--open');
        }
    }

    offcanvasTypeSetter();

    win.on('resize', function () {
        offcanvasTypeSetter();
    });


    /**
     * Handling offcanvas behavior on small screen
     * devices - setting the offcanvas outside of document.
     */

    $('.fn-offcanvasShow').on('click', function (event) {
        event.preventDefault()
        $('body').toggleClass('js-offcanvas--open')
    });


    win.on('load', function () {
        $(".js-sticky").stick_in_parent({
            recalc_every: 1
        });
    });


    // ---------------------------------------------------------
    // #Editor
    // ---------------------------------------------------------


    if ($('#toolbar').length > 0) {
        var editor = new wysihtml5.Editor("textarea", {
            toolbar: "toolbar",
            stylesheets: "styles/app.css",
            parserRules: wysihtml5ParserRules
        });
    }




    // ---------------------------------------------------------
    // #Scrollbarsw
    // ---------------------------------------------------------

    if (!isMobile.any) {
        win.on('load', function () {
            $('[data-scrollbar]').perfectScrollbar();
        });
    }




    // ---------------------------------------------------------
    // #Tabs
    // ---------------------------------------------------------

    $('[data-tab]').on('click', function (event) {
        event.preventDefault();

        var
          _this = $(this);

        if (!_this.hasClass('is-active')) {
            var
              target = $(_this.attr('href')),
              tabEnv = _this.attr('data-tab-env'),
              tabs = $('[data-tab-env=' + tabEnv + ']'),
              activeTab = tabs.filter('.is-active'),
              activeTabTarget = $(activeTab.attr('href'));

            tabs.removeClass('is-active');
            activeTabTarget.addClass('u-hide');
            _this.addClass('is-active');
            target.removeClass('u-hide');
        }

    });



    $(document).ready(function () {
        $('[data-tab]').on('click', function () {
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })

    })            

});

var currentDropDownDismissEvent = null;

function applyTriggers() {
    /**
     * Dropdown trigger function
     */

    var
      triggers = document.querySelectorAll('.fn-dropdownTrigger'),
      dropdowns = document.querySelectorAll('.c-menu--dropdown');

    for (var i = 0; i < triggers.length; i++) {
        triggers[i].removeEventListener('click', dropdownButtonClick);
        triggers[i].addEventListener('click', dropdownButtonClick);

        function dropdownButtonClick(event) {
                event.preventDefault();
                event.stopPropagation();

                var
                  _this = this,
                  dropdown = _this.nextElementSibling;
                
                if (!dropdown.classList.contains('is-visible')) {
                    for (var j = dropdowns.length - 1; j >= 0; j--) {
                        dropdowns[j].classList.remove('is-visible');
                    }

                    dropdown.classList.add('is-visible');
                } else {
                    // dropdown.classList.remove('is-visible');
                }

                document.addEventListener('click', dropdownDismiss);
                var currentDropDownDismissEvent = dropdownDismiss;

                function dropdownDismiss(event) {
                   document.removeEventListener('click', dropdownDismiss);
                   dropdown.classList.remove('is-visible');
                }
           }
    }
}